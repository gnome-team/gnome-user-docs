<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-automatic" xml:lang="uk">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="review"/>
    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.12" date="2014-06-18" status="review"/>
    
    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Автоматичний вхід до облікового запису користувача при вході до системи.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Налаштовування автоматичного входу до системи</title>

  <p its:locNote="TRANSLATORS: 'Administrator' and 'Automatic Login' are   strings from the Users dialog in the Settings panel user interface.">Користувач із типом облікового запису <em>Адміністратор</em> може <link href="help:gnome-help/user-autologin">вмикати <em>Автоматичний вхід</em> з панелі <app>Параметри</app></link>. Ви також можете налаштувати автоматичний вхід вручну у файлі нетипових налаштувань <sys its:translate="no">GDM</sys>, як це показано нижче.</p>
  
  <steps>
    <item>
      <p>Відкрийте для редагування файл <file its:translate="no">/etc/gdm/custom.conf</file> і переконайтеся, що у розділі <code>[daemon]</code> файлі вказано таке:</p>
      <listing its:translate="no">
        <title><file>custom.conf</file></title>
<code>
[daemon]
AutomaticLoginEnable=<input>True</input>
AutomaticLogin=<input>username</input>
</code>
      </listing>
    </item>
    <item>
      <p>Замініть <input its:translate="no">username</input> на назву облікового запису користувача, який має входити до системи автоматично.</p>
    </item>
  </steps>

  <note>
    <p>Файл <file its:translate="no">custom.conf</file> зазвичай зберігається у <file its:translate="no">/etc/gdm/</file>, але місце зберігання може бути іншим, залежно від вашого дистрибутива.</p>
  </note>

</page>
