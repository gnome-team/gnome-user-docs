<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="mime-types" xml:lang="uk">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Типи MIME використовуються для ідентифікації формату файла.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

    <title>Що таке типи MIME?</title>
    <p>У GNOME типи MIME (<em>Multipurpose Internet Mail Extension</em> або «багатоцільове розширення інтернет-пошти») використовують для ідентифікації формату файла. Стільничне середовище GNOME використовує типи MIME з такою метою:</p>
    <list>
      <item>
        <p>Визначення, яку програму слід відкрити типово для файла у певному форматі.</p>
      </item>
      <item>
        <p>Реєстрації інших програм, які також можуть відкривати файли у певному форматі.</p>
      </item>
      <item>
        <p>Визначення рядка, який описує тип файла, наприклад, у вікні властивостей програми <app>Файли</app>.</p>
      </item>
      <item>
        <p>Визначення піктограми, яку слід використовувати для файлів у певному форматі, наприклад, у вікні властивостей програми <app>Файли</app>.</p>
      </item>
    </list>
    <p>Назви типів MIME записують у такому форматі:</p>
<screen>
<var>тип-даних</var>/<var>ідентифікатор-підтипу</var>
</screen>
<p>Прикладом типу MIME є <sys>image/jpeg</sys>, де <sys>image</sys> є назвою мультимедійного типу, а <sys>jpeg</sys> є ідентифікатором підтипу.</p>
    <p>У GNOME реалізовано специфікацію <em>спільних даних MIME freedesktop.org</em> для визначення:</p>
    <list>
    <item>
      <p>Загальносистемних каталогів та каталогів користувача для зберігання усіх файлів специфікацій типів MIME.</p>
    </item>
    <item>
      <p>Як зареєструвати тип MIME так, щоб повідомити стільничному середовищу, які програми можна використовувати для відкриття файлів у певному форматі.</p>
    </item>
    <item>
      <p>Як користувач може змінити програми, які мають відкривати файли у певних форматах.</p>
    </item>
    </list>
    <section id="mime-database">
      <title>Для чого призначено базу даних MIME?</title>
      <p>База даних MIME є збіркою усіх файлів специфікацій типів MIME, яку GNOME використовує для зберігання даних щодо відомих типів MIME.</p>
      <p>Найважливішою частиною бази даних MIME з точки зору адміністратора системи є каталог <file>/usr/share/mime/packages/</file>, де зберігаються пов'язані із типами MIME файли, де містяться відомості щодо відомих типів MIME. Одним із прикладів таких файлів є файл <file>/usr/share/mime/packages/freedesktop.org.xml</file>, у якому зберігаються відомості щодо стандартних типів MIME, які є доступними у типовій системі. Цей файл є частиною пакунка <sys>shared-mime-info</sys>.</p>
    </section>
    <section id="mime-types-more-information">
    <title>Отримання додаткових відомостей</title>
    <p>Докладні відомості з описом системи типів MIME наведено у <em>специфікації спільних відомостей MIME freedesktop.org</em>, яку можна знайти на сайті freedesktop.org:</p>
    <list>
      <item>
        <p><link href="http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/"> http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/</link></p>
      </item>
    </list>
    </section>
</page>
