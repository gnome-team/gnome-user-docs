<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-user" xml:lang="uk">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <link type="seealso" xref="session-custom"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="draft"/>
    <revision pkgversion="3.8" date="2013-08-06" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Визначення типового сеансу для користувача.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Налаштовування типового сеансу користувача</title>

  <p>Типовий сеанс отримується за допомогою програми, яка має назву <app>AccountsService</app>. <app>AccountsService</app> зберігає дані у каталозі <file>/var/lib/AccountsService/users/</file>.</p>

<note style="note">
  <p>У GNOME 2 для створення типових сеансів використовувався файл <file>.dmrc</file> у домашньому каталозі користувача. У нових версіях цей файл <file>.dmrc</file> не використовується.</p>
</note>

  <steps>
    <title>Визначення типового сеансу для користувача</title>
    <item>
      <p>Переконайтеся, що у вашій системі встановлено пакунок <sys>gnome-session-xsession</sys>.</p>
    </item>
    <item>
      <p>Перейдіть до каталогу <file>/usr/share/xsessions</file>, ще зберігаються файли <file>.desktop</file> для усіх доступних сеансів. Ознайомтеся із вмістом файлів <file>.desktop</file>, щоб визначити сеанс, яким ви хочете скористатися.</p>
    </item>
    <item>
      <p>Щоб вказати типовий сеанс для користувача, оновіть запис <sys>account service</sys> користувача у файлі <file>/var/lib/AccountsService/users/<var>користувач</var></file>:</p>
<code>[User]
Language=
XSession=gnome-classic</code>
       <p>У цьому зразку як типовий сеанс встановлено <link href="help:gnome-help/gnome-classic">GNOME Classic</link> за допомогою файла <file>/usr/share/xsessions/gnome-classic.desktop</file>.</p>
     </item>
  </steps>

  <p>Після визначення типового сеансу для користувача цей сеанс буде використано під час наступного входу користувача до системи, якщо користувач не вибере інший сеанс на екрані входу до системи.</p>

</page>
