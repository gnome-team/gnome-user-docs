<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom" xml:lang="uk">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Створення специфікації типу MIME і реєстрація типової програми.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

    <title>Додавання нетипового типу MIME для усіх користувачів</title>
    <p>Щоб додати нетиповий тип MIME для усіх користувачів системи і зареєструвати типову програму для цього типу MIME, вам слід створити файл специфікації типу MIME у каталозі <file>/usr/share/mime/packages/</file> і файл <file>.desktop</file> у каталозі <file>/usr/share/applications/</file>.</p>
    <steps>
      <title>Додавання нетипового типу MIME <sys>application/x-newtype</sys> для усіх користувачів</title>
      <item>
        <p>Створіть файл <file>/usr/share/mime/packages/application-x-newtype.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>Наведений вище зразок файла <file>application-x-newtype.xml</file> визначає новий тип MIME <sys>application/x-newtype</sys> і пов'язує назви файлів із суфіксом <file>.xyz</file> із цим типом MIME.</p>
      </item>
      <item>
        <p>Створіть файл <file>.desktop</file> із назвою, наприклад, <file>myapplication1.desktop</file>, і збережіть його у каталозі <file>/usr/share/applications/</file>:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>Наведений вище зразок файла <file>myapplication1.desktop</file> пов'язує тип MIME <sys>application/x-newtype</sys> із програмою із назвою <app>My Application 1</app>, запуск якої відбувається за допомогою команди <cmd>myapplication1</cmd>.</p>
      </item>
      <item>
        <p>Від імені root, оновіть базу даних MIME, щоб ваші зміни набули чинності:</p>
        <screen><output># </output><input>update-mime-database /usr/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Від імені root, оновіть базу даних програм:</p>
        <screen><output># </output><input>update-desktop-database /usr/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Щоб перевірити, що ви успішно пов'язали файли <file>*.xyz</file> із типом MIME <sys>application/x-newtype</sys>, спочатку створіть порожній файл, наприклад <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Далі віддайте команду <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Щоб перевірити, що <file>myapplication1.desktop</file> було успішно встановлено як типову зареєстровану програму для типу MIME <sys>application/x-newtype</sys>, віддайте команду <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
