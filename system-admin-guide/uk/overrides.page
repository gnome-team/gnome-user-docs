<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overrides" xml:lang="uk">

  <info>
    <link type="guide" xref="setup"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>Перевизначення GSettings використовують у дистрибутивах для коригування типових параметрів.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Чому не слід використовувати перевизначення GSettings?</title>

  <p>Перевизначення GSettings використовуються дистрибутивами для коригування типових параметрів стільниці GNOME і програм. Перевизначення dconf було створено для системних адміністраторів з метою уможливлення коригування типових параметрів і встановлення обов'язкових параметрів для стільниці GNOME і програм.</p>

  <section id="what-are-vendor-overrides">
  <title>Для чого призначено перевизначення виробника?</title>

   <p>Типові значення визначаються у схемах, які встановлюються програмою. Іноді у виробника або поширювача виникає потреба у коригуванні цих типових значень.</p>

   <p>Оскільки латання початкових кодів XML для схеми є незручним і ризикованим, <link its:translate="no" href="man:glib-compile-schemas">
   <sys>glib-compile-schemas</sys></link> читає так звані файли <em>перевизначень виробника</em>. Такі файли є файлами ключів, які зберігаються в одному каталозі із початковими файлами схем у форматі XML і можуть перевизначати типові значення.</p>

  </section>

</page>
