<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-layout" xml:lang="ko">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>로그인 화면에서 키보드 배치 선택을 표시합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>로그인 화면에 다중 키보드 배치 표시</title>

  <p>로그인 화면에서 사용자가 선택할 다른 키보드 배치를 시스템 키보드 배치 설정에 추가하여 바꿀 수 있습니다. 로그인 화면에 나타난 키보드 배치가 아닌 다른 키보드 배치를 사용하려는 사용자에게 도움이 될 수 있습니다.</p>

  <steps>
    <title>시스템 키보드 배치 설정 바꾸기</title>
    <item>
      <p><file>/usr/share/X11/xkb/rules/base.lst</file> 파일의 <sys>! layout</sys> 섹션에서 원하는 언어 배치 코드를 찾으십시오.</p>
    </item>
    <item>
      <p>다음과 같이 <cmd>localectl</cmd> 도구로 시스템 키보드 배치 설정을 바꿀 수 있습니다:</p>
      <screen><cmd>localectl set-x11-keymap <var>layout</var></cmd></screen>
      <p>쉼표 구분 목록으로 다중 배치를 지정할 수 있습니다. 예를 들어 <sys>ko</sys>를 기본 배치로 설정하고 <sys>us</sys>를 두번째 배치로 설정하려면 다음 명령을 실행하십시오:</p>
      <screen><output>$ </output><input>localectl set-x11-keymap kr,us</input></screen>
    </item>
    <item>
      <p>지정 배치를 로그인 상단 표시줄에서 볼 수 있는지 확인하려면 로그아웃하십시오.</p>
    </item>
  </steps>
  <p>참고로 <cmd>localectl</cmd> 명령으로 머신 전체에서 사용할 기본 키보드 모델, 변종, 옵션을 지정할 수 있습니다. 자세한 정보는 <cmd>localectl</cmd>(1) 맨 페이지를 보십시오.</p>

  <section id="keyboard-layout-no-localectl">
  <title>localectl 명령을 사용하지 않고 다중 키보드 배치 표시</title>

  <p><cmd>localectl</cmd> 도구를 제공하지 않는 시스템에서는 <file>/usr/share/X11/xorg.conf.d/</file>의 설정 파일을 편집하여 시스템 키보드 배치 설정을 바꿀 수 있습니다.</p>

  <steps>
    <title>시스템 키보드 배치 설정 바꾸기</title>
    <item>
      <p><file>/usr/share/X11/xkb/rules/base.lst</file> 파일의 <sys>! layout</sys> 섹션에서 원하는 언어 배치 코드를 찾으십시오.</p>
    </item>
    <item>
      <p>다음과 같은 방법으로 <file>/usr/share/X11/xorg.conf.d/10-evdev.conf</file> 파일에 배치 코드를 추가하십시오:</p>
<screen>
Section "InputClass"
  Identifier "evdev keyboard catchall"
  MatchIsKeyboard "on"
  MatchDevicePath "/dev/input/event*"
  Driver "evdev"
  <input>Option "XkbLayout" "kr,en"</input>
EndSection
</screen>
      <p>예제의 국문(<sys>kr</sys>), 영문(<sys>en</sys>) 배치처럼, 쉼표 구분 목록 방식으로 다중 배치 설정을 추가할 수 있습니다.</p>
    </item>
    <item>
      <p>지정 배치를 로그인 상단 표시줄에서 볼 수 있는지 확인하려면 로그아웃하십시오.</p>
    </item>
  </steps>

  </section>

</page>
