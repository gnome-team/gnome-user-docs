<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-printing" xml:lang="ko">
     
  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-12-04" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>사용자의 문서 인쇄를 막습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>인쇄 기능 끄기</title>

  <p>사용자가 인쇄 대화상자를 보지 못하게 할 수 있습니다. 임시로 접근한 사용자가 네트워크 프린터로 인쇄하지 못하도록 할 경우에 이런 조치가 유용합니다.</p>

  <note style="warning">
    <p>이 기능은 이 기능을 지원하는 프로그램에서만 동작합니다! 모든 그놈 프로그램과 제3자 프로그램에서 이 기능을 사용할 수 있는건 아닙니다. 이 설정을 바꾸면 이 기능을 지원하지 않는 프로그램에 영향을 미치지 않습니다.</p>
  </note>

  <steps>
    <title>인쇄 기능 끄기</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p><sys>local</sys> 데이터베이스에 정보를 제공할 <file>/etc/dconf/db/local.d/00-printing</file> 키 파일을 만드십시오.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-printing</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/lockdown]
 
# Prevent applications from printing
disable-printing=true
</code>
     </listing>
    </item>
    <item>
      <p>이 설정을 사용자가 우선 적용하지 못하게 하려면, <file>/etc/dconf/db/local.d/locks/printing</file> 파일을 만들어 다음 내용을 넣으십시오:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/printing</file></title>
<code>
# Lock printing settings
/org/gnome/desktop/lockdown/disable-printing
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
