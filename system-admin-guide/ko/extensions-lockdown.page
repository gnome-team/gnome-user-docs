<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions-lockdown" xml:lang="ko">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="extensions-enable"/>
    <link type="seealso" xref="extensions"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>그놈 셸 활장을 사용자가 활성 비활성하지 못하게 합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>사용 확장 잠그기</title>
  
  <p>그놈 셸에서는 <code>org.gnome.shell.enabled-extensions</code>과 <code>org.gnome.shell.development-tools</code> 키를 잠궈 확장 활성/비활성을 사용자가 못하게 할 수 있습니다. 이렇게 하면 사용자가 사용할 확장 모음을 제공할 수 있습니다.</p>
   
  <p><code>org.gnome.shell.development-tools</code> 키를 잠그면 그놈 셸 통합 디버거와 검사 도구(<app>룩킹 글래스</app>)를 필수 확장 비활성에 사용할 수 없습니다.</p>
   
  <steps>
    <title>org.gnome.shell.enabled-extensions 키와 org.gnome.shell.development-tools 키 잠그기</title>
    <item>
      <p><file>/etc/dconf/profile/user</file>에 <code>user</code> 프로파일을 만드십시오:</p>
      <listing>
        <code>
user-db:user
system-db:local
</code>
      </listing>
    </item>
    <item>
      <p><file>/etc/dconf/db/local.d/00-extensions</file>에 머신 전체 설정을 넣을 <code>local</code> 데이터베이스를 만드십시오:</p>
      <listing>
        <code>
[org/gnome/shell]
# List all extensions that you want to have enabled for all users
enabled-extensions=['<input>myextension1@myname.example.com</input>', '<input>myextension2@myname.example.com</input>']
# Disable access to Looking Glass
development-tools=false
</code>
      </listing>
      <p><code>enabled-extensions</code>키는 확장의 UUID(<code>myextension1@myname.example.com</code>과 <code>myextension2@myname.example.com</code>)로 활성 확장을 정의합니다.</p>
      <p>룩킹 글래스 접근을 막으려면 <code>development-tools</code> 키를 거짓으로 설정하십시오.</p>
    </item>
    <item>
      <p>사용자 설정 대신 적용하고 <file>/etc/dconf/db/local.d/locks/extensions</file> 파일 설정을 사용자가 바꾸지 못하도록 막으려면:</p>
      <listing>
        <code>
# Lock the list of enabled extensions
/org/gnome/shell/enabled-extensions
/org/gnome/shell/disabled-extensions
/org/gnome/shell/development-tools
</code>
</listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>
  
  <p><code>org.gnome.shell.enabled-extensions</code>키와 <code>org.gnome.shell.development-tools</code>키를 잠그고 나면, <code>org.gnome.shell.enabled-extensions</code> 키에 나타나지 않는 <file>~/.local/share/gnome-shell/extensions</file>경로 또는 <file>/usr/share/gnome-shell/extensions</file> 경로에 설치한 어떤 확장이든 그놈 셸에서 불러오지 않아 사용자가 해당 확장을 사용하지 못하게 막을 수 있습니다.</p>

</page>
