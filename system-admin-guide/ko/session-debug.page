<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-debug" xml:lang="ko">

  <info>
    <link type="guide" xref="sundry#session"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc><file>~/.xsession-errors</file> 파일에 무슨 내용이 들어가죠?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>세션 문제 디버깅</title>

  <p>세션 문제에 대한 더 많은 내용을 찾아보고 문제를 해결하려면, 사용자 세션과 프로그램의 로그 데이터를 저장하는 시스템 로그를 살펴보십시오.</p>

  <p><file>~/.xsession-errors</file> X 세션 로그 파일 방식은 오래되어 더이상 사용하지 않습니다.</p>

<section id="session-log-systemd">
  <title>systemd 기반 시스템에서 세션 로그 보기</title>
  <p>systemd 기반 시스템에서는 <app>systemd</app> 저널에서 세션 로그 데이터를 살펴볼 수 있는데 이 데이터는 이진 형식으로 들어있습니다. 로그를 보려면 <cmd>journalctl</cmd> 명령을 활용하십시오.</p>

  <steps>
    <title>사용자 세션 로그를 보려면:</title>
    <item><p>다음 명령을 실행하면 사용자 ID(<sys>uid</sys>)를 확인합니다:</p>
    <screen><output>$ </output><input>id --user</input>
1000</screen></item>
    <item><p>위 명령에서 나온 ID를 가진 사용자의 저널 로그를 봅니다:</p>
    <screen><output>$ </output><input>journalctl _UID=1000</input></screen>
    </item>
  </steps>

  <p>systemd 저널에 대한 자세한 내용은 <link its:translate="no" href="man:journalctl"><cmd>journalctl</cmd>(1)</link> 맨 페이지를 살펴보십시오.</p>

</section>

</page>
