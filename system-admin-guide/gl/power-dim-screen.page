<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="power-dim-screen" xml:lang="gl">

  <info>
    <link type="guide" xref="user-settings"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.12" date="2014-06-20" status="candidate"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Make the screen dim after a specific amount of time when the user is
    idle.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2023</mal:years>
    </mal:credit>
  </info>

  <title>Dim screen when user is idle</title>

  <p>You can make the computer screen dim after the computer has been idle (not
  used) for some period of time.</p>

  <steps>
    <title>Escurecer pantalla cando o ordenador está inactivo</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Create the key file
      <file>/etc/dconf/db/local.d/00-power</file> to provide
      information for the <sys>local</sys> database.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-power</file></title>
<code>
# Specify the dconf path
[org/gnome/settings-daemon/plugins/power]

# Enable screen dimming
idle-dim=true

# Set brightness after dimming
idle-brightness=30
</code>
      </listing>
    </item>
    <item>
      <p>Create the key file
      <file>/etc/dconf/db/local.d/00-session</file> to provide
      information for the <sys>local</sys> database.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-session</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/session]

# The number of seconds of inactivity before the session is considered idle
idle-delay=uint32 300
</code>
      </listing>
      <p>You must include the <code>uint32</code> along with the
      integer key values as shown.</p>
    </item>
    <item>
      <p>To prevent the user from overriding these settings, create the file
      <file>/etc/dconf/db/local.d/locks/power-saving</file> with the following
      content:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/power-saving</file></title>
<code>
# Lock screen dimming and idle timeout
/org/gnome/settings-daemon/plugins/power/idle-dim
/org/gnome/settings-daemon/plugins/power/idle-brightness
/org/gnome/desktop/session/idle-delay
</code>
      </listing>
      <p>If you want to let the user change these settings, skip this step.</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
