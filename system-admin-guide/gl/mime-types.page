<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="mime-types" xml:lang="gl">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>MIME types are used to identify the format of a file.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2023</mal:years>
    </mal:credit>
  </info>

    <title>Que é un tipo MIME?</title>
    <p>
      In GNOME, MIME (<em>Multipurpose Internet Mail Extension</em>)
      types are used to identify the format of a file. The GNOME Desktop
      uses MIME types to:
    </p>
    <list>
      <item>
        <p>
          Determine which application should open a specific file format by
          default.
        </p>
      </item>
      <item>
        <p>
          Register other applications that can also open a specific file format.
        </p>
      </item>
      <item>
        <p>
          Provide a string describing the type of a file, for example,
          in a file properties dialog of the <app>Files</app>
          application.
        </p>
      </item>
      <item>
        <p>
          Provide an icon representing a specific file format, for
          example, in a file properties dialog of the <app>Files</app>
          application.
        </p>
      </item>
    </list>
    <p>Os nomes dos tipos MIME seguen os seguinte formato:</p>
<screen>
<var>tipo-media</var>/<var>identificador-subtipo</var>
</screen>
<p>
      <sys>image/jpeg</sys> is an example of a MIME type where
      <sys>image</sys> is the media type, and <sys>jpeg</sys>
      is the subtype identifier.
</p>
    <p>GNOME segue a especificación <em>Información MIME compartida de freedesktop.org</em>:</p>
    <list>
    <item>
      <p>
        The machine-wide and user-specific location to store all MIME type
        specification files.
      </p>
    </item>
    <item>
      <p>Cómo rexistrar un tipo MIME para que ambiente de escritorio saiba que aplicacións poden usarse para abrir un formato de ficheiro específico.</p>
    </item>
    <item>
      <p>
        How the user can change which applications should open what file formats.
      </p>
    </item>
    </list>
    <section id="mime-database">
      <title>Que é a base de datos MIME?</title>
      <p>
        The MIME database is a collection of all MIME type specification files
        that GNOME uses to store information about known MIME types.
      </p>
      <p>
        The most important part of the MIME database from the system administrator’s
        point of view is the <file>/usr/share/mime/packages/</file>
        directory where the MIME type related files specifying information on
        known MIME types are stored. One example of such a file is
        <file>/usr/share/mime/packages/freedesktop.org.xml</file>, specifying
        information about the standard MIME types available on the system by
        default. That file is provided by the <sys>shared-mime-info</sys>
        package.
      </p>
    </section>
    <section id="mime-types-more-information">
    <title>Obter máis información</title>
    <p>
      For detailed information describing the MIME type system, see the
      <em>freedesktop.org Shared MIME Info specification</em> located at the
      freedesktop.org website:
    </p>
    <list>
      <item>
        <p>
           <link href="http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/">
           http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/</link>
        </p>
      </item>
    </list>
    </section>
</page>
