<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-online-accounts" xml:lang="gl">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

   <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activar ou desactivar algunhas ou todas as contas en liña.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2023</mal:years>
    </mal:credit>
  </info>
  <title>Permitir ou desactivar contas de usuario</title>

  <p>The <app>GNOME Online Accounts</app> (GOA) are used for
  integrating personal network accounts with the GNOME Desktop and applications.
  The user can add their online accounts, such as Google, Facebook, Flickr,
  ownCloud, and others using the <app>Online Accounts</app> application.</p>

  <p>Como administrador do sistema, vostede pode:</p>
  <list>
    <item><p>activar todas as contas en liña;</p></item>
    <item><p>activar de forma selectiva unhas cantas contas en liña;</p></item>
    <item><p>desactivar todas as contas en liña.</p></item>
  </list>

<steps>
  <title>Configurar contas en liña</title>
  <item><p>Make sure that you have the <sys>gnome-online-accounts</sys> package
  installed on your system.</p>
  </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Create the key file <file>/etc/dconf/db/local.d/00-goa</file> to provide
  information for the <sys>local</sys> database containing the following
  configuration.</p>
   <list>
   <item><p>Para activar fornecedores específicos:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['google', 'facebook']
</code>
  </item>
   <item><p>Para desactivar todos os fornecedores:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['']
</code>
  </item>
  <item><p>Para permitir todos os fornecedores activos:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['all']
</code>
  <p>Esta é a configuración predeterminada.</p></item>
   </list>
  </item>
  <item>
    <p>To prevent the user from overriding these settings, create the file
    <file>/etc/dconf/db/local.d/locks/goa</file> with the following
    content:</p>
    <listing>
    <title><file>/etc/dconf/db/local.d/locks/goa</file></title>
<code>
# Lock the list of providers that are allowed to be loaded
/org/gnome/online-accounts/whitelisted-providers
</code>
    </listing>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
</steps>

</page>
