<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-command-line" xml:lang="hr">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="guide" xref="user-settings#lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sprječavanje korisnika u pristupu naredbenom retku.</desc>
  </info>

  <title>Onemogućavanje pristupa naredbenom retku</title>

  <p>Kako bi onemogućili pristup naredbenom retku za korisnika računala, morate učiniti promjene podešavanja u više različitih sadržaja. Zapamtite da sljedeći koraci ne uklanjaju dopuštenja korisnika računala za pristup naredbenom retku, već uklanjaju načine na koje korisnik računala može pristupiti naredbenom retku.</p>

  <list>
    <item>
      <p>Postavite <code>org.gnome.desktop.lockdown.disable-command-line</code> GSettings ključ, koji sprječava korisnika da pristupi terminalu ili odredi naredbeni redak koji će se izvršiti (<keyseq><key>Alt </key> <key>F2</key></keyseq> naredbeni redak).</p>
    </item>
    <item>
      <p>Spriječite korisnike da pristupe <keyseq><key>Alt</key><key>F2</key> </keyseq> naredbenom retku.</p>
    </item>
    <item>
      <p>Onemogućite prebacivanje na virtualne terminale (VT) pomoću <keyseq> <key>Ctrl</key><key>Alt</key><key><var>funkcijska tipka</var></key></keyseq> prečaca promjenom podešavanja X poslužitelja.</p>
    </item>
    <item>
      <p>Uklonite <app>Terminal</app> i sve ostale terminalske aplikacije iz <gui>Aktivnosti</gui> pregleda u GNOME ljusci. Još ćete morati spriječiti korisnika da instalira novu terminalsku aplikaciju.</p>
    </item>
  </list>

<section id="command-prompt">
  <title>Onemogućavanje naredbenog retka</title>

  <steps>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <item>
      <p>Stvorite <sys>lokalnu</sys> bazu podataka za postavke na cijelom sustavu u <file>/etc/dconf/db/local.d/00-lockdown</file>:</p>
      <code># Navedi dconf putanju
[org/gnome/desktop/lockdown]

# Onemogući naredbeni redak
disable-command-line=true</code>
    </item>
    <item>
      <p>Zaobiđite korisničku postavku i spriječite korisnika da je promijeni u <file>/etc/dconf/db/local.d/locks/lockdown</file>:</p>
      <code># Navedi ključeve koji se koriste za podešavanje zaključavanja
/org/gnome/desktop/lockdown/disable-command-line</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>
</section>

<section id="virtual-terminal">
  <title>Onemogućavanje prebacivanja na virtualni terminal</title>

  <p>Korisnici mogu uobičajeno koristiti <keyseq><key>Ctrl</key><key>Alt</key><key><var>funkcijske tipke</var></key></keyseq> prečace (primjerice, <keyseq><key>Ctrl</key><key>Alt</key><key>F2</key></keyseq>) za prebacivanje s GNOME radne površine na virtualni terminal.</p>

  <p>Ako računalo pokreće <em>X prozorski sustav</em>, možete onemogućiti pristup svim virtualnim terminalima dodavanjem mogućnosti <code>DontVTSwitch</code> u odjeljak <code>Serverflags</code> u X datoteku podešavanja u direktoriju <file>/etc/X11/xorg.conf.d/</file>.</p>

  <steps>
    <item>
      <p>Stvorite ili uredite X datoteku podešavanja u <file>/etc/X11/xorg.conf.d/</file>. Primjerice, <file>/etc/X11/xorg.conf.d/10-xorg.conf</file>:</p>
    <listing>
    <title><file>/etc/X11/xorg.conf.d/10-xorg.conf</file></title>
<code>Section "Serverflags"

Option "DontVTSwitch" "yes"

EndSection
</code>
    </listing>
    </item>
    <item>
      <p>Ponovno pokrenite X poslužitelja kako bi se promjene primijenile.</p>
    </item>
  </steps>

</section>

<!-- TODO: add section for removing applications from the Activities overview. -->
</page>
