<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="processes" xml:lang="hr">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="guide" xref="sundry#session"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <desc>Koji pokrenuti procesi se mogu očekivati u neprilagođenoj, standardnoj GNOME sesiji?</desc>
  </info>

  <title>Uobičajeni procesi</title>

  <p>U standardnoj <app>GNOME</app> sesiji, programi koji se nazivaju pozadinski programi ili usluge izvode se na sustavu kao pozadinski procesi. Trebali bi pronaći sljedeće pokrenute pozadinske programe po zadanim postavkama:</p>

   <terms>
     <item>
       <title>dbus-daemon</title>
       <p><app>dbus-daemon</app> omogućuje pozadinski program sabirnice poruka koji programi mogu koristiti za međusobnu razmjenu poruka. <app>dbus-daemon</app> implementiran je s D-Bus bibliotekom koja omogućuje komunikaciju jedan-na-jedan između bilo koje dvije aplikacije.</p>
       <p>Za opširnije informacije pogledajte <link href="man:dbus-daemon">dbus-daemon</link> stranicu priručnika.</p>
     </item>
     <item>
       <title>gnome-keyring-daemon</title>
       <p>Vjerodajnice poput korisničkog imena i lozinke za razne programe i web stranice pohranjuju se na siguran način pomoću <app>gnome-keyring-daemon</app>. Te se informacije zapisuju u šifriranu datoteku koja se naziva datoteka skupa ključeva a sprema se u korisnikov osobni direktorij.</p>
       <p>Za opširnije informacije pogledajte <link its:translate="no" href="man:gnome-keyring-daemon">gnome-keyring-daemon</link> stranicu priručnika.</p>
     </item>
     <item>
       <title>gnome-session</title>
       <p><app>gnome-session</app> program odgovoran je za pokretanje GNOME radnog okruženja uz pomoć upravitelja zaslona kao što je <app>GDM</app>, <app>LightDM</app> ili <app> NODM</app>. Zadanu sesiju za korisnika postavlja administrator sustava u trenutku instalacije sustava. <app>gnome-session</app> uobičajeno učitava posljednju sesiju koja se uspješno pokrenula na sustavu.</p>
       <p>Za opširnije informacije pogledajte <link its:translate="no" href="man:gnome-session">gnome-session</link> stranicu priručnika.</p>
     </item>
     <item>
       <title>gnome-settings-daemon</title>
       <p><app>gnome-settings-daemon</app> upravlja postavkama za GNOME sesiju i za sve programe koji se pokreću unutar sesije.</p>
       <p>Za opširnije informacije pogledajte <link its:translate="no" href="man:gnome-settings-daemon">gnome-settings-daemon</link> stranicu priručnika.</p>
     </item>
     <item>
       <title>gnome-shell</title>
       <p><app>gnome-shell</app> pruža osnovne funkcije korisničkog sučelja za GNOME poput pokretanja programa, pregledavanja direktorija, pregledavanja datoteka itd..</p>
       <p>Za opširnije informacije pogledajte <link its:translate="no" href="man:gnome-shell">gnome-shell</link> stranicu priručnika.</p>
     </item>
     <item>
       <title>pulseaudio</title>
       <p><app>PulseAudio</app> je zvučni poslužitelj za Linux, POSIX i Windows sustave koji programima omogućuje izlaz zvuka putem <app>Pulseaudio</app> pozadinskog programa.</p>
       <p>Za opširnije informacije pogledajte <link its:translate="no" href="man:pulseaudio">pulseaudio</link> stranicu priručnika.</p>
     </item>
   </terms>

  <p>Ovisno o korisničkim postavkama, između ostalog možete vidjeti i nešto od sljedećeg:</p>
   <list ui:expanded="false">
   <title>Dodatni procesi</title>
     <item><p><app>at-spi2-dbus-launcher</app></p></item>
     <item><p><app>at-spi2-registryd</app></p></item>
     <item><p><app>gnome-screensaver</app></p></item>
     <item><p><app>gnome-shell-calendar-server</app></p></item>
     <item><p><app>goa-daemon</app></p></item>
     <item><p><app>gsd-printer</app></p></item>
     <item><p>razni <app>Evolution</app> procesi</p></item>
     <item><p>razni <app>GVFS</app> procesi</p></item>
   </list>

</page>
