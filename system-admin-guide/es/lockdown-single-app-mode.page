<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-single-app-mode" xml:lang="es">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="guide" xref="sundry#session"/>
    <link type="seealso" xref="lockdown-printing"/>
    <link type="seealso" xref="lockdown-file-saving"/>
    <link type="seealso" xref="lockdown-repartitioning"/>
    <link type="seealso" xref="lockdown-command-line"/>
    <link type="seealso" xref="login-automatic"/>
    <link type="seealso" xref="session-custom"/>
    <link type="seealso" xref="session-user"/>

    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>mclasen@redhat.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
        <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>
    <credit type="author">
      <name>Marek Suchánek</name>
      <email>msuchane@redhat.com</email>
      <years>2023</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
       
    <desc>Configurar un sistema de una aplicación única tipo kiosco.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Configurar el modo de aplicación única.</title>

  <p>El modo de aplicación única es una interfaz de GNOME modificada que configura la Shell como un kiosco interactivo. El administrador bloquea algunos de los comportamientos estándar para hacer el escritorio más restrictivo para los usuarios, permitiéndoles concentrarse en las funciones seleccionadas.</p>

  <p>Configure el modo de aplicación única para un amplio rango de funciones en varios campos (desde comunicación a entretenimiento o educación) y úselo como máquina de autoservicio, administrador de eventos, punto de registro y otros.</p>

  <note style="tip">
    <p>Your distribution might provide the
    <link href="https://gitlab.gnome.org/GNOME/gnome-kiosk">GNOME Kiosk</link>
    session, which is usually available in the gnome-kiosk package. You can use
    it to configure single-application mode more easily. Otherwise,
    use the following procedure.</p>
  </note>

  <steps>
  <title>Configurar el modo de aplicación única</title>
    <item>
      <p>Bloquear ajustes para evitar impresión, acceso al terminal y otros.</p>
      <list type="disc">
        <item><p><link xref="lockdown-command-line"/></p></item>
        <item><p><link xref="lockdown-printing"/></p></item>
        <item><p><link xref="lockdown-file-saving"/></p></item>
        <item><p><link xref="lockdown-repartitioning"/></p></item>
      </list>
    </item>
    <item>
      <p>Configurar el inicio de sesión automático para el usuario en el archivo <file>/etc/gdm/custom.conf</file>.</p>
      <p>Consulte la sección <link xref="login-automatic"/> para más información.</p>
    </item>
    <item>
      <p>Create the following files with the listed content:</p>
      <terms>
        <item>
          <title>
            <file>/usr/share/applications/org.gnome.Kiosk.Script.desktop</file>
          </title>
<code>
[Desktop Entry]
Name=Kiosk
Type=Application
Exec=gnome-kiosk
</code>
        </item>
        <item>
          <title>
            <file>/usr/share/applications/org.gnome.Kiosk.WindowManager.desktop</file>
          </title>
<code>
[Desktop Entry]
Type=Application
Name=Mutter
Comment=Window manager
Exec=/usr/bin/mutter
Categories=GNOME;GTK;Core;
OnlyShowIn=GNOME;
NoDisplay=true
X-GNOME-Autostart-Phase=DisplayServer
X-GNOME-Provides=windowmanager;
X-GNOME-Autostart-Notify=true
X-GNOME-AutoRestart=false
X-GNOME-HiddenUnderSystemd=true
</code>
        </item>
        <item>
          <title>
            <file>/usr/share/gnome-session/sessions/gnome-kiosk.session</file>
          </title>
<code>
[GNOME Session]
Name=Kiosk
RequiredComponents=org.gnome.Kiosk.WindowManager;org.gnome.Kiosk.Script;
</code>
        </item>
        <item>
          <title>
            <file>/usr/share/X11/xorg.conf.d/20-gnome-kiosk.conf</file>
          </title>
<code>
Section "ServerFlags"
    Option "DontVTSwitch" "on"
EndSection
</code>
        </item>
        <item>
          <title>
            <file>/usr/share/xsessions/org.gnome.Kiosk.desktop</file>
          </title>
<code>
[Desktop Entry]
Name=Kiosk
Comment=Kiosk mode
Exec=/usr/bin/gnome-session --session=gnome-kiosk
DesktopNames=GNOME-Kiosk;GNOME;
</code>
        </item>
      </terms>
    </item>
    <item>
      <p>As the user that will open the single-application session,
      create the <file>/home/<var>user</var>/.local/bin/gnome-kiosk</file> file:</p>
<code>
[<var>user</var>]$ mkdir -p ~/.local/bin

[<var>user</var>]$ touch ~/.local/bin/gnome-kiosk
</code>
    </item>
    <item>
      <p>Edit the <file>/home/<var>user</var>/.local/bin/gnome-kiosk</file> file and enter
      the executable name of the application that you want to launch in single-application mode.</p>
      <p>For example, to launch the Firefox browser in single-application mode,
      enter the following content:</p>
<code>
#!/bin/sh

while true; do
    firefox --kiosk https://example.org
done
</code>
      <p>The <code>while true</code> loop ensures that the application restarts
      if it terminates for any reason.</p>
    </item>
    <item>
      <p>Make the file executable:</p>
<code>
[<var>user</var>]$ chmod +x ~/.local/bin/gnome-kiosk
</code>
    </item>
    <item>
      <p>If you created the file or its containing directories as a different
      user than the single-application user, such as root, ensure that the file
      has the correct permissions:</p>
<code>
# chown -R <var>user</var>:<var>group</var> ~<var>user</var>/.local
</code>
    </item>
    <item>
      <p>At the GNOME login screen, select the Kiosk session from the gear
      button menu and log in as the single-application user.</p>
    </item>
  </steps>

</page>
