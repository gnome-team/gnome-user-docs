<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-debug" xml:lang="es">

  <info>
    <link type="guide" xref="sundry#session"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>¿Qué ha pasado con el archivo <file>~/.xsession-errors</file>?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Depurar problemas de sesión</title>

  <p>Si quiere encontrar más información acerca de un problema en una sesión o quiere arreglarla, consulte el registro del sistema, que contiene información de registro de su sesión de usuario y aplicaciones.</p>

  <p>El archivo de registro de sesión de X <file>~/.xsession-errors</file> se ha marcado como obsoleto y no se está utilizando.</p>

<section id="session-log-systemd">
  <title>Ver los registros de sesión en sistemas basados en systemd</title>
  <p>En sistemas basados en systemd puede encontrar los datos del registro de la sesión en el registro de <app>systemd</app>, que almacena los datos en un formato binario. Para ver los registros, use el comando <cmd>journalctl</cmd>.</p>

  <steps>
    <title>Para ver los registros de su sesión de usuario:</title>
    <item><p>Determine su identificador de usuario (<sys>uid</sys>) ejecutando el siguiente comando:</p>
    <screen><output>$ </output><input>id --user</input>
1000</screen></item>
    <item><p>Ver los mensajes del registro para el identificador de usuario especificado arriba:</p>
    <screen><output>$ </output><input>journalctl _UID=1000</input></screen>
    </item>
  </steps>

  <p>Para obtener más información sobre el registro de systemd consulte la página del manual <link its:translate="no" href="man:journalctl"><cmd>journalctl</cmd>(1)</link>.</p>

</section>

</page>
