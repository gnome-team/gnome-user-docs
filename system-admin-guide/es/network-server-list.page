<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-server-list" xml:lang="es">

  <info>
    <link type="guide" xref="network"/>
    <revision pkgversion="3.8" date="2013-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
   <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
   <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@gvolny.cz</email>
      <years>2013</years>
    </credit>

    <desc>¿Como puedo hacer más fácil el acceso de distintos usuarios a distintos archivos compartidos?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Configurar una lista predeterminada de servidores</title>

  <p><app>Nautilus</app> (la aplicación <app>Archivos</app>) guarda una lista de los servidores de compartición de archivos en el archivo <file>~/.config/gtk-3.0/servers</file> en el formato XBEL. Añada la lista de servidores de compartición de archivos a ese archivo para hacer que los archivos compartidos sean fácilmente accesibles a su usuario.</p>
  
  <note style="tip">
    <p><em>XBEL</em> (<em>XML Bookmark Exchange Language</em> o <em>Lenguaje XML de intercambio de marcadores</em>) es un estándar XML que permite compartir URI (Uniform Resource Identifiers o Identificadores de recursos uniformes). En GNOME, XBEL se utiliza para compartir marcadores de escritorio en aplicaciones como <app>Nautilus</app>.</p>
  </note>

  <example>
    <p>Un ejemplo del archivo <file>~~/.config/gtk-3.0/servers</file>:</p>
    <code>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xbel version="1.0"
      xmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks"
      xmlns:mime="http://www.freedesktop.org/standards/shared-mime-info"&gt;
   &lt;bookmark href="<input>ftp://ftp.gnome.org/</input>"&gt;
      &lt;title&gt;<input>GNOME FTP</input>&lt;/title&gt;
   &lt;/bookmark&gt;
&lt;/xbel&gt;
</code>
    <p>En el ejemplo de arriba, <app>Nautilus</app> crea un marcador llamado <em>GNOME FTP</em> con el URI <code>ftp://ftp.gnome.org/</code>.</p>
  </example>

</page>
