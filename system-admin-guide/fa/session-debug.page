<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-debug" xml:lang="fa">

  <info>
    <link type="guide" xref="sundry#session"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>۲۰۱۲</years>
    </credit>
    <credit type="editor">
      <name>یانا سواروا</name>
      <email>jana.svarova@gmail.com</email>
      <years>۲۰۱۳</years>
    </credit>
    <credit type="editor">
      <name>پیتر کوار</name>
      <email>pknbe@volny.cz</email>
      <years>۲۰۱۴</years>
    </credit>

    <desc>What happened to <file>~/.xsession-errors</file>?</desc>
  </info>

  <title>اشکال‌زدایی مشکلات نشست</title>

  <p>If you want to find more information about a problem in a session or want
  to fix it, consult the system log, which stores log data for your user session
  and applications.</p>

  <p>The <file>~/.xsession-errors</file> X session log file has been deprecated
  and is no longer used.</p>

<section id="session-log-systemd">
  <title>دیدن گزارش نشست روی سامانه‌های مبتنی بر سیستم‌دی</title>
  <p>On systemd-based systems, you can find the session log data in the
  <app>systemd</app> journal, which stores the data in a binary format. To view
  the logs, use the <cmd>journalctl</cmd> command.</p>

  <steps>
    <title>برای دیدن گزارش‌های نشست کاربرتان:</title>
    <item><p>Determine your user ID (<sys>uid</sys>) by running the following
    command: </p>
    <screen><output>$ </output><input>id --user</input>
1000</screen></item>
    <item><p>View the journal logs for the user ID determined above:</p>
    <screen><output>$ </output><input>journalctl _UID=1000</input></screen>
    </item>
  </steps>

  <p>For more information on the systemd journal, see the
  <link its:translate="no" href="man:journalctl"><cmd>journalctl</cmd>(1)</link>
  man page.</p>

</section>

</page>
