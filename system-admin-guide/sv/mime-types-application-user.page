<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application-user" xml:lang="sv">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Anpassa för varje användare vilket program som öppnar en specifik MIME-typ.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2023</mal:years>
    </mal:credit>
  </info>

    <title>Åsidosätta registrerade standardprogram för enskilda användare</title>

    <p>Filerna <file>/usr/share/applications/mimeapps.list</file> och <file>/usr/share/applications/gnome-mimeapps.list</file> anger vilket program som är registrerat för att öppna specifika MIME-typer som standard. Dessa filer tillhandahålls av distributionen.</p>
    <p>För att åsidosätta systemets standardvärden för enskilda användare behöver du skapa en fil <file>~/.config/mimeapps.list</file> med en lista över MIME-typer för vilka du vill åsidosätta det registrerade standardprogrammet.</p>
    <steps>
      <title>Åsidosätta registrerade standardprogram för enskilda användare</title>
      <item>
        <p>Titta i <file>/usr/share/applications/mimeapps.list</file> för att avgöra för vilka MIME-typer som du vill ändra det registrerade standardprogrammet. Till exempel anger följande exempel på filen <file>mimeapps.list</file> det registrerade standardprogrammet för MIME-typerna <code>text/html</code> och <code>application/xhtml+xml</code>:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>Standardprogrammet (<app>Epiphany</app>) definieras genom att ange dess motsvarande <file>.desktop</file>-fil (<file>epiphany.desktop</file>). Systemets standardplats för andra programs <file>.desktop</file>-filer är <file>/usr/share/applications/</file>. Enskilda användares <file>.desktop</file>-filer kan lagras i <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Skapa filen <file>~/.config/mimeapps.list</file>. Ange i filen MIME-typerna och deras motsvarande registrerade standardprogram:</p>
        <code>[Default Applications]
text/html=<var>mittprogram1.desktop</var>
application/xhtml+xml=<var>mittprogram2.desktop</var>

[Added Associations]
text/html=<var>mittprogram1.desktop</var>;
application/xhtml+xml=<var>mittprogram2.desktop</var>;</code>
      <p>Detta ställer in det registrerade standardprogrammet för MIME-typen <code>text/html</code> till <code>mittprogram1.desktop</code>, och det registrerade standardprogrammet för MIME-typen <code>application/xhtml+xml</code> till <code>mittprogram2.desktop</code>.</p>
        <p>För att dessa inställningar ska fungera korrekt, säkerställ att både filen <file>mittprogram1.desktop</file> och <file>mittprogram2.desktop</file> är placerade i katalogen <file>/usr/share/applications/</file>. Enskilda användares <file>.desktop</file>-filer kan lagras i <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Du kan använda kommandot <cmd>gio mime</cmd> för att bekräfta att det registrerade standardprogrammet ställts in korrekt:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Standardprogram för ”text/html”: mittprogram1.desktop
Registrerade program:
	mittprogram1.desktop
	epiphany.desktop
Rekommenderade program:
	mittprogram1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
