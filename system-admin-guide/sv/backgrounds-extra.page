<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backgrounds-extra" xml:lang="sv">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hur gör jag extra bakgrunder tillgängliga för mina användare?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Lägga till extra bakgrunder</title>

  <p>Du kan göra extra bakgrunder tillgängliga för användare på ditt system genom att följa stegen nedan.</p>

  <steps>
  <title>Ställ in extra bakgrunder</title>
  <item>
  <p>Skapa en XML-fil, till exempel <file><var>filnamn</var>.xml</file>. Använd i denna fil nycklar från GSettings-schemat <sys>org.gnome.desktop.background</sys> för att ange extra bakgrunder och deras utseende.</p>

  <p>Nedan är en lista över de oftast använda nycklarna:</p>

  <table frame="top bottom" rules="all" shade="rows">
    <title>GSettings-nycklar för org.gnome.desktop.background-scheman</title>
    <tbody>
    <tr>
      <td><p>Nyckelnamn</p></td>
      <td><p>Möjliga värden</p></td>
      <td><p>Beskrivning</p></td>
    </tr>
    <tr>
      <td><p>picture-options</p></td>
      <td><p>”none”, ”wallpaper”, ”centered”, ”scaled”, ”stretched”, ”zoom”, ”spanned”</p></td>
      <td><p>Avgör hur bilden som anges av <var>wallpaper_filename</var> renderas.</p></td>
    </tr>
    <tr>
      <td><p>color-shading-type</p></td>
      <td><p>”horizontal”, ”vertical” och ”solid”</p></td>
      <td><p>Hur bakgrundsfärgen ska skuggas.</p></td>
    </tr>
    <tr>
      <td><p>primary-color</p></td>
      <td><p>standard: #023c88</p></td>
      <td><p>Vänster- eller toppfärg när toningar ritas eller den enda färgen vid enfärgat.</p></td>
    </tr>
    <tr>
      <td><p>secondary-color</p></td>
      <td><p>standard: #5789ca</p></td>
      <td><p>Höger- eller bottenfärg när toningar ritas, används inte för enfärgat.</p></td>
    </tr>
  </tbody>
  </table>

  <p>Du kan se en fullständig lista över <sys>org.gnome.desktop.background</sys>-nycklar och möjliga värden med <app>dconf-editor</app> eller kommandoradsverktyget <cmd>gsettings</cmd>. Se <link xref="gsettings-browse"/> för mer information.</p>
  <p>Nedan är en exempelfil <file><var>filnamn</var>.xml</file>:</p>

<code mime="application/xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd"&gt;
&lt;wallpapers&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Company Background&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/company-wallpaper.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ffffff&lt;/pcolor&gt;
    &lt;scolor&gt;#000000&lt;/scolor&gt;
  &lt;/wallpaper&gt;
&lt;/wallpapers&gt;

</code>

  </item>
  <item>
  <p>Placera filen <file><var>filnamn</var>.xml</file> i katalogen <file>/usr/share/gnome-background-properties/</file>.</p>
  <p>Användare kommer ha de extra bakgrunderna tillgängliga för konfiguration från <guiseq><gui>Inställningar</gui> <gui>Bakgrund</gui></guiseq>.</p>
  </item>
  </steps>


  <section id="backgrounds-extra-two-wallpapers">
  <title>Ange flera bakgrunder</title>
  <p>Du kan ange flera <code>&lt;wallpaper&gt;</code>-element i en konfigurationsfil för att lägga till fler bakgrunder.</p>
  <p>Se följande exempel med två <code>&lt;wallpaper&gt;</code>-element, vilket lägger till två olika bakgrunder:</p>

<code mime="application/xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd"&gt;
&lt;wallpapers&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Company Background&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/company-wallpaper.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ffffff&lt;/pcolor&gt;
    &lt;scolor&gt;#000000&lt;/scolor&gt;
  &lt;/wallpaper&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Company Background 2&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund 2&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/company-wallpaper-2.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ff0000&lt;/pcolor&gt;
    &lt;scolor&gt;#00ffff&lt;/scolor&gt;
  &lt;/wallpaper&gt;
&lt;/wallpapers&gt;
</code>

</section>
</page>
