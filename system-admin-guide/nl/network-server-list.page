<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-server-list" xml:lang="nl">

  <info>
    <link type="guide" xref="network"/>
    <revision pkgversion="3.8" date="2013-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
   <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
   <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@gvolny.cz</email>
      <years>2013</years>
    </credit>

    <desc>How do I make it easy for different users to access different file
    shares?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Een standaardserverlijst instellen</title>

  <p><app>Nautilus</app> (the <app>Files</app> application) stores a list of
  file-sharing servers in the file <file>~/.config/gtk-3.0/servers</file> in
  the XBEL format. Add the list of file-sharing servers to that file to make
  file shares easily accessible to your user.</p>
  
  <note style="tip">
    <p><em>XBEL</em> (<em>XML Bookmark Exchange Language</em>) is an XML
    standard that lets you share URIs (Uniform Resource Identifiers). In GNOME,
    XBEL is used to share desktop bookmarks in applications such as
    <app>Nautilus</app>.</p>
  </note>

  <example>
    <p>Een voorbeeld van het bestand <file>~/.config/gtk-3.0/servers</file>:</p>
    <code>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xbel version="1.0"
      xmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks"
      xmlns:mime="http://www.freedesktop.org/standards/shared-mime-info"&gt;
   &lt;bookmark href="<input>ftp://ftp.gnome.org/</input>"&gt;
      &lt;title&gt;<input>GNOME FTP</input>&lt;/title&gt;
   &lt;/bookmark&gt;
&lt;/xbel&gt;
</code>
    <p>In the example above, <app>Nautilus</app> creates a bookmark titled
    <em>GNOME FTP</em> with the URI <code>ftp://ftp.gnome.org/</code>.</p>
  </example>

</page>
