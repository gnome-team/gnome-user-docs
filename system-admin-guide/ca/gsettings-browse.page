<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="gsettings-browse" xml:lang="ca">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="overrides"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Quina eina puc utilitzar per explorar la configuració del sistema i de les aplicacions?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Navegar pels valors GSettings de les vostres aplicacions</title>

  <p>Hi ha dues eines que podeu utilitzar per examinar les preferències del sistema i de l'aplicació emmagatzemades com a valors de GSettings, la utilitat gràfica <app>dconf-editor</app> i la utilitat de la línia d'ordres <cmd>gsettings</cmd>.</p>

  <p>Ambdues <app>dconf-editor</app> i <cmd>gsettings</cmd> us permeten canviar les preferències per l'usuari actual.</p>

  <note style="warning"><p>Tingueu en compte que aquestes eines funcionen sempre mitjançant la base de dades GSettings de l'usuari actual, per la qual cosa no heu d'executar aquestes aplicacions com a usuari arrel.</p>
  
  <p>Ambdues <app>dconf-editor</app> i <cmd>gsettings</cmd> requereixen una sessió D-Bus per a aplicar qualsevol canvi. Aquest és el motiu pel qual el dimoni <sys>dconf</sys> cal que sigui activat utilitzant D-Bus.</p>

  <p>Podeu obtenir el bus de sessió necessari executant <cmd> gsettings </cmd> a la utilitat <sys>dbus-launch</sys>, com aquí:</p>

  <screen><output>$ </output><input>dbus-launch gsettings set org.gnome.desktop.background draw-background true</input></screen>
  </note>

  <p>El <app>dconf-editor</app> pot ser millor utilitzar-lo si no esteu familiaritzats amb les opcions disponibles en una aplicació. Mostra la jerarquia de la configuració en una vista en arbre i també mostra informació addicional sobre cada configuració, incloent-hi la descripció, el tipus i el valor predeterminat.</p>

  <p><cmd>gsettings</cmd> és més poderós que <app>dconf-editor</app>. La compleció bash es proporciona per a <cmd>gsettings</cmd>, i podeu escriure guions que incloguin ordres de <cmd>gsettings</cmd> per configuració automàtica.</p>

  <p>Per a una llista completa d'opcions de <cmd>gsettings</cmd>, mireu la pàgina man <link its:translate="no" href="man:gsettings"><cmd>gsettings</cmd>(1)</link> .</p>

</page>
