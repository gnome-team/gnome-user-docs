<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="fonts" xml:lang="ca">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="fonts-user"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Afegir fonts extra per a tots els usuaris.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Afegir una font extra per a tots els usuaris</title>

  <p>Es pot instal·lar una font extra que estarà disponible per als usuaris a les aplicacions que utilitzin <sys>fontconfig</sys> per la gestió de les fonts.</p>

  <steps>
    <title>Instal·lar una font extra</title>
    <item>
      <p>Copiar la font al directori <file>/usr/local/share/fonts/</file> per instal·lar-la.</p>
    </item>
    <item>
      <p>Pot ser necessari córrer la comanda següent per a actualitzar la memòria cau de fonts:</p>
      <screen><output>$ </output><input>fc-cache /usr/local/share/fonts/</input></screen>
    </item>
  </steps>

  <p>Pot ser necessari reiniciar les aplicacions que estiguin funcionant per a veure els canvis. Les sessions d'usuari no s'han de reiniciar.</p>
  
  <p>Opcionalment, podeu instal·lar fonts a un directori de sistema diferent al <file>/usr/local/share/fonts/</file> si es llista al fitxer <file>/etc/fonts/fonts.conf</file>. En cas contrari, haureu de crear el vostre fitxer de configuració de l'equip a <file>/etc/fonts/local.conf</file> contenint el directori que vulgueu utilitzar Consulteu la pàgina del manual <cmd>fonts-conf</cmd>(5) per a més informació.</p>
  <p>Si esteu utilitzant una carpeta alternativa, recordeu especificar el nom del directori quan actualitzeu la memòria cau de fonts amb la comanda <cmd>fc-cache</cmd>:</p>
  <screen><output>$ </output><input>fc-cache <var>directory_name</var></input></screen>

</page>
