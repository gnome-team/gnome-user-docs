<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-printing" xml:lang="ca">
     
  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-12-04" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Evitar que l'usuari pugui imprimir documents.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar la impressió</title>

  <p>Podeu desactivar el quadre de diàleg d'impressió per als usuaris. Això pot ser útil si esteu donant accés temporal a un usuari o no voleu que l'usuari imprimeixi a les impressores de la xarxa.</p>

  <note style="warning">
    <p>Aquesta funció només funciona en aplicacions que ho suportin! No totes les aplicacions de GNOME i de tercers tenen aquesta funció activada. Aquests canvis no tindran efecte a les aplicacions que no admeten aquesta funció.</p>
  </note>

  <steps>
    <title>Desactivar la impressió</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Crear el fitxer de claus <file>/etc/dconf/db/local.d/00-printing</file> per a proporcionar informació a la base de dades <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-printing</file></title>
<code>
# Especificar el camí a dconf
[org/gnome/desktop/lockdown]
 
# Evita que les aplicacions puguin imprimir
disable-printing=true
</code>
     </listing>
    </item>
    <item>
      <p>Per a evitar que l'usuari pugui sobreescriure aquesta configuració, creeu el fitxer <file>/etc/dconf/db/local.d/locks/printing</file> amb el següent contingut:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/printing</file></title>
<code>
# Bloquejar la configuració d'impressió
/org/gnome/desktop/lockdown/disable-printing
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
