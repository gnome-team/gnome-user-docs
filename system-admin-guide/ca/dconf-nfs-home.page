<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-nfs-home" xml:lang="ca">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <revision version="0.1" date="2013-03-19" status="draft"/>
    <revision pkgversion="3.8" date="2013-05-09" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Actualitza la configuració del sistema amb <sys>dconf</sys> per a emmagatzemar el directori principal <sys>NFS</sys>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Emmagatzemar la configuració sobre <sys>NFS</sys></title>

  <p>Per a què <sys its:translate="no">dconf</sys> funcioni correctament quan s'utilitzen directoris d'usuari <sys>Network File System</sys> (<sys>NFS</sys>), s'ha d'utilitzar <sys its:translate="no">dconf</sys> <em>un fitxer de claus</em>.</p>

  <steps>
    <item>
      <p>Crear o editar el fitxer <file its:translate="no">/etc/dconf/profile/user</file> a cada client.</p>
    </item>
    <item>
      <p>Just al començament d'aquest fitxer, afegiu la línia següent:</p>
      <code><input its:translate="no">service-db:keyfile/user</input></code>
    </item>
  </steps>
  <p>El <sys its:translate="no">dconf</sys> <em>fitxer de claus</em> tan sols tindrà efecte la propera vegada que l'usuari s'autentifiqui. Sondeja el fitxer de claus per a determinar si s'han fet actualitzacions, de manera que és possible que la configuració no s’actualitzi immediatament.</p>
  <p>Si no s'utilitza un <em>fitxer de claus</em>, la configuració de l'usuari no es recuperarà ni actualitzarà correctament.</p>

</page>
