<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-debug" xml:lang="fr">

  <info>
    <link type="guide" xref="sundry#session"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Quid de <file>~/.xsession-errors</file> ?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Thibault Leclair</mal:name>
      <mal:email>thibaultleclair@yahoo.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mathieu Bousquet</mal:name>
      <mal:email>mathieu.bousquet2@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Irénée Thirion</mal:name>
      <mal:email>irenee.thirion@e.email</mal:email>
      <mal:years>2023-2024</mal:years>
    </mal:credit>
  </info>

  <title>Résoudre les problèmes de session</title>

  <p>Si vous souhaitez consulter plus d’informations sur un problème de session ou que vous souhaitez en résoudre un, consultez le journal du système. Celui-ci enregistre l’historique d’une session utilisateur et de ses applications.</p>

  <p>Le fichier journal de la session X <file>~/.xsession-errors</file> est obsolète et n’est plus utilisé.</p>

<section id="session-log-systemd">
  <title>Afficher le journal de session sur les systèmes basés sur systemd</title>
  <p>Sur les systèmes basés sur systemd, les données du journal de session sont localisés dans le journal de <app>systemd</app>. Il enregistre les données dans un format binaire. Pour les afficher, utilisez la commande <cmd>journalctl</cmd>.</p>

  <steps>
    <title>Pour afficher le journal de session d’un utilisateur :</title>
    <item><p>Déterminez l’identifiant utilisateur (<sys>uid</sys>) en exécutant la commande suivante :</p>
    <screen><output>$ </output><input>id --user</input>
1000</screen></item>
    <item><p>Affichez l’historique du journal pour l’identifiant utilisateur déterminé ci-dessus :</p>
    <screen><output>$ </output><input>journalctl _UID=1000</input></screen>
    </item>
  </steps>

  <p>Pour plus d’informations sur le journal de systemd, consultez la page du manuel <link its:translate="no" href="man:journalctl"><cmd>journalctl</cmd>(1)</link>.</p>

</section>

</page>
