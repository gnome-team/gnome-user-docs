<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions-enable" xml:lang="id">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengaktifkan ekstensi Shell GNOME untuk semua pengguna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2024</mal:years>
    </mal:credit>
  </info>

  <title>Mengaktifkan ekstensi di seluruh mesin</title>
  
  <p>Untuk membuat ekstensi tersedia bagi semua pengguna pada sistem, pasang mereka di direktori <file>/usr/share/gnome-shell/extensions</file>. Perhatikan bahwa ekstensi di seluruh mesin yang baru dipasang dinonaktifkan secara baku.</p>

  <p>Anda perlu mengatur kunci <code>org.gnome.shell.enabled-extensions</code> agar mengatur ekstensi diaktifkan baku. Namun, saat ini tidak ada cara untuk mengaktifkan ekstensi tambahan bagi pengguna yang telah masuk. Ini tidak berlaku untuk pengguna yang sudah ada yang telah memasang dan mengaktifkan ekstensi GNOME mereka sendiri.</p>

  <steps>
    <title>Mengatur kunci org.gnome.shell.enabled-extensions</title>
    <item>
      <p>Buat profil <code>user</code> di <file>/etc/dconf/profile/user</file>:</p>
      <listing>
        <code>
user-db:user
system-db:local
</code>
      </listing>
    </item>
    <item>
      <p>Buat basis data <code>local</code> untuk pengaturan di seluruh mesin dalam <file>/etc/dconf/db/local.d/00-extensions</file>:</p>
      <listing>
        <code>
[org/gnome/shell]
# List all extensions that you want to have enabled for all users
enabled-extensions=['<input>myextension1@myname.example.com</input>', '<input>myextension2@myname.example.com</input>']
</code>
      </listing>
      <p>Kunci <code>enabled-extensions</code> menentukan ekstensi yang diaktifkan menggunakan UUID ekstensi (<code>myextension1@myname.example.com</code> dan <code>myextension2@myname.example.com</code>).</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
