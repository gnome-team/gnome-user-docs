<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom" xml:lang="id">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Buat spesifikasi jenis MIME dan mendaftarkan aplikasi baku.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2024</mal:years>
    </mal:credit>
  </info>

    <title>Menambahkan jenis MIME ubahan untuk semua pengguna</title>
    <p>Untuk menambahkan tipe MIME ubahan bagi semua pengguna di sistem dan mendaftarkan aplikasi baku untuk tipe MIME tersebut, Anda perlu membuat berkas spesifikasi tipe MIME baru di direktori <file>/usr/share/mime/packages/</file> dan berkas <file>.desktop</file> di direktori <file>/usr/share/applications/</file>.</p>
    <steps>
      <title>Tambahkan jenis MIME <sys>application/x-newtype</sys> bagi untuk semua pengguna</title>
      <item>
        <p>Buat berkas <file>/usr/share/mime/packages/application-x-newtype.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;tipe mime baru&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>Contoh berkas <file>application-x-newtype.xml</file> di atas mendefinisikan jenis MIME baru <sys>application/x-newtype</sys> dan menetapkan nama berkas dengan ekstensi <file>.xyz</file> ke jenis MIME itu.</p>
      </item>
      <item>
        <p>Buat sebuah berkas <file>.desktop</file> baru bernama, misalnya, <file>myapplication1.desktop</file>, dan tempatkan di direktori <file>/usr/share/applications/</file>:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>Contoh berkas <file>myapplication1.desktop</file> di atas mengaitkan time MIME <sys>application/x-newtype</sys> dengan aplikasi bernama <app>My Application 1</app>, yang dijalankan oleh perintah <cmd>myapplication1</cmd>.</p>
      </item>
      <item>
        <p>Sebagai root, perbarui basis data MIME agar perubahan Anda berdampak:</p>
        <screen><output># </output><input>update-mime-database /usr/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Sebagai root, perbarui basis data aplikasi:</p>
        <screen><output># </output><input>update-desktop-database /usr/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Untuk memverifikasi bahwa Anda telah berhasil mengasosiasikan berkas <file>*.xyz</file> dengan jenis MIME <sys>application/x-newtype</sys>, pertama-tama buat berkas kosong, misalnya <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Kemudian jalankan perintah <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Untuk memverifikasi bahwa <file>myapplication1.desktop</file> telah ditetapkan dengan benar sebagai aplikasi terdaftar baku bagi tipe MIME <sys>application/x-newtype</sys>, jalankan perintah <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
