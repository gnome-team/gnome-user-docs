<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="fonts" xml:lang="id">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="fonts-user"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Menambahkan fonta ekstra untuk semua pengguna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2024</mal:years>
    </mal:credit>
  </info>

  <title>Menambahkan fonta ekstra untuk semua pengguna</title>

  <p>Anda dapat memasang fonta ekstra yang akan tersedia bagi pengguna di aplikasi yang memakai <sys>fontconfig</sys> untuk penanganan fonta.</p>

  <steps>
    <title>Memasang fonta ekstra</title>
    <item>
      <p>Salin fonta ke direktori <file>/usr/local/share/fonts/</file> untuk memasangnya.</p>
    </item>
    <item>
      <p>Anda mungkin perlu menjalankan perintah berikut untuk memperbarui singgahan fonta:</p>
      <screen><output>$ </output><input>fc-cache /usr/local/share/fonts/</input></screen>
    </item>
  </steps>

  <p>Anda mungkin perlu memulai ulang aplikasi yang berjalan untuk melihat perubahannya. Sesi pengguna tidak perlu dijalankan ulang.</p>
  
  <p>Atau, Anda juga dapat memasang fonta di direktori sistem selain dari <file>/usr/local/share/fonts/</file> jika direktori tersebut tercantum dalam berkas <file>/etc/fonts/fonts.conf</file>. Jika tidak, maka Anda perlu membuat berkas konfigurasi mesin Anda sendiri di <file>/etc/fonts/local.conf</file> yang berisi direktori yang ingin Anda gunakan. Lihat halaman man <cmd>fonts-conf</cmd>(5) untuk informasi lebih lanjut.</p>
  <p>Jika Anda menggunakan direktori alternatif, ingatlah untuk menentukan nama direktori ketika memperbarui singgahan fonta dengan perintah <cmd>fc-cache</cmd>:</p>
  <screen><output>$ </output><input>fc-cache <var>directory_name</var></input></screen>

</page>
