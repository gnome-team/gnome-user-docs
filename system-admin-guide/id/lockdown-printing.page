<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-printing" xml:lang="id">
     
  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-12-04" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mencegah pengguna mencetak dokumen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2024</mal:years>
    </mal:credit>
  </info>

  <title>Menonaktifkan pencetakan</title>

  <p>Anda dapat menonaktifkan dialog cetak agar tidak ditampilkan kepada pengguna. Hal ini dapat berguna jika Anda memberikan akses sementara ke pengguna atau Anda tidak ingin pengguna untuk mencetak ke pencetak jaringan.</p>

  <note style="warning">
    <p>Fitur ini hanya akan bekerja dalam aplikasi yang mendukungnya! Tidak semua aplikasi GNOME dan pihak ketiga mengaktifkan fitur ini. Perubahan ini tidak akan berpengaruh pada aplikasi yang tidak mendukung fitur ini.</p>
  </note>

  <steps>
    <title>Menonaktifkan pencetakan</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Buat berkas kunci <file>/etc/dconf/db/local.d/00-printing</file> untuk memberikan informasi bagi basis data <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-printing</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/lockdown]
 
# Prevent applications from printing
disable-printing=true
</code>
     </listing>
    </item>
    <item>
      <p>Untuk mencegah pengguna mengganti pengaturan ini, buat berkas <file>/etc/dconf/db/local.d/locks/printing</file> dengan konten berikut:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/printing</file></title>
<code>
# Lock printing settings
/org/gnome/desktop/lockdown/disable-printing
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
