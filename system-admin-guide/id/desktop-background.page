<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-background" xml:lang="id">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="backgrounds-extra"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="incomplete">
      <desc>Semua prosa dan instruksi sudah baik. info tambahan perlu disediakan pada pilihan gambar, per komentar di bawah ini. --shaunm</desc>
    </revision>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Mengubah latar belakang desktop baku bagi semua pengguna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2024</mal:years>
    </mal:credit>
  </info>

  <title>Menata latar belakang ubahan</title>

  <p>Anda dapat mengubah latar belakang desktop baku ke salah satu yang ingin Anda gunakan. Misalnya, Anda mungkin ingin menggunakan latar belakang dengan logo perusahaan atau universitas, bukan latar belakang GNOME baku.</p>

  <steps>
    <title>Menata latar belakang baku</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Buat berkas kunci <file>/etc/dconf/db/local.d/00-background</file> untuk memberikan informasi bagi basis data <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-background</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/background]

# Specify the path to the desktop background image file
picture-uri='file:///usr/local/share/backgrounds/wallpaper.jpg'

# Specify one of the rendering options for the background image:
picture-options='scaled'

# Specify the left or top color when drawing gradients, or the solid color
primary-color='000000'

# Specify the right or bottom color when drawing gradients
secondary-color='FFFFFF'
</code>
      </listing>
    </item>
    <item>
      <p>Untuk mencegah pengguna mengganti pengaturan ini, buat berkas <file>/etc/dconf/db/local.d/locks/background</file> dengan konten berikut:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/background</file></title>
<code>
# Lock desktop background settings
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options
/org/gnome/desktop/background/primary-color
/org/gnome/desktop/background/secondary-color
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
