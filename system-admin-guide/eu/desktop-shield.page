<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-shield" xml:lang="eu">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.22" date="2017-01-09" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2017</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aldatu blokeo-pantaila blindatuaren atzeko planoa.</desc>
  </info>

  <title>Aldatu blokeo-pantaila blindatua</title>

  <p>The <em>lock screen shield</em> is the screen that quickly slides down when
  the system is locked. The background of the lock screen shield is controlled by
  the <sys>org.gnome.desktop.screensaver.picture-uri</sys> GSettings key. Since
  <sys>GDM</sys> uses its own <sys>dconf</sys> profile, you can set the default
  background by changing the settings in that profile.</p>

<steps>
  <title>Set the org.gnome.desktop.screensaver.picture-uri key</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Create a <sys>gdm</sys> database for machine-wide settings in
  <file>/etc/dconf/db/gdm.d/<var>01-screensaver</var></file>:</p>
  <code>[org/gnome/desktop/screensaver]
picture-uri='file://<var>/opt/enpresa/atzeko-planoa.jpg</var>'</code>
  <p>Ordeztu <var>/opt/enpresa/atzeko-planoa.jpg</var> blokeo-pantailaren atzeko planoan erabili nahi duzun irudi-fitxategiaren bide-izenarekin.</p>
  <p>Onartutako formatuak: PNG, JPG, JPEG eta TGA. Irudia eskalatu egingo da, beharrezkoa bada, pantaila osora doitzeko.</p>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item><p>Saioa amaitu behar da sistema-ezarpenak indarrean sartzeko.</p>
  </item>
</steps>

<p>Next time you lock the screen, the new lock screen shield will show in the
background. In the foreground, time, date and the current day of the week will
be displayed.</p>

<section id="troubleshooting-background">
  <title>Zer gertatzen ari da atzeko planoa eguneratzen ez bada?</title>

  <p>Ziurtatu <cmd its:translate="no">dconf update</cmd> komandoa exekutatu duzula sistemaren datu-baseak eguneratzeko.</p>

  <p>Atzeko planoa eguneratzen ez bada, saiatu <sys>GDM</sys> berrabiarazten.</p>
</section>

</page>
