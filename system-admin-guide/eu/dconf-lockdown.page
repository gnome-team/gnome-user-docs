<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-lockdown" xml:lang="eu">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="dconf-profiles"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Erabili <sys its:translate="no">dconf</sys>(e)n <em>blokeo</em> modua  erabiltzaileek ezarpenak aldatu ditzaten.</desc>
  </info>

  <title>Blokeatu ezarpen batzuk</title>

  <p>Blokeo modua erabiliz dconf aplikazioan, erabiltzaileek ezarpen jakin batzuk aldatu ditzaten eragotzi daiteke. Sistema-ezarpenak ez badira blokeatzen, erabiltzaile-ezarpenek lehentasuna izango dute sistema-ezarpenen aurrean.</p>

  <p>To <em>lock down</em> a <sys its:translate="no">dconf</sys> key or subpath,
  you will need to create a <file its:translate="no">locks</file> subdirectory
  in the keyfile directory. The files inside this directory contain a list of
  keys or subpaths to lock. Just as with the <link xref="dconf-keyfiles">keyfiles</link>,
  you may add any number of files to this directory.</p>

  <steps>
    <title>Blokeatu ezarpen bat</title>
    <item>
      <p>Before you can lock down a key or subpath, you need to set it. This
      example shows how to lock <link xref="desktop-background">a background
      setting</link> once it has been set.</p>

      <p>At this point, you should have
      <link xref="dconf-profiles">a <sys>user</sys> profile</link> and
      <link xref="dconf-keyfiles">a keyfile</link> with the settings that you
      want to lock down.</p>
    </item>
    <item>
      <p>Sortu <file its:translate="no">/etc/dconf/db/local.d/locks</file> izeneko direktorio bat.</p>
    </item>
    <item>
      <p>Create a file in the
      <file its:translate="no">/etc/dconf/db/local.d/locks/</file> directory
      and list one key or subpath per line. For example, create
      <file its:translate="no">/etc/dconf/db/local.d/locks/00_default-wallpaper</file>:</p>
<code its:translate="no">
# <span its:translate="yes">prevent changes to the background</span>
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options
/org/gnome/desktop/background/primary-color
/org/gnome/desktop/background/secondary-color
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
