<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-debug" xml:lang="de">

  <info>
    <link type="guide" xref="sundry#session"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Was ist mit der Datei <file>~/.xsession-errors</file>?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Fehlerdiagnose von Sitzungsproblemen</title>

  <p>Wenn Sie mehr über ein Problem in einer Sitzung herausfinden oder es beseitigen wollen, schauen Sie in das Systemprotokoll, welches Daten zu Ihren Benutzersitzungen und Anwendungen speichert.</p>

  <p>Das X-Sitzungsprotokoll in <file>~/.xsession-errors</file> ist überholt und wird nicht mehr verwendet.</p>

<section id="session-log-systemd">
  <title>Sitzungsprotokoll auf Systemd-basierten Systemen betrachten</title>
  <p>Auf Systemd-basierten Systemen finden Sie die Protokolldaten der Sitzung im <app>systemd</app>-Journal, wo die Daten in einem binären Format gespeichert werden. Diese Protokolle können Sie mit dem Befehl <cmd>journalctl</cmd> anzeigen lassen.</p>

  <steps>
    <title>So betrachten Sie die Protokolle von Benutzersitzungen:</title>
    <item><p>Ermitteln Sie Ihre Benutzerkennung (<sys>uid</sys>) mit dem folgenden Befehl:</p>
    <screen><output>$ </output><input>id --user</input>
1000</screen></item>
    <item><p>Zeigen Sie die Journal-Protokolle für die vorher ermittelte Benutzerkennung an:</p>
    <screen><output>$ </output><input>journalctl _UID=1000</input></screen>
    </item>
  </steps>

  <p>Weitere Informationen zum Journal von Systemd finden Sie in der Handbuchseite zu <link its:translate="no" href="man:journalctl"><cmd>journalctl</cmd>(1)</link>.</p>

</section>

</page>
