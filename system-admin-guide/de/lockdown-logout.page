<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-logout" xml:lang="de">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Den Benutzer am Abmelden und Benutzerwechsel hindern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Benutzerabmeldung und -wechsel deaktivieren</title>

  <p>Den Benutzer am Abmelden zu hindern, ist für spezielle Arten von GNOME-Installationen (unbesetzte Kioske, öffentliche Internet-Terminals usw.).</p>

  <note style="important">
  <p>Benutzer können der Sperrung der Abmeldung ausweichen, indem Sie zu einem anderen Benutzer wechseln. Das ist der Grund, weshalb bei der Konfiguration des Systems auch der <em>Benutzerwechsel</em> deaktiviert werden sollte.</p>
  </note>

  <steps>
  <title>Benutzerabmeldung und -wechsel deaktivieren</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Erstellen Sie die Schlüsseldatei <file>/etc/dconf/db/local.d/00-logout</file>, um Informationen für die <sys>local</sys>-Datenbank bereitzustellen:</p>
<screen>
[org/gnome/desktop/lockdown]
# Den Benutzer am Abmelden hindern
disable-log-out=true

# Benutzerwechsel verhindern
disable-user-switching=true
</screen>
  </item>
  <item>
  <p>So setzen Sie die Benutzereinstellung außer Kraft und verhindern, dass der Benutzer sie in <file>/etc/dconf/db/local.d/locks/lockdown</file> ändert:</p>
<screen>
# Benutzerabmeldung sperren
/org/gnome/desktop/lockdown/disable-log-out

# Benutzerwechsel sperren
/org/gnome/desktop/lockdown/disable-user-switching
</screen>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item>
  <p>Sie müssen sich abmelden, damit die systemweiten Einstellungen wirksam werden.</p>
  </item>
  </steps>

</page>
