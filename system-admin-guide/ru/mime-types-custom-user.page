<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom-user" xml:lang="ru">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Создание спецификации MIME-типа пользователя и регистрация приложения по умолчанию.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

    <title>Добавление пользовательского типа MIME для отдельных пользователей</title>
    <p>Чтобы добавить пользовательский MIME-тип для отдельных пользователей и зарегистрировать приложение для этого MIME-типа, необходимо создать новый файл спецификации MIME-типа в каталоге <file>~/.local/share/mime/packages/</file> и файл <file>.desktop</file> в каталоге <file>~/.local/share/applications/</file>.</p>
    <steps>
      <title>Добавление пользовательского MIME-типа <code>application/x-newtype</code> для отдельных пользователей</title>
      <item>
        <p>Создайте файл <file>~/.local/share/mime/packages/application-x-newtype.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>Приведённый выше пример файла <file>application-x-newtype.xml</file> определяет новый MIME-тип <sys>application/x-newtype</sys> и связывает названия файлов с расширением <file>.xyz</file> с этим типом MIME.</p>
      </item>
      <item>
        <p>Создайте новый файл <file>.desktop</file> с названием, например, <file>myapplication1.desktop</file>, и поместите его в каталог <file>~/.local/share/applications/</file>:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>Приведённый выше пример файла <file>myapplication1.desktop</file> связывает MIME-тип <code>application/x-newtype</code> с приложением <app>My Application 1</app>, которое запускается командой <cmd>myapplication1</cmd>.</p>
      </item>
      <item>
        <p>Обновите базу данных MIME, чтобы ваши изменения вступили в силу:</p>
        <screen><output>$ </output><input>update-mime-database ~/.local/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Обновите базу данных приложений:</p>
        <screen><output>$ </output><input>update-desktop-database ~/.local/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Чтобы убедиться, что вы успешно связали файлы <file>*.xyz</file> с типом MIME <sys>application/x-newtype</sys>, для начала создайте пустой файл, например <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Затем запустите команду <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Чтобы убедиться, что <file>myapplication1.desktop</file> был правильно установлен в качестве зарегистрированного приложения по умолчанию для типа MIME <sys>application/x-newtype</sys>, запустите команду <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
