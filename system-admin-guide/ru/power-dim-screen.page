<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="power-dim-screen" xml:lang="ru">

  <info>
    <link type="guide" xref="user-settings"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.12" date="2014-06-20" status="candidate"/>

    <credit type="author copyright">
      <name>Матиас Класен (Matthias Clasen)</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <desc>Уменьшить яркость экрана, если в течение определённого времени со стороны пользователя не было никаких действий.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Уменьшить яркость экрана при отсутствии действий со стороны пользователя</title>

  <p>Вы можете уменьшить яркость экрана при простое компьютера (не использовании) в течение некоторого времени.</p>

  <steps>
    <title>Уменьшить яркость экрана на неактивном компьютере</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Создайте файл ключа <file>/etc/dconf/db/local.d/00-power</file>, чтобы предоставить информацию для базы данных <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-power</file></title>
<code>
# Указать путь dconf
[org/gnome/settings-daemon/plugins/power]

# Включить уменьшение яркости экрана
idle-dim=true

# Установить яркость при затемнении
idle-brightness=30
</code>
      </listing>
    </item>
    <item>
      <p>Создайте файл ключа <file>/etc/dconf/db/local.d/00-session</file>, чтобы предоставить информацию для базы данных <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-session</file></title>
<code>
# Указать путь dconf
[org/gnome/desktop/session]

# Установить количество секунд для того, чтобы сеанс считался бездействующим
idle-delay=uint32 300
</code>
      </listing>
      <p>Вы должны добавить <code>uint32</code> к целочисленным ключам, как показано выше.</p>
    </item>
    <item>
      <p>Чтобы пользователь не мог изменить эти настройки, необходимо создать файл <file>/etc/dconf/db/local.d/locks/power-saving</file> со следующим содержимым:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/power-saving</file></title>
<code>
# Блокировка уменьшения яркости экрана и времени ожидания при простое
/org/gnome/settings-daemon/plugins/power/idle-dim
/org/gnome/settings-daemon/plugins/power/idle-brightness
/org/gnome/desktop/session/idle-delay
</code>
      </listing>
      <p>Если вы хотите, чтобы пользователь мог изменять данные настройки, пропустите этот шаг.</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
