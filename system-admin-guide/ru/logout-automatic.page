<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="logout-automatic" xml:lang="ru">

  <info>
    <link type="guide" xref="login#management"/>
<!--    <link type="seealso" xref="dconf-profiles" />-->
    <link type="seealso" xref="dconf-lockdown"/>
    <link type="seealso" xref="login-automatic"/>

    <revision pkgversion="3.12" date="2014-06-18" status="review"/>

    <credit type="author copyright">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <credit type="editor">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Завершение бездействующего сеанса пользователя.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Настройка автоматического выхода из системы</title>

  <p>Сеансы пользователей, которые не использовались в течение определённого периода времени, могут быть автоматически завершены. Вы можете настроить различное поведение в зависимости от того, работает ли машина от батареи или от сети, установив соответствующий ключ <sys its:translate="no">dconf</sys> с последующей его блокировкой.</p>

  <note style="warning">
    <p>Имейте в виду, что пользователи могут потерять несохранённые данные, если будет автоматически завершён бездействующий сеанс.</p>
  </note>

  <steps>
    <title>Установка автоматического выхода из системы для машины с питанием от сети</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Создайте базу данных <code>local</code> для общесистемных настроек в <file>/etc/dconf/db/local.d/00-autologout</file>:</p>
      <listing>
        <code>
[org/gnome/settings-daemon/plugins/power]
# Установить время ожидания в 900 секунд при питании от сети.
sleep-inactive-ac-timeout=900
# Установить вариант действия после окончания времени ожидания для выхода из системы при питании от сети.
sleep-inactive-ac-type='logout'
</code>
      </listing>
    </item>
    <item>
      <p>Переопределите пользовательскую настройку и запретите пользователю внесение изменений в <file>/etc/dconf/db/local.d/locks/autologout</file>:</p>
      <listing>
        <code>
# Блокировка настроек автоматического выхода
/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-timeout
/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-type
</code>
</listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

  <!-- There will shortly be a way to get key descriptions using gsettings, we
       should recommend this instead of listing the terms here. See
       https://bugzilla.gnome.org/show_bug.cgi?id=668232 -->
  <p>Представляют интерес следующие ключи GSettings:</p>
  <terms>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-ac-timeout</code></title>
      <p>Количество секунд, в течение которых компьютер должен быть неактивен, прежде чем перейдет в спящий режим при условии, что он работает от сети переменного тока.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-ac-type</code></title>
      <p>Что должно произойти по завершению времени ожидания, если компьютер работает от сети питания.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-battery-timeout</code></title>
      <p>Количество секунд, в течение которых компьютер должен быть неактивен, прежде чем перейдет в спящий режим при условии, что он работает от аккумуляторной батареи.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-battery-type</code></title>
      <p>Что должно произойти по завершению времени ожидания, если компьютер работает от аккумуляторной батареи.</p>
    </item>
  </terms>

  <p>Вы можете запустить для ключа команду <cmd>gsettings range</cmd>, чтобы получить список значений, которыми можете воспользоваться. Пример:</p>
  
  <screen>$ <input>gsettings range org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type</input>
enum
'blank' # очистить экран
'suspend' # приостановить работу системы
'shutdown' # запустить стандартную процедуру выключения
'hibernate' # перевод системы в спящий режим
'interactive' # запрос пользователя о дальнейших действиях
'nothing' # ничего не делать
'logout' # выход из сеанса</screen>

</page>
