<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-logout" xml:lang="ru">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Запретить пользователю выход из системы, а также переключение пользователей.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Отключить выход пользователя и переключение пользователей</title>

  <p>Запрещение выхода пользователя из системы полезно для особых случаев развертывания GNOME (автономные киоски, общественные терминалы доступа в интернет и т.д.).</p>

  <note style="important">
  <p>Пользователи могут обойти блокировку выхода из системы, переключившись на другого пользователя. По этой причине при настройке системы рекомендуется также отключать возможность <em>переключения пользователей</em>.</p>
  </note>

  <steps>
  <title>Отключить выход пользователя и переключение пользователей</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Создайте файл ключа <file>/etc/dconf/db/local.d/00-logout</file>, чтобы предоставить информацию для базы данных <sys>local</sys>:</p>
<screen>
[org/gnome/desktop/lockdown]
# Запретить пользователю выходить из системы
disable-log-out=true

# Запретить пользователю переключение пользователей
disable-user-switching=true
</screen>
  </item>
  <item>
  <p>Переопределите пользовательскую настройку и запретите пользователю внесение изменений в <file>/etc/dconf/db/local.d/locks/lockdown</file>:</p>
<screen>
# Заблокировать выход пользователя
/org/gnome/desktop/lockdown/disable-log-out

# Заблокировать переключение пользователей
/org/gnome/desktop/lockdown/disable-user-switching
</screen>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item>
  <p>Перезагрузите систему, чтобы общесистемные настройки вступили в силу.</p>
  </item>
  </steps>

</page>
