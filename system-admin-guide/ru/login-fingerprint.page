<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-fingerprint" xml:lang="ru">

  <info>
    <link type="guide" xref="login#management"/>
    <link type="guide" xref="user-settings#lockdown"/>
    <revision pkgversion="3.12" date="2014-06-17" status="candidate"/>

    <credit type="author copyright">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Заблокировать пользователю возможность авторизоваться с помощью сканера отпечатков пальцев.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Запретить вход по отпечатку пальца</title>

  <p>Пользователи, имеющие сканер отпечатков пальцев, могут использовать для входа в систему свои отпечатки пальцев вместо пароля. Предварительно такая возможность <link href="help:gnome-help#session-fingerprint">должна быть настроен пользователем</link>.</p>

  <p>Считыватели отпечатков пальцев не всегда надежны, поэтому из соображений безопасности вы можете отключить возможность входа в систему с помощью считывателя.</p>

  <steps>
    <title>Отключение вход в систему с помощью считывателя отпечатков пальцев:</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Создайте файл ключа <file>/etc/dconf/db/local.d/00-login</file>, чтобы предоставить информацию для базы данных <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-login</file></title>
<code>
# Указать путь dconf
[org/gnome/login-screen]

# Отключить считыватель отпечатков пальцев
enable-fingerprint-authentication=false
</code>
      </listing>
    </item>
    <item>
      <p>Чтобы пользователь не мог изменить эти настройки, необходимо создать файл <file>/etc/dconf/db/local.d/locks/login</file> со следующим содержимым:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/fingerprintreader</file></title>
<code>
# Список ключей, используемых для настройки входа
/org/gnome/login-screen/enable-fingerprint-authentication
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
