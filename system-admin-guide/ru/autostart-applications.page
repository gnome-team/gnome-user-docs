<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="ru">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Как добавить автозапуск приложения для всех пользователей?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Добавить автозапуск приложения для всех пользователей</title>

  <p>Для автоматического запуска приложения при входе в систему необходимо создать <file>.desktop</file> файл для этого приложения в каталоге <file>/etc/xdg/autostart/</file>.</p>

<steps>
  <title>Чтобы добавить автозапуск приложения для всех пользователей:</title>
  <item><p>Создайте <file>.desktop</file> файл в каталоге <file>/etc/xdg/autostart/</file>:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Files</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Замените <var>Files</var> именем приложения.</p></item>
  <item><p>Замените <var>nautilus -n</var> на необходимую для запуска требуемого приложения команду.</p></item>
  <item><p>Вы можете использовать ключ <code>AutostartCondition</code> для проверки значения ключа GSettings.</p>
  <p>Диспетчер сеансов автоматически запускает приложение, если значение ключа «true». Если значение ключа изменяется в течение текущего сеанса, диспетчер сеансов будет запускать или останавливать приложение в зависимости от предыдущего значения ключа.</p>
</item>
</steps>

</page>
