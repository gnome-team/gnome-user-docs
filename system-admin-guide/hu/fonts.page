<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="fonts" xml:lang="hu">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="fonts-user"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>További betűkészletek hozzáadása az összes felhasználónak.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs at fsf dot hu</mal:email>
      <mal:years>2018, 2019, 2020, 2021, 2023, 2024.</mal:years>
    </mal:credit>
  </info>

  <title>További betűkészlet hozzáadása az összes felhasználónak</title>

  <p>Telepíthet további betűkészletet, amely elérhető lesz a felhasználóknak azokban az alkalmazásokban, amelyek a <sys>fontconfig</sys> beállítást használják a betűkészletek kezeléséhez.</p>

  <steps>
    <title>További betűkészlet telepítése</title>
    <item>
      <p>Másolja a betűkészletet az <file>/usr/local/share/fonts/</file> könyvtárba a telepítéshez.</p>
    </item>
    <item>
      <p>Lehet, hogy le kell futtatnia a következő parancsot a betűkészlet gyorsítótárának frissítéséhez:</p>
      <screen><output>$ </output><input>fc-cache /usr/local/share/fonts/</input></screen>
    </item>
  </steps>

  <p>Előfordulhat, hogy újra kell indítania a futó alkalmazásokat a változtatások megtekintéséhez. A felhasználói munkameneteket nem szükséges újraindítani.</p>
  
  <p>Alternatívaként telepítheti a betűkészleteket az <file>/usr/local/share/fonts/</file> könyvtártól eltérő rendszerkönyvtárba is, ha az a könyvtár fal van sorolva az <file>/etc/fonts/fonts.conf</file> fájlban. Ha nincs, akkor létre kell hoznia a saját rendszerszintű beállítófájlját az <file>/etc/fonts/local.conf</file> fájlban, amely tartalmazza azt a könyvtárat, amit használni szeretne. További információkért nézze meg a <cmd>fonts-conf</cmd>(5) kézikönyvoldalát.</p>
  <p>Ha alternatív könyvtárat használ, akkor ne felejtse el megadni a könyvtár nevét, amikor frissíti a betűkészlet gyorsítótárát az <cmd>fc-cache</cmd> paranccsal:</p>
  <screen><output>$ </output><input>fc-cache <var>könyvtár_neve</var></input></screen>

</page>
