<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="hu">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hogyan adhatok hozzá automatikusan induló alkalmazást az összes felhasználónak?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs at fsf dot hu</mal:email>
      <mal:years>2018, 2019, 2020, 2021, 2023, 2024.</mal:years>
    </mal:credit>
  </info>

  <title>Automatikusan induló alkalmazás hozzáadása az összes felhasználónak</title>

  <p>A felhasználó bejelentkezésekor történő automatikus alkalmazásindításhoz létre kell hoznia annak az alkalmazásnak a <file>.desktop</file> fájlját az <file>/etc/xdg/autostart/</file> könyvtárban.</p>

<steps>
  <title>Egy automatikusan induló alkalmazás hozzáadásához az összes felhasználó számára:</title>
  <item><p>Hozzon létre egy <file>.desktop</file> fájlt az <file>/etc/xdg/autostart/</file> könyvtárban:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Files</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Cserélje ki a <var>Files</var> nevet az alkalmazás nevével.</p></item>
  <item><p>Cserélje ki a <var>nautilus -n</var> parancsot azzal a paranccsal, amelyet az alkalmazás futtatásához szeretne használni.</p></item>
  <item><p>Használhatja az <code>AutostartCondition</code> kulcsot egy GSettings kulcs értékének ellenőrzéséhez.</p>
  <p>A munkamenet-kezelő automatikusan futtatja az alkalmazást, ha a kulcs értéke igaz. Ha a kulcs értéke megváltozik a futó munkamenetben, akkor a munkamenet-kezelő elindítja vagy leállítja az alkalmazást attól függően, hogy mi volt a kulcs előző értéke.</p>
</item>
</steps>

</page>
