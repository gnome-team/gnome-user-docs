<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-os" xml:lang="eu">

  <info>
    <link type="guide" xref="about" group="os"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aurkitu zure siteman instalatutako sistema eragileari buruzko informazioa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Aurkitu zure sistema eragileari buruzko informazioa</title>

  <p>Zein sistema eragile dagoen instalatuta jakinda, software edo hardware berria zure sistemarekin bateragarria den ala ez hobeto ulertuko duzu.</p>

  <steps>
    <item>
      <p>Ireki <gui xref="shell-introduction#activities">Jarduerak</gui> ikuspegia eta hasi <gui>Sistema</gui> idazten.</p>
    </item>
    <item>
     <p>Emaitzetan, hautatu <guiseq><gui>Ezarpenak</gui><gui>Sistema</gui></guiseq>. Horrek <gui>Sistema</gui> panela irekiko du.</p>
    </item>
    <item>
      <p>Hautatu <gui>Honi buruz</gui> panela irekitzeko.</p>
    </item>
    <item>
      <p>Hautatu <gui>Sistemaren xehetasunak</gui>.</p>
    </item>
    <item>
      <p>Begiratu  <gui>SEaren izena</gui>, <gui>SE mota</gui>, <gui>GNOMEren bertsioa</gui> eta horrelakoetan ematen den informazioa.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Elementu bakoitzeko informazioa kopiatzeko, hautatu elementua eta kopiatu arbelean. Horrela, erraza izango zaizu zure sistemaren informazioa beste batzuekin partekatzea.</p>
  </note>

</page>
