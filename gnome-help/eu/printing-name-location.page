<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="eu">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aldatu inprimagailu baten izena edo kokalekua inprimatze-ezarpenetan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>
  
  <title>Inprimagailu baten izena edo kokalekua aldatzea</title>

  <p>Inprimagailu baten izena edo kokalekua inprimatze-ezarpenetan aldatu daiteke.</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to change the name or location of a printer.</p>
  </note>

  <section id="printer-name-change">
    <title>Inprimagailuaren izena aldatzea</title>

  <p>Inprimagailu baten izena aldatu nahi baduzu, jarraitu honako urratsak:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Sakatu <gui>Inprimagailuak</gui> panela irekitzeko.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Egin klik <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">ezarpenak</span></media> inprimagailuaren alboan dagoen botoian.</p>
    </item>
    <item>
      <p>Hautatu <gui style="menuitem">Inprimagailu-ezarpenak</gui>.</p>
    </item>
    <item>
      <p>Enter a new name for the printer in the <gui>Name</gui> field.</p>
    </item>
    <item>
      <p>Itxi elkarrizketa-koadroa.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Inprimagailuaren kokalekua aldatzea</title>

  <p>Inprimagailuaren kokalekua aldatzeko:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Sakatu <gui>Inprimagailuak</gui> panela irekitzeko.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Egin klik <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">ezarpenak</span></media> inprimagailuaren alboan dagoen botoian.</p>
    </item>
    <item>
      <p>Hautatu <gui style="menuitem">Inprimagailu-ezarpenak</gui>.</p>
    </item>
    <item>
      <p>Enter a new location for the printer in the <gui>Location</gui> field.</p>
    </item>
    <item>
      <p>Itxi elkarrizketa-koadroa.</p>
    </item>
  </steps>

  </section>

</page>
