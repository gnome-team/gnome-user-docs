<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="eu">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kontrolatu zein informazio bistaratzen den zerrenda-ikuspegiko zutabeetan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Fitxategiak aplikazioarne zerrenda-zutabeen hobespenak</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view..</p>
  <p>Click the view options button in the toolbar, pick <gui>Visible
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns.</p>


  <terms>
    <item>
      <title><gui>Izena</gui></title>
      <p>Karpeten eta fitxategien izena.</p>
      <note style="tip">
        <p><gui>Izena</gui> zutabea ezin da ezkutatu.</p>
      </note>
    </item>
    <item>
      <title><gui>Tamaina</gui></title>
      <p>The size of a folder is given as the number of items contained in the
      folder. The size of a file is given as bytes, KB, or MB.</p>
    </item>
    <item>
      <title><gui>Mota</gui></title>
      <p>Displayed as folder, or file type such as PDF document, JPEG image,
      MP3 audio, and more.</p>
    </item>
    <item>
      <title><gui>Jabea</gui></title>
      <p>Karpetaren edo fitxategiaren jabearen izena.</p>
    </item>
    <item>
      <title><gui>Taldea</gui></title>
      <p>The group the file is owned by. Each user is normally in their own
      group, but it is possible to have many users in one group. For example, a
      department may have their own group in a work environment.</p>
    </item>
    <item>
      <title><gui>Baimenak</gui></title>
      <p>Fitxategiaren sarbide-baimenak bistaratzen ditu. Adibidez, <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>Lehen karakterea fitxategi mota da. <gui>-</gui> karaktereak esan nahi du fitxategi arrunta dela eta <gui>d</gui> karaktereak esan nahi du direktorioa (karpeta) dela. Kasu bakan batzuetan, beste karaktere batzuk ere agertu daitezke.</p>
        </item>
        <item>
          <p>Hurrengo hiru karaktereek (<gui>rwx</gui>) fitxategiaren jabearen baimenak zehazten dituzte.</p>
        </item>
        <item>
          <p>Hurrengo hirurek (<gui>rw-</gui>) fitxategiaren jabea den taldearen kide guztien baimenak zehazten dituzte.</p>
        </item>
        <item>
          <p>Zutabeko azken hiru karaktereek (<gui>r--</gui>) sistemako gainerako erabiltzaile guztien baimenak zehazten dituzte.</p>
        </item>
      </list>
      <p>Baimen bakoitzak honako esanahia du:</p>
      <list>
        <item>
          <p><gui>r</gui>: irakurri daiteke, horrek esan nahi du fitxategia edo karpeta ireki daitekeela</p>
        </item>
        <item>
          <p><gui>w</gui>: idatzi daiteke, horrek esan nahi du aldaketak gorde daitezkeela bertan</p>
        </item>
        <item>
          <p><gui>x</gui>: exekutatu daiteke, horrek esan nahi du programa edo script bat bada exekutatu egin daitekeela, edo karpeta bat bada azpikarpetak eta fitxategiak atzitu daitezkeela</p>
        </item>
        <item>
          <p><gui>-</gui>: baimenak ez daude ezarrita</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Kokalekua</gui></title>
      <p>Fitxategiaren kokalekuaren bide-izena.</p>
    </item>
    <item>
      <title><gui>Aldatze-data</gui></title>
      <p>Fitxategia aldatu deneko azken orduaren azken data ematen du.</p>
    </item>
    <item>
      <title><gui>Aldatuta — Ordua</gui></title>
      <p>Gives the date and time of the last time the file was modified.</p>
    </item>
    <item>
      <title><gui>Atzitze-data</gui></title>
      <p>Gives the date or time of the last time the file was accessed.</p>
    </item>
    <item>
      <title><gui>Oraintsukoa</gui></title>
      <p>Gives the date or time of the last time the file was accessed by the user.</p>
    </item>
    <item>
      <title><gui>Mota xehea</gui></title>
      <p>Elementuaren MIME mota bistaratzen du.</p>
    </item>
    <item>
      <title><gui>Sortze-data</gui></title>
      <p>Fitxategia sortu zeneko data eta ordua ematen ditu.</p>
    </item>
  </terms>

</page>
