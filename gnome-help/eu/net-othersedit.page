<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="eu">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>You need to uncheck the <gui>Available to all users</gui> option in the network connection settings.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Beste erabiltzaileek ezin dituzte sareko konexioak editatu</title>

  <p>If you can edit a network connection but other users on your computer
  can’t, you may have set the connection to be <em>available to all users</em>.
  This makes it so that everyone on the computer can <em>connect</em> using
  that connection<!--, but only users
  <link xref="user-admin-explain">with administrative rights</link> are allowed
  to change its settings-->.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Ireki <gui xref="shell-introduction#activities">Jarduerak</gui> ikuspegia eta hasi <gui>Sarea</gui> idazten.</p>
    </item>
    <item>
      <p>Sakatu <gui>Sarea</gui> panela irekitzeko.</p>
    </item>
    <item>
      <p>Hautatu <gui>Wifia</gui> ezkerreko zerrendatik.</p>
    </item>
    <item>
      <p>Egin klik <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">ezarpenak</span></media> botoian konexioaren xehetasunak irekitzeko.</p>
    </item>
    <item>
      <p>Hautatu <gui>Identitatea</gui> ezkerreko paneletik.</p>
    </item>
    <item>
      <p>At the bottom of the <gui>Identity</gui> panel, check the <gui>Make
      available to other users</gui> option to allow other users to use the
      network connection.</p>
    </item>
    <item>
      <p>Sakatu <gui style="button">Aplikatu</gui> aldaketak gordetzeko.</p>
    </item>
  </steps>

</page>
