<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="eu">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc><gui>Egin klik gainetik igarotzean</gui> (ordezko klika) eginbideari esker, sagua geldi mantenduta egiten da klik.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Simulatu klik egitea gainetik pasatzean</title>

  <p>Saguraren erakuslea pantailaren kontrol edo objektu baten gainera eramatea nahiko da klik egiteko edo arrastatzeko. Aukera hori erabilgarria izan daiteke aldi berean sagua mugitzea eta klik egitea zaila egiten bazaizu. Eginbide horri <gui>Egin klik gainetik igarotzean</gui> edo ordezko klika deritzo.</p>

  <p><gui>Egin klik gainetik igarotzean</gui> gaituta badago, saguaren erakuslea kontrol baten gainetik pasatu eta sagua bertan utzi dezakezu, eta apur bat itxaronda, botoia sakatua izango da.</p>

  <steps>
    <item>
      <p>Ireki <gui xref="shell-introduction#activities">Jarduerak</gui> ikuspegia eta hasi <gui>Erabilerraztasuna</gui> idazten.</p>
    </item>
    <item>
      <p>Sakatu <gui>Erabilerraztasuna</gui> panela irekitzeko.</p>
    </item>
    <item>
      <p>Hautatu <gui>Erakustea eta klik egitea</gui> atala hura irekitzeko.</p>
    </item>
    <item>
      <p><gui>Klik egitearen laguntzailea</gui> atalean, aktibatu <gui>Egin klik gainetik igarotzean</gui> aukera.</p>
    </item>
  </steps>

  <p><gui>Egin klik gainetik igarotzean</gui> leihoa irekiko da eta beste leihoen gainean geratuko da. Gainetik igarotzean zein klik mota gertatuko den aukeratzeko erabili daiteke. Adibidez, <gui>2. mailako klik egitea</gui> aukeratzen baduzu, eskuineko klik egingo duzu gainetik igarotzean. Klik bikoitza, eskuineko klika edo arrastatzea egin ondoren, automatikoki itzuliko zara klik egitera.</p>

  <p>Saguaren erakuslea botoi baten gainetik igarotzen bada eta bertan uzten bada, apurka kolorez aldatuko da. Erabat kolorez aldatu denean, botoian klik egingo da.</p>

  <p>Doitu <gui>Atzerapena</gui> ezarpena klika gertatu arte saguaren erakuslea zenbat denbora eduki behar duzun aldatzeko.</p>

  <p>Ez duzu sagua geldi-geldi mantendu behar klik egiteko. Erakuslea apur bat mugitu daiteke eta hala ere klik egingo du tarte baten ondoren. Gehiegi mugitzen bada, ordea, klika ez da gertatuko.</p>

  <p><gui>Mugimenduaren atalasea</gui> ezarpena doituz, objektu baten gainetik erakuslea igarotzean klik egin dela kontsideratzeko erakuslearen mugimendua zein neurritakoa izan behar den aldatu ahal izango duzu.</p>

</page>
