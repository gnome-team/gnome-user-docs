<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-find" xml:lang="eu">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-hidden"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.12" date="2014-03-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOMEren dokumentazio-proiektua</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>The wireless could be turned off or broken, or you may be trying to
    connect to a hidden network.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Ez dut nire haririk gabeko sarea ikusten zerrendan</title>

  <p>Hainbat arrazoi daude haririk gabeko sarea sistema-menuaren sare erabilgarrien zerrendan ez ikusteko.</p>

<list>
 <item>
  <p>If no networks are shown in the list, your wireless hardware could be
  turned off, or it <link xref="net-wireless-troubleshooting">may not be
  working properly</link>. Make sure it is turned on.</p>
 </item>
<!-- Does anyone have lots of wireless networks to test this? Pretty sure it's
     no longer correct.
  <item>
    <p>If there are lots of wireless networks nearby, the network you are
    looking for might not be on the first page of the list. If this is the
    case, look at the bottom of the list for an arrow pointing towards the
    right and hover your mouse over it to display the rest of the wireless
    networks.</p>
  </item>-->
 <item>
  <p>You could be out of range of the network. Try moving closer to the
  wireless base station/router and see if the network appears in the list after
  a while.</p>
 </item>
 <item>
  <p>Denbora behar da haririk gabeko sareen zerrenda eguneratzeko. Ordenagailua piztu berri baduzu edo beste kokaleku batera eraman baduzu, itxaron minutu bat eta egiaztatu berriro sarea zerrendan agertu den ala ez.</p>
 </item>
 <item>
  <p>The network could be hidden. You need to
  <link xref="net-wireless-hidden">connect in a different way</link> if it is a
  hidden network.</p>
 </item>
</list>

</page>
