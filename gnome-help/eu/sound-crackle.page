<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="eu">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOMEren dokumentazio-proiektua</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Begiratu audio-kableak eta soinu-txartelen kontrolatzaileak ondo daudela.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>I hear crackling or buzzing when sounds are playing</title>

  <p>If you hear crackling or buzzing when sounds are playing on your computer,
 you may have a problem with the audio cables or connectors, or a problem with
 the drivers for the sound card.</p>

<list>
 <item>
  <p>Begiratu bozgorailuak ondo entxufatuta daudela.</p>
  <p>If the speakers are not fully plugged in, or if they are plugged into the
  wrong socket, you might hear a buzzing sound.</p>
 </item>

 <item>
  <p>Ziurtatu bozgorailuaren/entzungailuen kablea kaltetuta ez dagoela.</p>
  <p>Audio cables and connectors can gradually wear with use. Try plugging the
  cable or headphones into another audio device (like an MP3 player or a CD
  player) to check if there is still a crackling sound. If there is, you may need
  to replace the cable or headphones.</p>
 </item>

 <item>
  <p>Begiratu soinu-kontrolatzaileak onak diren ala ez.</p>
  <p>Some sound cards do not work very well on Linux because they do not have very
  good drivers. This problem is more difficult to identify. Try searching for
  the make and model of your sound card on the internet, plus the search term
  “Linux”, to see if other people are having the same problem.</p>
  <p>You can use the <cmd>lspci</cmd> command to get more information about your
  sound card.</p>
 </item>
</list>

</page>
