<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="eu">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gehitu edo kendu kontaktuan helbide-liburu lokalean.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Gehitu edo kendu kontaktu bat</title>

  <p>Kontaktu bat gehitzeko:</p>

  <steps>
    <item>
      <p>Sakatu <gui style="button">+</gui> botoia.</p>
    </item>
    <item>
      <p><gui>Kontaktu berria</gui> elkarrizketa-koadroan, sartu kontaktuaren izena eta informazioa. Sakatu eremu bakoitzaren alboko goitik beherako koadroa xehetasun-maila aukeratzeko.</p>
    </item>
    <item>
      <p>Xehetasun gehiago gehitzeko, sakatu <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Ikusi gehiago</span></media> aukera.</p>
    </item>
    <item>
      <p>Sakatu <gui style="button">Gehitu</gui> kontaktua gordetzeko.</p>
    </item>
  </steps>

  <p>Kontaktu bat kentzeko:</p>

  <steps>
    <item>
      <p>Hautatu kontaktua zerrendan.</p>
    </item>
    <item>
      <p>Egin klik goiburu-barraren goiko eskuineko izkinako <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Ikusi gehiago</span></media> botoian.</p>
    </item>
    <item>
      <p>Sakatu <gui style="menu item">Ezabatu</gui> aukera kontaktua kentzeko.</p>
    </item>
   </steps>
    <p>Kontaktu bat edo gehiago kentzeko, markatu ezabatu nahi dituzun kontaktuen alboan dauden kontrol-laukiak eta sakatu <gui style="button">Kendu</gui>.</p>
</page>
