<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="eu">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:46" date="2024-03-05" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ikusi oinarrizko fitxategi-informazioa, ezarri baimenak eta aukeratu aplikazio lehenetsiak.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Fitxategi-propietateak</title>

  <p>To view information about a file or folder, right-click it and select
  <gui>Properties</gui>. You can also select the file and press
  <keyseq><key>Alt</key><key>Enter</key></keyseq>.</p>

  <p>The file properties window shows you information like the type of file,
  the size of the file, and when you last modified it. If you need this
  information often, you
  can have it displayed in <link xref="nautilus-list">list view columns</link>
  or <link xref="nautilus-display#icon-captions">icon captions</link>.</p>

  <p>For certain types of files, such as images and videos, there will be an
  extra <gui>Properties</gui> entry that provides information like the
  dimensions, duration, and codec.</p>

  <p>For the <gui>Permissions</gui> entry see
  <link xref="nautilus-file-properties-permissions"/>.</p>

<section id="basic">
 <title>Oinarrizko propietateak</title>
 <terms>
  <item>
    <title><gui>Izena</gui></title>
    <p>Fitxategiaren edo karpetaren izena.</p>
    <note style="tip">
      <p>Fitxategi baten izena aldatzeko, ikus <link xref="files-rename"/>.</p>
    </note>
  </item>
  <item>
    <title><gui>Mota</gui></title>
    <p>This helps you identify the type of the file, such as PDF document,
    OpenDocument Text, or JPEG image. The file type determines which
    applications can open the file, among other things. For example, you
    can’t open a picture with a music player. See <link xref="files-open"/>
    for more information on this.</p>
    <note style="tip">
      <p>To change the default application to open a file type, see <link xref="files-open#default"/>.</p>
    </note>
  </item>

  <item>
    <title>Edukiak</title>
    <p>This field is displayed if you are looking at the properties of a folder rather than a file. It helps you see the number of items in the folder.  If the folder includes other folders, each inner folder is counted as one item, even if it contains further items. Each file is also counted as one item. If the folder is empty, the contents will display <gui>nothing</gui>.</p>
  </item>

  <item>
    <title>Tamaina</title>
    <p>This field is displayed if you are looking at a file (not a folder). The size of a file tells you how much disk space it takes up. This is also an indicator of how long it will take to download a file or send it in an email (big files take longer to send/receive).</p>
    <p>Sizes may be given in bytes, KB, MB, or GB; in the case of the last three, the size in bytes will also be given in parentheses. Technically, 1 KB is 1024 bytes, 1 MB is 1024 KB and so on.</p>
  </item>

  <item>
    <title>Leku librea</title>
    <p>This is only displayed for folders. It gives the amount of disk space
    which is available on the disk that the folder is on. This is useful for
    checking if the hard disk is full.</p>
  </item>

  <item>
    <title>Karpeta gurasoa</title>
    <p>The location of each file on your computer is given by its <em>absolute
    path</em>. This is a unique “address” of the file on your computer, made up
    of a list of the folders that you would need to go into to find the file.
    For example, if Jim had a file called <file>Resume.pdf</file> in his Home
    folder, its parent folder would be <file>/home/jim</file> and its location
    would be <file>/home/jim/Resume.pdf</file>.</p>
  </item>

  <item>
    <title>Atzitze-data</title>
    <p>Fitxategia ireki zen azken aldiaren data eta ordua.</p>
  </item>

  <item>
    <title>Aldatze-data</title>
    <p>Fitxategia aldatu eta gorde zen azken aldiaren data eta ordua.</p>
  </item>

  <item>
    <title>Sortze-data</title>
    <p>Fitxategia sortu zeneko data eta ordua.</p>
  </item>
 </terms>
</section>

</page>
