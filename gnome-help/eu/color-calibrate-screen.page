<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="eu">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pantaila kalibratzea garrantzitsua da kolore zehatzak bistaratzeko.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Asier Sarasua Garmendia</mal:name>
      <mal:email>asiersarasua@ni.eus</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Nola kalibratu pantaila?</title>

  <p>Pantaila kalibratu dezakezu koloreak zehatzagoak izan daitezen. Erabilgarria izan daiteke argazkigintza digitalean, diseinuan edo artelanetan lan egiten baduzu.</p>

  <p>Kolorimetro bat edo espektrofotometro bat beharko duzu hori egiteko. Bi gailu horiek erabiltzen dira pantailak profilatzeko, baina modu desberdinean funtzionatzen dute.</p>

  <steps>
    <item>
      <p>Ziurtatu zure kalibrazio-gailua ordenagailuarekin konektatuta duzula.</p>
    </item>
    <item>
      <p>Ireki <gui xref="shell-introduction#activities">Jarduerak</gui> ikuspegia eta hasi <gui>Ezarpenak</gui> idazten.</p>
    </item>
    <item>
      <p>Egin klik <gui>Ezarpenak</gui> aukeran.</p>
    </item>
    <item>
      <p>Egin klik alboko barrako <gui>Kolorea</gui> aukeran panela irekitzeko.</p>
    </item>
    <item>
      <p>Hautatu zure pantaila.</p>
    </item>
    <item>
      <p>Sakatu <gui style="button">Kalibratu...</gui> kalibrazioari ekiteko.</p>
    </item>
  </steps>

  <p>Pantailak etengabe aldatzen dira; TFT pantaila baten atzeko argiaren distira erdira murriztu daiteke 18 hilabetetik behin eta horiago bihurtuko da zahartzean. Horrek esan nahi du pantaila berriro kalibratu beharko duzula <gui>Kolorea</gui> panelean [!] ikonoa agertzen denean.</p>

  <p>LED pantailak ere denborarekin aldatzen doaz, baina TFT pantailak baino gutxiago.</p>

</page>
