<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="hr">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Savjet o tome gdje pohraniti sigurnosne kopije i koju vrstu uređaja za pohranu koristiti.</desc>
  </info>

<title>Gdje pohraniti sigurnosnu kopiju</title>

  <p>Sigurnosne kopije datoteka trebali bi pohraniti negdje odvojeno od računala — primjerice, na vanjski čvrsti disk. Na taj način, ako se računalo pokvari, izgubi ili je ukradeno, sigurnosna kopija će i dalje biti netaknuta. Za najveću sigurnost, sigurnosnu kopiju ne biste trebali držati u istoj građevini u kojoj je vaše računalo. Ako dođe do požara ili krađe, obje kopije podataka mogu biti izgubljene ako se drže na istom mjestu.</p>

  <p>Bitno je da odaberete prikladan <em>medij sigurnosne kopije</em>. Sigurnosne kopije morate pohraniti na uređaj koji ima dovoljan kapacitet diska za sve sigurnosno kopirane datoteke.</p>

   <list style="compact">
    <title>Lokalne i udaljene mogućnosti pohrane</title>
    <item>
      <p>USB memorijska pohrana (mali kapacitet)</p>
    </item>
    <item>
      <p>Unutrašnji čvrsti disk (velik kapacitet)</p>
    </item>
    <item>
      <p>Vanjski čvrsti disk (uobičajeno veliki kapacitet)</p>
    </item>
    <item>
      <p>Mrežni disk (veliki kapacitet)</p>
    </item>
    <item>
      <p>Poslužitelj datoteka/sigurnosnih kopija (veliki kapacitet)</p>
    </item>
    <item>
     <p>Zapisivi CD ili DVD (mali/srednji kapacitet)</p>
    </item>
    <item>
     <p>Mrežna usluga sigurnosnog kopiranja (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, primjerice; kapacitet ovisi o cijeni usluge)</p>
    </item>
   </list>

  <p>Pojedine od ovih mogućnosti imaju dovoljan kapacitet da omoguće sigurnosnu kopiju svake datoteke na vašem sustavu, što je poznato kao <em>potpuno sigurnosno kopiranje sustava</em>.</p>
</page>
