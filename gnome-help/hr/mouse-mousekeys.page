<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="hr">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Omogućavanje tipki miša za upravljanje mišem pomoću brojčane tipkovnice.</desc>
  </info>

  <title>Klikanje i pomicanje pokazivača miša pomoću brojčanog dijela tipkovnice</title>

  <p>Ako imate poteškoća s korištenjem miša ili drugog pokazivačkog uređaja, možete upravljati pokazivačem miša pomoću brojčanog dijela tipkovnice. Ova značajka se zove <em>tipke miša</em>.</p>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Pristupačnost</gui>.</p>
      <p>Pregledu <gui>Aktivnosti</gui> možete pristupiti pritiskom na njega, pomicanjem pokazivača miša prema gornjem lijevom kutu zaslona, kombinacijom tipki <keyseq><key>Ctrl</key><key>Alt </key><key>Tab</key></keyseq> i pritiskom na <key>Enter</key> tipku ili pomoću tipke <key xref="keyboard-key-super">Super</key>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Pristupačnost</gui> za otvaranje panela.</p>
    </item>
    <item>
      <p>Select the <gui>Pointing &amp; Clicking</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Mouse Keys</gui> switch to on.</p>
    </item>
    <item>
      <p>Provjerite je li tipka <key>Num Lock</key> isključena. Sada ćete moći pomicati pokazivač miša pomoću tipkovnice.</p>
    </item>
  </steps>

  <p>Brojčani dio tipkovnice je skup brojčanih tipki na vašoj tipkovnici, obično raspoređenih u kvadratnu mrežu. Ako imate tipkovnicu bez brojčanog dijela tipkovnice (poput tipkovnica na prijenosnicima), možda ćete morati držati funkcijsku tipku (<key>Fn</key>) i koristiti određene druge tipke na tipkovnici kao brojčani dio tipkovnice. Ako ovu značajku često koristite na prijenosniku, možete kupiti vanjsku USB ili Bluetooth brojčanu tipkovnicu.</p>

  <p>Svaki broj na brojčanom dijelu tipkovnice odgovara smjeru. Primjerice, pritiskom na tipku <key>8</key> pokazivač će se pomaknuti prema gore, a pritiskom na tipku <key>2</key> prema dolje. Pritisnite tipku <key>5</key> za jedan klik mišem ili je brzo pritisnite dvaput za dvostruki klik.</p>

  <p>Većina tipkovnica ima posebnu tipku koja vam omogućuje desni klik, ponekad se naziva <key xref="keyboard-key-menu">Izbornik</key> tipka. Zapamtite da ova tipka reagira na to gdje je fokus vaše tipkovnice, a ne na to gdje je pokazivač miša. Pogledajte <link xref="a11y-right-click"/> za informacije o tome kako kliknuti desnom tipkom miša držeći tipku <key>5</key> ili lijevu tipku miša.</p>

  <p>Ako želite koristiti brojčani dio tipkovnice za upisivanje brojeva dok su tipke miša omogućene, uključite <key>Num Lock</key>tipku. Međutim, mišem se ne može upravljati pomoću tipkovnice kada je <key>Num Lock</key> tipka uključena.</p>

  <note>
    <p>Uobičajene brojčane tipke, u retku na vrhu tipkovnice, neće upravljati pokazivačem miša. Mogu se koristiti samo tipke brojčanog dijela tipkovnice.</p>
  </note>

</page>
