<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="hr">

  <info>
    <revision version="gnome:46" date="2024-03-10" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Odredite funkcije tipki i osjećaj pritiska Wacom olovke.</desc>
  </info>

  <title>Prilagodba olovke</title>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Wacom tablet</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Wacom tablet</gui> u bočnoj traci za otvaranje panela.</p>
      <note style="tip"><p>Ako se tablet ne otkrije, od vas će se zatražiti da <gui>Priključite ili uključite Wacom tablet</gui>. Kliknite na <gui>Bluetooth</gui> u bočnoj traci za povezivanje bežičnog tableta.</p></note>
    </item>
    <item>
      <p>Postoji odjeljak koji sadrži specifične postavke za svaku olovku, s nazivom uređaja (klasa olovke) i dijagramom na vrhu.</p>
      <note style="tip"><p>Ako olovka nije otkrivena, od vas će se zatražiti da <gui>Primaknite olovku u blizinu tableta kako bi ju podesili</gui>.</p></note>
      <p>Ove postavke se mogu prilagoditi:</p>
      <list>
        <item><p><gui>Osjećaj pritiska vrha:</gui> koristite klizač za prilagodbu "osjećaja" između <gui>mekog</gui> i <gui>tvrdog</gui>.</p></item>
        <item><p>Button/Scroll Wheel configuration (these change to
        reflect the stylus). Click the menu next to each label to select one of
        these functions: Left Mouse Button Click, Middle Mouse Button Click,
        Right Mouse Button Click, Back, or Forward.</p></item>
     </list>
    </item>
    <item>
      <p>Click <gui>Test Settings</gui> in the header bar to pop down a
      sketchpad where you can test your stylus settings.</p>
    </item>
  </steps>

</page>
