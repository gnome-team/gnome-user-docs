<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-order" xml:lang="hr">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Razvrstavanje i promjena redoslijeda ispisa.</desc>
  </info>

  <title>Ispis stranica drugačijim redoslijedom</title>

  <section id="reverse">
    <title>Preokretanje redoslijeda</title>

    <p>Pisači uobičajeno prvo ispisuju prvu stranicu, a posljednju stranicu posljednju, tako da stranice završavaju preokrenutim redoslijedom kada ih uzmete u ruke i pregledavate. Ako je potrebno, možete preokrenuti ovaj redoslijed ispisa.</p>

    <steps>
      <title>Za preokretanje redoslijeda:</title>
      <item>
        <p>Pritisnite <keyseq><key>Ctrl</key><key>P</key></keyseq> za otvaranje dijaloga ispisa.</p>
      </item>
      <item>
        <p>U kartici <gui>Općenito</gui>, pod <gui>Kopije</gui>, odaberite <gui>Preokreni</gui>. Prva će se ispisati zadnja stranica i tako dalje.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>Promjena redoslijeda</title>

  <p>Ako ispisujete više od jedne kopije dokumenta, ispisi će po zadanome biti grupiran po broju stranice (to jest, izlaze sve kopije prve stranice, zatim kopije druge stranice i tako dalje). <em>Razvrstavanje</em> će učiniti da svaka kopija izađe sa svojim stranicama grupiranim zajedno u ispravnom redoslijedu.</p>

  <steps>
    <title>Za promjenu redoslijeda:</title>
    <item>
     <p>Pritisnite <keyseq><key>Ctrl</key><key>P</key></keyseq> za otvaranje dijaloga ispisa.</p>
    </item>
    <item>
      <p>Na kartici <gui>Općenito</gui>, pod <gui>Kopije</gui>, odaberite <gui>Razvrstaj</gui>.</p>
    </item>
  </steps>

</section>

</page>
