<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="hr">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Upravljanje koje su informacijama prikazane u stupcima u prikazu popisa.</desc>
  </info>

  <title>Osobitosti popisa stupaca datoteka</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view..</p>
  <p>Click the view options button in the toolbar, pick <gui>Visible
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns.</p>


  <terms>
    <item>
      <title><gui>Naziv</gui></title>
      <p>Naziv datoteka i mapa.</p>
      <note style="tip">
        <p>Stupac <gui>Naziv</gui> se ne može sakriti.</p>
      </note>
    </item>
    <item>
      <title><gui>Veličina</gui></title>
      <p>Veličina mape dana je kao broj stavki sadržanih u mapi. Veličina datoteke navedena je u bajtovima, KB ili MB.</p>
    </item>
    <item>
      <title><gui>Vrsta</gui></title>
      <p>Prikazuje se kao mapa ili vrsta datoteke kao što je PDF dokument, JPEG slika, MP3 zvuk i ostalo.</p>
    </item>
    <item>
      <title><gui>Vlasnik</gui></title>
      <p>Ime korisnika u čijem je vlasništvu mapa ili datoteka.</p>
    </item>
    <item>
      <title><gui>Grupa</gui></title>
      <p>Grupa čiji je vlasnik datoteka. Svaki korisnik je uobičajeno u svojoj grupi, ali moguće je imati više korisnika u jednoj grupi. Primjerice, odjel može imati vlastitu grupu u radnom okruženju.</p>
    </item>
    <item>
      <title><gui>Dozvole</gui></title>
      <p>Prikazuje dozvole za pristup datoteci. Primjerice, <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>Prvi znak je vrsta datoteke. <gui>-</gui> znači obična datoteka, a <gui>d</gui> znači direktorij (mapa). U rijetkim slučajevima mogu se prikazati i drugi znakovi.</p>
        </item>
        <item>
          <p>Sljedeća tri znaka <gui>rwx</gui> određuju dozvole za korisnika koji je vlasnik datoteke.</p>
        </item>
        <item>
          <p>Sljedeća tri znaka <gui>rw-</gui> određuju dozvole za sve članove grupe koja posjeduje datoteku.</p>
        </item>
        <item>
          <p>Posljednja tri znaka u stupcu <gui>r--</gui> određuju dozvole za sve ostale korisnike na sustavu.</p>
        </item>
      </list>
      <p>Svaka dozvola ima sljedeća značenja:</p>
      <list>
        <item>
          <p><gui>r</gui>: čitanje, što znači da možete otvoriti datoteku ili mapu</p>
        </item>
        <item>
          <p><gui>w</gui>: pisanje, što znači da možete spremati promjene</p>
        </item>
        <item>
          <p><gui>x</gui>: izvršna datoteka, što znači da je možete pokrenuti ako je datoteka program ili skripta, ili možete pristupiti podmapama i datotekama ako je mapa</p>
        </item>
        <item>
          <p><gui>-</gui>: dozvola nije postavljena</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Lokacija</gui></title>
      <p>Putanja do lokacije datoteke.</p>
    </item>
    <item>
      <title><gui>Promijenjeno</gui></title>
      <p>Prikazuje datum zadnje promjene datoteke.</p>
    </item>
    <item>
      <title><gui>Promijenjeno — Vrijeme</gui></title>
      <p>Prikazuje datum i vrijeme zadnje promjene datoteke.</p>
    </item>
    <item>
      <title><gui>Pristupljeno</gui></title>
      <p>Gives the date or time of the last time the file was accessed.</p>
    </item>
    <item>
      <title><gui>Recency</gui></title>
      <p>Gives the date or time of the last time the file was accessed by the user.</p>
    </item>
    <item>
      <title><gui>Detailed Type</gui></title>
      <p>Prikazuje MIME vrstu stavke.</p>
    </item>
    <item>
      <title><gui>Created</gui></title>
      <p>Gives the date and time when the file was created.</p>
    </item>
  </terms>

</page>
