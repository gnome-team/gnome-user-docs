<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="hr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-rename-multiple"/>
    <link type="seealso" xref="files-rename-music-metadata"/>

    <revision version="gnome:47" date="2024-08-31" status="candidate"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Promjena naziva datoteke ili mape.</desc>
  </info>

  <title>Preimenovanje datoteke i mape</title>

  <p>Kao i kod drugih upravitelja datoteka, možete koristiti <app>Datoteke</app> za promjenu naziva datoteke ili mape.</p>

  <steps>
    <title>Kako bi preimenovali datoteku ili mapu:</title>
    <item><p>Desno kliknite na stavku i odaberite <gui>Rename…</gui>, ili odaberite datoteku i pritisnite <key>F2</key> tipku.</p></item>
    <item><p>Upišite novi naziv i pritisnite <key>Enter</key> tipku ili kliknite na <gui>Preimenuj</gui>.</p></item>
  </steps>

  <p>Kada preimenujete datoteku, samo prvi dio naziva datoteke je odabran, bez nastavka vrste datoteke (dio nakon zadnje <file>.</file>). Nastavak uobičajeno označava o kojoj se vrsti datoteke radi (primjerice, <file>datoteka.pdf</file> je PDF dokument) i uobičajeno to ne želite promijeniti. Ako trebate promijeniti i nastavak vrste datoteke, odaberite cijeli naziv datoteke i promijenite ga.</p>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the sidebar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

  <section id="valid-chars">
    <title>Valjani znakovi za nazive datoteka</title>

    <p>Možete koristiti bilo koji znak osim znaka <file>/</file> (kosa crta) u nazivima datoteka. Pojedini uređaji, ipak, koriste <em>datotečni sustav</em> koji ima više ograničenja za nazive datoteka. Stoga je najbolja praksa izbjegavati sljedeće znakove u nazivima datoteka: <file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>Ako datoteku imenujete s <file>.</file> kao prvim znakom, datoteka će biti <link xref="files-hidden">skrivena</link> kada je pokušate pogledati u upravitelju datoteka.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Česti problemi</title>

    <terms>
      <item>
        <title>Naziv datoteke se već koristi</title>
        <p>Ne možete imati dvije datoteke ili mape s istim nazivom u istoj mapi. Ako pokušate preimenovati datoteku u naziv koji već postoji u mapi u kojoj radite, upravitelj datoteka to neće dopustiti.</p>
        <p>Nazivi datoteka i mapa razlikuju velika i mala slova, tako da naziv datoteke <file>Datoteka.txt</file> nije isti kao <file>DATOTEKA.txt</file>. Korištenje različitih naziva datoteka poput ovog je dopušteno, iako nije preporučljivo.</p>
      </item>
      <item>
        <title>Naziv datoteke je predugačak</title>
        <p>Na pojedinim datotečnim sustavima nazivi datoteka ne smiju imati više od 255 znakova u nazivima. Ovo ograničenje od 255 znakova uključuje naziv i putanju datoteke (primjerice, <file>/home/goran/Dokumenti/posao/poslovni-prijedlozi/…</file>), stoga biste trebali izbjegavati duge nazive datoteka i mapa gdje je to moguće.</p>
      </item>
      <item>
        <title>Mogućnost preimenovanja je zasivljena</title>
        <p>Ako je <gui>Preimenuj…</gui> mogućnost zasivljena, nemate dozvolu za preimenovanje datoteke. Trebali bi biti oprezni s preimenovanjem takvih datoteka, jer preimenovanje pojedinih zaštićenih datoteka može uzrokovati nestabilnost vašeg sustava. Pogledajte <link xref="nautilus-file-properties-permissions"/> za više informacija.</p>
      </item>
    </terms>

  </section>

</page>
