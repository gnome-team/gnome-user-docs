<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="hr">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Spremanje dokumenta kao PDF, PostScript ili SVG datoteke umjesto slanja na pisač.</desc>
  </info>

  <title>Ispis u datoteku</title>

  <p>Možete odabrati ispis dokumenta u datoteku umjesto slanja na ispis s pisača. Ispisom u datoteku stvorit će se <sys>PDF</sys>, <sys>PostScript</sys> ili <sys>SVG</sys> datoteka koja sadrži dokument. Ovo može biti korisno ako želite prenijeti dokument na drugo računalo ili ga podijeliti s nekim.</p>

  <steps>
    <title>Za ispis u datoteku:</title>
    <item>
      <p>Otvorite dijalog ispisa pritiskom na <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Select <gui>Print to File</gui> in the
      <gui style="tab">General</gui> tab.</p>
    </item>
    <item>
    <!-- "Save" button in GTK4, "Select" button in GTK3 -->
      <p>To change the default filename and where the file is saved to, click
      the filename below the printer selection. Click
      <gui style="button">Select</gui> or <gui style="button">Save</gui>
      once you have finished choosing.</p>
    </item>
    <item>
      <p><sys>PDF</sys> je zadana vrsta datoteke za dokument. Ako želite koristiti drugačiji <gui>format dokumenta</gui>, odaberite <sys>Postskripta</sys> ili <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Po potrebi prilagodite druge postavke stranice.</p>
    </item>
    <item>
      <p>Kliknite na <gui style="button">Ispis</gui> za spremanje datoteke.</p>
    </item>
  </steps>

</page>
