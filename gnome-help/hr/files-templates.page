<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="hr">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Brzo stvaranje novih dokumenata iz prilagođenih predložaka datoteka.</desc>
  </info>

  <title>Predlošci za najčešće korištene vrste dokumenata</title>

  <p>Ako često stvarate dokumente na temelju istog sadržaja, možda će vam koristiti predlošci datoteka. Predložak datoteke može biti dokument bilo koje vrste u formatu ili sa sadržajem koji želite ponovno upotrijebiti. Primjerice, možete stvoriti predložak dokumenta sa svojim zaglavljem.</p>

  <steps>
    <title>Stvaranje novog predloška</title>
    <item>
      <p>Stvorite dokument koji ćete koristiti kao predložak. Primjerice, možete napraviti svoje zaglavlje u aplikaciji za obradu teksta.</p>
    </item>
    <item>
      <p>Spremite datoteku sa sadržajem predloška u <file>Predlošci</file> mapu koja se nalazi unutar <file>Osobne mape</file>. Ako mapa <file>Predlošci</file> ne postoji, morati ćete ju prvo stvoriti.</p>
    </item>
  </steps>

  <steps>
    <title>Korištenje predloška za stvaranje dokumenta</title>
    <item>
      <p>Otvorite mapu u koju želite smjestiti novi dokument.</p>
    </item>
    <item>
      <p>Desno kliknite bilo gdje na prazni prostor u mapi, zatim odaberite <gui style="menuitem">Novi dokument</gui>. Nazivi dostupnih predložaka bit će navedeni u podizborniku.</p>
    </item>
    <item>
      <p>Odaberite željeni predložak s popisa.</p>
    </item>
    <item>
      <p>Dvostruko kliknite na datoteku za otvaranje i početak uređivanja. Možda ćete poželjeti <link xref="files-rename">preimenovati datoteku</link> kada završite.</p>
    </item>
  </steps>

</page>
