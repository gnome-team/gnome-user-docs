<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="hr">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>
    <revision version="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Promjena svjetline zaslona kako bi bio čitljiviji pri jakom svjetlu.</desc>
  </info>

  <title>Postavljanje svjetline zaslona</title>

  <p>Ovisno o hardveru, možete promijeniti svjetlinu zaslona u svrhu štednje energije ili da učinite zaslon čitljivijim pri jakom svjetlu.</p>

  <p>Za promjenu svjetline zaslona kliknite <gui xref="shell-introduction#systemmenu">izbornik sustava</gui> na desnoj strani gornje trake i prilagodite klizač svjetline zaslona na željenu vrijednost. Promjena bi se trebala odmah primijeniti.</p>

  <note style="tip">
    <p>Mnoge tipkovnice prijenosnika imaju posebne tipke za prilagodbu svjetline. Često imaju sliku koja izgleda kao sunce. Držite pritisnutu tipku <key>Fn</key> za korištenje tih tipki.</p>
  </note>

  <note style="tip">
    <p>Ako vaše računalo ima integrirani senzor svjetla, svjetlina zaslona automatski će se prilagoditi. Za više informacija pogledajte <link xref="power-autobrightness"/>.</p>
  </note>

  <p>Ako je moguće postavljanje svjetline zaslona, možete postaviti automatsko zatamnjenje zaslona u svrhu uštede energije. Za više informacija pogledajte <link xref="power-whydim"/>.</p>

</page>
