<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-whydim" xml:lang="hr">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="41" date="2021-09-08" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zaslon se zatamnjuje ako je računalo neaktivno u svrhu štednje energije.</desc>
  </info>

  <title>Zašto se zaslon zatamni nakon određenog vremena?</title>

  <p>Ako zaslon ima mogućnost postavljanja svjetline, on će se zatamniti kada je računalo neaktivno u svrhu štednje energije. Kada ponovno počnete koristiti računalo, zaslon će se posvijetliti.</p>

  <p>Za zaustavljanje zatamnjenja zaslona:</p>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Energija</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Energija</gui> u bočnoj traci za otvaranje panela.</p>
    </item>
    <item>
      <p>Isključite preklopnik <gui>Zatamni zaslon</gui> u odjeljku <gui>Mogućnosti štednje energije</gui>.</p>
    </item>
  </steps>

  <p>The screen will always dim, and dim more aggressively when choosing
  the “Power Saver” power mode. If you donʼt want the screen to dim at all,
  select the “Balanced” power mode instead.</p>

</page>
