<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="hr">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zaštita računa čestim mijenjanjem lozinke u postavkama računa.</desc>
  </info>

  <title>Promjena lozinke</title>

  <p>Dobro je svojevremeno promijeniti lozinku, osobito ako mislite da netko drugi zna vašu lozinku.</p>

  <p>Potrebne su vam <link xref="user-admin-explain">administratorske ovlasti</link> za uređivanje korisničkih računa koji nisu vaši.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Kliknite na oznaku <gui>·····</gui> pokraj <gui>Lozinka</gui>. Ako mijenjate lozinku za drugog korisnika, prvo ćete morati <gui>Otključati</gui> panel i odabrati račun pod <gui>Drugi korisnici</gui>.</p>
    </item>
    <item>
      <p>Upišite svoju trenutnu lozinku, a zatim novu lozinku. Ponovno upišite svoju novu lozinku u polje <gui>Potvrda nove lozinke</gui>.</p>
      <p>Možete pritisnuti <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">stvaranje lozinke</span></media></gui> ikonu za automatsko stvaranje naizmjenične lozinke.</p>
    </item>
    <item>
      <p>Kliknite na tipku <gui>Promijeni</gui>.</p>
    </item>
  </steps>

  <p>Pobrinite ste da ste <link xref="user-goodpassword">odabrali dobru lozinku</link>. To će vam pomoći da zaštitite svoj korisnički račun.</p>

  <note>
    <p>Kada nadopunite svoju lozinku prijave, vaša lozinka skupa ključeva prijave automatski će se nadopunti kako bi bila ista kao vaša nova lozinka prijave.</p>
  </note>

  <p>Ako zaboravite svoju lozinku, svaki korisnik s administratorskim ovlastima može je promijeniti umjesto vas.</p>

</page>
