<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="hr">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="shell-overview#apps"/>

    <!--
    Recommended statuses: stub incomplete draft outdated review candidate final
    -->
    <revision version="gnome:40" date="2021-03-12" status="candidate"/>
    <!--
    FIXME: I'm tentatively marking this final for GNOME 40, because it's at
    least no longer incorrect. But here's a lot to improve:

    * I'm not super happy with the "Other" catchall section at the end, but I also
      don't want to add lots of singleton sections. Tweak the presentation.

    * gnome-shell references network-wireless-disconnected but it doesn't exist:
      https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3827

    * The icons for disconnected states might change:
      https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/issues/102

    * topbar-audio-volume-overamplified: Write docs on overamplification:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/117

    * Write docs on setting mic sensitivity, and link in a learn more item:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/116

    * topbar-network-wireless-connected: We're super handwavy about when this is
      used. We could use some docs on ad hoc networks.

    * topbar-screen-shared: We have no docs on the screen share portal:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/118

    * topbar-thunderbolt-acquiring: We have no docs on Thunderbolt:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/119
    -->

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Objašnjava značenje ikona koje se nalaze na desnoj strani gornje trake.</desc>
  </info>

  <title>Što znače ikone na gornjoj traci?</title>

<p>Ova stranica objašnjava značenje ikona smještenih na gornjem desnom kutu zaslona. Točnije, opisane su različite varijacije ikona koje pruža sustav.</p>

<links type="section"/>


<section id="universalicons">
<title>Ikone pristupačnosti</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-accessibility.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-accessibility.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Omogućuje brzo isklj/uklj raznih mogućnosti pristupačnosti.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-pointer.svg"/>
        </if:when>
        <media src="figures/topbar-pointer.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje vrstu klika koji će se dogoditi kada koristite lebdeći klik.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="a11y">Saznajte više o pristupačnosti.</link></p></item>
  <item><p><link xref="a11y-dwellclick">Saznajte više o lebdećem kliku.</link></p></item>
</list>
</section>


<section id="audioicons">
<title>Ikone zvuka</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume.svg"/>
        </if:when>
        <media src="figures/topbar-audio-volume.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje glasnoću zvuka zvučnika ili slušalica.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-audio-volume-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Zvučnici ili slušalice su utišani.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity.svg"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje osjetljivost mikrofona.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Mikrofon je utišan.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="sound-volume">Saznajte više o glasnoći zvuka.</link></p></item>
</list>
</section>


<section id="batteryicons">
<title>Ikone baterije</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-charging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-charging.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje razinu energije baterije pri punjenju baterije.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100-charged.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100-charged.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Baterija je potpuno napunjena i priključena je na izvor napajanja.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-discharging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-discharging.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje razinu energije baterije pri pražnjenju baterije.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Baterija je potpuno napunjena i nije priključena na izvor napajanja.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-system-shutdown.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-system-shutdown.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Ikona isključivanja prikazana na računalima bez baterije.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="power-status">Saznajte više o stanju baterije.</link></p></item>
</list>
</section>


<section id="bluetoothicons">
<title>Ikone Bluetootha</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg"/>
      </if:choose>
    </td>
    <td><p>Zrakoplovni način rada je uključen. Bluetooth je onemogućen kada je zrakoplovni način rada uključen.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-bluetooth-active.svg"/>
        </if:when>
        <media src="figures/topbar-bluetooth-active.svg"/>
      </if:choose>
    </td>
    <td><p>Bluetooth uređaj je uparen i koristi se. Ova ikona je samo prikazana kada postoji aktivan uređaj, a ne i kada je Bluetooth samo omogućen.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Saznajte više o zrakoplovnom načinu rada.</link></p></item>
  <item><p><link xref="bluetooth">Saznajte više o Bluetoothu.</link></p></item>
</list>
</section>


<section id="networkicons">
<info>
<!--
FIXME: I don't want a bare desc, because it ends up in the section links above.
But this section also gets a seealso from net-wireless.page, and we'd like a
desc for that. In Mallard 1.2, we can use role on desc. It works in Yelp, but
it's not in a schema yet, so it will cause validation errors in CI.
  <desc type="link" role="seealso">Explains the meanings of the networking icons in the top bar.</desc>
-->
</info>
<title>Ikone mreže</title>

<table shade="rows">
  <title>Bežično (Wi-Fi) umrežavanje</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Zrakoplovni način rada je uključen. Bežična mreža je onemogućena kada je zrakoplovni način rada uključen.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezivanje s bežičnom mrežom.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-signal.svg"/>
        </if:when>
        <media src="figures/topbar-network-wireless-signal.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje jačinu povezivanja bežične mreže.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-signal-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-signal-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezano s bežičnom mrežom, ali nema signala.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezano s bežičnom mrežom. Ova ikona je prikazana samo ako se ne može odrediti jačina signala, kao kada se povezujete sa ad hoc mrežama.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-no-route.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-no-route.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezano s bežičnom mrežom, ali nema rute prema internetu. Ovo može biti uzrokovano pogrešnim podešavanjem vaše mreže ili zbog prekida rada vašeg pružatelja internetskih usluga.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Saznajte više o zrakoplovnom načinu rada.</link></p></item>
  <item><p><link xref="net-wireless-connect">Saznajte više o bežičnom umrežavanju.</link></p></item>
</list>

<table shade="rows">
  <title>Mobilno umrežavanje (mobilni pristup internetu)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Zrakoplovni način rada je uključen. Mobilna mreža je onemogućena kada je zrakoplovni način rada uključen.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezivanje s mobilnom mrežom.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal.svg"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal.svg"/>
      </if:choose>
    </td>
    <td><p>Označuje jačinu povezivanja mobilne mreže.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezano s mobilnom mrežom, ali nema signala.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezano s mobilnom mrežom. Ova ikona je prikazana samo ako se ne može odrediti jačina signala, kao kada se povezujete putem Bluetootha. Ako se jačina signala može odrediti, tada se prikazuje ikona jačine signala.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Saznajte više o zrakoplovnom načinu rada.</link></p></item>
  <item><p><link xref="net-mobile">Saznajte više o mobilnom umrežavanju.</link></p></item>
</list>

<table shade="rows">
  <title>Žično umrežavanje</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Povezivanje s žičnom mrežom.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired.svg"/>
      </if:choose>
    </td>
    <td><p>Povezano s žičnom mrežom.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-disconnected.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-disconnected.svg"/>
      </if:choose>
    </td>
    <td><p>Prekinuto povezivanje s žičnom mrežom.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-no-route.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-no-route.svg"/>
      </if:choose>
    </td>
    <td><p>Povezano s žičnom mrežom, ali nema rute prema internetu. Ovo može biti uzrokovano pogrešnim podešavanjem vaše mreže ili zbog prekida rada vašeg pružatelja internetskih usluga.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wired-connect">Saznajte više o žičnom umrežavanju.</link></p></item>
</list>

<table shade="rows">
  <title>VPN (virtualna privatna mreža)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Povezivanje s virtualnom privatnom mrežom.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn.svg"/>
      </if:choose>
    </td>
    <td><p>Povezano s virtualnom privatnom mrežom.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-vpn-connect">Saznajte više o virtualnim privatnim mrežama.</link></p></item>
</list>

</section>


<section id="othericons">
<title>Ostale ikone</title>
<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-input-method.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-input-method.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Označuje raspored tipkovnice ili način unosa koji se trenutno koristi. Kliknite na željeni raspored tipkovnice. Izbornika rasporeda tipkovnice samo se prikazuje ako imate više podešenih načina unosa.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-find-location.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-find-location.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Aplikacija trenutno pristupa vašoj lokaciji. Lociranje možete onemogućiti iz izbornika.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-night-light.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-night-light.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Noćno osvjetljenje promijenilo je temperaturu boje zaslona kako bi se smanjilo naprezanje očiju. Možete privremeno onemogućiti noćno osvjetljenje iz izbornika.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-media-record.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-media-record.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Terenutno snimanje cijelog zaslona.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-screen-shared.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-screen-shared.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Aplikacija trenutno dijeli zaslon ili drugi prozor.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-thunderbolt-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-thunderbolt-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Povezivanje s Thunderbolt uređajem, poput doka.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="keyboard-layouts">Saznajte više o rasporedima tipkovnice.</link></p></item>
  <item><p><link xref="privacy-location">Saznajte više o privatnosti i uslugama lociranja.</link></p></item>
  <item><p><link xref="display-night-light">Saznajte više o noćnom osvjetljenju i tonu boje.</link></p></item>
  <item><p><link xref="screen-shot-record">Saznajte više o slikanju i snimanju zaslona.</link></p></item>
</list>

</section>

</page>
