<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="hr">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pojedini bežični uređaji imaju problema s rukovanjem kada je računalo suspendirano i ne nastavljaju pravilno s radom.</desc>
  </info>

  <title>Nema bežične mreže kada se računalo probudi</title>

  <p>Ako ste suspendirali računalo, možda ćete ustanoviti kako vaša bežična mreža ne radi kada je ponovno pokrenete. To se događa kada <link xref="hardware-driver">upravljački program</link> za bežični uređaj ne podržava u potpunosti određene značajke uštede energije. Uobičajeno se bežična mreža ne uspije pravilno uključiti kada računalo nastavi s radom.</p>

  <p>Ako se to dogodi, pokušajte isključiti bežičnu mrežu i zatim ju ponovno uključiti:</p>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Bežična mreža</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Bežična mreža</gui> za otvaranje panela.</p>
    </item>
    <item>
      <p>Isključite preklopnik <gui>Bežična mreža</gui> u gornjem desnom kutu prozora i zatim ga ponovno uključite.</p>
    </item>
    <item>
      <p>Ako bežična mreža i dalje ne radi, uključite preklopnik <gui>Način rada u zrakoplovu</gui> i zatim ga ponovno isključite.</p>
    </item>
  </steps>

  <p>Ako ovo ne uspije, ponovnim pokretanjem računala bi bežična mreža ponovno trebala raditi. Ako i nakon toga imate problema, priključite se s internetom pomoću žičnog kabla i nadopunite svoje računalo.</p>

</page>
