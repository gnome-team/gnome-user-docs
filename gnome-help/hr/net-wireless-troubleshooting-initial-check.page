<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="hr">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Suradnici wiki dokumentacije za Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Provjerite jesu li jednostavne mrežne postavke ispravne i pripremite se za sljedećih nekoliko koraka za rješavanje problema.</desc>
  </info>

  <title>Rješavanje problema bežične mreže</title>
  <subtitle>Obavljanje početne provjere povezivanja</subtitle>

  <p>U ovom koraku provjerit ćete neke osnovne informacije vašeg bežičnog mrežnog povezivanja. Ovaj korak se obavlja kako bi bili sigurni da vaš problem s umrežavanjem nije uzrokovan relativno jednostavnim problemom, kao primjerice, bežično povezivanje je isključeno, te kako bi se pripremili za sljedećih nekoliko koraka za rješavanje problema.</p>

  <steps>
    <item>
      <p>Provjerite nije li vaše prijenosno računalo povezano s <em>žičnim</em> internetskim povezivanjem.</p>
    </item>
    <item>
      <p>Ako imate vanjski bežični uređaj (poput USB uređaja ili PCMCIA kartice koja se priključuje u prijenosno računalo), provjerite je li čvrsto umetnut u odgovarajući utičnicu na vašem računalu.</p>
    </item>
    <item>
      <p>Ako je vaša bežična kartica ugrađena <em>unutar</em> vašeg računala, provjerite je li prekidač bežičnog povezivanja uključen (ako ga ima). Prijenosna računala često imaju prekidače bežičnog povezivanja koje možete uključiti pritiskom na kombinaciju tipki na tipkovnici.</p>
    </item>
    <item>
      <p>Otvorite <gui xref="shell-introduction#systemmenu">izbornik sustava</gui> s desne strane gornje trake i odaberite Bežična mreža, zatim odaberite <gui>Postavke bežične mreže</gui>. Provjerite je li preklopnik <gui>Bežična mreža</gui> uključen. Isto tako provjerite da <link xref="net-wireless-airplane">Način rada u zrakoplovu</link> <em>nije</em> uključen.</p>
    </item>
    <item>
      <p>Otvorite Terminal, upišite <cmd>nmcli device</cmd> i pritisnite <key>Enter</key> tipku.</p>
      <p>Ovo će prikazati informacije o vašim mrežnim sučeljima i stanju povezivanja. Pogledajte dolje na popisu informacija i provjerite postoji li stavka povezana s uređajem bežične mreže. Ako je stanje <code>povezan</code>, to znači da uređaj radi i povezan je s vašim bežičnim usmjernikom.</p>
    </item>
  </steps>

  <p>Ako ste povezani na bežični usmjernik, ali još uvijek ne možete pristupiti internetu, vaš usmjernik možda nije ispravno podešen ili vaš pružatelj internetskih usluga (ISP) možda ima nekih tehničkih problema. Pregledajte vodiče za podešavanje usmjerivača i pružatelja internetskih usluga (ISP)  kako bi bili sigurni da su postavke ispravne ili se obratite svom pružatelj internetskih usluga (ISP) za podršku.</p>

  <p>Ako informacije naredbe <cmd>nmcli device</cmd> ne ukazuju na to da ste povezani s mrežom, kliknite <gui>Sljedeće</gui> za nastavak na sljedeći dio vodiča za rješavanje problema.</p>

</page>
