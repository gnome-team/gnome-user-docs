<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="hr">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Korištenje postavki <gui>Datum i vrijeme</gui> za promjenu datuma ili vremena.</desc>
  </info>

<title>Promjena datuma i vremena</title>

  <p>Ako su datum i vrijeme prikazani na gornjoj traci netočni ili u pogrešnom formatu, možete ih promijeniti:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Date &amp; Time</gui> to open the panel.</p>
    </item>
    <item>
      <p>Ako imate uključen preklopnik <gui>Automatski datum i vrijeme</gui>, vaš datum i vrijeme se automatski nadopune s interneta. Za ručno postavljanje datuma i vremena, isključite preklopnik.</p>
    </item> 
    <item>
      <p>Kliknite na <gui>Datum i vrijeme</gui>, zatim ih prilagodite.</p>
    </item>
    <item>
      <p>Možete promijeniti način prikaza sata odabirom između <gui>24-satni</gui> ili <gui>12-satni</gui> <gui>Format vremena</gui>.</p>
    </item>
  </steps>

  <p>Možete i <link xref="clock-timezone">ručno postaviti vremensku zonu</link>.</p>

</page>
