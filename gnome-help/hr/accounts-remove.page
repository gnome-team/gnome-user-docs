<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="hr">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uklanjanje računa pružatelja mrežnih usluga iz aplikacija.</desc>
  </info>

  <title>Uklanjanje računa</title>

  <p>Možete ukloniti mrežni račun koji više ne želite koristiti.</p>

  <note style="tip">
    <p>Mnogi pružatelji mrežnih usluga pružaju token ovjere kojeg vaše računalo pohranjuje umjesto lozinke. Ako uklonite račun, trebali bi opozvati i tu vjerodajnicu u mrežnoj usluzi. To će osigurati da se druga aplikacija ili web stranica može povezati s tom uslugom pomoću ovjere za vašu radnu površinu.</p>

    <p>Način opoziva ovjere ovisi o pružatelju usluga. Provjerite svoje postavke na web stranici pružatelja usluga za ovjerene ili povezane aplikacije ili web stranice. Potražite aplikaciju naziva "GNOME" i uklonite ju.</p>
  </note>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Mrežni računi</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Mrežni računi</gui> u bočnoj traci za otvaranje panela.</p>
    </item>
    <item>
      <p>Odaberite račun koji želite ukloniti.</p>
    </item>
    <item>
      <p>Kliknite na <gui>-</gui> tipku u donjem lijevom kutu prozora.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Ukloni</gui> u dijalogu potvrde.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Umjesto potpunog brisanja računa, moguće je <link xref="accounts-disable-service">ograničiti usluge</link> kojima pristupa vaša radna površina.</p>
  </note>
</page>
