<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="hr">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Pojedini računalni hardver uzrokuje probleme sa suspenzijom.</desc>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Zašto se računalo ponovno ne uključuje nakon suspenzije?</title>

<p>Ako <link xref="power-suspend">suspendirate</link> računalo, a zatim ga pokušate ponovno pokrenuti, možda neće raditi očekivano. Uzrok tome može biti hardver koji pravilno ne podržava suspenziju.</p>

<section id="resume">
  <title>Računalo je suspendirano i ne nastavlja s radom</title>
  <p>Ako suspendirate računalo i zatim pritisnete tipku ili kliknete mišem, računalo bi se trebalo probuditi i prikazati zaslon na kojem se traži vaša lozinka. Ako se to ne dogodi, pokušajte pritisnuti tipku isključivanja (ne držite ju, samo ju pritisnite jednom).</p>
  <p>Ako ovo i dalje ne pomaže, provjerite je li monitor računala uključen i pokušajte ponovno pritisnuti tipku na tipkovnici.</p>
  <p>U konačnici, isključite računalo držeći tipku isključivanja 5-10 sekundi, iako ćete time izgubiti sav nespremljeni rad. Tada bi trebali ponovno moći uključiti računalo.</p>
  <p>Ako se to dogodi svaki puta kada suspendirate računalo, najvjerojatnije značajka suspenzije ne radi s vašim hardverom.</p>
  <note style="warning">
    <p>Ako vaše računalo gubi energiju i nema alternativno napajanje (poput ispravne baterije), ono će se isključiti.</p>
  </note>
</section>

<section id="hardware">
  <title>Bežična mreža (ili drugi hardver) ne radi kada probudim računalo</title>
  <p>Ako suspendirate računalo i zatim ga ponovno pokrenete, i ustanovite da vaše internetsko povezivanje , miš ili neki drugi uređaj ne rade ispravno. Uzrok tome može biti što upravljački program za uređaj prikladno ne podržava suspenziju. Ovo je <link xref="hardware-driver">problem s upravljačkim programom</link>, a ne samim uređajem.</p>
  <p>Ako uređaj ima prekidač za napajanje, pokušajte ga isključiti i zatim ponovno uključiti. U većini slučajeva uređaj će ponovno početi raditi. Ako se povezuje putem USB kabla ili slično, odspojite uređaj, zatim ga ponovno priključite i provjerite radi li.</p>
  <p>Ako ne možete isključiti ili odspojiti uređaj, ili ako to ne radi, možda morate ponovno pokrenuti računalo kako bi uređaj ponovno počeo raditi.</p>
</section>

</page>
