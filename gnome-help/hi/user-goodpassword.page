<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="hi">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ग्नोम प्रलेखन परियोजना</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>फील् बूल्</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>टैफ्फिनी अंटोपोलस्की</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ज्यादा कठिन और लम्बे कूट-शब्द का इस्तेमाल करे।</desc>
  </info>

  <title>सुरक्षित कूट-शब्द का चुनाव करे। </title>

  <note style="important">
    <p>आपका कूट-शब्द आपके लिए आसान पर दूसरों के लिए (कम्प्यूटर अनुप्रयोग के लिए भी) अत्यधिक कठिन होना चाहिए।</p>
  </note>

  <p>अच्छे कूट-शब्द का चुनाव आपके कम्प्यूटर को सुरक्षित रखता है। अगर आप के कूट-शब्द का आसानी से अंदाज़ा लगाया जा सकता है तो कोई और व्यक्ति उसके जरिए आपकी निजी जानकारी को हासिल कर सकता है।</p>

  <p>यहाँ तक की कई व्यक्ति आपने कम्प्यूटर को व्यवस्थित ढंग से इस्तेमाल कर के भी आप के कूट-शब्द का अनुमान लगा सकते है। तो जो कूट-शब्द व्यक्तियों के लिए आसान है उसी कूट-शब्द का कम्प्यूटर द्वारा आसानी से अंदाजा लगाया जा सकता है। यहाँ कुछ सुझाव दिए जा रहे जिस की मदद से आप एक अच्छा कूट-शब्द चुन सकते है।</p>
  
  <list>
    <item>
      <p>Use a mixture of upper-case and lower-case letters, numbers, symbols
      and spaces in the password. This makes it more difficult to guess; there
      are more symbols from which to choose, meaning more possible passwords
      that someone would have to check when trying to guess yours.</p>
      <note>
        <p>A good method for choosing a password is to take the first letter of
        each word in a phrase that you can remember. The phrase could be the
        name of a movie, a book, a song or an album. For example, “Flatland: A
        Romance of Many Dimensions” would become F:ARoMD or faromd or f:
        aromd.</p>
      </note>
    </item>
    <item>
      <p>अपना कूट-शब्द अत्यधिक लम्बा बनाए। उसमे जितने अधिक अक्षरों का इस्तेमाल होगा, उतना ही किसी व्यक्ति या कम्प्यूटर के द्वारा उसका अंदाजा लगाना कठिन होगा।</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Do not use any personal information such as a date, license plate
      number, or any family member’s name.</p>
    </item>
    <item>
      <p>संज्ञा का इस्तेमाल ना करे।</p>
    </item>
    <item>
      <p>साथ-ही किसी ऐसे कूट-शब्द का इस्तेमाल करे जिस को जल्दी से टाइप किया जा सके जिससे की उसे टाइप करते वक्त कोई और ना देख सके।</p>
      <note style="tip">
        <p>कभी अपने कूट-शब्द को कही लिख कर ना रखे। उन्हे आसानी से ढूंढा जा सकता है!</p>
      </note>
    </item>
    <item>
      <p>अलग-अलग चीज़ो/कामों के लिए भिन्न-भिन्न कूट-शब्द का इस्तेमाल करे। </p>
    </item>
    <item>
      <p>अलग-अलग खातों के लिए भिन्न-भिन्न कूट-शब्द का इस्तेमाल करे। </p>
      <p>अगर आप एक ही कूट-शब्द का इस्तेमाल हर के खाते के लिए करते है तो, जो भी उसका अंदाजा लगाएगा, उसकी पहुँच आपके सभी खातों तक होगी।</p>
      <p>It can be difficult to remember lots of passwords, however. Though not
      as secure as using a different passwords for everything, it may be easier
      to use the same one for things that do not matter (like websites), and
      different ones for important things (like your online banking account and
      your email).</p>
   </item>
   <item>
     <p>अपने कूट-शब्द को निरंतर बदलते रहे।  </p>
   </item>
  </list>

</page>
