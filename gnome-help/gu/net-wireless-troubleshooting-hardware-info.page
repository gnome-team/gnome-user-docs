<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="gu">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu દસ્તાવેજીકરણ વિકિ માટે ફાળકો</name>
    </credit>
    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમને વિગતોની જરૂર પડી શકે છે જેમ કે મુશ્કેલીનિવારણ તબકકામાં તમારા વાયરલેસ ઍડપ્ટરનું મોડલ નંબર.</desc>
  </info>

  <title>વાયરલેસ નેટવર્ક મુશ્કેલીનિવારક</title>
  <subtitle>તમારાં નેટવર્ક હાર્ડવેર વિશે જાણકારીને ભેગી કરો</subtitle>

  <p>આ તબક્કામાં, તમે તમારાં વાયરલેસ નેટવર્ક ઉપકરણ વિશે જાણકારીને સંગ્રહશો. તમે જે રીતે વાયરલેસ સમસ્યાને સુધારો છે તે વાયરલેસ ઍડપ્ટરનાં મેક અને મોડલ પર આધાર રાખે છે, તેથી તમે આ વિગતોની નોંધને બનાવવાની જરૂર પડશે. તે અમુક વસ્તુઓને રાખવા માટે પણ ઉપયોગી થઇ શકે છે કે જે તમારાં કમ્પ્યૂટર સાથે પણ આવ્યુ હતુ, જેમ કે ઉપકરણ ડ્રાઇવર સ્થાપન ડિસ્ક. નીચેની વસ્તુઓને જુઓ જો તેઓ હજુ તમારી પાસે હોય:</p>

  <list>
    <item>
      <p>તમારાં વાયરલેસ ઉપકરણો માટે પેકેજીંગ અને સૂચનાઓ (ખાસ કરીને તમારાં રાઉટર માટે વપરાશકર્તા માર્ગદર્શિકા)</p>
    </item>
    <item>
      <p>ડિસ્ક તમારાં વાયરલેસ ઍડપ્ટર માટે ડ્રાઇવરોને સમાવી રહ્યુ છે (જો તે ફક્ત વિન્ડો ડ્રાઇવરને સમાવી રહ્યુ હોય તો પણ)</p>
    </item>
    <item>
      <p>The manufacturers and model numbers of your computer, wireless adapter
      and router. This information can usually be found on the
      underside or reverse of the device.</p>
    </item>
    <item>
      <p>Any version or revision numbers that may be printed on your wireless
      network devices or their packaging. These can be especially helpful, so
      look carefully.</p>
    </item>
    <item>
      <p>Anything on the driver disc that identifies either the device itself,
      its “firmware” version, or the components (chipset) it uses.</p>
    </item>
  </list>

  <p>જો શક્ય હોય તો, વૈકલ્પિક કામ કરવાનું જોડાણને વાપરવાનો પ્રયત્ન કરો તેથી તે તમે સોફ્ટવેર અને ડ્રાઇવરોને ડાઉનલોડ કરી શકો છો જો જરૂરી હોય તો (ઇથરનેટ નેટવર્ક કેબલ સાથે રાઉટરમાં સીધા કમ્પ્યૂટર સાથે પ્લગ કરવું આને પૂરુ પાડવાનો એક રસ્તો છે, પરંતુ તે ફક્ત તે તેમાં પ્લગ થાય છે જ્યારે તમને જરૂરી હોય.)</p>

  <p>એકવાર તમારી પાસે શક્ય હોય તેટલી મોટાભાગની વસ્તુઓ હોય તો, <gui>આગળ</gui> પર ક્લિક કરો.</p>

</page>
