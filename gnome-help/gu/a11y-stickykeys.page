<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="gu">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>એકવાર બધી કીઓને અટકાવી રાખવી તેનાં કરતા એક જ સમયે કિબોર્ડ ટૂંકાણ એક કીને ટાઇપ કરો.</desc>
  </info>

  <title>સ્ટીકી કી ચાલુ કરો</title>

  <p><em>સ્ટીકી કી</em> એ એકજ સમયે બધી કીને પકડી રાખ્યા વગર એક જ સમયે કિબોર્ડ ટૂંકાણ એક કીને ટાઇપ કરવા માટે તમને પરવાનગી આપે છે. ઉદાહરણ તરીકે, <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> ટૂંકાણ વિન્ડો વચ્ચે બદલાય છે. સ્ટીકી કી ચાલુ કર્યા વગર, તમને એજ સમયે તમારે બંને કીને પકડવુ જ પડશે; ચાલુ થયેલ સ્ટીકી કી સાથે, તમારે <key>Super</key> કીને દબાવવવુ પડશે અને એજ કરવા માટે પછી <key>Tab</key> ને દબાવો.</p>

  <p>તમે સ્ટીકી કીને ચાલુ રાખી શકો છો જો તમે એકવાર ઘણી કીને પકડવાની મુશ્કેલી થતી હોય.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the
      <gui>Sticky Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>ઝડપથી સ્ટીકી કીને ચાલુ અને બંધ કરો</title>
    <p>Switch the <gui>Enable by Keyboard</gui> switch
    to turn sticky keys on and off from the keyboard. When this option is
    selected, you can press <key>Shift</key> five times in a row to enable or
    disable sticky keys.</p>
    <p>You can also turn sticky keys on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> on the top bar and
    selecting <gui>Sticky Keys</gui>. The accessibility icon is visible when
    one or more settings have been enabled from the <gui>Accessibility</gui>
    panel.</p>
  </note>

  <p>જો તમે એકવખતમાં જ બે કીને દબાવો તો, તમે સામાન્ય રીતે જ કિબોર્ડ ટૂંકાણને દાખલ કરવા દેવા માટે કામચલાઉ રીતે પોતાની જાતે સ્ટીકી કીને ચાલુ કરી શકાય છે.</p>

  <p>ઉદાહરણ તરીકે, જો તમે સ્ટીકી કી ચાલુ કરી હોય તો પરંતુ <key>Super</key> અને <key>Tab</key> સાથે દબાવો, સ્ટીકી કી બીજી કીને દબાવવા માટે તમારી માટે રાહ જોશે નહિં જો તમારી પાસે આ વિકલ્પ ચાલુ હોય તો. તે રાહ <em>જોશે</em> જો તમે ફક્ત એક કીને દબાવેલ હોય તો, છતાંપણ. આ ઉપયોગી છે જો તમે અમુક કિબોર્ડ ટૂંકાણોને દબાવવા સક્ષમ છો (ઉદાહરણ તરીકે, કી કે જે સાથે બંધ થાય છે), પરંતુ બીજી નહિં.</p>

  <p>આને સક્રિય કરવા માટે <gui>નિષ્ક્રિય કરો જો બે કી ભેગી દબાવેલ હોય</gui>.</p>

  <p>You can have the computer make a “beep” sound when you start typing a
  keyboard shortcut with sticky keys turned on. This is useful if you want to
  know that sticky keys is expecting a keyboard shortcut to be typed, so the
  next key press will be interpreted as part of a shortcut. Select <gui>Beep
  when a modifier key is pressed</gui> to enable this.</p>

</page>
