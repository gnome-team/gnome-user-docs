<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="gu">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>શોભા ત્યાગી</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>બીજી વાર માઉસ બટનને બે વાર ક્લિક કરીને કેટલુ ઝડપી તમારે દબાવવાની જરૂર છે તેની પર નિયંત્રીત કરો.</desc>
  </info>

  <title>બે વાર ક્લિક ઝડપને ગોઠવો</title>

  <p>Double-clicking only happens when you press the mouse button twice
  quickly enough. If the second press is too long after the first, you’ll
  just get two separate clicks, not a double click. If you have difficulty
  pressing the mouse button quickly, you should increase the timeout.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Pointing &amp; Clicking</gui> section to open it.</p>
    </item>
    <item>
      <p>Adjust the <gui>Double-Click Delay</gui> slider to a value you find comfortable.</p>
    </item>
  </steps>

  <p>If your mouse double-clicks when you want it to single-click even though
  you have increased the double-click timeout, your mouse may be faulty. Try
  plugging a different mouse into your computer and see if that works properly.
  Alternatively, plug your mouse into a different computer and see if it still
  has the same problem.</p>

  <note>
    <p>આ સુયોજન એ તમારા માઉસ અને ટચપેડને અસર કરશે, કોઇપણ બીજા નિર્દેશક ઉપકરણને પણ.</p>
  </note>

</page>
