<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-firewall-on-off" xml:lang="gu">

  <info>
    <link type="guide" xref="net-security" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-03" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>તમે નિયંત્રિત કરી શકો છો કે ક્યાં કાર્યક્રમો નેટવર્કને વાપરી શકે છે. આ તમારાં કમ્પ્યૂટરને સુરક્ષિત રાખવા માટે મદદ કરે છે.</desc>
  </info>

  <title>ફાયરવોલ વપરાશને સક્રિય અને બ્લોક કરો</title>

  <p>GNOME does not come with a firewall, so for support beyond this document
  check with your distribution’s support team or your organization’s IT department.
  Your computer should be equipped with a <em>firewall</em> that allows it to
  block programs from being accessed by other people on the internet or your
  network. This helps to keep your computer secure.</p>

  <p>Many applications can use your network connection. For instance, you can
  share files or let someone view your desktop remotely when connected to a
  network. Depending on how your computer is set up, you may need to adjust the
  firewall to allow these services to work as intended.</p>

  <p>Each program that provides network services uses a specific <em>network
  port</em>. To enable other computers on the network to access a service, you
  may need to “open” its assigned port on the firewall:</p>


  <steps>
    <item>
      <p>Go to <gui>Activities</gui> in the top left corner of the screen and
      start your firewall application. You may need to install a firewall
      manager yourself if you can’t find one (for example, GUFW).</p>
    </item>
    <item>
      <p>તમારાં નેટવર્ક સેવા માટે પોર્ટને ખોલો અથવા નિષ્ક્રિય કરો, શું તમે લોકોને વાપરવા સક્ષમ હશો કે નહિં તેની પર આધાર રાખી રહ્યા છે. ક્યો પોર્ટ કે જે તમારે બદલવાની જરૂર છે તે <link xref="net-firewall-ports">સેવા પર આધાર</link> રાખશે.</p>
    </item>
    <item>
      <p>ફેરફારોને લાગુ કરો અથવા સંગ્રહો, નીચેનાં કોઇપણ વધારાની સૂચનાઓ ફાયરવોલ સાધન દ્દારા આપેલ છે.</p>
    </item>
  </steps>

</page>
