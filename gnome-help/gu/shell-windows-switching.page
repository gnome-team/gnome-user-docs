<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="gu">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>શોભા ત્યાગી</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><keyseq><key>Super</key><key>Tab</key></keyseq> દબાવો.</desc>
  </info>

<title>વિન્ડો વચ્ચે બદલો</title>

  <p>You can see all the running applications that have a graphical user
  interface in the <em>window switcher</em>. This makes
  switching between tasks a single-step process and provides a full picture of
  which applications are running.</p>

  <p>કામ કરવાની જગ્યામાંથી:</p>

  <steps>
    <item>
      <p>Press
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      to bring up the <gui>window switcher</gui>.</p>
    </item>
    <item>
      <p>સ્વીચરમાં આગળ (પ્રકાશિત થયેલ) વિન્ડોને પસંદ કરવા માટે <key xref="keyboard-key-super">Super</key> પ્રકાશિત કરો.</p>
    </item>
    <item>
      <p>Otherwise, still holding down the <key xref="keyboard-key-super">Super</key>
      key, press <key>Tab</key> to cycle through the list of open
      windows, or <keyseq><key>Shift</key><key>Tab</key></keyseq> to cycle
      backwards.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">બધી તમારી ખુલ્લી વિન્ડોને વાપરવા માટે નીચેની પટ્ટી પર તમે વિન્ડો યાદીને વાપરી શકો છો અને તેઓ વચ્ચે બદલી શકો છો.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>વિન્ડો સ્વીચરમાં વિન્ડો એ કાર્યક્રમ દ્દારા જૂથ થયેલ છે. ઘણી વિન્ડો સાથે કાર્યક્રમોનું પૂર્વદર્શન નીચે પોપ થાય છે તમે તેની મારફતે ક્લિક કરો. <key xref="keyboard-key-super">Super</key> ને પકડી રાખો અને યાદી મારફતે પગલું લેવા માટે <key>`</key> (અથવા <key>Tab</key> ઉપકર કી) ને દબાવો.</p>
  </note>

  <p>તમે <key>→</key> અથવા <key>←</key> કી સાથે વિન્ડો સ્વીચરમાં કાર્યક્રમ ચિહ્ન વચ્ચે ખસેડી શકો છો, અથવા માઉસ સાથે તેની પર ક્લિક કરીને એકને પસંદ કરો.</p>

  <p>એક જ વિન્ડો સાથે કાર્યક્રમોનાં પૂર્વદર્શનો એ <key>↓</key> કી સાથે દર્શાવી શકાય છે.</p>

  <p>From the <gui>Activities</gui> overview, click on a
  <link xref="shell-windows">window</link> to switch to it and leave the
  overview. If you have multiple
  <link xref="shell-windows#working-with-workspaces">workspaces</link> open,
  you can click on each workspace to view the open windows on each
  workspace.</p>

</page>
