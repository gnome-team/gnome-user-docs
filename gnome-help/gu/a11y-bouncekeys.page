<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="gu">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>સરખી કીને ઝડપથી વારંવાર દબાવવાનું અવગણો.</desc>
  </info>

  <title>બાઉન્સ કીને ચાલુ કરો</title>

  <p>કી દબાવવાનું અવગણવા માટે <em>બાઉન્સ કી</em> ને ચાલુ કરો કે જે ઝડપથી પૂનરાવર્તિત થાય છે. ઉદાહરણ તરીકે, જો તમારો હાજ ધ્રૂજતો હોય તો કે જેને કારણે તમે ઘણી વખત કી દબાવો છો જ્યારે તમારે એકજ વાર તેને દબાવવાની છે, તમારે બાઉન્સ કીને ચાલુ કરવી જોઇએ.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the <gui>Bounce Keys</gui>
      switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>ઝડપથી બાઉન્સ કીને ચાલુ અને બંધ કરો</title>
    <p>You can turn bounce keys on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> on the top bar and
    selecting <gui>Bounce Keys</gui>. The accessibility icon is visible when
    one or more settings have been enabled from the <gui>Accessibility</gui>
    panel.</p>
  </note>

  <p>બદલવા માટે <gui>સ્વીકૃતિ વિલંબ</gui> સ્લાઇડરને વાપરો કે કેટલો સમય બાઉન્સ કી રાહ જોવે છે તે બીજી કીને દબાવતા પહેલાં તમે પછી પહેલી વખતે તમે કીને દબાવેલ છે. Select <gui>જ્યારે કી રદ થયેલ હોય ત્યારે બીપ વગાડો</gui> જો તમે દરેક વખતે કમ્પ્યૂટરનો અવાજ કરવા માંગતા હોય તે કી દબાવવાનું અવગણે છે તે ઝલ્દી બને છે પહેલાંની કી દબાવે પછી.</p>

</page>
