<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="fr">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Il y a peut-être d’autres téléchargements en cours, vous avez une connexion faible, ou c’est un moment de la journée très encombré.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Ralentissement de la navigation internet</title>

  <p>Si vous surfez sur Internet et qu’il vous semble lent, il peut y avoir plusieurs raisons à ce ralentissement.</p>

  <p>Essayez de fermer votre navigateur web puis de le rouvrir et de vous déconnecter puis reconnecter à Internet (faire ceci réinitialise beaucoup de choses qui pouvaient ralentir votre connexion).</p>

  <list>
    <item>
      <p><em style="strong">Heures de la journée à forte activité</em></p>
      <p>Les fournisseurs de service internet configurent leurs connexions de façon à ce qu’elles soient partagées par plusieurs foyers. Même si vous êtes connecté individuellement par votre propre ligne téléphonique ou votre câble, la connexion à Internet au central téléphonique peut être actuellement partagée. Si c’est le cas et que beaucoup de vos voisins utilisent Internet en même temps que vous, vous pourriez constater un ralentissement. Ceci se produit le plus souvent à des heures où vos voisins sont sur Internet (le soir par exemple).</p>
    </item>
    <item>
      <p><em style="strong">Téléchargement de nombreux fichiers simultanément</em></p>
      <p>Si vous (ou quelqu’un partageant votre connexion internet) êtes en train de télécharger plusieurs fichiers à la fois, ou regardez des vidéos, votre connexion internet peut ne pas être assez rapide pour satisfaire à la demande. Dans ce cas, elle vous semblera ralentir.</p>
    </item>
    <item>
      <p><em style="strong">Connexion erratique</em></p>
      <p>Certaines connexions internet sont tout simplement peu fiables, spécialement les temporaires ou celles situées dans des endroits à demande élevée. Si vous êtes dans un bar très fréquenté ou dans un centre de conférences, la liaison internet peut être surchargée ou tout simplement devenir erratique.</p>
    </item>
    <item>
      <p><em style="strong">Signal de connexion sans fil faible</em></p>
      <p>Si vous êtes connecté à Internet par une liaison sans fil (Wi-Fi), vérifiez l’icône réseau dans la barre supérieure qui vous indique la qualité du signal reçu. S’il est faible, la navigation Internet peut être lente.</p>
    </item>
    <item>
      <p><em style="strong">Utilisation d’une connexion internet mobile plus lente</em></p>
      <p>Si vous avez une connexion internet mobile et que vous constatez un ralentissement, c’est peut-être que vous êtes entré dans une zone où la réception du signal est plus faible. Quand cela arrive, votre connexion bascule automatiquement d’une « large bande mobile » rapide (comme la 3G) vers une autre plus lente mais plus fiable (comme GPRS).</p>
    </item>
    <item>
      <p><em style="strong">Le navigateur web a un problème</em></p>
      <p>Parfois, les navigateurs web rencontrent un problème qui les ralentit. Ce problème a beaucoup de causes — par exemple si vous allez sur un site web qui engorge le navigateur au chargement, ou si vous laissez le navigateur ouvert trop longtemps. Essayez de fermer toutes les fenêtres et de rouvrir le navigateur ensuite pour voir s’il y a une différence.</p>
    </item>
  </list>

</page>
