<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="fr">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>
    <revision version="gnome:45" date="2024-04-04" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Masquer un fichier pour le rendre invisible dans le gestionnaire de fichiers.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Masquage d’un fichier</title>

  <p>Le gestionnaire de fichiers <app>Fichiers</app> vous donne la possibilité de masquer et d’afficher des fichiers à votre convenance. Quand un fichier est caché, il n’est pas affiché par le gestionnaire de fichiers, mais il est toujours présent dans son dossier.</p>

  <p>Pour cacher un fichier, <link xref="files-rename">renommez-le</link> en ajoutant un point (<file>.</file>) au début de son nom. Par exemple, pour cacher le fichier appelé <file>exemple.txt</file>, modifiez son nom en <file>.exemple.txt</file>.</p>

<note>
  <p>Vous pouvez cacher des dossiers de la même façon que les fichiers. Renommez le dossier en ajoutant un point (<file>.</file>) au début de son nom.</p>
</note>

<section id="show-hidden">
 <title>Affichage de tous les fichiers cachés</title>

  <p>If you want to see all hidden files in a folder, go to that folder and
  either press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again,
  either press the menu button in the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>Affichage d’un fichier caché</title>

  <p>To unhide a file, go to the folder containing the hidden file. Press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either press the menu button in
  the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>Par défaut, les fichiers cachés sont visibles dans la fenêtre du gestionnaire de fichiers jusqu’à sa fermeture. Pour modifier ce paramètre et toujours afficher tous les fichiers cachés, consultez <link xref="nautilus-views"/>.</p></note>

  <note><p>La plupart des fichiers cachés possèdent un <file>.</file> au début de leur nom, mais d’autres peuvent avoir un <file>~</file> à la fin du nom. Ces fichiers sont des fichiers de sauvegarde. Consultez <link xref="files-tilde"/> pour plus d’informations.</p></note>

</section>

</page>
