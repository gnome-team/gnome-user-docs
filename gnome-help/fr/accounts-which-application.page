<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="fr">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Les applications peuvent utiliser les comptes créés dans <app>Comptes en ligne</app> et les services qu’elles exploitent.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Services en ligne et applications</title>

  <p>Une fois que vous avez ajouté un compte en ligne, n’importe quelle application peut utiliser ce compte pour tous les services que vous n’avez pas <link xref="accounts-disable-service">désactivés</link>. Différents fournisseurs fournissent différents services. Cette page liste les différents services et certaines des applications qui les utilisent.</p>

  <terms>
    <item>
      <title>Agenda</title>
      <p>Le service Agenda vous permet de visualiser, ajouter, et modifier des évènements dans un agenda en ligne. Il est utilisé par des applications telles que <app>Agenda</app>, <app>Evolution</app>, et <app>California</app>.</p>
    </item>

    <item>
      <title>Discussion</title>
      <p>Le service Discussion vous permet de discuter avec vos contacts sur des plateformes populaires de messagerie instantanée. Il est utilisé par l’application <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contacts</title>
      <p>Le service Contacts vous permet de voir les détails publiés de vos contacts sur divers services. Il est utilisé par des applications telles que <app>Contacts</app> et <app>Evolution</app>.</p>
    </item>

<!-- See https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/commit/32f35cc07fcbee839978e3630972b67ed55f0da6
    <item>
      <title>Documents</title>
      <p>The Documents service allows you to view your online documents
      such as those in Google docs. You can view your documents using the
      <app>Documents</app> application.</p>
    </item>
-->
    <item>
      <title>Fichiers</title>
      <p>Le service Fichiers ajoute un emplacement de fichier distant, comme si vous en aviez ajouté un en utilisant la fonctionnalité <link xref="nautilus-connect">Connexion à un serveur</link> dans le gestionnaire de fichiers. Vous pouvez accéder aux fichiers distants en utilisant le gestionnaire de fichiers, ainsi qu’à travers les fenêtres Ouvrir et Sauvegarder des fichiers, dans n’importe quelle application.</p>
    </item>

    <item>
      <title>Courriel</title>
      <p>Le service Courriel vous permet d’envoyer et de recevoir des courriels via un fournisseur de courriel, tel que Google. Il est utilisé par <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->
<!-- TODO: Not sure what this does in which app. Seems to support e.g. VKontakte.
    <item>
      <title>Music</title>
    </item>
-->
    <item>
      <title>Photos</title>
      <p>Le service Photos vous permet de visualiser vos photos en ligne comme celles que vous publiez sur Facebook. Vous pouvez visualiser vos photos en utilisant l’application <app>Photos</app>.</p>
    </item>

    <item>
      <title>Imprimantes</title>
      <p>Le service Imprimantes vous permet d’envoyer une copie PDF à un fournisseur depuis la fenêtre d’impression de n’importe quelle application. Le fournisseur peut fournir des services d’impression, ou il peut simplement servir de stockage pour les PDF, que vous pouvez télécharger et imprimer plus tard.</p>
    </item>

  </terms>

</page>
