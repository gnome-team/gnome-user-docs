<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="fr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Les fichiers supprimés sont généralement déplacés dans la corbeille, d’où ils peuvent être récupérés.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Restauration d’un fichier à partir de la corbeille</title>

  <p>Si vous effacez un fichier avec le gestionnaire de fichiers, le fichier est normalement placé dans la <gui>Corbeille</gui> et il est possible de le restaurer.</p>

  <steps>
    <title>Pour restaurer un fichier à partir de la corbeille :</title>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <app>Fichiers</app>.</p>
    </item>
    <item>
      <p>Cliquez sur <app>Fichiers</app> pour ouvrir le gestionnaire de fichiers.</p>
    </item>
    <item>
      <p>Click <gui>Trash</gui> in the sidebar. If you do not see the sidebar,
      press the <gui>Show Sidebar</gui> button in the top-left corner of the window.</p>
    </item>
    <item>
      <p>If your deleted file is there, click on it and select
      <gui>Restore From Trash</gui>. It will be restored to the folder from where it was
      deleted.</p>
    </item>
  </steps>

  <p>Si votre fichier a été supprimé avec <keyseq><key>Maj</key><key>Suppr</key></keyseq> ou en ligne de commande, il a été supprimé définitivement. Les fichiers supprimés définitivement ne peuvent plus être récupérés dans la <gui>corbeille</gui>.</p>

  <p>Il existe de nombreux outils de restauration capables de récupérer parfois des fichiers qui ont été supprimés définitivement. Ces outils ne sont en général pas très simples à manipuler. S’il vous arrive malencontreusement de supprimer définitivement un fichier important, il est sans doute mieux de demander conseil sur un forum spécialisé pour voir si vous pouvez le récupérer.</p>

</page>
