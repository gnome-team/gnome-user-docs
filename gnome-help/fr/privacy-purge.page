<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="fr">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Régler la fréquence à laquelle la corbeille et le dossier des fichiers temporaires doivent être vidés.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Purge de la corbeille et des fichiers temporaires</title>

  <p>Vider la corbeille et les fichiers temporaires permet de supprimer les fichiers devenus inutiles, et aussi de faire de la place sur le disque dur. Il est possible de vider la corbeille et d’éliminer les fichiers temporaires manuellement, mais vous pouvez aussi régler l’ordinateur pour qu’il le fasse automatiquement à votre place.</p>

  <p>Les fichiers temporaires sont des fichiers créés automatiquement par des applications en arrière-plan. Ils peuvent améliorer les performances en fournissant une copie des données téléchargées ou traitées.</p>

  <steps>
    <title>Purge automatique de la corbeille et des fichiers temporaires</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy &amp; Security</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Privacy &amp; Security</gui></guiseq> from the
      results. This will open the <gui>Privacy &amp; Security</gui> panel.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Historique des fichiers et corbeille</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Activez <gui>Supprimer automatiquement le contenu de la corbeille</gui> et/ou <gui>Supprimer automatiquement les fichiers temporaires</gui>.</p>
    </item>
    <item>
      <p>Définissez la fréquence de purge de la <em>Corbeille</em> et des <em>Fichiers temporaires</em> en modifiant la valeur <gui>Périodicité de suppression automatique</gui>.</p>
    </item>
    <item>
      <p>Utilisez les boutons <gui>Vider la corbeille</gui> ou <gui>Supprimer les fichiers temporaires</gui> pour exécuter ces actions immédiatement.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Il est possible de supprimer des fichiers immédiatement et de façon permanente sans passer par la corbeille. Consultez <link xref="files-delete#permanent"/> pour plus d’informations.</p>
  </note>

</page>
