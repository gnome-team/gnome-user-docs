<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="fr">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Appuyez sur <keyseq><key>Ctrl</key><key>S</key></keyseq> pour sélectionner plusieurs fichiers avec des noms similaires.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Sélection de fichiers par motif</title>

  <p>Vous pouvez sélectionner plusieurs fichiers d’un dossier à l’aide d’un motif sur le nom de fichier. Appuyez sur <keyseq><key>Ctrl</key><key>S</key></keyseq> pour afficher la fenêtre de <gui>Sélection d’éléments selon un motif</gui>. Saisissez un motif contenant la partie commune des noms de fichier et des caractères génériques. Deux caractères génériques sont disponibles :</p>

  <list style="compact">
    <item><p><file>*</file> remplace un nombre quelconque de caractères, y compris zéro caractère.</p></item>
    <item><p><file>?</file> remplace exactement un seul caractère, n’importe lequel.</p></item>
  </list>

  <p>Par exemple :</p>

  <list>
    <item><p>Si vous avez un document au format de fichier OpenDocument, un autre en PDF et une image portant le même nom de base <file>Facture</file>, sélectionnez tous les trois à l’aide du motif :</p>
    <example><p><file>Facture.*</file></p></example></item>

    <item><p>Si vous avez des photos portant des noms du genre <file>Vacances-001.jpg</file>, <file>Vacances-002.jpg</file>, <file>Vacances-003.jpg</file>, sélectionnez-les à l’aide du motif :</p>
    <example><p><file>Vacances-???.jpg</file></p></example></item>

    <item><p>Si vous avez des photos comme ci-dessus, mais que vous en avez modifié quelques-unes et ajouté <file>-modifiée</file> à la fin de leur nom, sélectionnez-les à l’aide du motif :</p>
    <example><p><file>Vacances-???-modifiée.jpg</file></p></example></item>
  </list>

</page>
