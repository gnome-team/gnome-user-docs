<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="fr">

  <info>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Utiliser votre téléphone ou votre adaptateur internet pour vous connecter au réseau cellulaire de votre mobile.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Connexion au réseau cellulaire</title>

  <p>Vous pouvez configurer une connexion à un réseau cellulaire (3G) avec le modem 3G intégré de votre ordinateur, de votre téléphone ou de votre adaptateur internet.</p>

  <note style="tip">
  <p>La plupart des téléphones possèdent un paramètre appelé <link xref="net-tethering">Partage de connexion USB</link> qui ne nécessite aucune configuration sur l’ordinateur, et qui est généralement la meilleure méthode pour se connecter au réseau cellulaire.</p>
  </note>

  <steps>
    <item><p>Si vous ne possédez pas de modem 3G intégré, connectez votre téléphone ou votre adaptateur internet sur un port USB de votre ordinateur.</p>
    </item>
    <item>
    <p>Ouvrez le <gui xref="shell-introduction#systemmenu">menu système</gui> dans la partie droite de la barre supérieure.</p>
  </item>
  <item>
    <p>Sélectionnez <gui>Connexion mobile désactivée</gui>. La partie <gui>Connexion mobile</gui> du menu se déroule.</p>
      <note>
        <p>Si <gui>Connexion mobile</gui> ne s’affiche pas dans le menu système, vérifiez que votre périphérique n’est pas configuré comme étant un périphérique de stockage de masse.</p>
      </note>
    </item>
    <item><p>Sélectionnez <gui>Se connecter</gui>. Si vous vous connectez pour la première fois, l’assistant de <gui>Configuration d’une connexion mobile</gui> se lance. La boîte de dialogue affiche une liste d’informations nécessaires. Appuyez sur <gui style="button">Suivant</gui>.</p></item>
    <item><p>Sélectionnez le pays ou la région de votre fournisseur dans la liste, puis cliquez sur <gui style="button">Suivant</gui>.</p></item>
    <item><p>Choisissez votre fournisseur dans la liste, puis cliquez sur <gui style="button">Suivant</gui>.</p></item>
    <item><p>Choisissez la carte qui correspond au type de périphérique que vous connectez. Cela détermine le nom du point d’accès, puis cliquez sur <gui style="button">Suivant</gui>.</p></item>
    <item><p>Confirmez votre choix des paramètres en appuyant sur <gui style="button">Appliquer</gui>. L’assistant se ferme et le panneau <gui>Réseau</gui> affiche les propriétés de votre connexion.</p></item>
  </steps>

</page>
