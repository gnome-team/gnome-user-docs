<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="zh-CN">

  <info>
    <revision version="gnome:46" date="2024-03-10" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>设置按钮功能和手写笔的压力感应。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>配置手写笔</title>

  <steps>
    <item>
      <p>打开 <gui xref="shell-introduction#activities">活动</gui> 概览，并开始输入 <gui>Wacom 数位板</gui>。</p>
    </item>
    <item>
      <p>点击 <gui>Wacom 数位板</gui>打开面板。</p>
      <note style="tip"><p>如果没有检测到数位板，您会被要求 <gui>请插入并打开您的 Wacom 数位板</gui>。点击侧边栏中的 <gui>蓝牙</gui> 以连接无线数位板。</p></note>
    </item>
    <item>
      <p>有一个部分包含特定于每个手写笔的设置，设备名称（手写笔类别）和图表位于顶部。</p>
      <note style="tip"><p>如果没有检测到手写笔，您将被要求<gui>请将您的手写笔移到数位板附近以进行配置</gui>。</p></note>
      <p>可以调整这些设置：</p>
      <list>
        <item><p><gui>笔尖压力感应：</gui>使用滑块在<gui>软</gui>和<gui>硬</gui>之间调整“感应”。</p></item>
        <item><p>按钮/滚轮配置（这将改变手写笔的功能）。点击每个标签旁边的菜单，选择其中一个功能：鼠标左键点击、鼠标中键点击、鼠标右键点击、后退或前进。</p></item>
     </list>
    </item>
    <item>
      <p>点击标题栏中的 <gui>测试设置</gui> 将弹出一个草图板，您可以在其中测试手写笔设置。</p>
    </item>
  </steps>

</page>
