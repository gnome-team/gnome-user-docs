<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="net-wireless-connect" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>连接互联网——无线。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>连接到一个无线网络</title>

<p>如果您的计算机启用了无线，则可在处于无线网线覆盖范围时连接到该网络，以访问互联网、查看网络上的共享文件以及执行其他操作。</p>

<steps>
  <item>
    <p>从顶栏右侧打开<gui xref="shell-introduction#systemmenu">系统菜单</gui>。</p>
  </item>
  <item>
    <p>选择 <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-disabled-symbolic.svg"/> Wi-Fi</gui>。菜单的 Wi-Fi 部分将展开。</p>
  </item>
  <item>
    <p>点击您要用的网络名称。</p>
    <p>如果网络名称不在列表中，请向下滚动列表。如果您仍然找不到网络，可能您的计算机超出了网络覆盖范围，或者网络<link xref="net-wireless-hidden">可能被隐藏了</link>。</p>
  </item>
  <item>
    <p>如果网络受到（<link xref="net-wireless-wepwpa">密码</link>）保护，请在出现提示时输入密码并单击<gui>连接</gui>。</p>
    <p>如果不知道密钥，它可能写在无线路由器或基站设备的底部，或在其说明书中，再或者您可能要咨询无线网络管理员。</p>
  </item>
  <item>
    <p>当计算机连接到网络过程中，网络图标会发生变化。</p>
  </item>
  <item>
    <if:choose>
    <if:when test="platform:gnome-classic">
    <p>如果连接成功，图标会变成一个圆点加上弧状条纹（<media its:translate="no" type="image" mime="image/svg" src="figures/classic-topbar-network-wireless-signal-excellent.svg" width="28" height="28"/>）。条纹越多表示网络连接越强。越少表示连接越弱而且可能不太稳定。</p>
    </if:when>
    <p>如果连接成功，图标会变成一个圆点加上弧状条纹（<media its:translate="no" type="image" mime="image/svg" src="figures/topbar-network-wireless-signal-excellent.svg" width="28" height="28"/>）。条纹越多表示网络连接越强。越少表示连接越弱而且可能不太稳定。</p>
    </if:choose>
  </item>
</steps>

  <p>如果连接不成功，可能会再次要求您输入密码，或者可能只是告诉您连接已经断开。导致这种情况的原因有很多。例如，您可能输入了错误的密码、无线信号太弱或者计算机的无线网卡可能有问题。请参阅<link xref="net-wireless-troubleshooting"/>获取更多帮助。</p>

  <p>较强的无线网络连接并不一定意味着互联网连接速度更快，或者您的下载速度更快。无线连接将您的计算机连接到<em>提供互联网连接</em>的设备（如路由器或调制解调器），但设备本身所提供无线信号的强度与网速无关。</p>

</page>
