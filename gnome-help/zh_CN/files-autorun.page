<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>插入 CD、DVD、数码相机、音乐播放器和其他设备和介质时，自动运行相关应用程序。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>当您插入某个设备时自动打开应用程序</title>

  <p>当您插入某个设备、光盘或介质卡时，您可以自动启动特定的应用程序。例如，当您插入数码相机时，您可能希望启动您的照片管理器。您也可以将其关闭，这样在您插入对应设备时系统将不自动进行任何操作。</p>

  <p>接入不同设备时，选择用哪一个程序打开它：</p>

<steps>
  <item>
    <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>可移动介质</gui>。</p>
  </item>
  <item>
    <p>从结果中选择<guiseq><gui>设置</gui><gui>应用</gui></guiseq>。这将打开<gui>应用</gui>面板。</p>
  </item>
  <item>
    <p>选择<gui>默认应用</gui>打开面板。</p>
  </item>
  <item>
    <p>在 <gui>可移动媒体</gui> 部分，切换 <gui>自动启动应用</gui> 开关为开启状态。</p>
  </item>
  <item>
    <p>找到您想要设置的设备或介质类型，然后为它选择一个应用程序或操作。不同的设备和介质类型，请参阅下面的描述。</p>
    <p>除了用程序打开外，您也可以通过设置<gui>打开文件夹</gui>选项使该设备显示在文件管理器中。这种情况下，您会被询问如何操作，否则什么也不做。</p>
  </item>
  <item>
    <p>如果列表中没有您要更改的设备或介质类型（如蓝光光盘或电子书阅读器），点击<gui>其他介质</gui>查看更详细的设备列表。从<gui>类型</gui>下拉菜单中选择设备或介质类型，从<gui>动作</gui>下拉菜单中选择应用程序或操作。</p>
  </item>
</steps>

  <note style="tip">
    <p>如果无论插入什么设备，您都不希望任何应用程序自动启动，请关闭<gui>自动启动应用</gui>。</p>
  </note>

<section id="files-types-of-devices">
  <title>设备或介质类型</title>
<terms>
  <item>
    <title>音乐光碟</title>
    <p>选择您喜爱的音乐应用程序或 CD 音频提取器来处理音频 CD。如果使用音频 DVD（DVD-A），在<gui>其他介质</gui>下选择如何打开。如果使用文件管理器打开音频光盘，曲目将以 WAV 文件的形式显示，您可以用任何音频播放器应用程序播放它们。</p>
  </item>
  <item>
    <title>视频光碟</title>
    <p>选择您喜爱的视频应用程序来处理视频 DVD。使用<gui>其他介质</gui>按钮设置处理蓝光、高清 DVD、视频 CD（VCD）和超级视频 CD（SVCD）的应用程序。如果插入 DVD 或其他视频光盘后不能正常播放，请参阅<link xref="video-dvd"/>。</p>
  </item>
  <item>
    <title>空白光盘</title>
    <p>使用<gui>其他介质</gui>按钮设置处理空白的 CD、DVD、蓝光光盘或高清 DVD 的光盘刻录应用程序。</p>
  </item>
  <item>
    <title>相机和照片</title>
    <p>点击<gui>照片</gui>下拉列表，选择一个照片处理器程序与之关联，当您插入一个数码相机或者插入一个相机介质卡，如 CF 卡、SD 卡、MMC 或 MS 卡，会启动这个应用程序，您可以设为仅在文件管理器中查看。</p>
    <p>在<gui>其他介质</gui>下，您可以选择应用程序来打开柯达图片 CD，例如那些您在商店里制作好的。这些常规数据 CD 都带有包含 JPEG 图片的<file>图片</file>文件夹。</p>
  </item>
  <item>
    <title>音乐播放器</title>
    <p>选择一个管理您的音乐媒体库的应用程序，比如您的移动音乐播放器或者文件管理器中的音乐文件。</p>
    </item>
    <item>
      <title>电子书阅读器</title>
      <p>使用<gui>其他介质</gui>按钮选择应用程序来管理您的电子书阅读器上的书籍，或者您也可以使用文件管理器来管理。</p>
    </item>
    <item>
      <title>软件</title>
      <p>一些光碟或可移动介质中，包含插入介质时支持的自动运行程序，点击<gui>软件</gui>选项来设置插入一个带自动运行的媒体时如何操作。您可以设置程序在运行前总是询问。</p>
      <note style="warning">
        <p>不要运行不可信介质上的程序。</p>
      </note>
   </item>
</terms>

</section>

</page>
