<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>厂家设计或者电池续航估算不同可能导致这个问题。</desc>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>为什么我的电池续航比在 Windows/Mac OS 下要短？</title>

<p>一些计算机出现在 Linux 下，电池续航比 Windows 或 Mac OS 下短的问题，一个原因是那些计算机厂家为某些型号的计算机，安装了特殊的 Windows/Mac OS 优化硬件/软件设计。这些设计常常高度专一，很难描述，因此包含到 Linux 下也是很困难的。</p>

<p>遗憾的是，在对其不十分了解的情况下，Linux 开发者很难为您应用这些调整。但是，您可能会发现使用一些<link xref="power-batterylife">简单的节能方法</link>会有所帮助。如果您的计算机有<link xref="power-batteryslow">变速处理器</link>，您会发现更改其设置也很有用。</p>

<p>这种差异的另一个可能的原因是，Windows/Mac OS 的电池续航估算方法与 Linux 不同。电池实际的续航时间可能完全相同，但不同的方法给出了不同的估计值。</p>
	
</page>
