<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>管理好工作区中的各个窗口，可以让您的工作更加高效。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>移动和缩放窗口</title>

  <p>您可以移动窗口和调整窗口大小，以便更高效地工作。除了您可能想要的拖动外，系统还提供了快捷键和修饰键来帮您快速布置窗口。</p>

  <list>
    <item>
      <p>通过拖动标题栏来移动窗口，或按住 <key xref="keyboard-key-super">Super</key>，然后拖动窗口中任意位置。移动时按住 <key>Shift</key>，可将窗口贴到屏幕边缘和其他窗口边上。</p>
    </item>
    <item>
      <p>改变窗口大小可以拖动窗口的四个边或角，按住 <key>Shift</key> 键可以让窗口贴紧屏幕或其他窗口的边缘。</p>
      <p if:test="platform:gnome-classic">您也可以通过点击标题栏的最大化按钮来调整最大化窗口的大小。</p>
    </item>
    <item>
      <p>使用键盘来移动或改变窗口大小。按 <keyseq><key>Alt</key><key>F7</key></keyseq> 来移动窗口，或者按 <keyseq><key>Alt</key><key>F8</key></keyseq> 改变窗口大小。使用方向键移动或改变窗口大小，然后按 <key>Enter</key> 确定，也可以按 <key>Esc</key> 取消，返回到原来的状态。</p>
    </item>
    <item>
      <p>把窗口拖动到屏幕顶部可以<link xref="shell-windows-maximize">最大化窗口</link>，把窗口拖到屏幕的一侧，将会在屏幕这一半最大化，这样您可以将<link xref="shell-windows-tiled">两个窗口并排最大化</link>。</p>
    </item>
  </list>

</page>
