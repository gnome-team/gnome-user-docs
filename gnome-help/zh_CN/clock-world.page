<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>在日历下方显示其他城市的时间。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>添加一个世界时钟</title>

  <p>使用<app>时钟</app>添加其他城市的时间。</p>

  <note>
    <p>这需要先安装<app>时钟</app>应用程序。</p>
    <p>大多数发行版都默认安装了<app>时钟</app>。如果您没有找到，可能需要通过发行版的软件包管理器来安装它。</p>
  </note>

  <p>要添加一个世界时钟：</p>

  <steps>
    <item>
      <p>点击顶栏上的时钟。</p>
    </item>
    <item>
      <p>点击日历下方的<gui>添加世界时钟…</gui>按钮，启动<app>时钟</app>。</p>

    <note>
       <p>如果已添加了一个或多个世界时钟，点击其中一个，<app>时钟</app>就会启动。</p>
    </note>

    </item>
    <item>
      <p>在<app>时钟</app>窗口中，点击 <gui style="button">+</gui> 按钮或按 <keyseq><key>Ctrl</key><key>N</key></keyseq> 添加新城市。</p>
    </item>
    <item>
      <p>在搜索框输入城市名。</p>
    </item>
    <item>
      <p>从列表中选择正确的城市或离您最近的地点。</p>
    </item>
    <item>
      <p>按<gui style="button">添加</gui>完成添加城市。</p>
    </item>
  </steps>

  <p>请参阅<link href="help:gnome-clocks">时钟帮助</link>，了解<app>时钟</app>的更多功能。</p>

</page>
