<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>设置开机时自动登录。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>自动登录</title>

  <p>您可以更改设置使计算机启动时自动登录到您的账号：</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>系统</gui>。</p>
    </item>
    <item>
      <p>从结果中选择<guiseq><gui>设置</gui><gui>系统</gui></guiseq>。这将打开<gui>系统</gui>面板。</p>
    </item>
    <item>
      <p>选择 <gui>用户</gui> 以打开面板。</p>
    </item>
    <item>
      <p>按右上角的<gui style="button">解锁</gui>并在提示时输入密码。</p>
    </item>
    <item>
      <p>如果是要自动登录的其他用户账号，请在 <gui>其他用户</gui> 下选择该账号。</p>
    </item>
    <item>
      <p>切换 <gui>自动登录</gui> 开关为开启状态。</p>
    </item>
  </steps>

  <p>当下次启动电脑时，您将自动登录。如果启用了这个选项，您将不需要输入密码来登录您的账号，这意味着如果其他人启动您的电脑，他们将能够访问您的账号和个人数据，包括您的文件和浏览器历史记录。</p>

  <note>
    <p>如果您的账号类型是<em>标准</em>，您无法更改此设置。请联系可以为您更改此设置的系统管理员。</p>
  </note>

</page>
