<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>在触摸板或触摸屏上使用手势操控桌面。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>使用触摸板和触摸屏手势</title>

  <p>触摸板或触摸屏上的多点触控手势可以在用于系统导航，也可以用于应用程序。</p>

  <p>许多应用程序都支持使用手势。在<app>文档查看器</app>中，可以用手势缩放和滑动文档，<app>图像查看器</app>允许您缩放、旋转和平移图像。</p>

<section id="system">
  <title>系统手势</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>打开活动概览和应用程序视图</em></p>
    <p>三根手指按住触摸板或触摸屏并向上滑动打开活动概览。</p>
    <p>要打开应用程序视图，请再次用三根手指按住并向上滑动。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>切换工作区</em></p>
    <p>三根手指按住触摸板或触摸屏并向左或向右滑动。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>退出全屏</em></p>
    <p>在触摸屏上，从顶部边缘向下拖动可退出任意窗口的全屏模式。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>调出屏幕键盘</em></p>
    <p>在触摸屏上，如果启用了屏幕键盘，从底部边缘向上拖动将弹出<link xref="keyboard-osk">屏幕键盘</link>。</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>应用程序手势</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>打开项目，启动应用程序，播放歌曲</em></p>
    <p>轻触项目。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>选择项目并列出其可执行的操作</em></p>
    <p>按住一到两秒钟。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>滚动屏幕上的区域</em></p>
    <p>拖动：一根手指按住屏幕并滑动。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>更改视图的缩放级别（<app>地图</app>，<app>照片</app>）</em></p>
    <p>两指捏抻：用两根手指触摸屏幕并靠拢或分开。</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>旋转照片</em></p>
    <p>两指旋转：用两根手指触摸屏幕并旋转。</p></td>
  </tr>
</table>

</section>

</page>
