<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>添加新用户以便其他从可以登录计算机。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>添加一个新用户</title>

  <p>您可以在计算机上添加多个用户，给家里或公司里每个人一个助记词账号，每个用户具有自己的主文件夹、文档和设置文件。</p>

  <p>您需要<link xref="user-admin-explain">管理员权限</link>来添加用户账号。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>系统</gui>。</p>
    </item>
    <item>
      <p>从结果中选择<guiseq><gui>设置</gui><gui>系统</gui></guiseq>。这将打开<gui>系统</gui>面板。</p>
    </item>
    <item>
      <p>选择 <gui>用户</gui> 以打开面板。</p>
    </item>
    <item>
      <p>按右上角的<gui style="button">解锁</gui>并在提示时输入密码。</p>
    </item>
    <item>
      <p>点击 <gui>其他用户</gui> 下方的 <gui style="button">添加用户…</gui> 按钮，即可添加新用户账号。</p>
    </item>
    <item>
      <p>如果您想让新用户拥有计算机的<link xref="user-admin-explain">管理员权限</link>，请将其账号类型选为 <gui>管理员</gui>。</p>
      <p>管理员可以做诸如添加和删除用户、安装软件和驱动程序以及更改日期和时间之类的事。</p>
    </item>
    <item>
      <p>输入新用户的全名。用户名将根据全名自动填写。如果不喜欢自动填写的用户名，您可以更改它。</p>
    </item>
    <item>
      <p>您可以选择为新用户设置密码，也可以让他们在首次登录时自行设置密码。如果选择立即设置密码，可以点击 <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">生成密码</span></media></gui> 图标自动生成随机密码。</p>
      <p>要将用户连接到网络域，请点击 <gui>企业登录</gui>。</p>
    </item>
    <item>
      <p>点击 <gui>添加</gui>。添加用户后，可以调整 <gui>家长控制</gui> 和 <gui>语言</gui> 设置。</p>
    </item>
  </steps>

  <p>如果创建账号后想修改密码，选择账号，<gui style="button">解锁</gui> 面板，然后按当前密码。</p>

  <note>
    <p>在 <gui>用户</gui> 面板中，您可以点击右侧用户名称旁边的图片，为该账号设置一张图片。这张图片会显示在登录窗口中。系统提供了一些您可以选择的图片，您也可以选择自己的图片或者用摄像头拍摄一张。</p>
  </note>

</page>
