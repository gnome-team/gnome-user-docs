<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>停止或限制电脑追踪最近使用的文件。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>关闭或限制文件历史追踪</title>
  
  <p>追踪最近使用过的文件和文件夹可以让您更容易地在文件管理器和应用程序的文件对话框中找到您一直在处理的项目。您可能不想追踪文件使用历史，或者只追踪非常近的历史。</p>

  <steps>
    <title>关闭文件历史追踪</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>隐私与安全</gui>。</p>
    </item>
    <item>
      <p>从结果中选择<guiseq><gui>设置</gui><gui>隐私与安全</gui></guiseq>。这将打开<gui>隐私与安全</gui>面板。</p>
    </item>
    <item>
      <p>点击<gui>文件历史和回收站</gui>打开面板。</p>
    </item>
    <item>
     <p>切换<gui>文件历史</gui>开关为关闭状态。</p>
     <p>要重新启用此功能，请将<gui>文件历史</gui>开关切换到开启状态。</p>
    </item>
    <item>
      <p>使用<gui>清除历史…</gui>按钮可立即清除历史记录。</p>
    </item>
  </steps>
  
  <note><p>此设置不会影响您的 Web 浏览器存储您访问的网站信息的方式。</p></note>

  <steps>
    <title>限制您的文件历史被追踪的时间长度</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>文件历史和回收站</gui>。</p>
    </item>
    <item>
      <p>点击<gui>文件历史和回收站</gui>打开面板。</p>
    </item>
    <item>
     <p>确保<gui>文件历史</gui>开关已设置为开启状态。</p>
    </item>
    <item>
     <p>在<gui>文件历史持续时间</gui>下，选择保留文件历史的时间。可以从<gui>1天</gui>、<gui>7天</gui>、<gui>30天</gui>或<gui>永久</gui>等选项中选择。</p>
    </item>
    <item>
      <p>使用<gui>清除历史…</gui>按钮可立即清除历史记录。</p>
    </item>
  </steps>

</page>
