<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.38.1" date="2020-11-04" status="review"/>
    <revision version="gnome:42" status="final" date="2022-03-02"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ivan Stanton</name>
      <email>northivanastan@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>选择样式并设置背景。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>更改桌面的外观</title>
  
  <p>您可以通过将样式首选项设置为亮色或暗色来更改屏幕上显示内容的外观。您可以选择图像或墙纸作为桌面背景。</p>
  
<section id="style">
  <title>亮色或暗色样式</title>
  <p>要在亮色和暗色桌面样式之间切换：</p>
  
  <steps>
    <item>
      <p>打开 <gui xref="shell-introduction#activities">活动</gui> 概览后输入 <gui>外观</gui>。</p>
    </item>
    <item>
      <p>点击 <gui>外观</gui> 打开面板。当前选用的样式显示在顶部，周围环绕着蓝色边框。</p>
    </item>
    <item>
      <p>点击选择 <gui>亮色</gui> 或 <gui>暗色</gui>。</p>
    </item>
    <item>
      <p>设置立即生效。</p>
    </item>
  </steps>
  
</section>

<section id="background">
  <title>背景</title>
  <p>要改变用于背景的图像：</p>

  <steps>
    <item>
      <p>打开 <gui xref="shell-introduction#activities">活动</gui> 概览后输入 <gui>外观</gui>。</p>
    </item>
    <item>
      <p>点击<gui>外观</gui>打开面板。当前选用的壁纸显示在当前样式预览的顶部。</p>
    </item>
    <item>
      <p>有两种方法可以改变背景用的图像：</p>
      <list>
        <item>
          <p>点击一张系统自带的背景图片。</p>
        <note style="info">
          <p>有些壁纸会在一天中自动更换。这些壁纸的右下角有一个小时钟图标。</p>
        </note>
        </item>
        <item>
          <p>点击<gui>添加图片…</gui>来使用自定义图片。默认情况下，将打开<file>图片</file>文件夹，因为大多数照片管理应用程序都将照片存储在这里。</p>
        </item>
      </list>
    </item>
    <item>
      <p>设置立即生效。</p>
        <note style="tip">
          <p>另一种自定义图片为背景的方法是，在<app>文件</app>中的图片文件上点击右键，选择<gui>设为壁纸</gui>，或在<app>图片查看器</app>中打开图片，点击标题栏中的菜单按钮，选择<gui>设为壁纸</gui>。</p>
        </note>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">切换到一个空的工作区</link>查看整个桌面。</p>
    </item>
  </steps>
  
  </section>

</page>
