<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>在<gui>键盘</gui>设置项中定义或修改键盘快捷键。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>设置键盘快捷键</title>

<p>要改变键盘快捷键的设置：</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>设置</gui>。</p>
    </item>
    <item>
      <p>点击<gui>设置</gui>。</p>
    </item>
    <item>
      <p>点击侧边栏中的<gui>键盘</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>键盘快捷键</gui>部分，选择<gui>查看及自定义快捷键</gui>。</p>
    </item>
    <item>
      <p>选择所需类别，或输入搜索词。</p>
    </item>
    <item>
      <p>点击要操作的行。将显示<gui>设置快捷键</gui>的窗口。</p>
    </item>
    <item>
      <p>按下想要设置的组合键，按 <key>Backspace</key> 重置，按 <key>Esc</key> 取消。</p>
    </item>
  </steps>


<section id="defined">
<title>预定义的快捷键</title>
  <p>有许多定义好的快捷键可以被更改，按组排列如下：</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>辅助功能</title>
  <tr>
	<td><p>减小文本字号</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>高对比度开或关</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>增大文本字号</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>开关屏幕键盘</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>开关屏幕阅读器</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>开关屏幕缩放</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>放大</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>缩小</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>启动器</title>
  <tr>
	<td><p>主文件夹</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> 或 <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> 或者 <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>启动计算器</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> 或 <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>启动邮件客户端</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> 或 <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>启动帮助浏览器</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>启动 Web 浏览器</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> 或 <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> 或者 <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>搜索</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> 或 <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>设置</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>导航</title>
  <tr>
	<td><p>隐藏所有正常窗口</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>移至左侧工作区</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>移至右侧工作区</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口下移一个显示器</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口左移一个显示器</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口右移一个显示器</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口上移一个显示器</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口左移一个工作区</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key> <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口右移一个工作区</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口移至最后一个工作区</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口移至工作区 1</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口移至工作区 2</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>将窗口移至工作区 3</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>将窗口移至工作区 4</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>切换应用程序</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>切换系统控制</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>直接切换系统控制</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>切换至最后一个工作区</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>切换至工作区 1</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>切换至工作区 2</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>切换至工作区 3</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>切换至工作区 4</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
        <td><p>切换窗口</p></td>
        <td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>直接切换窗口</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>直接切换同一应用的窗口</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>切换同一应用程序的窗口</p></td>
	<td><p>禁用</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>屏幕截图</title>
  <tr>
	<td><p>保存窗口的屏幕截图</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>保存整个屏幕的屏幕截图</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>启动屏幕截图工具</p></td>
	<td><p><key>打印</key></p></td>
  </tr>
  <tr>
        <td><p>简短屏幕录制</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>声音和媒体</title>
  <tr>
	<td><p>弹出</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media>（弹出）</p></td>
  </tr>
  <tr>
	<td><p>启动媒体播放器</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media>（音频媒体）</p></td>
  </tr>
  <tr>
	<td><p>麦克风静音/取消静音</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>下一曲目</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media>（下一音频）</p></td>
  </tr>
  <tr>
	<td><p>暂停播放</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media>（暂停音频）</p></td>
  </tr>
  <tr>
	<td><p>播放（或者 播放/暂停）</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media>（播放音频）</p></td>
  </tr>
  <tr>
	<td><p>上一曲目</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media>（上一音频）</p></td>
  </tr>
  <tr>
	<td><p>停止播放</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media>（停止音频）</p></td>
  </tr>
  <tr>
	<td><p>降低音量</p></td>
	<td><p>降低音频音量<media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media>（降低音频音量）</p></td>
  </tr>
  <tr>
	<td><p>静音</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media>（静音音频）</p></td>
  </tr>
  <tr>
	<td><p>增大音量</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media>（增大音频音量）</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>系统</title>
  <tr>
        <td><p>聚焦活动通知</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>锁定屏幕</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>显示关机对话框</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>恢复键盘快捷键</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>显示所有应用</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>显示活动概览</p></td>
	<td><p><key>Super</key></p></td>
  </tr>
  <tr>
	<td><p>显示通知列表</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>显示系统菜单</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>显示运行命令提示符</p></td>
	<td><p>按组合键 <keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>打字</title>
  <tr>
  <td><p>切换至下个输入源</p></td>
  <td><p><keyseq><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>切换至上个输入源</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>窗口</title>
  <tr>
	<td><p>激活窗口菜单</p></td>
	<td><p><keyseq><key>Alt</key><key>空格</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>关闭窗口</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>隐藏窗口</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口降低至其他窗口之下</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>最大化窗口</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>水平最大化窗口</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>垂直最大化窗口</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>移动窗口</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>将窗口提升至其他窗口之上</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>如果窗口被其他窗口遮盖，则提升它，否则降低它</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>改变窗口大小</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>恢复窗口</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>切换全屏模式</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
	<td><p>切换最大化状态</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>窗口在单一还是所有工作区显示</p></td>
	<td><p>禁用</p></td>
  </tr>
  <tr>
        <td><p>在左半边查看</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>在右半边查看</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>自定义快捷键</title>

  <p>要在<gui>键盘</gui>设置中自定义应用程序快捷键：</p>

  <steps>
    <item>
      <p>选择<gui>自定义快捷键</gui>。</p>
    </item>
    <item>
      <p>如果尚未设置自定义快捷键，请点击<gui style="button">添加快捷键</gui>按钮。否则点击 <gui style="button">+</gui> 按钮。将显示<gui>添加自定义快捷键</gui>窗口。</p>
    </item>
    <item>
      <p>键入<gui>名称</gui>来标识快捷键，并键入<gui>命令</gui>来运行应用程序。例如，如果想用快捷键打开 <app>Rhythmbox</app>，您可以将其命名为<input>音乐</input>，并使用 <input>rhythmbox</input> 命令。</p>
    </item>
    <item>
      <p>点击<gui style="button">添加快捷键…</gui>按钮。在<gui>添加自定义快捷方式</gui>窗口中，按下想要的快捷键组合。</p>
    </item>
    <item>
      <p>点击<gui>添加</gui>。</p>
    </item>
  </steps>

  <p>您输入的命令名应该是有效的系统命令。您可以通过打开终端并输入命令来检查其是否有效。用于打开应用程序的命令不能与应用程序本身的名称相同。</p>

  <p>如果您要更改与自定义键盘快捷键相关联的命令，请点击该快捷键所在的行。将出现<gui>设置自定义快捷键</gui>窗口，然后您可以编辑该命令。</p>

</section>

</page>
