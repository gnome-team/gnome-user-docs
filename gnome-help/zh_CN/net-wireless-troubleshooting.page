<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu wiki 文档贡献者</name>
    </credit>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>确认并解决无线连接问题。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>无线网络故障排除</title>

  <p>这是一个逐步的故障排除指南，可以帮助您确认和解决无线问题。如果您由于某种原因无法连接到无线网络，请尝试按照这里的说明进行操作。</p>

  <p>我们将通过以下步骤将您的计算机连接到互联网：</p>

  <list style="numbered compact">
    <item>
      <p>执行最初的检查</p>
    </item>
    <item>
      <p>收集硬件相关信息</p>
    </item>
    <item>
      <p>检测硬件</p>
    </item>
    <item>
      <p>尝试创建一个到无线路由器的连接</p>
    </item>
    <item>
      <p>检测调制解调器和路由器</p>
    </item>
  </list>

  <p>要开始操作，请单击页面右上角的<em>下一步</em>链接。此链接及后续页面上的类似链接将带领您完成本指南中的各个步骤。</p>

  <note>
    <title>使用命令行</title>
    <p>本指南中的一些说明要求您在<em>命令行</em>（终端）中输入命令。您可以在<gui>活动</gui>概览中找到<app>终端</app>应用程序。</p>
    <p>如果您不熟悉命令行，请不要担心——本指南将指导您完成每一步。您需要记住的是，命令是区分大小写的（所以必须<em>完全</em>按照给出的样子输入），并在输入完成后按 <key>Enter</key> 来运行它。</p>
  </note>

</page>
