<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>自动控制屏幕亮度以省电。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>启用自动亮度</title>

  <p>如果您的电脑集成有光线传感器，它可以用来自动设置屏幕亮度。这样可以确保屏幕在不同环境的光线条件下始终容易看清，并有助于减少电池消耗。</p>

  <steps>

    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>电源</gui>。</p>
    </item>
    <item>
      <p>点击<gui>电源</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>节电选项</gui>部分，确保<gui>自动亮度</gui>开关已设置为开启状态。</p>
    </item>

  </steps>

  <p>要禁用自动屏幕亮度，请将其切换为关闭状态。</p>

</page>
