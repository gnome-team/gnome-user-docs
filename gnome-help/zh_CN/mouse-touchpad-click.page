<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>
    <revision version="gnome:46" date="2024-04-22" status="draft"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Emanuel Cisár</name>
      <email>536429@mail.muni.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>在触摸板上点击、拖动、滚动等手势操作。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>用触摸板点击、拖动、滚屏操作</title>

  <p>您可以直接在触摸板上进行单击、双击、拖动和滚动操作，而不需要切换到按键上。</p>

  <note>
    <p><link xref="touchscreen-gestures">触摸板手势</link>会单独介绍。</p>
  </note>

<section id="secondary-click">
  <title>副键点击</title>

  <p>您可以自定义触摸板的副键点击（右键单击）操作。</p>

  <list type="bullet">
    <item>
      <p>双指推动：用两根手指在任意位置推动。</p>
    </item>
    <item>
      <p>角落推动：用一根手指在角落处推动。</p>
    </item>
  </list>

  <steps>
    <title>选择副键点击的方式</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>鼠标和触摸板</gui>。</p>
    </item>
    <item>
      <p>点击<gui>鼠标和触摸板</gui>打开面板。</p>
    </item>
    <item>
      <p>点击顶栏中的<gui>触摸板</gui>。</p>
    </item>
    <item>
      <p>在<gui>点击</gui>中，选择首选的点击方式。</p>
    </item>
  </steps>
</section>

<section id="tap">
  <title>轻触点击</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>您可以轻点触摸板来代替使用鼠标进行点击。</p>

  <list>
    <item>
      <p>要点击，在触摸板上触击。</p>
    </item>
    <item>
      <p>要双击，触击两次。</p>
    </item>
    <item>
      <p>要拖动一个项目，双击该项目但第二次点击后不要抬起手指。将物品拖动到想要的位置后抬起手指将其放下。</p>
    </item>
    <item>
      <p>如果您的触摸板支持多指点击，则可以通过两指同时点击来触发右键单击动作。否则您需要按下实体右键方可完成动作。请参阅<link xref="a11y-right-click"/>获得更多关于模拟右键点击的信息。</p>
    </item>
    <item>
      <p>如果您的触摸板支持多指点击，三指同时点击可以模拟<link xref="mouse-middleclick">中键点击</link>动作。</p>
    </item>
  </list>

  <note>
    <p>当用多指触击或拖动时，请确保您的手指分开足够的距离。如果手指太近，计算机可能会将其误认为单指操作。</p>
  </note>

  <steps>
    <title>启用轻触点击</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>鼠标和触摸板</gui>。</p>
    </item>
    <item>
      <p>点击<gui>鼠标和触摸板</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>触摸板</gui>部分，确保<gui>触摸板</gui>开关为开启状态。</p>
      <note>
        <p><gui>触摸板</gui>部分仅在您系统中包含触摸板的时候才可见。</p>
      </note>
    </item>
   <item>
      <p>切换<gui>轻触点击</gui>开关为开启状态。</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>双指滚动</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>您可以在触摸板上使用两根手指滚屏。</p>

  <p>选择此选项后，用单指轻触和拖动和平常操作一样，但如果您用两根手指在触摸板上拖动，则会自动滚动。在触摸板的顶部和底部之间移动手指可上下滚动，而在触摸板上横向移动手指可横向滚动。注意手指之间要有一定的间隔。如果手指靠得太近，触摸板可能将其识别为一根大的手指在操作。</p>

  <note>
    <p>双指滚动可能在有些触摸板上无法工作。</p>
  </note>

  <steps>
    <title>启用双指滚动</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>鼠标和触摸板</gui>。</p>
    </item>
    <item>
      <p>点击<gui>鼠标和触摸板</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>触摸板</gui>部分，确保<gui>触摸板</gui>开关为开启状态。</p>
    </item>
    <item>
      <p>切换<gui>双指滚动</gui>开关为开启状态。</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>边缘滚动</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>如果您只想用一根手指滚动，请使用边缘滚动。</p>

  <p>您的触摸板的使用说明应该提供了用于边缘滚动的传感器的确切位置。通常，垂直滚动传感器位于触摸板的右侧。水平传感器位于触摸板的底部边缘。</p>

  <p>若要垂直滚动，请在触摸板的右边缘上下拖动手指。若要水平滚动，请在触摸板的底部边缘拖动手指。</p>

  <note>
    <p>边缘滚动可能不适用于所有触摸板。</p>
  </note>

  <steps>
    <title>启用边缘滚动</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>鼠标和触摸板</gui>。</p>
    </item>
    <item>
      <p>点击<gui>鼠标和触摸板</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>触摸板</gui>部分，确保<gui>触摸板</gui>开关为开启状态。</p>
    </item>
    <item>
      <p>切换<gui>边缘滚动</gui>开关为开启状态。</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>自然滚动</title>

  <p>您可以使用触摸板拖动内容，像移动一张真正的纸一样。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>鼠标和触摸板</gui>。</p>
    </item>
    <item>
      <p>点击<gui>鼠标和触摸板</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>触摸板</gui>部分，确保<gui>触摸板</gui>开关已设为开启状态。</p>
    </item>
    <item>
      <p>切换<gui>自然滚动</gui>开关为开启状态。</p>
    </item>
  </steps>

  <note>
    <p>此功能也被称为<em>反向滚动</em>。</p>
  </note>

</section>

</page>
