<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="zh-CN">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>将电脑配置为自动挂起。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>设置自动挂起</title>

  <p>您可以配置电脑在闲置时自动挂起。可以指定以电池供电或插电运行时的不同时间间隔。</p>

  <steps>

    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>电源</gui>。</p>
    </item>
    <item>
      <p>点击<gui>电源</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>节电选项</gui>部分，点击<gui>自动挂起</gui>。</p>
    </item>
    <item>
      <p>选择<gui>使用电池时</gui>或<gui>已插入电源</gui>，将开关设置为开启状态，并选择<gui>延迟</gui>。这两个选项都可以进行配置。</p>

      <note style="tip">
        <p>在台式机上，有一个名为<gui>空闲时</gui>的选项。</p>
      </note>
    </item>

  </steps>

</page>
