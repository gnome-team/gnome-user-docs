<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>改变息屏时间以节省电力。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>设置息屏时间</title>

  <p>为了省电，您可以调整空闲时息屏前的时间。您也可以完全禁用息屏。</p>

  <steps>
    <title>设置屏幕息屏时间：</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>电源</gui>。</p>
    </item>
    <item>
      <p>点击<gui>电源</gui>打开面板。</p>
    </item>
    <item>
      <p>使用 <gui>节电选项</gui> 下的 <gui>息屏</gui> 下拉列表来设置息屏时间，或完全禁用息屏。</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>当您的计算机处于空闲状态时，出于安全考虑，其屏幕会自动锁定。要更改此行为，请参阅<link xref="session-screenlocks"/>。</p>
  </note>

</page>
