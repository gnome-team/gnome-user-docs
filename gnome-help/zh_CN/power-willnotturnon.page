<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>接线松了和硬件故障是可能的原因。</desc>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>我的计算机不能开启</title>

<p>有多种原因造成计算机不能开启，本主题简要给出一些可能的原因。</p>
	
<section id="nopower">
  <title>计算机没有插电，电池没电或者电源插头松了</title>
  <p>确保计算机电源插头牢固接在插座上，并且电源是通的，确保显示器接好也是打开的。如果是笔记本电脑，连接上充电插座（以免电池没电）。您还可以检查电池是否正确接入（检查笔记本的一边）。</p>
</section>

<section id="hardwareproblem">
  <title>计算机硬件错误</title>
  <p>您的电脑的部件可能已损坏或出现故障。如果是这种情况，您将需要对电脑进行维修。常见故障包括电源单元损坏、部件（如内存或 RAM）安装出错以及主板故障。</p>
</section>

<section id="beeps">
  <title>计算机报警然后关闭</title>
  <p>如果计算机在开机时发出数次警报声随后自动关机（或无法启动），则可能表明它已检测到问题。这些警报声有时被称为<em>哔声代码（beep codes）</em>，警报声的模式旨在告诉您计算机的问题所在。不同的制造商使用不同的哔声代码，因此您必须查阅计算机主板的手册，或将其送去维修。</p>
</section>

<section id="fans">
  <title>计算机风扇在转，但是屏幕上无显示</title>
  <p>首先检查显示器插头是否接好，开关是否打开。</p>
  <p>这个错误也可能是硬件问题，当您按下电源开关风扇通电，但是其他重要部分开启失败，如果是这样，请送去维修。</p>
</section>

</page>
