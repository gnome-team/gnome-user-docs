<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-closelid" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>笔记本电脑会在盖子合上后进入睡眠以省电。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>为什么我合上笔记本盖时它会关闭？</title>

  <p>当您关闭笔记本的盖子时，您的计算机将<link xref="power-suspend"><em>挂起</em></link>以省电。这意味着计算机实际上并没有被关闭，只是进入了睡眠状态。您可以通过打开盖子唤醒它。如果不能唤醒，请尝试点击鼠标或按下某个键。如果还是不行，那就按电源键。</p>

  <p>一些计算机不能很好地挂起，这通常是因为硬件不完全支持操作系统（例如，Linux 驱动不完善）。在这种情况下，您可能会发现合上盖再打开时，不能唤醒电脑。您可以试着<link xref="power-suspendfail">修复挂起错误</link>，或者您可以阻止电脑在合上盖时挂起。</p>

<section id="nosuspend">
  <title>防止计算机合盖时挂起</title>

  <note style="important">
    <p>这些说明只有在您使用 <app>systemd</app> 的前提下才有效。请联系您的发行版获取更多信息。</p>
  </note>

  <note style="important">
    <p>您需要在电脑上安装<app>优化</app>才能更改此设置。</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">安装<app>优化</app></link></p>
    </if:if>
  </note>

  <p>如果不想计算机在关闭盖子时挂起，您可以更改相应的设置。</p>

  <note style="warning">
    <p>更改这些设置要非常小心，一些笔记本合上盖之后，如果它们还在运行，电脑会变得很热，尤其是装在像包里这样狭小的地方。</p>
  </note>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>优化</gui>。</p>
    </item>
    <item>
      <p>点击<gui>优化</gui>打开应用程序。</p>
    </item>
    <item>
      <p>选择<gui>常规</gui>标签。</p>
    </item>
    <item>
      <p>将<gui>笔记本电脑盖子关闭时挂起</gui>开关切换为关闭状态。</p>
    </item>
    <item>
      <p>关闭<gui>优化</gui>窗口。</p>
    </item>
  </steps>

</section>

</page>
