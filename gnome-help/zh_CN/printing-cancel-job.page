<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>取消一个打印任务，并从队列中删除。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>取消、暂停或释放打印任务</title>

  <p>您可以在打印机设置中取消待处理的打印任务，并将其从队列中移除。</p>

  <section id="cancel-print-job">
    <title>取消打印任务</title>

  <p>如果您不小心开始打印文档，可以取消打印，这样就不用浪费墨水和纸张了。</p>

  <steps>
    <title>如何取消打印任务：</title>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>打印机</gui>。</p>
    </item>
    <item>
      <p>点击<gui>打印机</gui>打开面板。</p>
    </item>
    <item>
      <p>点击打印机旁边显示作业数的按钮。</p>
    </item>
    <item>
      <p>点击停止按钮取消打印任务。</p>
    </item>
  </steps>

  <p>如果此操作无法按您预期的那样取消打印任务，则尝试按住打印机上的<em>取消</em>按钮。</p>

  <p>作为最后的手段，特别是当您有大量的打印任务，且有很多页面无法取消时，请从打印机的纸盒中取出纸张。这样打印机应该会意识到缺纸并停止打印。然后您可以尝试再次取消打印任务，或者关闭打印机后再打开。</p>

  <note style="warning">
    <p>要注意取纸时不要损坏打印机。如果您感觉要用很大力气才能将其取出，最好就别动它了。</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>暂停和释放打印任务</title>

  <p>如果想暂停或释放打印任务，您可以通过打开打印机设置中的任务对话框并点击相应的按钮来实现。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>打印机</gui>。</p>
    </item>
    <item>
      <p>点击<gui>打印机</gui>打开面板。</p>
    </item>
    <item>
      <p>点击打印机旁边显示作业数的按钮。</p>
    </item>
    <item>
      <p>点击相应的按钮暂停或取消打印任务。</p>
    </item>
  </steps>

  </section>

</page>
