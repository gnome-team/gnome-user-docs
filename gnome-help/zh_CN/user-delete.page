<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>删除您计算机上不再使用的用户。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>删除用户账号</title>

  <p>您可以<link xref="user-add">在您的计算机上添加多个用户账号</link>。如果有用户不再使用您的计算机，您可以删除该用户的账号。</p>

  <p>您需要<link xref="user-admin-explain">管理员权限</link>来删除用户账号。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>系统</gui>。</p>
    </item>
    <item>
      <p>从结果中选择<guiseq><gui>设置</gui><gui>系统</gui></guiseq>。这将打开<gui>系统</gui>面板。</p>
    </item>
    <item>
      <p>选择 <gui>用户</gui> 以打开面板。</p>
    </item>
    <item>
      <p>按右上角的<gui style="button">解锁</gui>并在提示时输入密码。</p>
    </item>
    <item>
      <p>在 <gui>其它用户</gui> 下点击要删除的用户账号。</p>
    </item>
    <item>
      <p>点击 <gui style="button">移除用户...</gui> 以删除该用户账号。</p>
    </item>
    <item>
      <p>每个用户的文件和设置都存放在自己的主文件夹中。您可以选择保留或删除用户的主文件夹。如果您确定不再使用这些文件，并且需要释放磁盘空间，请单击<gui>删除文件</gui>。这些文件将被永久删除。它们无法恢复。在删除这些文件之前，您可能需要将其备份到外部存储设备。</p>
    </item>
  </steps>

</page>
