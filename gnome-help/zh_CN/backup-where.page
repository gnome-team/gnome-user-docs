<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>有关在何处存储备份以及使用何种存储设备的建议。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>在何处存储备份</title>

  <p>您应该将文件的备份副本存储在独立于您计算机的地方，例如，存储在外部硬盘上。这样，如果电脑坏了、丢失或被盗，备份仍将完好无损。为了最大程度的安全，您不应该将备份与您的计算机放在同一建筑物内。如果发生火灾或盗窃，两份放在一起的数据副本可能会一起丢失。</p>

  <p>选择一种适当的<em>备份介质</em>也很重要。您需要将备份存储在具有足够磁盘容量存放所有备份文件的设备上。</p>

   <list style="compact">
    <title>本地和远程存储选项</title>
    <item>
      <p>U盘（容量较小）</p>
    </item>
    <item>
      <p>内置硬盘（容量较大）</p>
    </item>
    <item>
      <p>外接硬盘（容量通常较大）</p>
    </item>
    <item>
      <p>网络驱动器（容量较大）</p>
    </item>
    <item>
      <p>文件/备份服务器（容量较大）</p>
    </item>
    <item>
     <p>可擦写 CD 或 DVD 光盘（低/中等容量）</p>
    </item>
    <item>
     <p>在线备份服务（如，<link href="http://aws.amazon.com/s3/">Amazon S3</link>；容量与收费相关）</p>
    </item>
   </list>

  <p>其中的一些选项具有足够的容量，可以备份您系统上的所有文件，因此也被称为<em>完整系统备份</em>。</p>
</page>
