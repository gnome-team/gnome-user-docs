<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-profile" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Equilibre o desempenho e o uso da bateria.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Escolhendo um perfil de energia</title>

  <p>Você pode gerenciar o uso de energia escolhendo um <gui>Modo de energia</gui>. Existem três perfis possíveis:</p>
  
  <list>
    <item>
      <p><gui>Desempenho</gui>: Alto desempenho e uso de energia. Este modo só estará visível se for compatível com o seu computador. Ele será selecionável se o seu computador estiver funcionando com alimentação CA. Se o seu dispositivo suportar detecção de volta, você não poderá selecionar esta opção se o dispositivo estiver em uso no colo de alguém.</p>
    </item>
    <item>
      <p><gui>Balanceada</gui>: Desempenho e uso de energia padrão. Esta é a configuração padrão.</p>
    </item>
    <item>
      <p><gui>Economia de energia</gui>: Desempenho e consumo de energia reduzidos. Essa configuração maximiza a duração da bateria. Ele pode ativar algumas opções de economia de energia, como escurecer agressivamente a tela e impedir que seja desligada.</p>
    </item>
  </list>

  <steps>
    <title>Para escolher um perfil de energia:</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Energia</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Modo de energia</gui>, escolha um dos perfis.</p>
    </item>
    
  </steps>

</page>
