<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Use o leitor de tela <app>Orca</app> para falar a interface com o usuário.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Lendo a tela em voz alta</title>

  <p>O leitor de tela <app>Orca</app> pode narrar a interface para o usuário. Dependendo de como você instalou seu sistema, você pode não ter o Orca instalado. Se não tiver, instale o Orca primeiro.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Instalar Orca</link></p>
  
  <p>Para iniciar o <app>Orca</app> usando o teclado:</p>
  
  <steps>
    <item>
    <p>Pressione <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Ou para iniciar o <app>Orca</app> usando um mouse e teclado:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Acessibilidade</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione a categoria <gui>Visão</gui>.</p>
    </item>
    <item>
      <p>Ative o <gui>Leitor de Tela</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ative e desative rapidamente o leitor de tela</title>
    <p>Você pode ativar e desativar rapidamente o leitor de tela clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Leitor de tela</gui>.</p>
  </note>

  <p>Consulte a <link href="help:orca">Ajuda do Orca</link> para mais informações.</p>
</page>
