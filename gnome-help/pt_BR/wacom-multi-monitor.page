<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="pt-BR">

  <info>
    <revision version="gnome:46" date="2024-03-10" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Associe o tablet Wacom para um monitor específico.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Escolhendo um monitor</title>

<steps>
  <item>
    <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Tablet Wacom</gui>.</p>
  </item>
  <item>
    <p>Clique em <gui>Tablet Wacom</gui> para abrir o painel.</p>
    <note style="tip"><p>Se nenhum tablet for detectado, você será solicitado a <gui>Conectar ou ligar seu tablet Wacom</gui>. Clique em <gui>Bluetooth</gui> na barra lateral para conectar um tablet sem fio.</p></note>
  </item>
  <item>
    <p>Clique em <gui>Mapear para o monitor</gui> e selecione o monitor que você deseja receber informações de sua mesa digitalizadora, ou escolha <gui>Todas as telas</gui>.</p>
    <note style="tip"><p>Apenas os monitores que estão configurados serão selecionáveis.</p></note>
  </item>
  <item>
    <p>Ative <gui>Manter proporção</gui> para combinar a área de desenho do tablet com as proporções do monitor. Essa configuração, também chamada de <em>forçar proporções</em>, cria “letterboxes” na área de desenho em um tablet para corresponder mais diretamente a uma exibição. Por exemplo, um tablet 4∶3 seria mapeado de modo que a área de desenho correspondesse a uma tela widescreen.</p>
  </item>
</steps>

</page>
