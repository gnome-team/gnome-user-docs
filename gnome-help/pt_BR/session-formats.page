<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Escolha uma região usada para data e hora, numeração, unidade monetária e de medidas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando os formatos de data e medidas</title>

  <p>Você pode controlar os formatos que são usados para datas, horas, numerais, unidade monetária e unidades de medida para corresponder aos costumes locais de sua região.</p>

  <steps>
    <item>
      <p>Abra o Panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Sistema</gui>.</p>
    </item>
    <item>
      <p>Selecione <guiseq><gui>Configurações</gui><gui>Sistema</gui></guiseq> nos resultados. Isto abrirá o painel <gui>Sistema</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Região e idioma</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Sua conta</gui>, clique em <gui>Formatos</gui>.</p>
    </item>
    <item>
      <p>Sob <gui>Formatos comuns</gui>, selecione a região e o idioma que mais se aproximam dos formatos que você gostaria de usar.</p>
    </item>
    <item>
      <p>Clique em <gui style="button">Concluído</gui> para salvar.</p>
    </item>
    <item>
      <p>Sua sessão precisa ser reiniciada para que as alterações tenham efeito; Clique em <gui style="button">Reiniciar…</gui> ou reinicie manualmente a sessão posteriormente.</p>
    </item>
  </steps>

  <p>Depois de ter selecionado a região, a área à direita da lista mostra vários exemplos de como as datas e outros valores são mostrados. Apesar de não mostrado nos exemplos, sua região também controla o dia que inicia a semana nos calendários.</p>

  <note style="tip">
    <p>Se houver múltiplas contas de usuário em seu sistema, há uma seção separada para a <gui>Tela de início de sessão</gui> na seção <gui>Região e idioma</gui>.</p>
  </note>

</page>
