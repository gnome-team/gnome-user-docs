<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mantenha sua conta segura alterando sua senha frequentemente nas configurações da sua conta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando a sua senha</title>

  <p>É uma boa ideia alterar sua senha de tempos em tempos, especialmente se você acha que alguém conhece ela.</p>

  <p>Você precisa de <link xref="user-admin-explain">privilégios administrativos</link> para editar contas de usuários além da sua própria.</p>

  <steps>
    <item>
      <p>Abra o Panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Sistema</gui>.</p>
    </item>
    <item>
      <p>Selecione <guiseq><gui>Configurações</gui><gui>Sistema</gui></guiseq> nos resultados. Isto abrirá o painel <gui>Sistema</gui>.</p>
    </item>
    <item>
      <p>Selecione <gui>Usuários</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Clique no rótulo <gui>·····</gui> ao lado de <gui>Senha</gui>. Se você estiver alterando a senha de um usuário diferente, primeiro você precisará <gui>Desbloquear</gui> o painel e selecionar a conta em <gui>Outros usuários</gui>.</p>
    </item>
    <item>
      <p>Informe sua senha atual e, então, uma nova. Informe sua nova senha outra vez no campo <gui>Verificar nova senha</gui>.</p>
      <p>Você pode pressionar o ícone <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">gerar senha</span></media></gui> para automaticamente gerar uma senha aleatória.</p>
    </item>
    <item>
      <p>Clique em <gui>Alterar</gui>.</p>
    </item>
  </steps>

  <p>Certifique-se de <link xref="user-goodpassword">escolher uma boa senha</link>. Isso lhe ajudará a manter segura sua conta de usuário.</p>

  <note>
    <p>Quando você atualizar sua senha de conta, a senha do chaveiro da sua conta será automaticamente atualizada para ser a mesma que sua senha da conta.</p>
  </note>

  <p>Se você esquecer sua senha, qualquer usuário com privilégios de administrador pode alterá-la para você.</p>

</page>
