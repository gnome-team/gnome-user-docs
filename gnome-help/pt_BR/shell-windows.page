<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="ui" id="shell-windows" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">Janelas</title>
    <desc>Mova e organize suas janelas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Janelas e espaços de trabalho</title>

  <p>Assim como em outros ambientes, o sistema usa janelas para exibir seus aplicativos em execução. Usando ambos panorama de <gui xref="shell-introduction#activities">Atividades</gui> e o <em>dash</em>, você pode inicializar novos aplicativos e controlar janelas ativas.</p>

  <p>Você também pode agrupar seus aplicativos em espaços de trabalho. Visite os tópicos de ajuda sobre janela e espaço de trabalho logo abaixo para aprender mais sobre como usar esses recursos.</p>
<!-- 
  <p>In the <gui>Activities</gui> overview, the <gui>dash</gui> displays your 
  favorite applications as well as your running applications. 
  The <gui>dash</gui> will place a slight glow behind any running applications.
  </p>
      
  <p>Clicking the application icon will launch it if it is not running.  
  If it is already running, clicking the application will open the last used 
  window of that application.</p>

  <p>Right clicking the application icon for a running application will 
  bring all windows for that application forward. A menu with the titles of your
  windows will be displayed. You can select a window from this menu. It also 
  provides options to open a new window for that application and to remove or 
  add that application to favorites depending on its current status.</p>

  <p>Windows are shown on their corresponding 
  <link xref="shell-windows-workspaces">workspaces</link>.</p>
-->
<section id="working-with-windows" style="2column">
  <info>
    <title type="link" role="trail">Janelas</title>
  </info>
 <title>Trabalhando com janelas</title>
</section>

<section id="working-with-workspaces" style="2column">
  <info>
    <title type="link" role="trail">Espaços de trabalho</title>
  </info>
 <title>Trabalhando com espaços de trabalho</title>
</section>

</page>
