<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>O recurso de <gui>Clique flutuante</gui> (Clique de permanência) permite que você clique ao manter o mouse parado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Simulando cliques por flutuação</title>

  <p>Você pode clicar ou arrastar simplesmente flutuando o cursor do seu mouse sobre um controle ou objeto na tela. Isso é útil se você tem dificuldade em mover o mouse e clicar ao mesmo tempo. Este recurso é chamado de <gui>Clique flutuante</gui> ou clique de permanência.</p>

  <p>Quando o <gui>Clique flutuante</gui> está habilitado, você pode mover o cursor do seu mouse sobre um controle, soltar o mouse e, então, esperar um momento até que o botão ser clicado por você.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Acessibilidade</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione a seção <gui>Apontamento e clique</gui> para abri-la.</p>
    </item>
    <item>
      <p>Na seção <gui>Assistência de clique</gui>, ative o botão <gui>Clique flutuante</gui>.</p>
    </item>
  </steps>

  <p>A janela do <gui>Clique flutuante</gui> abrirá e ficará acima de todas outras janelas. Você pode usar ela para escolher qual tipo de clique deveria acontecer quando você flutuar. Por exemplo, se você selecionar <gui>Clique secundário</gui>, você fará o clique com botão direito quando flutuar. Depois você fizer clique duplo, clique com botão da direita ou arrastar, você retornará automaticamente para cliques.</p>

  <p>Quando você flutuar o cursor do seu mouse sobre um botão, e não movê-lo mais, o cursor vai gradualmente mudar de cor. Quando estiver com a cor completamente alterada, o botão será clicado.</p>

  <p>Ajuste a configuração do <gui>Intervalo</gui> para alterar por quanto tempo você tem que manter parado o cursor do mouse até que o clique seja realizado.</p>

  <p>Você não precisa segurar o mouse perfeitamente parado, quando estiver flutuando para clicar. O cursor pode mover um pouco e mesmo assim clicar após um tempo. Porém, se mover demais, o clique não será realizado.</p>

  <p>Ajuste o <gui>Limiar de movimento</gui>, definindo-o para alterar o quanto o cursor pode mover e mesmo assim ser considerado como flutuante.</p>

</page>
