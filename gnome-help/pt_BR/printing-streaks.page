<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Se as impressões estiverem com listras, falhas ou faltando cores, confira os níveis da tinta ou limpe a cabeça de impressão.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

  <title>Por que há listras, linhas ou cores incorretas nas minhas impressões?</title>

  <p>Se suas impressões estiverem com listras, com falhas, tiverem linhas nelas que não deveria haver ou estiverem com qualidade ruim, isso pode ocorrer por causa de um problema com a impressora ou nível baixo de tinta ou toner.</p>

  <terms>
     <item>
       <title>Textos e imagens falhando</title>
       <p>Você pode estar ficando sem tinta ou toner. Verifique sua carga de tinta e toner e compre um novo cartucho, se necessário.</p>
     </item>
     <item>
       <title>Listras e linhas</title>
       <p>Se você tem uma impressora de jato de tinta, a cabeça de impressão pode estar suja ou parcialmente bloqueada. Tente limpas a cabeça de impressão. Veja o manual da impressora para instruções.</p>
     </item>
     <item>
       <title>Cores incorretas</title>
       <p>A impressora pode estar sem reserva de uma cor de tinta ou toner. Verifique sua carga de tinta e toner e compre um novo cartucho, se necessário.</p>
     </item>
     <item>
       <title>Linhas irregulares ou linhas não estão retas</title>
       <p>Se linhas em sua impressão que deveriam estar retas aparecem de forma irregular, você pode precisar alinhar a cabeça de impressão. Veja o manual de instrução da impressora para detalhes sobre como fazer isso.</p>
     </item>
  </terms>

</page>
