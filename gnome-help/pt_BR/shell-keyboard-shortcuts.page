<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.29" date="2018-08-27" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conheça o ambiente de trabalho usando o teclado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

<title>Atalhos de teclado úteis</title>

<p>Essa página fornece uma visão geral dos atalhos que podem lhe ajudar a usar seu ambiente e aplicativos de forma mais eficiente. Se você não conseguir usar um mouse ou outro dispositivo de apontamento, veja <link xref="keyboard-nav"/> para mais informações sobre navegação em interfaces de usuário com apenas o teclado.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Conhecendo o ambiente</title>
  <tr xml:id="super">
    <td><p>tecla <key xref="keyboard-key-super">Super</key></p></td>
    <td><p>Alterna entre o panorama de <gui>Atividades</gui> e a área de trabalho. No panorama, comece a digitar para pesquisar instantaneamente seus aplicativos, contatos e documentos.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Faz surgir a janela de comando (para executar rapidamente comandos).</p>
    <p>Usa as teclas de setas para acessar rapidamente comandos executados previamente.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Alterna rapidamente entre janelas</link>. Mantenha o <key>Shift</key> pressionado para uma ordem reversa de alternância.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Super</key><key>`</key></keyseq></p></td>
    <td>
      <p>Alterna entre janelas do mesmo aplicativo ou do aplicativo selecionado depois após <keyseq><key>Super</key><key>Tab</key></keyseq>.</p>
      <p>Este atalho usa <key>`</key> em teclados americanos, sendo que a tecla <key>`</key> está sobre o <key>Tab</key>. Em todos os outros teclados, o atalho é <key>Super</key> mais a tecla que estiver sobre o <key>Tab</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td>
      <p>Alterna rapidamente entre janelas no espaço de trabalho atual. Mantenha o <key>Shift</key> pressionado para uma ordem reversa.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Dá ao teclado o foco na barra superior. No panorama de <gui>Atividades</gui>, alterna o foco do teclado entre a barra superior, o dash, o panorama de janelas, a lista de aplicativos e o campo de pesquisa. Use as teclas de setas para navegar.</p>
    </td>
  </tr>
<!--
  <tr xml:id="ctrl-alt-t">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq></p></td>
    <td>
      <p>Open a Terminal.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-t">
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>T</key></keyseq></p></td>
    <td>
      <p>Open a new Terminal tab on the same window.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-n">
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>N</key></keyseq></p></td>
    <td>
      <p>Open a new Terminal window. To use this shortcut, you should already be on a terminal window.</p>
    </td>
  </tr>
-->
  <tr xml:id="super-a">
    <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
    <td><p>Mostra a lista de aplicativos.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>e</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Down</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Alterna entre espaços de trabalho</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>e</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Move a janela atual para um espaço de trabalho diferente</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Move a janela atual um monitor à esquerda.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Move a janela atual um monitor à esquerda.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Mostra o diálogo de desligar</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Bloquear a tela</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p>Mostra <link xref="shell-notifications#notificationlist">a lista de notificação</link>. Pressione <keyseq><key>Super</key><key>V</key></keyseq> novamente ou <key>Esc</key> para fechar.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Atalhos comuns para edição</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Seleciona todo o texto ou todos os itens em uma lista.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Recorta (remove) o texto selecionado ou os itens e coloca-os na área de transferência.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Copia o texto selecionado ou os itens para a área de transferência.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>Cola o conteúdo da área de transferência.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>Desfaz a última ação.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>C</key></keyseq></p></td>
    <td><p>Copia o texto ou comandos realçados para a área de transferência no Terminal.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>V</key></keyseq></p></td>
    <td><p>Cola o conteúdo da área de transferência no Terminal.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Capturando a tela</title>
  <tr>
    <td><p><key>Print</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Iniciar a ferramenta de captura de tela.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Faz uma captura de uma janela.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Faça uma captura de tela da tela inteira.</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Inicia e finaliza uma gravação de tela.</link></p></td>
  </tr>
</table>

</page>
