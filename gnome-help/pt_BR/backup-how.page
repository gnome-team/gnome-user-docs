<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use Déjà Dup (ou outro aplicativo para fazer backup) para fazer cópias dos seus arquivos e configurações valiosos para se proteger contra perdas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2023.</mal:years>
    </mal:credit>
  </info>

<title>Como fazer backup</title>

  <p>A forma mais fácil de fazer um backup dos seus arquivos e das suas configurações é deixar um aplicativo especializado gerenciar esse processo para você. Há uma boa diversidade de aplicativos de backup disponíveis; por exemplo, o <app>Déjà Dup</app>.</p>

  <p>A ajuda do seu aplicativo escolhido irá guiá-lo sobre como configurar suas preferências para as cópias, e também como restaurar seus dados.</p>

  <p>Uma opção alternativa é <link xref="files-copy">copiar seus arquivos</link> para um local seguro, como um disco rígido externo, um serviço de armazenamento online ou uma unidade USB. Seus <link xref="backup-thinkabout">arquivos pessoais</link> e suas configurações normalmente ficam na pasta pessoal, então você pode copiá-los de lá.</p>

  <p>A quantidade de dados que você pode fazer backup é limitada ao tamanho do dispositivo de armazenamento. Se você tiver espaço no seu dispositivo de segurança, é melhor fazer a cópia de toda a pasta pessoal, com as seguintes exceções:</p>

<list>
 <item><p>Arquivos que já possuem backups em outro lugar, como ema unidade USB ou outra mídia removível.</p></item>
 <item><p>Arquivos que você possa recriar facilmente. Por exemplo, se você for um programador, você não tem que fazer backup dos arquivos que são produzidos quando você compila seus programas. Em vez disso, apenas se certifique de que o backup dos arquivos fontes originais seja feito.</p></item>
 <item><p>Quaisquer arquivos na pasta Lixeira. Sua pasta Lixeira pode ser localizada em <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
