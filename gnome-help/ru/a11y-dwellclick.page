<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="ru">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Функция <gui>Нажатие при наведении</gui> позволяет выполнить нажатие, некоторое время удерживая мышь на одном месте.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Имитация нажатия при наведении указателя</title>

  <p>Можно выполнять нажатия или перетаскивание, просто удерживая указатель мыши над элементом интерфейса или объектом на экране. Это удобно, если вам трудно одновременно перемещать мышь и выполнять нажатие. Эта функция называется <gui>Нажатие при наведении</gui>.</p>

  <p>Если <gui>Нажатие при наведении</gui> включено, то достаточно навести указатель мыши на кнопку, снять руку с мыши и затем подождать некоторое время. Нажатие кнопки будет выполнено автоматически.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Специальные возможности</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Специальные возможности</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите раздел <gui>Наведение и нажатие</gui>, чтобы открыть его.</p>
    </item>
    <item>
      <p>В разделе <gui>Помощник нажатия</gui> переведите переключатель <gui>Нажатие при наведении</gui> в положение «включено».</p>
    </item>
  </steps>

  <p>Откроется окно <gui>Нажатие при наведении</gui> и некоторое время будет оставаться поверх всех других окон. Оно позволит вам выбрать, какой тип нажатия должен быть выполнен при наведении. Например, если выбрать <gui>Второе нажатие</gui>, будет выполнено нажатие правой кнопкой. После выполнения двойного нажатия, нажатия правой кнопкой или перетаскивания, вы автоматически вернётесь в режим одинарного нажатия.</p>

  <p>Если навести указатель мыши на кнопку и держать его неподвижно, его цвет будет постепенно меняться. Когда указатель полностью сменит цвет, будет выполнено нажатие на кнопку.</p>

  <p>Настройте параметр <gui>Задержка</gui>, чтобы изменить время, в течение которого нужно удерживать указатель мыши неподвижно, чтобы произошло нажатие.</p>

  <p>Необязательно удерживать мышь абсолютно неподвижно во время выполнения «нажатия по наведению». Нажатие будет выполнено, даже если курсор немного сместится. Однако, если он сместится слишком сильно, нажатия не произойдёт.</p>

  <p>Настройте параметр <gui>Двигательный порог</gui>, чтобы указать, насколько может смещаться курсор, чтобы всё равно считаться неподвижным.</p>

</page>
