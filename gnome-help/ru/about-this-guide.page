<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="ru">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Несколько советов по использования справочного руководства.</desc>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

<title>Об этом руководстве</title>
<p>Это руководство разработано, чтобы предоставить вам обзор возможностей рабочего стола, ответить на связанные с компьютером вопросы и дать советы по более эффективному использованию компьютера. Вот несколько примечаний относительно справочного руководства:</p>

<list>
  <item><p>Руководство состоит не из глав, а из небольших, ориентированных на конкретную задачу, статей. Это означает, что вам не понадобится просматривать всё руководство, чтобы найти ответ на свой вопрос.</p></item>
  <item><p>Связанные друг с другом темы объединены ссылками. Ссылки «Смотрите также» внизу некоторых страниц перенаправляют на темы, связанные с просматриваемой темой. Это упрощает поиск похожих тем, способных помочь вам в выполнении определённой задачи.</p></item>
  <item><p>Руководство содержит встроенный поиск. Панель наверху окна браузера справки — это <em>панель поиска</em>, в которой по мере набора поискового запроса будут появляться подходящие результаты.</p></item>
  <item><p>Руководство постоянно продолжает улучшаться. Хотя мы стараемся обеспечить вас подробным сборником полезной информации, мы понимаем, что не сможем ответить здесь на все ваши вопросы. Если вам нужна дополнительная помощь, вы можете обратиться в службу поддержки вашего дистрибутива.</p></item>
</list>

</page>
