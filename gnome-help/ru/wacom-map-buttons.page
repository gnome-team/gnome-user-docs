<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-map-buttons" xml:lang="ru">
      
  <info>
    <revision version="gnome:46" date="2024-03-10" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Назначение функций аппаратным кнопкам на графическом планшете.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Привязка кнопок планшена</title>

  <p>Аппаратные кнопки на планшете можно настроить под различные функции.</p>

<steps>
  <item>
    <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Планшет Wacom</gui>.</p>
  </item>
  <item>
    <p>Нажмите <gui>Планшет Wacom</gui>, чтобы открыть этот раздел настроек.</p>
    <note style="tip"><p>Если планшет не обнаружен, вы увидите сообщение <gui>Подключите или включите планшет Wacom</gui>. Нажмите <gui>Bluetooth</gui> на боковой панели, чтобы подключить беспроводной планшет.</p></note>
  </item>
  <item>
    <p>Нажмите <gui>Отображение кнопок</gui>.</p>
  </item>
  <item>
    <p>На экране будет показано расположение кнопок планшета. Нажмите каждую кнопку на планшете и выберите одну из следующих функций:</p>
    <list>
      <item><p><gui>Приложение определено</gui></p></item>
      <item><p><gui>Показывать справку на экране</gui></p></item>
      <item><p><gui>Переключение монитора</gui></p></item>
      <item><p><gui>Событие нажатие клавиши</gui></p></item>
   </list>
  </item>
  <item>
    <p>Нажмите <gui>Готово</gui>, когда каждая кнопка будет настроена, а затем <key>Esc</key> для выхода.</p>
  </item>
</steps>


</page>
