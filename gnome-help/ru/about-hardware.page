<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hardware" xml:lang="ru">

  <info>
    <link type="guide" xref="about" group="hardware"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="draft" date="2024-03-02"/>

    <credit type="author">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Получение информации об оборудовании, установленном в вашей системе.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Получение информации об аппаратной части вашей системы</title>

  <p>Знания об установленном оборудовании может помочь в понимании того, будет ли новое программное обеспечение или оборудование совместимо с вашей системой.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Система</gui>.</p>
    </item>
    <item>
     <p>В результатах выберите <guiseq><gui>Настройки</gui><gui>Система</gui></guiseq>. Откроется панель <gui>Система</gui>.</p>
    </item>
    <item>
      <p>Выберите <gui>О системе</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Посмотрите информацию, указанную в разделах <gui>Модель оборудования</gui>, <gui>Процессор</gui>, <gui>Память</gui> и т.д.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Информацию из каждого раздела можно скопировать: выделив необходимый раздел, а затем скопировать его в буфер обмена. Это упрощает обмен информацией о вашей системе с другими пользователями.</p>
  </note>

</page>
