<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-testing" xml:lang="ru">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-gettingprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>
    
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Используйте идущие в поставке тестовые профили для проверки корректности применения профилей к вашему монитору.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Как проверить, правильно ли работает управление цветом?</title>

  <p>Иногда эффект от применения цветового профиля едва различимый и трудно увидеть, изменилось ли что-нибудь.</p>

  <p>Система поставляет несколько профилей для тестирования, результат применения которых сразу заметен:</p>

  <terms>
    <item>
      <title>Синий</title>
      <p>Экран станет синим, и будет проведена проверка, посылаются ли на монитор калибровочные кривые.</p>
    </item>
<!--    <item>
      <title>ADOBEGAMMA-test</title>
      <p>This will turn the screen pink and tests different features of a
      screen profile.</p>
    </item>
    <item>
      <title>FakeBRG</title>
      <p>This will not change the screen, but will swap around the RGB channels
      to become BGR. This will make all the colors gradients look mostly
      correct, and there won’t be much difference on the whole screen, but
      images will look very different in applications that support color
      management.</p>
    </item>-->
  </terms>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Цвет</gui> в боковой панели, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите устройство, для которого необходимо добавить цветовой профиль. Возможно, понадобится сделать примечание о том, какой из профилей является текущим.</p>
    </item>
    <item>
      <p>Чтобы добавить тестовый профиль, нажмите <gui>Добавить профиль</gui>. Эти профили обычно находятся внизу списка.</p>
    </item>
    <item>
      <p>Нажмите <gui>Добавить…</gui> для подтверждения выбора.</p>
    </item>
    <item>
      <p>Чтобы вернуться на предыдущий профиль, выберите устройство в разделе <gui>Цвет</gui>, затем выберите профиль, который использовался до того, как был применён один из тестовых профилей, и нажмите <gui>Включить</gui>, чтобы вернуться на него.</p>
    </item>
  </steps>


  <p>Использование этих профилей поможет вам точно узнать, поддерживает ли то или иное приложение управление цветом.</p>

</page>
