<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="ru">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Удалите файлы или папки, которые больше не нужны.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

<title>Удаление файлов и папок</title>

  <p>Если файл или папка больше не нужны, можно удалить их. При удалении объект отправляется в <gui>Корзину</gui>, где будет хранится, пока вы не очистите корзину. Можно <link xref="files-recover">восстановить объекты </link> из <gui>Корзины</gui> в их исходное местоположение, если вы всё же решите, что они нужны, или если они были удалены случайно.</p>

  <steps>
    <title>Чтобы отправить файл в корзину:</title>
    <item><p>Выберите объект, который хотите поместить в корзину, нажав на него.</p></item>
    <item><p>Нажмите клавишу <key>Delete</key>. Вы также можете перетащить объект в <gui>Корзину</gui> в боковой панели.</p></item>
  </steps>

  <p>Файл будет отправлен в корзину. После этого на несколько секунд появится кнопка <gui>Отменить</gui>. Если вы нажмёте на эту кнопку, то файл будет восстановлен и перемещён в исходное местоположение.</p>

  <p>Чтобы окончательно удалить файлы и освободить место, занимаемое ими на компьютере, нужно очистить корзину. Для этого нажмите правой кнопкой на значок <gui>Корзина</gui> в боковой панели и выберите <gui>Очистить корзину</gui>.</p>

  <section id="permanent">
    <title>Окончательное удаление файла</title>
    <p>Можно окончательно удалить файл сразу же, не отправляя его сначала в корзину.</p>

  <steps>
    <title>Чтобы окончательно удалить файл:</title>
    <item><p>Выберите объект, который нужно удалить.</p></item>
    <item><p>Удерживая нажатой клавишу <key>Shift</key>, нажмите <key>Delete</key> на клавиатуре.</p></item>
    <item><p>Поскольку отменить это действие невозможно, вам будет предложено подтвердить, действительно ли вы хотите удалить файл или папку.</p></item>
  </steps>

  <note><p>Удалённые файлы на <link xref="files#removable">съёмных носителях</link> могут быть не видны в других операционных системах, таких как Windows или Mac OS. Тем не менее, файлы никуда не исчезают с носителя и снова будут доступны при повторном подключении носителя к вашему компьютеру.</p></note>

  </section>

</page>
