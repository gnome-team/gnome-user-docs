<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="ru">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>У некоторых беспроводных устройств возникают проблемы с обработкой состояния компьютера, если компьютер входил в режим ожидания и потом вышел из него некорректным образом.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>При «пробуждении» компьютера отсутствует беспроводное соединение</title>

  <p>После перевода компьютера режим ожидания вы можете обнаружить, что при возвращении в рабочий режим беспроводное соединение с Интернетом перестало действовать. Это бывает, когда <link xref="hardware-driver">драйвер</link> беспроводного устройства не полностью поддерживает некоторые функции энергосбережения. Обычно не удаётся правильно включить беспроводное соединение при «пробуждении» компьютера.</p>

  <p>Если это случится, попробуйте выключить и снова включить беспроводное соединение:</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Щёлкните <gui>Wi-Fi</gui>, чтобы открыть панель.</p>
    </item>
    <item>
      <p>Переведите переключатель <gui>Wi-Fi</gui> в правом верхнем углу окна в положение «выключено», а затем снова включите его.</p>
    </item>
    <item>
      <p>Если беспроводное соединение всё равно не работает, попробуйте включить и снова отключить <gui>Авиарежим</gui>.</p>
    </item>
  </steps>

  <p>Если ничего не помогло, беспроводная сеть должна снова заработать после перезагрузки компьютера. Если и после перезагрузки проблема не исчезла, подключитесь к интернету с помощью проводного соединения и обновите систему.</p>

</page>
