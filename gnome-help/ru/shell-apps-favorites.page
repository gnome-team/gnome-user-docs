<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="ru">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Добавление (или удаление) значков часто используемых приложений из панели задач.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Добавление приложений в панель задач</title>

  <p>Чтобы добавить приложение на <link xref="shell-introduction#activities">панель задач</link> для быстрого доступа:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Войдите в режим <gui xref="shell-introduction#activities">Обзора</gui>, нажав кнопку <gui>Обзор</gui> в левом верхнем углу экрана</p>
      <p if:test="platform:gnome-classic">Щёлкните меню <gui xref="shell-introduction#activities">Приложения</gui> в левом верхнем углу экрана и выберите в пункт <gui>Обзор</gui>.</p></item>
    <item>
      <p>Нажмите кнопку с изображением сетки в панели задач («Показать приложения») и найдите приложение, которое нужно добавить.</p>
    </item>
    <item>
      <p>Нажмите на значок приложения правой кнопкой и выберите <gui>Добавить в избранное</gui>.</p>
      <p>Также можно просто перетащить значок на панель задач.</p>
    </item>
  </steps>

  <p>Чтобы удалить значок приложения из панели задач, нажмите правой кнопкой мыши по значку приложения и выберите <gui>Удалить из избранного</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Избранные приложения также отображаются в пункте <gui>Избранное</gui> меню <gui xref="shell-introduction#activities">Приложения</gui>.</p>
  </note>

</page>
