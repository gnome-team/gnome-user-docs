<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="ru">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Клавиша <key>Super</key> открывает режим <gui>Обзора</gui>. На клавиатуре она обычно находится рядом с клавишей <key>Alt</key>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Что такое клавиша <key>Super</key>?</title>

  <p>Нажатие на клавишу <key>Super</key> открывает <gui>Обзор</gui>. Она располагается, как правило, в левом нижнем углу клавиатуры, рядом с клавишей <key>Alt</key>, на многих клавиатурах на клавише <key>Super</key> изображён логотип Windows. Иногда её называют <em>клавишей Windows</em> или <em>системной клавишей</em>.</p>

  <note>
    <p>На клавиатурах Apple вместо клавиши Windows расположена клавиша командная клавиша <key>⌘</key>, а на Chromebook — клавиша со значком лупы.</p>
  </note>

  <p>Чтобы для открытия <gui>Обзора</gui> назначить другую клавишу:</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Клавиатура</gui> на боковой панели, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>В разделе <gui>Комбинации клавиш</gui> выберите <gui>Просмотр и изменение комбинаций клавиш</gui>.</p>
    </item>
    <item>
      <p>Выберите категорию <gui>Система</gui>.</p>
    </item>
    <item>
      <p>Щёлкните строку <gui>Показать обзор</gui>.</p>
    </item>
    <item>
      <p>Нажмите нужную комбинацию клавиш.</p>
    </item>
  </steps>

</page>
