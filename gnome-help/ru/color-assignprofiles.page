<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="ru">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Добавить цветовой профиль монитора можно в разделе <guiseq><gui>Настройки</gui><gui>Цвет</gui></guiseq>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Как назначать профили устройствам?</title>

  <p>Возможно, вы захотите назначить цветовой профиль монитору или принтеру, для более качественного отображения цветов.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Цвет</gui> в боковой панели, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите устройство, для которого необходимо добавить цветовой профиль.</p>
    </item>
    <item>
      <p>Чтобы выбрать уже существующий профиль или импортировать новый, нажмите кнопку <gui>Добавить профиль</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Добавить…</gui> для подтверждения выбора.</p>
    </item>
    <item>
      <p>Чтобы изменить используемый профиль, выберите необходимый вам профиль и нажмите <gui>Включить</gui>, чтобы подтвердить свой выбор.</p>
    </item>
  </steps>

  <p>Каждому устройству может быть назначено несколько профилей, но только один из них может быть профилем <em>по умолчанию</em>. Профиль по умолчанию используется, когда отсутствует дополнительная информация, позволяющая выбрать профиль автоматически. Примером автоматического выбора может быть, если один профиль был создан для глянцевой, а другой — для обычной бумаги.</p>

  <p>Если подключено калибровочное устройство, кнопка <gui>Калибровать…</gui> создаст новый профиль.</p>

</page>
