<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-device-drivers" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Люди, делающие вклад в wiki документации для Ubuntu</name>
    </credit>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Некоторые драйверы устройств не очень хорошо работают с определёнными беспроводными адаптерами, и вам может понадобиться найти более подходящие.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Устранение неполадок с беспроводной сетью</title>

  <subtitle>Проверьте, установлены ли рабочие драйверы устройства</subtitle>

<!-- Needs links (see below) -->

  <p>На этом этапе можно проверить доступность рабочих драйверов для вашего беспроводного адаптера. <em>Драйвер устройства</em> — это программа, которая рассказывает компьютеру о том, как сделать чтобы аппаратное устройство заработало правильно. Даже если компьютер распознал беспроводной адаптер, у компьютера может не быть драйверов для его правильной работы. Можно найти несколько работающих драйверов, попробовав некоторые опции ниже:</p>

  <list>
    <item>
      <p>Проверьте, есть ли ваш беспроводной адаптер в списке поддерживаемых устройств.</p>
      <p>У большинства дистрибутивов Linux существуют списки поддерживаемых в них беспроводных устройств. Иногда эти списки содержат дополнительную информацию о том, как заставить некоторые адаптеры работать правильно. Найдите список для вашего дистрибутива (например, <link href="https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported">Ubuntu</link>, <link href="https://wiki.archlinux.org/index.php/Wireless_network_configuration">Arch</link>, <link href="https://wireless.wiki.kernel.org/en/users/Drivers">Fedora</link> or <link href="http://en.opensuse.org/HCL:Network_(Wireless)">openSUSE</link>) и проверьте, содержитcя ли в нём производитель и модель вашего беспроводного адаптера. Возможно, вы найдёте там какую-то информацию, которая поможет обеспечить правильную работу драйвера вашего устройства.</p>
    </item>
    <item>
      <p>Поищите проприетарные (двоичные) драйверы.</p>
      <p>Многие дистрибутивы Linux содержат лишь <em>свободные</em> драйверы устройств <em>с открытым исходным кодом</em>, так как они не имеют права распространять проприетарные драйверы, исходный код которых закрыт. Если драйвер вашего беспроводного адаптера доступен только в виде несвободной «только двоичной» версии, он может не устанавливаться по умолчанию. В этом случае посмотрите на веб-сайте производителя беспроводного адаптера, чтобы узнать, есть ли у них какие-либо драйверы для Linux.</p>
      <p>В некоторых дистрибутивах Linux есть инструмент, который может загрузить для вас проприетарные драйверы. Если он имеется в вашем дистрибутиве, используйте его, чтобы проверить, не найдёт ли он драйверы для беспроводного устройства.</p>
    </item>
    <item>
      <p>Используйте для своего адаптера драйверы для Windows.</p>
      <p>В целом, нельзя использовать драйвер устройства, разработанный дли одной операционной системы (например, Windows) в другой операционной системе (например, Linux), так как они используют отличающиеся методы работы с устройствами. Но для беспроводных адаптеров можно установить слой совместимости (так называемый <em>NDISwrapper</em>), который позволяет использовать некоторые беспроводные драйверы для Windows в Linux. Это полезно, поскольку беспроводные адаптеры почти всегда имеют драйверы для Windows, а драйверы для Linux иногда недоступны. Узнать больше об использовании NDISwrapper можно <link href="https://sourceforge.net/p/ndiswrapper/ndiswrapper/Main_Page/">здесь</link>. Обратите внимание, что не все беспроводные драйверы можно использовать с NDISwrapper.</p>
    </item>
  </list>

  <p>Если ни один из этих вариантов не помог, можете попробовать другой беспроводной адаптер, чтобы проверить, не будет ли он работать. Беспроводные USB адаптеры обычно достаточно дёшевы и их можно подключить к любому компьютеру. Однако перед покупкой нужно проверить, совместим ли данный адаптер с вашим дистрибутивом Linux.</p>

</page>
