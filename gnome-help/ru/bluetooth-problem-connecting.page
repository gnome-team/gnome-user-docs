<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="ru">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Адаптер может быть отключен или для него нет драйверов, либо Bluetooth может быть отключен или заблокирован.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Я не могу подключить моё устройство Bluetooth</title>

  <p>Может существовать ряд причин, препятствующих подключению к устройству Bluetooth, такому как телефон или гарнитура.</p>

  <terms>
    <item>
      <title>Соединение заблокировано или не является доверенным</title>
      <p>Некоторые устройства Bluetooth по умолчанию блокируют подключения или требуют от вас изменить настройки, чтобы выполнить подключение. Убедитесь, что настройки вашего устройства позволяют подключаться к нему.</p>
    </item>
    <item>
      <title>Оборудование Bluetooth не распознано</title>
      <p>Ваш адаптер или приёмник Bluetooth может быть не распознан компьютером. Это может произойти из-за того, что не установлены <link xref="hardware-driver">драйверы</link> для адаптера. Некоторые адаптеры Bluetooth не поддерживаются в Linux, и вы не сможете найти для них подходящих драйверов. В таком случае, вероятно, придётся купить другой адаптер Bluetooth.</p>
    </item>
    <item>
      <title>Адаптер не включён</title>
        <p>Проверьте, включен ли адаптер Bluetooth. Откройте панель Bluetooth и убедитесь, что он не <link xref="bluetooth-turn-on-off">отключен</link>.</p>
    </item>
    <item>
      <title>Выключено соединение с Bluetooth на устройстве</title>
      <p>Проверьте, включён ли Bluetooth на устройстве, с которым вы пытаетесь соединиться, и что оно является <link xref="bluetooth-visibility">обнаруживаемым или видимым</link>. Например, при попытке соединиться с телефоном, убедитесь, что авиарежим отключён.</p>
    </item>
    <item>
      <title>В вашем компьютере нет адаптера Bluetooth</title>
      <p>Многие компьютеры не оснащены адаптерами Bluetooth. Чтобы использовать Bluetooth, купите адаптер.</p>
    </item>
  </terms>

</page>
