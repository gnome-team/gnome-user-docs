<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Подключение к скрытой беспроводной сети, отсутствующей в списке.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

<title>Подключение к скрытой беспроводной сети</title>

<p>Возможно также настроить «скрытую» беспроводную сеть. Скрытые сети не показываются в списке доступных сетей в окне настроек <gui>Сеть</gui>. Чтобы подключиться к скрытой беспроводной сети:</p>

<steps>
  <item>
    <p>Откройте <gui xref="shell-introduction#systemmenu">системное меню</gui> справа в верхней панели.</p>
  </item>
  <item>
    <p>Выберите стрелку <gui>Wi-Fi</gui>. Раздел Wi-Fi в меню развернётся.</p>
  </item>
  <item>
    <p>Нажмите <gui>Все сети</gui>.</p>
  </item>
  <item><p>Нажмите кнопку меню в правом верхнем углу окна и выберите <gui>Подключиться к скрытой сети…</gui>.</p></item>
 <item>
  <p>В появившемся окне в выпадающем списке <gui>Подключение</gui> выберите скрытую сеть, с которой ранее было установлено соединение, или пункт <gui>Создать</gui> для новой сети.</p>
 </item>
 <item>
  <p>Введите имя сети для нового соединения и выберите способ защиты беспроводного соединения из выпадающего списка <gui>Защита Wi-Fi</gui>.</p>
 </item>
 <item>
  <p>Введите свой пароль или другие параметры безопасности.</p>
 </item>
 <item>
  <p>Нажмите <gui>Подключиться</gui>.</p>
 </item>
</steps>

  <p>Возможно, вам понадобится проверить параметры беспроводной точки доступа или маршрутизатора, чтобы узнать имя сети. Если этого имени (SSID) у вас нет, то можно использовать <em>BSSID</em> (Basic Service Set Identifier, аппаратный или MAC-адрес точки доступа), который выглядит примерно как <gui>02:00:01:02:03:04</gui>. Обычно его можно найти на нижней стороне точки доступа.</p>

  <p>Следует также проверить параметры защиты беспроводной базовой станции. Ищите такие термины, как WEP и WPA.</p>

<note>
 <p>Вы можете подумать, что скрытие вашей беспроводной сети улучшит безопасность, не позволяя подключиться людям, которые не знают о ней. На практике, это не так: обнаружить сеть немного сложнее, но всё же возможно.</p>
</note>

</page>
