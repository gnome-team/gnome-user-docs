<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="ru">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>
    <revision version="gnome:46" date="2024-04-22" status="draft"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Emanuel Cisár</name>
      <email>536429@mail.muni.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Нажатия, перетаскивание и прокрутка с помощью сенсорной панели.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Нажатия, перетаскивание и прокрутка с помощью сенсорной панели</title>

  <p>Нажатия, двойные нажатия, перетаскивание и прокрутку можно выполнять непосредственно на сенсорной панели, без использования отдельных аппаратных кнопок.</p>

  <note>
    <p><link xref="touchscreen-gestures">Жесты сенсорной панели</link> рассматриваются отдельно.</p>
  </note>

<section id="secondary-click">
  <title>Нажатие вторичной кнопки</title>

  <p>Вы можете настроить действие нажатия вторичной (правой) кнопкой мыши на сенсорной панели.</p>

  <list type="bullet">
    <item>
      <p>Нажатие двумя пальцами: Нажмите двумя пальцами в любом месте.</p>
    </item>
    <item>
      <p>Нажатие в углу: Надавите одним пальцем на угол.</p>
    </item>
  </list>

  <steps>
    <title>Выберите способ обработки нажатия вторичной кнопки</title>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мышь и сенсорная панель</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Мышь и сенсорная панель</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Нажмите <gui>Сенсорная панель</gui> на верхней панели.</p>
    </item>
    <item>
      <p>В разделе <gui>Нажатие</gui> выберите подходящий способ обработки нажатия.</p>
    </item>
  </steps>
</section>

<section id="tap">
  <title>Нажатие касанием</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Вместо того, чтобы нажимать на кнопки, расположенные около сенсорной панели, вы можете просто касаться сенсорной панели.</p>

  <list>
    <item>
      <p>Чтобы выполнить нажатие, коснитесь сенсорной панели.</p>
    </item>
    <item>
      <p>Чтобы выполнить двойное нажатие, коснитесь дважды.</p>
    </item>
    <item>
      <p>Чтобы перетащить объект, выполните двойное касание, но не поднимайте палец после второго касания. Перетащите объект в нужное место, а затем поднимите палец, чтобы «уронить» объект.</p>
    </item>
    <item>
      <p>Если сенсорная панель распознаёт многопальцевые касания, для нажатия правой кнопкой коснитесь панели двумя пальцами одновременно. В противном случае, для выполнения нажатия правой кнопкой нужно использовать аппаратные кнопки. Чтобы узнать, как выполнить нажатие правой кнопкой, если у мыши нет второй кнопки, смотрите <link xref="a11y-right-click"/>.</p>
    </item>
    <item>
      <p>Если сенсорная панель поддерживает многопальцевые касания, для выполнения <link xref="mouse-middleclick">нажатия средней кнопкой</link> коснитесь панели тремя пальцами сразу.</p>
    </item>
  </list>

  <note>
    <p>При касаниях или перетаскивании старайтесь держать пальцы на достаточном расстоянии друг от друга. Если пальцы слишком близко друг к другу, компьютер может принять их за один палец.</p>
  </note>

  <steps>
    <title>Включить параметр «Нажатие касанием»</title>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мышь и сенсорная панель</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Мышь и сенсорная панель</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>В разделе <gui>Сенсорная панель</gui> убедитесь, что переключатель <gui>Сенсорная панель</gui> включен.</p>
      <note>
        <p>Раздел <gui>Сенсорная панель</gui> появится только если в вашей системе есть сенсорная панель.</p>
      </note>
    </item>
   <item>
      <p>Выключите переключатель <gui>Нажатие касанием</gui>.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Прокрутка двумя пальцами</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Можно выполнять прокрутку на сенсорной панели двумя пальцами.</p>

  <p>Когда эта опция включена, прикосновения и перетаскивания одним пальцем будут работать, как обычно, но если провести двумя пальцами в любом месте сенсорной панели, то будет выполняться прокрутка. Перемещая пальцы вверх и вниз по сенсорной панели, можно выполнять прокрутку вверх и вниз, а перемещая пальцы поперёк сенсорной панели, можно выполнять горизонтальную прокрутку. Старайтесь держать пальцы на некотором расстоянии друг от друга. Если они расположены слишком близко, сенсорная панель может воспринять их, как один большой палец.</p>

  <note>
    <p>Прокрутка двумя пальцами может не работать на некоторых панелях.</p>
  </note>

  <steps>
    <title>Включить параметр «Прокрутка двумя пальцами»</title>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мышь и сенсорная панель</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Мышь и сенсорная панель</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>В разделе <gui>Сенсорная панель</gui> убедитесь, что переключатель <gui>Сенсорная панель</gui> включен.</p>
    </item>
    <item>
      <p>Включите переключатель <gui>Прокрутка двумя пальцами</gui>.</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>Прокрутка по краям</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>Используйте прокрутку по краям, если хотите прокручивать только одним пальцем.</p>

  <p>В спецификации вашей сенсорной панели должно быть указано точное расположение датчиков для прокрутки по краям. Как правило, датчик вертикальной прокрутки находится на правой стороне сенсорной панели, датчик горизонтальной прокрутки - на нижнем краю панели.</p>

  <p>Для вертикальной прокрутки проведите пальцем вверх и вниз по правому краю сенсорной панели. Для горизонтальной прокрутки проведите пальцем по нижнему краю сенсорной панели.</p>

  <note>
    <p>Прокрутка по краям может не работать на некоторых сенсорных панелях.</p>
  </note>

  <steps>
    <title>Включить параметр «Участки прокрутки по краям»</title>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мышь и сенсорная панель</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Мышь и сенсорная панель</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>В разделе <gui>Сенсорная панель</gui> убедитесь, что переключатель <gui>Сенсорная панель</gui> включен.</p>
    </item>
    <item>
      <p>Включите переключатель <gui>Участки прокрутки по краям</gui>.</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>Естественная прокрутка</title>

  <p>С помощью сенсорной панели можно перетаскивать содержимое, как если бы вы перемещали реальный лист бумаги.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мышь и сенсорная панель</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Мышь и сенсорная панель</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>В разделе <gui>Сенсорная панель</gui> убедитесь, что переключатель <gui>Сенсорная панель</gui> включен.</p>
    </item>
    <item>
      <p>Включите переключатель <gui>Естественная прокрутка</gui>.</p>
    </item>
  </steps>

  <note>
    <p>Эту функцию также часто называют <em>обратной прокруткой</em>.</p>
  </note>

</section>

</page>
