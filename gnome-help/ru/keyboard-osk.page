<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="ru">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Юлита Инка (Julita Inca)</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Используйте экранную клавиатуру для ввода текста нажатиями мыши или с помощью сенсорного экрана.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Использование экранной клавиатуры</title>

  <p>Если к компьютеру не подключена клавиатура, или вы предпочитаете её не использовать, можно вводить текст с помощью <em>экранной клавиатуры</em>.</p>

  <note>
    <p>Экранная клавиатура включается автоматически, если вы используете сенсорный экран</p>
  </note>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Специальные возможности</gui>.</p>
    </item>
    <item>
      <p>Щёлкните <gui>Специальные возможности</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите раздел <gui>Ввод текста</gui>, чтобы открыть его.</p>
    </item>
    <item>
      <p>Переведите переключатель <gui>Экранная клавиатура</gui> в положение «включено».</p>
    </item>
  </steps>

  <p>В следующий раз когда вам придётся что-то печатать, экранная клавиатура появится в нижней части экрана.</p>

  <p>Для ввода цифр и символов нажмите кнопку <gui style="button">?123</gui>. Дополнительные символы будут доступны при нажатии кнопки<gui style="button">=/&lt;</gui>. Чтобы снова вернуться к алфавитной клавиатуре, нажмите кнопку <gui>ABC</gui>.</p>

  <p>Если экранная клавиатура мешает, нажмите кнопку <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">вниз</span></media></gui> чтобы временно скрыть клавиатуру. Клавиатура снова автоматически появится, если вы нажмёте на что-то, где возможно использовать клавиатуру. На сенсорном экране вы также можете вытащить клавиатуру, <link xref="touchscreen-gestures">потянув вверх от нижнего края</link>.</p>
  <p>Нажмите кнопку <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">флаг</span></media></gui>, чтобы изменить настройки для <link xref="session-language">Языка</link> или <link xref="keyboard -layouts">Источников ввода</link>.</p>

</page>
