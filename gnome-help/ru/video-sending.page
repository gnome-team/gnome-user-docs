<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="ru">

  <info>
    <link type="guide" xref="media#music"/>

    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Проверьте, установлены ли нужные видеокодеки.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Другие люди не могут воспроизвести созданные мной видеофайлы</title>

  <p>Если вы создали видеофайл на компьютере с Linux и отправили его кому-то, использующему Windows или Mac OS, у него могут возникнуть проблемы с воспроизведением этого файла.</p>

  <p>Для воспроизведения вашего видео у человека, которому вы его отправили, должны быть установлены нужные <em>кодеки</em>. Кодек — это небольшая программа, знающая, как воспроизвести видео на экране. Существует множество различных видеоформатов и для воспроизведения каждого из них нужен отдельный кодек. Проверить, какой формат используется в вашем видеофайле, можно следующим образом:</p>

  <list>
    <item>
      <p>Откройте приложение <app>Файлы</app> из режима <gui xref="shell-introduction#activities">Обзор</gui>.</p>
    </item>
    <item>
      <p>Нажмите правой кнопкой на видеофайл и выберите <gui>Свойства</gui>.</p>
    </item>
    <item>
      <p>Перейдите на вкладку <gui>Аудио/Видео</gui> или <gui>Video</gui> и посмотрите, какой <gui>кодек</gui> указан в разделе <gui>Видео</gui> и <gui>Audio</gui> (если в видео есть звук).</p>
    </item>
  </list>

  <p>Спросите того, у кого возникли проблемы с воспроизведением, установлены ли у него необходимые кодеки. Ему может помочь поиск в Интернете по названию кодека и названию его приложения для воспроизведения видео. Например, если ваш файл в формате <em>Theora</em>, а ваш друг использует для его просмотра Windows Media Player, поисковый запрос должен иметь вид «theora windows media player». Часто необходимый кодек можно скачать бесплатно.</p>

  <p>Если не удалось найти нужный кодек, попробуйте <link href="http://www.videolan.org/vlc/">медиапроигрыватель VLC</link>. Он работает как в Windows и Mac OS, так и в Linux, и поддерживает множество различных видеоформатов. Если это также не помогло, попробуйте преобразовать свой видеофайл в другой формат. Многие видеоредакторы умеют это делать, существуют также специальные программы — видеоконвертеры. Посмотрите, какие из них доступны в приложении для установки программного обеспечения.</p>

  <note>
    <p>Возможны также несколько других проблем, способных помешать кому-либо воспроизвести ваше видео. Видеофайл мог быть повреждён при пересылке (иногда при копировании больших файлов случаются ошибки), могли возникнуть неполадки в приложении для воспроизведения видео, или видеофайл был создан неправильно (произошли какие-то ошибки при его сохранении).</p>
  </note>

</page>
