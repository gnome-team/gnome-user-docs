<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="ru">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Используйте экранный диктор <app>Orca</app> для произнесения вслух пользовательского интерфейса.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Чтение с экрана вслух</title>

  <p>GNOME предоставляет экранный диктор <app>Orca</app> для произнесения вслух пользовательского интерфейса. В зависимости от способа установки вашей системы, Orca может быть не установлен. Если если это так, то сначала установите Orca.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Установите Orca</link></p>
  
  <p>Для запуска <app>Orca</app> используйте клавиши:</p>
  
  <steps>
    <item>
    <p>Нажмите <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Или запустите <app>Orca</app> используя мышь или клавиатуру:</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Специальные возможности</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Специальные возможности</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите раздел <gui>Зрение</gui>, чтобы открыть его.</p>
    </item>
    <item>
      <p>Переведите переключатель <gui>Чтение с экрана</gui> в положение «включено».</p>
    </item>
  </steps>

  <note style="tip">
    <title>Быстрое включение и отключение «Чтения с экрана»</title>
    <p>Можно включить или отключить средство чтение с экрана, нажав на <link xref="a11y-icon">значок специальных возможностей</link> в верхней панели и выбрав <gui>Чтение с экрана</gui>.</p>
  </note>

  <p>Дополнительную информацию смотрите в <link href="help:orca">Справке Orca</link>.</p>
</page>
