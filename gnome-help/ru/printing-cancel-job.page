<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="ru">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Отмена незаконченного задания на печать и удаление его из очереди.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Отменить, приостановить или возобновить задачу печати</title>

  <p>Можно отменить ждущие обработки задания печати и удалить их из очереди печати в настройках принтера.</p>

  <section id="cancel-print-job">
    <title>Отмена задания на печать</title>

  <p>Можно отменить печать документа, случайно отправленного на печать, чтобы не расходовать чернила и бумагу.</p>

  <steps>
    <title>Как отменить задание печати:</title>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Принтеры</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Принтеры</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Щёлкните кнопу рядом с принтером, на которой отображается количество заданий.</p>
    </item>
    <item>
      <p>Отмените задание печати, нажав на кнопку отмены.</p>
    </item>
  </steps>

  <p>Если задание печати не отменится как ожидалось, попробуйте нажать и не отпускать кнопку <em>Отмена</em> на принтере.</p>

  <p>Как последнее средство, особенно если не удаётся отменить крупное задание печати с большим количеством страниц, удалите бумагу из входного лотка принтера. Принтер должен обнаружить отсутствие бумаги и остановить печать. После этого можно снова попытаться отменить задание печати, или попробовать выключить принтер и снова его включить.</p>

  <note style="warning">
    <p>Будьте осторожны, чтобы не повредить принтер при извлечении бумаги — если для этого приходится слишком сильно тянуть бумагу, возможно, лучше оставить её там, где она есть.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Приостановка и возобновление заданий печати</title>

  <p>Если необходимо приостановить и возобновить задание печати, это можно сделать из диалогового окна заданий печати в настройках принтера, нажав соответствующую кнопку.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Принтеры</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Принтеры</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Щёлкните кнопу рядом с принтером, на которой отображается количество заданий.</p>
    </item>
    <item>
      <p>Приостановите или завершите выполнение задания печати, нажав соответствующую кнопку.</p>
    </item>
  </steps>

  </section>

</page>
