<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="ru">

  <info>

    <link type="guide" xref="power#faq"/>
    <link type="guide" xref="status-icons#batteryicons"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision version="gnome:40" date="2021-03-22" status="candidate"/>

    <desc>Оставшееся время работы, отображаемое при нажатии на <gui>значок аккумулятора</gui>, является приблизительным.</desc>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

<title>Неправильная оценка времени работы от аккумулятора</title>

<p>При проверке оставшегося времени работы от аккумулятора может оказаться, что отображаемое оставшееся время работы не совпадает с фактическим временем работы от аккумулятора. Причина в том, что оставшееся время работы от аккумулятора можно оценить лишь приблизительно. Как правило, со временем оценка становится точнее.</p>

<p>Чтобы оценить оставшееся время работы от аккумулятора, нужно принять во внимание множество факторов. Один из них — это мощность, потребляемая компьютером в данный момент: потребляемая мощность изменяется в зависимости от того, сколько программ открыто, какие устройства подключены и запущены ли программы, интенсивно использующие вычислительные ресурсы (такие, как просмотр видео высокой чёткости или преобразование музыкальных файлов). Всё это постоянно меняется и является трудно предсказуемым.</p>

<p>Ещё один фактор — то, как разряжается аккумулятор. Некоторые аккумуляторы разряжаются с большей скоростью при уменьшении оставшегося уровня заряда. Без точного знания динамики процесса разрядки аккумулятора трудно оценить оставшееся время работы.</p>

<p>По мере того, как аккумулятор разряжается, диспетчер питания уточняет параметры этого процесса и учится лучше оценивать оставшееся время работы. Но абсолютно точной оценки добиться всё-таки невозможно.</p>

<note>
  <p>Если отображается совершенно нереальное время работы от аккумулятора (например, сотни дней), вероятно, диспетчеру питания не хватает каких-то данных для правильного расчёта.</p>
  <p>Если отключить ноутбук от сети и некоторое время поработать от аккумулятора, а затем подключить его и дать аккумулятору подзарядиться, то диспетчер питания сможет получить необходимые ему данные.</p>
</note>

</page>
