<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.38.1" date="2020-11-04" status="review"/>
    <revision version="gnome:42" status="final" date="2022-03-02"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Андрэ Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шобха Тьяги (Shobha Tyagi)</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ivan Stanton</name>
      <email>northivanastan@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Выбор стиля и установка фона.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2024</mal:years>
    </mal:credit>
  </info>

  <title>Изменение внешний вид вашего рабочего стола</title>
  
  <p>Вы можете изменить внешний вид элементов, отображаемых на экране, установив предпочтение светлого или тёмного стиля. Вы можете выбрать изображение в качестве фона рабочего стола.</p>
  
<section id="style">
  <title>Светлая и тёмная тема оформления</title>
  <p>Чтобы переключиться между светлым и темным темами оформления рабочего стола:</p>
  
  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Внешний вид</gui>.</p>
    </item>
    <item>
      <p>Щёлкните <gui>Внешний вид</gui>, чтобы открыть панель настроек. Текущий выбранный стиль отображается вверху панели в синей рамке.</p>
    </item>
    <item>
      <p>Выберите <gui>Светлая</gui> или <gui>Тёмная</gui>.</p>
    </item>
    <item>
      <p>Изменения вступят в силу немедленно.</p>
    </item>
  </steps>
  
</section>

<section id="background">
  <title>Фон</title>
  <p>Чтобы поменять изображение, используемое в качестве фона:</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Внешний вид</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Внешний вид</gui>, чтобы открыть панель. В начале списка предварительного просмотра для используемого стиля будет показано текущее выбранное фоновое изображение.</p>
    </item>
    <item>
      <p>Есть два способа изменить изображение, используемое в качестве фона:</p>
      <list>
        <item>
          <p>Нажмите одно из фоновых изображений, поставляемых вместе с системой.</p>
        <note style="info">
          <p>Некоторые обои меняются в течение дня. Такие обои имеют маленький значок часов в нижнем правом углу.</p>
        </note>
        </item>
        <item>
          <p>Нажмите <gui>Добавить изображение…</gui>, чтобы использовать одну из своих фотографий. По умолчанию будет открыта папка <file>Изображения</file>, так как большинство приложений для управления фотографиями хранят фотографии там.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Изменения вступят в силу немедленно.</p>
        <note style="tip">
          <p>Другой способ установить одну из ваших фотографий в качестве фона: щёлкните правой кнопкой мыши файл изображения в приложении <app>Файлы</app> и выберите <gui>Установить как фон</gui> или откройте файл изображения в приложении <app>Просмотр изображений</app>, нажмите кнопку меню в панели заголовка и выберите <gui>Сделать фоном рабочего стола</gui>.</p>
        </note>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Переключитесь на пустой виртуальный рабочий стол</link>, чтобы увидеть рабочий стол целиком.</p>
    </item>
  </steps>
  
  </section>

</page>
