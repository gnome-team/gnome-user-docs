<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="nautilus-views" xml:lang="ta">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-views"/>

    <revision pkgversion="3.8" version="0.2" date="2013-04-03" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision version="gnome:45" date="2024-03-04" status="candidate"/>

    <credit type="author">
      <name>டிஃபானி அன்ட்டொபோல்ஸ்கி</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Specify the default sort order for files and folders.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title><app>கோப்புகள்</app> இல் உள்ள முன்னுரிமைகளைக் காட்டும்</title>

<p>You can change how files and folders are grouped and sorted by default.
Press the menu button in the sidebar of the window and select
<gui style="menuitem">Preferences</gui>.</p>

<!-- TODO FIXME: Merge / sort out with "nautilus-display.page" overlap for "Expandable Folders in List View" and "Grid view options" -->
<section id="default-view">
<title>General</title>
<terms>
  <item>
    <title><gui>Sort Folders Before Files</gui></title>
    <p>By default, the file manager does not show all folders before files.
    To see all folders listed before files, enable this option.</p>
  </item>
</terms>

<note style="tip">
  <p>To change how files are sorted in folders, see <link xref="files-sort"/>.</p>
</note>

</section>

</page>
