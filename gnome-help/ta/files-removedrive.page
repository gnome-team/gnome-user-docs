<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="ta">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ஜிம் காம்ப்பெல்</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>USB ஃபிளாஷ் இயக்கி, CD, DVD அல்லது பிற சாதனத்தை வெளித்தள்ளுதல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>ஒரு வெளிப்புற இயக்கியை பாதுகாப்பாக நீக்குதல்</title>

  <p>நீங்கள் USB இயக்கிகள் போன்ற வெளி சேமிப்பு சாதனங்களைப் பயன்படுத்தும் போது, அவற்றை கணினியில் இருந்து எடுக்கும் முன்பு பாதுகாப்பாக அகற்ற வேண்டும். நீங்கள் ஒரு சாதனத்தை சட்டென எடுத்தால் ஒரு பயன்பாடு அதைப் பயன்படுத்திக் கொண்டிருக்கும் போதே நீக்குவதால் ஏதேனும் சிக்கல் ஏற்படலாம். இதனால் அதில் உள்ள உங்கள் கோப்புகள் சில இழக்கப்படலாம் அல்லது சேதமடையலாம். நீங்கள் CD அல்லது DVD போன்ற ஒளியிழை வட்டைப் பயன்படுத்தினால், உங்கள் கணினியில் இருந்து வட்டை வெளியே தள்ளுவதற்கும் இதே செயல்படிகளைப் பயன்படுத்தலாம்.</p>

  <steps>
    <title>ஒரு நீக்கக்கூடிய சாதனத்தை வெளித்தள்ள:</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>பக்கப்பட்டியில் சாதனத்தைக் கண்டறியவும். அதன் பெயருக்கு அருகில் ஒரு சிறிய வெளியேற்று சின்னம் இருக்கும். சாதனத்தை பாதுகாப்பாக அகற்ற அல்லது வெளித்தள்ள அந்த சின்னத்தை சொடுக்கவும்.</p>
      <p>மாறாக, நீங்கள் பக்கப்பட்டியில் உள்ள சாதனத்தின் பெயரை வலது சொடுக்கி, <gui>வெளியேற்று</gui> என்பதையும் தேர்ந்தெடுக்கலாம்.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>பயன்பாட்டில் உள்ள ஒரு சாதனத்தை பாதுகாப்பாக நீக்குதல்</title>

  <p>சாதனத்தில் உள்ள கோப்புகளில் ஏதேனும் ஒன்றை ஏதேனும் ஒரு பயன்பாடு பயன்படுத்திக் கொண்டிருந்தால், நீங்கள் சாதனத்தைப் பாதுகாப்பாக நீக்க முடியாது. அப்போது நீங்கள் நீக்க முயற்சித்தால் <gui>தொகுதி பணிமிகுதியாக உள்ளது</gui> என்று கூறும் ஒரு சாளரம் தோன்றூம். சாதனத்தை பாதுகாப்பாக நீக்க:</p>

  <steps>
    <item><p><gui>ரத்து</gui> என்பதை சொடுக்கவும்.</p></item>
    <item><p>சாதனத்தில் உள்ள அனைத்து கோப்புகளையும் மூடவும்.</p></item>
    <item><p>சாதனம் பாதுகாப்பாக நீக்க அல்லது வெளித்தள்ள வெளியேற்று சின்னத்தை சொடுக்கவும்.</p></item>
    <item><p>மாறாக, நீங்கள் பக்கப்பட்டியில் உள்ள சாதனத்தின் பெயரை வலது சொடுக்கி, <gui>வெளியேற்று</gui> என்பதையும் தேர்ந்தெடுக்கலாம்.</p></item>
  </steps>

  <note style="warning"><p>நீங்கள் கோப்புகளை மூடாமலே சாதனத்தை வெளியேற்ற <gui>பரவாயில்லை, வெளியேற்று</gui> என்பதைத் தேர்வு செய்யலாம். இப்படிச் செய்தால், கோப்புகளைத் திறந்துவைத்துள்ள பயன்பாடுகளில் பிழைகள் ஏற்படலாம்.</p></note>

  </section>

</page>
