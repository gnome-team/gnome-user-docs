<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="ta">

  <info>
    <link type="guide" xref="mouse" group="#first"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-05" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit>
      <name>டிஃபானி அன்ட்டொபோல்ஸ்கி</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>நீங்கள் உங்கள் சொடுக்கி அல்லது தொடுதிட்டை பயன்படுத்தும் போது சுட்டி நகரும் வேகத்தை மாற்றுதல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Adjust the speed of the mouse and touchpad</title>

  <p>நீங்கள் உங்கள் சொடுக்கியை நகர்த்தும் போது அல்லது உங்கள் தொடுதிட்டைப் பயன்படுத்தும் போது உங்கள் சுட்டி மிக வேகமாக அல்லது மெதுவாக நகர்ந்தால், நீங்கள் இந்த சாதனங்களுக்கான சுட்டி வேகத்தை சரிசெய்துகொள்ள முடியும்.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>Adjust the <gui>Pointer Speed</gui> slider
      until the pointer motion is comfortable for you. Sometimes the most
      comfortable settings for one type of device are not the best for
      the other.</p>
    </item>
  </steps>

  <note>
    <p>The <gui>Touchpad</gui> section only appears if your system has a
    touchpad, while the <gui>Mouse</gui> section is only visible when a mouse
    is connected.</p>
  </note>

</page>
