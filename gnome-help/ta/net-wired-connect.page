<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="ta">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>பெரும்பாலான வயர்டு பிணைய இணைப்புகளை இணைக்க நீங்கள் ஒரு பிணைய கேபிளை இணைத்தால் போதும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>வயர்டு (ஈத்தர்நெட்) பிணையத்திற்கு இணைத்தல்</title>

  <!-- TODO: create icon manually because it is one overlaid on top of another
       in real life. -->
  <p>To set up most wired network connections, all you need to do is plug in a
  network cable. The wired network icon
  (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">settings</span></media>)
  is displayed on the top bar with three dots while the connection is being
  established. The dots disappear when you are connected.</p>
  <media its:translate="no" type="image" src="figures/net-wired-ethernet-diagram.svg" mime="image/svg" width="51" height="38" style="floatend floatright">
    <p its:translate="yes">Diagram of an Ethernet cable</p>
  </media>

  <p>If this does not happen, you should first of all make sure that your
  network cable is plugged in. One end of the cable should be plugged into the
  rectangular Ethernet (network) port on your computer, and the other end
  should be plugged into a switch, router, network wall socket or similar
  (depending on the network setup you have). The Ethernet port is often on the
  side of a laptop, or near the top of the back of a desktop computer. Sometimes, a
  light beside the Ethernet port will indicate that it is plugged in and active.</p>

  <note>
    <p>You cannot plug one computer directly into another one with a network
    cable (at least, not without some extra setting-up). To connect two
    computers, you should plug them both into a network hub, router or
    switch.</p>
  </note>

  <p>அப்போதும் இணைப்பு கிடைக்காவிட்டால், உங்கள் பிணையம் தானியங்கு அமைவை (DHCP) ஆதரிக்காமல் இருக்கலாம். அப்படியானால் நீங்கள் <link xref="net-manual">அதை கைமுறையாக அமைவாக்கம் செய்ய</link> வேண்டும்.</p>

</page>
