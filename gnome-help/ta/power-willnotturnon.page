<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="ta">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>கேபிள்கள் தளர்வாக இருப்பது அல்லது வன்பொருள் சிக்கல்கள் காரணமாக இருக்கலாம்.</desc>
    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>என் கணினி இயங்கவில்லை</title>

<p>கணினி இயங்காமல் போவதற்கு பல காரணங்கள் உள்ளன. இந்த தலைப்பு சாத்தியமுள்ள சில காரணங்களின் மேலோட்டப் பார்வையை வழங்குகிறது.</p>
	
<section id="nopower">
  <title>கணினி பிளக் இன் செய்யப்படவில்லை, பேட்டரி காலி அல்லது கேபிள் தளர்வாக உள்ளது</title>
  <p>கணினியின் பவர் கேபிள்கள் பவர் அவுட்லெட்டில் உறுதியாக செருகப்பட்டு ஆன் செய்யப்பட்டுள்ளதா எனப் பார்க்கவும். மானிட்டரும் பிளக் இன் செய்து ஆன் செய்யப்பட்டுள்ளதா எனப் பார்க்கவும். உங்கள் கணினி மடிக்கணினி எனில், (பேட்டரி தீர்ந்துவிட்டிருந்தால்) சார்ஜிங் கேபிளை இணைக்கவும். பேட்டரி அகற்றக்கூடிய பேட்டரி எனில், அது தனக்குரிய இடத்தில் சரியாகப் பொருந்தியுள்ளதா எனவும் பார்க்கலாம்.</p>
</section>

<section id="hardwareproblem">
  <title>கணினி வன்பொருளில் சிக்கல்</title>
  <p>A component of your computer may be broken or malfunctioning. If this is
  the case, you will need to get your computer repaired. Common faults include
  a broken power supply unit, incorrectly-fitted components (such as the
  memory or RAM) and a faulty motherboard.</p>
</section>

<section id="beeps">
  <title>கணினி பீப் ஒலி எழுப்பி அணைந்துவிடுகிறது</title>
  <p>If the computer beeps several times when you turn it on and then turns off
  (or fails to start), it may be indicating that it has detected a problem.
  These beeps are sometimes referred to as <em>beep codes</em>, and the pattern
  of beeps is intended to tell you what the problem with the computer is.
  Different manufacturers use different beep codes, so you will have to consult
  the manual for your computer’s motherboard, or take your computer in for
  repairs.</p>
</section>

<section id="fans">
  <title>கணினி விசிறிகள் சுழல்கின்றன ஆனால் திரையில் எதுவும் வரவில்லை</title>
  <p>முதலில், மானிட்டர் பிளக் இன் செய்யப்பட்டு ஆன் செய்யப்பட்டுள்ளதா என சோதிக்க வேண்டும்.</p>
  <p>இதற்கு வன்பொருள் சிக்கலும் காரணமாக இருக்கலாம். பவர் பொத்தானை அழுத்தியதும் விசிறிகள் ஆன் ஆகலாம், ஆனால் கணினியின் பிற முக்கிய பாகங்கள் ஆன் ஆகாமல் இருக்கலாம். இப்படியானால் கணினியை பழுதுபார்க்க கொண்டு செல்லவும்.</p>
</section>

</page>
