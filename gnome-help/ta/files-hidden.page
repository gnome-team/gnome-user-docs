<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="ta">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>
    <revision version="gnome:45" date="2024-04-04" status="candidate"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Make a file invisible, so you cannot see it in the file
    manager.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>ஒரு கோப்பை மறைத்தல்</title>

  <p>The <app>Files</app> file manager gives you the ability to hide and unhide
  files at your discretion. When a file is hidden, it is not displayed by the
  file manager, but it is still there in its folder.</p>

  <p>ஒரு கோப்பை மறைக்க, அதன் பெயரின் முன்பு ஒரு <file>.</file> ஐ சேர்த்து அதை <link xref="files-rename">மறுபெயரிடவும்</link>. உதாரணமாக <file> example.txt</file> எனும் பெயருள்ள ஒரு கோப்பை மறைக்க, நீங்கள் அதை <file>.example.txt</file> என மறுபெயரிட வேண்டும்.</p>

<note>
  <p>You can hide folders in the same way that you can hide files. Hide a
  folder by placing a <file>.</file> at the beginning of the folder’s name.</p>
</note>

<section id="show-hidden">
 <title>மறைக்கப்பட்ட கோப்புகள் அனைத்தையும் காட்டுதல்</title>

  <p>If you want to see all hidden files in a folder, go to that folder and
  either press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again,
  either press the menu button in the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>ஒரு கோப்பைக் காண்பித்தல்</title>

  <p>To unhide a file, go to the folder containing the hidden file. Press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either press the menu button in
  the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>முன்னிருப்பாக, நீங்கள் கோப்பு மேலாளரை மூடும் வரை மட்டுமே மறைக்கப்பட்ட கோப்புகளைப் பார்க்க முடியும். கோப்பு மேலாளர் எப்போதும் மறைக்கப்பட்ட கோப்புகளைக் காட்ட வேண்டும் என அமைக்க <link xref="nautilus-views"/> ஐப் பார்க்கவும்.</p></note>

  <note><p>மறைக்கப்பட்ட கோப்புகள் பெரும்பாலானவை தங்கள் பெயரின் தொடக்கத்தில் <file>.</file> ஐக் கொண்டிருக்கும், ஆனால் மாறாக சில கோப்புகளில் பெயரின் முடிவில் <file>~</file> இருக்கலாம். இந்த கோப்புகள் மறுபிரதி கோப்புகளாகும். மேலும் தகவலுக்கு <link xref="files-tilde"/> ஐப் பார்க்கவும்.</p></note>

</section>

</page>
