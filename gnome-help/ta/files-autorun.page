<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="ta">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ஷோபா தியாகி</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>CD மற்றும் DVD, கேமரா, ஆடியோ பிளேயர் மற்றும் பிற சாதனங்கள் மற்றும் ஊடகங்களுக்கு பயன்பாடுகளை தானாக இயக்குதல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>சாதனங்கள் அல்லது வட்டுகளுக்கான பயன்பாடுகளைத் திறத்தல்</title>

  <p>நீங்கள் ஒரு சாதனத்தை செருகும் போது அல்லது வட்டு அல்லது ஊடக அட்டையைச் செருகும் போது ஒரு பயன்பாட்டை தானாகவே திறக்கும்படி அமைக்க முடியும். உதாரணமாக, நீங்கள் ஒரு டிஜிட்டல் கேமராவை இணைக்கும் போது புகைப்பட ஒழுங்கமைப்புப் பயன்பாடு தொடங்க வேண்டும் என நீங்கள் விரும்பலாம். நீங்கள் எதை இணைத்தாலும் எதுவும் நடக்காமல் இருக்க இந்த வசதியை அணைக்கவும் செய்யலாம்.</p>

  <p>நீங்கள் பல்வேறு சாதனங்களை இணைக்கும் போது, எந்தப் பயன்பாடுகள் தொடங்க வேண்டும் என்பதை முடிவு செய்ய:</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Removable Media</gui>.</p>
  </item>
  <item>
    <p>Select <guiseq><gui>Settings</gui><gui>Apps</gui></guiseq> from the
    results. This will open the <gui>Apps</gui> panel.</p>
  </item>
  <item>
    <p>Select <gui>Default Apps</gui> to open the panel.</p>
  </item>
  <item>
    <p>In the <gui>Removeable Media</gui> section, switch the
    <gui>Automatically Launch Apps</gui> switch to on.</p>
  </item>
  <item>
    <p>நீங்கள் விரும்பும் சாதனம் அல்லது ஊடக வகைக் கண்டறிந்து, பின்னர் அந்த ஊடக வகைக்கான ஒரு பயன்பாடு அல்லது செயலைத் தேர்வு செய்யவும். சாதனங்கள் மற்றும் ஊடகங்களின் பல்வேறு வகைகளுக்கான விளக்கம் கீழே உள்ளது.</p>
    <p>Instead of starting an application, you can also set it so that the
    device will be shown in the file manager, with the <gui>Open folder</gui>
    option. When that happens, you will be asked what to do, or nothing will
    happen automatically.</p>
  </item>
  <item>
    <p>If you do not see the device or media type that you want to change in
    the list (such as Blu-ray discs or E-book readers), click <gui>Other
    Media</gui> to see a more detailed list of devices. Select the type of
    device or media from the <gui>Type</gui> drop-down and the application or
    action from the <gui>Action</gui> drop-down.</p>
  </item>
</steps>

  <note style="tip">
    <p>If you do not want any applications to be opened automatically, whatever
    you plug in, switch the <gui>Automatically Launch Apps</gui> switch to off.</p>
  </note>

<section id="files-types-of-devices">
  <title>சாதனங்கள் மற்றும் ஊடக வகைகள்</title>
<terms>
  <item>
    <title>ஆடியோ வட்டுகள்</title>
    <p>ஆடியோ CDகளைக் கையாள உங்கள் பிடித்தமான இசை பயன்பாடு அல்லது CD ஆடியோ பிரித்தெடுப்பு பயன்பாடுகளைத் தேர்வு செய்யவும். நீங்கள் ஆடியோ டிவிடிகளைப் (டிவிடி-A) பயன்படுத்தினால், <gui>பிற மீடியா </gui> என்பதன் கீழ் அவற்றை எப்படி திறக்க வேண்டும் எனத் தேர்வு செய்யலாம். நீங்கள் ஒரு ஆடியோ வட்டை கோப்பு மேலாளரில் திறந்தால், டிராக்குகள் WAV கோப்புகளாகக் காண்பிக்கப்படும், அவற்றை நீங்கள் ஒரு ஆடியோ பிளேயர் மூலம் இசைக்கலாம்.</p>
  </item>
  <item>
    <title>வீடியோ வட்டுகள்</title>
    <p>வீடியோ டிவிடிகளைக் கையாள நீங்கள் விரும்பும் வீடியோ பயன்பாட்டைத் தேர்வு செய்யவும். HD டிவிடி, வீடியோ CD (VCD) மற்றும் சூப்பர் வீடியோ CD (SVCD) ஆகியவற்றுக்கான பயன்பாட்டை அமைக்க <gui>பிற ஊடகம்</gui> பொத்தானைப் பயன்படுத்தவும். நீங்கள் டிவிடிகள் அல்லது மற்ற வீடியோ வட்டுகளை செருகும் போது அவை சரியாக வேலை செய்யவில்லை என்றால், <link xref="video-dvd"/> ஐப் பார்க்கவும்.</p>
  </item>
  <item>
    <title>காலி வட்டுகள்</title>
    <p>காலி CDகள், காலி டிவிடிகள், காலி Blu-ray வட்டுகள் மற்றும் காலி HD DVD களுக்கு ஒரு வட்டு எழுதல் பயன்பாட்டைத் தேர்ந்தெடுக்க <gui>பிற ஊடகம்</gui> பொத்தானைப் பயன்படுத்தவும்.</p>
  </item>
  <item>
    <title>கேமராக்கள் மற்றும் புகைப்படங்கள்</title>
    <p>நீங்கள் உங்கள் டிஜிட்டல் கேமராவை இணைக்கும் போது அல்லது ஒரு கேமராவிn CF, SD, MMC அல்லது MS போன்ற ஒரு மீடியா கார்டைச் செருகும் போது இயங்க வேண்டிய புகைப்பட நிர்வாக பயன்பாட்டைத் தேர்வு செய்ய <gui>புகைப்படங்கள்</gui> கீழ் தோன்று விருப்பத்தைப் பயன்படுத்தவும். நீங்கள் கோப்பு நிர்வாகியைப் பயன்படுத்தி உங்கள் புகைப்படங்களை உலவ முடியும்.</p>
    <p>Under <gui>Other Media</gui>, you can select an application to open
    Kodak picture CDs, such as those you might have made in a store. These are
    regular data CDs with JPEG images in a folder called
    <file>Pictures</file>.</p>
  </item>
  <item>
    <title>மியூஸிக் பிளேயர்கள்</title>
    <p>உங்கள் போர்ட்டபிள் மியூசிக் பிளேயரில் உள்ள மியூஸிக் லைப்ரரியை நிர்வகிக்க ஒரு பயன்பாட்டைத் தேர்வு செய்யவும் அல்லது கோப்பு மேலாளரைப்  பயன்படுத்தி கோப்புகளை நீங்களே நிர்வகிக்கவும்.</p>
    </item>
    <item>
      <title>மின் புத்தக வாசிப்புக் கருவிகள்</title>
      <p>உங்கள் மின் புத்தக வாசிப்புக் கருவியில் உள்ள புத்தகங்களை நிர்வகிக்க ஒரு பயன்பாட்டைத் தேர்வு செய்ய <gui>பிற ஊடகம்</gui> பொத்தானைப் பயன்படுத்தவும் அல்லது கோப்பு மேலாளரைப்  பயன்படுத்தி கோப்புகளை நீங்களே நிர்வகிக்கவும்.</p>
    </item>
    <item>
      <title>மென்பொருள்</title>
      <p>சில வட்டுகள் மற்றும் நீக்கக்கூடிய ஊடகத்தில், ஊடகம் செருகப்படும் போது தானாக இயங்கக்கூடிய மென்பொருள் இருக்கும். தானியங்கு மென்பொருள் உள்ள ஊடகம் செருகப்படும் போது என்ன செய்ய வேண்டும் என்பதைக் கட்டுப்படுத்த <gui>மென்பொருள்</gui> விருப்பத்தைப் பயன்படுத்தவும். மென்பொருள் இயங்கும் முன்பு எப்போதும் உறுதிப்படுத்துமாறு உங்களிடம் கேட்கப்படும்.</p>
      <note style="warning">
        <p>Never run software from media you don’t trust.</p>
      </note>
   </item>
</terms>

</section>

</page>
