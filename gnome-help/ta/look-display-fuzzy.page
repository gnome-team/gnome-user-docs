<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="ta">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    	<revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>நடாலியா ருஸ் லெய்வா</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ஷோபா தியாகி</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>திரை தெளிவுத்திறன் தவறாக அமைக்கப்பட்டிருக்கலாம்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>என் திரையில் உள்ளவை ஏன் மங்கலாக/புள்ளி புள்ளியாகக் காட்சியளிக்கின்றன?</title>

  <p>The display resolution that is configured may not be the correct one for
  your screen. To solve this:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Displays</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>Try some of the <gui>Resolution</gui> options and select the one that
      makes the screen look better.</p>
    </item>
  </steps>

<section id="multihead">
  <title>பல காட்சி சாதனங்கள் இணைக்கப்பட்டிருக்கும் போது</title>

  <p>If you have two displays connected to the computer (for example, a normal
  monitor and a projector), the displays might have different optimal, or
  <link xref="look-resolution#native">native</link>, resolutions.</p>

  <p>Using <link xref="display-dual-monitors#modes">Mirror</link> mode, you can
  display the same thing on two screens. Both screens use the same resolution,
  which may not match the native resolution of either screen, so the sharpness
  of the image may suffer on both screens.</p>

  <p>Using <link xref="display-dual-monitors#modes">Join Displays</link> mode,
  the resolution of each screen can be set independently, so they can both be
  set to their native resolution.</p>

</section>

</page>
