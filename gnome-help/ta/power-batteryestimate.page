<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="ta">

  <info>

    <link type="guide" xref="power#faq"/>
    <link type="guide" xref="status-icons#batteryicons"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision version="gnome:40" date="2021-03-22" status="candidate"/>

    <desc>நீங்கள் <gui>பேட்டரி சின்னத்தை</gui> சொடுக்கும் போது காட்டப்படும் பேட்டரி ஆயுள் ஒரு கணக்கீடே.</desc>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>கணக்கிடப்படும் பேட்டரி ஆயுள் தவறு</title>

<p>நீங்கள் மீதமுள்ள பேட்டரி ஆயுளை சோதிக்கும் போது, அது மீதமுள்ளது எனக் காட்டும் நேரமும் பேட்டரி உண்மையில் நீடிக்கும் நேரமும் வேறுபடக்கூடும். மீதமுள்ள பேட்டரி அளவை கணக்கிட மட்டுமே முடியும் என்பதே இதற்குக் காரணம். சாதாரணமாக நாட்கள் ஆக ஆக கணக்கீடு மேம்படக்கூடும்.</p>

<p>In order to estimate the remaining battery life, a number of factors must be
taken into account. One is the amount of power currently being used by the
computer: power consumption varies depending on how many programs you have
open, which devices are plugged in, and whether you are running any intensive
tasks (like watching high-definition video or converting music files, for
example). This changes from moment to moment, and is difficult to predict.</p>

<p>பேட்டரி எப்படி மின்னிறக்கமாகிறது என்பது மற்றொரு காரணியாகும். வெளியேற்றப்பட்டு இருக்கிறது. சில பேட்டரிகள் சார்ஜைப் பெறும் வேகத்தை விட சார்ஜை வேகமாக இழக்கலாம். பேட்டரி எப்படி சார்ஜை இழக்கிறது என்பது பற்றிய துல்லியமான விவரமில்லாமல் மீதமுள்ள பேட்டரி அளவு என்பதை தோராயமாகவே கணக்கிட முடியும்.</p>

<p>பேட்டரி சார்ஜ் இழக்கும் போது, மின்சக்தி நிர்வாகி அதன் சார்ஜ் இழத்தல் பண்புகளை அறிந்துகொண்டு, பேட்டரி ஆயுளை எப்படி சிறப்பாக கணக்கிடுவது என்று அறிந்துகொள்ளும். இருப்பினும் அவை ஒருபோதும் முற்றிலும் துல்லியமாக இருக்காது.</p>

<note>
  <p>உங்கள் பேட்டரி ஆயுள் மிக விநோதமான முறையில் கணக்கிடப்பட்டால் (உதாரணம், நூற்றுக்கணக்கான நாட்கள்) மின்சக்தி நிர்வாகிக்கு சரியான கணக்கீட்டைச் செய்ய தேவையான ஏதோ தரவு கிடைக்கவில்லை எனப் பொருள்.</p>
  <p>நீங்கள் மின் சக்தியை அகற்றிவிட்டு மடிக்கணினியை சற்று நேரம் பேட்டரியில் இயக்கிவிட்டு, மீண்டும் மின் பிளக்கை செருகி மீண்டும் ரீசார்ஜ் செய்யத் தொடங்கினால், மின் சக்தி நிர்வாகி தனக்கு தேவையான தரவைப் பெற முடியும்.</p>
</note>

</page>
