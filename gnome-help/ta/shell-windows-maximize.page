<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="ta">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ஒரு சாளரத்தை பெரிதாக்க அல்லது மீட்டமைக்க அதன் தலைப்புப் பட்டியை இரு சொடுக்கவும் அல்லது இழுக்கவும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>ஒரு சாளரத்தை பெரிதாக்குதல் மற்றும் பெரிதாக்கல் நீக்குதல்</title>

  <p>ஒரு சாளரம் பணிமேசை முழுவதும் அடைத்துக் கொள்ளும் வண்ணம் அதை பெரிதாக்கலாம், அதன் வழக்கமான அளவுக்கு மீட்டமைக்கலாம். திரையின் இடப்புறமாக செங்குத்தாகவும் சாளரங்களைப் பெரிதாக்கலாம், இதன் மூலம் நீங்கள் இரண்டு சாளரங்களை ஒரே சமயத்தில் பார்க்கலாம். விவரங்களுக்கு <link xref="shell-windows-tiled"/> ஐப் பார்க்கவும்.</p>

  <p>ஒரு சாளரத்தை பெரிதாக்க, அதன் தலைப்புப் பட்டியைப் பிடித்து அதை திரையின் மேலே இழுத்துச் செல்லவும் அல்லது தலைப்புப் பட்டியை இரு சொடுக்கவும். விசைப்பலகையைப் பயன்படுத்தி ஒரு சாளரத்தைப் பெரிதாக்க <key xref="keyboard-key-super">Super</key> விசையை அழுத்திக் கொண்டு <key>↑</key> ஐ அழுத்தவும் அல்லது <keyseq><key>Alt</key><key>F10</key></keyseq> ஐ அழுத்தவும்.</p>

  <p if:test="platform:gnome-classic">தலைப்புப் பட்டியில் உள்ள பெரிதாக்கு பொத்தானை சொடுக்கியும் சாளரத்தைப் பெரிதாக்கலாம்.</p>

  <p>ஒரு சாளரத்தை பெரிதாக்காத அளவுக்கு மீட்டமைக்க, அதை திரையின் விளிம்பில் இருந்து விலக்கி இழுத்து வரவும். சாளரம் முழுவதும் பெரிதாக்கப்பட்டிருந்தால், அதை மீட்டமைக்க தலைப்புப் பட்டியில் இரு சொடுக்கலாம். பெரிதாக்கப் பயன்படுத்திய அதே விசைப்பலகைக் குறுக்குவழிகளையும் பயன்படுத்தலாம்.</p>

  <note style="tip">
    <p>Hold down the <key>Super</key> key and drag anywhere in a window to move
    it.</p>
  </note>

</page>
