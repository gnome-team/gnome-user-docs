<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="ta">

  <info>
    <link type="guide" xref="media#music"/>

    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>சரியான வீடியோ கோடெக்குகள் நிறுவப்பட்டுள்ளதா என சோதிக்கவும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Other people can’t play the videos I made</title>

  <p>நீங்கள் உங்கள் Linux கணினியில் உருவாக்கிய வீடியோவை Windows அல்லது Mac OS ஐப் பயன்படுத்தும் மற்றவர்களுக்கு அனுப்பினால், அதை இயக்குவதில் சிக்கல் ஏற்படக்கூடும்.</p>

  <p>உங்கள் வீடியோவை இயக்க அவர்கள் கணினியில் சரியான <em>கோடெக்குகள்</em> நிறுவப்பட்டிருக்க வேண்டும். கோடெக் என்பது வீடியோவை எடுத்து திரையில் காட்டுவது எப்படி எனத் தெரிந்த ஒரு சிறு மென்பொருளாகும். பல வீடியோ வடிவங்கள் உள்ளன, அவற்றை இயக்க ஒவ்வொன்றுக்கும் ஒரு கோடெக் தேவை. உங்கள் வீடியோவின் வடிவமைப்பு எது என அறிய இதைச் செய்யலாம்:</p>

  <list>
    <item>
      <p>Open <app>Files</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Right-click on the video file and select <gui>Properties</gui>.</p>
    </item>
    <item>
      <p>Go to the <gui>Audio/Video</gui> or <gui>Video</gui> tab and look at
      which <gui>Codec</gui> are listed under <gui>Video</gui> and
      <gui>Audio</gui> (if the video also has audio).</p>
    </item>
  </list>

  <p>Ask the person having problems with playback if they have the right codec
  installed. They may find it helpful to search the web for the name of the
  codec plus the name of their video playback application. For example, if your
  video uses the <em>Theora</em> format and you have a friend using Windows
  Media Player to try and watch it, search for “theora windows media player”.
  You will often be able to download the right codec for free if it’s not
  installed.</p>

  <p>If you can’t find the right codec, try the
  <link href="http://www.videolan.org/vlc/">VLC media player</link>. It works
  on Windows and Mac OS as well as Linux, and supports a lot of different video
  formats. Failing that, try converting your video into a different format.
  Most video editors are able to do this, and specific video converter
  applications are available. Check the software installer application to see
  what’s available.</p>

  <note>
    <p>There are a few other problems which might prevent someone from playing
    your video. The video could have been damaged when you sent it to them
    (sometimes big files aren’t copied across perfectly), they could have
    problems with their video playback application, or the video may not have
    been created properly (there could have been some errors when you saved the
    video).</p>
  </note>

</page>
