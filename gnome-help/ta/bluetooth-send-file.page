<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-send-file" xml:lang="ta">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>ஜிம் காம்ப்பெல்</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>பால் W. ஃப்ரியெல்ட்ஸ்</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>உங்கள் தொலைபேசி போன்ற Bluetooth சாதனங்களுக்கு கோப்புகளைப் பகிர்தல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>ஒரு Bluetooth சாதனத்திற்கு கோப்புகளை அனுப்புதல்</title>

  <p>You can send files to connected Bluetooth devices, such as some mobile
  phones or other computers. Some types of devices do not allow the transfer
  of files, or specific types of files. You can send files using the Bluetooth
  settings window.</p>

  <note style="important">
    <p><gui>Send Files</gui> does not work on unsupported devices such as iPhones.</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </item>
    <item>
      <p>Make sure Bluetooth is enabled: the switch in the titlebar should be
      set to on.</p>
    </item>
    <item>
      <p>In the <gui>Devices</gui> list, select the device to which to send the
      files. If the desired device is not shown as <gui>Connected</gui> in the
      list, you need to <link xref="bluetooth-connect-device">connect</link>
      to it.</p>
      <p>வெளிப்புற சாதனத்திற்குரிய ஒரு பலகம் தோன்றும்.</p>
    </item>
    <item>
      <p>Click <gui>Send Files…</gui> and the file chooser will appear.</p>
    </item>
    <item>
      <p>நீங்கள் அனுப்ப விரும்பும் கோப்பைத் தேர்வு செய்துவிட்டு <gui>தேர்ந்தெடு</gui> என்பதை சொடுக்கவும்.</p>
      <p>ஒரு கோப்புறையிலுள்ள பல கோப்புகளை அனுப்ப கோப்புகளைத் தேர்ந்தெடுக்கும் போது <key>Ctrl</key> விசையைப் பிடித்துக்கொள்ளவும்.</p>
    </item>
    <item>
      <p>The owner of the receiving device usually has to press a button to
      accept the file. The <gui>Bluetooth File Transfer</gui> dialog will show
      the progress bar. Click <gui>Close</gui> when the transfer is
      complete.</p>
    </item>
  </steps>

</page>
