<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="as">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>নাটালিয়া ৰুজ লিভা</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Use <gui>Disk Usage Analyzer</gui>, <gui>System Monitor</gui>, or
    <gui>Usage</gui> to check space and capacity.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>কিমান ডিস্ক স্থান অৱশিষ্ট নিৰীক্ষণ কৰক</title>

  <p>You can check how much disk space is left with <app>Disk Usage Analyzer</app>,
  <app>System Monitor</app>, or <app>Usage</app>.</p>

<section id="disk-usage-analyzer">
<title>ডিস্ক ব্যৱহাৰ বিশ্লেষকৰ সৈতে নিৰীক্ষণ কৰক</title>

  <p><app>ডিস্ক ব্যৱহাৰ বিশ্লেষক</app> ব্যৱহাৰ কৰি মুক্ত ডিস্ক স্থান আৰু ডিস্কৰ ক্ষমতা নিৰীক্ষণ কৰিবলৈ:</p>

  <list>
    <item>
      <p>Open <app>Disk Usage Analyzer</app> from the <gui>Activities</gui>
      overview. The window will display a list of file locations together with
      the usage and capacity of each.</p>
    </item>
    <item>
      <p>Click one of the items in the list to view a detailed summary of the
      usage for that item. Click the menu button, and then <gui>Scan
      Folder…</gui> to scan a different location.</p>
    </item>
  </list>
  <p>The information is displayed according to <gui>Folder</gui>,
  <gui>Size</gui>, <gui>Contents</gui> and when the data was last
  <gui>Modified</gui>. See more details in <link href="help:baobab"><app>Disk
  Usage Analyzer</app></link>.</p>

</section>

<section id="system-monitor">

<title>চিস্টেম মনিটৰৰ সৈতে নিৰীক্ষণ কৰক</title>

  <p><app>চিস্টেম মনিটৰ</app>  ৰ সৈতে মুক্ত ডিস্ক স্থান আৰু ডিস্কৰ ক্ষমতা নিৰীক্ষণ কৰিবলৈ:</p>

<steps>
 <item>
  <p><gui>কাৰ্য্যসমূহ</gui> অভাৰভিউৰ পৰা <app>চিস্টেম মনিটৰ</app> এপ্লিকেচন খোলক।</p>
 </item>
 <item>
  <p>Select the <gui>File Systems</gui> tab to view the system’s partitions and
  disk space usage.  The information is displayed according to <gui>Total</gui>,
  <gui>Free</gui>, <gui>Available</gui> and <gui>Used</gui>.</p>
 </item>
</steps>
</section>

<section id="usage">
<title>Check with Usage</title>

  <p>To check the free disk space and disk capacity with <app>Usage</app>:</p>

<steps>
  <item>
    <p>Open the <app>Usage</app> application from the <gui>Activities</gui>
    overview.</p>
  </item>
  <item>
    <p>Select <gui>Storage</gui> tab to view the system’s total <gui>Used</gui>
    and <gui>Available</gui> disk space, as well as the used by the
    <gui>Operating System</gui> and common user’s directories.</p>
  </item>
</steps>

<note style="tip">
  <p>Disk space can be freed from user’s directories and its subdirectories
  by checking the box next to the directory name.</p>
</note>
</section>

<section id="disk-full">

<title>ডিস্ক অত্যাধিক ভৰ্তি থাকিলে কি হব?</title>

  <p>যদি ডিস্ক অত্যাধিক ভৰ্তি থাকে আপুনি নিম্নলিখিত কাৰ্য্যসমূহ কৰিব লাগিব:</p>

 <list>
  <item>
   <p>Delete files that aren’t important or that you won’t use anymore.</p>
  </item>
  <item>
   <p>Make <link xref="backup-why">backups</link> of the important files that
   you won’t need for a while and delete them from the hard drive.</p>
  </item>
 </list>
</section>

</page>
