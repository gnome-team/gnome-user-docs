<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="as">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>জুলিটা ইনকা</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ইউয়ানো মাৰিন</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>শোভা ত্যাগী</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>আন্দ্ৰে ক্লেপাৰ</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>কিবৰ্ড</gui> সংহতিসমূহত কিবৰ্ড চৰ্টকাটসমূহৰ বিৱৰণ দিয়ক অথবা পৰিবৰ্তন কৰক।</desc>
  </info>

  <title>কিবৰ্ড চৰ্টকাটসমূহ সংহতি কৰক</title>

<p>এটা কিবৰ্ড চৰ্টকাটৰ বাবে কি' অথবা কি'সমূহ পৰিবৰ্তন কৰিবলৈ:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>View and Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the desired category, or enter a search term.</p>
    </item>
    <item>
      <p>Click the row for the desired action. The <gui>Set shortcut</gui>
      window will be shown.</p>
    </item>
    <item>
      <p>Hold down the desired key combination, or press <key>Backspace</key> to
      reset, or press <key>Esc</key> to cancel.</p>
    </item>
  </steps>


<section id="defined">
<title>পূৰ্ব-নিৰ্ধাৰিত চৰ্টকাটসমূহ</title>
  <p>পূৰ্ব-সংৰূপিত বহুতো চৰ্টকাট আছে যাক সলনি কৰিব পাৰি, যাক নিম্নলিখিত বিভাগসমূহত দলবদ্ধ কৰা হৈছে:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accessibility</title>
  <tr>
	<td><p>লিখনী আকাৰ সৰু কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উচ্চ কন্ট্ৰাস্ট অন বা অফ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>লিখনী আকাৰ বৃদ্ধি কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>অন-স্ক্ৰিন কিবৰ্ড অন অথবা অফ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>স্ক্ৰিন ৰিডাৰ অন বা অফ কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>জুম অন বা অফ কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>জুম ইন</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>জুম আউট</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>আৰম্ভকসমূহ</title>
  <tr>
	<td><p>ঘৰ ফোল্ডাৰ</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> or <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>গণক আৰম্ভ কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> or <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>ই-মেইল ক্লাএন্ট আৰম্ভ কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> or <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>সহায় ব্ৰাউছাৰ আৰম্ভ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>ৱেব ব্ৰাউছাৰ আৰম্ভ কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> or <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>সন্ধান কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> or <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>সংহতিসমূহ</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>মাৰ্গদৰ্শন</title>
  <tr>
	<td><p>সকলো স্বাভাৱিক উইন্ডো লুকাওক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the left</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the right</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor down</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the left</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the right</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor up</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক বাওঁফালে এটা কৰ্মস্থানলৈ স্থানান্তৰ কৰক</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key>
  <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক সোঁফালে এটা কৰ্মস্থানলৈ স্থানান্তৰ কৰক</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window to last workspace</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক কৰ্মস্থান ১লৈ স্থানান্তৰ কৰক</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক কৰ্মস্থান ২লৈ স্থানান্তৰ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক কৰ্মস্থান ৩লৈ স্থানান্তৰ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক কৰ্মস্থান ৪লৈ স্থানান্তৰ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>এপ্লিকেচনসমূহ চুইচ কৰক</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>চিস্টেম নিয়ন্ত্ৰণসমূহ অদল বদল কৰক</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>চিস্টেম নিয়ন্ত্ৰণসমূহ প্ৰত্যক্ষভাৱে অদল বদল কৰক</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to last workspace</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>কৰ্মস্থান ১লৈ যাওক</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>কৰ্মস্থান ২লৈ যাওক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>কৰ্মস্থান ৩লৈ যাওক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>কৰ্মস্থান ৪লৈ যাওক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
        <td><p>উইন্ডোসমূহ চুইচ কৰক</p></td>
        <td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোসমূহ প্ৰত্যক্ষভাৱে অদল বদল কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>এটা এপৰ উইন্ডোসমূহ প্ৰত্যক্ষভাৱে অদল বদল কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>এটা এপ্লিকেচনৰ উইন্ডোসমূহ অদল বদল কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>স্ক্ৰিনশ্বটসমূহ</title>
  <tr>
	<td><p>Save a screenshot of a window</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of the entire screen</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Launch the screenshot tool</p></td>
	<td><p><key>Print</key></p></td>
  </tr>
  <tr>
        <td><p>Record a short screencast</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>শব্দ আৰু মাধ্যম</title>
  <tr>
	<td><p>বাহিৰ কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Eject)</p></td>
  </tr>
  <tr>
	<td><p>মিডিয়া প্লেয়াৰ আৰম্ভ কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Audio media)</p></td>
  </tr>
  <tr>
	<td><p>Microphone mute/unmute</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>পৰৱৰ্তী ট্ৰেক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Audio next)</p></td>
  </tr>
  <tr>
	<td><p>প্লেবেক বিৰাম দিয়ক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Audio pause)</p></td>
  </tr>
  <tr>
	<td><p>চলাওক (অথবা চলাওক/ৰখাওক)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Audio play)</p></td>
  </tr>
  <tr>
	<td><p>পূৰ্বৱৰ্তী ট্ৰেক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Audio previous)</p></td>
  </tr>
  <tr>
	<td><p>প্লেবেক বন্ধ কৰক</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Audio stop)</p></td>
  </tr>
  <tr>
	<td><p>ভলিউম কম</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Audio lower volume)</p></td>
  </tr>
  <tr>
	<td><p>ভলিউম মৌন</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Audio mute)</p></td>
  </tr>
  <tr>
	<td><p>ভলিউম বেছি</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Audio raise volume)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>চিস্টেম</title>
  <tr>
        <td><p>সক্ৰিয় অধিসূচনাত ফকাচ কৰক</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>পৰ্দা লক কৰক</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the Power Off dialog</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restore the keyboard shortcuts</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>সকলো এপ্লিকেচন দেখুৱাওক</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>কাৰ্য্যসমূহ অভাৰভিউ দেখুৱাওক</p></td>
	<td><p><key>Super</key></p></td>
  </tr>
  <tr>
	<td><p>Show the notification list</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the system menu</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>ৰান কমান্ড প্ৰম্পট দেখুৱাওক</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>টাইপিং</title>
  <tr>
  <td><p>পৰৱৰ্তী ইনপুট উৎসলৈ যাওক</p></td>
  <td><p><keyseq><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>পূৰ্বৱৰ্তী ইনপুট উৎসলৈ যাওক</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Windows</title>
  <tr>
	<td><p>উইন্ডো মেনু সক্ৰিয় কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>Space</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>উইন্ডো বন্ধ কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>উইন্ডোক লুকাওক</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোটোক অন্য উইন্ডোৰ তলত নমাওক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডো ডাঙৰ কৰক</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক আনুভূমিকভাৱে ডাঙৰ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক উলম্বভাৱে ডাঙৰ কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডো স্থানান্তৰ কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>অন্যান্য উইন্ডোৰ উপৰত উইন্ডো উত্থাপন কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>যদি উইন্ডো ঢকা থাকে তাক উত্থাপন কৰক, নহলে তলত নমাওক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>উইন্ডোক পুনৰ আকাৰ দিয়ক</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>উইন্ডো পুনৰুদ্ধাৰ কৰক</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>পূৰ্ণপৰ্দা অৱস্থা অদল-বদল কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
	<td><p>সৰ্বোচ্চ অবস্থা অদল-বদল কৰক</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>সকলো কৰ্মস্থানত অথবা এটাত থকা উইন্ডো অদল বদল কৰক</p></td>
	<td><p>অসামৰ্থবান</p></td>
  </tr>
  <tr>
        <td><p>বিভাজনক বাঁওফালে দৰ্শন কৰক</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>বিভাজনক সোঁফালে দৰ্শন কৰক</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>স্বনিৰ্বাচিত চৰ্টকাটসমূহ</title>

  <p>To create your own application keyboard shortcut in the
  <gui>Keyboard</gui> settings:</p>

  <steps>
    <item>
      <p>Select <gui>Custom Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Type a <gui>Name</gui> to identify the shortcut, and a
      <gui>Command</gui> to run an application.
      For example, if you wanted the shortcut to open <app>Rhythmbox</app>, you
      could name it <input>Music</input> and use the <input>rhythmbox</input>
      command.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut…</gui> button. In the
      <gui>Add Custom Shortcut</gui> window, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p><gui>যোগ কৰক</gui> ক্লিক কৰক.</p>
    </item>
  </steps>

  <p>The command name that you type should be a valid system command. You can
  check that the command works by opening a Terminal and typing it in there.
  The command that opens an application cannot have the same name as the
  application itself.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the row of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
