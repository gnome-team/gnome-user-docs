<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="as">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tips such as “Do not let the battery charge get too low”.</desc>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>আপোনাৰ লেপটপ বেটাৰিৰ পৰা সৰ্বাধিক প্ৰাপ্ত কৰক</title>

<p>লেপটপ বেটাৰিসমূহ যিমান পুৰনি হয়, তিমান সিহতৰ চাৰ্জ সংৰক্ষণ কৰাৰ ক্ষমতা কম হয় আৰু সিহতৰ ক্ষমতা কম হয়। কিছুমান প্ৰক্ৰিয়াৰে আপুনি সিহতৰ উপযোগী জীৱনকাল বৃদ্ধি কৰিব পাৰিব, কিন্তু আপুনি এটা বৃহত পাৰ্থক্য নাপাব।</p>

<list>
  <item>
    <p>Do not let the battery run all the way down. Always recharge
    <em>before</em> the battery gets very low, although most batteries have
    built-in safeguards to prevent the battery running too low. Recharging when
    it is only partially discharged is more efficient, but recharging when it
    is only slightly discharged is worse for the battery.</p>
  </item>
  <item>
    <p>তাপে বেটাৰিৰ চাৰ্জিং ক্ষমতাৰ ওপৰত এটা বেয়া প্ৰভাৱে পেলায়। বেটাৰিক প্ৰয়োজনৰ অধিক গৰম হব নিদিব।</p>
  </item>
  <item>
    <p>Batteries age even if you leave them in storage. There is little
    advantage in buying a replacement battery at the same time as you get the
    original battery — always buy replacements when you need them.</p>
  </item>
</list>

<note>
  <p>এই উপদেশ বিশেষকৈ Lithium-Ion (Li-Ion) বেটাৰিসমূহৰ বাবে প্ৰযোজ্য, যি আটাইতকৈ সূলভ। অন্য ধৰণৰ বেটাৰিয়ে ভিন্ন প্ৰক্ৰিয়াৰে লাভদায়ক হব পাৰে।</p>
</note>

</page>
