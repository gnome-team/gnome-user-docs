<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-whydim" xml:lang="as">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="41" date="2021-09-08" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>The screen will dim when the computer is idle in order to save power.</desc>
  </info>

  <title>কিছু সময় পিছত মোৰ পৰ্দা কিয় স্লান হৈ যায়?</title>

  <p>If it is possible to set the brightness of your screen, it will dim when
  the computer is idle in order to save power. When you start using the
  computer again, the screen will brighten.</p>

  <p>To stop the screen from dimming itself:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Power</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Dim Screen</gui> switch to off in the
      <gui>Power Saving Options</gui> section.</p>
    </item>
  </steps>

  <p>The screen will always dim, and dim more aggressively when choosing
  the “Power Saver” power mode. If you donʼt want the screen to dim at all,
  select the “Balanced” power mode instead.</p>

</page>
