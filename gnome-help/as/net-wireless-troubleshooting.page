<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="as">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu তথ্যচিত্ৰ ৱিকিৰ অৱদানকাৰীসকল</name>
    </credit>
    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identify and fix problems with wireless connections.</desc>
  </info>

  <title>বেতাঁৰ নেটৱাৰ্ক সমস্যামুক্তক</title>

  <p>This is a step-by-step troubleshooting guide to help you identify and fix
  wireless problems. If you cannot connect to a wireless network for some
  reason, try following the instructions here.</p>

  <p>আমি আপোনাৰ কমপিউটাৰক ইন্টাৰনেটৰ সৈতে সংযুক্ত কৰিবলৈ নিম্নলিখিত স্তৰবোৰৰে আগবাঢ়িম:</p>

  <list style="numbered compact">
    <item>
      <p>এটা আৰম্ভণি পৰিক্ষা পৰিৱেশন কৰা হৈছে</p>
    </item>
    <item>
      <p>আপোনাৰ হাৰ্ডৱেৰৰ বিষয়ে তথ্য সংগ্ৰহ কৰা হৈছে</p>
    </item>
    <item>
      <p>আপোনাৰ হাৰ্ডৱেৰ পৰিক্ষা কৰা হৈছে</p>
    </item>
    <item>
      <p>আপোনাৰ বেতাঁৰ ৰাউটাৰলৈ এটা সংযোগ সৃষ্টি কৰাৰ চেষ্টা কৰা হৈছে</p>
    </item>
    <item>
      <p>আপোনাৰ মডেম আৰু ৰাউটাৰৰ এটা পৰিক্ষা কৰা হৈছে</p>
    </item>
  </list>

  <p>আৰম্ভ কৰিবলৈ, পৃষ্ঠাৰ ওপৰ সোঁফালে <em>পৰৱৰ্তী</em> লিঙ্কত ক্লিক কৰক। এই লিঙ্ক, আৰু নিম্নলিখিত পৃষ্ঠাসমূহত ইয়াৰ নিচিনা অন্যয়, আপোনাক সহায়কত প্ৰত্যকটো স্তৰৰে নিব।</p>

  <note>
    <title>কমান্ড শাৰী ব্যৱহাৰ কৰা</title>
    <p>Some of the instructions in this guide ask you to type commands into the
    <em>command line</em> (Terminal). You can find the <app>Terminal</app> application in
    the <gui>Activities</gui> overview.</p>
    <p>If you are not familiar with using a command line, don’t worry — this
    guide will direct you at each step. All you need to remember is that
    commands are case-sensitive (so you must type them <em>exactly</em> as they
    appear here), and to press <key>Enter</key> after typing each command to
    run it.</p>
  </note>

</page>
