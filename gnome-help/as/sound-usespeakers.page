<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="as">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>স্পিকাৰসমূহ অথবা হেডফোনসমূহ বাছক আৰু এটা অবিকল্পিত অডিঅ' ইনপুট ডিভাইচ বাছক।</desc>
  </info>

  <title>ভিন্ন স্পিকাৰ অথবা হেডফোন ব্যৱহাৰ কৰক</title>

  <p>You can use external speakers or headphones with your computer. Speakers
  usually either connect using a circular TRS (<em>tip, ring, sleeve</em>) plug
  or a USB.</p>

  <p>If your speakers or headphones have a TRS plug, plug it into the
  appropriate socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light green in color
  or is accompanied by a picture of headphones. Speakers or headphones
  plugged into a TRS socket are usually used by default. If not, see the
  instructions below for selecting the default device.</p>

  <p>Some computers support multi-channel output for surround sound. This
  usually uses multiple TRS jacks, which are often color-coded. If you are
  unsure which plugs go in which sockets, you can test the sound output in the
  sound settings.</p>

  <p>If you have USB speakers or headphones, or analog headphones plugged into
  a USB sound card, plug them into any USB port. USB speakers act as separate
  audio devices, and you may have to specify which speakers to use by
  default.</p>

  <steps>
    <title>Select a default audio output device</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Output</gui> section, select the device that you want to
      use.</p>
    </item>
  </steps>

  <p>Use the <gui style="button">Test</gui> button to check that all
  speakers are working and are connected to the correct socket.</p>

</page>
