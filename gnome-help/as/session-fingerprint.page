<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-fingerprint" xml:lang="as">

  <info>
    <link type="guide" xref="hardware-auth"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.12" date="2014-06-16" status="final"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
      <years>২০১৩</years>
    </credit>
    <credit type="editor">
      <name>জিম কেম্পবেল</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>You can log in to your system using a supported fingerprint scanner
    instead of typing in your password.</desc>
  </info>

  <title>এটা ফিঙ্গাৰপ্ৰিন্টৰ সৈতে লগ ইন কৰক</title>

  <p>If your system has a supported fingerprint scanner, you can record your
  fingerprint and use it to log in.</p>

<section id="record">
  <title>Record a fingerprint</title>

  <p>Before you can log in with your fingerprint, you need to record it so that
  the system can use it to identify you.</p>

  <note style="tip">
    <p>যদি আপোনাৰ আঙুলি খুবেই শুকান, আপোনাৰ ফিংগাৰপ্ৰিন্ট ৰেজিস্টাৰ কৰোতে অসুবিধা হব পাৰে। যদি এনেকুৱা হয়, আপোনাৰ আঙুলিক অলপ তিয়াই, ইয়াক এটা পৰিষ্কাৰ, লিন্ট-মুক্ত কাপোৰ দি পৰিষ্কাৰ কৰক, আৰু পুনৰ চেষ্টা কৰক।</p>
  </note>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press on <gui>Disabled</gui>, next to <gui>Fingerprint Login</gui> to
      add a fingerprint for the selected account. If you are adding the
      fingerprint for a different user, you will first need to
      <gui>Unlock</gui> the panel.</p>
    </item>
    <item>
      <p>Select the finger that you want to use for the fingerprint, then
      <gui style="button">Next</gui>.</p>
    </item>
    <item>
      <p>Follow the instructions in the dialog and swipe your finger at a
      <em>moderate speed</em> over your fingerprint reader. Once the computer
      has a good record of your fingerprint, you will see a <gui>Done!</gui>
      message.</p>
    </item>
    <item>
      <p>Select <gui>Next</gui>. You will see a confirmation message that
      your fingerprint was saved successfully. Select <gui>Close</gui> to
      finish.</p>
    </item>
  </steps>

</section>

<section id="verify">
  <title>Check that your fingerprint works</title>

  <p>এতিয়া আপোনাৰ নতুন ফিংগাৰপ্ৰিন্ট লগিন কাম কৰিছে নে নিৰীক্ষণ কৰক। যদি আপুনি এটা ফিংগাৰপ্ৰিন্ট ৰেজিস্টাৰ কৰে, আপোনাৰ তথাপিও আপোনাৰ পাছৱাৰ্ডৰ সৈতে লগিন কৰাৰ বিকল্প থাকিব।</p>

  <steps>
    <item>
      <p>Save any open work, and then <link xref="shell-exit#logout">log
      out</link>.</p>
    </item>
    <item>
      <p>লগিন পৰ্দাত, তালিকাৰ পৰা আপোনাৰ নাম বাছক। পাছৱাৰ্ড প্ৰৱিষ্টি ফৰ্ম উপস্থিত হব।</p>
    </item>
    <item>
      <p>Instead of typing your password, you should be able to swipe your
      finger on the fingerprint reader.</p>
    </item>
  </steps>

</section>

</page>
