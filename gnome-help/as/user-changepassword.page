<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="as">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>আপোনাৰ একাওন্ট সংহতিত আপোনাৰ পাছৱাৰ্ড সময়ে সময়ে সলনি কৰি আপোনাৰ একাওন্ট সুৰক্ষিত ৰাখক।</desc>
  </info>

  <title>আপোনাৰ পাছৱাৰ্ড পৰিবৰ্তন কৰক</title>

  <p>It is a good idea to change your password from time to time, especially if
  you think someone else knows your password.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to edit user accounts other than your own.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click the label <gui>·····</gui> next to <gui>Password</gui>. If you
      are changing the password for a different user, you will first need to
      <gui>Unlock</gui> the panel and select the account under
      <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>Enter your current password, then a new password. Enter your new
      password again in the <gui>Verify New Password</gui> field.</p>
      <p>You can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
    </item>
    <item>
      <p><gui>Change</gui> ক্লিক  কৰক।</p>
    </item>
  </steps>

  <p>আপুনি <link xref="user-goodpassword">এটা ভাল পাছৱাৰ্ড বাছক</link>। ই আপোনাৰ একাওন্ট সুৰক্ষিত ৰখাত সহায় কৰিব।</p>

  <note>
    <p>When you update your login password, your login keyring password will
    automatically be updated to be the same as your new login password.</p>
  </note>

  <p>If you forget your password, any user with administrator privileges can
  change it for you.</p>

</page>
