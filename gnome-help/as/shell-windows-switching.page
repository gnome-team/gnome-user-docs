<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="as">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>শোভা ত্যাগী</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><keyseq><key>Super</key><key>Tab</key></keyseq> টিপক।</desc>
  </info>

<title>উইন্ডোসমূহৰ মাজত চুইচ কৰক</title>

  <p>You can see all the running applications that have a graphical user
  interface in the <em>window switcher</em>. This makes
  switching between tasks a single-step process and provides a full picture of
  which applications are running.</p>

  <p>এটা কাৰ্য্যস্থানৰ পৰা:</p>

  <steps>
    <item>
      <p>Press
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      to bring up the <gui>window switcher</gui>.</p>
    </item>
    <item>
      <p>চুইচাৰত পৰৱৰ্তী (উজ্জ্বল) উইন্ডো বাছিবলৈ <key xref="keyboard-key-super">Super</key> এৰক।</p>
    </item>
    <item>
      <p>Otherwise, still holding down the <key xref="keyboard-key-super">Super</key>
      key, press <key>Tab</key> to cycle through the list of open
      windows, or <keyseq><key>Shift</key><key>Tab</key></keyseq> to cycle
      backwards.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">আপোনাৰ সকলো খোলা উইন্ডো অভিগম কৰিবলৈ আৰু সিহতৰ মাজত অদল বদল কৰিবলৈ আপুনি তলৰ বাৰত থকা উইন্ডো তালিকা ব্যৱহাৰ কৰিব পাৰিব।</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>উইন্ডো চুইচাৰত উইন্ডোসমূহ এটা এপ্লিকেচন দ্বাৰা দলবদ্ধ কৰা হয়। একাধিক এপ্লিকেচনৰ পূৰ্বদৰ্শন আপুনি ক্লিক কৰোতে পপ ডাউন হয়। <key xref="keyboard-key-super">Super</key> ধৰি থওক আৰু তালিকাত যাবলৈ <key>`</key> (<key>Tab</key> ৰ ওপৰত থকা কি) টিপক।</p>
  </note>

  <p>আপুনি <key>→</key> অথবা <key>←</key> কি'সমূহ ব্যৱহাৰ কৰি উইন্ডো চুইচাৰত এপ্লিকেচন আইকনসমূহৰ মাজত ভ্ৰমণ কৰিব পাৰিব, অথবা মাউছৰ সৈতে ক্লিক কৰি এটা বাছক।</p>

  <p>এটা উইন্ডোৰ সৈতে এপ্লিকেচনসমূহৰ পূৰ্বদৰ্শন <key>↓</key> কি'ৰ সৈতে প্ৰদৰ্শন কৰিব পাৰি।</p>

  <p>From the <gui>Activities</gui> overview, click on a
  <link xref="shell-windows">window</link> to switch to it and leave the
  overview. If you have multiple
  <link xref="shell-windows#working-with-workspaces">workspaces</link> open,
  you can click on each workspace to view the open windows on each
  workspace.</p>

</page>
