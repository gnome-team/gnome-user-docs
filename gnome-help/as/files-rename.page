<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="as">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-rename-multiple"/>
    <link type="seealso" xref="files-rename-music-metadata"/>

    <revision version="gnome:47" date="2024-08-31" status="candidate"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ফাইল অথবা ফোল্ডাৰৰ নাম পৰিবৰ্তন কৰক।</desc>
  </info>

  <title>এটা ফাইল অথবা ফোল্ডাৰ পুনৰনামকৰণ কৰক</title>

  <p>As with other file managers, you can use <app>Files</app> to change the
  name of a file or folder.</p>

  <steps>
    <title>এটা ফাইল অথবা ফোল্ডাৰ পুনৰ নামকৰণ কৰিবলৈ:</title>
    <item><p>বস্তুত ৰাইট-ক্লিক কৰক আৰু <gui>পুনৰ নামকৰণ কৰক</gui> বাছক, অথবা ফাইল বাছক আৰু <key>F2</key> টিপক।</p></item>
    <item><p>Type the new name and press <key>Enter</key> or click
    <gui>Rename</gui>.</p></item>
  </steps>

  <p>When you rename a file, only the first part of the name of the file is
  selected, not the file extension (the part after the last <file>.</file>).
  The extension normally denotes what type of file it is (for example,
  <file>file.pdf</file> is a PDF document), and you usually do not want to
  change that. If you need to change the extension as well, select the entire
  file name and change it.</p>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the sidebar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

  <section id="valid-chars">
    <title>ফাইল নামৰ বাবে বৈধ আখৰবোৰ</title>

    <p>You can use any character except the <file>/</file> (slash) character in
    file names. Some devices, however, use a <em>file system</em> that has more
    restrictions on file names. Therefore, it is a best practice to avoid the
    following characters in your file names: <file>|</file>, <file>\</file>,
    <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>,
    <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>If you name a file with a <file>.</file> as the first character, the
    file will be <link xref="files-hidden">hidden</link> when you attempt to
    view it in the file manager.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>সাধাৰণ সমস্যাবোৰ</title>

    <terms>
      <item>
        <title>ফাইল নাম ইতিমধ্যে ব্যৱহৃত</title>
        <p>You cannot have two files or folders with the same name in the same
        folder. If you try to rename a file to a name that already exists in
        the folder you are working in, the file manager will not allow it.</p>
        <p>File and folder names are case sensitive, so the file name
        <file>File.txt</file> is not the same as <file>FILE.txt</file>.
        Using different file names like this is allowed, though it is not
        recommended.</p>
      </item>
      <item>
        <title>ফাইল নাম অত্যাধিক দীঘল</title>
        <p>On some file systems, file names can have no more than 255
        characters in their names.  This 255 character limit includes both the
        file name and the path to the file (for example,
        <file>/home/wanda/Documents/work/business-proposals/…</file>), so you
        should avoid long file and folder names where possible.</p>
      </item>
      <item>
        <title>পুনৰ নামকৰণ কৰিবলৈ বিকল্প ধূসৰ কৰা আছে</title>
        <p>If <gui>Rename</gui> is grayed out, you do not have permission to
        rename the file. You should use caution with renaming such files, as
        renaming some protected files may cause your system to become unstable.
        See <link xref="nautilus-file-properties-permissions"/> for more
        information.</p>
      </item>
    </terms>

  </section>

</page>
