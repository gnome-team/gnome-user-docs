<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="as">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu তথ্যচিত্ৰ ৱিকিৰ অৱদানকাৰীসকল</name>
    </credit>
    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>পৰৱৰ্তী সমস্যামুক্তি স্তৰসমূহত আপোনাক বিৱৰণ যেনে আপোনাৰ বেতাঁৰ এডাপ্টাৰৰ আৰ্হি সংখ্যাৰ প্ৰয়োজন হব পাৰে।</desc>
  </info>

  <title>বেতাঁৰ নেটৱাৰ্ক সমস্যামুক্তক</title>
  <subtitle>আপোনাৰ নেটৱাৰ্ক হাৰ্ডৱেৰৰ বিষয়ে তথ্য সংগ্ৰহ কৰক</subtitle>

  <p>In this step, you will collect information about your wireless network
  device. The way you fix many wireless problems depends on the make and model
  number of the wireless adapter, so you will need to make a note of these
  details. It can also be helpful to have some of the items that came with your
  computer too, like device driver installation discs. Look for the following
  items, if you still have them:</p>

  <list>
    <item>
      <p>আপোনাৰ বেতাঁৰ ডিভাইচসমূহৰ বাবে পেকেইজিং আৰু নিৰ্দেশ (বিশেষভাৱে আপোনাৰ ৰাউটাৰৰ বাবে ব্যৱহাৰকাৰী সহায়ক)</p>
    </item>
    <item>
      <p>আপোনাৰ বেতাঁৰ এডাপ্টাৰৰ বাবে ড্ৰাইভাৰসমূহ অন্তৰ্ভুক্ত কৰা ডিস্ক (যদিও ই কেৱল Windows ড্ৰাইভাৰ অন্তৰ্ভুক্ত কৰে)</p>
    </item>
    <item>
      <p>The manufacturers and model numbers of your computer, wireless adapter
      and router. This information can usually be found on the
      underside or reverse of the device.</p>
    </item>
    <item>
      <p>Any version or revision numbers that may be printed on your wireless
      network devices or their packaging. These can be especially helpful, so
      look carefully.</p>
    </item>
    <item>
      <p>Anything on the driver disc that identifies either the device itself,
      its “firmware” version, or the components (chipset) it uses.</p>
    </item>
  </list>

  <p>যদি সম্ভব, এটা বিকল্প কাৰ্য্যকৰি ইন্টাৰনেট সংযোগলৈ অভিগম কৰাৰ চেষ্টা কৰক যাতে আপুনি প্ৰয়োজন সাপেক্ষে চফ্টৱেৰ আৰু ড্ৰাইভাৰসমূহ ডাউনল'ড কৰিব পাৰে। (এটা ইথাৰনেট নেটৱাৰ্ক কেবুলৰ সৈতে আপোনাৰ কমপিউটাৰক প্ৰত্যক্ষভাৱে ৰাউটাৰত প্লাগ কৰাটো ইয়াক প্ৰদান কৰাৰ এটা ধৰণ, কিন্তু কেৱল প্ৰয়োজন হলেহে ইয়াক প্লাগ কৰিব।)</p>

  <p>যেতিয়া আপেনাৰ ওচৰত যিমান সম্ভব এই বস্তুবোৰ থাকিব, <gui>পৰৱৰ্তী</gui> ক্লিক কৰিব।</p>

</page>
