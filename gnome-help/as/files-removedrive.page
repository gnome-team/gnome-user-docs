<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="as">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>এটা USB ফ্লেশ্ব ড্ৰাইভ, CD, DVD, অথবা অন্য ডিভাইচ বাহিৰ উলিৱাওক অথবা আনমাউন্ট কৰক।</desc>
  </info>

  <title>এটা বহিৰ্তম ড্ৰাইভ সুৰক্ষিতভাৱে আতৰাওক</title>

  <p>When you use external storage devices like USB flash drives, you
  should safely remove them before unplugging them. If you just unplug
  a device, you run the risk of unplugging while an application is still
  using it. This could result in some of your files being lost or damaged.
  When you use an optical disc like a CD or DVD, you can use the same steps
  to eject the disc from your computer.</p>

  <steps>
    <title>এটা আতৰাব পৰা ডিভাইচ বাহিৰ উলিৱাবলৈ:</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>কাষবাৰত ডিভাইচক অৱস্থিত কৰক। ইয়াৰ নামৰ কাষত এটা সৰু উলিৱাওক আইকন থাকিব লাগে। ডিভাইচক সুৰক্ষিতভাৱে আতৰাবলৈ অথবা উলিৱাবলৈ উলিৱাওক আইকনত ক্লিক কৰক।</p>
      <p>বিকল্পভাৱে, আপুনি কাষবাৰত ডিভাইচৰ নামত ৰাইট-ক্লিক কৰি <gui>উলিৱাওক</gui> বাছিব পাৰিব।</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>ব্যৱহৃত এটা ডিভাইচক সুৰক্ষিতভাৱে আতৰাওক</title>

  <p>If any of the files on the device are open and in use by an application,
  you will not be able to safely remove the device. You will be prompted with a
  window telling you <gui>Volume is busy</gui>. To safely remove the device:</p>

  <steps>
    <item><p><gui>বাতিল কৰক</gui> ক্লিক কৰক।</p></item>
    <item><p>ডিভাইচত সকলো ফাইল বন্ধ কৰক।</p></item>
    <item><p>ডিভাইচক সুৰক্ষিতভাৱে আতৰাবলৈ অথবা বাহিৰ উলিৱাবলৈ বাহিৰ উলিৱাওক আইকনত ক্লিক কৰক।</p></item>
    <item><p>বিকল্পভাৱে, আপুনি কাষবাৰত ডিভাইচৰ নামত ৰাইট-ক্লিক কৰি <gui>উলিৱাওক</gui> বাছিব পাৰিব।</p></item>
  </steps>

  <note style="warning"><p>আপুনি ফাইলসমূহ বন্ধ নকৰাকৈ ডিভাইচ আতৰাবলৈ <gui>তথাপিও বাহিৰ উলিৱাওক</gui> বাছিব পাৰিব। ইয়াৰ বাবে খোলা ফাইলসমূহ থকা এপ্লিকেচনসমূহত ত্ৰুটি হব পাৰে।</p></note>

  </section>

</page>
