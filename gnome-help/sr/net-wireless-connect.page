<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="net-wireless-connect" xml:lang="sr">

  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Приступите интернету — бежично.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Повезивање на бежичну мрежу</title>

<p>Ако имате рачунар са бежичном картицом, можете да се повежете на бежичну мрежу којој сте у домету да бисте добили приступ интернету, видели дељене датотеке на мрежи, и тако даље.</p>

<steps>
  <item>
    <p>Отворите <gui xref="shell-introduction#systemmenu">изборник система</gui> на десној страни горње траке.</p>
  </item>
  <item>
    <p>Изаберите <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-disabled-symbolic.svg"/> Бежична мрежа</gui>. Одељак бежичне везе у изборнику ће се раширити.</p>
  </item>
  <item>
    <p>Кликните на назив мреже на коју желите да се повежете.</p>
    <p>Ако назив мреже није на списку, премакните списак на доле. Ако још увек не видите мрежу, може бити да сте ван домета или да је мрежа <link xref="net-wireless-hidden">можда скривена</link>.</p>
  </item>
  <item>
    <p>Ако је мрежа заштићена лозинком (<link xref="net-wireless-wepwpa">кључем шифровања</link>), унесите је када вам буде била затражена и кликните на <gui>Успостави везу</gui>.</p>
    <p>Ако не знате кључ, може бити да је записан на полеђини бежичног усмеривача или основне поставе или у приручнику упутства, или ћете морати да питате особу која администрира бежичну мрежу.</p>
  </item>
  <item>
    <p>Иконица мреже ће променити изглед како рачунар буде покушавао да се повеже на мрежу.</p>
  </item>
  <item>
    <if:choose>
    <if:when test="platform:gnome-classic">
    <p>Ако је повезивање успешно обављено, иконица ће се променити у тачкице са неколико усправних штапића изнад (<media its:translate="no" type="image" mime="image/svg" src="figures/classic-topbar-network-wireless-signal-excellent.svg" width="28" height="28"/>). Више штапића означавају јачу везу са мрежом. Мање штапића значи да је веза слаба и неће бити много поуздана.</p>
    </if:when>
    <p>Ако је повезивање успешно обављено, иконица ће се променити у тачкице са неколико усправних штапића изнад (<media its:translate="no" type="image" mime="image/svg" src="figures/topbar-network-wireless-signal-excellent.svg" width="28" height="28"/>). Више штапића означавају јачу везу са мрежом. Мање штапића значи да је веза слаба и неће бити много поуздана.</p>
    </if:choose>
  </item>
</steps>

  <p>Ако повезивање на мрежу није успешно, од вас може бити затражено да поново унесете вашу лозинку или вас може обавестити да је веза са мрежом прекинута. Узрок овоме може бити велики број ствари. Можда сте унели погрешну лозинку, бежични сигнал може бити превише слаб, или можда бежична картица вашег рачунара није исправна. За више помоћи погледајте <link xref="net-wireless-troubleshooting"/>.</p>

  <p>Јача веза на бежичну мрежу не мора обавезно да значи да ћете имати бржу везу на интернет, или да ћете имати веће брзине преузимања. Бежична веза повезује ваш рачунар на <em>уређај који обезбеђује везу на интернет</em> (на усмеривач или модем), али ове две везе су заправо различите, тако да ће радити при другачијим брзинама.</p>

</page>
