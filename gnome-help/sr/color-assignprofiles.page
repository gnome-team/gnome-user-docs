<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="sr">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Ричардс Хјуџиз</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Погледајте <guiseq><gui>Подешавања</gui><gui>Боје</gui></guiseq> да додате профил боје за ваш екран.</desc>
  </info>

  <title>Како да доделим профил уређају?</title>

  <p>Можете пожелети да доделите профил боје вашем екрану или штампачу тако да боје које приказује буду што прецизније.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Подешавања</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Подешавања</gui>.</p>
    </item>
    <item>
      <p>Кликните <gui>Боја</gui> у бочној траци да отворите панел.</p>
    </item>
    <item>
      <p>Изаберите уређај за који желите да додате профил.</p>
    </item>
    <item>
      <p>Кликните <gui>Додај профил</gui> да изаберете постојећи или да увезете нови профил.</p>
    </item>
    <item>
      <p>Притисните <gui>Додај</gui> да потврдите ваш избор.</p>
    </item>
    <item>
      <p>Да измените коришћени профил, изаберите профил који желите да користите и притисните <gui>Омогући</gui> да потврдите ваш избор.</p>
    </item>
  </steps>

  <p>Сваки уређај може имати више додељених профила, али само један профил може бити <em>основни</em>. Основни профил се користи тамо где нема додатних података који би омогућили да профил буде самостално изабран. Пример самосталног бирања би био ако је један профил направљен за сјајан папир а други за обичан папир.</p>

  <p>Ако је хардвер за калибрацију повезан, онда ће дугме <gui>Калибрација…</gui> направити нови профил.</p>

</page>
