<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="sr">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Андре Клапер</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Пребаците на други језик за корисничко сучеље и текст помоћи.</desc>
  </info>

  <title>Промените језик који користите</title>

  <p>Можете да користите вашу радну површ и програме на неком од десетак језика, који су обезбеђени својим пакетима језика инсталираним на вашем рачунару.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Your Account</gui> section, click <gui>Language</gui>.</p>
    </item>
    <item>
      <p>Изаберите жељени регион и језик. Ако ваш регион и језик нису на списку, притисните <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> на дну списка да изаберете из свих доступних региона и језика.</p>
    </item>
    <item>
      <p>Кликните <gui style="button">Изабери</gui> да сачувате.</p>
    </item>
    <item>
      <p>Ваша сесија треба да буде поново покренута да би измене деловале. Или кликните <gui style="button">Поново покрени</gui>, или се касније ручно пријавите.</p>
    </item>
  </steps>

  <p>Неки преводи могу бити непотпуни, а одређени програми можда уопште не подржавају српски језик. Сваки непреведени текст ће бити приказан на језику на коме је софтвер оригинално развијен, обично на америчком енглеском.</p>

  <p>Постоје неке посебне фасцикле у вашој личној фасцикли у којима програми могу да ускладиште ствари као што су музика, слике и документи. Ове фасцикле користе стандардне називе у складу са вашим језиком. Када се поново пријавите, бићете упитани да ли желите да преименујете те фасцикле на стандардне називе за изабрани језик. Ако планирате да користите нови језик сво време, требали бисте да ажурирате називе фасцикли.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    section for the <gui>Login Screen</gui> in the
    <gui>Region &amp; Language</gui> panel.</p>
  </note>

</page>
