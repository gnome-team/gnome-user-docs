<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="sr">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>
    <revision version="gnome:45" date="2024-03-03" status="review"/>

    <credit type="author">
      <name>Тифани Антополоски</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Додајте, обришите, и преименујте обележиваче у управнику датотека.</desc>

  </info>

  <title>Уредите обележиваче фасцикле</title>

  <p>Ваши обележивачи су наведени у бочној површи у управнику датотека.</p>

  <steps>
    <title>Додајте обележивач:</title>
    <item>
      <p>Отворите фасциклу (или место) које желите да обележите.</p>
    </item>
    <item>
      <p>Кликните на тренутну фасциклу у траци путање и затим изаберите <gui style="menuitem">Додај у обележиваче</gui>.</p>
      <note>
        <p>Можете такође да превучете фасциклу на бочну површ, и да је пустите преко ставке <gui>Нови обележивач</gui>, који се појављује динамички.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Обришите обележивач:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Remove from Bookmarks</gui> from the menu.</p>
    </item>
  </steps>

  <steps>
    <title>Преименујте обележивач:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Rename</gui>.</p>
    </item>
    <item>
      <p>У пољу за текст <gui>Назив</gui>, упишите нови назив за обележивач.</p>
      <note>
        <p>Преименовање обележивача неће променити назив фасцикле. Ако имате обележиваче ка две различите фасцикле на два различита места, али чија свака има исти назив, обележивачи ће имати исти назив, и нећете моћи да их разликујете. У овим случајевима, корисно је да обележивачу дате назив другачији од назива фасцикле на коју указује.</p>
      </note>
    </item>
  </steps>

</page>
