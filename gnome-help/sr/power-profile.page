<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-profile" xml:lang="sr">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Уравнотежите учинковитост и коришћење батерије.</desc>
  </info>

  <title>Изаберите профил напајања</title>

  <p>Можете да управљате коришћењем напајања изабравши <gui>Режим напајања</gui>. Постоје три могућа профила:</p>
  
  <list>
    <item>
      <p><gui>Учинковитост</gui>: Висока учинковитост и коришћење напајања. Овај ће режим бити видљив само ако га ваш рачунар подржава. Биће избирљив ако се ваш рачунар напаја наизменичном струјом. Ако ваш уређај подржава откривање крила, нећете моћи да изаберете ову опцију ако уређај ради на нечијем крилу.</p>
    </item>
    <item>
      <p><gui>Уравнотежено</gui>: Уобичајена учинковитост и коришћење напајања. Ово је основна поставка.</p>
    </item>
    <item>
      <p><gui>Штедиша напајања</gui>: Умањена учинковитост и коришћење напајања. Ова поставка повећава трајање батерије. Може укључити неке опције уштеде напајања, као што је нападно затамњење екрана, и спречи њихово искључивање.</p>
    </item>
  </list>

  <steps>
    <title>Да изаберете профил напајања:</title>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Напајање</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Напајање</gui> да отворите панел.</p>
    </item>
    <item>
      <p>У одељку <gui>Режим напајања</gui>, изаберите један од профила.</p>
    </item>
    
  </steps>

</page>
