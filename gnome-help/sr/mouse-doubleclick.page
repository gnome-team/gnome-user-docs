<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="sr">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Фил Бул</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шоба Тјаги</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Одредите колико брзо ћете морати да притиснете дугме миша по други пут да бисте извршили двоструки клик.</desc>
  </info>

  <title>Подесите брзину двоструког клика</title>

  <p>Двоклик се одиграва само када тастер миша притиснете довољно брзо по други пут. Ако до другог притиска дође после дужег времена након првог, добићете само два одвојена клика, а не двоклик. Ако имате потешкоћа да довољно брзо притиснете тастер миша, требали бисте да повећате временски интервал.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Приступачност</gui>.</p>
    </item>
    <item>
      <p>Кликните <gui>Приступачност</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Select the <gui>Pointing &amp; Clicking</gui> section to open it.</p>
    </item>
    <item>
      <p>Adjust the <gui>Double-Click Delay</gui> slider to a value you find comfortable.</p>
    </item>
  </steps>

  <p>Ако ваш миш извршава двоклик када желите само један клик чак и када сте повећали временски интервал дуплог клика, може бити да је ваш миш неисправан. Покушајте да прикључите неки други миш на ваш рачунар и видите да ли ће радити исправно. Другачије, прикључите ваш миш на неки други рачунар и видите да ли још увек има исти проблем.</p>

  <note>
    <p>Ово подешавање ће утицати и на миша и на додирну таблу, као и на сваки други указивачки уређај.</p>
  </note>

</page>
