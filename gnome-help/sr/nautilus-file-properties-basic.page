<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="sr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:46" date="2024-03-05" status="final"/>

    <credit type="author">
      <name>Тифани Антополоски</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Погледајте основне податке о датотеци, подесите овлашћења, и изаберите основне програме.</desc>

  </info>

  <title>Својства датотеке</title>

  <p>Да погледате податке о датотеци или фасцикли, кликните десним тастером миша на њу и изаберите <gui>Особине</gui>. Можете такође да изаберете датотеку и да притиснете <keyseq><key>Алт</key><key>Унеси</key></keyseq>.</p>

  <p>Прозор особина датотеке ће вам показати податке као што је врста датотеке, величина, и када сте је последњи пут изменили. Ако су вам често потребни ови подаци, можете да их прикажете у <link xref="nautilus-list">колонама прегледа списком</link> или у <link xref="nautilus-display#icon-captions">натпису иконице</link>.</p>

  <p>For certain types of files, such as images and videos, there will be an
  extra <gui>Properties</gui> entry that provides information like the
  dimensions, duration, and codec.</p>

  <p>For the <gui>Permissions</gui> entry see
  <link xref="nautilus-file-properties-permissions"/>.</p>

<section id="basic">
 <title>Основна својства</title>
 <terms>
  <item>
    <title><gui>Назив</gui></title>
    <p>The name of the file or folder.</p>
    <note style="tip">
      <p>To rename a file, see <link xref="files-rename"/>.</p>
    </note>
  </item>
  <item>
    <title><gui>Врста</gui></title>
    <p>Ово ће вам помоћи да одредите врсту датотеке, као што је ПДФ документ, текст Отвореног документа, или ЈПЕГ слика. Врста датотеке одређује који програми могу да отворе датотеку, уз друге ствари. На пример, не можете да отворите слику програмом за пуштање музике. Погледајте <link xref="files-open"/> за више података о овоме.</p>
    <note style="tip">
      <p>To change the default application to open a file type, see <link xref="files-open#default"/>.</p>
    </note>
  </item>

  <item>
    <title>Садржај</title>
    <p>Ово поље се приказује ако разгледате особине фасцикле уместо датотеке. Помаже вам да видите број ставки у фасцикли. Ако фасцикла садржи друге фасцикле, свака унутрашња фасцикла се броји као једна ставка, чак и ако садржи додатне ставке. Свака датотека се такође броји као једна ставка. Ако је фасцикла празна, садржај ће приказати <gui>ништа</gui>.</p>
  </item>

  <item>
    <title>Величина</title>
    <p>Ово поље се приказује ако посматрате датотеку (а не фасциклу). Величина датотеке вам говори о томе колико заузима простора на диску. Ово вам такође показује колико времена ће бити потребно за преузимање датотеке или слање путем е-поште (великим датотекама је потребно више времена за слање/пријем).</p>
    <p>Величине могу бити дате у бајтовима, KB, MB, или GB; у случају последња три, величина у бајтовима ће такође бити дата у загради. Технички, 1 KB је 1024 бајта, 1 MB је 1024 KB и тако редом.</p>
  </item>

  <item>
    <title>Слободан простор</title>
    <p>Ово се приказује само за фасцикле. Приказује износ простора диска доступног на диску на коме се налази фасцикла. Ово је корисно приликом провере заузећа чврстог диска.</p>
  </item>

  <item>
    <title>Родитељска фасцикла</title>
    <p>Путања сваке датотеке на вашем рачунару је дата њеном <em>апсолутном путањом</em>. То је јединствена „адреса“ датотеке на вашем рачунару, коју чини списак фасцикли кроз које ћете морати да прођете да бисте пронашли датотеку. На пример, ако Пера има датотеку под називом <file>Плате.pdf</file> у својој личној фасцикли, њена родитељска фасцикла биће <file>/home/pera</file> а њена путања биће <file>/home/pera/Плате.pdf</file>.</p>
  </item>

  <item>
    <title>Приступљен</title>
    <p>Датум и време када је датотека била отворена последњи пут.</p>
  </item>

  <item>
    <title>Измењено</title>
    <p>Датум и време када је датотека била последњи пут измењена и сачувана.</p>
  </item>

  <item>
    <title>Created</title>
    <p>The date and time when the file was created.</p>
  </item>
 </terms>
</section>

</page>
