<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="sr">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Отворите подешавања мреже и пребаците режим у авиону на укључено.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Искључите бежичну (режим у авиону)</title>

<p>Ако сте понели рачунар и налазите се у авиону (или на неком другом месту на коме бежичне везе нису дозвољене), требате да угасите вашу бежичну везу. Можда ћете морати да искључите вашу бежичну везу из других разлога (да сачувате батерију, на пример).</p>

  <note>
    <p>Коришћење <em>Режима у авиону</em> ће у потпуности да искључи све бежичне везе, укључујући бежичну, 3Г и блутут везу.</p>
  </note>

  <p>Да искључите режим у авиону:</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Бежична</gui>.</p>
    </item>
    <item>
      <p>Кликните <gui>Бежична</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Пребаците <gui>Режим у авиону</gui> на укључено. Ово ће искључити вашу бежичну везу све док поново не искључите режим у авиону.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Можете искључити вашу бежичну везу из <gui xref="shell-introduction#systemmenu">изборника система</gui> тако што ћете кликнути на назив везе и изабрати <gui>Искључи</gui>.</p>
  </note>

</page>
