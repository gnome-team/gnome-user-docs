<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="sr">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision version="gnome:44" date="2023-12-29" status="review"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Андре Клапер</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Видни претпреглед у вашу радну површ, горњу траку, и преглед <gui>Активности</gui>.</desc>
  </info>

  <title>Видни претпреглед Гнома</title>

  <p>Гном карактерише корисничко сучеље осмишљено да вам не стоји на путу, умањи ометање, и да вам помогне да обавите посао. Када се први пут пријавите, видећете преглед <gui>Активности</gui> и горњу траку.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Горња трака Гномове шкољке</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Горња трака Гномове шкољке</p>
    </media>
  </if:when>
</if:choose>

  <p>Горња трака обезбеђује приступ вашим прозорима и програмима, вашем календару и састанцима, и <link xref="status-icons">особинама система</link> као што је звук, умрежавање, и напајање. У изборнику система на горњој траци, можете да измените јачину звука или осветљеност екрана, да уређујете појединости ваше <gui>бежичне</gui> везе, да проверите стање батерије, да се одјавите или да промените корисника, или да искључите рачунар.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>Преглед <gui>Активности</gui></title>

  <p if:test="!platform:gnome-classic">Када покренете Гном, аутоматски улазите у претпреглед <gui>Активности</gui>. Претпреглед вам омогућава да приступите вашим прозорима и програмима. У претпрегледу можете такође само да започнете да куцате да бисте претражили ваше програме, датотеке, фасцикле и веб.</p>

  <p if:test="!platform:gnome-classic">To access the overview at any time,
  click the Activities button in the top-left corner, or just move your mouse
  pointer to the top-left hot corner. You can also press the
  <key xref="keyboard-key-super">Super</key> key on your keyboard.</p>

  <p if:test="platform:gnome-classic">Да приступите прозорима и програмима, кликните на дугме доле лево на екрану у списку прозора. Исто тако можете да притисете тастер <key xref="keyboard-key-super">Супер</key> да видите претпреглед са живим сличицама свих прозора на тренутном радном простору.</p>

  <media type="image" its:translate="no" src="figures/shell-activities-dash.png" height="65" style="floatend floatright" if:test="!target:mobile, !platform:gnome-classic">
    <p>Activities button and Dash</p>
  </media>
  <p if:test="!platform:gnome-classic">На дну претпрегледа, наћи ћете <em>полетника</em>. Полетник вам приказује ваше омиљене програме и оне који су већ покренути. Кликните на неку иконицу на полетнику да отворите тај програм; ако је програм већ покренут, имаће малу тачку испод своје иконице. Клик на иконицу ће издићи најскорије коришћени прозор. Можете такође да превучете иконицу на радни простор.</p>

  <p if:test="!platform:gnome-classic">Клик десним тастером миша на иконицу приказује изборник који вам омогућава да изаберете било који прозор у покренутом програму, или да отворите нови прозор. Можете такође да кликнете на иконицу док држите притиснутим тастер <key>Ктрл</key> да отворите нови прозор.</p>

  <p if:test="!platform:gnome-classic">Када сте у прегледу, на почетку ће вам увек бити приказан преглед прозора. У њему су вам приказане живе минијатуре свих прозора на текућем радном простору.</p>

  <p if:test="!platform:gnome-classic">Кликните на дугме мреже (које има девет тачака) у полетнику да прикажете претпреглед програма. Ово ће вам приказати све програме инсталиране на вашем рачунару. Кликните на неки од програма да га покренете, или превуците програм на радни простор приказан изнад инсталираног програма. Можете такође да превучете програм у полетнику да бисте га учинили омиљеним. Ваши омиљени програми остају у полетнику чак и када нису покренути, тако да им можете приступити брзо.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Сазнајте више о почетним програмима.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Сазнајте више о прозорима и радним просторима.</link></p>
    </item>
  </list>

</section>

<section id="clock">
  <title>Сат, календар и састанци</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Сат, календар, састанци и обавештења</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Сат, календар, и састанци</p>
    </media>
  </if:when>
</if:choose>

  <p>Кликните на сат на горњој траци да видите тренутни датум, календар појединачних месеци, списак ваших надолазећих састанака и нова обавештења. Можете да отворите календар и тако што ћете притиснути <keyseq><key>Супер</key><key>V</key></keyseq>. Можете да приступите подешавањима датума и времена и да отворите потпуни календар управо из изборника.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Сазнајте више о календару и састанцима.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Сазнајте више о обавештењима и списку обавештења.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Системски изборник</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Кориснички изборник</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Кориснички изборник</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the system menu in the top-right corner to manage your system
  settings and your computer. The top part of the menu shows the battery
  status indicator, and buttons to launch Settings and the screenshot tool. The
  <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
  power</media> button allows you to suspend or power off the computer, or
  quickly give somebody else access to the computer without logging out
  completely. Sliders allow you to control the sound volume or screen
  brightness.</p>

  <p>The rest of the menu consists of Quick Settings buttons which let you
  quickly control available services and devices like Wi-Fi, Bluetooth, power
  settings, and background apps.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Сазнајте више о промени корисника, одјављивању, и гашењу рачунара.</link></p>
    </item>
    <item>
      <p><link xref="quick-settings">Learn more about Quick
      Settings.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Закључавање екрана</title>

  <p>Када закључате екран, или ако се сам закључа, бива приказан екран закључавања. Поред тога што штити вашу радну површ када сте далеко од рачунара, екран закључавања приказује датум и време. Такође приказује информације о стању ваше батерије и мреже.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Сазнајте више о закључавању екрана.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Списак прозора</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>Гнома одликује другачији приступ пребацивања прозора него што је то стално видљив списак прозора који налазимо у другим окружењима радне површи. Ово вам допушта да се усредсредите на задатак на коме радите без ометања.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Сазнајте више о пребацивању прозора.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Списак прозора</p>
    </media>
    <p>Списак прозора на дну екрана обезбеђује приступ свим вашим отвореним прозорима и програмима и допушта вам да их брзо умањите и повратите.</p>
    <p>Са десне стране списка прозора, Гном приказује четири радна простора. да се пребаците на други радни простор, изаберите радни простор који желите да користите.</p>
  </if:when>
</if:choose>

</section>

</page>
