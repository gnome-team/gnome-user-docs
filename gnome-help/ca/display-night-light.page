<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="ca">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Llum nocturna canvia el color de les seves pantalles segons l'hora del dia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Ajustar la temperatura de color de la pantalla</title>

  <p>El monitor d'ordinador emet llum blava que contribueix a l'insomni i a la tensió ocular després de la foscor. <gui>Llum nocturna</gui> canvia el color de les pantalles segons l'hora del dia, fent que el color sigui més càlid a la nit. Per a habilitar <gui>Llum nocturna</gui>:</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Pantalles</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Pantalles</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Llum nocturna</gui> per a obrir la configuració.</p>
    </item>
    <item>
      <p>Assegureu-vos que l'interruptor de <gui>Llum nocturna</gui> està activat.</p>
    </item>
    <item>
      <p>Sota <gui>Planificació</gui>, seleccioneu <gui>Posta de sol a la sortida del sol</gui> per a fer que el color de la pantalla segueixi la posta de sol i l'hora de sortida del sol per a la vostra ubicació. Seleccioneu <gui>Planificació manual</gui> per a establir el <gui>Temps</gui> a una planificació personalitzada.</p>
    </item>
    <item>
      <p>Utilitzeu el control lliscant per a ajustar la <gui>temperatura del color</gui> a més o menys càlid.</p>
    </item>
  </steps>
      <note>
        <p>La <link xref="shell-introduction">barra superior</link> es mostra quan <gui>Llum nocturna</gui> està activat. Es pot desactivar temporalment des del menú del sistema.</p>
      </note>



</page>
