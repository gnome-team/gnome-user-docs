<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="ca">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mirar a <guiseq><gui>Paràmetres</gui><gui>Color</gui></guiseq> per a afegir un perfil de color per a la vostra pantalla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Com puc assignar perfils als dispositius?</title>

  <p>És possible que vulgueu assignar un perfil de color a la vostra pantalla o impressora perquè els colors que mostrin siguin més precisos.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Color</gui> en la barra lateral per a obrir el quadre.</p>
    </item>
    <item>
      <p>Seleccioneu el dispositiu per al qual vulgueu afegir un perfil.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Afegeix un perfil</gui> per a seleccionar un perfil existent o importar-ne un de nou.</p>
    </item>
    <item>
      <p>Premeu <gui>Afegeix</gui> per a confirmar la vostra selecció.</p>
    </item>
    <item>
      <p>Per a canviar el perfil usat seleccioneu el perfil que voleu utilitzar i premeu <gui>Habilita</gui> per a confirmar la selecció.</p>
    </item>
  </steps>

  <p>Cada dispositiu pot tenir diversos perfils assignats a ell, però només un perfil pot ser el perfil <em>per defecte</em>. El perfil predeterminat s'utilitza quan no hi ha informació addicional per a permetre que el perfil se seleccioni automàticament. Un exemple d'aquesta selecció automàtica seria si es creés un perfil per a paper brillant i un altre paper normal.</p>

  <p>Si el maquinari de calibratge està connectat, el botó <gui>Calibra…</gui> crearà un nou perfil.</p>

</page>
