<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-closelid" xml:lang="ca">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Els ordinadors portàtils s'adormen quan es tanca la tapa per a estalviar energia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Per què el meu ordinador s'apaga quan tanco la tapa?</title>

  <p>Quan tanqueu la tapa del portàtil, l'ordinador ho entrarà en <link xref="power-suspend"><em>suspensió</em></link> per a estalviar energia. Això significa que l'ordinador no està desactivat. Acaba d'adormir-se. Podeu recuperar-lo obrint la tapa. Si no es recupera, cliqueu al ratolí o premeu una tecla. Si això encara no és suficient, premeu el botó d'engegada.</p>

  <p>Alguns ordinadors no poden entrar en suspensió correctament, normalment perquè el seu maquinari no està totalment suportat pel seu sistema operatiu (per exemple, els controladors de Linux estan incomplets). En aquest cas, us podeu trobar que no podeu recuperar l'ordinador després d'haver tancat la tapa. Podeu intentar <link xref="power-suspendfail">solucionar el problema amb la suspensió</link>, o pot evitar que l'ordinador intenti suspendre's quan tanqui la tapa.</p>

<section id="nosuspend">
  <title>Fer que l'ordinador no entri en suspensió quan es tanqui la tapa</title>

  <note style="important">
    <p>Aquestes instruccions només funcionaran si esteu utilitzant <app>systemd</app>. Contacteu amb la vostra distribució per a obtenir més informació.</p>
  </note>

  <note style="important">
    <p>Heu de tenir instal·lat <app>Ajustaments</app> a l'ordinador per a canviar aquesta configuració.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Instal·la <app>Ajustaments</app></link></p>
    </if:if>
  </note>

  <p>Si no voleu que l'ordinador entri en suspensió quan es tanqui la tapa, podeu canviar la configuració.</p>

  <note style="warning">
    <p>Tingueu molta cura si canvieu aquesta configuració. Alguns ordinadors portàtils poden sobreescalfar-se si es deixen corrent amb la tapa tancada, especialment si estan en un lloc tancat com una motxilla.</p>
  </note>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Ajustaments</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Ajustaments</gui> per a obrir l'aplicació.</p>
    </item>
    <item>
      <p>Seleccioneu la pestanya <gui>General</gui>.</p>
    </item>
    <item>
      <p>Canvieu <gui>Suspendre quan es tanqui la tapa del portàtil</gui> a <gui>OFF</gui>.</p>
    </item>
    <item>
      <p>Tanqueu la finestra <gui>Ajustaments</gui>.</p>
    </item>
  </steps>

</section>

</page>
