<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="ca">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definir o canviar dreceres de teclat a la configuració de <gui>Teclat</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Estableix dreceres de teclat</title>

<p>Per a canviar la tecla o les tecles a prémer per a una drecera de teclat:</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Teclat</gui> a la barra lateral per a obrir el quadre.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>View and Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Seleccioneu la categoria desitjada, o introduïu un terme de cerca.</p>
    </item>
    <item>
      <p>Cliqueu a la fila per l'acció desitjada. Es mostrarà la finestra <gui>Configura drecera</gui>.</p>
    </item>
    <item>
      <p>Mantingueu premuda la combinació de tecles desitjada, o premeu <key>Retrocés</key> per a restablir, o premeu <key>Esc</key> per a cancel·lar.</p>
    </item>
  </steps>


<section id="defined">
<title>Dreceres predefinides</title>
  <p>Hi ha una sèrie d'accessos directes preconfigurats que es poden canviar, agrupats en aquestes categories:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accés universal</title>
  <tr>
	<td><p>Redueix la mida del text</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Activa o inhabilita el contrast alt</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Augmenta la mida del text</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Activa o desactiva el teclat virtual</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Activa o desactiva el lector de pantalla</p></td>
	<td><p><keyseq><key>Alt</key><key>Súper</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activa o desactiva l'ampliació</p></td>
	<td><p><keyseq><key>Alt</key><key>Súper</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Apropa</p></td>
  <td><p><keyseq><key>Alt</key><key>Súper</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Allunya</p></td>
  <td><p><keyseq><key>Alt</key><key>Súper</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Llançadors</title>
  <tr>
	<td><p>Carpeta de l'usuari</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> o <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> o <key>Explorar</key></p></td>
  </tr>
  <tr>
	<td><p>Llençar la calculadora</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> o <key>Calculadora</key></p></td>
  </tr>
  <tr>
	<td><p>Llençar el client de correu electrònic</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> o <key>Correu electrònic</key></p></td>
  </tr>
  <tr>
	<td><p>Llença el navegador d'ajuda</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Llençar el navegador web</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> o <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> o <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Cercar</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> o <key>Cercar</key></p></td>
  </tr>
  <tr>
	<td><p>Ajustos</p></td>
	<td><p><key>Eines</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navegació</title>
  <tr>
	<td><p>Amagueu totes les finestres normals</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Mou a l'espai de treball a l'esquerra</p></td>
	<td><p><keyseq><key>Súper</key><key>Re Pàg</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou a l'espai de treball de la dreta</p></td>
	<td><p><keyseq><key>Súper</key><key>Av Pàg</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra un monitor avall</p></td>
	<td><p><keyseq><key>Maj</key><key xref="keyboard-key-super">Súper</key>↓<key>Re Pàg</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra un monitor a l'esquerra</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra un monitor a la dreta</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra un monitor amunt</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra un espai de treball a l'esquerra</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key> <key>Re Pàg</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra un espai de treball a la dreta</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key><key>Av Pàg</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra a l'últim espai de treball</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key><key>Fi</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra a l'espai de treball 1</p></td>
	<td><p><keyseq><key>Maj</key><key>Súper</key><key>Inici</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra a l'espai de treball 2</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra a l'espai de treball 3</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra a l'espai de treball 4</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Canvieu les aplicacions</p></td>
	<td><p><keyseq><key>Súper</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Canvieu els controls del sistema</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Commuteu els controls del sistema directament</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Canvia a l'últim espai de treball</p></td>
	<td><p><keyseq><key>Súper</key><key>Fi</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Canvieu a l'espai de treball 1</p></td>
	<td><p><keyseq><key>Súper</key><key>Inici</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Canvieu a l'espai de treball 2</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Canvieu a l'espai de treball 3</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Canvieu a l'espai de treball 4</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
        <td><p>Canvieu les finestres</p></td>
        <td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Canvieu les finestres directament</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Canvieu les finestres d'una aplicació directament</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Canvieu les finestres d'una aplicació</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Captures de pantalla</title>
  <tr>
	<td><p>Desar una captura de pantalla d'una finestra</p></td>
	<td><p><keyseq><key>Alt</key><key>Imprimeix</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Desar una captura de pantalla de tota la pantalla</p></td>
	<td><p><keyseq><key>Maj</key><key>Imprimeix</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Inicia l'eina de captura de pantalla</p></td>
	<td><p><key>Imprimeix</key></p></td>
  </tr>
  <tr>
        <td><p>Grava un enregistrament de pantalla curt</p></td>
        <td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>So i multimèdia</title>
  <tr>
	<td><p>Expulsa</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Expulsar)</p></td>
  </tr>
  <tr>
	<td><p>Inicia el reproductor multimèdia</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Mitjans d'àudio)</p></td>
  </tr>
  <tr>
	<td><p>Silencia o deixa de silenciar el micròfon</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>Pista següent</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Àudio següent)</p></td>
  </tr>
  <tr>
	<td><p>Fes una pausa a la reproducció</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Posa en pausa l'àudio)</p></td>
  </tr>
  <tr>
	<td><p>Reprodueix (o reprodueix/pausa)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Reproduir l'àudio)</p></td>
  </tr>
  <tr>
	<td><p>Pista anterior</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Àudio anterior)</p></td>
  </tr>
  <tr>
	<td><p>Atura la reproducció</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Aturar l'àudio)</p></td>
  </tr>
  <tr>
	<td><p>Abaixa el volum</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Abaixar el volum d'àudio)</p></td>
  </tr>
  <tr>
	<td><p>Volum silenciat</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Silenciar àudio)</p></td>
  </tr>
  <tr>
	<td><p>Apuja el volum</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Apujar el volum d'àudio)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sistema</title>
  <tr>
        <td><p>Centrar el focus la notificació activa</p></td>
        <td><p><keyseq><key>Súper</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Bloqueja la pantalla</p></td>
	<td><p><keyseq><key>Súper</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostra el diàleg d'apagada</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Supr</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Estableix dreceres de teclat</p></td>
        <td><p><keyseq><key>Súper</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Mostrar totes les aplicacions</p></td>
        <td><p><keyseq><key>Súper</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostra la vista general d'activitats</p></td>
	<td><p><key>Super</key></p></td>
  </tr>
  <tr>
	<td><p>Mostra la llista de notificacions</p></td>
	<td><p><keyseq><key>Súper</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the system menu</p></td>
	<td><p><keyseq><key>Súper</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar el símbol de sistema</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Escrivint</title>
  <tr>
  <td><p>Commuta a la font d'entrada següent</p></td>
  <td><p><keyseq><key>Súper</key><key>Espai</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Commuta a la font d'entrada anterior</p></td>
  <td><p><keyseq><key>Maj</key><key>Súper</key><key>Espai</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Finestres</title>
  <tr>
	<td><p>Activa el menú de la finestra</p></td>
	<td><p><keyseq><key>Alt</key><key>Espai</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Tanca la finestra</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Amaga la finestra</p></td>
        <td><p><keyseq><key>Súper</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Baixar la finestra darrera les altres</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Maximitza la finestra</p></td>
	<td><p><keyseq><key>Súper</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximitzar la finestra horitzontalment</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Maximitzar verticalment la finestra</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Mou la finestra</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Portar la finestra davant de les altres</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Pujar la finestra si està coberta, si no, baixar-la</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Redimensiona la finestra</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restaura la finestra</p></td>
        <td><p><keyseq><key>Súper</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Commuta el mode a pantalla completa</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
	<td><p>Commuta l'estat de maximització</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Intercanvia la finestra en tots els espais de treball o en un</p></td>
	<td><p>Inhabilitat</p></td>
  </tr>
  <tr>
        <td><p>Dividir la vista a l'esquerra</p></td>
        <td><p><keyseq><key>Súper</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Dividir la vista a la dreta</p></td>
        <td><p><keyseq><key>Súper</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Personalitza les dreceres</title>

  <p>Creeu la vostra pròpia drecera de teclat de l'aplicació a la configuració del <gui>Teclat</gui>:</p>

  <steps>
    <item>
      <p>Seleccioneu <gui>Dreceres personalitzades</gui>.</p>
    </item>
    <item>
      <p>Cliqueu al botó <gui style="button">Afegeix una drecera</gui> si encara no s'ha establert cap drecera personalitzada. Altrament, cliqueu al botó <gui style="button">+</gui>. Apareixerà la finestra <gui>Afegeix una drecera personalitzada</gui>.</p>
    </item>
    <item>
      <p>Escriviu un <gui>Nom</gui> per a identificar la drecera, i una <gui>Ordre</gui> per a executar una aplicació. Per exemple, si voleu la drecera per a obrir <app>Rhythmbox</app>, la podeu anomenar <input>Música</input> i utilitzar l'ordre <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Cliqueu al botó <gui style="button">Afegeix una drecera…</gui>. A la finestra <gui>Afegeix una drecera personalitzada</gui>, manteniu premuda la combinació de tecles de drecera desitjada.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Afegeix</gui>.</p>
    </item>
  </steps>

  <p>El nom de l'ordre que escriviu hauria de ser una ordre de sistema vàlida. Podeu comprovar que l'ordre funciona obrint un terminal i escrivint-la allí. L'ordre que obre una aplicació no pot tenir el mateix nom que la mateixa aplicació.</p>

  <p>Si voleu canviar l'ordre associada amb una drecera de teclat personalitzada, cliqueu a la fila de la drecera. Apareixerà la finestra <gui>Estableix una drecera personalitzada</gui> i podreu editar l'ordre.</p>

</section>

</page>
