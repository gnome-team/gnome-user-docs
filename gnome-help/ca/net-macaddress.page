<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="ca">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>L'identificador únic assignat al maquinari de la xarxa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Què és una adreça MAC?</title>

  <p>Una <em>adreça MAC</em> és l'identificador únic assignat pel fabricant a una peça de maquinari de xarxa (com una targeta sense fil o una targeta ethernet). MAC significa <em>Media Access Control</em>, i cada identificador està pensat per a ser únic per a un dispositiu en particular.</p>

  <p>Una adreça MAC consta de sis conjunts de dos caràcters, cadascun separats per dos punts. <code>00:1B:44:11:3A:B7</code> és un exemple d'adreça MAC.</p>

  <p>Per a identificar l'adreça MAC del vostre propi maquinari de xarxa:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui> for wired connections or <gui>Wi-Fi</gui> for
      wireless connections.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> or <gui>Wi-Fi</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection.</p>
    </item>
    <item>
      <p>The MAC address for the device will be displayed as the
      <gui>Hardware Address</gui> in the <gui>Details</gui> panel.</p>
    </item>
  </steps>

  <p>A la pràctica, és possible que hàgiu de modificar o «falsejar» una adreça MAC. Per exemple, alguns proveïdors de serveis d'Internet poden requerir que s'utilitzi una adreça MAC específica per a accedir al seu servei. Si la targeta de xarxa deixa de funcionar, i l'heu de canviar per una targeta nova, el servei ja no funcionarà. En aquests casos, hauríeu de falsejar l'adreça MAC.</p>

</page>
