<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="ca">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Afegir usuaris nous perquè altres persones puguin iniciar sessió a l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Afegir un nou compte d'usuari</title>

  <p>Podeu afegir diversos comptes d'usuari a l'ordinador. Doneu un compte a cada persona de la vostra llar o empresa. Cada usuari té la seva pròpia carpeta d'inici, documents i configuració.</p>

  <p>Heu de tenir <link xref="user-admin-explain">privilegis d'administrador</link> per a afegir comptes d'usuari.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Desbloqueja</gui> a l'extrem superior dret i escriviu la vostra contrasenya quan se us demani.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add User...</gui> button under
      <gui>Other Users</gui> to add a new user account.</p>
    </item>
    <item>
      <p>Si voleu que el nou usuari tingui <link xref="user-admin-explain">accés d'administrador</link> a l'ordinador, seleccioneu <gui>Administrador</gui> al tipus de compte.</p>
      <p>Els administradors poden fer coses com afegir o eliminar usuaris, instal·lar programari i controladors, i canviar la data i l'hora.</p>
    </item>
    <item>
      <p>Introduïu el nom complet del nou usuari. El nom d'usuari s'omplirà automàticament en funció del nom complet. Si no us agrada el nom d'usuari proposat, podeu canviar-lo.</p>
    </item>
    <item>
      <p>Podeu optar per establir una contrasenya per a l'usuari nou o deixar que la configurin ells mateixos en el seu primer inici de sessió. Si decidiu establir la contrasenya ara, podeu prémer la icona<gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generar contrasenya</span></media></gui> per a generar automàticament una contrasenya aleatòria.</p>
      <p>Per a connectar l'usuari a un domini de xarxa, cliqueu a <gui>Inici de sessió empresarial</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Afegeix</gui>. Quan s'ha afegit l'usuari, es poden ajustar els <gui>Controls parentals</gui> i l'<gui>Idioma</gui> .</p>
    </item>
  </steps>

  <p>Si voleu canviar la contrasenya després de crear el compte, seleccioneu el compte, <gui style="button">Desbloquegeu</gui> el quadre i premeu l'estat actual de la contrasenya.</p>

  <note>
    <p>Al quadre <gui>Usuaris</gui>, podeu clicar a la imatge a l'esquerra del nom de l'usuari per a configurar una imatge per al compte. Aquesta imatge es mostrarà a la finestra d'inici de sessió. Podeu utilitzar alguna de les imatges d'arxiu proporcionades pel sistema, seleccionar-ne una vostra o fer una fotografia amb la càmera web.</p>
  </note>

</page>
