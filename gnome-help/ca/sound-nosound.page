<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="ca">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comproveu que el so no estigui silenciat, que els cables estiguin connectats correctament, i que s'ha detectat la targeta de so.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>No sento cap so a l'ordinador</title>

  <p>Si no sentiu cap so a l'ordinador, per exemple, quan intenteu reproduir música, seguiu els consells de resolució de problemes següents.</p>

<section id="mute">
  <title>Assegureu-vos que el so no està silenciat</title>

  <p>Obriu el <gui xref="shell-introduction#systemmenu">menú del sistema</gui> i assegureu-vos que el so no està silenciat ni apagat.</p>

  <p>Alguns portàtils tenen interruptors o tecles de silenci — proveu de prémer aquesta tecla per a veure si canvia a so.</p>

  <p>També heu de comprovar que no heu silenciat l'aplicació que esteu utilitzant per a reproduir el so (per exemple, el reproductor de música o el reproductor de pel·lícules). L'aplicació pot tenir un botó de silenci o volum a la finestra principal, així que comproveu-ho.</p>

  <p>També podeu comprovar el control lliscant del volum al quadre <gui>So</gui>:</p>
  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>So</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>So</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Sota <gui>Nivells de volum</gui>, comproveu que la vostra aplicació no estigui silenciada. El botó al final del control lliscant de volum commuta <gui>Silencia</gui> a activat i desactivat.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>Comprovar que els altaveus estiguin activats i connectats correctament</title>
  <p>Si l'equip disposa d'altaveus externs, assegureu-vos que estiguin activats i que tinguin volum. Assegureu-vos que el cable de l'altaveu estigui connectat de manera segura a la presa d'àudio «sortida» de l'ordinador. Aquest sòcol sol ser de color verd clar.</p>

  <p>Algunes targetes de so poden canviar entre el sòcol que utilitzen per a la sortida (als altaveus) i el sòcol per a l'entrada (des d'un micròfon, per exemple). El sòcol de sortida pot ser diferent quan s'executa Linux, Windows o Mac OS. Proveu de connectar el cable de l'altaveu a una presa d'àudio diferent de l'ordinador.</p>

 <p>L'última cosa que cal comprovar és que el cable d'àudio està connectat de manera segura a la part posterior dels altaveus. Alguns altaveus també tenen més d'una entrada.</p>
</section>

<section id="device">
  <title>Comprovar que estigui seleccionat el dispositiu de so correcte</title>

  <p>Alguns equips tenen diversos "dispositius de so" instal·lats. Alguns d'aquests són capaços de generar so i d'altres no, així que heu de comprovar que seleccioneu el dispositiu de so correcte. Això pot comportar fer diverses proves-error per a triar el dispositiu correcte.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>So</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>So</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A <gui>Sortida</gui>, seleccioneu un <gui>dispositiu de sortida</gui> i cliqueu al botó <gui>Prova</gui> per a veure si funciona.</p>

      <p>És possible que hàgiu de provar cada dispositiu disponible.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>Comprovar que la targeta de so s'hagi detectat correctament</title>

  <p>Pot ser que la vostra targeta de so no s'hagi detectat correctament, probablement perquè els controladors de la targeta no estan instal·lats. És possible que necessiteu instal·lar els controladors de la targeta manualment. Com es fa això depèn del tipus de targeta.</p>

  <p>Executeu l'ordre <cmd>lspci</cmd> al Terminal per a esbrinar quina targeta de so teniu:</p>
  <steps>
    <item>
      <p>Aneu a la vista general d'<gui>Activitats</gui> i obriu un Terminal.</p>
    </item>
    <item>
      <p>Executeu <cmd>lspci</cmd> amb <link xref="user-admin-explain">privilegis administratius</link>; o bé escriviu <cmd>sudo lspci</cmd> i escriviu la vostra contrasenya, o bé teclegeu <cmd>su</cmd>, i introduïu la contrasenya <em>root</em> (administrativa) i després escriviu <cmd>lspci</cmd>.</p>
    </item>
    <item>
      <p>Comproveu que el <em>controlador d'àudio</em> o el <em>dispositiu d'àudio</em> apareix a la llista: en aquest cas, hauríeu de mirar la marca i el número de model de la targeta de so. L'ordre <cmd>lspci -v</cmd> mostra una llista amb informació més detallada.</p>
    </item>
  </steps>

  <p>És possible que pugueu trobar i instal·lar controladors per a la vostra targeta. El millor és demanar ajuda als fòrums de suport (o d'una altra manera) per a obtenir instruccions per a la vostra distribució de Linux.</p>

  <p>Si no podeu obtenir controladors per a la vostra targeta de so, és possible que preferiu comprar una targeta de so nova. Podeu obtenir targetes de so que es puguin instal·lar a l'interior de l'ordinador i targetes de so USB externes.</p>

</section>

</page>
