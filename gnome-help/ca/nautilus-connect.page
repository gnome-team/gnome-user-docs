<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-connect" xml:lang="ca">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>

    <revision pkgversion="3.6.0" date="2012-10-06" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permet visualitzar i editar fitxers en un altre ordinador a través d'FTP, SSH, recursos compartits de Windows o WebDAV.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Navegar per fitxers en un servidor o en una xarxa compartida</title>

<p>Podeu connectar-vos a un servidor o compartir xarxa per a explorar i visualitzar fitxers en aquest servidor, exactament com si estiguessin al vostre propi ordinador. Aquesta és una manera pràctica de descarregar o carregar fitxers a Internet o compartir fitxers amb altres persones a la vostra xarxa local.</p>

<p>Per a explorar els fitxers a través de la xarxa, obriu l'aplicació <app>Fitxers</app> des d'<gui>Activitats</gui>, i cliqueu a <gui>Altres ubicacions</gui> a la barra lateral. El gestor de fitxers trobarà qualsevol ordinador a la vostra xarxa local que publiqui la seva capacitat de servir fitxers. Si voleu connectar-vos a un servidor d'Internet o si no veieu l'equip que esteu cercant, podeu connectar-vos-hi manualment escrivint la seva adreça d'internet / xarxa.</p>

<steps>
  <title>Connecta a un servidor de fitxers</title>
  <item><p>Al gestor de fitxers, cliqueu a <gui>Altres ubicacions</gui> a la barra lateral.</p>
  </item>
  <item><p>A <gui>Connecta a un Servidor</gui>, introduïu l'adreça del servidor, en forma d'<link xref="#urls">URL</link>. Els detalls dels URL admesos s'<link xref="#types">enumeren a continuació</link>.</p>
  <note>
    <p>Si us heu connectat al servidor prèviament, podeu seleccionar-lo a la llista de <gui>Servidors recents</gui>.</p>
  </note>
  </item>
  <item>
    <p>Cliqueu a <gui>Connecta</gui>. Es mostraran els fitxers del servidor. Podeu navegar pels fitxers com si estiguéssiu al propi ordinador. El servidor s'afegirà a la barra lateral perquè pugui accedir-hi ràpidament en el futur.</p>
  </item>
</steps>

<section id="urls">
 <title>Escriure les URLs</title>

<p>Un <em>URL</em>, o <em>Uniform Resource Locator</em>, és una forma d'adreça que fa referència a una ubicació o fitxer en una xarxa. L'adreça està formada com a:</p>
  <example>
    <p><sys>scheme://servername.example.com/folder</sys></p>
  </example>
<p>L'<em>esquema (protocol)</em> especifica el protocol o tipus de servidor. La porció d'adreça <em>example.com</em> s'anomena <em>nom del domini</em>. Si es requereix un nom d'usuari, s'insereix abans del nom del servidor:</p>
  <example>
    <p><sys>scheme://username@servername.example.com/folder</sys></p>
  </example>
<p>Alguns esquemes (protocols) requereixen que s'especifiqui el número de port. Inseriu-lo després del nom del domini:</p>
  <example>
    <p><sys>scheme://servername.example.com:port/folder</sys></p>
  </example>
<p>A continuació es mostren exemples específics per als diferents tipus de servidor admesos.</p>
</section>

<section id="types">
 <title>Tipus de servidor</title>

<p>Podeu connectar-vos a diferents tipus de servidors. Alguns servidors són públics i permeten que tothom connecti. Altres servidors requereixen que inicieu sessió amb un nom d'usuari i contrasenya.</p>
<p>Podeu no tenir permisos per a realitzar certes accions sobre fitxers en un servidor. Per exemple, en llocs FTP públics, probablement no podreu esborrar fitxers.</p>
<p>L'URL que introduïu depèn del protocol que el servidor utilitzi per a exportar les seves accions sobre fitxers.</p>
<terms>
<item>
  <title>SSH</title>
  <p>Si teniu un compte <em>SSH</em> a un servidor, podeu connectar-vos mitjançant aquest mètode. Molts proveïdors d'allotjament web proporcionen comptes SSH als membres perquè pugin fitxers de manera segura. Els servidors SSH sempre requereixen un inici de sessió.</p>
  <p>Un URL SSH normalment es mostra com:</p>
  <example>
    <p><sys>ssh://username@servername.example.com/folder</sys></p>
  </example>

  <p>Quan utilitzeu SSH, totes les dades que envieu (inclosa la vostra contrasenya) es xifren perquè els altres usuaris de la vostra xarxa no puguin veure-la.</p>
</item>
<item>
  <title>FTP (amb usuari)</title>
  <p>FTP és una popular forma d'intercanviar fitxers a Internet. Com que les dades no es xifren sobre FTP, molts servidors proporcionen accés a través de SSH. Alguns servidors, tanmateix, encara permeten o requereixen que utilitzeu FTP per a carregar o descarregar fitxers. Els llocs FTP amb inici de sessió normalment permeten eliminar i carregar fitxers.</p>
  <p>Un URL FTP es mostra com:</p>
  <example>
    <p><sys>ftp://username@ftp.example.com/path/</sys></p>
  </example>
</item>
<item>
  <title>FTP públic</title>
  <p>Els llocs que permeten descarregar fitxers a vegades ofereixen accés FTP públic o anònim. Aquests servidors no requereixen un nom d'usuari ni una contrasenya, i normalment no permeten que esborreu o carregueu fitxers.</p>
  <p>Un URL d'un FTP anònim es mostra com:</p>
  <example>
    <p><sys>ftp://ftp.example.com/path/</sys></p>
  </example>
  <p>Alguns llocs FTP anònims requereixen que inicieu sessió amb un nom d'usuari públic i contrasenya, o amb un nom d'usuari públic utilitzant la vostra adreça de correu electrònic com a contrasenya. Per a aquests servidors, utilitzeu el mètode <gui>FTP (amb autentificació)</gui>, i utilitzeu les credencials especificades pel lloc FTP.</p>
</item>
<item>
  <title>Recursos compartits de Windows</title>
  <p>Els ordinadors Windows utilitzen un protocol propietari per a compartir fitxers a través d'una xarxa d'àrea local. En algunes ocasions, els ordinadors d'una xarxa de Windows es troben agrupades en un <em>domini</em> per a l'organització i per a controlar millor l'accés. Si teniu els permisos adequats a l'equip remot, podeu connectar-vos a un recurs de Windows des del gestor de fitxers.</p>
  <p>Un URL típic de recursos compartits de Windows es mostra així:</p>
  <example>
    <p><sys>smb://servername/Share</sys></p>
  </example>
</item>
<item>
  <title>WebDAV i WebDAV Segur</title>
  <p>Basat en el protocol HTTP utilitzat a la web, de vegades WebDAV s'utilitza per a compartir fitxers en una xarxa local i per a emmagatzemar fitxers a Internet. Si el servidor al qual us esteu connectant admet connexions segures, heu de triar aquesta opció. WebDAV segur, utilitza xifrat SSL fort, de manera que els altres usuaris no poden veure la vostra contrasenya.</p>
  <p>Un URL WebDAV es mostra com:</p>
  <example>
    <p><sys>dav://example.hostname.com/path</sys></p>
  </example>
</item>
<item>
  <title>Compartir NFS</title>
  <p>Els ordinadors UNIX tradicionalment utilitzen el protocol del sistema de fitxers de xarxa (NFS) per a compartir fitxers a través d'una xarxa local. Amb NFS, la seguretat es basa en el UID de l'usuari que accedeix al recurs compartit, de manera que no es necessiten credencials d'autenticació quan es connecta.</p>
  <p>Un URL típic de NFS per a compartir és mostra com:</p>
  <example>
    <p><sys>nfs://servername/path</sys></p>
  </example>
</item>
</terms>
</section>

</page>
