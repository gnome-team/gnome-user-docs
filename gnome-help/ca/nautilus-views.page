<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="nautilus-views" xml:lang="ca">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-views"/>

    <revision pkgversion="3.8" version="0.2" date="2013-04-03" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision version="gnome:45" date="2024-03-04" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Specify the default sort order for files and folders.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Preferències de visualització a <app>Fitxers</app></title>

<p>You can change how files and folders are grouped and sorted by default.
Press the menu button in the sidebar of the window and select
<gui style="menuitem">Preferences</gui>.</p>

<!-- TODO FIXME: Merge / sort out with "nautilus-display.page" overlap for "Expandable Folders in List View" and "Grid view options" -->
<section id="default-view">
<title>General</title>
<terms>
  <item>
    <title><gui>Sort Folders Before Files</gui></title>
    <p>By default, the file manager does not show all folders before files.
    To see all folders listed before files, enable this option.</p>
  </item>
</terms>

<note style="tip">
  <p>To change how files are sorted in folders, see <link xref="files-sort"/>.</p>
</note>

</section>

</page>
