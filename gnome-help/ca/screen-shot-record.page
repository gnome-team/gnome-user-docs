<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0 ui/1.0" id="screen-shot-record" xml:lang="ca">
      
  <info>
    <link type="guide" xref="tips"/>

    <revision version="gnome:44" status="final" date="2023-12-30"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Feu una foto o enregistreu un vídeo de què està passant a la vostra pantalla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Captures i enregistraments de pantalla</title>

  <list>
    <item><p>Captura de tota o part de la pantalla com a imatge</p></item>
    <item><p>Envieu-lo com a fitxer o enganxeu-lo des del porta-retalls</p></item>
    <item><p>Desar un vídeo de l'activitat de la pantalla</p></item>
  </list>

  <media type="image" src="figures/screenshot-tool.png" width="500"/>

  <p>Podeu fer una imatge de la vostra pantalla (una <em>captura de pantalla</em>) o gravar un vídeo de què està passant a la pantalla (un <em>enregistrament de pantalla</em>). Això és útil si, per exemple, voleu mostrar a algú com fer alguna cosa a l'ordinador. Les captures de pantalla i els enregistraments són fitxers d'imatge i de vídeo normals, de manera que podeu enviar-les per correu electrònic i compartir-les a la web.</p>

<section id="screenshot">
  <title>Fer una captura de pantalla</title>

  <steps>
    <item>
      <p>Press the <key>Print</key> key or press the screenshot button in the
      <gui xref="shell-introduction#systemmenu">system menu</gui>.</p>
    </item>
    <item>
      <p>La màscara de captura de pantalla us presenta nanses per a seleccionar l'àrea a capturar i <media its:translate="no" type="image" src="figures/camera-photo-symbolic.svg"/> indica el mode de captura de pantalla (imatge fixa).</p>
       <note>
         <p>Cliqueu al botó del punter per a incloure el punter a la captura de pantalla.</p>
       </note>
    </item>
    <item>
      <p>Cliqueu i arrossegueu l'àrea que vulgueu per a la captura de pantalla mitjançant les nanses o el punt de mira.</p>
    </item>
    <item>
       <p>Per a capturar l'àrea seleccionada, cliqueu al botó gran.</p>
    </item>
    <item>
       <p>Per a capturar tota la pantalla, cliqueu a <gui>Pantalla</gui> i, a continuació, cliqueu al botó rodó gran.</p>
    </item>
    <item>
       <p>Per a capturar una finestra, cliqueu a <gui>Finestra</gui>. Es mostra una visió general de totes les finestres obertes amb la finestra activa marcada. Cliqueu per a triar una finestra i, a continuació, cliqueu al botó rodó gran.</p>
    </item>
  </steps>

  </section>
  
  <section id="locations">
  <title>On van?</title>

  <list>
    <item>
      <p>Les imatges de captura de pantalla es desen automàticament a la carpeta <file>Imatges/Captures de pantalla</file> de la carpeta d'inici amb un nom de fitxer que comença amb <file>Captura de pantalla</file> i inclou la data i l'hora en què es va fer.</p>
    </item>
    <item>
      <p>La imatge també es desa al porta-retalls, de manera que podeu enganxar-la immediatament en una aplicació d'edició d'imatges o compartir-la a les xarxes socials.</p>
    </item>
    <item>
      <p>Els enregistraments de pantalla es desen automàticament a la carpeta <file>Vídeos/enregistrament de pantalla</file> de la carpeta d'inici, amb un nom de fitxer que comença amb <file>enregistrament de pantalla</file> i inclou la data i l'hora en què s'ha pres.</p>
    </item>
  </list>
    
  </section>

<section id="screencast">
  <title>Fer un enregistrament de pantalla</title>

  <p>Podeu fer un enregistrament de vídeo de què està passant a la pantalla:</p>

  <steps>
    <item>
      <p>Press the <key>Print</key> key or press the screenshot button in the
      <gui xref="shell-introduction#systemmenu">system menu</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <media its:translate="no" type="image" src="figures/camera-video-symbolic.svg"/> per a canviar al mode enregistrament de pantalla.</p>
       <note>
         <p>Cliqueu al botó del punter per a incloure el punter a l'enregistrament de pantalla.</p>
       </note>
    </item>
    <item>
      <p>Trieu <gui>Selecciona</gui> o <gui>Pantalla</gui>. Per a <gui>Selecciona</gui>, cliqueu i arrossegueu l'àrea que vulgueu per a l'enregistrament de pantalla mitjançant les nanses o el punt de mira.</p>
    </item>
    <item>
      <p>Cliqueu al botó vermell rodó gran per a començar a gravar el que hi ha a la pantalla.</p>
      <p>Es mostra un indicador vermell a l'extrem superior dret de la pantalla quan la gravació està en curs, mostrant els segons transcorreguts.</p>
    </item>
    <item>
      <p>Once you have finished, click the red indicator
      <media its:translate="no" type="image" src="figures/topbar-media-record.svg"/>
      in the top bar.</p>
<!-- Commented until https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/5386 is fixed -->
<!--      <p>Once you have finished, click the red indicator or press
      <keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq>
      to stop the recording.</p> -->
    </item>
  </steps>

</section>
  
  <section id="keyboard-shortcuts">
    <title>Dreceres de teclat</title>

    <p>Dins de la funció de captura de pantalla, podeu utilitzar aquestes dreceres de teclat:</p>

<table rules="rows" frame="top bottom">

  <tr>
    <td><p><key>S</key></p></td>
    <td><p>Seleccioneu l'àrea</p></td>
  </tr>
  <tr>
    <td><p><key>C</key></p></td>
    <td><p>Captura de pantalla</p></td>
  </tr>
  <tr>
    <td><p><key>W</key></p></td>
    <td><p>Finestra de captura</p></td>
  </tr>
  <tr>
    <td><p><key>P</key></p></td>
    <td><p>Canvia entre el punter vist o ocult</p></td>
  </tr>
  <tr>
    <td><p><key>V</key></p></td>
    <td><p>Canvia entre captura de pantalla i enregistrament de pantalla</p></td>
  </tr>
  <tr>
    <td><p><key>Retorn</key></p></td>
    <td><p>Captura, també activada per <key>Espai</key> o <keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
  </tr>
</table>

    <p>Aquestes dreceres es poden utilitzar per a ometre la funció de captura de pantalla:</p>

<table rules="rows" frame="top bottom">
  <tr>
    <td><p><keyseq><key>Alt</key><key>Imprimeix</key></keyseq></p></td>
    <td><p>Captura la finestra que actualment té el focus</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>Imprimeix</key></keyseq></p></td>
    <td><p>Captura tota la pantalla</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
<!-- Commented until https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/5386 is fixed -->
<!--    <td><p>Start and stop recording a screencast</p></td> -->
    <td><p>Start recording a screencast</p></td>
  </tr>
</table>

  </section>

</page>
