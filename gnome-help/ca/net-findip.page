<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="ca">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conèixer la vostra adreça IP us pot ajudar a resoldre problemes de xarxa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Trobar la vostra adreça IP</title>

  <p>Conèixer la vostra adreça IP us pot ajudar a resoldre problemes amb la vostra connexió a Internet. És possible que us sorprengueu de saber que teniu <em>dues</em> adreces IP: una adreça IP per a l'ordinador a la xarxa interna i una adreça IP per a l'ordinador a Internet.</p>
  
  <section id="wired">
    <title>Cercar l'adreça IP interna (xarxa) de la connexió per cable</title>
  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Xarxa</gui> a la barra lateral per a obrir el quadre.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">Si hi ha més d'un tipus de connexió per cable, és possible que s'hi mostrin noms com ara <gui>PCI Ethernet</gui> o <gui>USB Ethernet</gui> en comptes de <gui>Cablejat</gui>.</p>
      </note>
    </item>
    <item>
      <p>Cliqueu al botó <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">configuració</span></media> situat al costat de la connexió activa de l'adreça IP i altres detalls.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Cercar l'adreça IP interna (xarxa) de la connexió sense fil</title>
  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Paràmetres</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Wi-Fi</gui> a la barra lateral per a obrir el quadre.</p>
    </item>
    <item>
      <p>Cliqueu al botó <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">configuració</span></media> situat al costat de la connexió activa de l'adreça IP i altres detalls.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Cerqueu la vostra adreça IP externa (Internet)</title>
  <steps>
    <item>
      <p>Visiteu <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>El lloc mostrarà l'adreça IP externa.</p>
    </item>
  </steps>
  <p>En funció de com es connecti l'ordinador a Internet, les adreces internes i externes poden ser les mateixes.</p>  
  </section>

</page>
