<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="ca">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Feu que el teclat no repeteixi les lletres quan mantingueu premuda una tecla, o si canvieu el retard i la velocitat per a repetir tecles.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Gestionar les pulsacions de tecla repetides</title>

  <p>Per defecte, quan mantingueu premuda una tecla, es repetirà la lletra o el símbol fins que deixeu anar la tecla. Si teniu dificultats per a retirar el vostre dit prou de pressa podeu inhabilitar aquesta funció, canviar el temps que triga abans que les pulsacions de les tecles comencin a repetir-se, o la rapidesa a la qual es repeteixen les tecles premudes.</p>

  <steps>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Accés universal</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Accés universal</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the <gui>Repeat Keys</gui> switch to off.</p>
      <p>Alternativament, ajusteu el botó lliscant <gui>Retard</gui> per a controlar el temps haureu de prémer una tecla per a començar a repetir-la, i ajustar el control lliscant <gui>Velocitat</gui> per a controlar la rapidesa amb la qual es repeteix una tecla premuda.</p>
    </item>
  </steps>

</page>
