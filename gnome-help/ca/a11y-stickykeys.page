<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="ca">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Com escriure les dreceres de teclat prement una única tecla cada vegada en comptes d'haver-les de mantenir totes premudes alhora.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Activació de les tecles enganxoses</title>

  <p>Les <em>tecles enganxoses</em> us permeten escriure les dreceres de teclat prement les tecles d'una a una en comptes d'haver-les de prémer totes alhora. Per exemple, la drecera de teclat <keyseq><key xref="keyboard-key-super">Súper</key><key>Tab</key></keyseq> canvia entre finestres. Sense les tecles enganxoses activades, hauríeu de mantenir premudes les dues tecles alhora. En canvi, amb les tecles enganxoses activades, primer hauríeu de prémer <key>Súper</key> i, després, <key>Tab</key> per a aconseguir el mateix efecte.</p>

  <p>Activeu les tecles enganxoses si teniu problemes a l'hora de prémer més d'una tecla alhora.</p>

  <steps>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Accés universal</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Accés universal</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the
      <gui>Sticky Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Canvieu ràpidament les tecles enganxoses a ON i OFF</title>
    <p>Modifiqueu l'opció <gui>Habilita per teclat</gui> per a activar i desactivar les tecles enganxoses des del teclat. Quan aquesta opció està activada, podeu prémer la tecla <key>Maj</key> cinc cops seguits per a activar o desactivar les tecles enganxoses.</p>
    <p>També podeu activar i desactivar les tecles enganxoses clicant a la <link xref="a11y-icon">icona d'accessibilitat</link> de la barra superior i seleccionant <gui>Tecles enganxoses</gui>. La icona d'accessibilitat és visible quan s'ha activat una o més opcions des del quadre <gui>Accés universal</gui>.</p>
  </note>

  <p>Si premeu dues tecles alhora, podeu fer que les tecles enganxoses es desactivin automàticament, de forma temporal, per tal que pugueu introduir una drecera de teclat de la forma «estàndard».</p>

  <p>Per exemple, si tot i tenir activades les tecles enganxoses premeu <key>Súper</key> i <key>Tab</key> simultàniament, les tecles enganxoses no esperaran què premeu una altra tecla. Només l'<em>esperarien</em> si haguéssiu premut una única tecla. Això és útil si sou capaços de prémer algunes dreceres de teclat alhora (aquelles en què, per exemple, les tecles estan juntes), i d'altres no.</p>

  <p>Seleccioneu <gui>Inhabilita si es premen dues tecles alhora</gui> per a activar-ho.</p>

  <p>Podeu fer que l'ordinador faci un so «beep» quan comenceu a escriure una drecera de teclat amb les tecles enganxoses activades. Això és útil si voleu saber que les tecles enganxoses esperen que es teclegi un accés directe pel teclat, de manera que la següent tecla premuda s'interpretarà com a part d'una drecera. Seleccioneu <gui>Beep quan es pressiona una tecla modificadora</gui> per a activar-ho.</p>

</page>
