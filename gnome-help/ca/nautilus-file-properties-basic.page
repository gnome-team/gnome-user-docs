<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="ca">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:46" date="2024-03-05" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Veure la informació bàsica del fitxer, establir permisos i triar aplicacions predeterminades.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Propietats del fitxer</title>

  <p>Per a veure informació sobre un fitxer o carpeta, feu-hi clic amb el botó dret i seleccioneu <gui>Propietats</gui>. També podeu seleccionar el fitxer i prémer <keyseq><key>Alt</key><key>Retorn</key></keyseq>.</p>

  <p>La finestra de propietats del fitxer us mostra informació com el tipus de fitxer, la mida, i la darrera modificació. Si necessiteu aquesta informació amb freqüència, podeu mostrar-la a <link xref="nautilus-list">columnes de visualització en llista</link> o <link xref="nautilus-display#icon-captions">llegendes de les icones</link>.</p>

  <p>For certain types of files, such as images and videos, there will be an
  extra <gui>Properties</gui> entry that provides information like the
  dimensions, duration, and codec.</p>

  <p>For the <gui>Permissions</gui> entry see
  <link xref="nautilus-file-properties-permissions"/>.</p>

<section id="basic">
 <title>Propietats bàsiques</title>
 <terms>
  <item>
    <title><gui>Nom</gui></title>
    <p>The name of the file or folder.</p>
    <note style="tip">
      <p>To rename a file, see <link xref="files-rename"/>.</p>
    </note>
  </item>
  <item>
    <title><gui>Tipus</gui></title>
    <p>Això us permet identificar el tipus de fitxer, com ara un document PDF, un text OpenDocument o una imatge JPEG. El tipus de fitxer determina quines aplicacions poden obrir el fitxer, entre altres coses. Per exemple, no podeu obrir una imatge amb un reproductor de música. Consulteu <link xref="files-open"/> per a obtenir més informació.</p>
    <note style="tip">
      <p>To change the default application to open a file type, see <link xref="files-open#default"/>.</p>
    </note>
  </item>

  <item>
    <title>Continguts</title>
    <p>Aquest camp es mostra si cerca les propietats d'una carpeta en lloc de les d'un fitxer. Us ajuda a veure el nombre d'elements a la carpeta. Si la carpeta inclou altres carpetes, cada subcarpeta es compta com un element, encara que contingui altres elements. Cada fitxer també es compta com un element. Si la carpeta està buida, als continguts es mostrarà <gui>res</gui>.</p>
  </item>

  <item>
    <title>Mida</title>
    <p>Aquest camp es mostra si esteu veient un fitxer (no una carpeta). La mida d'un fitxer us indica quant d'espai al disc ocupa. Aquest també és un indicador del temps que trigarà a descarregar-se o enviar-se per correu electrònic (els fitxers grans triguen més a enviar-se).</p>
    <p>Les mides es poden obtenir en bytes, KB, MB o GB; en el cas dels tres últims, la mida en bytes també es dona entre parèntesis. Tècnicament, 1 KB són de 1024 bytes, 1 MB són de 1024 KB i així successivament.</p>
  </item>

  <item>
    <title>Espai lliure</title>
    <p>Això només es mostra per a carpetes. Proporciona la quantitat d'espai disponible al disc on hi ha la carpeta. Això és útil per a comprovar si el disc dur està ple.</p>
  </item>

  <item>
    <title>Carpeta pare</title>
    <p>La ubicació de cada fitxer a l'ordinador la proporciona <em>el camí absolut</em>. Aquesta és una «adreça» única del fitxer a l'ordinador, format per una llista de les carpetes que haureu d'introduir per a trobar el fitxer. Per exemple, si Jordi tenia un fitxer anomenat <file>Resum.pdf</file> a la seva carpeta Inici, la seva carpeta principal seria <file>/home/jordi</file> i la seva ubicació seria <file>/home/jordi/Resum.pdf</file>.</p>
  </item>

  <item>
    <title>Accedit</title>
    <p>La data i l'hora en què es va obrir el fitxer.</p>
  </item>

  <item>
    <title>Modificat</title>
    <p>La data i l'hora en què el fitxer es va modificar i desar per última vegada.</p>
  </item>

  <item>
    <title>Created</title>
    <p>The date and time when the file was created.</p>
  </item>
 </terms>
</section>

</page>
