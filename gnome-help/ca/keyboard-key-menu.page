<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-menu" xml:lang="ca">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>
    <link type="seealso" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="new"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>La tecla de <key>Menú</key> obre un menú contextual amb el teclat en comptes d'amb el botó dret del ratolí.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Quina és la tecla de <key>Menú</key>?</title>

  <p>La tecla de <key>Menú</key>, també coneguda com a tecla <em>Aplicació</em>, és una tecla que es troba en alguns teclats orientats a Windows. Aquesta tecla sol estar en la part inferior dreta del teclat, al costat de la tecla <key>Ctrl</key>, però els fabricants de teclat poden situar-la en una ubicació diferent. Se sol representar com un cursor que flota sobre un menú:<media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-menu.svg">
  <key>Menu</key> key icon</media>.</p>

  <p>La funció principal d'aquesta tecla és llançar un menú contextual amb el teclat en lloc de clicar amb el botó dret del ratolí: això és útil si el ratolí o un dispositiu similar no estan disponibles o quan el botó dret del ratolí no existeix.</p>

  <p>La tecla de <key>Menú</key> de vegades s'omet en interès de l'espai, sobretot en teclats d'ordinadors portàtils. En aquest cas, alguns teclats inclouen la funció <key>Menú</key> en una tecla que es pot activar en combinació amb la tecla de Funció (<key>Fn</key>).</p>

  <p>El <em>menu contextual</em> és un menú que apareix quan cliqueu amb el botó dret. El menú que veieu, si existeix, depèn del context i és funció de l'àrea que hàgiu clicat amb el botó dret. Quan utilitzeu la tecla de <key>Menú</key>, el menú contextual es mostra en funció de la zona de pantalla on estava el cursor en el moment de prémer la tecla.</p>

</page>
