<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="ca">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>
    <revision version="gnome:46" date="2024-04-22" status="draft"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Emanuel Cisár</name>
      <email>536429@mail.muni.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Fer clic, arrossegar o desplaçar-vos fent servir tocs i gestos al touchpad.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Fer clic, arrossegar o desplaçar-vos amb el touchpad</title>

  <p>Podeu clicar, fer doble clic, arrossegar i desplaçar només fent servir el touchpad, sense necessitat de botons.</p>

  <note>
    <p>Els <link xref="touchscreen-gestures">gestos del touchpad</link> es recullen per separat.</p>
  </note>

<section id="secondary-click">
  <title>Secondary click</title>

  <p>You can customize your touchpad's secondary click (right click) action.</p>

  <list type="bullet">
    <item>
      <p>Two-Finger Push: Push anywhere with 2 fingers.</p>
    </item>
    <item>
      <p>Corner Push: Push with a single finger in the corner.</p>
    </item>
  </list>

  <steps>
    <title>Select Secondary Click Method</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i touchpad</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Touchpad</gui> in the top bar.</p>
    </item>
    <item>
      <p>In the <gui>Clicking</gui> section, choose the preferred click
      method.</p>
    </item>
  </steps>
</section>

<section id="tap">
  <title>Toc per a clicar</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Podeu tocar el touchpad per a clicar en comptes d'utilitzar un botó.</p>

  <list>
    <item>
      <p>Per a clicar, toqueu sobre el touchpad.</p>
    </item>
    <item>
      <p>Per a doble clic, toqueu dues vegades.</p>
    </item>
    <item>
      <p>Per a arrossegar un element, toqueu dos cops però no aixequeu el dit després del segon toc. Arrossegueu l'element on vulgueu i, a continuació, aixequeu el dit per a deixar-lo anar.</p>
    </item>
    <item>
      <p>Si el vostre touchpad és compatible amb tocs amb diversos dits, feu clic dret tocant amb dos dits alhora. En cas contrari, encara haureu d'utilitzar botons del maquinari per a fer clic dret. Veure <link xref="a11y-right-click"/> el mètode per a fer el clic dret sense el segon botó del ratolí.</p>
    </item>
    <item>
      <p>Si el vostre touchpad dona suport a tocs amb diversos dits, feu <link xref="mouse-middleclick">clic central</link> tocant-lo amb tres dits a la vegada.</p>
    </item>
  </list>

  <note>
    <p>Quan toqueu o arrossegueu amb diversos dits, assegureu-vos que els vostres dits estiguin ben separats. Si els vostres dits estan massa a prop, l'equip pot pensar que és un sol dit.</p>
  </note>

  <steps>
    <title>Habilitar toc per a clicar</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliqueu sobre <gui>Ratolí i touchpad</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Touchpad</gui>, assegureu-vos que l'interruptor <gui>Touchpad</gui> està activat.</p>
      <note>
        <p>La secció <gui>Touchpad</gui> només apareix si el seu sistema té un touchpad.</p>
      </note>
    </item>
   <item>
      <p>Commuteu <gui>Toc per a clicar</gui> a actiu.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Desplaçar amb dos dits</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Podeu desplaçar-vos usant el vostre touchpad amb dos dits.</p>

  <p>Quan estigui seleccionat, tocar i arrossegar amb un dit funcionarà de manera normal, però si arrossegueu dos dits a través de qualsevol part del touchpad, es desplaçarà. Moveu els dits entre la part superior i inferior del touchpad per a desplaçar-vos cap amunt i cap avall, o moveu els dits per sobre del touchpad per a desplaçar-vos de costat. Aneu amb compte de deixar els dits una mica separats. Si els vostres dits estan molt a prop, pot semblar un sol dit.</p>

  <note>
    <p>Desplaçar amb dos dits pot no funcionar a tots els touchpads.</p>
  </note>

  <steps>
    <title>Habilitar el desplaçament amb dos dits</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliqueu sobre <gui>Ratolí i touchpad</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Touchpad</gui>, assegureu-vos que l'interruptor <gui>Touchpad</gui> està activat.</p>
    </item>
    <item>
      <p>Canvieu <gui>Desplaçament amb dos dits</gui> a actiu.</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>Edge scroll</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>Use edge scroll if you want to scroll with only one finger.</p>

  <p>Your touchpad's specifications should give the exact
  location of the sensors for edge scrolling. Typically, the vertical
  scroll sensor is on a touchpad's right-hand side. The horizontal
  sensor is on the touchpad's bottom edge.</p>

  <p>To scroll vertically, drag your finger up and down the right-hand
  edge of the touchpad. To scroll horizontally, drag your finger across
  the bottom edge of the touchpad.</p>

  <note>
    <p>Edge scrolling may not work on all touchpads.</p>
  </note>

  <steps>
    <title>Enable Edge Scrolling</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliqueu sobre <gui>Ratolí i touchpad</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Touchpad</gui>, assegureu-vos que l'interruptor <gui>Touchpad</gui> està activat.</p>
    </item>
    <item>
      <p>Switch the <gui>Edge Scrolling</gui> switch to on.</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>Desplaçament natural</title>

  <p>Podeu arrossegar el contingut com si féssiu lliscar un tros de paper utilitzant el touchpad.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Ratolí i touchpad</gui>.</p>
    </item>
    <item>
      <p>Cliqueu sobre <gui>Ratolí i touchpad</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la secció <gui>Touchpad</gui>, assegureu-vos que el commutador <gui>Touchpad</gui> estigui a activat.</p>
    </item>
    <item>
      <p>Canvieu el <gui>Desplaçament natural</gui> a activat.</p>
    </item>
  </steps>

  <note>
    <p>Aquesta funcionalitat també es coneix com a <em>Desplaçament invers</em>.</p>
  </note>

</section>

</page>
