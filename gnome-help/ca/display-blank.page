<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="ca">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Canvieu el temps per a entrar en inactivitat de la pantalla per a estalviar energia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Establir el temps d'entrada en inactivitat de la pantalla</title>

  <p>Per a estalviar energia, podeu ajustar el temps abans que la pantalla entri en mode d'espera i quedi inactiva. També podeu desactivar completament la desconnexió.</p>

  <steps>
    <title>Per a establir el temps d'entrada en inactivitat:</title>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Energia</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Utilitzeu la llista desplegable <gui>Pantalla en blanc</gui> sota les <gui>Opcions d'estalvi</gui> per a establir el temps que la pantalla està en blanc, o desactivar-la completament.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Quan l'ordinador estigui inactiu, la pantalla es bloquejarà automàticament per raons de seguretat. Per a canviar aquest comportament, mireu <link xref="session-screenlocks"/>.</p>
  </note>

</page>
