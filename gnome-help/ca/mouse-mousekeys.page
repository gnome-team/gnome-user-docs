<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="ca">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Activa les tecles del ratolí per a controlar el ratolí amb el teclat numèric.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Fer clic i moure el punter del ratolí usant el teclat numèric</title>

  <p>Si teniu problemes amb un ratolí o un altre dispositiu d'apuntament, podeu controlar el punter del ratolí mitjançant el teclat numèric del teclat. Aquesta funcionalitat s'anomena <em>tecles de ratolí</em>.</p>

  <steps>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Accés universal</gui>.</p>
      <p>Podeu accedir a la visió general d'<gui>Activitats</gui> prement-hi a sobre, movent el punter del ratolí a la part superior esquerra de la pantalla, utilitzant <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> seguit de <key>Retorn</key> o utilitzant la tecla <key xref="keyboard-key-super">Súper</key>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Accés universal</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Select the <gui>Pointing &amp; Clicking</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Mouse Keys</gui> switch to on.</p>
    </item>
    <item>
      <p>Assegureu-vos que <key>Bloq Núm</key> està inactiu. Aleshores sereu capaç de moure el punter del ratolí utilitzant el teclat.</p>
    </item>
  </steps>

  <p>El teclat numèric és un conjunt de botons numèrics del vostre teclat generalment disposats en una quadrícula rectangular. Si teniu un teclat sense un teclat numèric (com ara un teclat d'ordinador portàtil), és possible que necessiteu mantenir premuda la tecla de funció (<key>Fn</key>) i utilitzar certes altres tecles del teclat com a teclat numèric. Si utilitzeu aquesta funció sovint en un ordinador portàtil, podeu adquirir teclats numèrics USB o Bluetooth externs.</p>

  <p>Cada número del teclat correspon a una adreça. Per exemple, prémer <key>8</key> mou el punter cap amunt i prémer <key>2</key> el mourà avall. Premeu la tecla <key>5</key> per a clicar amb el ratolí, o premeu ràpidament dues vegades per a fer doble clic.</p>

  <p>La majoria de teclats tenen una clau especial que us permet clicar amb el botó dret, de vegades anomenat la tecla de <key xref="keyboard-key-menu">Menú</key>. Tingueu en compte, però, que aquesta tecla respon a on està el focus del teclat, no on està el punter del ratolí. Veure <link xref="a11y-right-click"/> per a obtenir informació sobre com clicar amb el botó dret mantenint premuda la tecla <key>5</key> o el botó esquerre del ratolí.</p>

  <p>Si voleu utilitzar el teclat numèric per a escriure números mentre les tecles del ratolí estan habilitades, Poseu <key>Bloq Núm</key> a ON. El ratolí no es pot controlar amb el teclat quan <key>Bloq Núm</key> està activat.</p>

  <note>
    <p>Les tecles numèriques normals, en una línia a la part superior del teclat, no controlaran el punter del ratolí. Només es poden utilitzar les tecles numèriques del teclat numèric.</p>
  </note>

</page>
