<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Connectar-se a una xarxa sense fil que no es mostra a la llista de xarxes.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Connectar a una xarxa sense fil oculta</title>

<p>És possible configurar una xarxa sense fil perquè estigui "oculta". Les xarxes ocultes no apareixeran a la llista que es mostra a la configuració de <gui>Xarxa</gui>. Per a connectar-vos a una xarxa sense fil oculta:</p>

<steps>
  <item>
    <p>Obriu el <gui xref="shell-introduction#systemmenu">menú de sistema</gui> des de la part dreta a la barra superior.</p>
  </item>
  <item>
    <p>Select the arrow of
    <gui>Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>All Networks</gui>.</p>
  </item>
  <item><p>Premeu el botó de menú a la cantonada superior dreta de la finestra i seleccioneu <gui>Connecta't a una xarxa oculta...</gui>.</p></item>
 <item>
  <p>A la finestra que apareix, seleccioneu una xarxa oberta connectada prèviament utilitzant la llista desplegable <gui>Connexió</gui>, o <gui>Nova</gui> per una de nova.</p>
 </item>
 <item>
  <p>Per a obtenir una nova connexió, escriviu el nom de la xarxa i escolliu el tipus de seguretat de la llista desplegable <gui>Seguretat Wi-Fi</gui>.</p>
 </item>
 <item>
  <p>Introduïu la contrasenya o altres detalls de seguretat.</p>
 </item>
 <item>
  <p>Cliqueu a <gui>Connecta</gui>.</p>
 </item>
</steps>

  <p>És possible que hàgiu de comprovar la configuració del punt d'accés o l'encaminador per a veure quin és el nom de la xarxa. Si no teniu el nom de la xarxa (SSID), podeu utilitzar el <em>BSSID</em> (Basic Service Set Identifier, l'adreça MAC del punt d'accés), que té un aspecte semblant a <gui>02:00:01:02:03:04</gui> i normalment es pot trobar a la part inferior del punt d'accés.</p>

  <p>També haureu de comprovar la configuració de seguretat del punt d'accés. Cerqueu termes com WEP i WPA.</p>

<note>
 <p>Podeu pensar que amagar la vostra xarxa sense fil millorarà la seguretat evitant que les persones que no la coneguin s'hi connectin. A la pràctica, aquest no és el cas; la xarxa és una mica més difícil de trobar, però encara és detectable.</p>
</note>

</page>
