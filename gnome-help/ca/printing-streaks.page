<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Si les impressions es veuen ratllades, descolorides o falten colors, consulteu els nivells de tinta o bé netegeu el capçal d'impressió.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Per què hi ha ratlles, línies o colors incorrectes a les meves impressions?</title>

  <p>Si les vostres impressions són fluixes, descolorides, hi ha línies que estan fora de lloc o tenen de mala qualitat, pot ser degut a un problema amb la impressora o amb un baix nivell de tinta o tòner.</p>

  <terms>
     <item>
       <title>Text o imatges que s'esvaeixen</title>
       <p>És possible que us estigueu quedant sense tinta o tòner. Comproveu el subministrament de tinta o tòner i compreu un cartutx nou si és necessari.</p>
     </item>
     <item>
       <title>Ratlles i línies</title>
       <p>Si teniu una impressora d'injecció de tinta, el capçal d'impressió pot estar brut o parcialment bloquejat. Intenteu netejar el capçal d'impressió. Consulteu el manual de la impressora per a obtenir instruccions.</p>
     </item>
     <item>
       <title>Colors incorrectes</title>
       <p>La impressora pot haver quedat sense un color de tinta o tòner. Comproveu el subministrament de tinta o tòner i compreu un cartutx nou si és necessari.</p>
     </item>
     <item>
       <title>Línies irregulars, o línies que no són rectes</title>
       <p>Si les línies de la impressió han de ser rectes i es tornen irregulars, és possible que hàgiu d'alinear el capçal d'impressió. Consulteu el manual d'instruccions de la impressora per a obtenir detalls sobre com fer-ho.</p>
     </item>
  </terms>

</page>
