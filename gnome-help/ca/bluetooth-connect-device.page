<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="bluetooth-connect-device" xml:lang="ca">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="seealso" xref="sharing-bluetooth"/>
    <link type="seealso" xref="bluetooth-remove-connection"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Com aparellar dispositius Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Connectar l'ordinador a un dispositiu Bluetooth</title>

  <p>Abans de poder utilitzar un dispositiu Bluetooth com un ratolí o un auricular, primer s'ha de connectar l'ordinador al dispositiu. A això també s'anomena <em>aparellar</em> els dispositius Bluetooth.</p>

  <steps>
    <item>
      <p>Obriu la vista d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Bluetooth</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Assegureu-vos que Bluetooth està actiu: l'interruptor de la part superior ha d'estar a on. Amb el quadre obert i l'interruptor a on, l'ordinador cercarà dispositius.</p>
    </item>
    <item>
      <p>Feu que l'altre dispositiu Bluetooth estigui <link xref="bluetooth-visibility">detectable o visible</link> i situeu-lo a 5-10 metres de l'ordinador.</p>
    </item>
    <item>
      <p>Cliqueu al dispositiu a la llista de <gui>Dispositius</gui>. S'obrirà el quadre pel dispositiu.</p>
    </item>
    <item>
      <p>Si és necessari, confirmeu el PIN a l'altre dispositiu. El dispositiu us mostrarà el PIN que veieu a la pantalla de l'ordinador. Confirmeu el PIN al dispositiu potser heu de clicar a <gui>Aparella</gui> o <gui>Confirma</gui>), aleshores cliqueu a <gui>Confirma</gui> a l'ordinador.</p>
      <p>La majoria de dispositius exigeixen que introduïu el PIN en uns 20 segons, o la connexió no es completarà. Si succeeix això, torneu a la llista de dispositius i comenceu de nou.</p>
    </item>
    <item>
      <p>L'entrada pel dispositiu a la llista de <gui>Dispositius</gui> mostrarà l'estatus <gui>Connectat</gui>.</p>
    </item>
    <item>
      <p>Per a editar el dispositiu, cliqueu a la llista de <gui>Dispositius</gui>. Veureu un quadre específic pel dispositiu. Pot mostrar opcions addicionals aplicables al tipus de dispositiu amb el qual esteu connectant.</p>
    </item>
    <item>
      <p>Tanqueu el quadre un cop hàgiu canviat la configuració.</p>
    </item>
  </steps>

  <media its:translate="no" type="image" src="figures/bluetooth-symbolic.svg" style="floatend">
    <p its:translate="yes">Cliqueu a la icona de Bluetooth del quadre superior</p>
  </media>
  <p>Quan un o més dispositius Bluetooth es connecten, la icona de Bluetooth apareix a l'àrea d'estat del sistema.</p>

</page>
