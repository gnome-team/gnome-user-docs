<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="ca">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Feu doble clic o arrossegueu una barra de títol per a maximitzar o restaurar una finestra.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Maximitzar i desmaximitzar una finestra</title>

  <p>Podeu maximitzar una finestra per a ocupar tot l'espai de l'escriptori i restaurar-la a la mida normal. També podeu maximitzar les finestres verticalment a la part esquerra i dreta de la pantalla, de manera que podeu mirar fàcilment dues finestres alhora. Veure <link xref="shell-windows-tiled"/> per més detalls.</p>

  <p>Per a maximitzar una finestra, agafeu la barra de títol i arrossegueu-la cap a la part superior de la pantalla o simplement feu doble clic a la barra de títol. Per a maximitzar una finestra amb el teclat, mantingueu premuda la tecla <key xref="keyboard-key-super">Súper</key> i premeu <key>↑</key>, o premeu <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">També podeu maximitzar una finestra clicant al botó maximitzar de la barra de títol.</p>

  <p>Per a restaurar una finestra a la seva mida no maximitzada, arrossegueu-la lluny de les vores de la pantalla. Si la finestra està plenament maximitzada, podeu fer doble clic a la barra de títol per a restaurar-la. També podeu utilitzar les mateixes dreceres de teclat que heu utilitzat per a maximitzar finestra.</p>

  <note style="tip">
    <p>Mantingueu premuda la tecla <key>Súper</key> i arrossegueu qualsevol part d'una finestra per a moure-la.</p>
  </note>

</page>
