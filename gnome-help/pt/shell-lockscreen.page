<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="pt">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A decorativa e funciono ecrã de bloqueio transmite informações úteis.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>A ecrã de bloqueio</title>

  <p>A ecrã de bloqueio é feita para que possa ver o que está acontecendo enquanto o seu computador está bloqueado e ela permite que obtenha um resumo do que está acontecendo enquanto esteve ausente. A ecrã de bloqueio fornece informações úteis:</p>

  <list>
<!--<item><p>the name of the logged-in user</p></item> -->
    <item><p>data e hora e certas notificações</p></item>
    <item><p>status da bateria e da rede</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Para desbloquear seu computador, clique uma vez com seu rato ou touchpad ou  pressionando <key>Esc</key> ou <key>Enter</key>. Isso vai revelar o ecrã de início de sessão, onde pode digitar sua palavra-passe para desbloquear. Alternativamente, basta começar a digitar a sua palavra-passe e o ecrã de bloqueio será automaticamente mostrada na medida em que digita. Você também pode alterar utilizadores se o seu computador estiver configurado para mais do que um.</p>

  <p>Para ocultar notificações do ecrã de bloqueio, veja <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
