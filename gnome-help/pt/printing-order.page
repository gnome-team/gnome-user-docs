<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-order" xml:lang="pt">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Intercale e inverta a ordem de impressão.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Fazendo páginas imprimirem em uma ordem diferente</title>

  <section id="reverse">
    <title>Inverter</title>

    <p>Impressoras geralmente imprimem a primeira página primeiro e a última página por último, de forma que as páginas terminam na ordem inversa quando as pega. Se necessário, pode inverter essa ordem de impressão.</p>

    <steps>
      <title>Para inverter a ordem:</title>
      <item>
        <p>Pressione <keyseq><key>Ctrl</key><key>F</key></keyseq> para abrir o diálogo <gui>Imprimir</gui>.</p>
      </item>
      <item>
        <p>Na aba <gui>Geral</gui>, embaixo de <gui>Cópias</gui>, marque <gui>Inverter</gui>. A última página será imprimida primeiro, e assim por diante.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>Intercalar</title>

  <p>Se está imprimindo mais de uma cópia do documento, as impressões serão agrupadas por número de página por padrão (isto é, todas as cópias da página 1 virão, então as cópias da página 2, etc.). <em>Intercalar</em> fará com que cada cópia venha com suas páginas agrupadas na ordem correta.</p>

  <steps>
    <title>Para intercalar:</title>
    <item>
     <p>Pressione <keyseq><key>Ctrl</key><key>F</key></keyseq> para abrir o diálogo <gui>Imprimir</gui>.</p>
    </item>
    <item>
      <p>Na aba <gui>Geral</gui>, sob de <gui>Cópias</gui>, marque <gui>Intercalar</gui>.</p>
    </item>
  </steps>

</section>

</page>
