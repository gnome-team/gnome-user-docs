<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-os" xml:lang="pt">

  <info>
    <link type="guide" xref="about" group="os"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Discover information about the operating system installed on your
    system.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Find information about your operating system</title>

  <p>Knowing about what operating system is installed may help you understand
  if new software or hardware will be compatible with your system.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>System</gui>.</p>
    </item>
    <item>
     <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
     results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>About</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>System Details</gui>.</p>
    </item>
    <item>
      <p>Look at the information that is listed under <gui>OS Name</gui>,
      <gui>OS Type</gui>, <gui>GNOME Version</gui> and so on.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Information can be copied from each item by selecting it and then
    copying to the clipboard. This makes it easy to share information about
    your system with others.</p>
  </note>

</page>
