<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="pt">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>
    <revision version="gnome:45" date="2024-04-04" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Torne um ficheiro invisível, para que não o veja no gestor de ficheiros.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Ocultando um ficheiro</title>

  <p>O gestor de ficheiros <app>Ficheiros</app> permite a ocultar e desocultar ficheiros como desejar. Quando um ficheiro é ocultado, ele não é exibido pelo gestor de ficheiros, mas ele ainda está na pasta.</p>

  <p>Para ocultar um ficheiro, <link xref="files-rename">renomeie-o</link> com um <file>.</file> no começo do seu nome. Por exemplo, para ocultar um ficheiro chamado <file>exemplo.txt</file>, deveria renomeá-lo para <file>.exemplo.txt</file>.</p>

<note>
  <p>Pode ocultar pastas da mesma maneira que se oculta ficheiros. Renomeie a pasta colocando um <file>.</file> no começo do nome da pasta.</p>
</note>

<section id="show-hidden">
 <title>Mostrar todos os ficheiros ocultos</title>

  <p>If you want to see all hidden files in a folder, go to that folder and
  either press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again,
  either press the menu button in the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>Desocultar um ficheiro</title>

  <p>To unhide a file, go to the folder containing the hidden file. Press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either press the menu button in
  the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>Por padrão, verá apenas os ficheiros ocultos no gestor de ficheiros até que este seja fechado. Para alterar essa configuração de forma que o gestor de ficheiros sempre mostre os ficheiros ocultos, veja <link xref="nautilus-views"/>.</p></note>

  <note><p>A maioria dos ficheiros ocultos terão um <file>.</file> no começo de seus nomes. Outros, no entanto, podem ter um <file>~</file> no final dos seus nomes. Estes são cópias de segurança de ficheiros. Veja <link xref="files-tilde"/> para mais informações.</p></note>

</section>

</page>
