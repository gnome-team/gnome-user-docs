<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="pt">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Crie rapidamente novos documentos de modelos de ficheiros personalizados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Modelos para tipos de documentos comumente usados</title>

  <p>Se frequentemente cria documentos baseados no mesmo conteúdo, pode se beneficiar do uso de modelos de ficheiros. Um modelo de ficheiro pode ser um documento de qualquer tipo com a formatação ou conteúdo que gostaria de reusar. Por exemplo, poderia criar um modelo de documento com cabeçalho de carta.</p>

  <steps>
    <title>Criar um novo modelo</title>
    <item>
      <p>Crie um documento que vai usar como um modelo. Por exemplo, poderia fazer um cabeçalho de carta em uma aplicativo processador de texto.</p>
    </item>
    <item>
      <p>Salve o ficheiro com o conteúdo do modelo na pasta <file>Modelos</file> em sua <file>Pasta pessoal</file>. Se a pasta <file>Modelos</file> não existir, precisará criá-la antes.</p>
    </item>
  </steps>

  <steps>
    <title>Usar um modelo para criar um documento</title>
    <item>
      <p>Abra a pasta onde gostaria de colocar o novo documento.</p>
    </item>
    <item>
      <p>Clique com o botão direito em qualquer lugar do espaço vazio da pasta. Então, escolha <gui style="menuitem">Novo documento</gui>. Os nomes dos modelos disponíveis serão listados no submenu.</p>
    </item>
    <item>
      <p>Escolha o modelo desejado da lista.</p>
    </item>
    <item>
      <p>Clique duplo no ficheiro para abri-lo e começar a editá-lo. Pode querer <link xref="files-rename">renomear o ficheiro</link> quando finalizar a edição.</p>
    </item>
  </steps>

</page>
