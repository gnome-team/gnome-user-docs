<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="pt">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Você precisa desmarcar a opção <gui>Disponível para todos os utilizadores</gui> nas definições de rede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Outros utilizadores não conseguem editar as conexões de rede</title>

  <p>Se consegue editar uma conexão de rede, mas outros utilizadores do seu computador não conseguem, pode ter que configurar a conexão para estar <em>disponível para todos os utilizadores</em>. Isso faz com que todos no computador possam se <em>conectar</em> usar esta conexão.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Rede</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Rede</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione <gui>Wi-Fi</gui> na lista à esquerda.</p>
    </item>
    <item>
      <p>Clique no botão <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">definições</span></media> para abrir os detalhes da conexão.</p>
    </item>
    <item>
      <p>Selecione <gui>Identidade</gui> no painel à esquerda.</p>
    </item>
    <item>
      <p>Na parte inferior do painel <gui>Identidade</gui>, marque a opção <gui>Disponível para todos os utilizadores</gui> para permitir que outros utilizadores usem a conexão de rede.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Aplicar</gui> para salvar as alterações.</p>
    </item>
  </steps>

</page>
