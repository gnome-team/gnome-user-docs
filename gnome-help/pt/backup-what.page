<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="pt">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Faça cópias de qualquer coisa que não suportaria perder se algo der errado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>O que deve ter cópia de segurança</title>

  <p>Sua prioridade deveria ser fazer cópias de segurança dos seus <link xref="backup-thinkabout">ficheiros mais importantes</link> assim como aqueles que sejam difíceis de recriar. Por exemplo, classificados do mais para o menos importantes:</p>

<terms>
 <item>
  <title>Seus ficheiros pessoais</title>
   <p>Isso pode incluir documentos, planilhas, e-mails, compromissos da agenda, dados financeiros, fotos de família ou outros ficheiros pessoais que consideraria insubstituíveis.</p>
 </item>

 <item>
  <title>Suas definições pessoais</title>
   <p>Aqui estão incluídas as alterações que tenha feito para cores, planos de fundo, resolução de ecrã e definições d rato no seu ambiente. Isto também inclui as definições/preferências de aplicações, tais como as do <app>LibreOffice</app>, do seu reprodutor de música e do seu programa de e-mail. Elas são substituíveis, mas pode levar um tempo para recriá-las.</p>
 </item>

 <item>
  <title>Configurações de sistema</title>
   <p>A maioria das pessoas nunca mudam as definições do sistema que são criadas durante a instalação. Se realmente personaliza suas definições do sistema por alguma razão, ou se usa seu computador com servidor, então pode querer fazer cópias de segurança dessas definições.</p>
 </item>

 <item>
  <title>Softwares instalados</title>
   <p>O software que usa pode ser recuperado bem rapidamente após um problema grave no computador, basta reinstalar.</p>
 </item>
</terms>

  <p>Em geral, vai querer fazer cópias de segurança que sejam insubstituíveis e aqueles que exijam um grande tempo investido para substituí-lo sem uma cópia de segurança. Se são coisas fáceis de substituir, por outro lado, talvez não queira usar espaço em disco com cópias de segurança delas.</p>

</page>
