<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="pt">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Compartilhe mídias na sua rede local usando UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Compartilhando suas músicas, fotos e vídeos</title>

  <p>Pode navegador, pesquisar e reproduzir a mídia em seu computador usando um dispositivo com capacidade de <sys>UPnP</sys> ou <sys>DLNA</sys> tal como um telefone, TV ou console de videogame. Configura <gui>Compartilhamento de mídia</gui> para permitir que esses dispositivos acessem as pastas contendo suas músicas, fotos e vídeos.</p>

  <note style="info package">
    <p>Você deve ter o pacote <app>Rygel</app> instalado para que <gui>Compartilhamento de mídia</gui> esteja visível.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Instalar Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Compartilhamento</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Compartilhamento</gui> para abrir o painel.</p>
    </item>
    <item>
      <note style="info"><p>If the text below <gui>Device Name</gui> allows
      you to edit it, you can <link xref="about-hostname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p>Selecione <gui>Compartilhamento de multimídia</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Compartilhamento de mídia</gui> para ligada.</p>
    </item>
    <item>
      <p>Por padrão, <file>Músicas</file>, <file>Imagens</file> e <file>Vídeos</file> são compartilhados. Para remover um desses, clique no <gui>×</gui> próximo ao nome da pasta.</p>
    </item>
    <item>
      <p>Para adicionar uma outra pasta, clique no <gui style="button">+</gui> para abrir a janela <gui>Escolha uma pasta</gui>. Navegue <em>até</em> a pasta desejada e clique em <gui style="button">Abrir</gui>.</p>
    </item>
    <item>
      <p>Close the <gui>Media Sharing</gui> dialog. You will now be able to browse
      or play media in the folders you selected using the external device.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Redes</title>

  <p>A seção <gui>Redes</gui> lista as redes às quais está atualmente conectada. Use o alternador próximo a cada um para escolher onde sua mídia pode ser compartilhada.</p>

  </section>

</page>
