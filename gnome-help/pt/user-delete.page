<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="pt">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Remova os utilizadores que não usam mais seu computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Excluindo uma conta de utilizador</title>

  <p>Pode adicionar <link xref="user-add">múltiplas contas de utilizadores ao seu computador</link>. Se alguém não está mais usando o seu computador, pode excluir a conta deste utilizador.</p>

  <p>Você precisa de <link xref="user-admin-explain">privilégios administrativos</link> para excluir contas de utilizadores.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Desbloquear</gui> no canto superior direito e insira sua palavra-passe quando solicitada.</p>
    </item>
    <item>
      <p>Click on the user account that you want to delete under
      <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Remove User...</gui> button to delete
      that user account.</p>
    </item>
    <item>
      <p>Cada utilizador tem sua própria pasta pessoal para seus ficheiros e definições. Pode escolher entre manter ou excluir essa pasta pessoal do utilizador. Clique em <gui>Excluir ficheiros</gui> se tem certeza de que ela não será mais usada e precisa liberar espaço em disco. Esses ficheiros serão excluídos permanentemente. Eles não poderão ser recuperados. Talvez queira fazer uma cópia de segurança dos ficheiros em um disco externo ou CD antes de excluí-los.</p>
    </item>
  </steps>

</page>
