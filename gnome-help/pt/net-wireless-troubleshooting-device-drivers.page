<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-device-drivers" xml:lang="pt">

  <info>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Contribuidores para o wiki de documentação do Ubuntu</name>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alguns drivers de dispositivos não funcionam bem com certos adaptadores de rede sem fim, de forma que pode ter de localizar um melhor.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Solução de problemas de rede sem fio</title>

  <subtitle>Certificando-se de que os drivers de dispositivo estão instalados</subtitle>

<!-- Needs links (see below) -->

  <p>Nesta etapa pode verificar para descobrir se pode obter drivers de dispositivos funcionais para seu adaptador de rede sem fio. Um <em>driver de dispositivo</em> é um pedaço de software que fala para seu computador como fazer para um dispositivo de hardware funcionar adequadamente. Ainda que seu adaptador de rede sem fio seja reconhecido pelo computador, este pode não ter os drivers que funcionam muito bem com o adaptador. Pode conseguir localizar drivers diferentes para o adaptador de rede sem fio que funcione de verdade. Tente algumas opções abaixo:</p>

  <list>
    <item>
      <p>Verificar para ver se seu adaptador de rede sem fio está em uma lista de dispositivos com suporte.</p>
      <p>A maioria das distribuições Linux mantém uma lista de dispositivos sem fio aos quais eles oferecem suporte. Em alguns casos, essas listas fornecem informação extra sobre como fazer para que os drivers para certos adaptadores funcionem adequadamente. Acesse a lista de sua distribuição (por exemplo <link href="https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported">Ubuntu</link>, <link href="https://wiki.archlinux.org/index.php/Wireless_network_configuration">Arch</link>, <link href="https://wireless.wiki.kernel.org/en/users/Drivers">Fedora</link> ou <link href="http://en.opensuse.org/HCL:Network_(Wireless)">openSUSE</link>) e veja se a marca e o modelo de seu adaptador de rede sem fio estão listados. Pode conseguir usar algumas das informações registradas lá para fazer com que seus drivers de rede sem fio funcionem.</p>
    </item>
    <item>
      <p>Procurar por drivers restritos (binários).</p>
      <p>Muitas distribuições Linux vêm apenas com os drivers de dispositivo que são <em>livres</em> (free) e <em>código aberto</em> (open source). Isso ocorre porque elas não podem distribuir drivers que são proprietários, ou código fechado. Se o driver correto para seu adaptador de rede sem fio estiver disponível apenas em uma versão não-livre ou “binário-apenas”, ele pode não estar instalado por padrão. Se este for o caso, procure no site do fabricante do adaptador de rede sem fio por algum driver para Linux.</p>
      <p>Algumas distribuições Linux possuem uma ferramenta que pode baixar drivers restritos para. Se sua distribuição possui uma dessas, use-a para ver se ela localizar algum driver de rede sem fio para.</p>
    </item>
    <item>
      <p>Usar os drivers Windows para seu adaptador.</p>
      <p>In general, you cannot use a device driver designed for one operating
      system (like Windows) on another operating system (like Linux). This is
      because they have different ways of handling devices. For wireless
      adapters, however, you can install a compatibility layer called
      <em>NDISwrapper</em> which lets you use some Windows wireless drivers on
      Linux. This is useful because wireless adapters almost always have
      Windows drivers available for them, whereas Linux drivers are sometimes
      not available. You can learn more about how to use NDISwrapper
      <link href="https://sourceforge.net/p/ndiswrapper/ndiswrapper/Main_Page/">here</link>.
      Note that not all wireless drivers can be used through NDISwrapper.</p>
    </item>
  </list>

  <p>Se nenhuma das opções funcionarem, pode preferir tentar um adaptador de rede sem fio diferente para ver se pode fazê-lo funcionar. Adaptadores de rede sem fio USB geralmente são baratos e pode conectá-los em qualquer computador. Mesmo assim, deveria verificar a compatibilidade do adaptador com sua distribuição Linux antes de comprá-lo.</p>

</page>
