<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="pt">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Calibrar sua câmera é importante para capturar cores de forma acurada.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Como calibro minha câmera?</title>

  <p>
    Camera devices are calibrated by taking a photograph of a target
    under the desired lighting conditions.
    By converting the RAW file to a TIFF file, it can be used to
    calibrate the camera device in the <gui>Color</gui> panel.
  </p>
  <p>Você vai precisar cortar o ficheiro TIFF para somente o alvo esteja visível. Certifique-se de que as bordas brancas ou pretas ainda estejam visíveis. A calibração não vai funcionar se a imagem estiver de cabeça para baixo ou estiver muito distorcida.</p>

  <note style="tip">
    <p>O perfil resultante só é válido sob as mesmas condições de iluminação quando adquiriu a imagem original. Isso significa que pode precisar gerar perfis para as condições de iluminação de <em>estúdio</em>, <em>sol forte</em> e <em>dia nublado</em>.</p>
  </note>

</page>
