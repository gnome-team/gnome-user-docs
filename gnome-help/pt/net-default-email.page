<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-email" xml:lang="pt">

  <info>
    <link type="guide" xref="net-email"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.38.2" date="2021-03-07" status="final"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the default email client by going to <gui>Default Apps</gui> in <gui>Settings</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Alterando qual aplicativo de correio é usado para escrever e-mails</title>

  <p>Quando clica em um botão ou link para enviar um novo e-mail (por exemplo, em seu aplicativo processador de textos), seu aplicativo padrão de correio irá se abrir com uma mensagem em branco, pronta para escrever. Se tiver mais que um aplicativo de correio instalado, contudo, o aplicativo de correio errado pode se abrir. Pode corrigir isso alterando qual é o aplicativo de e-mail padrão:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Apps</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Apps</gui></guiseq> from the
      results. This will open the <gui>Apps</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Default Apps</gui>.</p>
    </item>
    <item>
      <p>Escolha qual cliente de e-mail gostaria que fosse usado por padrão alterando a opção <gui>Correio</gui>.</p>
    </item>
  </steps>

</page>
