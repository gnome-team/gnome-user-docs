<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="pt">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A tecla <key>Super</key> abre o panorama de <gui>Atividades</gui>. Ela normalmente fica próxima à tecla <key>Alt</key> do seu teclado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>O que é a tecla <key>Super</key>?</title>

  <p>Quando pressiona a tecla <key>Super</key>, o panorama de <gui>Atividades</gui> é exibido. Essa tecla é normalmente encontrada no canto inferior esquerdo do seu teclado, próximo à tecla <key>Alt</key>, e muitas vezes há um ícone do Windows. Algumas vezes, é chamada de <em>tecla “Windows”</em> ou de tecla de sistema.</p>

  <note>
    <p>Se tiver um teclado da Apple, terá uma tecla <key>⌘</key> (Command) em vez da tecla Windows, enquanto os Chromebooks possui uma lente de aumento.</p>
  </note>

  <p>Para alterar qual tecla é usada para exibir o panorama de <gui>Atividades</gui>:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Definições</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Definições</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>View and Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the <gui>System</gui> category.</p>
    </item>
    <item>
      <p>Click the row with <gui>Show the activities overview</gui>.</p>
    </item>
    <item>
      <p>Pressione a combinação de teclas desejada.</p>
    </item>
  </steps>

</page>
