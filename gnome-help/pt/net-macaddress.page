<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="pt">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>O identificador único atribuído a uma hardware de rede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>O que é um endereço MAC?</title>

  <p>Um <em>endereço MAC</em> é o identificador único que é atribuído pelo fabricante a uma peça do hardware de rede (como uma placa de rede sem fio ou uma placa ethernet). MAC significa <em>Media Access Control</em>, ou controle de acesso ao meio, e cada identificador tem a intenção de ser único para um dispositivo em particular.</p>

  <p>Um endereço MAC consiste em seis conjuntos de dois caracteres, cada um separado por uma vírgula. <code>00:1B:44:11:3A:B7</code> é um exemplo de endereço MAC.</p>

  <p>Para identificar o endereço MAC de seu próprio hardware de rede:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui> for wired connections or <gui>Wi-Fi</gui> for
      wireless connections.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> or <gui>Wi-Fi</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection.</p>
    </item>
    <item>
      <p>The MAC address for the device will be displayed as the
      <gui>Hardware Address</gui> in the <gui>Details</gui> panel.</p>
    </item>
  </steps>

  <p>Na prática, pode precisar modificar ou “burlar” um endereço MAC. Por exemplo, alguns provedores de serviço de Internet podem exigir que um endereço MAC específico seja usado para acessar seus serviços. Se a placa de rede parar de funcionar e precisar trocar por uma nova placa, o serviço não vai mais funcionar. Em tais casos, precisaria burlar o endereço MAC.</p>

</page>
