<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="user-admin-explain" xml:lang="pt">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
        <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Você precisa de privilégios administrativos para alterar partes importantes do seu sistema.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Como funcionam os privilégios administrativos?</title>

  <p>Assim como os ficheiros que <em>você</em> cria, seu computador também tem uma quantidade de ficheiros que são necessários pelo sistema para seu funcionamento adequado. Se esses <em>ficheiros de sistema</em> importantes forem alterados incorretamente, eles podem fazer com que várias coisas deixem de funcionar e, por isso, eles são protegidos contra alterações, por padrão. Certas aplicações também modificam partes importantes do sistema e, assim, também são protegidos.</p>

  <p>A forma como eles são protegidos é permitindo que apenas utilizadores com <em>privilégios administrativos</em> alterem os ficheiros ou usem as aplicações. No uso diário, não precisará alterar quaisquer ficheiros de sistema ou usar esses aplicações, então, por padrão, não tem privilégios administrativos.</p>

  <p>Em alguns casos, é necessário usar esses aplicações. Nesses casos, pode conseguir privilégios administrativos temporariamente para que seja capaz de fazer as alterações desejadas. Se um aplicativo precisa desses privilégios, ele lhe pedirá uma palavra-passe. Por exemplo, se quiser instalar algum aplicativo novo, o instalador de programas (ou gestor de pacotes) será solicitada a sua palavra-passe de administrador para que possa adicionar o novo aplicativo ao sistema. Uma vez que ele tenha terminado, seus privilégios administrativos serão retirados novamente.</p>

  <p>Os privilégios administrativos estão associados a sua conta de utilizador. Utilizadores <gui>Administrador</gui> têm esses privilégios, enquanto utilizadores <gui>Padrão</gui> não têm. Sem esses privilégios, não será capaz de instalar programas. Algumas contas de utilizadores (por exemplo, a conta “root”) têm esses privilégios de forma permanente. Você não deveria usar tais privilégios a toda hora, porque poderia acidentalmente alterar alguma coisa que não tinha intenção (como, excluir um ficheiro crítico para o sistema, por exemplo).</p>

  <p>Em resumo, os privilégios administrativos lhe permitem alterar partes importantes do seu sistema quando necessário, mas também evita que faça algo acidentalmente.</p>

  <note>
    <title>What does “superuser” mean?</title>
    <p>A user with administrative privileges is sometimes called a
    <em>superuser</em>. This is simply because that user has more privileges
    than normal users. You might see people discussing things like <cmd>su</cmd>
    and <cmd>sudo</cmd>; these are programs for temporarily giving you
    “superuser” (administrative) privileges.</p>
  </note>

<section id="advantages">
  <title>Por que os privilégios administrativos são úteis?</title>

  <p>Exigir que os utilizadores tenham privilégios administrativos antes de importantes alterações do sistema serem feitas é útil porque ajuda a impedir que seu sistema se quebre (deixe de funcionar adequadamente), intencionalmente ou não.</p>

  <p>Se tem tais privilégios o tempo todo, pode acidentalmente alterar um ficheiro importante ou executar um aplicativo que altera algo importante por engano. Obter os privilégios administrativos apenas temporariamente, quando precisar deles, reduz o risco desses enganos acontecerem.</p>

  <p>Somente alguns utilizadores de confiança deveriam ter permissão a tais privilégios. Isso evita que outros utilizadores baguncem o seu computador e façam coisas como desinstalar aplicações que precisa, instalar aplicações que não queira ou alterar ficheiros importantes. Isso é útil do ponto de vista da segurança.</p>

</section>

</page>
