<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="pt">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Imprima folhetos dobrados (como um livro ou panfleto) de um PDF usando papel A4/Carta de tamanho normal.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Imprimindo um folheto em uma impressora de frente e verso</title>

  <p>Pode fazer folhetos dobrados (como um pequeno livro ou panfleto) imprimindo páginas de um documento em uma ordem especial e alterando algumas opções de impressão.</p>

  <p>Essas instruções são para impressão de um folheto a partir de um documento PDF.</p>

  <p>Se deseja imprimir um folheto a partir de um documento do <app>LibreOffice</app>, primeiro exporte-o para um PDF escolhendo <guiseq><gui>Ficheiro</gui><gui>Exportar como PDF…</gui></guiseq>. Seu documento precisa ter um número de páginas múltiplo de 4 (4, 8, 12, 16, …). Pode precisar adicionar até 3 páginas em branco.</p>

  <p>Para imprimir um folheto:</p>

  <steps>
    <item>
      <p>Abra o diálogo de impressão. Isso normalmente pode ser feito por meio de <gui style="menuitem">Imprimir</gui> no menu ou usando o atalho de teclado <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Clique no botão <gui>Prioridades…</gui></p>
      <p>Na lista suspensa <gui>Orientação</gui>, certifique-se de que <gui>Paisagem</gui> esteja selecionado.</p>
      <p>Na lista suspensa <gui>Duplex</gui>, selecione <gui>Borda menor</gui>.</p>
      <p>Clique em <gui>OK</gui> para voltar para o diálogo de impressão.</p>
    </item>
    <item>
      <p>Sob <gui>Intervalos e Cópias</gui>, selecione <gui>Páginas</gui>.</p>
    </item>
    <item>
      <p>Digite os números das páginas nesta ordem (n é o número total de páginas, e um múltiplo de 4:</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>Exemplos:</p>
      <list>
        <item><p>Livreto de 4 páginas: Digite <input>4,1,2,3</input></p></item>
        <item><p>Folheto de 8 páginas: Digite <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>Folheto de 20 páginas: Digite <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Escolha a aba <gui>Leiaute da página</gui>.</p>
      <p>Na opção <gui>Leiaute</gui>, selecione <gui>Brochura</gui>.</p>
      <p>Sob <gui>Lados da página</gui>, no lista suspensa de <gui>Incluir</gui>, selecione <gui>Todas as páginas</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Imprimir</gui>.</p>
    </item>
  </steps>

</page>
