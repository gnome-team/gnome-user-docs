<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="pt">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere o tempo até apagar o ecrã para economizar energia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Definindo o tempo de apagamento do ecrã</title>

  <p>Para economizar energia, pode ajustar o tempo até que o ecrã se apague quando houver ociosidade. Você também pode desabilitar o apagamento completamente.</p>

  <steps>
    <title>Para definir o tempo de apagamento do ecrã:</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Energia</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Use the <gui>Screen Blank</gui> drop-down list under <gui>Power Saving
      Options</gui> to set the time until the screen blanks, or disable the blanking
      completely.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Quando seu computador ficar ocioso, o ecrã será automaticamente bloqueada por motivos de segurança. Para alterar este comportamento, veja <link xref="session-screenlocks"/>.</p>
  </note>

</page>
