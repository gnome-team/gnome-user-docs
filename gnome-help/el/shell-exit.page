<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="el">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision version="gnome:44" date="2023-12-30" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μάθετε πώς να αφήσετε τον λογαριασμό χρήστη, αποσυνδέοντας, αλλάζοντας χρήστες κ.ο.κ.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αποσύνδεση, τερματισμός ή αλλαγή χρηστών</title>

  <p>Όταν έχετε τελειώσει τη χρήση του υπολογιστή σας, μπορείτε να τον σβήσετε, να τον αναστείλετε (για εξοικονόμηση ισχύος) ή να τον αφήσετε αναμμένο και να αποσυνδεθείτε.</p>

<section id="logout">
  <info>
    <link type="seealso" xref="user-add"/>
  </info>

  <title>Αποσύνδεση ή αλλαγή χρηστών</title>

  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-exit-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Μενού χρήστη</p>
      </media>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-exit-classic-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Μενού χρήστη</p>
      </media>
    </if:when>
  </if:choose>

  <p>Για να επιτρέψετε σε άλλους χρήστες να χρησιμοποιήσουν τον υπολογιστή σας, μπορείτε είτε να αποσυνδεθείτε, ή να αφήσετε τον εαυτό σας συνδεμένο και να αλλάξετε απλά χρήστες. Εάν αλλάξετε χρήστες, όλες οι εφαρμογές σας θα συνεχίσουν να εκτελούνται και καθετί θα είναι εκεί που το αφήσατε όταν επανασυνδεθείτε.</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p>Οι καταχωρίσεις <gui>Αποσύνδεση</gui> και <gui>Αλλαγή χρήστη</gui> εμφανίζονται μόνο στο μενού εάν έχετε περισσότερους από έναν λογαριασμούς χρήστη στο σύστημά σας.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>Η καταχώριση <gui>Αλλαγή χρήστη</gui> εμφανίζεται μόνο στο μενού, αν έχετε περισσότερους από έναν λογαριασμούς χρήστη στο σύστημά σας.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Κλείδωμα της οθόνης</title>

  <p>If you’re leaving your computer for a short time, you should lock your
  screen to prevent other people from accessing your files or running
  applications. When you return, you will see the
  <link xref="shell-lockscreen">lock screen</link>. Enter your
  password to log back in. If you don’t lock your screen, it will lock
  automatically after a certain amount of time.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and click the
    <media type="image" its:translate="no" src="figures/system-lock-screen-symbolic.svg">
      Lock
    </media>
  button.</p>

  <p>When your screen is locked, other users can log in to their own accounts
  by clicking <gui>Log in as another user</gui> at the bottom right of the login
  screen. You can switch back to your desktop when they are finished.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Αναστολή</title>

  <p>To save power, suspend your computer when you are not using it. If you use
  a laptop, the system, by default, suspends your computer automatically when
  you close the lid.
  This saves your state to your computer’s memory and powers off most of the
  computer’s functions. A very small amount of power is still used during
  suspend.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select <gui>Suspend</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Τερματισμός και επανεκκίνηση</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>Εάν υπάρχουν άλλοι συνδεδεμένοι χρήστες, μπορεί να μην επιτραπεί ο τερματισμός ή η επανεκκίνηση του υπολογιστή, επειδή αυτό θα τερματίσει τις συνεδρίες τους. Εάν είσαστε διαχειριστής, μπορεί να σας ζητηθεί ο κωδικός πρόσβασής σας για τον τερματισμό.</p>

  <note style="tip">
    <p>Μπορεί να θελήσετε να κλείσετε τον υπολογιστή σας, εάν επιθυμείτε να τον μετακινήσετε και δεν έχετε μπαταρία, εάν η μπαταρία σας είναι χαμηλή ή δεν κρατά φορτίο καλά. Επίσης ένας απενεργοποιημένος υπολογιστής χρησιμοποιεί <link xref="power-batterylife">λιγότερη ενέργεια</link> από έναν που είναι σε αναστολή.</p>
  </note>

</section>

</page>
