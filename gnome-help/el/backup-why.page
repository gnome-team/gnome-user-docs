<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="tip" id="backup-why" xml:lang="el">

  <info>
    <link type="guide" xref="files#backup"/>
    <title type="link" role="trail">Αντίγραφα ασφαλείας</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Γιατί, τι, πού και πώς των αντιγράφων ασφαλείας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Αντίγραφα ασφαλείας των σημαντικών σας αρχείων</title>

  <p><em>Αντίγραφα ασφαλείας</em> των αρχείων σας απλά σημαίνει να αντιγράψετε για ασφαλή αποθήκευση. Αυτό γίνεται σε περίπτωση που τα αρχικά αρχεία γίνονται άχρηστα λόγω απώλειας ή φθοράς. Αυτά τα αντίγραφα μπορούν να χρησιμοποιηθούν για να επαναφέρουν τα αρχικά δεδομένα σε περίπτωση απώλειας. Τα αντίγραφα πρέπει να αποθηκευτούν σε μια διαφορετική συσκευή από τα αρχικά αρχεία. Για παράδειγμα, μπορείτε να χρησιμοποιήσετε ένα USB, έναν εξωτερικό σκληρό δίσκο, ένα CD/DVD, ή μια υπηρεσία αποθήκευσης δεδομένων.</p>

  <p>Ο καλύτερος τρόπος για να δημιουργήσετε αντίγραφα ασφαλείας των αρχείων σας, είναι να το κάνετε συχνά, κρατώντας τα αντίγραφα εκτός τόπου και (πιθανόν) κρυπτογραφημένα.</p>

<links type="topic" style="2column"/>

</page>
