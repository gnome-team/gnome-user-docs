<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="el">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Ενεργοποιήστε οπτικές ειδοποιήσεις να αναβοσβήνουν στην οθόνη ή το παράθυρο όταν ένας ήχος ειδοποίησης παίζεται.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αναλαμπή της οθόνης για ήχους ειδοποίησης</title>

  <p>Ο υπολογιστής σας θα παίξει έναν απλό ήχο ειδοποίησης για συγκεκριμένους τύπους μηνυμάτων και συμβάντων. Εάν δυσκολεύεστε να ακούσετε αυτούς τους ήχους, μπορείτε να κάνετε ή όλη την οθόνη ή το τρέχον παράθυρο να αναλάμψει οπτικά όποτε παίζεται ο ήχος ειδοποίησης.</p>

  <p>This can also be useful if you’re in an environment where you need your
  computer to be silent, such as in a library. See <link xref="sound-alert"/>
  to learn how to mute the alert sound, then enable visual alerts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Hearing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Visual Alerts</gui> section, switch the <gui>Visual Alerts</gui>
      switch to on.</p>
    </item>
    <item>
      <p>Under <gui>Flash Area</gui>, select whether you want the entire screen or just
      your current window title to flash.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Μπορείτε γρήγορα να ενεργοποιήσετε ή όχι τις οπτικές ειδοποιήσεις κάνοντας κλικ στο <link xref="a11y-icon">εικονίδιο προσιτότητας</link> στην πάνω γραμμή και να επιλέξετε <gui>οπτικές ειδοποιήσεις</gui>.</p>
  </note>

</page>
