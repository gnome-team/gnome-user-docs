<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="el">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="43" date="2022-09-10" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Αντιγραφή ή μετακίνηση στοιχείων σε νέο φάκελο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Αντιγραφή ή μετακίνηση αρχείων και φακέλων</title>

 <p>Ένα αρχείο ή φάκελος μπορεί να αντιγραφεί ή να μετακινηθεί σε μια νέα θέση με σύρσιμο και απόθεση με το ποντίκι, χρησιμοποιώντας τις εντολές αντιγραφής και επικόλλησης, ή χρησιμοποιώντας τις συντομεύσεις πληκτρολογίου.</p>

 <p>For example, you might want to copy a presentation onto a memory stick so
 you can take it to work with you. Or, you could make a back-up copy of a
 document before you make changes to it (and then use the old copy if you don’t
 like your changes).</p>

 <p>Αυτές οι οδηγίες εφαρμόζονται και στα αρχεία και στους φακέλους. Αντιγράφετε και μετακινείτε αρχεία και φακέλους με τον ίδιο ακριβώς τρόπο.</p>

<steps ui:expanded="false">
<title>Αντιγραφή και επικόλληση αρχείων</title>
<item><p>Επιλέξτε το αρχείο που επιθυμείτε να αντιγράψετε κάνοντας κλικ σε αυτό μια φορά.</p></item>
<item><p>Right-click and select <gui>Copy</gui>, or press
 <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>Περιηγηθείτε σε έναν άλλο φάκελο, όπου θέλετε να βάλετε το αντίγραφο του αρχείου.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Αποκοπή και επικόλληση αρχείων για μετακίνηση</title>
<item><p>Επιλέξτε το αρχείο που επιθυμείτε να μετακινήσετε κάνοντας κλικ πάνω του μια φορά.</p></item>
<item><p>Right-click and select <gui>Cut</gui>, or press
 <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>Περιηγηθείτε σε έναν άλλο φάκελο, όπου θέλετε να μετακινήσετε το αρχείο.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Σύρσιμο αρχείων για αντιγραφή ή μετακίνηση</title>
<item><p>Ανοίξτε τον διαχειριστή αρχείων και πηγαίνετε στον φάκελο που περιέχει το αρχείο που θέλετε να αντιγράψετε.</p></item>
<item><p>Press the menu button in the sidebar of the window and select
 <gui style="menuitem">New Window</gui> (or
 press <keyseq><key>Ctrl</key><key>N</key></keyseq>) to open a second window. In
 the new window, navigate to the folder where you want to move or copy the file.
 </p></item>
<item>
 <p>Κάντε κλικ και σύρετε το αρχείο από ένα παράθυρο σε ένα άλλο. Αυτό θα <em>το μετακινήσει</em> εάν ο προορισμός είναι στην <em>ίδια</em> συσκευή, ή θα <em>το αντιγράψει</em> εάν ο προορισμός είναι σε <em>διαφορετική</em> συσκευή.</p>
 <p>For example, if you drag a file from a USB memory stick to your Home folder,
 it will be copied, because you’re dragging from one device to another.</p>
 <p>Μπορείτε να εξαναγκάσετε το αρχείο να αντιγραφεί κρατώντας πατημένο το πλήκτρο <key>Ctrl</key> ενώ σύρετε, ή εξαναγκάζοντας το να μετακινηθεί κρατώντας πατημένο το πλήκτρο <key>Shift</key> ενώ σύρετε.</p>
 </item>
</steps>

<note>
  <p>Δεν μπορείτε να αντιγράψετε ή να μετακινήσετε ένα αρχείο σε έναν φάκελο που είναι <em>μόνο για ανάγνωση</em>. Μερικοί φάκελοι είναι μόνο για ανάγνωση για να αποτρέψουν την αλλαγή των περιεχομένων τους. Μπορείτε να αλλάξετε πράγματα από το να είναι μόνο για ανάγνωση, <link xref="nautilus-file-properties-permissions">αλλάζοντας τα δικαιώματα του αρχείου</link>.</p>
</note>

</page>
