<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="el">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Εκτυπώστε ένα έγγραφο σε χαρτί διαφορετικού μεγέθους, σχήματος ή προσανατολισμού.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αλλαγή του μεγέθους χαρτιού κατά την εκτύπωση</title>

  <p>Αν επιθυμείτε να αλλάξετε το μέγεθος χαρτιού του εγγράφου σας (για παράδειγμα, εκτύπωση ενός PDF μεγέθους γράμματος US σε μέγεθος Α4), μπορείτε να αλλάξετε τον τύπο εκτύπωσης του εγγράφου.</p>

  <steps>
    <item>
      <p>Άνοίξτε τον διάλόγο εκτύπωσης πατώντας <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Επιλέξτε την καρτέλα <gui>Διαμόρφωση σελίδας</gui>.</p>
    </item>
    <item>
      <p>Στη στήλη <gui>Χαρτί</gui>, επιλέξτε το <gui>Μέγεθος χαρτιού</gui> από την πτυσσόμενη λίστα.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Εκτύπωση</gui> για να εκτυπωθεί το έγγραφο σας.</p>
    </item>
  </steps>

  <p>Μπορείτε επίσης να χρησιμοποιήσετε την πτυσσόμενη λίστα <gui>Προσανατολισμός</gui> για να επιλέξετε διαφορετικό προσανατολισμό:</p>

  <list>
    <item><p><gui>Κάθετα</gui></p></item>
    <item><p><gui>Οριζόντια</gui></p></item>
    <item><p><gui>Αντίστροφα κάθετα</gui></p></item>
    <item><p><gui>Αντίστροφα οριζόντια</gui></p></item>
  </list>

</page>
