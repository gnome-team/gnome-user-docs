<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="el">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μεταφέρετε εύκολα αρχεία στις επαφές ηλεκτρονικής αλληλογραφίας σας από τον διαχειριστή αρχείων.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Κοινή χρήση αρχείων με ηλεκτρονική αλληλογραφία</title>

<p>Μπορείτε εύκολα να μοιραστείτε αρχεία με τις επαφές σας με ηλεκτρονική αλληλογραφία άμεσα από τον διαχειριστή αρχείων.</p>

  <note style="important">
    <p>Πριν ξεκινήσετε, βεβαιωθείτε ότι το <app>Evolution</app> ή το <app>Geary</app> είναι εγκατεστημένο στον υπολογιστή σας και  έχει ρυθμιστεί ο λογαριασμός ηλεκτρονικής αλληλογραφίας.</p>
  </note>

<steps>
  <title>Για να μοιραστείτε ένα αρχείο με ηλεκτρονική αλληλογραφία:</title>
    <item>
      <p>Ανοίξτε τα <app>Αρχεία</app> από την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui>.</p>
    </item>
  <item><p>Εντοπίστε το αρχείο που θέλετε να μεταφέρετε.</p></item>
    <item>
      <p>Κάντε δεξί κλικ στο αρχείο και επιλέξτε <gui>Αποστολή σε…</gui>. Το παράθυρο σύνταξης μηνύματος θα εμφανιστεί με το συνημμένο αρχείο.</p>
    </item>
  <item><p>Κάντε κλικ στο <gui>Προς</gui> για επιλογή μιας επαφής, ή εισάγετε μια διεύθυνση ηλεκτρονικής αλληλογραφίας, όπου θέλετε να στείλετε το αρχείο. Συμπληρώστε το <gui>Θέμα</gui> και το σώμα του μηνύματος όπως απαιτείται και πατήστε <gui>Αποστολή</gui>.</p></item>
</steps>

<note style="tip">
  <p>Μπορείτε να στείλετε πολλά αρχεία με μια φορά. Επιλέξτε πολλαπλά αρχεία κρατώντας πατημένο το <key>Ctrl</key> ενώ κάνετε κλικ στα αρχεία, έπειτα κάντε δεξί κλικ σε κάποιο από τα επιλεγμένα αρχεία.</p>
</note>

</page>
