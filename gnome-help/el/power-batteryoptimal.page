<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="el">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tips such as “Do not let the battery charge get too low”.</desc>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Πάρτε το περισσότερο από την μπαταρία του φορητού υπολογιστή σας</title>

<p>Καθώς οι μπαταρίες του φορητού υπολογιστή παλιώνουν, χειροτερεύουν στη φόρτιση αποθήκευσης και η χωρητικότητά τους σταδιακά μειώνεται. Υπάρχουν λίγες τεχνικές που μπορείτε να χρησιμοποιήσετε για να επιμηκύνετε την ωφέλιμη διάρκεια ζωής τους, αν και δεν θα πρέπει να περιμένετε μεγάλη διαφορά.</p>

<list>
  <item>
    <p>Μην αφήνετε τη μπαταρία να αδειάσει τελείως. Να επαναφορτίζετε πάντα <em>πριν</em> η μπαταρία αδειάσει, αν και οι περισσότερες μπαταρίες έχουν ενσωματωμένη προστασία για την αποφυγή του. Η επαναφόρτιση όταν είναι μόνο μερικώς αποφορτισμένη είναι πιο αποτελεσματική, αλλά η επαναφόρτιση όταν είναι μόνο λίγο αποφορτισμένη είναι χειρότερη για τη μπαταρία.</p>
  </item>
  <item>
    <p>Η ζέστη έχει επιζήμια επίδραση στην αποτελεσματικότητα φόρτισης της μπαταρίας. Μην αφήνετε την μπαταρία να θερμανθεί περισσότερο απ' όσο πρέπει.</p>
  </item>
  <item>
    <p>Batteries age even if you leave them in storage. There is little
    advantage in buying a replacement battery at the same time as you get the
    original battery — always buy replacements when you need them.</p>
  </item>
</list>

<note>
  <p>Αυτή η συμβουλή εφαρμόζεται ειδικά για μπαταρίες ιόντος λιθίου, που είναι ο πιο συνηθισμένος τύπος. Άλλοι τύποι μπαταρίας ενδέχεται να επωφελούνται από διαφορετική μεταχείριση.</p>
</note>

</page>
