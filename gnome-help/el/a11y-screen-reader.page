<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="el">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Χρησιμοποιήστε τον αναγνώστη οθόνης <app>Orca</app> για επικοινωνία με τη διεπαφή χρήστη.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Ανάγνωση οθόνης δυνατά</title>

  <p>The <app>Orca</app> screen reader can speak the user interface. Depending
  on how you installed your system, you might not have Orca installed. If not,
  install Orca first.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Εγκατάσταση Orca</link></p>
  
  <p>To start <app>Orca</app> using the keyboard:</p>
  
  <steps>
    <item>
    <p>Press <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Or to start <app>Orca</app> using a mouse and keyboard:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Seeing</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Screen Reader</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ενεργοποιήστε και απενεργοποιήστε γρήγορα τον αναγνώστη οθόνης</title>
    <p>Μπορείτε να ενεργοποιήσετε ή όχι τον αναγνώστη οθόνης κάνοντας κλικ στο <link xref="a11y-icon">εικονίδιο προσιτότητας</link> στην πάνω γραμμή και επιλέγοντας <gui>Αναγνώστης οθόνης</gui>.</p>
  </note>

  <p>Για περισσότερες πληροφορίες, δείτε τη <link href="help:orca">Βοήθεια Orca</link>.</p>
</page>
