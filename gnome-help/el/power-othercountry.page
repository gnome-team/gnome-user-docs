<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="el">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Ο υπολογιστής σας θα δουλέψει, αλλά μπορεί να χρειαστείτε ένα διαφορετικό καλώδιο παροχής ή προσαρμογέα ταξιδιού.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Θα δουλέψει ο υπολογιστής μου με μια παροχή σε άλλη χώρα;</title>

<p>Διαφορετικές χώρες χρησιμοποιούν παροχές ισχύος με διαφορετικές τάσεις (συνήθως 110V ή 220-240V) και συχνότητες εναλλασσόμενου (συνήθως 50 Hz ή 60 Hz). Ο υπολογιστής σας πρέπει να δουλέψει με μια παροχή ισχύος σε μια διαφορετική χώρα εφόσον έχετε έναν κατάλληλο προσαρμογέα ισχύος. Μπορεί επίσης να χρειαστείτε να αντιστρέψετε έναν διακόπτη.</p>

<p>Εάν έχετε έναν φορητό υπολογιστή, αυτό που χρειαζόσαστε είναι να πάρετε τον σωστό ρευματολήπτη για τον προσαρμογέα ισχύος. Μερικοί φορητοί υπολογιστές έρχονται συσκευασμένοι με περισσότερους από έναν ρευματολήπτες για τον προσαρμογέα σας, έτσι μπορεί να έχετε ήδη τον σωστό. Εάν όχι, η σύνδεση με τον υπάρχοντα σε έναν τυπικό προσαρμογέα ταξιδιού θα αρκέσει.</p>

<p>If you have a desktop computer, you can also get a cable with a different
plug, or use a travel adapter. In this case, however, you may need to change
the voltage switch on the computer’s power supply, if there is one. Many
computers do not have a switch like this, and will happily work with either
voltage. Look at the back of the computer and find the socket that the power
cable plugs into. Somewhere nearby, there may be a small switch marked “110V”
or “230V” (for example). Switch it if you need to.</p>

<note style="warning">
  <p>Προσέξτε όταν αλλάζετε τα καλώδια ισχύος ή χρησιμοποιείτε προσαρμογείς ταξιδιού. Απενεργοποιήστε το καθετί, εάν μπορείτε.</p>
</note>

</page>
