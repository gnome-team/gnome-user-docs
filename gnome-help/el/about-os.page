<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-os" xml:lang="el">

  <info>
    <link type="guide" xref="about" group="os"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Discover information about the operating system installed on your
    system.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Find information about your operating system</title>

  <p>Knowing about what operating system is installed may help you understand
  if new software or hardware will be compatible with your system.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>System</gui>.</p>
    </item>
    <item>
     <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
     results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>About</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>System Details</gui>.</p>
    </item>
    <item>
      <p>Look at the information that is listed under <gui>OS Name</gui>,
      <gui>OS Type</gui>, <gui>GNOME Version</gui> and so on.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Information can be copied from each item by selecting it and then
    copying to the clipboard. This makes it easy to share information about
    your system with others.</p>
  </note>

</page>
