<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename-multiple" xml:lang="el">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-rename"/>
    <link type="seealso" xref="files-rename-music-metadata"/>

    <revision version="gnome:47" date="2024-08-31" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rename more than one file at once.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Rename multiple files</title>

  <p><app>Files</app> can be used to change the names of a group of files. You
  can <gui>Rename using a template</gui> or <gui>Find and replace text</gui>.</p>

  <steps>
    <title>To rename using a template:</title>
    <item><p>Select two or more files in <app>Files</app>.</p></item>
    <item><p>Press <key>F2</key> or right-click on the selection and pick
    <gui>Rename</gui>. The <gui>Rename</gui> window opens.</p></item>
    <item><p>Make sure <gui>Rename using a template</gui> is selected.</p>
      <note>
        <p>The name field contains "[Original filename]" as a placeholder. It
        can be deleted or added back from the <gui>+ Add</gui> menu.</p>
      </note>
    <p>Click before or after "[Original filename]" to add letters, words,
    or numbers to create the pattern you want.</p>
      <note style="warning">
        <p>Certain <link xref="files-rename#valid-chars">characters</link>
        should be avoided on some devices.</p>
      </note>
    <p>To number the files sequentially, click <gui>+ Add</gui> to choose
    the number of digits in the number you want added automatically. If you are
    renaming 10 or more files but fewer than 100, choose two digits (<gui>01,
    02, 03, 04</gui>). Choose three (<gui>001, 002, 003, 004</gui>) if you have
    more than 99 files to rename.</p>
    <p>The preview list will show the current filenames on the left and the
    resulting filenames on the right.</p></item>
    <item><p>Click the <gui>Rename</gui> button to apply the pattern you have
    created, and the files will be renamed.</p></item>
  </steps>
  
  <p>Όταν μετονομάζετε ένα αρχείο, επιλέγεται μόνο το πρώτο μέρος του ονόματος αρχείου και όχι η επέκταση του (το μέρος μετά τη <file>.</file>). Η επέκταση κανονικά δηλώνει τον τύπο του αρχείου (για παράδειγμα, <file>αρχείο.pdf</file> είναι ένα έγγραφο PDF) και συνήθως δεν θέλετε να το αλλάξετε. Αν χρειάζεστε επίσης να αλλάξετε την επέκταση, επιλέξτε όλο το όνομα αρχείου και αλλάξτε το.</p>
  
  <steps>
    <title>To rename using find and replace text:</title>
    <item><p>Select two or more files in <app>Files</app>.</p></item>
    <item><p>Press <key>F2</key> or right-click on the selection and pick
    <gui>Rename</gui>.</p></item>
    <item><p>Select <gui>Find and replace text</gui>.</p></item>
    <item><p>For <gui>Existing Text</gui> type the text to be replaced. For
    <gui>Replace With</gui>, type the new text. The preview list will show the
    current filenames on the left and the resulting filenames on the
    right.</p></item>
    <item><p>Click the <gui>Rename</gui> button to accept the replacement in the
    preview, and the files will be renamed.</p></item>
  </steps>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the sidebar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

</page>
