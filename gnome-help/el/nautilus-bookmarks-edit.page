<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="el">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>
    <revision version="gnome:45" date="2024-03-03" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Προσθήκη, διαγραφή και μετονομασία σελιδοδεικτών στον διαχειριστή αρχείων.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Επεξεργασία φακέλων σελιδοδεικτών</title>

  <p>Οι σελιδοδείκτες σας καταχωρίζονται στην πλευρική στήλη του διαχειριστή αρχείων.</p>

  <steps>
    <title>Προσθήκη σελιδοδείκτη:</title>
    <item>
      <p>Επιλέξτε τον φάκελο (ή τη θέση) που επιθυμείτε να προσθέσετε.</p>
    </item>
    <item>
      <p>Click the current folder in the path bar and then select
      <gui style="menuitem">Add to Bookmarks</gui>.</p>
      <note>
        <p>You can also drag a folder to the sidebar, and drop it
        over <gui>New bookmark</gui>, which appears dynamically.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Διαγραφή ενός σελιδοδείκτη:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Remove from Bookmarks</gui> from the menu.</p>
    </item>
  </steps>

  <steps>
    <title>Μετονομασία σελιδοδείκτη:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Rename</gui>.</p>
    </item>
    <item>
      <p>Στο πλαίσιο κειμένου <gui>Όνομα</gui>, πληκτρολογήστε το νέο όνομα για τον σελιδοδείκτη.</p>
      <note>
        <p>Renaming a bookmark does not rename the folder. If you have
        bookmarks to two different folders in two different locations, but
        which each have the same name, the bookmarks will have the same name,
        and you won’t be able to tell them apart. In these cases, it is useful
        to give a bookmark a name other than the name of the folder it points
        to.</p>
      </note>
    </item>
  </steps>

</page>
