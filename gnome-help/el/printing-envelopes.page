<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="el">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Βεβαιωθείτε ότι έχετε τον φάκελο τοποθετημένο σωστά και έχετε επιλέξει το σωστό μέγεθος χαρτιού.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Εκτύπωση φακέλων</title>

  <p>Οι περισσότεροι εκτυπωτές θα επιτρέψουν την εκτύπωση απευθείας σε έναν φάκελο. Αυτό είναι ιδιαίτερα χρήσιμο, εάν για παράδειγμα έχετε πολλά γράμματα να στείλετε.</p>

  <section id="envelope">
    <title>Εκτύπωση σε φακέλους</title>

  <p>Υπάρχουν δύο πράγματα που πρέπει να προσέξετε όταν εκτυπώνετε σε φάκελο</p>
  <p>The first is that your printer knows what size the envelope is. Press
  <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print dialog, go to
  the <gui>Page Setup</gui> tab and choose the <gui>Paper type</gui> as “Envelope”
  if you can. If you cannot do this, see if you can change the <gui>Paper
  size</gui> to an envelope size (for example, <gui>C5</gui>). The pack of
  envelopes will say what size they are; most envelopes come in standard
  sizes.</p>

  <p>Secondly, you need to make sure that the envelopes are loaded with the
  right side up in the printer’s in-tray. Check the printer’s manual for this,
  or try to print a single envelope and check which side is printed on to see
  which way is the right way up.</p>

  <note style="warning">
    <p>Some printers are not designed to be able to print envelopes, especially
    some laser printers. Check your printer’s manual to see if it accepts
    envelopes. Otherwise, you could damage the printer by feeding an envelope
    in.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
