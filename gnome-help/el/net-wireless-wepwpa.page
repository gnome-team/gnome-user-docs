<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-wireless-wepwpa" xml:lang="el">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Τα WEP και WPA είναι τρόποι κρυπτογράφησης δεδομένων σε ασύρματα δίκτυα.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Τι σημαίνουν τα WEP και WPA;</title>

  <p>WEP and WPA (along with WPA2) are names for different encryption tools
  used to secure your wireless connection. Encryption scrambles the network
  connection so that no one can “listen in” to it and look at which web pages
  you are viewing, for example. WEP stands for <em>Wired Equivalent
  Privacy</em>, and WPA stands for <em>Wireless Protected Access</em>. WPA2 is
  the second version of the WPA standard.</p>

  <p>Χρησιμοποιώντας <em>κάποια</em> κρυπτογράφηση είναι πάντα καλύτερα από καμία, αλλά το WEP είναι το λιγότερο ασφαλές από αυτά τα πρότυπα και δεν θα πρέπει να το χρησιμοποιήσετε εάν μπορείτε να το αποφύγετε. Το WPA2 είναι το πιο ασφαλές από τα τρία. Εάν η ασύρματη κάρτα σας και ο δρομολογητής υποστηρίζουν WPA2, θα πρέπει να το χρησιμοποιήσετε όταν ρυθμίζετε το ασύρματο δίκτυό σας.</p>

</page>
