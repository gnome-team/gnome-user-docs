<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="vi">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Control icon captions used in the file manager.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

<title>Tuỳ thích hiển thị trình quản lý tập tin</title>

<p>You can control how the file manager displays captions under icons. Click
the menu button in the sidebar of the window, select
<gui>Preferences</gui>, then go to the <gui>Grid View Captions</gui> section.</p>

<!-- TODO FIXME: Merge / sort out with "nautilus-views.page" overlap for "Sort Folders Before Files" -->
<section id="icon-captions">
  <title>Tên biểu tượng</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Biểu tượng trình quản lý tập tin kèm mô tả</p>
  </media>
  <p>When you use grid view, you can choose to have extra information about
  files and folders displayed in a caption under each icon. This is useful,
  for example, if you often need to see who owns a file or when it was last
  modified.</p>
  <p>You can zoom in a folder by pressing the menu button in the sidebar of the
  window and selecting one of the buttons under
  <gui style="menuitem">Icon Size</gui>. As you zoom in, the
  file manager will display more and more information in captions. You can
  choose up to three things to show in captions. The first will be displayed at
  most zoom levels. The last will only be shown at very large sizes.</p>
  <p>Thông tin bạn có thể xem ở tên biểu tượng cũng giống như thông tin trong các cột khi xem kiểu danh sách. Xem <link xref="nautilus-list"/> để biết thêm.</p>
</section>

<section id="list-view">

  <title>List View</title>

  <p>When viewing files as a list, you can display <gui>Expandable Folders in
  List View</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
