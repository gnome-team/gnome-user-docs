<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="vi">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>
    <revision version="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author">
      <name>Dự án tài liệu GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the screen brightness to make it more readable in bright
    light.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Đặt độ sáng màn hình</title>

  <p>Depending on your hardware, you can change the brightness of your screen to
  save power or to make the screen more readable in bright light.</p>

  <p>To change the brightness of your screen, click the
  <gui xref="shell-introduction#systemmenu">system menu</gui> on the right side
  of the top bar and adjust the screen brightness slider to the value you want
  to use. The change should take effect immediately.</p>

  <note style="tip">
    <p>Many laptop keyboards have special keys to adjust the brightness. These
    often have a picture that looks like the sun. Hold down the <key>Fn</key>
    key to use these keys.</p>
  </note>

  <note style="tip">
    <p>If your computer features an integrated light sensor, the screen
    brightness will automatically be adjusted for you. For more information, see
  <link xref="power-autobrightness"/>.</p>
  </note>

  <p>If it is possible to set the brightness of your screen, you can also have
  the screen dim automatically to save power. For more information, see
  <link xref="power-whydim"/>.</p>

</page>
