<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="vi">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.29" date="2018-08-27" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Điều khiển bằng bàn phím.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

<title>Các phím tắt hữu dụng</title>

<p>This page provides an overview of keyboard shortcuts that can help you
use your desktop and applications more efficiently. If you cannot use a
mouse or pointing device at all, see <link xref="keyboard-nav"/> for more
information on navigating user interfaces with only the keyboard.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Đi vòng màn hình</title>
  <tr xml:id="super">
    <td><p>
      <key xref="keyboard-key-super">Super</key> key
    </p></td>
    <td><p>Switch between the <gui>Activities</gui> overview and desktop. In
    the overview, start typing to instantly search your applications, contacts,
    and documents.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Pop up command window (for quickly running commands).</p>
    <p>Use the arrow keys to quickly access previously run commands.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Quickly switch between
    windows</link>. Hold down <key>Shift</key> for reverse order.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Super</key><key>`</key></keyseq></p></td>
    <td>
      <p>Switch between windows from the same application, or from the selected
      application after <keyseq><key>Super</key><key>Tab</key></keyseq>.</p>
      <p>This shortcut uses <key>`</key> on US keyboards, where the <key>`</key>
      key is above <key>Tab</key>. On all other keyboards, the shortcut is
      <key>Super</key> plus the key above <key>Tab</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td>
      <p>Switch between windows in the current workspace. Hold down
      <key>Shift</key> for reverse order.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Đặt tiêu điểm vào thanh đỉnh. Trong Tổng quan <gui>hoạt động</gui>, chuyển tiêu điểm bàn phím giữa thanh đỉnh, neo ứng dụng, tổng quan cửa sổ, danh sách ứng dụng và ô tìm kiếm. Dùng mũi tên để di chuyển.</p>
    </td>
  </tr>
<!--
  <tr xml:id="ctrl-alt-t">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq></p></td>
    <td>
      <p>Open a Terminal.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-t">
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>T</key></keyseq></p></td>
    <td>
      <p>Open a new Terminal tab on the same window.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-n">
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>N</key></keyseq></p></td>
    <td>
      <p>Open a new Terminal window. To use this shortcut, you should already be on a terminal window.</p>
    </td>
  </tr>
-->
  <tr xml:id="super-a">
    <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
    <td><p>Show the list of applications.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Page Down</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Switch between
     workspaces</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Up</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p>
      <p>and</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Move the current window to a
     different workspace</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Move the current window one monitor to the left.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Move the current window one monitor to the right.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Show the Power Off dialog</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Khoá màn hình.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p>Show <link xref="shell-notifications#notificationlist">the notification
    list</link>. Press <keyseq><key>Super</key><key>V</key></keyseq> again or
    <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Phím tắt sửa đổi thông dụng</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Chọn toàn văn bản hoặc tất cả các mục trong danh sách.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Cắt (loại bỏ) văn bản hoặc những mục được chọn và cho vào bảng nháp.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Chép văn bản hoặc các mục được chọn vào bảng nháp.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>Dán nội dung vào bản nháp.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>Phục hồi hành động cuối cùng.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>C</key></keyseq></p></td>
    <td><p>Copy the highlighted text or commands to the clipboard in the Terminal.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>V</key></keyseq></p></td>
    <td><p>Paste the contents of the clipboard in the Terminal.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Chụp màn hình</title>
  <tr>
    <td><p><key>Print</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Launch the screenshot tool.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Take a screenshot of a
     window.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Take a screenshot of the
    entire screen.</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Start and stop screencast
     recording.</link></p></td>
  </tr>
</table>

</page>
