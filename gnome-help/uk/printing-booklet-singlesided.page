<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-singlesided" xml:lang="uk">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Друк буклету з PDF за допомогою однобічного принтера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Друк буклету на однобічному принтері</title>

  <note>
    <p>Цими настановами можна скористатися для друку буклету з документа PDF.</p>
    <p>Якщо ви хочете надрукувати буклет з документа <app>LibreOffice</app>, спочатку експортуйте документ до PDF за допомогою пункту меню <guiseq><gui>Файл</gui><gui>Експортувати як PDF…</gui></guiseq>. У вашому документі має бути кратна до 4 кількість сторінок (4, 8, 12, 16,…). Можливо, вам доведеться додати до 3 порожніх сторінок.</p>
  </note>

  <p>Для друку виконайте такі дії:</p>

  <steps>
    <item>
      <p>Відкрийте діалогове вікно друку. Зазвичай, це можна зробити за допомогою пункту <gui style="menuitem">Друк</gui> меню або за допомогою натискання комбінації клавіш <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Властивості…</gui></p>
      <p>Переконайтеся, що у спадному списку <gui>Орієнтація</gui> вибрано варіант <gui>Альбомна</gui>.</p>
      <p>Натисніть кнопку <gui>Гаразд</gui>, щоб повернутися до діалогового вікна друку.</p>
    </item>
    <item>
      <p>У розділі <gui>Діапазон і копії</gui> виберіть <gui>Сторінки</gui>.</p>
      <p>Вкажіть номери сторінок у такому порядку (n — загальна кількість сторінок, яка є кратною до 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>…аж доки не введете усі сторінки.</p>
    <note>
      <p>Приклади:</p>
      <p>Букле з 4 сторінок: введіть <input>4,1,2,3</input></p>
      <p>Буклет з 8 сторінок: введіть <input>8,1,2,7,6,3,4,5</input></p>
      <p>Буклет з 12 сторінками: введіть <input>12,1,2,11,10,3,4,9,8,5,6,7</input></p>
      <p>Буклет з 16 сторінками: введіть <input>16,1,2,15,14,3,4,13,12,5,6,11,10,7,8,9</input></p>
      <p>Буклет з 20 сторінок: введіть <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p>
     </note>
    </item>
    <item>
      <p>Перейдіть на вкладку <gui>Параметри сторінки</gui>.</p>
      <p>У розділі <gui>Розташування</gui> виберіть <gui>Брошура</gui>.</p>
      <p>У розділі <gui>Сторони сторінки</gui> у спадному списку <gui>Включити</gui> виберіть <gui>Передні сторони / праві сторінки</gui>.</p>
    </item>
    <item>
      <p>Натисніть <gui>Надрукувати</gui>.</p>
    </item>
    <item>
      <p>Коли всі сторінки буде надруковано, переверніть стос сторінок і покладіть його до вхідного лотка принтера.</p>
    </item>
    <item>
      <p>Відкрийте діалогове вікно друку. Зазвичай, це можна зробити за допомогою пункту <gui style="menuitem">Друк</gui> меню або за допомогою натискання комбінації клавіш <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Перейдіть на вкладку <gui>Параметри сторінки</gui>.</p>
      <p>У розділі <gui>Сторони сторінки</gui> у спадному списку <gui>Включити</gui> виберіть <gui>Задні сторони / ліві сторінки</gui>.</p>
    </item>
    <item>
      <p>Натисніть <gui>Надрукувати</gui>.</p>
    </item>
  </steps>

</page>
