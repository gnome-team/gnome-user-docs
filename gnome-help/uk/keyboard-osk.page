<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="uk">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Хуліта Інка (Julita Inca)</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Користування екранною клавіатурою для введення тексту шляхом натискання кнопок за допомогою миші або сенсорної панелі.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Користування екранною клавіатурою</title>

  <p>Якщо з вашим комп'ютером не з'єднано клавіатури або ви не хочете нею користуватися, ви можете увімкнути для введення тексту <em>екранну клавіатуру</em>.</p>

  <note>
    <p>Екранну клавіатуру буде автоматично увімкнено, якщо ви скористаєтеся сенсорною панеллю</p>
  </note>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Доступність</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Доступність</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Виберіть розділ <gui>Набирання</gui>, щоб відкрити його.</p>
    </item>
    <item>
      <p>Перемкніть пункт <gui>Екранна клавіатура</gui> у позицію «увімкнено».</p>
    </item>
  </steps>

  <p>Далі, коли вам потрібно буде щось набрати, екранна клавіатура відкриватиметься у нижній частині екрана.</p>

  <p>Натисніть кнопку <gui style="button">?123</gui>, щоб ввести цифри і символи. Доступ до додаткових символів можна отримати за допомогою натискання кнопки <gui style="button">=/&lt;</gui>. Щоб повернутися до клавіатури із абеткою, натисніть кнопку <gui style="button">ABC</gui>.</p>

  <p>Ви можете натиснути кнопку <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">вниз</span></media></gui>, щоб тимчасово приховати клавіатуру. Клавіатуру буде автоматично показано знову, коли ви далі натиснете на щось, де її можна використати. Крім того, на сенсорному екрані ви можете витягнути клавіатуру, <link xref="touchscreen-gestures">потягнувши екран від нижнього краю</link>.</p>
  <p>Натисніть кнопку <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">прапорець</span></media></gui> для зміни параметрів <link xref="session-language">Мова</link> або <link xref="keyboard-layouts">Джерела введення</link>.</p>

</page>
