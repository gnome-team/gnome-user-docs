<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-testing" xml:lang="uk">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-gettingprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>
    
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Користування тестовими профілями для перевірки того, чи було належним чином застосовано профілі до вашого екрана.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Як перевірити, чи працює керування кольорами належним чином?</title>

  <p>Результат застосування профілю кольорів іноді є ледве вловимим — може бути складно помітити зміни.</p>

  <p>Система постачається із декількома профілями для тестування. Застосування цих профілів дає дуже помітні зміни:</p>

  <terms>
    <item>
      <title>Синій</title>
      <p>Робить колір екрана синім. Призначено для перевірки, чи надсилаються криві калібрування на дисплей.</p>
    </item>
<!--    <item>
      <title>ADOBEGAMMA-test</title>
      <p>This will turn the screen pink and tests different features of a
      screen profile.</p>
    </item>
    <item>
      <title>FakeBRG</title>
      <p>This will not change the screen, but will swap around the RGB channels
      to become BGR. This will make all the colors gradients look mostly
      correct, and there won’t be much difference on the whole screen, but
      images will look very different in applications that support color
      management.</p>
    </item>-->
  </terms>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Колір</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Виберіть пристрій, для якого ви хочете додати профіль. Варто зауважити, який профіль використано.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Додати профіль</gui>, щоб вибрати тестовий профіль, який має бути у нижній частині списку.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Додати</gui>, щоб підтвердити ваш вибір.</p>
    </item>
    <item>
      <p>Щоб повернутися до попереднього профілю, виберіть пристрій на панелі <gui>Колір</gui>, потім виберіть профіль, яким ви користувалися раніше, до того, як спробувати один із текстових профілів, і натисніть кнопку <gui>Увімкнути</gui>, щоб скористатися ним знову.</p>
    </item>
  </steps>


  <p>За допомогою цих профілів ви зможете без проблем помітити, чи передбачено у програмі підтримку керування кольорами.</p>

</page>
