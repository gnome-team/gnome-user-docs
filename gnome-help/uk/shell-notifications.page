<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="40" date="2021-09-10" status="review"/>

    <credit type="author">
      <name>Марина Журахінська</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Панель повідомлень випадає з верхнього краю екрана і повідомляє вас про настання певних подій.</desc> 
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Сповіщення і список сповіщень</title>

<section id="what">
  <title>Що таке сповіщення?</title>

  <p>Якщо програмі або компоненту системи потрібно буде привернути вашу увагу, у верхній частині екрана буде або на екрані блокування буде показано сповіщення.</p>

  <p>Наприклад, якщо вам надійшло повідомлення або нова пошта, ви побачите сповіщення про це. Сповіщення щодо служб обміну повідомленнями підлягають особливій обробці — їх буде показано за окремими контактами, які надсилали вам повідомлення.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>Для інших сповіщень буде показано кнопки параметрів. Щоб закрити одне з таких сповіщень без вибору його параметрів, натисніть кнопку закриття.</p>

  <p>Натискання кнопки закриття для деяких сповіщень призводить до їхнього остаточного вилучення. Інші ж сповіщення, зокрема сповіщення Rhythmbox або ваших програм для спілкування, лишатимуться після закриття прихованими у списку сповіщень.</p>

</section>

<section id="notificationlist">

  <title>Список сповіщень</title>

  <p>За допомогою списку сповіщень ви можете будь-коли повернутися до перегляду сповіщень. Список можна відкрити клацанням на годиннику або натисканням комбінації клавіш <keyseq><key xref="keyboard-key-super">Super</key><key>V</key></keyseq>. У списку сповіщень буде показано усі сповіщення, на які ви не відреагували, або сповіщення, які закріплено у списку.</p>

  <p>Переглянути сповіщення можна клацанням на його пункті у списку. Ви можете закрити список сповіщень повторним натисканням комбінації клавіш <keyseq><key>Super</key><key>V</key></keyseq> або натисканням клавіші <key>Esc</key>.</p>

  <p>Натисніть кнопку <gui>Очистити список</gui>, щоб спорожнити список сповіщень.</p>

</section>

<section id="hidenotifications">

  <title>Приховування сповіщень</title>

  <p>Якщо ви працюєте над чимось і не хочете відволікатися, ви можете вимкнути сповіщення.</p>

  <p>Ви можете приховати усі сповіщення — відкрийте список сповіщень і увімкніть пункт <gui>Не турбувати</gui> у нижній його частині. Інший варіант:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Клацніть на пункті <gui>Сповіщення</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Увімкніть пункт <gui>Не турбувати</gui>.</p>
    </item>
  </steps>

  <p>Коли увімкнено пункт <gui>Не турбувати</gui>, система показуватиме на екрані контекстні панелі лише дуже важливих сповіщень, зокрема сповіщення про критичний рівень заряду батареї. З повним переліком сповіщеннями можна буде ознайомитися у списку сповіщень, коли його буде відкрито (клацанням на годиннику або натисканням комбінації клавіш <keyseq><key>Super</key><key>V</key></keyseq>). Сповіщення знову почнуть з'являтися, коли ви знову увімкнете перемикач.</p>

  <p>Ви також можете вимкнути або повторно увімкнути сповіщення для окремих програм за допомогою панелі <gui>Сповіщення</gui>.</p>

</section>

<section id="lock-screen-notifications">

  <title>Приховування сповіщень на екрані блокування</title>

  <p>Коли екран заблоковано, сповіщення буде показано на екрані блокування. Ви можете налаштувати екран блокування для приховування цих сповіщень з міркувань конфіденційності.</p>

  <steps>
    <title>Щоб вимкнути сповіщення, коли екран вашої системи заблоковано, виконайте такі дії:</title>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Клацніть на пункті <gui>Сповіщення</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item><p>Зніміть позначку з пункту <gui>Сповіщення на заблокованому екрані</gui>.</p></item>
  </steps>

</section>

</page>
