<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="uk">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Аніта Райтере (Anita Reitere)</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Перевірте скільки лишилося чорнила чи тонера у картриджах принтера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Як перевірити рівні чорнил або тонера принтера?</title>

  <p>Спосіб визначення залишкового рівня чорнила або тонера у вашому принтері залежить від моделі і виробника принтера, а також драйверів та програм, які встановлено у системі.</p>

  <p>На деяких принтерах передбачено вбудований екран для показу рівнів чорнил та іншої інформації.</p>

  <p>Деякі принтери повідомляють про рівні тонера або чорнила комп'ютеру. Ці дані можна знайти на панелі <gui>Принтери</gui> програми <app>Параметри</app>. Рівень чорнил буде показано разом із іншими параметрами принтера, якщо такі дані доступні.</p>

  <p>Драйвери та програми для керування станом більшості принтерів HP постачаються проєктом HP Linux Imaging and Printing (HPLIP). Інші виробники можуть постачати закриті драйвери із подібними можливостями.</p>

  <p>Крім того, ви можете встановити програму для перевірки і стеження за рівнями чорнил. <app>Inkblot</app> показує стан картриджів для багатьох принтерів HP, Epson та Canon. Розробниками програми створено спеціальний <link href="http://libinklevel.sourceforge.net/#supported">список підтримуваних моделей</link>. Іншою програмою для стеження за рівнями чорнил для принтерів Epson та деяких інших виробників є <app>mtink</app>.</p>

  <p>Підтримка деяких принтерів у Linux ще далека від досконалості, а інші просто не можуть повідомляти про рівень чорнила.</p>

</page>
