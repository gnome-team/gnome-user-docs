<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="uk">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ви можете зберегти параметри (зокрема пароль) з'єднання мережі так, щоб будь-хто, хто користується комп'ютером, міг встановити з'єднання.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Інші користувачі не можуть з'єднатися із інтернетом</title>

  <p>Після налаштування з'єднання мережею, зазвичай, ним можуть скористатися усі інші користувачі вашого комп'ютера. Якщо дані з'єднання не надаються у спільне користування, вам слід перевірити параметри з'єднання.</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Мережа</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Мережа</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Виберіть <gui>Wi-Fi</gui> зі списку ліворуч.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">параметри</span></media>, щоб відкрити панель параметрів з'єднання.</p>
    </item>
    <item>
      <p>Виберіть <gui>Засвідчення</gui> на панелі ліворуч.</p>
    </item>
    <item>
      <p>У нижній частині панелі <gui>Засвідчення</gui> позначте пункт <gui>Зробити доступним для інших користувачів</gui>, щоб дозволити користувачам користуватися з'єднанням мережею.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui style="button">Застосувати</gui>, щоб зберегти зміни.</p>
    </item>
  </steps>

  <p>Після цього інші користувачі комп'ютера зможуть скористатися цим з'єднанням без введення будь-яких додаткових даних.</p>

  <note>
    <p>Цей параметр може змінити будь-який користувач.</p>
  </note>

</page>
