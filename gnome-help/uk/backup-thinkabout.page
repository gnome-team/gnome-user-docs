<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="uk">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.37.1" date="2020-07-30" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Список тек, де можна знайти документи, файли і налаштування, резервні копії яких варто створити.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Де шукати файли, резервні копії яких слід створити?</title>

  <p>Вибір файлів для резервного копіювання та пошук цих файлів є найскладнішим кроком при спробі створити резервну копію. Нижче наведено найтиповіші місця, у яких зберігаються важливі файли і параметри, резервні копії яких варто створити.</p>

<list>
 <item>
  <p>Особисті файли (документи, музика, фотографії і відео)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs   and need to be translated.  You can find the correct translations for your   language here: http://translationproject.org/domain/xdg-user-dirs.html">Ці файли, зазвичай, зберігаються у вашій домашній теці (<file>/home/ім'я_вашого_користувача</file>). Вони можуть зберігатися у підтеках, зокрема <file>Стільниця</file>, <file>Документи</file>, <file>Картинки</file>, <file>Музика</file> і <file>Відео</file>.</p>
  <p>Якщо на вашому носії для резервних копій достатньо місця (якщо це, наприклад, зовнішній диск), варто виконувати резервне копіювання усієї домашньої теки. Дізнатися, скільки місця займає ваша домашня тека можна за допомогою <app>Аналізатора використання диска</app>.</p>
 </item>

 <item>
  <p>Приховані файли</p>
  <p>Будь-який файл або тека, назва якої починається з крапки (.), типово є прихованою. Щоб переглянути приховані файли, натисніть кнопку меню на бічній панелі вікна програми <app>Файли</app> і натисніть пункт <gui>Показати приховані файли</gui>, або натисніть комбінацію клавіш <keyseq><key>Ctrl</key><key>H</key></keyseq>. Ви можете скопіювати приховані файли до теки резервної копії подібно до будь-яких інших файлів.</p>
 </item>

 <item>
  <p>Особисті параметри (налаштування стільниці, теми та параметри роботи програмного забезпечення)</p>
  <p>Більшість програм зберігають свої налаштування у прихованих теках у домашній теці вашого користувача (див. вище про приховані файли).</p>
  <p>Налаштування більшості програм буде збережено у прихованих теках <file>.config</file> і <file>.local</file> у домашній теці вашого користувача.</p>
 </item>

 <item>
  <p>Загальносистемні параметри</p>
  <p>Параметри важливих частин системи зберігаються не у вашій домашній теці. Ці параметри можуть зберігатися у декількох місцях, але більша їхня частина зберігається у теці <file>/etc</file>. Загалом кажучи, потреби у резервному копіюванні цих файлів на домашньому комп'ютері немає. Втім, якщо ви працюєте із сервером, вам слід створити резервні копії файлів для служб, які на ньому запущено.</p>
 </item>
</list>

</page>
