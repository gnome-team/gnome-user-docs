<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="uk">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>Девід Кінг (David King)</name>
      <email>davidk@gnome.org</email>
      <years>2014–2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Уможливлення вивантаження даних на ваш комп'ютер за допомогою Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Керування оприлюдненням даних за допомогою Bluetooth</title>

  <p>Ви можете увімкнути <gui>Bluetooth</gui> для отримання файлів за допомогою Bluetooth до теки <file>Завантаження</file></p>

  <steps>
    <title>Уможливлення оприлюднення файлів до вашої теки <file>Завантаження</file></title>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Bluetooth</gui></p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Bluetooth</gui> для відкриття панелі.</p>
    </item>
    <item>
      <p>Переконайтеся, що перемикач <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> перебуває у стані «увімкнено»</link>.</p>
    </item>
    <item>
      <p>Пристрої із увімкненим Bluetooth можуть надсилати файл до вашої теки <file>Завантаження</file>, лише коли відкрито панель <gui>Bluetooth</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Ви можете <link xref="about-hostname">змінити</link> назву, яку ваш комп'ютер показуватиме іншим пристроям.</p>
  </note>

</page>
