<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-firewall-on-off" xml:lang="uk">

  <info>
    <link type="guide" xref="net-security" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-03" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ви можете керувати тим, які програми матимуть доступ до мережі. Це допомагає підтримувати безпеку комп'ютера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Вмикання доступу крізь брандмауер та заборона доступу</title>

  <p>До складу GNOME не включено програми-брандмауера. Додаткову підтримку, поза цим документом, можна отримати від команди супроводу дистрибутива або IT-відділу вашої установи або компанії. У вашій системі має бути <em>брандмауер</em> або мережевий екран, який надаватиме системі змогу блокувати програми від доступу сторонніх користувачів з інтернету або локальної мережі. Брандмауери допомагають захистити комп'ютер.</p>

  <p>Інтернет-з'єднанням можуть користуватися багато програм. Наприклад, ви можете оприлюднювати файли або надавати комусь змогу переглядати зображення з вашого дисплея віддалено, якщо комп'ютер з'єднано з мережею. Залежно від налаштувань вашого комп'ютера, у вас може виникнути потреба у коригуванні роботи брандмауера так, щоб відповідні можливості працювали належним чином.</p>

  <p>Кожна програма, яка послуговується мережею, використовує певний <em>мережевий порт</em>. Щоб інші комп'ютери у мережі могли отримувати доступ до певної служби, вам, можливо, доведеться «відкрити» пов'язаний зі службою порт у брандмауері:</p>


  <steps>
    <item>
      <p>Перейдіть до панелі <gui>Діяльності</gui>, кнопку якої розташовано у верхньому лівому куті екрана і запустіть вашу програму-брандмауер. Можливо, вам доведеться встановити програму для керування брандмауером (наприклад, GUFW) власноруч, якщо її ще не встановлено.</p>
    </item>
    <item>
      <p>Відкрийте або вимкніть порт для вашої мережевої служби, залежно від того, хочете ви надати користувачам мережі доступ до відповідного порту, чи ні. Номер порту, до параметрів якого вам слід внести зміни, <link xref="net-firewall-ports">залежить від служби</link>.</p>
    </item>
    <item>
      <p>Збережіть або застосуйте внесені зміни, виконуючи додаткові настанови, які показуватиме програма налаштовування брандмауера.</p>
    </item>
  </steps>

</page>
