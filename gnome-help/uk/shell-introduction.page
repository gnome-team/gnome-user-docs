<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision version="gnome:44" date="2023-12-29" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Андре Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Візуальний огляд вашої стільниці, верхньої панелі та огляду <gui>Діяльності</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Візуальний огляд GNOME</title>

  <p>У GNOME реалізовано новітній інтерфейс користувача, який не заважає вам діяти, не відволікає вас і допомагає досягти бажаних результатів. Після першого входу до системи ви побачите огляд <gui>Активності</gui> і верхню панель.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Верхня панель GNOME Shell</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Верхня панель GNOME Shell</p>
    </media>
  </if:when>
</if:choose>

  <p>Верхня панель надає доступ до ваших вікон і програм, вашого календаря і записів зустрічей та <link xref="status-icons">властивостей системи</link>, зокрема звуку, мережі та живлення. За допомогою меню системи на верхній панелі ви можете змінити гучність та параметри яскравості екрана, редагувати параметри з'єднання <gui>Wi-Fi</gui>, перевірити стан акумулятора, вийти з облікового запису або перемкнути користувача, а також вимкнути ваш комп'ютер.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title>Огляд <gui>Діяльності</gui></title>

  <p if:test="!platform:gnome-classic">Після запуску GNOME вам автоматично буде показано огляд <gui>Діяльності</gui>. За допомогою панелі огляду ви зможете отримати доступ до ваших вікон і програм. Ви також можете просто почати вводити критерій пошуку для пошуку програм, файлів тек та сторінок інтернету.</p>

  <p if:test="!platform:gnome-classic">Щоб будь-коли відкрити огляд, натисніть кнопку <gui>Діяльності</gui> у верхньому лівому куті або просто пересуньте вказівник миші у верхній лівий активний кут екрана. Ви також можете натиснути клавішу <key xref="keyboard-key-super">Super</key> на клавіатурі.</p>

  <p if:test="platform:gnome-classic">Щоб отримати доступ до вікон і програм, натисніть кнопку у нижньому лівому куті екрана у списку вікон. Ви також можете натиснути клавішу <key xref="keyboard-key-super">Super</key>, щоб переглянути панель із інтерактивними мініатюрами усіх вікон у поточному робочому просторі.</p>

  <media type="image" its:translate="no" src="figures/shell-activities-dash.png" height="65" style="floatend floatright" if:test="!target:mobile, !platform:gnome-classic">
    <p>Activities button and Dash</p>
  </media>
  <p if:test="!platform:gnome-classic">У нижній частині огляду ви побачите <em>панель приладів</em>. На панелі приладів буде показано улюблені та запущені програми. Натисніть будь-яку піктограму на панелі приладів, щоб відкрити вікно відповідної програми. Якщо програму вже запущено, під її піктограмою буде показано невеличку крапку. Натискання піктограми відкриє те вікно програми, яким користувалися востаннє. Ви також можете перетягнути піктограму програми на робочий простір.</p>

  <p if:test="!platform:gnome-classic">Клацання правою кнопкою миші на піктограмі відкриває меню, за допомогою якого ви можете вибрати будь-яке вікно запущеної програми або відкрити нове вікно. Ви також можете клацнути на піктограмі, утримуючи натиснутою клавішу <key>Ctrl</key>, щоб відкрити нове вікно.</p>

  <p if:test="!platform:gnome-classic">Після входу на панель огляду спочатку ви побачите огляд вікон. На цій панелі буде показано інтерактивні мініатюри усіх вікон на поточному робочому просторі.</p>

  <p if:test="!platform:gnome-classic">Натисніть кнопку таблиці (кнопку із дев'ятьма крапками) на панелі приладів, щоб оглянути доступні програми. Вам буде показано пункти усіх програм, які встановлено у вашій системі. Клацніть на пункті будь-якої програми, щоб запустити її, або перетягніть пункт програми на робочий простір, який показано над списком встановлених програм. Ви також можете перетягнути пункт програми на панель приладів, щоб зробити програму улюбленою. Ваші улюблені програми лишатимуться на панелі приладів, навіть якщо їх не запущено, для пришвидшення доступу до них.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Докладніше про запуск програм.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Докладніше про вікна і робочі простори.</link></p>
    </item>
  </list>

</section>

<section id="clock">
  <title>Годинник, календар і зустрічі</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Годинник, календар, зустрічі і сповіщення</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Годинник, календар та зустрічі</p>
    </media>
  </if:when>
</if:choose>

  <p>Клацніть на годиннику на верхній панелі, щоб переглянути поточну дату, календар на місяць, писок ваших записів зустрічей та нові сповіщення. Ви також можете відкрити календар натисканням комбінації клавіш <keyseq><key>Super</key><key>V</key></keyseq>. Безпосередньо з меню ви можете отримати доступ до параметрів дати і часу і відкрити повноцінну програму для керування календарем.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Докладніше про календар та зустрічі.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Докладніше про сповіщення і список сповіщень.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Меню системи</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Меню користувача</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Меню користувача</p>
    </media>
  </if:when>
</if:choose>

  <p>Натисніть кнопку меню системи у верхньому правому куті, щоб керувати параметрами роботи вашої системи та вашого комп'ютера. У верхній частині меню буде показано індикатор стану акумулятора і кнопки для запуску програми «Параметри» та інструмента створення знімків екрана. За допомогою кнопки <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
  power</media> ви зможете приспати або вимкнути комп'ютер, а також швидко надати комусь доступ до комп'ютера без повного входу до системи. За допомогою повзунків ви зможете керувати гучністю відтворення звуків та яскравістю екрана.</p>

  <p>Решта пунктів меню складається з кнопок «Швидкого налаштовування», за допомогою яких ви зможете швидко керувати доступними службами та пристроями, зокрема Wi-Fi, Bluetooth, параметрами живлення та фоновими програмами.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Докладніше про перемикання користувачів, вихід із системи та вимикання вашого комп'ютера.</link></p>
    </item>
    <item>
      <p><link xref="shell-lockscreen">Докладніше про швидке налаштовування.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Блокування екрана</title>

  <p>Після блокування вами екрана або автоматичного блокування система показує екран блокування. Окрім захисту вашої стільниці, доки ви відійшли від комп'ютера, на екрані блокування буде показано дату і час. Також буде показано відомості щодо стану акумулятора та роботи мережі.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Докладніше про блокування екрана.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Список вікон</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>У GNOME реалізовано підхід до перемикання вікон, який є відмінним від увесь час показаного списку видимих вікон інших стільничних середовищ. Цей підхід надає вам змогу зосередитися на поточному завданні і не відволікатися.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Докладніше про перемикання вікон.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Список вікон</p>
    </media>
    <p>За допомогою списку вікон у нижній частині екрана можна отримати доступ до усіх відкритих вікон і програм та швидко мінімізовувати та відновлювати вікна.</p>
    <p>Праворуч від списку вікон GNOME показує чотири робочих простори. Для перемикання на інший робочий простір виберіть пункт потрібного вам робочого простору.</p>
  </if:when>
</if:choose>

</section>

</page>
