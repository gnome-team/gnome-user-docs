<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Андре Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Користування засобом вибору робочого простору.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

 <title>Аплет перемикає виводить між робочими областями</title>

 <steps>  
 <title>Використання миші:</title>
 <item>
  <p if:test="!platform:gnome-classic">Відкрийте панель огляду <gui xref="shell-introduction#activities">Діяльності</gui>.</p>
  <p if:test="platform:gnome-classic">У нижньому правому куті екрана натисніть пункт одного з чотирьох робочих просторів, щоб активувати цей робочий простір.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Клацніть на робочому просторі, який часткового показано праворуч від поточного робочого простору, щоб переглянути відкриті вікна на наступному робочому просторі. Якщо поточний позначений робочий простір є найлівішим на панелі, клацніть на пункті робочого простору, який частково показано ліворуч від нього, щоб переглянути попередній робочий простір.</p>
  <p>Якщо використовується декілька робочих просторів, ви також можете клацнути на панелі <link xref="shell-workspaces">вибору робочого простору</link> між полем пошуку і списком вікон для безпосереднього доступу до іншого робочого простору.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Клацніть на робочому просторі, щоб активувати робочий простір.</p>
 </item>
 </steps>
 
 <list>
 <title>Використання клавіатури:</title>  
  <item>
    <p if:test="!platform:gnome-classic">Натисніть комбінацію клавіш <keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq> або <keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq>, щоб перейти до робочого простору, який показано ліворуч від поточного робочого простору на панелі вибору робочого простору.</p>
    <p if:test="platform:gnome-classic">Натисніть комбінацію клавіш <keyseq><key>Ctrl</key> <key>Alt</key><key>←</key></keyseq>, щоб перейти до робочого простору, який показано ліворуч від поточного робочого простору на панелі <em>вибору робочого простору</em>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Натисніть комбінацію клавіш <keyseq><key>Super</key><key>Page Down</key></keyseq> або <keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq>, щоб перейти до робочого простору, який показано праворуч від поточного робочого простору на панелі вибору робочого простору.</p>
    <p if:test="platform:gnome-classic">Натисніть <keyseq><key>Ctrl</key> <key>Alt</key><key>→</key></keyseq>, щоб перейти до робочого простору, який показано праворуч від поточного робочого простору на <em>засобі вибору робочого простору</em>.</p>
  </item>
 </list>

</page>
