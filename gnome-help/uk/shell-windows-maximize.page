<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Двічі клацніть або перетягніть смужку заголовка для максимізації або відновлення розміру вікна.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Максимізація і скасування максимізації вікна</title>

  <p>Ви можете максимізувати вікно, щоб воно зайняло увесь простір вашої стільниці, і скасувати максимізацію вікна для відновлення його звичайного розміру. Ви також можете максимізувати вікна вертикально уздовж лівого або правого країв екрана, щоб можна було без проблем переглядати два вікна одразу. Див. розділ <link xref="shell-windows-tiled"/>, щоб дізнатися більше.</p>

  <p>Щоб максимізувати вікно, захопіть його смужку заголовка і перетягніть її до верхнього краю екрана або просто двічі клацніть на смужці заголовка. Щоб максимізувати вікно за допомогою клавіатури, утримуйте натиснутою клавішу <key xref="keyboard-key-super">Super</key> і натискайте клавішу <key>↑</key> або клавіші <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Крім того, ви можете розгорнути вікно клацанням на кнопці розгортання на смужці заголовка вікна.</p>

  <p>Щоб відновити вікно до немаксимізованого розміру, перетягніть його від країв екрана. Якщо вікно буде повністю максимізовано, ви можете двічі клацнути на смужці заголовка, щоб відновити його початкові розміри. Ви можете також скористатися тими самими клавіатурними скороченнями, якими ви скористалися для максимізації вікна.</p>

  <note style="tip">
    <p>Натисніть і утримуйте клавішу <key>Super</key> і перетягніть вікно за будь-яку точку, щоб пересунути його.</p>
  </note>

</page>
