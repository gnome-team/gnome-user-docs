<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="uk">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Вибір принтера, яким ви користуватиметься найчастіше.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Встановлення типового принтера</title>

  <p>Якщо у системі доступними є декілька принтерів, ви можете вибрати той з них, який буде типовим. Таким принтером можна вибрати принтер, яким ви користуєтеся найчастіше.</p>

  <note>
    <p>Для встановлення типового принтера вам знадобляться <link xref="user-admin-explain">адміністративні права доступу</link> до системи.</p>
  </note>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Принтери</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Принтери</gui>.</p>
    </item>
    <item>
      <p>Залежно від вашої системи, вам, можливо, доведеться натиснути кнопку <gui style="button">Розблокувати</gui> у верхньому правому куті і ввести ваш пароль, коли вас про це попросять.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">параметри</span></media>, яку розташовано поряд із пунктом принтера.</p>
    </item>
    <item>
      <p>Позначте пункт <gui style="menuitem">Використовувати як типовий принтер</gui>.</p>
    </item>
  </steps>

  <p>При друці з програми буде автоматично використано типовий принтер, якщо ви не виберете інший.</p>

</page>
