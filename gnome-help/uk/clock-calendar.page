<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="clock-calendar" xml:lang="uk">

  <info>
    <link type="guide" xref="clock"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.16" date="2015-03-02" status="outdated"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Показ ваших зустрічей в області календаря у верхній частині екрана.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

 <title>Зустрічі у календарі</title>

  <note>
    <p>Для цього вам слід користуватися календарем з <app>Evolution</app> або <app>Календарем</app> чи мати налаштований обліковий запис, підтримку якого передбачено у <gui>Календарі</gui>.</p>
    <p>У більшості дистрибутивів встановлено принаймні одну з цих програм. Якщо у вашому дистрибутиві ці програми не встановлено, вам слід встановити одну з них за допомогою програми для керування пакунками дистрибутива.</p>
 </note>

  <p>Щоб переглянути список ваших зустрічей, виконайте такі дії:</p>
  <steps>
    <item>
      <p>Клацніть на годиннику на верхній панелі.</p>
    </item>
    <item>
      <p>Клацніть на пункті дати, для якої ви хочете переглянути зустрічі з календаря.</p>

    <note>
       <p>Під пунктом кожної з дат, на яку призначено зустріч, буде показано крапку.</p>
    </note>

      <p>Наявні записи зустрічей буде показано ліворуч від календаря. Після додавання записів зустрічей до вашого календаря <app>Evolution</app> або програми <app>Календар</app> ці записи з'являться у списку зустрічей годинника.</p>
    </item>
  </steps>

 <if:choose>
 <if:when test="!platform:gnome-classic">
 <media type="image" src="figures/shell-appts.png" width="500">
  <p>Годинник, календар та зустрічі</p>
 </media>
 </if:when>
 <if:when test="platform:gnome-classic">
 <media type="image" src="figures/shell-appts-classic.png" width="373" height="250">
  <p>Годинник, календар та зустрічі</p>
 </media>
 </if:when>
 </if:choose>

</page>
