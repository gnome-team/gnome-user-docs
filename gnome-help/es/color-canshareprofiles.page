<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="es">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Compartir perfiles de color nunca es una buena idea ya que el hardware cambia a lo largo del tiempo.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Se puede compartir un perfil de color?</title>

  <p>Los perfiles de color que ha creado usted mismo son específicos del hardware y de las condiciones de luz para las que lo ha calibrado. Una pantalla que ha estado encendida unos pocos cientos de horas va a tener un perfil de color muy diferente que un dispositivo similar con un número de serie próximo, si ésta última ha estado encendida unas mil horas.</p>
  <p>Esto significa que si comparte su perfil de color con alguien, puede que esté <em>mejorando</em> su calibración, pero es incorrecto decir que su dispositivo está calibrado.</p>
  <p>De forma similar, a menos que todo el mundo tenga el control de luz recomendado (sin luz solar de ventanas, paredes negras, bombillas de día, etc.= en una habitación donde se editan imágenes, compartir un perfil que ha creado bajo sus propias condiciones de luz específicas no tiene mucho sentido.</p>

  <note style="warning">
    <p>Debería comprobar cuidadosamente las condiciones de redistribución de los perfiles descargados de las páginas web de los fabricantes o que se crearon en su nombre.</p>
  </note>

</page>
