<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="es">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision version="gnome:44" date="2023-12-30" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aprender cómo salir de su cuenta de usuario, cerrando la sesión, cambiando de usuario, etc.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cerrar la sesión, apagar o cambiar de usuario</title>

  <p>Cuando haya terminado de usar su equipo, puede apagarlo, suspenderlo (para ahorrar energía), o dejarlo encendido y cerrar la sesión.</p>

<section id="logout">
  <info>
    <link type="seealso" xref="user-add"/>
  </info>

  <title>Cerrar la sesión o cambiar de usuario</title>

  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-exit-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menú del usuario</p>
      </media>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-exit-classic-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menú del usuario</p>
      </media>
    </if:when>
  </if:choose>

  <p>Para permitir que otros usuarios usen su equipo, puede salir de la sesión, o seguir conectado y simplemente cambiar de usuario. Si cambia de usuario, todas las aplicaciones seguirán funcionando y todo estará donde lo dejó cuando vuelva a iniciar sesión.</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p>Las opciones <gui>Salir de la sesión</gui> y <gui>Cambiar de usuario</gui>, sólo aparecen en el menú si tiene más de una cuenta de usuario en su sistema.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>La opción <gui>Cambiar de usuario</gui>, sólo aparece en el menú si tiene más de una cuenta de usuario en su sistema.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Bloquear la pantalla</title>

  <p>Si deja su equipo durante un breve periodo de tiempo, debe bloquear la pantalla para evitar que otras personas tengan acceso a sus archivos o ejecuten aplicaciones. Cuando vuelva, suba el panel <link xref="shell-lockscreen">bloquear pantalla</link> e introduzca su contraseña para volver a iniciar la sesión. Si no bloquea su pantalla, se bloqueará automáticamente después de cierto tiempo.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and click the
    <media type="image" its:translate="no" src="figures/system-lock-screen-symbolic.svg">
      Lock
    </media>
  button.</p>

  <p>Cuando su pantalla está bloqueada otros usuarios pueden iniciar sesión en sus propias cuentas pulsando <gui>Iniciar sesión como otro usuario</gui> en la parte inferior de la pantalla de inicio de sesión. Puede volver a su escritorio cuando hayan terminado.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Suspender</title>

  <p>Para ahorrar energía, suspenda su equipo cuando no lo esté usando. Si usa un equipo portátil, el sistema suspende su equipo automáticamente cuando cierra su tapa. Esto guarda su estado en la memoria de su equipo y apaga la mayor parte de las funciones de su equipo. Durante la suspensión se sigue usando una cantidad muy pequeña de energía.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select <gui>Suspend</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Apagar o reiniciar</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>Si hay otros usuarios conectados, no podrá apagar o reiniciar el equipo, porque esto cerraría sus sesiones. Si es un usuario administrador, se le pedirá su contraseña para apagar.</p>

  <note style="tip">
    <p>Puede querer apagar el equipo si quiere moverlo y no tiene una batería, o si su batería está baja o no se carga bien. Un equipo apagado también usa <link xref="power-batterylife">menos energía</link> que uno que está suspendido.</p>
  </note>

</section>

</page>
