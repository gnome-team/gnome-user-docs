<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="es">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Los equipos, en general, se calientan pero pueden sobrecalentarse, lo que puede causar daños.</desc>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Mi equipo se calienta demasiado</title>

<p>La mayoría de los equipos se calientan después de un tiempo, y algunos pueden llegar a estar muy calientes. Esto es normal: simplemente es parte de la forma en la que el equipo se refrigera. Sin embargo, si el equipo se pone muy caliente podría ser una señal de sobrecalentamiento, lo que potencialmente puede causar daños.</p>

<p>La mayoría de los portátiles se calientan razonablemente una vez que se han estado usando durante un tiempo. Por lo general no es nada preocupante. Los equipos producen una gran cantidad de calor y los portátiles son muy compactos, por lo que necesitan que se caliente su cubierta externa para eliminar el calor rápidamente. Algunos portátiles se calientan demasiado y esto puede hacerlos incómodos de usar. Este es normalmente el resultado de un sistema de refrigeración mal diseñado. A veces se pueden conseguir accesorios adicionales de refrigeración que se ajustan a la parte inferior del equipo portátil y proporcionan una refrigeración más eficiente.</p>

<p>Si tiene un equipo de escritorio que se nota caliente al tacto, puede que tenga una refrigeración insuficiente. Si esto le preocupa, puede comprar más ventiladores o comprobar que los ventiladores y rejillas de ventilación estén libres de polvo y otras obstrucciones. Es posible que quiera considerar la posibilidad de tener el equipo en un lugar mejor ventilado; si está en un espacio cerrado (por ejemplo, en un armario), el sistema de refrigeración puede no ser capaz de eliminar el calor y hacer circular el aire fresco con suficiente rapidez.</p>

<p>Some people are concerned about the health risks of using hot laptops. There
are suggestions that prolonged use of a hot laptop on your lap might possibly
reduce fertility, and there are reports of minor burns being suffered
too (in extreme cases). If you are concerned about these potential problems,
you may wish to consult a medical practitioner for advice. Of course, you can
simply choose not to rest the laptop on your lap.</p>

<p>La mayoría de los equipos modernos se apagan solos si se calientan demasiado, con el fin de evitar que se produzcan daños. Si el equipo se mantiene apagado puede ser por esto. Si su equipo se ha sobrecalentando, probablemente tendrá que repararlo.</p>

</page>
