<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="guide" style="a11y task" id="a11y" xml:lang="es">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Usar tecnologías de asistencia para ayudar con necesidades especiales de visión, audición y movilidad.</desc>
    <uix:thumb role="experimental-gnome-tiles" src="figures/tile-a11y.svg"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Accesibilidad</title>

  <p>El sistema incluye tecnologías de asistencia para dar soporte a usuarios con diversas deficiencias y necesidades especiales y para interactuar con dispositivos de asistencia comunes. Se puede añadir un menú de accesibilidad a la barra superior, para simplificar el acceso a muchas de las características de accesibilidad.</p>

  <section id="vision">
    <title>Deficiencias visuales</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Ceguera</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Visión deficiente</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Daltonismo</title>
    </links>
    <links type="topic" style="linklist">
      <title>Otros temas</title>
    </links>
  </section>

  <section id="sound">
    <title>Deficiencias auditivas</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Deficiencias motoras</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Movimiento del ratón</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Pulsar y arrastrar</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Uso del teclado</title>
    </links>
    <links type="topic" style="linklist">
      <title>Otros temas</title>
    </links>
  </section>
</page>
