<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="es">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cuando elimina un archivo, normalmente se envía a la papelera, pero es posible recuperarlo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Recuperar un archivo de la papelera</title>

  <p>Si elimina un archivo con el gestor de archivos, normalmente se envía a la <gui>Papelera</gui>, y debería poder recuperarlo.</p>

  <steps>
    <title>Para restaurar un archivo de la papelera:</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Archivos</gui>.</p>
    </item>
    <item>
      <p>Pulse en <app>Archivos</app> para abrir el gestor de archivos.</p>
    </item>
    <item>
      <p>Click <gui>Trash</gui> in the sidebar. If you do not see the sidebar,
      press the <gui>Show Sidebar</gui> button in the top-left corner of the window.</p>
    </item>
    <item>
      <p>If your deleted file is there, click on it and select
      <gui>Restore From Trash</gui>. It will be restored to the folder from where it was
      deleted.</p>
    </item>
  </steps>

  <p>Si eliminó el archivo pulsando <keyseq><key>Mayús</key><key>Supr</key></keyseq> o desde la línea de comandos, el archivo no se podrá recuperar usando este método porque se habrá eliminado permanentemente. Los archivos eliminados permanentemente no se pueden recuperar de la <gui>Papelera</gui>.</p>

  <p>Existen varias herramientas de recuperación disponibles que a veces pueden recuperar archivos que se eliminaron permanentemente. No obstante, normalmente no son muy fáciles de usar. Si eliminó un archivo permanentemente de forma accidental, probablemente sea mejor pedir ayuda en los foros de soporte para ver si es posible recuperarlo.</p>

</page>
