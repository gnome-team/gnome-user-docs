<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="es">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use contraseñas más largas y complicadas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Elegir una contraseña segura</title>

  <note style="important">
    <p>Asegúrese de que sus contraseñas sean sencillas de recordar, pero que sean difíciles de adivinar para otros (incluyendo programas de computación).</p>
  </note>

  <p>Elegir una buena contraseña le ayudará a mantener su equipo seguro. Si su contraseña es fácil de adivinar, alguien puede hacerlo y obtener acceso a toda su información personal.</p>

  <p>Las personas pueden incluso usar equipos para intentar sistemáticamente adivinar su contraseña, de tal forma que incluso una que fuese difícil adivinar a un humano, puede ser extremadamente fácil de obtener para un programa. Aquí tiene algunos consejos para elegir una buena contraseña:</p>
  
  <list>
    <item>
      <p>Use una mezcla de letras mayúsculas y minúsculas, números, símbolos y espacios en la contraseña. Esto hace que sea más difícil de adivinar; cuantos más símbolos hay para elegir, hay más contraseñas posibles que, alguien que intenta adivinar, su contraseña debería probar.</p>
      <note>
        <p>Un buen método para elegir una contraseña es tomar la primera letra de cada palabra de una frase que pueda recordar. La frase puede ser el nombre de una película, un libro, una canción o un disco. Por ejemplo, «Planilandia: Una novela de muchas dimensiones» podría ser «P:UNdMD», «pundmd» o «p: undmd».</p>
      </note>
    </item>
    <item>
      <p>Haga que su contraseña sea lo más larga posible. Cuantos más caracteres contenga, más tiempo le llevará a una persona o a un ordenador adivinarla.</p>
    </item>
    <item>
      <p>No use palabras que aparezcan en diccionarios estándar de cualquier idioma. Serán las que se prueben primero para intentar romper la contraseña. La contraseña más común es «password»; la gente adivina este tipo de contraseñas muy rápidamente.</p>
    </item>
    <item>
      <p>No use ninguna información personal, tal como fechas, números de matrículas o el nombre de cualquier familiar.</p>
    </item>
    <item>
      <p>No use ningún sustantivo.</p>
    </item>
    <item>
      <p>Elija una contraseña que se pueda teclear rápidamente, para reducir las oportunidades de que alguien pueda ver lo que escribe, en caso de que le estén observando.</p>
      <note style="tip">
        <p>Nunca escriba sus contraseñas en ningún sitio. Es fácil encontrarlas.</p>
      </note>
    </item>
    <item>
      <p>Use contraseñas diferentes para diferentes cosas.</p>
    </item>
    <item>
      <p>Use contraseñas diferentes para diferentes cuentas.</p>
      <p>Si usa la misma contraseña para todas sus cuentas, cualquiera que la adivine será capaz de acceder a toda su información de forma inmediata.</p>
      <p>Sin embargo, puede ser difícil recordar muchas contraseñas. Aunque no es tan seguro como usar una contraseña diferente para cada cosa, puede ser más fácil usar la misma para cosas sin importancia (como páginas web), y contraseñas diferentes para las cosas importantes (como su cuenta bancaria y su correo-e).</p>
   </item>
   <item>
     <p>Cambie sus contraseñas con regularidad.</p>
   </item>
  </list>

</page>
