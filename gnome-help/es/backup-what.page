<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="es">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Respalde cualquier cosa de la que no pueda prescindir si algo va mal.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>De qué hacer una copia de respaldo</title>

  <p>Su prioridad debería ser respaldar sus <link xref="backup-thinkabout">archivos más importantes</link> así como los que resulten difíciles de crear de nuevo. Por ejemplo, ordenados de mayor a menor importancia:</p>

<terms>
 <item>
  <title>Sus archivos personales</title>
   <p>Esto puede incluir documentos, hojas de cálculo, correo-e, citas en el calendario, datos financieros, fotos familiares o cualquier cosa personal que considere irreemplazable.</p>
 </item>

 <item>
  <title>Su configuración personal</title>
   <p>Esto incluye los cambios que haya hecho a los colores, fondos, resolución de la pantalla y configuración del ratón en su escritorio. También incluye las preferencias de las aplicaciones, como la configuración de <app>LibreOffice</app>, su reproductor de música y su programa de correo electrónico. Son reemplazables, pero puede tardar un tiempo en volverlos a crear.</p>
 </item>

 <item>
  <title>Configuración del sistema</title>
   <p>La mayoría de la gente nunca cambia la configuración que se crea durante la instalación. Si se personaliza la configuración del sistema, o si usa su equipo como un servidor, entonces es posible que quiera hacer una copia de respaldo de esta configuración.</p>
 </item>

 <item>
  <title>Software instalado</title>
   <p>El software que usa se puede restaurar normalmente de manera rápida después de un problema serio en el equipo, reinstalándolo.</p>
 </item>
</terms>

  <p>En general, querrá respaldar los archivos que sean irreemplazables así como los que necesiten de una gran inversión para sustituirlos si no se tuviera copia de respaldo. Por otra parte, si todo resulta fácil de sustituir, puede que no desee usar espacio de disco respaldándolo.</p>

</page>
