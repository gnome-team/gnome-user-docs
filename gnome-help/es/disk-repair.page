<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="es">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Comprobar si un sistema de archivos está dañado y devolverlo a un estado en que se pueda usar.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Reparar un sistema de archivos dañado</title>

  <p>Los sistemas de archivos se pueden corromper debido a una pérdida de energía inesperada, cuelgues del sistema y desconexión insegura de la unidad. Después de un incidente de este tipo es recomendable <em>reparar</em> o al menos <em>comprobar</em> el sistema de archivos para prevenir futuras pérdidas de datos.</p>
  <p>Algunas veces es necesario reparar para poder montar o modificar un sistema de archivos. Incluso si la <em>comprobación</em> no informa de ningún daño el sistema de archivos puede seguir marcado como «sucio» y es necesario repararlo.</p>

<steps>
  <title>Comprobar si un sistema de archivos está dañado</title>
  <item>
    <p>Abra <app>Discos</app> desde la vista de <gui>Actividades</gui>.</p>
  </item>
  <item>
    <p>Seleccione el disco que contiene el sistema de archivos de la lista de dispositivos de almacenamiento de la izquierda. Si hay más de un volumen en el disco, seleccione el que contiene el sistema de archivos.</p>
  </item>
  <item>
    <p>En la barra de herramientas, debajo de la sección <gui>Volúmenes</gui>, pulse el botón del menú. Después pulse <gui>Comprobar sistema de archivos…</gui>.</p>
  </item>
  <item>
    <p>Dependiendo de la cantidad de datos almacenados en el sistema de archivos, la comprobación puede tardar mucho tiempo. Confirme para iniciar la acción en el diálogo emergente.</p>
   <p>Esta acción no modificará el sistema de archivos, pero lo desmontará si es necesario. Sea paciente mientras se comprueba,</p>
  </item>
  <item>
    <p>Después de finalizar se le notificará en caso de que el sistema de archivos esté dañado. Tenga en cuenta que en algunos caso, incluso si el sistema de archivos no está dañado, todavía puede ser necesario repararlo para reinicializar un marcador interno «desordenado».</p>
  </item>
</steps>

<note style="warning">
 <title>Es posible que se pierdan datos al reparar</title>
  <p>Si la estructura del sistema de archivos está dañada puede afectar a los archivos guardados en el sistema de archivos. En algunos casos, estos archivos no se pueden recuperar a un estado válido y se eliminarán o moverán a una carpeta especial.- Normalmente es la carpeta <em>lost+found</em> en la raiz del sistema de archivos en el que podrá encontrar las partes de archivos recuperadas.</p>
  <p>Si los datos son demasiado valiosos como para perderlos durante este proceso, se recomienda que haga una copia de seguridad guardando una imagen del volumen antes de repararlo.</p>
  <p>Esta imagen se puede procesar con herramientas de análisis forense como <app>sleuthkit</app> para posteriormente recuperar archivos y partes de archivo que no se haya podido recuperar durante la reparación, e incluso archivos eliminados.</p>
</note>

<steps>
  <title>Reparar un sistema de archivos</title>
  <item>
    <p>Abra <app>Discos</app> desde la vista de <gui>Actividades</gui>.</p>
  </item>
  <item>
    <p>Seleccione el disco que contiene el sistema de archivos de la lista de dispositivos de almacenamiento de la izquierda. Si hay más de un volumen en el disco, seleccione el que contiene el sistema de archivos.</p>
  </item>
  <item>
    <p>En la barra de herramientas, debajo de la sección <gui>Volúmenes</gui>, pulse el botón del menú. Después pulse en <gui>Reparar sistema de archivos…</gui>.</p>
  </item>
  <item>
    <p>Dependiendo de la cantidad de datos almacenados en el sistema de archivos, la reparación puede tardar mucho tiempo. Confirme para iniciar la acción en el diálogo emergente.</p>
   <p>La acción desmontará el sistema de archivos si es necesario. La acción de reparar intenta llevar el sistema de archivos a un estado consistente y mueve los archivos que estén dañados a una carpeta especial. Sea paciente mientras el sistema de archivos se repara.</p>
  </item>
  <item>
    <p>Una vez finalizado notificará cuando haya sido posible reparar el sistema correctamente. En caso de éxito puede usarse de nuevo normalmente.</p>
    <p>Si el sistema de archivos no puede repararse, saque una copia guardando una imagen del volumen para recuperar archivos importantes posteriormente. Puede hacerlo montando la imagen en modo sólo lectura o usando herramientas de análisis forense como <app>sleuthkit</app>.</p>
    <p>Para hacer uso del volumen de nuevo debe <link xref="disk-format">formatearse</link> con un nuevo sistema de archivos. Todos los datos se perderán.</p>
  </item>
</steps>

</page>
