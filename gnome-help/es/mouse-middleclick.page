<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="es">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.4" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use el botón central para abrir aplicaciones, abrir pestañas y más.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Pulsación central</title>

<p>Muchos ratones y algunos paneles táctiles tienen un botón central. En ratones que disponen de rueda de desplazamiento, a veces puede presionar la rueda directamente para hacer una pulsación de botón central. Si no dispone de botón central, puede emularlo pulsando los botones izquierdo y derecho al mismo tiempo.</p>

<p>En paneles táctiles que sean multitáctiles puede pulsar con tres dedos a la vez para realizar una pulsación con el botón del medio. Para que esto funcione, debe activar <link xref="mouse-touchpad-click">Activar pulsaciones del ratón con el panel táctil</link> en la configuración del panel táctil.</p>

<p>Muchas aplicaciones usan la pulsación central para atajos de pulsación avanzados.</p>

<list>
  <item><p>En aplicaciones con barras de desplazamiento, al pulsar con el botón izquierdo en el espacio vacío de la barra se mueve la posición de desplazamiento directamente a ese lugar. Al pulsar con el botón central se mueve hacia arriba a una sola página hacia esa ubicación.</p></item>

  <item><p>En la vista de <gui>Actividades</gui>, puede abrir rápidamente una nueva ventana para una aplicación con una pulsación central. Simplemente pulse con el botón central en el icono de la aplicación, ya sea en el tablero a la izquierda o en la descripción general de las aplicaciones. La descripción general de las aplicaciones se muestra con el botón de cuadrícula en el tablero.</p></item>

  <item><p>La mayoría de los navegadores web le permiten abrir enlaces en pestañas rápidamente con el botón central del ratón. Simplemente pulse en cualquier enlace con el botón central del ratón y se abrirá en una nueva pestaña.</p></item>

  <item><p>En el gestor de archivos, el botón central tiene dos funciones. Si pulsa con el botón central en una carpeta, se abrirá en una pestaña nueva. Este comportamiento imita al de los navegadores web más populares. Si pulsa con el botón central sobre un archivo, lo abrirá como si hubiese pulsado dos veces sobre él.</p></item>
</list>

<p>Algunas aplicaciones especializadas le permiten usar el botón central del ratón para otras funciones. Busque en la ayuda de su aplicación algún apartado referente a <em>pulsación central</em> o <em>botón central del ratón</em>.</p>

</page>
