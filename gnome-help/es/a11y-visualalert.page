<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="es">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Activar alertas visuales para iluminar la pantalla o la ventana cuando se reproduzca un sonido de alerta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Destello de pantalla para sonidos de alerta</title>

  <p>Su equipo reproducirá un sencillo sonido de alerta para ciertos tipos de mensajes y eventos. Si tiene dificultades para oír esos sonidos, puede hacer que se ilumine la ventana actual o la pantalla completa cada vez que se reproduzca un sonido de alerta.</p>

  <p>También puede resultar útil si se encuentra en un entorno en el que necesita que su equipo esté en silencio, como en una biblioteca. Consulte la <link xref="sound-alert"/> para saber cómo silenciar el sonido de alerta y activar las alertas visuales.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Accesibilidad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Accesibilidad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Select the <gui>Hearing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Visual Alerts</gui> section, switch the <gui>Visual Alerts</gui>
      switch to on.</p>
    </item>
    <item>
      <p>Under <gui>Flash Area</gui>, select whether you want the entire screen or just
      your current window title to flash.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Puede activar y desactivar rápidamente las alertas visuales pulsando en el <link xref="a11y-icon">icono de accesibilidad</link> en la barra superior y seleccionando <gui>Alertas visuales</gui>.</p>
  </note>

</page>
