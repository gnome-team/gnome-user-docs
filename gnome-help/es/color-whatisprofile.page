<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-whatisprofile" xml:lang="es">

  <info>
    <link type="guide" xref="color#profiles"/>
    <desc>Un perfil de color es un simple archivo que expresa un espacio de color o una respuesta de un dispositivo.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Qué es un perfil de color?</title>

  <p>Un perfil de color es un conjunto de datos que caracteriza o un dispositivo, tal como un proyector, o un espacio de color, tal como sRGB.</p>
  <p>La mayoría de los perfiles de color están en un formato ICC, que es un pequeño archivo con una extensión <input>.ICC</input> o <input>.ICM</input>.</p>
  <p>Los perfiles de color se pueden empotrar en imágenes para especificar el espectro del rango de los datos. Esto asegura que el usuario ve los mismos colores en diferentes dispositivos.</p>
  <p>Cada dispositivo que procese color debe tener su propio perfil ICC y cuando esto se logra se dice que el sistema tiene un <em>flujo de trabajo de color de principio a fin</em>. Con este flujo de trabajo puede estar seguro de que los colores no se pierden ni modifican.</p>

</page>
