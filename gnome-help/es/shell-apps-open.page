<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="es">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lanzar aplicaciones desde la vista de <gui>Actividades</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Iniciar aplicaciones</title>

  <p if:test="!platform:gnome-classic">Mueva el puntero de su ratón a la esquina de <gui>Actividades</gui> en la parte superior izquierda de la pantalla para mostrar la vista de <gui xref="shell-introduction#activities">Actividades</gui>. Aquí es donde puede encontrar todas sus aplicaciones. También puede abrir la vista pulsando la tecla <link xref="keyboard-key-super">Super</link>.</p>
  
  <p if:test="platform:gnome-classic">Puede iniciar aplicaciones desde el <gui xref="shell-introduction#activities">Aplicaciones</gui> en la parte superior izquierda de la pantalla, o pude usar la vista de <gui>Actividades</gui> pulsando la tecla <key xref="keyboard-key-super">Super</key>.</p>

  <p>Hay varias maneras de abrir una aplicación una vez que está en la vista de <gui>Actividades</gui>:</p>

  <list>
    <item>
      <p>Comience a escribir el nombre de una aplicación; la búsqueda comenzará al instante. (Si esto no sucede, pulse en la barra de búsqueda en la parte superior de la pantalla y comience a escribir.) Si no conoce el nombre exacto de la aplicación, pruebe a escribir un término relacionado. Pulse en el icono de la aplicación para iniciarla.</p>
    </item>
    <item>
      <p>Some applications have icons in the <em>dash</em>, the horizontal strip
      of icons at the bottom of the <gui>Activities</gui> overview.
      Click one of these to start the corresponding application.</p>
      <p>Si tiene aplicaciones que usa muy frecuentemente, puede <link xref="shell-apps-favorites">añadirlas al tablero</link>.</p>
    </item>
    <item>
      <p>Click the grid button (which has nine dots) in the dash.
      You will see the first page of all installed applications. To see more
      applications, press the dots at the bottom, above the dash, to view other
      applications. Press on the application to start it.</p>
    </item>
    <item>
      <p>You can launch an application in a separate
      <link xref="shell-workspaces">workspace</link> by dragging its icon from
      the dash, and dropping it onto one of the workspaces. The application will
      open in the chosen workspace.</p>
      <p>You can launch an application in a <em>new</em> workspace by dragging its
      icon to an empty workspace, or to the small gap between two workspaces.</p>
    </item>
  </list>

  <note style="tip">
    <title>Ejecutar rápidamente un comando</title>
    <p>Otra manera de ejecutar una aplicación es pulsar <keyseq><key>Alt</key><key>F2</key></keyseq>, introduciendo el <em>nombre del comando</em> y pulsando la tecla <key>Intro</key>.</p>
    <p>Por ejemplo, para lanzar <app>Rhythmbox</app>, pulse <keyseq><key>Alt</key><key>F2</key></keyseq> y escriba «<cmd>rhythmbox</cmd>» (sin las comillas). El nombre de la aplicación es el comando para lanzar el programa.</p>
    <p>Use las teclas de flechas para acceder rápidamente a comandos ejecutados anteriormente</p>
  </note>

</page>
