<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-menu" xml:lang="es">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>
    <link type="seealso" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="new"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>La tecla de <key>Menú</key> muestra un menú contextual con el teclado en lugar de con el botón derecho.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Qué es la tecla <key>Menú</key>?</title>

  <p>La tecla de <key>Menú</key>, llamada también tecla de <em>Aplicación</em>, es una tecla que se encuentra en algunos teclados pensados para Windows. Generalmente, esta tecla está en la parte inferior derecha del teclado, junto a la tecla <key>Ctrl</key>, pero algunos fabricantes pueden colocarla en otro lugar. Normalmente se representa por una flecha encima de un menú: <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-menu.svg">
  <key>Menu</key> key icon</media>.</p>

  <p>La función principal de esta tecla es mostrar un menú contextual con el teclado en lugar de hacerlo con el botón derecho del ratón: esto es útil si su ratón o su dispositivo similar no está disponible, o cuando no tiene botón derecho del ratón.</p>

  <p>Algunas veces, la tecla de  <key>Menú</key> no se incluye para ahorrar espacio, especialmente en teclados portables o de portátiles. En estos casos, algunos teclados incluyen una tecla de función de <key>Menú</key> que se puede activar junto con la tecla de Función (<key>Fn</key>).</p>

  <p>El <em>menú contextual</em> es un menú emergente que se muestra cuando pulsa con el botón derecho. El menú que ve, si existe, depende del contexto y de la función del área en la que pulsa con el botón derecho. Cuando usa la tecla de <key>Menú</key>, el menú contextual se muestra para el área de la pantalla en la que está su cursor en el momento de pulsarla.</p>

</page>
