<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-notspecifiededid" xml:lang="es">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="guide" xref="color-gettingprofiles"/>
    <link type="guide" xref="color-why-calibrate"/>
    <desc>Los perfiles de monitor predeterminados no tienen una fecha de calibración.</desc>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Por qué no caducan los perfiles predeterminados para pantallas?</title>
  <p>El perfil de color predeterminado usado para cada monitor se genera automáticamente basándose en los datos <link href="https://en.wikipedia.org/wiki/Extended_Display_Identification_Data">EDID</link> de la pantalla, almacenados en el chip de memoria dentro del monitor. Los datos EDID sólo le dan una instantánea de los colores disponibles que el monitor es capaz de mostrar en el momento de su fabricación y no contiene mucha más información de corrección de color.</p>

  <note style="tip">
    <p>Obtener un perfil del fabricante del monitor o crear un perfil usted mismo hará que obtenga una corrección de color más precisa.</p>
  </note>

</page>
