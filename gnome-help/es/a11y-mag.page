<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="es">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Ampliar su pantalla de manera que sea más fácil ver cosas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Magnificar un área de la pantalla</title>

  <p>Magnificar la pantalla no es simplemente aumentar el <link xref="a11y-font-size">tamaño del texto</link>. Esta característica es como tener una lupa que puede mover aumentando partes de la pantalla.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Accesibilidad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Accesibilidad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Select the <gui>Zoom</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Desktop Zoom</gui> switch to on.</p>
    </item>
  </steps>

  <p>Puede moverse en el área de la pantalla. Moviendo su ratón por los bordes de la ptanlla, puede mover el área magnificada en diferentes direcciones, permitiéndole ver el área de su elección.</p>

  <note style="tip">
    <p>Puede activar o desactivar la ampliación rápidamente pulsando el <link xref="a11y-icon">icono de accesibilidad</link> en la barra superior y seleccionando <gui>Ampliación</gui>.</p>
  </note>

  <p>You can change the magnification factor, the mouse tracking, and the
  position of the magnified view on the screen. Adjust these in the
  <gui>Magnifier</gui> section.</p>

  <p>You can activate crosshairs to help you find the mouse or touchpad
  pointer. Switch them on and adjust their length, color, and thickness in the
  <gui>Crosshairs</gui> section.</p>

  <p>You can switch to inverted colors video, and adjust
  brightness, contrast and color options for the magnifier in the
  <gui>Color Filters</gui> section. The combination of these options is
  useful for people with low-vision, any degree of photophobia, or just
  for using the computer under adverse lighting conditions.</p>

</page>
