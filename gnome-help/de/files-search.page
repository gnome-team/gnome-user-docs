<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="de">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>
    <revision pkgversion="3.36" date="2020-04-18" status="draft"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">Suchen</title>
    <desc>Finden Sie Dateien anhand ihres Namens und Typs.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Nach Dateien suchen</title>

  <p>Sie können nach Dateien anhand ihres Namens oder Dateityps direkt in der Dateiverwaltung suchen.</p>

  <links type="topic" style="linklist">
    <title>Weitere Suchanwendungen</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Suchen</title>
    <item>
      <p>Öffnen Sie in der <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht die Anwendung <app>Dateien</app>.</p>
    </item>
    <item>
      <p>Wenn Sie wissen, dass die Dateien in einem bestimmten Ordner oder seinen Unterordnern sind, gehen Sie zu diesem Ordner.</p>
    </item>
    <item>
      <p>Tippen Sie ein Wort oder Wörter, von denen Sie wissen, dass sie im Dateinamen enthalten sind. Wenn Sie zum Beispiel alle Ihre Rechnungen mit dem Wort »Rechnung« benennen, tippen Sie <input>Rechnung</input> ein. Groß- und Kleinschreibung spielt dabei keine Rolle.</p>
      <note>
        <p>Klicken Sie auf <media its:translate="no" type="image" mime="image/svg" src="figures/nautilus-folder-search-symbolic.svg"> <key>Search</key> key symbol
        </media> in der Werkzeugleiste oder drücken Sie <keyseq><key>Strg</key><key>F</key></keyseq>, anstatt direkt Wörter einzutippen und die Suchleiste zu aktivieren.</p>
      </note>
    </item>
    <item>
      <p>Schränken Sie die Ergebnisse nach Datum, Dateityp oder danach ein, ob der Dateiinhalt oder nur die Dateinamen durchsucht werden.</p>
      <p>Um Filter anzuwenden, klicken Sie auf den Auswahlmenü-Knopf links neben dem <media its:translate="no" type="image" mime="image/svg" src="figures/nautilus-folder-search-symbolic.svg"> <key>Search</key> key symbol</media>-Symbol der Dateiverwaltung und wählen Sie aus den verfügbaren Filtern:</p>
      <list>
        <item>
          <p><gui>Wann</gui>: Wie weit möchten Sie zurück in die Vergangenheit suchen?</p>
        </item>
        <item>
          <p><gui>Was</gui>: Was ist der gesuchte Objekttyp?</p>
        </item>
        <item>
          <p><gui>Suchen</gui>: Soll Ihre Suche eine Volltextsuche sein, oder suchen Sie nur anhand von Dateinamen?</p>
        </item>
      </list>
    </item>
    <item>
      <p>Wählen Sie das <gui>X</gui> neben dem Filter, den Sie entfernen möchten.</p>
    </item>
    <item>
      <p>Sie können die Dateien des Suchergebnisses öffnen, kopieren, löschen oder auf andere Weise damit arbeiten, so wie Sie es mit jedem anderen Ordner in der Dateiverwaltung auch machen würden.</p>
    </item>
    <item>
      <p>Klicken Sie erneut auf <media its:translate="no" type="image" mime="image/svg" src="figures/nautilus-folder-search-symbolic.svg"> <key>Search</key> key symbol
      </media> in der Werkzeugleiste, um die Suche zu beenden und zum Ordner zurückzukehren.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Dateisuche anpassen</title>

<p>Schließen Sie für gezielte Suchen in der Anwendung <app>Dateien</app> bestimmte Ordner ein oder aus. So passen Sie die zu durchsuchenden Ordner an:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Suchen</gui> ein.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui>Einstellungen</gui><gui>Suchen</gui></guiseq> in der Ergebnisliste. Daraufhin öffnet sich die Leiste <gui>Sucheinstellungen</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Suchorte</gui>.</p>
    </item>
  </steps>

<p>Daraufhin öffnet sich eine Dialogfenster, in dem Sie die Suche in Ordnern ein- oder ausschalten können. Sie können die Suchen für jeden der drei Abschnitte ein- oder ausschalten:</p>

  <list>
    <item>
      <p><gui>Orte</gui>: Hier werden übliche Orte für persönliche Ordner aufgeführt</p>
    </item>
    <item>
      <p><gui>Lesezeichen</gui>: Hier werden Ordner aufgeführt, für die Sie Lesezeichen in <app>Dateien</app> angelegt haben</p>
    </item>
    <item>
      <p><gui>Weitere</gui>: Mit dem Knopf <gui>+</gui> ausgewählte Ordner werden hier aufgeführt.</p>
    </item>
  </list>

</section>

</page>
