<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="de">

  <info>
    <link type="guide" xref="media#music"/>

    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Überprüfen Sie, ob die erforderlichen Video-Codecs installiert sind.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Andere können von mir erstellte Videos nicht anschauen</title>

  <p>Falls Sie auf Ihrem Linux-Rechner ein Video erzeugt haben und es an jemanden senden, der Windows oder Mac OS verwendet, könnte es Probleme bei der Wiedergabe des Videos geben.</p>

  <p>Damit die Person, an die Sie das Video gesendet haben, es auch abspielen kann, müssen die richtigen <em>Codecs</em> installiert sein. Ein Codec ist eine Software, mit deren Hilfe das Video richtig interpretiert und auf dem Bildschirm angezeigt werden kann. Es gibt eine Vielzahl verschiedener Videoformate, und jedes davon benötigt den passenden Codec für die Wiedergabe. Sie können folgendermaßen ermitteln, in welchem Format Ihr Video vorliegt:</p>

  <list>
    <item>
      <p>Öffnen Sie <app>Dateien</app> in der <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht.</p>
    </item>
    <item>
      <p>Klicken Sie mit der rechten Maustaste auf die Videodatei und wählen Sie <gui>Eigenschaften</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Reiter <gui>Audio/Video</gui> oder <gui>Video</gui> und schauen Sie nach, welcher <gui>Codec</gui> unter <gui>Video</gui> und ggfs. <gui>Audio</gui> in der Liste steht.</p>
    </item>
  </list>

  <p>Fragen Sie die Person, die Wiedergabeprobleme hat, ob die richtigen Codecs installiert sind. Hilfreich ist hier eine Websuche nach dem Namen des Codecs und dem Namen des verwendeten Wiedergabeprogramms. Wenn Ihr Video beispielsweise im <em>Theora</em>-Format vorliegt und jemand dieses Video mit dem Windows Media Player wiedergeben will, suchen Sie nach »theora windows media player«. Oft ist es möglich, den entsprechenden Codec kostenlos herunterzuladen, falls dieser nicht installiert ist.</p>

  <p>Falls der richtige Codec nicht gefunden werden kann, versuchen Sie den <link href="http://www.videolan.org/vlc/">VLC Media Player</link>. Dieser funktioniert unter Windows, Mac OS und Linux gleichermaßen und unterstützt eine Vielzahl von verschiedenen Videoformaten. Falls auch das scheitert, versuchen Sie Ihr Video in ein anderes Format umzuwandeln. Die meisten Videobearbeitungsprogramme sind dazu fähig. Weiterhin sind spezielle Video-Umwandlungsprogramme erhältlich. Schauen Sie in Ihrer Softwareverwaltung nach, was für diese Zwecke verfügbar ist.</p>

  <note>
    <p>Allerdings gibt es noch weitere Fehlerquellen, welche die Wiedergabe des Videos verhindern können. Das Video könnte bei der Übertragung zum Empfänger beschädigt worden sein, da manchmal das Kopieren von großen Dateien nicht fehlerfrei geschieht. Weiterhin könnte es Probleme mit dem Wiedergabeprogramm selbst geben, oder das Video könnte nicht sauber erzeugt worden sein, zum Beispiel könnten Fehler beim Speichern des Videos aufgetreten sein.</p>
  </note>

</page>
