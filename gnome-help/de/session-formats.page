<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eine Region für Datum und Zeit, Zahlen, Währung und Maßeinheiten wählen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Datumsformat und Maßeinheiten ändern</title>

  <p>Sie können die Formate und Einheiten, die für das Datum, die Zeit, Zahlen, Währung und Maßeinheiten verwendet werden, so einstellen, wie es in Ihrer Region üblich ist.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>System</gui> ein.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui>Einstellungen</gui><gui>System</gui></guiseq> in der Ergebnisliste. Daraufhin öffnet sich das Seitenfenster <gui>System</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Region und Sprache</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie im Abschnitt <gui>Ihr Konto</gui> auf <gui>Formate</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie unter <gui>Bekannte Formate</gui> die Region und Sprache aus, die Ihren gewünschten Formaten am besten entspricht.</p>
    </item>
    <item>
      <p>Klicken Sie zum Speichern auf <gui style="button">Fertig</gui>.</p>
    </item>
    <item>
      <p>Sie müssen die Sitzung neu starten, damit die Änderungen wirksam werden. Klicken Sie hierzu auf <gui style="button">Neustart …</gui>, oder führen Sie den Neustart später manuell durch.</p>
    </item>
  </steps>

  <p>Nachdem Sie eine Region gewählt haben, wird rechts neben der Liste anhand verschiedener Beispiele gezeigt, wie das Datum und anderes mehr dargestellt wird. Obwohl in den Beispielen nicht aufgeführt, wird mit der Region auch der Wochentag festgelegt, mit dem die Kalenderwoche beginnt.</p>

  <note style="tip">
    <p>Wenn auf Ihrem System mehrere Benutzerkonten vorhanden sind, dann gibt es einen eigenen Abschnitt <gui>Anmeldebildschirm</gui> im Seitenfenster <gui>Region und Sprache</gui>.</p>
  </note>

</page>
