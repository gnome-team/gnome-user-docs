<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="de">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>
    <revision version="gnome:45" date="2024-03-03" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hinzufügen, Löschen und Umbenennen von Lesezeichen mit der Dateiverwaltung.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ordner-Lesezeichen bearbeiten</title>

  <p>Ihre Lesezeichen sind in der Seitenleiste der Dateiverwaltung aufgeführt.</p>

  <steps>
    <title>Ein Lesezeichen hinzufügen:</title>
    <item>
      <p>Wählen Sie den Ordner (oder den Ort) aus, den Sie als Lesezeichen speichern wollen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den aktuellen Ordner in der Pfadleiste und wählen Sie <gui style="menuitem">Zu Lesezeichen hinzufügen</gui>.</p>
      <note>
        <p>Sie können auch einen Ordner in die Seitenleiste ziehen und ihn über <gui>Neues Lesezeichen</gui> ablegen, was dynamisch angezeigt wird.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>So löschen Sie ein Lesezeichen:</title>
    <item>
      <p>Klicken Sie mit der rechten Maustaste auf das Lesezeichen in der Seitenleiste und wählen Sie <gui>Aus Lesezeichen entfernen</gui> aus dem Menü.</p>
    </item>
  </steps>

  <steps>
    <title>Ein Lesezeichen umbenennen:</title>
    <item>
      <p>Klicken Sie mit der rechten Maustaste auf das Lesezeichen in der Seitenleiste und wählen Sie <gui>Umbenennen</gui>.</p>
    </item>
    <item>
      <p>Geben Sie im Eingabefeld <gui>Name</gui> den Namen für das Lesezeichen ein.</p>
      <note>
        <p>Das Umbenennen eines Lesezeichens benennt nicht den Ordner um. Wenn Sie Lesezeichen für zwei verschiedene Ordner an verschiedenen Orten haben, die Lesezeichen aber denselben Namen verwenden, können Sie sie nicht auseinander halten. In solchen Fällen ist es sinnvoll, dem Lesezeichen einen anderen Namen zu geben als den Namen des Ordners, zu dem es führt.</p>
      </note>
    </item>
  </steps>

</page>
