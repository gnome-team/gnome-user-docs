<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="de">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ein analoges oder USB-Mikrofon verwenden und das vorgegebene Eingabegerät wählen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ein anderes Mikrofon verwenden</title>

  <p>Sie können ein externes Mikrofon für Unterhaltungen mit Freunden, Gespräche mit Kollegen, Aufzeichnen von Sprache oder für andere Multimedia-Zwecke verwenden. Selbst wenn der Rechner bereits über ein eingebautes Mikrofon verfügen sollte, bietet ein externes Mikrofon meist eine bessere Klangqualität.</p>

  <p>Falls Ihr Mikrofon einen runden Stecker hat, stecken Sie es in die entsprechende Buchse Ihres Rechners ein. Die meisten Rechner haben zwei Buchsen: eine für Mikrofone und eine für Lautsprecher. Schauen Sie nach der Abbildung eines Mikrofons neben der Buchse. Sie ist meistens hellrot. An der Buchse angeschlossene Mikrofone werden üblicherweise sofort verwendet. Falls nicht, befolgen Sie die nachstehenden Anweisungen für die Auswahl des Standard-Eingabegerätes.</p>

  <p>Falls Sie ein USB-Mikrofon haben, stecken Sie es an einen USB-Port Ihres Rechners. USB-Mikrofone funktionieren als separate Audiogeräte, daher müssen Sie angeben, welches Mikrofon als Standard verwendet werden soll.</p>

  <steps>
    <title>Vorgegebenes Audio-Eingabegerät wählen</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Audio</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Audio</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie im Abschnitt <gui>Eingang</gui> das zu verwendende Gerät aus. Die Pegelanzeige sollte ausschlagen, wenn Sie sprechen.</p>
    </item>
  </steps>

  <p>Sie können in diesem Seitenfenster die Lautstärke anpassen und das Mikrofon ausschalten.</p>

</page>
