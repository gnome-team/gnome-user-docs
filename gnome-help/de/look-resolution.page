<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display" group="#first"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>
    <revision version="gnome:46" status="draft" date="2024-04-22"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Emanuel Cisár</name>
      <email>536429@mail.muni.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Auflösung des Bildschirms und seine Ausrichtung (Drehung) ändern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ändern der Auflösung und der Ausrichtung des Bildschirms</title>

  <p>Sie können ändern, wie groß (oder wie detailliert) Dinge auf dem Bildschirm erscheinen, indem Sie die <em>Bildschirmauflösung</em> ändern. Sie können ändern, wie herum die Dinge erscheinen (zum Beispiel wenn Sie einen drehbaren Monitor haben), indem Sie die <em>Drehung</em> ändern.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Bildschirme</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bildschirme</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Wenn Sie mehrere Bildschirme verwenden und diese nicht gespiegelt sind, können Sie für jeden Bildschirm eigene Einstellungen festlegen. Wählen Sie einen Bildschirm im Vorschaubereich aus.</p>
    </item>
    <item>
      <p>Wählen Sie die Ausrichtung, Auflösung oder Skalierung und die Aktualisierungsrate.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Anwenden</gui>. Die geänderten Einstellungen werden für 20 Sekunden angewendet und dann wieder rückgängig gemacht. Auf diese Weise werden Ihre alten Einstellungen wiederhergestellt, falls Sie mit den neuen Einstellungen nichts sehen können. Wenn Sie mit den neuen Einstellungen zufrieden sind, klicken Sie auf <gui>beibehalten</gui>.</p>
    </item>
  </steps>

<section id="orientation">
  <title>Ausrichtung</title>

  <p>Auf einigen Geräten kann man den Bildschirm physikalisch in viele Richtungen drehen. Klicken Sie auf <gui>Ausrichtung</gui> im Seitenfenster und wählen Sie entweder <gui>Querformat</gui>, <gui>Hochformat rechts gedreht</gui>, <gui>Hochformat links gedreht</gui> oder <gui>Kopfstehendes Querformat</gui> aus.</p>

  <note style="tip">
    <p>Wenn Ihr Gerät den Bildschirm automatisch dreht, so können Sie die Drehung mit dem Knopf <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">Drehsperre</span></media> unten im <gui xref="shell-introduction#systemmenu">Systemmenü</gui> sperren. Klicken Sie zum Aufheben der Sperre auf den Knopf <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">Drehsperrung aufheben</span></media></p>
  </note>

</section>

<section id="resolution">
  <title>Auflösung</title>

  <p>Die Auflösung wird als Anzahl der Pixel (Punkte auf dem Bildschirm) angegeben, in jeder darstellbaren Richtung. Jede Auflösung hat ein <em>Seitenverhältnis</em>, das Verhältnis der Breite zur Höhe. Breitbildschirme verwenden ein Seitenverhältnis von 16:9, während traditionelle Bildschirme 4:3 verwenden. Wenn Sie eine Auflösung wählen, die nicht dem Seitenverhältnis Ihres Bildschirms entspricht, werden schwarze Balken oben und unten oder an den Seiten hinzugefügt, um Verzerrungen zu vermeiden.</p>

  <p>Sie können aus der Auswahlliste die <gui>Auflösung</gui> auswählen, die Sie bevorzugen. Beachten Sie, dass wenn Sie eine Auflösung wählen, die für Ihren Bildschirm ungeeignet ist, das Bild <link xref="look-display-fuzzy">unscharf oder verpixelt</link> aussehen kann.</p>

</section>

<section id="native">
  <title>Native Auflösung</title>

  <p>Die <em>native Auflösung</em> eines Laptop-Bildschirms oder LCD-Monitors ist diejenige, welche optimal arbeitet: Die Pixel im Video-Signal fallen exakt auf die Pixel auf dem Bildschirm. Wenn der Bildschirm andere Auflösungen darstellen muss, so wird eine Interpolation notwendig, was die Bildqualität verschlechtert.</p>

</section>

<section id="refresh">
  <title>Aktualisierungsrate</title>

  <p>Die Aktualisierungsrate gibt an, wie oft in der Sekunde das Bild dargestellt oder aktualisiert wird.</p>

  <section id="variable-refresh-rate">
    <title>Variable Aktualisierungsrate (VRR)</title>
      <p>Synchronisieren Sie die Bildwiederholungsrate des Bildschirms mit dem Bildsignal für eine gleichmäßigere Darstellung und verbesserten Energieverbrauch.</p>

  <note style="tip">
    <p>VRR unterscheidet sich von Adaptive V-Sync (aktuelles V-Sync), welches dynamisch V-Sync ein- und ausschaltet anhand der Bildrate.</p>
  </note>

  </section>
  <section id="preferred-refresh-rate">
    <title>Gewünschte Aktualisierungsrate</title>
    <p>Geben Sie Ihre gewünschte Wiederholungsrate an, um die optimale optische Qualität zu gewährleisten.</p>
  </section>
</section>

<section id="scale">
  <title>Skalieren</title>

  <p>Die Skalieren-Einstellung vergrößert die auf dem Bildschirm dargestellten Objekte, um der Dichte Ihrer Anzeige zu entsprechen, wodurch sie leichter lesbar werden. Wählen Sie entweder <gui>100%</gui> oder <gui>200%</gui>.</p>

</section>

</page>
