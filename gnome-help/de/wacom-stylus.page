<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="de">

  <info>
    <revision version="gnome:46" date="2024-03-10" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Knopffunktionen und das Druckverhalten für den Wacom Stylus festlegen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Den Stylus einrichten</title>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Wacom-Tablett</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Wacom-Grafiktablett</gui>, um das Seitenfenster zu öffnen.</p>
      <note style="tip"><p>Falls kein Tablett erkannt wurde, dann werden Sie gebeten <gui>Bitte schließen Sie Ihr Wacom-Tablett an oder schalten Sie es ein</gui>. Klicken Sie auf <gui>Bluetooth</gui> in der Seitenleiste, um ein Funk-Tablett zu verbinden.</p></note>
    </item>
    <item>
      <p>Für jeden Stylus gibt es einen Bereich mit dessen Einstellungen, der durch den Gerätenamen (die Stylus-Klasse) und das Diagramm oben gekennzeichnet ist.</p>
      <note style="tip"><p>Falls kein Stylus erkannt wurde, dann werden Sie gebeten <gui>Ihren Stylus in die Nähe des Tabletts zur weiteren Einrichtung zu bringen</gui>.</p></note>
      <p>Folgende Einstellungen können angepasst werden:</p>
      <list>
        <item><p><gui>Druckschwelle der Spitze:</gui> Verwenden Sie den Schieberegler, um zwischen <gui>Weich</gui> und <gui>Hart</gui> zu regeln.</p></item>
        <item><p>Konfiguration für Knöpfe/Scrollrad (ändert sich zwecks Anpassung an den Stylus). Klicken Sie auf das Menü neben jedem Eintrag, um eine der folgenden Funktionen auszuwählen: Klick mit der linken/mittleren/rechten Maustaste, Zurück oder Vorwärts.</p></item>
     </list>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Testen Sie Ihre Einstellungen</gui> in der Kopfleiste, um ein Skizzenfeld herunterzuklappen, auf dem Sie die Einstellungen Ihres Stylus prüfen können.</p>
    </item>
  </steps>

</page>
