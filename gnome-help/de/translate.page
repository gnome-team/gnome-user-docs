<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="translate" xml:lang="de">

  <info>
   <link type="guide" xref="more-help"/>
   <link type="seealso" xref="get-involved"/>
    <desc>Hier erfahren Sie, wo und wie Sie diese Themen übersetzen können.</desc>
    <revision pkgversion="3.11.4" version="0.1" date="2014-01-26" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
     <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
     <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
        <name>Zaria</name>
        <email>zaria@vivaldi.net</email>
    </credit>

  <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

<title>Verbesserung der Übersetzungen</title>

<p>Das Benutzerhandbuch wird von einer weltweiten Gemeinschaft aus Freiwilligen übersetzt. Übersetzungen sind noch für <link href="https://l10n.gnome.org/module/gnome-user-docs/">zahlreiche Sprachen</link> nötig.</p>
<p>Um mit der Übersetzung zu beginnen, <link href="https://l10n.gnome.org/register/">erstellen Sie ein Konto</link> und treten Sie einem <link href="https://l10n.gnome.org/teams/">Übersetzungsteam</link> für Ihre Sprache bei. So erhalten Sie die Möglichkeit, neue Übersetzungen hochzuladen.</p>
<p>Treten Sie mit anderen GNOME Übersetzern im Matrix-Raum <link href="https://matrix.to/#/#i18n:gnome.org">#i18n:gnome.org</link> in Kontakt. Weitere Informationen dazu finden Sie auf der Seite <link xref="help-matrix">Hilfe zu Matrix</link>. Es gibt auf dem <link xref="help-irc">GNOME IRC-Server</link> auch einen IRC-Kanal »#gnome-i18n«. Da die anderen Teilnehmer in den Räumen weltweit verteilt sind, kann es einen Moment dauern, bis Sie eine Antwort erhalten.</p>
<p>Alternativ können Sie das Internationalisierungsteam über <link href="https://discourse.gnome.org/tag/i18n">GNOME Discourse</link> kontaktieren.</p>

</page>
