<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="de">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.29" date="2018-08-27" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bewegen in der Arbeitsumgebung mit der Tastatur.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

<title>Nützliche Tastenkombinationen</title>

<p>Hier finden Sie einen Überblick über Tastenkombinationen, die Ihnen helfen können, Ihre Arbeitsumgebung und Anwendungen effizienter nutzen zu können. Falls Sie überhaupt keine Maus oder andere Zeigegeräte verwenden können, finden Sie im Abschnitt <link xref="keyboard-nav"/> weitere Informationen über die reine Tastaturnavigation in Benutzeroberflächen.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Bewegen in der Arbeitsumgebung</title>
  <tr xml:id="super">
    <td><p><key xref="keyboard-key-super">Super</key>-Taste</p></td>
    <td><p>Wechseln Sie zwischen der <gui>Aktivitäten</gui>-Übersicht und der Arbeitsfläche. Geben Sie in der Übersicht Text ein, um unmittelbar nach Anwendungen, Kontakten und Dokumenten zu suchen.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
    <td><p>Schnellstartfenster (zum schnellen Ausführen von Befehlen)</p>
    <p>Mit den Pfeiltasten können Sie schnell auf zuvor ausgeführte Befehle zugreifen.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Schneller Wechsel zwischen Fenstern</link>. Halten Sie die <key>Umschalttaste</key> gedrückt, um die Wechselrichtung umzukehren.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Super</key><key>`</key></keyseq></p></td>
    <td>
      <p>Wechsel zwischen Fenstern der gleichen Anwendung, oder aus der gewählten Anwendung nach <keyseq><key>Super</key><key>Tabulator</key></keyseq>.</p>
      <p>Dieses Tastenkürzel bezieht sich auf das <key>`</key>-Zeichen auf US-Tastaturen, wo die <key>`</key>-Taste sich oberhalb der <key>Tabulatortaste</key> befindet. Auf allen anderen Tastaturen besteht dieses Tastenkürzel aus <key>Alt</key> und der Taste oberhalb von <key>Tabulator</key>, was auch immer es ist.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td>
      <p>Schneller Wechsel zwischen Fenstern der aktuellen Arbeitsfläche. Halten Sie die <key>Umschalttaste</key> gedrückt, um die Wechselrichtung umzukehren.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Strg</key><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td>
      <p>Setzen Sie den Tastaturfokus auf die oberste Leiste. Wechseln Sie in der <gui>Aktivitäten</gui>-Übersicht den Fokus zwischen oberster Leiste, Dash, Fensterübersicht, Anwendungsliste und Suchfeld. Verwenden Sie die Pfeiltasten zum Navigieren.</p>
    </td>
  </tr>
<!--
  <tr xml:id="ctrl-alt-t">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq></p></td>
    <td>
      <p>Open a Terminal.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-t">
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>T</key></keyseq></p></td>
    <td>
      <p>Open a new Terminal tab on the same window.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-n">
    <td><p><keyseq><key>Ctrl</key><key>Shift</key><key>N</key></keyseq></p></td>
    <td>
      <p>Open a new Terminal window. To use this shortcut, you should already be on a terminal window.</p>
    </td>
  </tr>
-->
  <tr xml:id="super-a">
    <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
    <td><p>Die Liste der Anwendungen anzeigen.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Bild↑</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Strg</key><key>Alt</key><key>→</key></keyseq></p>
      <p>und</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Super</key><key>Bild↓</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Strg</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Zwischen Arbeitsflächen wechseln</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Umschalttaste</key><key>Super</key><key>Bild↑</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>→</key></keyseq></p>
      <p>und</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Umschalttaste</key><key>Super</key><key>Bild↓</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Das aktuelle Fenster auf eine andere Arbeitsfläche verschieben</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Umschalttaste</key><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Das aktuelle Fenster einen Bildschirm nach links verschieben.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Umschalttaste</key><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Das aktuelle Fenster einen Bildschirm nach rechts verschieben.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Strg</key><key>Alt</key><key>Löschtaste</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Den Ausschalten-Dialog anzeigen</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Den Bildschirm sperren.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p>Öffnen Sie die <link xref="shell-notifications#notificationlist">Benachrichtigungsliste.</link> Drücken Sie erneut <keyseq><key>Super</key><key>V</key></keyseq> oder <key>Esc</key> zum Schließen.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Häufig verwendete Tastenkürzel zum Bearbeiten</title>
  <tr>
    <td><p><keyseq><key>Strg</key><key>A</key></keyseq></p></td>
    <td><p>Allen Text oder Objekte einer Liste wählen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>X</key></keyseq></p></td>
    <td><p>Ausgewählten Text oder ausgewählte Objekte in die Zwischenablage verschieben (ausschneiden).</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>C</key></keyseq></p></td>
    <td><p>Ausgewählten Text oder ausgewählte Objekte in die Zwischenablage kopieren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>V</key></keyseq></p></td>
    <td><p>Den Inhalt der Zwischenablage einfügen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>Z</key></keyseq></p></td>
    <td><p>Die letzte Aktion rückgängig machen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>Umschalt</key><key>C</key></keyseq></p></td>
    <td><p>Den ausgewählten Text oder Befehle in die Zwischenablage im Terminal kopieren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>Umschalt</key><key>V</key></keyseq></p></td>
    <td><p>Den Inhalt der Zwischenablage in das Terminal einfügen.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Bildschirmfotos</title>
  <tr>
    <td><p><key>Druck</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Das Bildschirmfoto-Werkzeug öffnen.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Druck</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Ein Bildschirmfoto eines Fensters aufnehmen.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Umschalttaste</key><key>Druck</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Ein Bildschirmfoto des gesamten Bildschirms aufnehmen.</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Starten und Beenden eines Bildschirmmitschnitts.</link></p></td>
  </tr>
</table>

</page>
