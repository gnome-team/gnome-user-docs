<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="de">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Fügen Sie Ihr Foto zum Anmeldefenster und zur Benutzeranzeige hinzu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ändern des Fotos für den Anmeldebildschirm</title>

  <p>Wenn Sie sich anmelden oder den Benutzer wechseln, sehen Sie eine Benutzerliste mit deren Anmeldefotos. Sie können das Foto in ein Repertoirebild oder in ein Foto von sich selbst ändern. Sie können einfach ein neues Foto mit Ihrer Webcam aufnehmen und es verwenden.</p>

  <p>Sie benötigen <link xref="user-admin-explain">Systemverwalterrechte</link>, um fremde Benutzerkonten zu bearbeiten.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>System</gui> ein.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui>Einstellungen</gui><gui>System</gui></guiseq> in der Ergebnisliste. Daraufhin öffnet sich das Seitenfenster <gui>System</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Benutzer</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Wenn Sie einen anderen Benutzer als Sie selbst bearbeiten wollen, klicken Sie auf <gui style="button">Entsperren</gui> in der oberen rechten Ecke und geben Sie Ihr Passwort ein, wenn Sie darum gebeten werden. Wählen Sie den Benutzer unter <gui>Andere Benutzer</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf das Bleistift-Symbol neben Ihrem Namen. Eine Galerie mit einigen vorinstallierten Anmelde-Bildern wird ausgeklappt. Gefällt Ihnen eines davon, klicken Sie darauf, um es für sich selbst zu verwenden.</p>
      <list>
        <item>
          <p>Falls Sie lieber ein Bild verwenden wollen, das bereits auf Ihrem Rechner vorhanden ist, klicken Sie auf <gui>Eine Datei wählen …</gui>.</p>
        </item>
        <item>
          <p>Falls Sie über eine Webcam verfügen, können Sie sofort ein neues Anmeldefoto aufnehmen, indem Sie auf <gui>Ein Foto aufnehmen …</gui> klicken. Nehmen Sie Ihr Foto auf und passen Sie den Quadratrahmen an, um unerwünschte Teile auszuschneiden. Falls Ihnen das aufgenommene Foto nicht gefällt, klicken Sie auf <gui style="button">Ein weiteres Foto aufnehmen</gui>, um es erneut zu versuchen, oder auf <gui>Abbrechen</gui>, um den Versuch aufzugeben.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
