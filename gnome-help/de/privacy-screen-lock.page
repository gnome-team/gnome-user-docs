<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="de">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hindern Sie andere Personen daran, Ihre Arbeitsumgebung zu verwenden, wenn Sie sich von Ihrem Rechner entfernen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ihren Bildschirm automatisch sperren</title>
  
  <p>Wenn Sie Ihren Rechner verlassen, sollten Sie <link xref="shell-exit#lock-screen">den Bildschirm sperren</link>, um andere Personen daran zu hindern, Ihre Arbeitsumgebung zu benutzen und auf Ihre Dateien zuzugreifen. Sie können den Bildschirm automatisch nach einer festgelegten Zeit sperren lassen, falls Sie häufiger das Sperren vergessen. Somit wird Ihr Rechner besser gesichert, wenn Sie ihn nicht verwenden.</p>

  <note><p>Wenn Ihr Bildschirm gesperrt ist, werden Anwendungen und Systemprozesse weiter ausgeführt, aber Sie werden Ihr Passwort eingeben müssen, um Ihre Arbeit fortzusetzen.</p></note>
  
  <steps>
    <title>So legen Sie die Zeit bis zum automatischen Sperren des Bildschirms fest:</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Datenschutz und Sicherheit</gui> ein.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui>Einstellungen</gui><gui>Datenschutz und Sicherheit</gui></guiseq> in der Ergebnisliste. Daraufhin öffnet sich das Teilfenster <gui>Datenschutz und Sicherheit</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bildschirmsperre</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie sicher, dass <gui>Automatische Bildschirmsperre</gui> eingeschaltet ist und wählen Sie dann eine Zeit in der Auswahlliste <gui>Verzögerung bis zur automatischen Bildschirmsperre</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Anwendungen können Benachrichtigungen ausgeben, die noch auf dem Sperrbildschirm angezeigt werden. Dies ist beispielsweise sinnvoll, um den Eingang einer E-Mail anzuzeigen, ohne dass der Bildschirm entsperrt werden muss. Wenn Sie nicht wollen, dass andere Benutzer diese Benachrichtigungen sehen, schalten Sie <gui>Benachrichtigungen auf dem Sperrbildschirm</gui> aus. Weitere Benachrichtigungseinstellungen finden Sie in <link xref="shell-notifications"/>.</p>
  </note>

  <p>Wenn Ihr Bildschirm gesperrt ist und Sie ihn entsperren wollen, drücken Sie die <key>Esc</key>-Taste oder wischen Sie mit der Maus auf dem Bildschirm von unten nach oben. Geben Sie dann Ihr Passwort ein und drücken Sie die <key>Eingabetaste</key> oder klicken Sie auf <gui>Entsperren</gui>. Alternativ können Sie einfach mit der Eingabe Ihres Passworts beginnen, woraufhin der Sperrvorhang automatisch geöffnet wird.</p>

</page>
