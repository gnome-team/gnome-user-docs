<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="de">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Den Zugriff auf Online-Dienste entfernen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ein Konto entfernen</title>

  <p>Entfernen Sie ein Online-Konto, das Sie nicht länger benötigen.</p>

  <note style="tip">
    <p>Viele Anbieter für Internet-Dienste liefern eine Legitimierung, die Ihre Arbeitsumgebung anstelle eines Passworts speichert. Wenn Sie ein Konto entfernen, sollten Sie auch das Zertifikat im Internet-Dienst zurückziehen. Somit wird sichergestellt, dass keine andere Anwendung oder Internetseite sich mit diesem Dienst verbinden kann mithilfe der Legitimierung von Ihrer Arbeitsumgebung.</p>

    <p>So widerrufen Sie die Legitimierung, abhängig vom Service-Anbieter: Prüfen Sie Ihre Einstellungen auf der Webseite des Anbieters bezüglich legitimierten oder verbundenen Anwendungen und Webseiten. Schauen Sie nach einer Anwendung namens »GNOME« und entfernen Sie sie.</p>
  </note>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Online-Konten</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Online-Konten</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie das zu löschende Konto aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui>-</gui> in der linken unteren Ecke des Fensters.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Entfernen</gui> im Bestätigungsfenster.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Anstatt das Konto vollständig zu entfernen, ist es möglich, die <link xref="accounts-disable-service">Dienste einzuschränken</link>, auf die Ihre Arbeitsumgebung zugreift.</p>
  </note>
</page>
