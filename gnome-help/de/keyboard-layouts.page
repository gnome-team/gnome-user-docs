<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tastaturbelegungen hinzufügen und zwischen diesen wechseln.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Alternative Tastaturbelegungen verwenden</title>

  <p>Tastaturen gibt es in Hunderten von Tastaturbelegungen für verschiedene Sprachen. Selbst für eine einzelne Sprache gibt es oft mehrere Tastaturbelegungen, etwa die deutsche Dvorak-Tastaturbelegung. Sie können Ihre Tastatur so benutzen, als hätte Sie eine andere Tastaturbelegung, unabhängig von den Buchstaben und Zeichen, die auf den Tasten aufgedruckt sind. Das ist nützlich, wenn Sie häufig zwischen verschiedenen Sprachen wechseln.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Tastatur</gui> ein.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui>Einstellungen</gui><gui>Tastatur</gui></guiseq> in der Ergebnisliste. Daraufhin öffnet sich die Leiste <gui>Tastatur</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui>+ Eingabequelle hinzufügen …</gui> im Abschnitt <gui>Eingabequellen</gui>, wählen Sie die zur Belegung passende Sprache aus, wählen Sie dann die Belegung und klicken Sie auf <gui>Hinzufügen</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Wenn auf Ihrem System mehrere Benutzerkonten vorhanden sind, dann gibt es einen eigenen Abschnitt <gui>Anmeldebildschirm</gui> im Seitenfenster <gui>Region und Sprache</gui> im <gui>System</gui>-Fenster.</p>

    <p>Einige selten verwendete Tastaturbelegungs-Varianten sind standardmäßig nicht verfügbar, wenn Sie auf den Knopf <gui>+ Eingabequelle hinzufügen …</gui> klicken. Um auch diese Eingabequellen zur Verfügung zu stellen, können Sie ein Terminalfenster öffnen und diesen Befehl ausführen:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Sie können sich eine Vorschau jeder Tastaturbelegung ansehen, indem Sie auf den <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Einstellungen</span></media>-Knopf neben der Sprache in der Liste der <gui>Eingabequellen</gui> klicken und <gui style="menuitem">Tastaturbelegung ansehen</gui> auswählen.</p>
  </note>

  <p>Für bestimmte Sprachen sind zusätzliche Konfigurationsoptionen verfügbar. Diese Sprachen erkennen Sie am <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">Einstellungen</span></media></gui>-Symbol neben sich im <gui>Eingabequellen hinzufügen</gui>-Dialog. Wenn Sie auf diese zusätzlichen Parameter zugreifen wollen, klicken Sie auf den <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Einstellungen</span></media>-Knopf neben der Sprache in der <gui>Eingabequellen</gui>-Liste und wählen Sie <gui style="menuitem">Einstellungen</gui> aus, woraufhin Ihnen zusätzliche Einstellungen zur Verfügung stehen.</p>

  <p>Wenn Sie mehrere Tastaturbelegungen verwenden, können Sie für alle Fenster dieselbe Belegung verwenden oder unterschiedliche Belegungen für jedes einzelne Fenster. Unterschiedliche Belegungen für jedes Fenster zu verwenden ist zum Beispiel dann nützlich, wenn Sie einen Artikel in einer anderen Sprache in einem Textverarbeitungsfenster schreiben. Ihre Tastaturauswahl wird für jedes Fenster gespeichert, wenn Sie zwischen den Fenstern wechseln. Klicken Sie auf den Knopf <gui style="button">Optionen</gui> und wählen Sie, wie Sie mehrere Belegungen verwalten möchten.</p>

  <p>In der obersten Leiste wird eine Kurzbezeichnung der aktuellen Belegung angezeigt, zum Beispiel <gui>de</gui> für die deutsche Standardbelegung. Klicken Sie auf die Kurzbezeichnung und wählen Sie die gewünschte Belegung im Menü aus. Falls für die gewählte Sprache zusätzliche Einstellungen möglich sind, werden diese unter der Liste der verfügbaren Belegungen angezeigt. Dadurch erhalten Sie einen schnellen Überblick über Ihre Einstellungen. Sie können zum Vergleich auch ein Bild der aktuellen Tastaturbelegung öffnen.</p>

  <p>Die schnellste Möglichkeit zum Wechsel zu einer anderen Belegung sind die <gui>Eingabequellen</gui>-<gui>Tastenkombinationen</gui>. Diese Kürzel öffnen den <gui>Eingabequellen</gui>-Wähler, in dem Sie vor- und zurückschalten können. In der Voreinstellung wechseln zur nächsten Eingabequelle mit <keyseq><key xref="keyboard-key-super">Super</key><key>Leertaste</key></keyseq> und zur vorherigen mit <keyseq><key>Umschalttaste</key><key>Super</key><key>Leertaste</key></keyseq> wechseln. Sie können diese Tastenkombinationen in den <gui>Tastatur</gui>-Einstellungen unter <guiseq><gui>Tastenkombinationen</gui><gui>Tastenkombinationen ansehen und anpassen</gui><gui>Tastatur benutzen</gui></guiseq> ändern.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
