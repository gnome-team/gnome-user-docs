<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="de">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Eine Bildschirmtastatur zur Texteingabe durch Mausklicks oder mit einem Tastbildschirm verwenden.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Eine Bildschirmtastatur verwenden</title>

  <p>Falls an Ihren Rechner keine Tastatur angeschlossen ist oder Sie absichtlich keine verwenden wollen, können Sie zur Eingabe von Text die <em>Bildschirmtastatur</em> verwenden.</p>

  <note>
    <p>Die Bildschirmtastatur ist automatisch aktiviert, wenn Sie einen Tastbildschirm verwenden</p>
  </note>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Barrierefreiheit</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Barrierefreiheit</gui>, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie den Abschnitt <gui>Tastatur benutzen</gui> aus, um ihn zu öffnen.</p>
    </item>
    <item>
      <p>Schalten Sie <gui>Bildschirmtastatur</gui> ein.</p>
    </item>
  </steps>

  <p>Bei der nächsten Notwendigkeit einer Eingabe öffnet sich die Bildschirmtastatur am unteren Rand des Bildschirms.</p>

  <p>Klicken Sie auf den Knopf <gui style="button">?123</gui>, um Ziffern und Symbole einzugeben. Weitere Symbole sind verfügbar, wenn Sie auf den Knopf <gui style="button">=/&lt;</gui> klicken. Um zur Buchstabentastatur zurückzukehren, klicken Sie auf <gui style="button">ABC</gui>.</p>

  <p>Klicken Sie auf das Symbol <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">Pfeil nach unten</span></media></gui>, um die Tastatur vorübergehend zu verbergen. Die Tastatur wird automatisch wieder angezeigt, sobald Sie auf ein Objekt klicken, das diese verwendet. Auf einem Tastbildschirm können Sie die Tastatur auch hervorholen, indem Sie <link xref="touchscreen-gestures">von der unteren Kante aus nach oben wischen</link>.</p>
  <p>Klicken Sie auf den Knopf <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">Flagge</span></media></gui>, um die Einstellungen der <link xref="session-language">Sprache</link> oder der <link xref="keyboard-layouts">Eingabequellen</link> anzupassen.</p>

</page>
