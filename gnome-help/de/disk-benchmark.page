<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="de">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ihre Festplatte Leistungstests unterziehen, um die Geschwindigkeit zu überprüfen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

<title>Testen der Leistungsfähigkeit Ihrer Festplatte</title>

  <p>So testen Sie die Leistungsfähigkeit Ihrer Festplatte:</p>

  <steps>
    <item>
      <p>Öffnen Sie <app>Laufwerke</app> in der <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht.</p>
    </item>
    <item>
      <p>Wählen Sie das Laufwerk aus der Liste in der linken Leiste.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Menü-Knopf und wählen Sie <gui>Leistungstest für Laufwerk …</gui> im Menü aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Leistungstest starten …</gui> und passen Sie die Parameter für <gui>Übertragungsrate</gui> und <gui>Zugriffszeit</gui> wie gewünscht an.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Leistungstest starten</gui>, um zu testen, wie schnell Daten vom Medium gelesen werden. Dies erfordert <link xref="user-admin-explain">Systemverwalterrechte</link>. Geben Sie Ihr Passwort oder das Passwort des Systemverwalters ein.</p>
      <note>
        <p>Wenn <gui>Schreib-Leistungstest ausführen</gui> ausgewählt ist, so wird der Test untersuchen, wie schnell Daten vom Laufwerk gelesen und wie schnell Daten darauf geschrieben werden. Diese Möglichkeit wird mehr Zeit in Anspruch nehmen.</p>
      </note>
    </item>
  </steps>

  <p>Wenn die Untersuchung abgeschlossen ist wird das Ergebnis im Diagramm dargestellt. Die grünen Punkte und deren verbindende Linien repräsentieren die Messwerte. Für sie gilt die rechte Achse, welche die Zugriffszeit bemisst. Die untere Achse stellt die verstrichene Testzeit prozentual dar. Die blaue Linie repräsentiert die Lesegeschwindigkeit und die rote Linie die Schreibgeschwindigkeit. Die zugehörigen Transferraten sind auf der linken Achse aufgetragen und die untere Achse zeigt prozentual den von außen zur Spindel hin überfahrenen Medienbereich.</p>

  <p>Unterhalb des Graphen werden Werte für minimale, maximale und durchschnittliche Schreib- und Leseraten angezeigt, sowie die mittlere Zugriffszeit und die seit dem letzten Leistungstest vergangene Zeit.</p>

</page>
