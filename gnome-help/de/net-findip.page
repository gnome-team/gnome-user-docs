<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="de">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Kenntnis Ihrer IP-Adresse kann Ihnen bei der Lösung von Netzwerkproblemen helfen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021, 2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022-2024.</mal:years>
    </mal:credit>
  </info>

  <title>Ihre IP-Adresse ermitteln</title>

  <p>Die Kenntnis Ihrer IP-Adresse kann Ihnen bei der Lösung von Problemen mit Ihrer Internetverbindung helfen. Sie könnten überrascht sein, dass Sie <em>zwei</em> IP-Adressen haben: eine IP-Adresse für Ihren Rechner im internen Netzwerk und eine im Internet.</p>
  
  <section id="wired">
    <title>Ermitteln Sie die interne (Netzwerk-)IP-Adresse Ihrer Kabelverbindung</title>
  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Netzwerk</gui> in der Seitenleiste, um das Seitenfenster zu öffnen.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">Wenn mehrere Arten von kabelgebundenen Verbindungen verfügbar sind, sehen Sie zum Beispiel Begriffe wie <gui>PCI-Ethernet</gui> oder <gui>USB-Ethernet</gui> statt <gui>Kabelgebunden</gui>.</p>
      </note>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">Einstellungen</span></media> neben der aktiven Verbindung für die IP-Adresse und andere Details.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Ermitteln Sie die interne (Netzwerk-)IP-Adresse Ihrer Funkverbindung</title>
  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>WLAN</gui> in der Seitenleiste, um das Seitenfenster zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">Einstellungen</span></media> neben der aktiven Verbindung für die IP-Adresse und andere Details.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Ihre externe (Internet-)IP-Adresse ermitteln</title>
  <steps>
    <item>
      <p>Besuchen Sie <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Diese Seite zeigt Ihnen Ihre externen IP-Adressen an.</p>
    </item>
  </steps>
  <p>Abhängig von der Art, wie sich Ihr Rechner mit dem Internet verbindet, könnten die interne und die externe Adresse gleich sein.</p>  
  </section>

</page>
