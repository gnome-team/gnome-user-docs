<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Povežite zvučnike ili slušalice i izaberite osnovni izlazni zvučni uređaj.</desc>
  </info>

  <title>Koristite druge zvučnike ili slušalice</title>

  <p>Možete da koristite spoljne zvučnike ili slušalice sa vašim računarom. Zvučnici se obično priključuju korišćenjem kružnog TRS (<em>špic, prsten, naglavak</em>) priključka ili pomoću USB-a.</p>

  <p>Ako vaš mikrofon ima kružni priključak, samo ga priključite u odgovarajću zvučnu utičnicu na vašem računaru. Većina računara ima dve utičnice: jedna je za mikrofone a druga za zvučnike. Ta utičnica je obično svetlo-zelene boje ili se pored nje nalazi slika slušalica. Zvučnici ili slušalice uključene u TRS utičnicu obično će biti korišćeni po osnovi. Ako ne, pogledajte uputstva ispod za izbor osnovnog ulaznog uređaja.</p>

  <p>Neki računari podržavaju višekanalni izlaz za zvuk okruženja. Oni obično koriste više TRS utikača, koji su često kodirani bojom. Ako niste sigurni gde šta ide, možete da isprobate izlaz zvuka u podešavanjima.</p>

  <p>Ako imate USB zvučnike ili slušalice, ili analogne slušalice priključene na USB zvučnu karticu, priključite ih na neki USB priključak. USB zvučnici deluju kao zasebni zvučni uređaji, i moraćete da odredite koji zvučnici će biti korišćeni kao osnovni.</p>

  <steps>
    <title>Select a default audio output device</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Zvuk</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>In the <gui>Output</gui> section, select the device that you want to
      use.</p>
    </item>
  </steps>

  <p>Use the <gui style="button">Test</gui> button to check that all
  speakers are working and are connected to the correct socket.</p>

</page>
