<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="sr-Latn">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Smanjite ili povećajte sistem datoteka i njegovu particiju.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Podesite veličinu sistema datoteka</title>

  <p>Sistem datoteka se može povećati kako bi se iskoristio slobodni prostor nakon njegove particije. Često ovo je moguće čak i kada je sistem datoteka prikačen.</p>
  <p>Da napravite prostor za drugu particiju nakon sistema datoteka, može se smanjiti prema slobodnom prostoru unutar njega.</p>
  <p>Nemaju svi sistemi datoteka podršku promene veliččine.</p>
  <p>Veličina particije će se promeniti zajedno sa veličinom sistema datoteka. Takođe je moguće promeniti veličinu particije bez sistema datoteka na isti način.</p>

<steps>
  <title>Promenite veličinu sistema datoteka/particije</title>
  <item>
    <p>Otvorite program <app>Diskovi</app> iz pregleda <gui>Aktivnosti</gui>.</p>
  </item>
  <item>
    <p>Izaberite disk koji želite da proverite sa spiska skladišnih uređaja na levoj strani. Ako ima više od jednog volumena na disku, izaberite onaj koji sadrži sistem datoteka.</p>
  </item>
  <item>
    <p>Na traci alata ispod odeljka <gui>Volumeni</gui>, kliknite na dugme izbornika. Nakon toga kliknite <gui>Promeni veličinu sistema datoteka…</gui> ili <gui>Promeni veličinu…</gui> ako ne postoji sistem datoteka.</p>
  </item>
  <item>
    <p>Otvoriće se prozorče u kome se može izabrati nova veličina. Sistem datoteka biće prikačen da bi se izračunala najmanja veličina prema količini trenutnog sadržaja. Ako smanjivanje nije podržano najmanja veličina je trenutna veličina. Ostavite dovoljno prostora unutar sistema datoteka prilikom smanjivanja da osigurate da može raditi brzo i pouzdano.</p>
    <p>U zavisnosti od toga koliko podataka mora biti premešteno iz smanjenog dela, promena veličine sistema datoteka može potrajati duže.</p>
    <note style="warning">
      <p>Promena veličine sistema datoteka automatski uključuje <link xref="disk-repair">popravku</link> sistema datoteka. Zbog toga se savetuje da napravite rezervni primerak važnih podataka pre nego što počnete. Radnja se ne sme zaustaviti jer će rezultirati oštećenim sistemom datoteka.</p>
    </note>
  </item>
  <item>
      <p>Potvrdite početak radnje klikom na <gui style="button">Promeni veličinu</gui>.</p>
   <p>Radnja će otkačiti sistem datoteka ako promena veličine prikačenog sistema datoteka nije podržana. Budite strpljivi dok se menja veličina sistema datoteka.</p>
  </item>
  <item>
    <p>Nakon završetka potrebnih radnji promene veličine i popravake, sistem datoteka je spreman za ponovno korišćenje.</p>
  </item>
</steps>

</page>
