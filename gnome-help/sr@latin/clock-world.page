<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Majkl Hil</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prikažite vremena u drugim gradovima u kalendaru.</desc>
  </info>

  <title>Dodaj svetski sat</title>

  <p>Koristite <app>Satove</app> da dodate vremena u drugim gradovima.</p>

  <note>
    <p>Ovo zahteva da je instaliran program <app>Satovi</app>.</p>
    <p>Na većini distribucija program <app>Satovi</app> je već podrazumevano instaliran. Ako na vašoj nije, moraćete da ga instalirate koristeći upravnika paketa vaše distribucije.</p>
  </note>

  <p>Da dodate svetski sat:</p>

  <steps>
    <item>
      <p>Click the clock on the top bar.</p>
    </item>
    <item>
      <p>Click the <gui>Add world clocks…</gui> button under the calendar to
      launch <app>Clocks</app>.</p>

    <note>
       <p>Ako već imate jedan ili nekoliko svetskih satova, pritisnite na neki od njih i program <app>Satovi</app> biće pokrenut.</p>
    </note>

    </item>
    <item>
      <p>In the <app>Clocks</app> window, click
      <gui style="button">+</gui> button or press
      <keyseq><key>Ctrl</key><key>N</key></keyseq> to add a new city.</p>
    </item>
    <item>
      <p>Počnite da upisujete naziv grada u polju za pretragu.</p>
    </item>
    <item>
      <p>Izaberite odgovarajući grad ili vama najbliže mesto sa spiska.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="button">Dodaj</gui> da završite dodavanje grada.</p>
    </item>
  </steps>

  <p>Pogledajte <link href="help:gnome-clocks">Pomoć za satove</link> da vidite i druge mogućnosti <app>Satova</app>.</p>

</page>
