<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Upravljajte i organizujte datoteke sa upravnikom datoteka.</desc>
  </info>

<title>Pregledajte datoteke i fascikle</title>

<p>Koristite upravnika datoteka <app>Nautilus</app> da razgledate i organizujete datoteke na vašem računaru. Možete takođe da ga koristite za rukovanje datotekama na skladišnim uređajima (kao što su spoljni čvrsti diskovi), na <link xref="nautilus-connect">serverima datoteka</link>, i na mrežnim deljenjima.</p>

<p>Da pokrenete upravnika datoteka, otvorite <app>Datoteke</app> u pregledu <gui xref="shell-introduction#activities">Aktivnosti</gui>. Možete takođe da tražite datoteke i fascikle u pregledu na isti način kao kada biste <link xref="shell-apps-open">tražili programe</link>.</p>

<section id="files-view-folder-contents">
  <title>Istražite sadržaje fascikli</title>

<p>U upravniku datoteka, kliknite dva puta na neku fasciklu da vidite njen sadržaj, i kliknite dva puta ili <link xref="mouse-middleclick">srednjim tasterom</link> na neku datoteku da je otvorite osnovnim programom za tu datoteku. Pritisnite srednjim tasterom miša na fasciklu da je otvorite u novom jezičku. Možete takođe da kliknete desnim tasterom miša na fasciklu da je otvorite u novom jezičku ili u novom prozoru.</p>

<p>Kada razgledate datoteke u fascikli, možete brzo da <link xref="files-preview">prikažete pregled svake datoteke</link> tako što ćete pritisnuti razmaknicu na tastaturi da se uverite da je to odgovarajuća datoteka pre nego li je otvorite, umnožite, ili obrišete.</p>

<p><em>Traka putanje</em> iznad spiska datoteka i fascikli vam pokazuje koju fasciklu razgledate, uključujući nadređene fascikle tekuće fascikle. Kliknite na nadređenu fasciklu na traci putanje da se prebacite u tu fasciklu. Kliknite desnim tasterom miša na bilo koju fasciklu na traci putanje da je otvorite u novom jezičku ili prozoru, ili da pristupite njenim osobinama.</p>

<p>Ako želite da na brzinu <link xref="files-search">potražite datoteku</link>, u ili ispod fascikle koju razgledate, počnite da kucate njen naziv. Pojaviće se <em>polje za pretragu</em> pri vrhu prozora i samo datoteke koje odgovaraju vašoj pretrazi biće prikazane. Pritisnite <key>Esc</key> da otkažete pretragu.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, press the <gui>Show Sidebar</gui> button in the top-left
corner of the window. You can <link xref="nautilus-bookmarks-edit">add bookmarks
to folders that you use often</link> and they will appear in the sidebar.</p>

</section>

</page>
