<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-device-drivers" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Priložnici vikija Ubuntuove dokumentacije</name>
    </credit>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Neki upravljački programi uređaja neće raditi kako treba sa određenim bežičnim prilagođivačima, tako da ćete morati da pronađete bolji.</desc>
  </info>

  <title>Rešavanje problema bežične mreže</title>

  <subtitle>Uverite se da su instalirani upravljački programi radnog uređaja</subtitle>

<!-- Needs links (see below) -->

  <p>U ovom koraku možete da proverite da vidite da li možete da nabavite radne upravljačke programe uređaja za vaš bežični prilagođivač. <em>Upravljački program uređaja</em> je komad softvera koji govori računaru kako da natera hardverski uređaj da radi ispravno. Čak i ako je bežični prilagođivač prepoznat od strane računara, možda nema upravljačke programe koji rade dobro. Možete biti u mogućnosti da pronađete drugačije upravljačke programe za bežični prilagođivač koji će završiti posao. Pokušajte neku od sledećih opcija:</p>

  <list>
    <item>
      <p>Proverite da li je vaš bežični prilagođivač na spisku podržanih uređaja.</p>
      <p>Most Linux distributions keep a list of wireless devices that they
      have support for. Sometimes, these lists provide extra information on how
      to get the drivers for certain adapters working properly. Go to the list
      for your distribution (for example,
      <link href="https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported">Ubuntu</link>,
      <link href="https://wiki.archlinux.org/index.php/Wireless_network_configuration">Arch</link>,
      <link href="https://wireless.wiki.kernel.org/en/users/Drivers">Fedora</link> or
      <link href="http://en.opensuse.org/HCL:Network_(Wireless)">openSUSE</link>)
      and see if your make and model of wireless adapter is listed. You may be
      able to use some of the information there to get your wireless drivers
      working.</p>
    </item>
    <item>
      <p>Potražite ograničene (izvršne) upravljačke programe.</p>
      <p>Mnoge distribucije Linuksa dolaze samo sa upravljačkim programima uređaja koji su <em>slobodni</em> i <em>otvorenog koda</em>. Ovo je zbog toga što ne mogu da dostavljaju upravljačke programe koji su vlasnički, ili zatvorenog koda. Ako je pravi upravljački program za vaš bežični prilagođivač dostupan jedino u neslobodnom, ili „izvršnom-samo“ izdanju, neće biti unapred instaliran. U ovom slučaju, pogledajte na veb sajtu proizvođača bežičnog prilagođivača da li imaju neki upravljački program za Linuks.</p>
      <p>Neke distribucije Linuksa imaju alat koji može da vam preuzme ograničene upravljačke programe. Ako vaša distribucija ima jedan od njih, upotrebite ga da vidite da li može da vam pronađe neki bežični upravljački program.</p>
    </item>
    <item>
      <p>Upotrebite Vindouzove upravljačke programe za vaš prilagođivač.</p>
      <p>In general, you cannot use a device driver designed for one operating
      system (like Windows) on another operating system (like Linux). This is
      because they have different ways of handling devices. For wireless
      adapters, however, you can install a compatibility layer called
      <em>NDISwrapper</em> which lets you use some Windows wireless drivers on
      Linux. This is useful because wireless adapters almost always have
      Windows drivers available for them, whereas Linux drivers are sometimes
      not available. You can learn more about how to use NDISwrapper
      <link href="https://sourceforge.net/p/ndiswrapper/ndiswrapper/Main_Page/">here</link>.
      Note that not all wireless drivers can be used through NDISwrapper.</p>
    </item>
  </list>

  <p>Ako nijedna od ovih opcija ne bude radila, možete isprobati neki drugi bežični prilagođivač da vidite da li bi radio. USB bežični prilagođivači su često vrlo jeftini, i priključuju se na skoro svaki računar. Ipak, pre nego što ga kupite proverite da li je saglasan sa vašom distribucijom Linuksa.</p>

</page>
