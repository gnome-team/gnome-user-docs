<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author">
      <name>Džim Kembel</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Pol V. Frilds</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izaberite štampač koji najčešće koristite.</desc>
  </info>

  <title>Podesite osnovni štampač</title>

  <p>Ako imate više od jednog štampača, možete da izaberete koji će biti vaš osnovni štampač. Možda ćete poželeti da odaberete štampač koji koristite najčešće.</p>

  <note>
    <p>Potrebna su vam <link xref="user-admin-explain">administratorska prava</link> na sistemu da postavite osnovni štampač.</p>
  </note>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Štampači</gui>.</p>
    </item>
    <item>
      <p>Kliknite <gui>Štampači</gui>.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the printer.</p>
    </item>
    <item>
      <p>Select the <gui style="menuitem">Use Printer by Default</gui> checkbox.</p>
    </item>
  </steps>

  <p>Kada štampate u nekom programu, osnovni štampač biva samostalno izabran, osim ako ne izaberete drugi štampač.</p>

</page>
