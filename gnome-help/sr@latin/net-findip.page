<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Poznavanje vaše IP adrese može da vam pomogne u rešavanju mrežnih problema.</desc>
  </info>

  <title>Saznajte vašu IP adresu</title>

  <p>Poznavanje vaše IP adrese može da vam pomogne da rešite probleme vaše veze na internet. Možda ćete biti iznenađeni kada budete saznali da imate <em>dve</em> IP adrese: jedna IP adresa za vaš računar na unutrašnjoj mreži i jedna IP adresa za vaš računar na internetu.</p>
  
  <section id="wired">
    <title>Find your wired connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> in the sidebar to open the panel.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">
        If more than one type of wired connected is available, you might see
        names like <gui>PCI Ethernet</gui> or <gui>USB Ethernet</gui> instead
        of <gui>Wired</gui>.</p>
      </note>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Find your wireless connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Saznajte vašu spoljnu (internet) IP adrsesu</title>
  <steps>
    <item>
      <p>Visit
      <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Sajt će vam prikazati vašu spoljnu IP adresu.</p>
    </item>
  </steps>
  <p>Depending on how your computer connects to the internet, the internal and
  external addresses may be the same.</p>  
  </section>

</page>
