<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Neki bežični uređaji imaju probleme sa radom kada je vaš računar obustavljen a nije se povratio kako treba.</desc>
  </info>

  <title>Nemam bežičnu mrežu kada probudim računar</title>

  <p>Ako ste obustavili vaš računar, možete uvideti da vaša bežična internet veza ne radi kada ga ponovo povratite. To se dešava kada <link xref="hardware-driver">upravljački programi</link> bežičnog uređaja ne podržavaju u potpunosti neke funkcije za uštedu energije. Uobičajeno, bežične veze ne uspevaju da se uključe ispravno kada se računar povrati.</p>

  <p>Ako se ovo desi, pokušajte da isključite i da ponovo uključite bežičnu karticu:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Wi-Fi</gui> switch at the top-right of the window to
      off and then on again.</p>
    </item>
    <item>
      <p>If the wireless still does not work, switch the <gui>Airplane
      Mode</gui> switch to on and then switch it off again.</p>
    </item>
  </steps>

  <p>Ako ovo ne radi, ponovno pokretanje računara bi trebalo da učini da bežična veza ponovo proradi. Ako još uvek imate problema nakon toga, povežite se na internet koristeći žičanu vezu i ažurirajte vaš računar.</p>

</page>
