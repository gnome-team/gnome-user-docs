<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Džim Kembel</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Otkažite zakazani posao štampanja i uklonite ga iz reda.</desc>
  </info>

  <title>Otkažite, pauzirajte ili otpustite posao štampanja</title>

  <p>Možete da otkažete zakazani posao štampanja i da ga uklonite iz reda u podešavanjima štampača.</p>

  <section id="cancel-print-job">
    <title>Otkažite posao štampanja</title>

  <p>Ako ste slučajno započeli štampanje dokumenta, možete da otkažete štampanje tako da ne morate da trošite mastilo ili papir.</p>

  <steps>
    <title>Kako da otkažete posao štampanja:</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Štampači</gui>.</p>
    </item>
    <item>
      <p>Pritisnite na <gui>Štampači</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Click the button next to the printer which shows the number of jobs.</p>
    </item>
    <item>
      <p>Otkažite posao štampanja pritiskom na dugme za zaustavljanje.</p>
    </item>
  </steps>

  <p>Ako ovo ne otkaže posao štampanja kao što ste očekivali, pokušajte da držite pritisnutim dugme <em>Cancel</em> (Otkaži) na vašem štampaču.</p>

  <p>Kao poslednji pokušaj, naročito ako imate poveći posao štampanja sa mnogo stranica koje ne želite da otkažete, uklonite papir iz ulazne fioke štampača. Štampač će zaključiti da nema više papira i zaustaviće štampanje. Možete zatim ponovo da pokušate da otkažete posao štampanja, ili da ugasite štampač i da ga upalite ponovo.</p>

  <note style="warning">
    <p>Budite pažljivi da ne oštetite štampač kada uklanjate papir. Ako budete morali snažno da povučete papir da biste ga uklonili, najbolje će biti da ga ostavite tamo gde jeste.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Otkažite i otpustite posao štampanja</title>

  <p>Ako želite da pauzirate ili da otpustite posao štampanja, možete da uradite ovo tako što ćete otići na prozorče poslova u podešavanjima štampača i pritisnuti odgovarajuće dugme.</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Štampači</gui>.</p>
    </item>
    <item>
      <p>Pritisnite na <gui>Štampači</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Click the button next to the printer which shows the number of jobs.</p>
    </item>
    <item>
      <p>Pause or release the print job by clicking the corresponding button.</p>
    </item>
  </steps>

  </section>

</page>
