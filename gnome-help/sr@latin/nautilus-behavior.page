<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Izvršite jedan klik da otvorite datoteke, pokrenete ili pogledate izvršne tekstualne datoteke, i da odredite ponašanje smeća.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Postavke ponašanja upravnika datoteka</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click the menu
button in the sidebar of the window, select <gui>Preferences</gui>,
then go to the <gui>General</gui> section.</p>

<section id="behavior">
<title>General</title>
<terms>
 <item>
  <title><gui>Action to Open Items</gui></title>
  <p>Po osnovi, jedan klik bira datoteke a dvoklik ih otvara. Umesto ovoga možete da izaberete da se i datoteke i fascikle otvore kada na njih kliknete jednom. Kada koristite režim jednog klika, možete da držite pritisnutim taster <key>Ktrl</key> dok klikate da izaberete jednu ili više datoteka.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Izvršne tekstualne datoteke</title>
 <p>Izvršna tekstualna datoteka je datoteka koja sadrži program koji možete da pokrenete (izvršite). <link xref="nautilus-file-properties-permissions">Ovlašćenja datoteke</link> moraju takođe da dopuštaju da datoteka bude pokrenuta kao program. Najčešće su skripte <sys>Školjke</sys>, <sys>Pitona</sys> i <sys>Perla</sys>. Njihova proširenja su redom <file>.sh</file>, <file>.py</file> i <file>.pl</file>.</p>

 <p>Izvršne tekstualne datoteke se takođe nazivaju <em>skriptama</em>. Sve skripte u fascikli <file>~/.local/share/nautilus/scripts</file> će se pojaviti u priručnom izborniku za datoteku pod podizbornikom <gui style="menuitem">Skripte</gui> submenu. Kada se skripta izvrši iz mesne fascikle, sve izabrane datoteke će joj biti prosleđene kao parametri. Da izvršite skriptu nad datotekom:</p>

<steps>
  <item>
    <p>Idite do željene fascikle.</p>
  </item>
  <item>
    <p>Izaberite željenu datoteku.</p>
  </item>
  <item>
    <p>Kliknite desnim tasterom miša nad datotekom da otvorite priručni izbornik i izaberite željenu skriptu za izvršavanje iz izbornika <gui style="menuitem">Skripte</gui>.</p>
  </item>
</steps>

 <note style="important">
  <p>Skripti neće biti prosleđeni nikakvi parametri kada se izvršava iz udaljene fascikle kao što je fascikla koja pokazuje veb ili <sys>ftp</sys> sadržaj.</p>
 </note>

</section>

</page>
