<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Saveti kao što je „Nemojte dozvoliti da se baterija previše isprazni“.</desc>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Izvucite što više iz baterije prenosnog računara</title>

<p>Kako baterije prenosnih računara stare, sve teže se pune i njihov kapacitet se postepeno smanjuje. Postoje nekoliko tehnika koje možete da koristite da produžite njihovo korisno radno vreme, mada ne biste trebali da očekujete veliku razliku.</p>

<list>
  <item>
    <p>Nemojte dozvoliti da se baterija u potpunosti isprazni. Uvek dopunite bateriju <em>pre</em> nego što se isprazni, mada većina baterija imaju ugrađenu samozaštitu koja ne dozvoljava da se baterija isprazni previše. Dopunjavanje baterije kada je samo delimično ispražnjena je mnogo delotvornije, ali ako je dopunite kada je samo delimično ispražnjena je loše za bateriju.</p>
  </item>
  <item>
    <p>Toplota ima štetno dejstvo na delotvornost punjenja baterije. Ne dozvolite da se baterija pregreje više nego što mora.</p>
  </item>
  <item>
    <p>Baterije stare čak i ako ih ostavite uskladištenim. Mala je prednost ako kupite rezervnu bateriju u isto vreme kada i originalnu — uvek kupite zamenu onda kada vam zatreba.</p>
  </item>
</list>

<note>
  <p>Ovaj savet je naročito primenjiv za Litijum-Jonske (Li-Ion) baterije, koje su najzastupljenije. Ostale vrste baterija se mogu ponašati drugačije.</p>
</note>

</page>
