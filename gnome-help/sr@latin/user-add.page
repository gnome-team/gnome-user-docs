<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dodajte nove korisnike tako da neko drugi može da se prijavi na računar.</desc>
  </info>

  <title>Dodajte nalog novog korisnika</title>

  <p>Možete da dodate više korisničkih naloga na vaš računar. Dodelite jedan nalog svakoj osobi u vašem domaćinstvu ili preduzeću. Svaki korisnik ima svoju ličnu fasciklu, dokumenta, i podešavanja.</p>

  <p>Potrebna su vam <link xref="user-admin-explain">administratorska prava</link> za dodavanje korisničkih naloga.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="button">Otključaj</gui> u gornjem desnom uglu i unesite vašu lozinku.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add User...</gui> button under
      <gui>Other Users</gui> to add a new user account.</p>
    </item>
    <item>
      <p>Ako želite da novi korisnik ima <link xref="user-admin-explain">administratorski pristup</link> računaru, za vrstu naloga izaberite <gui>Administrator</gui>.</p>
      <p>Administratori mogu da dodaju i da brišu korisnike, instaliraju softver i upravljačke programe i da izmene datum i vreme.</p>
    </item>
    <item>
      <p>Unesite ime i prezime novog korisnika. Korisničko ime će biti popunjeno na osnovu imena i prezimena. Ako vam se ne sviđa predloženo, možete da ga izmenite.</p>
    </item>
    <item>
      <p>You can choose to set a password for the new user, or let them set it
      themselves on their first login. If you choose to set the password now,
      you can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
      <p>To connect the user to a network domain, click
      <gui>Enterprise Login</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Add</gui>. When the user has been added,
      <gui>Parental Controls</gui> and <gui>Language</gui> settings can be
      adjusted.</p>
    </item>
  </steps>

  <p>Ako želite da izmenite lozinku nakon stvranja naloga, izaberite nalog, <gui style="button">otključajte</gui> panel i pritisnite na stanje tekuće lozinke.</p>

  <note>
    <p>In the <gui>Users</gui> panel, you can click the image next to the
    user’s name to the right to set an image for the account. This image will
    be shown in the login window. The system provides some stock photos you can
    use, or you can select your own or take a picture with your webcam.</p>
  </note>

</page>
