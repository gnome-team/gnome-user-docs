<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Šoba Tjagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pritisnite <keyseq><key>Super</key><key>Tab</key></keyseq>.</desc>
  </info>

<title>Prebacujte se između prozora</title>

  <p>Možete videti sve pokrenute programe koji imaju grafičko korisničko sučelje u <em>prebacivaču prozora</em>. Ovo čini prebacivanje između zadataka postupak jednog koraka i obezbeđuje punu sliku pokrenutih programa.</p>

  <p>Iz radnog prostora:</p>

  <steps>
    <item>
      <p>Pritisnite <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> da prikažete <gui>prebacivač prozora</gui>.</p>
    </item>
    <item>
      <p>Otpustite <key xref="keyboard-key-super">Super</key> da izaberete sledeći (označeni) prozor u prebacivaču.</p>
    </item>
    <item>
      <p>Otherwise, still holding down the <key xref="keyboard-key-super">Super</key>
      key, press <key>Tab</key> to cycle through the list of open
      windows, or <keyseq><key>Shift</key><key>Tab</key></keyseq> to cycle
      backwards.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">Možete da koristite spisak prozora na donjoj traci da pristupite svim vašim otvorenim prozorima i da se brzo prebacujete između njih.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>Prozori u prebacivaču prozora su grupisani po programima. Pregledi programa sa više prozora bivaju prikazani na klik. Držite pritisnutim <key xref="keyboard-key-super">Super</key> i pritisnite <key>`</key> (ili taster iznad tastera <key>Tab</key>) da se krećete po spisku.</p>
  </note>

  <p>Možete takođe da se pomerate između ikonica programa u prebacivaču prozora tasterima <key>→</key> ili <key>←</key>, ili da izaberete jedan tako što ćete kliknuti mišem na njega.</p>

  <p>Pregledi programa sa jednim prozorom mogu biti prikazani tasterom <key>↓</key>.</p>

  <p>Iz pregleda <gui>aktivnosti</gui>, pritisnite na <link xref="shell-windows">prozor</link> da se prebacite na njega i da napustite pregled. Ako imate otvorenih više <link xref="shell-windows#working-with-workspaces">radnih prostora</link>, možete da kliknete na svaki radni prostor da vidite otvorene prozore na svakom radnom prostoru.</p>

</page>
