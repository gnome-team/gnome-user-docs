<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>
    <revision version="gnome:45" date="2024-04-04" status="candidate"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Učinite datoteku nevidljivom, tako da nećete moći da je vidite u upravniku datoteka.</desc>
  </info>

<title>Sakrijte datoteku</title>

  <p>Upravnik datoteka <app>Datoteke</app> vam pruža mogućnost da sakrijete i da prikažete datoteke prema vašoj želji. Kada je datoteka skrivena, nije prikazana u upravniku datoteka, ali se još uvek nalazi u fascikli.</p>

  <p>Da sakrijete datoteku <link xref="files-rename">preimenujte je</link> tačkom <file>.</file> na početku njenog naziva. Na primer, da sakrijete datoteku pod nazivom <file>primer.txt</file>, trebate da je preimenujete u <file>.primer.txt</file>.</p>

<note>
  <p>Možete da sakrijete fascikle na isti način na koji možete da sakrijete datoteke. Sakrijte fasciklu tako što ćete postaviti <file>.</file> na početak njenog naziva.</p>
</note>

<section id="show-hidden">
 <title>Prikažite sve skrivene datoteke</title>

  <p>If you want to see all hidden files in a folder, go to that folder and
  either press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again,
  either press the menu button in the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>Otkrijte datoteku</title>

  <p>To unhide a file, go to the folder containing the hidden file. Press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either press the menu button in
  the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>Po osnovi, videćete samo skrivene datoteke u upravniku datoteka sve dok ga ne zatvorite. Da izmenite ovo podešavanje tako da upravnik datoteka uvek prikazuje skrivene datoteke, pogledajte <link xref="nautilus-views"/>.</p></note>

  <note><p>Većina skrivenih datoteka će imati <file>.</file> na početku njihovog naziva, ali neke druge mogu imati <file>~</file> na kraju njihovog naziva umesto ovoga. Te datoteke su datoteke rezerve. Pogledajte <link xref="files-tilde"/> za više podataka.</p></note>

</section>

</page>
