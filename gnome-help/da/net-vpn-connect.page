<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="da">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Opsæt en VPN-forbindelse til et lokalt netværk over internettet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Opret forbindelse til et VPN</title>

<p>Et VPN (eller <em>Virtuelt Privat Netværk</em>) er en måde at oprette forbindelse til et lokalt netværk over internettet på. Antag, at du vil oprette forbindelse til det lokale netværk på din arbejdsplads, mens du er på forretningsrejse. Du ville finde en internetforbindelse et sted (som på et hotel) og derefter oprette forbindelse til din arbejdsplads's VPN. Det ville være, som om du var direkte forbundet til netværket på arbejdet, men selve netværksforbindelsen ville være via hotellets internetforbindelse. VPN-forbindelser er normalt <em>krypteret</em> for at forhindre folk i at få adgang til det lokale netværk, du opretter forbindelse til, uden at logge ind.</p>

<p>Der findes en række forskellige slags VPN. Du skal muligvis installere noget ekstra software afhængigt af hvilken type VPN, du opretter forbindelse til. Find ud af forbindelsesdetaljerne fra den, der har ansvaret for VPN'et, og se hvilken <em>VPN-klient</em> du skal bruge. Gå derefter til softwareinstallationsprogrammet og søg efter pakken <app>NetworkManager</app>, der fungerer med din VPN (hvis der er en), og installér den.</p>

<note>
 <p>Hvis der ikke er en NetworkManager-pakke til din type VPN, skal du sandsynligvis downloade og installere noget klientsoftware fra det firma, der leverer VPN-softwaren. Du skal sandsynligvis følge nogle forskellige instruktioner for at få det til at virke.</p>
</note>

<p>Opsæt VPN-forbindelsen:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Netværk</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Netværk</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Klik på knappen <gui>+</gui> ved siden af <gui>VPN</gui>-afsnittet for at tilføje en ny forbindelse.</p>
    </item>
    <item>
      <p>Vælg den type VPN-forbindelse, du har.</p>
    </item>
    <item>
      <p>Udfyld VPN-forbindelsesoplysningerne, og tryk derefter på <gui>Tilføj</gui>, når du er færdig.</p>
    </item>
    <item>
      <p>When you have finished setting up the VPN, open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right side of
      the top bar, click the VPN connection, and select <gui>Connect</gui>. You
      may need to enter a password for the connection before it is established.
      Once the connection is made, you will see a lock shaped icon in the top
      bar.</p>
    </item>
    <item>
      <p>Forhåbentlig lykkes det dig at oprette forbindelse til VPN'et. Hvis ikke, skal du muligvis dobbelttjekke de VPN-indstillinger, du har indtastet. Du kan gøre dette fra panelet <gui>Netværk</gui>, som du brugte til oprettelse af forbindelsen. Vælg VPN-forbindelsen fra listen, og tryk derefter på knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">indstillinger</span></media> for at gennemgå indstillingerne.</p>
    </item>
    <item>
      <p>For at afbryde forbindelsen til VPN skal du klikke på systemmenuen i toppanelet og klikke på <gui>Sluk</gui> under navnet på din VPN-forbindelse.</p>
    </item>
  </steps>

</page>
