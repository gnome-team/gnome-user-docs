<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-closelid" xml:lang="da">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bærbare computere sover for at spare på strømmen, når du lukker låget.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Hvorfor slukker min computer, når jeg lukker låget?</title>

  <p>Når du lukker låget på din bærbare computer, vil din computer <link xref="power-suspend"><em>gå i hviletilstand</em></link> for at spare strøm. Det betyder, at computeren faktisk ikke er slukket; den er bare gået i dvale. Du kan vække den ved at åbne låget. Hvis det ikke vækker den, kan du prøve at klikke med musen eller trykke på en tast. Hvis det stadig ikke virker, skal du trykke på tænd/sluk-knappen.</p>

  <p>Nogle computere er ikke i stand til korrekt at gå i hviletilstand, normalt fordi deres hardware ikke understøttes fuldstændigt af styresystemet (f.eks. er Linux-driverne ufuldstændige). I dette tilfælde kan du opleve, at du ikke er i stand til at vække din computer, efter du har lukket låget. Du kan prøve at <link xref="power-suspendfail">fikse problemet med hviletilstand</link>, eller du kan forhindre computeren i at forsøge at gå i hviletilstand, når du lukker låget.</p>

<section id="nosuspend">
  <title>Forhindr computeren i at gå i hviletilstand, når låget lukkes</title>

  <note style="important">
    <p>Disse instruktioner virker kun, hvis du bruger <app>systemd</app>. Tjek din distribution for mere information.</p>
  </note>

  <note style="important">
    <p>Du skal have installeret <app>Tilpasninger</app> på din computer for at ændre denne indstilling.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Installér <app>Tilpasninger</app></link></p>
    </if:if>
  </note>

  <p>Hvis du ikke ønsker, at computeren skal gå i hviletilstand, når du lukker låget, kan du ændre indstillingen for den opførsel.</p>

  <note style="warning">
    <p>Vær meget forsigtig, hvis du ændrer denne indstilling. Nogle bærbare kan overophedes, hvis de efterlades kørende med låget lukket, især hvis de er på et dårligt ventileret sted som en rygsæk.</p>
  </note>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Tilpasninger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilpasninger</gui> for at åbne programmet.</p>
    </item>
    <item>
      <p>Vælg fanebladet <gui>Generelt</gui>.</p>
    </item>
    <item>
      <p>Sluk for <gui>Sæt i hviletilstand, når låget lukkes på den bærbare</gui>.</p>
    </item>
    <item>
      <p>Luk vinduet <gui>Tilpasninger</gui>.</p>
    </item>
  </steps>

</section>

</page>
