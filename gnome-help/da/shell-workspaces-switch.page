<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="da">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Brug arbejdsområdevælgeren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

 <title>Skift mellem arbejdsområder</title>

 <steps>  
 <title>Med musen:</title>
 <item>
  <p if:test="!platform:gnome-classic">Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui>.</p>
  <p if:test="platform:gnome-classic">Klik på et af de fire arbejdsområder nederst til højre på skærmen for at aktivere arbejdsområdet.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Klik på det delvist viste arbejdsområde i højre side af det aktuelle arbejdsområde for at se de åbne vinduer på det næste arbejdsområde. Hvis det aktuelt valgte arbejdsområde ikke er længst til venstre, skal du klikke på det delvist viste arbejdsområde i venstre side for at se det forrige arbejdsområde.</p>
  <p>Hvis der bruges mere end ét arbejdsområde, kan du også klikke på <link xref="shell-workspaces">arbejdsområdevælgeren</link> mellem søgefeltet og vindueslisten for at få direkte adgang til et andet arbejdsområde.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Klik på arbejdsområdet for at aktivere arbejdsområdet.</p>
 </item>
 </steps>
 
 <list>
 <title>Med tastaturet:</title>  
  <item>
    <p if:test="!platform:gnome-classic">Press
    <keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq>
    or <keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq> to move to
    the workspace shown left of the current workspace in the workspace selector.
    </p>
    <p if:test="platform:gnome-classic">Tryk på <keyseq><key>Ctrl</key> <key>Alt</key><key>←</key></keyseq> for at flytte til det arbejdsområde der vises til venstre for det nuværende arbejdsområde i <em>arbejdsområdevælgeren</em>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Press <keyseq><key>Super</key><key>Page Down</key></keyseq> or
    <keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq> to move to the
    workspace shown right of the current workspace in the workspace selector.</p>
    <p if:test="platform:gnome-classic">Tryk på <keyseq><key>Ctrl</key> <key>Alt</key><key>→</key></keyseq> for at flytte til det arbejdsområde der vises til højre for det nuværende arbejdsområde i <em>arbejdsområdevælgeren</em>.</p>
  </item>
 </list>

</page>
