<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="da">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Brug større skrifttyper for at gøre teksten lettere at læse.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Skift tekststørrelse på skærmen</title>

  <p>Hvis du har svært ved at læse teksten på skærmen, så kan du ændre størrelsen på skrifttypen.</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Tilgængelighed</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilgængelighed</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Vælg afsnittet <gui>Syn</gui> for at åbne det.</p>
    </item>
    <item>
      <p>Tænd for <gui>Stor tekst</gui>-kontakten.</p>
    </item>
  </steps>

  <p>Eller du kan hurtigt ændre tekststørrelsen ved at klikke på <link xref="a11y-icon">tilgængelighedsikonet</link> i toppanelet og vælge <gui>Stor tekst</gui>.</p>

  <note style="tip">
    <p>I mange programmer kan du når som helst gøre teksten større ved at trykke på <keyseq><key>Ctrl</key><key>+</key></keyseq>. Tryk på <keyseq><key>Ctrl</key><key>-</key></keyseq> for at gøre teksten mindre.</p>
  </note>

  <p><gui>Stor tekst</gui> skalerer teksten 1,2 gange. Du kan bruge <app>Tilpasninger</app> til at gøre teksten større eller mindre.</p>

</page>
