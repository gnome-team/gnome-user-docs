<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="da">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Den unikke identifikator som netværkshardwaren har fået tildelt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Hvad er en MAC-adresse?</title>

  <p>En <em>MAC-adresse</em> er den unikke identifikator, der er tildelt af producenten til et stykke netværkshardware (som et trådløst kort eller et Ethernet-kort). MAC står for <em>Media Access Control</em>, og hver identifikator er beregnet til at være unik for en bestemt enhed.</p>

  <p>En MAC-adresse består af seks sæt af to tegn (i hexadecimal notation: 0..9 og A..F) hver adskilt af et kolon. <code>00:1B:44:11:3A:B7</code> er et eksempel på en MAC-adresse.</p>

  <p>Find din egen netværkshardwares MAC-adresse:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Netværk</gui> for kablede forbindelser eller <gui>Wi-fi</gui> for trådløse forbindelser.</p>
    </item>
    <item>
      <p>Klik på <gui>Netværk</gui> eller <gui>Wi-fi</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Klik på knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">indstillinger</span></media> ved siden af den aktive forbindelse.</p>
    </item>
    <item>
      <p>Enhedens MAC-adresse vil blive vist som <gui>Hardwareadressen</gui> i panelet <gui>Detaljer</gui>.</p>
    </item>
  </steps>

  <p>I praksis kan det være nødvendigt at ændre eller “spoofe” en MAC-adresse. For eksempel kan nogle internetudbydere kræve, at en specifik MAC-adresse bruges til at få adgang til deres tjeneste. Hvis netværkskortet holder op med at virke, og du skal sæt et nyt kort i, fungerer tjenesten ikke længere. I sådanne tilfælde kan du “forfalske” MAC-adressen.</p>

</page>
