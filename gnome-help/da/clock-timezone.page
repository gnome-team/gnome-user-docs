<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-timezone" xml:lang="da">

  <info>
    <link type="guide" xref="clock"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.16" date="2015-01-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Jim Campbell</name>
       <email>jcampbell@gnome.org</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Opdater din tidszone til din nuværende placering, så dit klokkeslæt er korrekt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Skift din tidszone</title>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Date &amp; Time</gui> to open the panel.</p>
    </item>
    <item>
      <p>Hvis der er tændt for <gui>Automatisk tidszone</gui>-kontakten, så bør din tidszone automatisk blive opdateret hvis du har en internetforbindelse og <link xref="privacy-location">funktionaliteten for placeringstjeneste</link> er aktiveret. Sluk for den, for at opdatere din tidszone manuelt.</p>
    </item>
    <item>
      <p>Click <gui>Time Zone</gui>, then search for your current city.</p>
    </item>
  </steps>

  <p>Tiden opdateres automatisk når du vælger en anden placering. Det kan også være du ønsker at <link xref="clock-set">indstille uret manuelt</link>.</p>

</page>
