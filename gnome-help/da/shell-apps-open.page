<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="da">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Start programmer fra <gui>Aktivitetsoversigten</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Start programmer</title>

  <p if:test="!platform:gnome-classic">Flyt din musemarkør til hjørnet med <gui>Aktiviteter</gui> øverst til venstre på skærmen for at vise <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui>. Her finder du alle dine programmer. Du kan også åbne oversigten ved at trykke på <key xref="keyboard-key-super">Super</key>-tasten.</p>
  
  <p if:test="platform:gnome-classic">Du kan starte programmer fra <gui xref="shell-introduction#activities">Programmer</gui>-menuen øverst til venstre på skærmen, eller du kan bruge <gui>Aktivitetsoversigten</gui> ved at trykke på <key xref="keyboard-key-super">Super</key>-tasten.</p>

  <p>Der er flere måder hvorpå du kan åbne et program fra <gui>Aktivitetsoversigten</gui>:</p>

  <list>
    <item>
      <p>Begynd at skrive navnet på et program — søgningen starter med det samme (sker det ikke, så klik på søgelinjen øverst på skærmen og begynd at skrive). Kender du ikke det præcise navn på et program, så prøv at skrive relaterede termer. Klik på programmets ikon for at starte det.</p>
    </item>
    <item>
      <p>Nogle programmer har ikoner i <em>favoritområdet</em> (den vandrette stribe ikoner i bunden af <gui>Aktivitetsoversigten</gui>). Klik på et af dem for at starte det tilhørende program.</p>
      <p>Hvis du har programmer, som du bruger meget tit, kan du selv <link xref="shell-apps-favorites">tilføje dem i favoritområdet</link>.</p>
    </item>
    <item>
      <p>Klik på gitterknappen (den med ni prikker) i favoritområdet. Du få vist den første side over alle installerede programmer. For at se flere programmer skal du trykke på prikkerne i bunden over favoritområdet. Tryk på programmet for at starte det.</p>
    </item>
    <item>
      <p>Du kan starte et program i et separat <link xref="shell-workspaces">arbejdsområde</link> ved at trække dets ikon fra favoritområdet og slippe det i et af arbejdsområderne. Programmet åbnes i det valgte arbejdsområde.</p>
      <p>Du kan starte et program i et <em>nyt</em> arbejdsområde ved at trække dets ikon til et tomt arbejdsområde eller til det lille mellemrum mellem to arbejdsområder.</p>
    </item>
  </list>

  <note style="tip">
    <title>Kør hurtigt en kommando</title>
    <p>Du kan også starte et program ved at trykke på <keyseq><key>Alt</key><key>F2</key></keyseq>, indtaste dets <em>kommandonavn</em> og herefter trykke på <key>Enter</key>-tasten.</p>
    <p>Du kan f.eks. starte <app>Rhythmbox</app> ved at trykke på <keyseq><key>Alt</key><key>F2</key></keyseq> og skrive “<cmd>rhythmbox</cmd>” (uden citationstegn). Programmets navn er kommandoen til at starte programmet.</p>
    <p>Brug piletasterne til hurtigt at tilgå kommandoer, som blev kørt tidligere.</p>
  </note>

</page>
