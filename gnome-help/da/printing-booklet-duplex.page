<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="da">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Udskriv foldet brochure (som en bog eller pjece) fra en PDF med normal papirstørrelse i A4 eller Letter.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Udskriv en brochure på en duplexprinter</title>

  <p>Du kan lave en foldet brochure (såsom en lille bog eller pamflet) ved at udskrive sider af et dokument i en speciel rækkefølge og ændre et par udskrivningsmuligheder.</p>

  <p>Instruktioner til at udskrive en brochure fra et PDF-dokument.</p>

  <p>Vil du udskrive en brochure fra et <app>LibreOffice</app>-dokument, skal du først eksportere det til en PDF ved at vælge <guiseq><gui>Fil</gui><gui>Eksportér som PDF …</gui></guiseq>. Antallet af sider i dokumentet skal være et multiplum af 4 (4, 8, 12, 16, …). Du skal muligvis tilføje op til 3 tomme sider.</p>

  <p>Sådan udskrives en brochure:</p>

  <steps>
    <item>
      <p>Åbn udskrivningsdialogen. Dette kan normalt gøres via <gui style="menuitem">Udskriv</gui> i menuen eller ved at bruge <keyseq><key>Ctrl</key><key>P</key></keyseq>-tastaturgenvejen.</p>
    </item>
    <item>
      <p>Klik på knappen <gui>Egenskaber …</gui></p>
      <p>I rullelisten <gui>Orientering</gui> skal du sørge for, at <gui>Landskab</gui> er valgt.</p>
      <p>I rullelisten <gui>Duplex</gui> skal du vælge <gui>Kort kant</gui>.</p>
      <p>Klik <gui>OK</gui> for at vende tilbage til udskrivningsdialogen.</p>
    </item>
    <item>
      <p>Under <gui>Område og kopier</gui> vælges <gui>Sider</gui>.</p>
    </item>
    <item>
      <p>Indtast numrene på siderne i denne rækkefølge (n er det samlede antal sider og et multiplum af 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11 …</p>
      <p>Eksempler:</p>
      <list>
        <item><p>4-siders brochure: Skriv <input>4,1,2,3</input></p></item>
        <item><p>8-siders brochure: Skriv <input>8,1,2,7,6,3,4,5</input></p></item>
        <item><p>20-siders brochure: Skriv <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p></item>
      </list>
    </item>
    <item>
      <p>Vælg fanebladet <gui>Sidelayout</gui>.</p>
      <p>Under <gui>Layout</gui> vælges <gui>Brochure</gui>.</p>
      <p>Under <gui>Papirsider</gui> i rullelisten <gui>Medtag</gui> skal du vælge <gui>Alle sider</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Udskriv</gui>.</p>
    </item>
  </steps>

</page>
