<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="da">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Computere bliver gerne varme, men hvis de bliver for varme, kan de overophede, hvilket kan være skadeligt.</desc>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Min computer bliver meget varm</title>

<p>De fleste computere bliver varme efter et stykke tid, og nogle kan blive ret varme. Det er normalt: Det er simpelthen en del af den måde, computeren køler sig selv på. Men hvis din computer bliver meget varm, kan det være et tegn på, at den overophedes, hvilket potentielt kan forårsage skade.</p>

<p>De fleste bærbare computere bliver rimelig varme, når du har brugt dem i et stykke tid. Det er generelt ikke noget at bekymre sig om. Computere producerer meget varme, og bærbare computere er meget kompakte, så de skal fjerne deres varme hurtigt, og deres ydre kabinet varmes op som et resultat. Nogle bærbare computere bliver dog for varme og kan være ubehagelige at bruge. Dette er normalt resultatet af et dårligt designet kølesystem. Du kan nogle gange få ekstra køletilbehør, som sættes i bunden af den bærbare computer og giver mere effektiv køling.</p>

<p>Hvis du har en stationær computer, der er varm at røre ved, kan den have utilstrækkelig køling. Hvis dette bekymrer dig, kan du købe ekstra køleventilatorer eller tjekke, at køleventilatorer og ventilationsåbninger er fri for støv og andre blokeringer. Du vil måske også overveje at placere computeren i et bedre ventileret område. Hvis den opbevares i lukkede rum (for eksempel i et skab), kan kølesystemet i computeren muligvis ikke fjerne varmen og cirkulere kølig luft hurtigt nok.</p>

<p>Nogle mennesker er bekymrede over sundhedsrisiciene ved at bruge varme bærbare computere. Der er indikationer på, at langvarig brug af en varm bærbar computer på skødet muligvis kan reducere fertiliteten, og der er rapporter om lette forbrændinger (i ekstreme tilfælde). Hvis du er bekymret over disse potentielle problemer, kan du kontakte en læge for at få råd. Du kan selvfølgelig blot vælge ikke at hvile den bærbare computer på skødet.</p>

<p>De fleste moderne computere vil slukke af sig selv, hvis de bliver for varme, for at forhindre dem i at blive beskadiget. Hvis din computer bliver ved med at lukke ned, kan dette være årsagen. Hvis din computer bliver overophedet, skal du sandsynligvis få den repareret.</p>

</page>
