<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-turn-on-off" xml:lang="da">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktivér eller deaktivér Bluetooth-enheden på din computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Slå Bluetooth til eller fra</title>

  <p>Du kan tænde for Bluetooth for at oprette forbindelse til andre Bluetooth-enheder, eller slukke for det, for at spare på strømmen. Tænd for Bluetooth:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Bluetooth</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Tænd for kontakten øverst.</p>
    </item>
  </steps>

  <p>Mange bærbare computere har en hardwarekontakt eller tastekombination til at tænde og slukke for Bluetooth. Kig efter en kontakt på din computer eller en tast på dit tastatur. Tastaturtasten bruges ofte i kombination med <key>Fn</key>-tasten.</p>

  <p>Sluk for Bluetooth:</p>
  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#systemmenu">systemmenuen</gui> fra højre side af toppanelet.</p>
    </item>
    <item>
      <p>Vælg <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Bluetooth</gui>.</p>
    </item>
  </steps>

  <note><p>Din computer er <link xref="bluetooth-visibility">synlig</link> så længe panelet <gui>Bluetooth</gui> er åbent.</p></note>

</page>
