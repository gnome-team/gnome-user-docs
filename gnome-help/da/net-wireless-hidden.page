<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="da">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Opret forbindelse til et trådløst netværk, som ikke vises på netværkslisten.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Opret forbindelse til et skjult trådløst netværk</title>

<p>Det er muligt at sætte et trådløst netværk op, så det er “skjult”. Skjulte netværk vises ikke på listen over trådløse netværk, der vises i indstillingerne for <gui>Netværk</gui>. For at oprette forbindelse til et skjult trådløst netværk:</p>

<steps>
  <item>
    <p>Åbn <gui xref="shell-introduction#systemmenu">systemmenuen</gui> fra højre side af toppanelet.</p>
  </item>
  <item>
    <p>Select the arrow of
    <gui>Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>All Networks</gui>.</p>
  </item>
  <item><p>Tryk på menuknappen i øverste højre hjørne af vinduet, og vælg <gui>Forbind til skjult netværk …</gui>.</p></item>
 <item>
  <p>I vinduet, der vises, skal du vælge et tidligere tilsluttet skjult netværk ved hjælp af rullelisten <gui>Forbindelse</gui> eller <gui>Ny</gui> for et nyt.</p>
 </item>
 <item>
  <p>For en ny forbindelse skal du indtaste netværksnavnet og vælge typen af trådløs sikkerhed på rullelisten <gui>Wi-fi-sikkerhed</gui>.</p>
 </item>
 <item>
  <p>Indtast adgangskoden eller andre sikkerhedsdetaljer.</p>
 </item>
 <item>
  <p>Klik <gui>Forbind</gui>.</p>
 </item>
</steps>

  <p>Du skal muligvis kontrollere indstillingerne for det trådløse adgangspunkt eller routeren for at se, hvad netværksnavnet er. Hvis du ikke har netværksnavnet (SSID), kan du bruge <em>BSSID</em> (Basic Service Set Identifier, adgangspunktets MAC-adresse), som består af seks sæt á to bogstaver/tal adskilt af kolon som f.eks. <gui>02:00:01:02:03:04</gui>; den kan normalt findes i bunden af adgangspunktet.</p>

  <p>Du bør også kontrollere sikkerhedsindstillingerne for det trådløse adgangspunkt. Se efter udtryk som WEP og WPA.</p>

<note>
 <p>Du tror måske, at hvis du skjuler dit trådløse netværk, vil det forbedre sikkerheden ved at forhindre folk, der ikke kender til det, i at oprette forbindelse. I praksis er dette ikke tilfældet; netværket er lidt sværere at finde, men det kan stadig detekteres.</p>
</note>

</page>
