<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="da">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tjek mængden af blæk eller toner, der er tilbage i printerpatronerne.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Hvordan tjekker jeg min printers blæk- eller tonerniveauer?</title>

  <p>Hvordan du kontrollerer, hvor meget blæk eller toner, der er tilbage i din printer, afhænger af printerens model og producent samt de drivere og programmer, der er installeret på din computer.</p>

  <p>Nogle printere har en indbygget skærm til at vise blækniveauer og andre oplysninger.</p>

  <p>Nogle printere rapporterer toner- eller blækniveauer til computeren, som kan findes i panelet <gui>Printere</gui> i <app>Indstillinger</app>. Blækniveauet vil blive vist sammen med printeroplysningerne, hvis det er tilgængeligt.</p>

  <p>Driverne og statusværktøjerne til de fleste HP-printere leveres af HP Linux Imaging and Printing-projektet (HPLIP). Andre producenter kan levere proprietære drivere med lignende funktioner.</p>

  <p>Alternativt kan du installere et program til at kontrollere eller overvåge blækniveauer. <app>Inkblot</app> viser blækstatus for mange HP-, Epson- og Canon-printere. Se, om din printer er på <link href="http://libinklevel.sourceforge.net/#supported">listen over understøttede modeller</link>. Et andet program til kontrol af blækniveauet i Epson og nogle andre printere er <app>mtink</app>.</p>

  <p>Nogle printere er endnu ikke godt understøttet på Linux, og andre er ikke designet til at rapportere deres blækniveauer.</p>

</page>
