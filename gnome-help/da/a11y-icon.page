<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="da">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Tilgængelighedsmenuen er det ikon i toppanelet, der ligner en person.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Find tilgængelighedsmenuen</title>

  <p><em>Tilgængelighedsmenuen</em> er stedet, hvor du kan slå nogle af tilgængelighedsindstillingerne til. Du kan finde menuen ved at klikke på det ikon i toppanelet, der ligner en person.</p>

  <figure>
    <desc>Tilgængelighedsmenuen findes i toppanelet.</desc>
        <media its:translate="no" type="image" src="figures/classic-topbar-accessibility.svg"/>
  </figure>

  <p>Hvis du ikke kan se tilgængelighedsmenuen, så kan du aktivere den i indstillingspanelet <gui>Tilgængelighed</gui>:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Tilgængelighed</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilgængelighed</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Tænd for <gui>Tilgængelighedsmenu</gui>-kontakten.</p>
    </item>
  </steps>

  <p>For at få adgang til menuen med tastaturet, frem for med musen, trykkes på <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> for at flytte tastaturfokusset til toppanelet. Knappen <gui xref="shell-introduction#activities">Aktiviteter</gui> i det øverste venstre hjørne vil blive fremhævet — det fortæller dig, hvilket element i toppanelet som er valgt. Brug piletasterne på tastaturet til at flytte det valgte element til tilgængelighedsmenuikonet og tryk så på <key>Enter</key> for at åbne det. Du kan bruge piletasterne op og ned til at vælge elementer i menuen. Tryk på <key>Enter</key> for at slå det valgte punkt til/fra.</p>

</page>
