<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whyimportant" xml:lang="da">

  <info>
    <link type="guide" xref="color"/>
    <desc>Farvestyring er vigtig for designere, fotografer og kunstnere.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Hvorfor er farvestyring vigtigt?</title>
  <p>Farvestyring er processen med at optage en farve ved hjælp af en inputenhed, vise den på en skærm og udskrive den, samtidig med at man styrer de nøjagtige farver og farveområdet på hvert medie.</p>

  <p>Behovet for farvestyring kan nok bedst forklares med et foto af en fugl på en frostklar vinterdag.</p>

  <figure>
    <desc>En fugl på en frosset væg som det ses i kameraets søger</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-camera.png"/>
  </figure>

  <p>Skærme overmætter typisk den blå kanal, hvilket får billederne til at se kolde ud.</p>

  <figure>
    <desc>Hvad brugeren ser på skærmen på en typisk bærbar computer til virksomhedsbrug</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-display.png"/>
  </figure>

  <p>Bemærk, at det hvide ikke er “papirhvidt”, og at det sorte i øjet nu er mudret brunt.</p>

  <figure>
    <desc>Hvad brugeren ser, når der udskrives på en typisk inkjetprinter</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-printer.png"/>
  </figure>

  <p>Det grundlæggende problem her er, at hver enhed kan håndtere et forskelligt farveområde. Så selv om du måske kan tage et foto af elektrisk blå farve, vil de fleste printere ikke kunne gengive den.</p>
  <p>De fleste billeddannende enheder optager i RGB (rød, grøn, blå) og skal konvertere til CMYK (cyan, magenta, gul og sort) for at udskrive. Et andet problem er, at du ikke kan have <em>hvid</em> blæk, og derfor kan hvidheden kun være lige så god som papirfarven.</p>

  <p>Et andet problem er enhederne. Uden at angive den skala, som en farve måles på, ved vi ikke, om 100 % rødt er nærinfrarødt eller bare det dybeste røde blæk i printeren. Hvad der er 50 % rødt på en skærm er sandsynligvis noget i retning af 62 % på en anden skærm. Det svarer til at fortælle en person, at du lige har kørt 7 afstandsenheder, men uden enheden ved du ikke, om det er 7 kilometer eller 7 meter.</p>

  <p>Inden for farver betegner vi enhederne som gamut. Gamut er i det væsentlige det område af farver, der kan gengives. En enhed som et digitalt spejlreflekskamera kan have et meget stort gamut og kan opfange alle farverne i en solnedgang, men en projektor har et meget lille gamut, og alle farverne vil se “udvaskede” ud.</p>

  <p>I nogle tilfælde kan vi <em>korrigere</em> enhedens output ved at ændre de data, vi sender til den, men i andre tilfælde, hvor det ikke er muligt (man kan ikke udskrive elektrisk blå farve), skal vi vise brugeren, hvordan resultatet kommer til at se ud.</p>

  <p>For fotografier giver det mening at bruge hele toneområdet for en farveenhed, for at kunne foretage jævne farveovergange. Til anden slags grafik vil du måske matche farven nøjagtigt, hvilket er vigtigt, hvis du prøver at udskrive et brugerdefineret krus med Red Hat-logoet, der <em>skal</em> være den nøjagtige Red Hat-rød.</p>

</page>
