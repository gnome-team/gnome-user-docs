<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="da">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Tjek om et filsystem er beskadiget og bring det tilbage til en brugbar tilstand.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Reparér et beskadiget filsystem</title>

  <p>Filsystemer kan blive beskadiget på grund af strømafbrydelser, systemnedbrud og usikker fjernelse af drevet. Efter en sådan hændelse anbefales det at <em>reparere</em> eller i det mindste <em>tjekke</em> filsystemet for at forhindre fremtidige datatab.</p>
  <p>Nogle gange kræves en reparation for at montere eller ændre et filsystem. Selvom et <em>tjek</em> ikke finder nogen skade, kan filsystemet stadig være markeret som "beskidt" internt og kræve en reparation.</p>

<steps>
  <title>Tjek om et filsystem er beskadiget</title>
  <item>
    <p>Åbn <app>Diske</app> fra <gui>Aktivitetsoversigten</gui>.</p>
  </item>
  <item>
    <p>Vælg den disk, der indeholder det pågældende filsystem, fra listen over lagerenheder til venstre. Hvis der er mere end én diskenhed på disken, skal du vælge den diskenhed, der indeholder filsystemet.</p>
  </item>
  <item>
    <p>Klik på menuknappen på værktøjslinjen under sektionen <gui>Diskenheder</gui>. Klik derefter på <gui>Kontrollér filsystem …</gui>.</p>
  </item>
  <item>
    <p>Afhængigt af, hvor meget data der er gemt i filsystemet, kan en kontrol tage længere tid. Bekræft for at starte handlingen i dialogboksen, der vises.</p>
   <p>Handlingen vil ikke ændre filsystemet, men vil afmontere det, hvis det er nødvendigt. Vær tålmodig, mens filsystemet kontrolleres.</p>
  </item>
  <item>
    <p>Efter afslutningen vil du få besked om, hvorvidt filsystemet er beskadiget. Bemærk, at i nogle tilfælde, selvom filsystemet er ubeskadiget, kan det stadig være nødvendigt at reparere for at nulstille en intern “beskidt” markør.</p>
  </item>
</steps>

<note style="warning">
 <title>Muligt datatab under reparation</title>
  <p>Hvis filsystemets struktur er beskadiget, kan det påvirke filerne, der er gemt i den. I nogle tilfælde kan disse filer ikke bringes på en brugbar form igen og vil blive slettet eller flyttet til en særlig mappe. Det er normalt mappen <em>lost+found</em> i mappen på øverste niveau i filsystemet, hvor disse gendannede fildele kan findes.</p>
  <p>Hvis dataene er for værdifulde til at gå tabt under denne proces, anbefales det at sikkerhedskopiere dem ved at gemme et billede af diskenheden før reparation.</p>
  <p>Dette billede kan derefter behandles med forensiske analyseværktøjer som <app>sleuthkit</app> for yderligere at gendanne manglende filer og datadele, som ikke blev gendannet under reparationen, og også tidligere fjernede filer.</p>
</note>

<steps>
  <title>Reparér et filsystem</title>
  <item>
    <p>Åbn <app>Diske</app> fra <gui>Aktivitetsoversigten</gui>.</p>
  </item>
  <item>
    <p>Vælg den disk, der indeholder det pågældende filsystem, fra listen over lagerenheder til venstre. Hvis der er mere end én diskenhed på disken, skal du vælge den diskenhed, der indeholder filsystemet.</p>
  </item>
  <item>
    <p>Klik på menuknappen på værktøjslinjen under sektionen <gui>Diskenheder</gui>. Klik derefter på <gui>Reparér filsystem …</gui>.</p>
  </item>
  <item>
    <p>Afhængigt af, hvor meget data der er gemt i filsystemet, kan en reparation tage længere tid. Bekræft for at starte handlingen i dialogboksen, der vises.</p>
   <p>Handlingen vil afmontere filsystemet, hvis det er nødvendigt. Reparationshandlingen forsøger at bringe filsystemet i en konsistent tilstand og flytter filer, der er blevet beskadiget, til en speciel mappe. Vær tålmodig, mens filsystemet repareres.</p>
  </item>
  <item>
    <p>Efter afslutningen vil du få besked om, hvorvidt filsystemet kunne repareres. Lykkedes det, kan det bruges igen på normal vis.</p>
    <p>Hvis filsystemet ikke kunne repareres, skal du sikkerhedskopiere det ved at gemme et billede af diskenheden for at kunne hente vigtige filer senere. Dette kan gøres ved at montere billedet skrivebeskyttet eller ved at bruge forensiske analyseværktøjer som <app>sleuthkit</app>.</p>
    <p>For at gøre brug af diskenheden igen skal den være <link xref="disk-format">formateret</link> med et nyt filsystem. Alle data vil blive kasseret.</p>
  </item>
</steps>

</page>
