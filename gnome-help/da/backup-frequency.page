<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="da">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lær hvor ofte du bør sikkerhedskopiere dine vigtige filer, for at sørge for at de er sikre.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Hyppighed for sikkerhedskopieringer</title>

  <p>Hvor ofte du foretager sikkerhedskopieringer afhænger af hvilken type data der sikkerhedskopieres. Hvis du f.eks. kører et netværksmiljø med kritisk data gemt på dine servere, så er natlige sikkerhedskopieringer måske ikke nok.</p>

  <p>Modsat, hvis du sikkerhedskopierer dataene på din hjemmecomputer, så er timelige sikkerhedskopieringer sandsynligvis unødvendigt. Det kan være du finder det nyttigt at overveje følgende punkter når du planlægger din sikkerhedskopieringsplan:</p>

<list style="compact">
  <item><p>Den mængde tid du bruger på computeren.</p></item>
  <item><p>Hvor ofte og hvor meget dataene på computeren ændres.</p></item>
</list>

  <p>Hvis de data du vil sikkerhedskopiere har lav prioritet eller har få ændringer såsom musik, e-mails og familiebilleder, så kan ugentlige eller endda månedlige sikkerhedskopieringer være tilstrækkeligt. Men hvis du er midt i en skatterevision, så kan det være nødvendigt med hyppigere sikkerhedskopieringer.</p>

  <p>En tommelfingerregel er at mængden af tid mellem sikkerhedskopieringer ikke skal være længere end den mængde tid du er villig til at bruge på at gøre det mistede arbejde igen. F.eks. hvis det at bruge en uge på at skrive mistede dokumenter igen er for længe for dig, så skal du sikkerhedskopiere mindst en gang om ugen.</p>

</page>
