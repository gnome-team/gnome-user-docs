<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="da">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision version="gnome:45" date="2024-03-04" status="candidate"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Følg disse tips hvis du ikke kan finde en fil, som du oprettede eller hentede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Find en mistet fil</title>

<p>Hvis du har oprettet eller hentet en fil, men nu ikke kan finde den, skal du følge disse tips.</p>

<list>
  <item><p>Kan du ikke huske, hvor du har gemt filen, men du har en idé om, hvordan du navngav den, kan du <link xref="files-search">søge efter filen efter navn</link>.</p></item>

  <item><p>Har du lige hentet filen, har din webbrowser muligvis automatisk gemt den i en almindelig mappe. Tjek mapperne <file>Skrivebord</file> og <file>Hentet</file> i din hjemmemappe.</p></item>

  <item><p>Du kan have slettet filen ved et uheld. Når du sletter en fil, bliver den flyttet til papirkurven, hvor den bliver, indtil du manuelt tømmer papirkurven. Se <link xref="files-recover"/> for oplysninger om, hvordan du gendanner en slettet fil.</p></item>

  <item><p>You might have renamed the file in a way that made the file hidden.
  Files that start with a <file>.</file> or end with a <file>~</file> are
  hidden in the file manager. Press the menu button in the sidebar of the
  window  and enable <gui>Show Hidden Files</gui> to display them. See
  <link xref="files-hidden"/> to learn more.</p></item>
</list>

</page>
