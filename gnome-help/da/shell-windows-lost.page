<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="da">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tjek <gui>Aktivitetsoversigten</gui> eller andre arbejdsområder.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Find et mistet vindue</title>

  <p>Det er let at finde et vindue på et andet arbejdsområde eller et som er skjult bag et andet vindue med <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui>:</p>

  <list>
    <item>
      <p>Åbn <gui>Aktivitetsoversigten</gui>. Hvis det manglende vindue er på det nuværende <link xref="shell-windows#working-with-workspaces">arbejdsområde</link>, bliver det vist her som miniature. Klik blot på miniaturen for at vise vinduet igen, eller</p>
    </item>
    <item>
      <p>Klik på de forskellige arbejdsområder i <link xref="shell-workspaces">arbejdsområdevælgeren</link> og prøv om du kan finde dit vindue, eller</p>
    </item>
    <item>
      <p>Højreklik på programmet i favoritområdet for at vise dets åbne vinduer. Klik på vinduet i listen for at skifte til det.</p>
    </item>
  </list>

  <p>Med vinduesskifteren:</p>

  <list>
    <item>
      <p>Tryk på <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> for at vise <link xref="shell-windows-switching">vinduesskifteren</link>. Bliv ved med at holde <key>Super</key>-tasten nede og tryk på <key>Tab</key> for at gennemløbe de åbne vinduer, eller <keyseq><key>Skift</key><key>Tab</key> </keyseq> for at gennemløbe baglæns.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Hvis programmet har flere åbne vinduer, så hold <key>Super</key> nede og tryk på <key>½</key> (eller tasten over <key>Tab</key>) for at gennemløbe dem.</p>
    </item>
  </list>

</page>
