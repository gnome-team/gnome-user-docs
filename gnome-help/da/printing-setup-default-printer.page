<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="da">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vælg den printer, du bruger mest.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Indstil standardprinteren</title>

  <p>Hvis du har mere end én printer tilgængelig, kan du vælge, hvilken der skal være din standardprinter. Du vil måske vælge den printer, du oftest bruger.</p>

  <note>
    <p>Du skal have <link xref="user-admin-explain">administratorrettigheder</link> på systemet for at angive standardprinteren.</p>
  </note>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Printere</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Printere</gui>.</p>
    </item>
    <item>
      <p>Afhængigt af dit system skal du måske trykke på <gui style="button">Lås op</gui> i øverste højre hjørne og skrive din adgangskode, når du bliver spurgt.</p>
    </item>
    <item>
      <p>Klik på knappen <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">indstillinger</span></media> ved siden af printeren.</p>
    </item>
    <item>
      <p>Vælg afkrydsningsfeltet <gui style="menuitem">Brug printer som standard</gui>.</p>
    </item>
  </steps>

  <p>Når du udskriver i et program, bruges standardprinteren automatisk, medmindre du vælger en anden printer.</p>

</page>
