<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-security-tips" xml:lang="da">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Generelle tips du kan være opmærksom på, når du bruger internettet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Sikkerhed på internettet</title>

  <p>En mulig grund til, hvorfor du bruger Linux, er den robuste sikkerhed, som det er kendt for. En grund til, at Linux er relativt sikkert mod malware og vira, skyldes det lavere antal mennesker, der bruger det. Virus er rettet mod populære operativsystemer som Windows, der har en ekstremt stor brugerbase. Linux er også meget sikkert, fordi det er open source, hvilket giver eksperter mulighed for at ændre og forbedre sikkerhedsfunktionerne inkluderet i hver distribution.</p>

  <p>På trods af de foranstaltninger, der er truffet for at sikre, at din installation af Linux er sikker, er der altid sårbarheder. Som en gennemsnitlig bruger på internettet kan du stadig være modtagelig for:</p>

  <list>
    <item>
      <p>Phishing-svindel (websteder og e-mails, der forsøger at indhente følsomme oplysninger gennem bedrag)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Videresendelse af ondsindede e-mails</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Programmer med ondsindede hensigter (vira)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Uautoriseret fjernadgang eller gennem lokalt netværk</link></p>
    </item>
  </list>

  <p>Vær opmærksom på følgende for at være sikker online:</p>

  <list>
    <item>
      <p>Vær på vagt over for e-mails, vedhæftede filer eller links, der er sendt fra personer, du ikke kender.</p>
    </item>
    <item>
      <p>Hvis et websteds tilbud er for godt til at være sandt, eller beder om følsomme oplysninger, der virker unødvendige, så tænk to gange over, hvilke oplysninger du indsender og de potentielle konsekvenser, hvis disse oplysninger kompromitteres af identitetstyve eller andre kriminelle.</p>
    </item>
    <item>
      <p>Vær forsigtig med at give <link xref="user-admin-explain">rettigheder på rodniveau</link> til ethvert program, især dem, du ikke har brugt før, eller som ikke er velkendte. At give nogen eller noget rettigheder på rodniveau udsætter din computer for høj risiko for at blive udnyttet.</p>
    </item>
    <item>
      <p>Sørg for, at du kun kører nødvendige fjernadgangstjenester. At have SSH eller VNC kørende kan være nyttigt, men lader også din computer være åben for indtrængen, hvis den ikke er sikret ordentligt. Overvej at bruge en <link xref="net-firewall-on-off">firewall</link> for at beskytte din computer mod indtrængen.</p>
    </item>
  </list>

</page>
