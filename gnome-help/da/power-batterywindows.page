<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="da">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Tilpasninger fra producenten og forskelle i anslået levetid for batteriet kan være årsagen til problemet.</desc>
    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Hvorfor er batterilevetiden mindre, end da jeg brugte Windows/Mac OS?</title>

<p>Nogle computere ser ud til at have en kortere batterilevetid, når de kører på Linux, end de gør, når de kører Windows eller Mac OS. En grund til dette er, at computerleverandører installerer speciel software til Windows/Mac OS, der optimerer forskellige hardware/software-indstillinger for en given computermodel. Disse tilpasninger er ofte meget specifikke og er muligvis ikke dokumenterede, så det er svært at inkludere dem i Linux.</p>

<p>Desværre er der ikke en nem måde selv at anvende disse tilpasninger på uden at vide præcis, hvad de er. Du kan dog opleve, at det hjælper at bruge nogle <link xref="power-batterylife">strømbesparende metoder</link>. Hvis din computer har en <link xref="power-batteryslow">processor med variabel hastighed</link>, kan det måske også være nyttigt at ændre dens indstillinger.</p>

<p>En anden mulig årsag til uoverensstemmelsen er, at metoden til at estimere batterilevetid er anderledes på Windows/Mac OS end på Linux. Den faktiske batterilevetid kunne være nøjagtig den samme, men de forskellige metoder giver forskellige skøn.</p>
	
</page>
