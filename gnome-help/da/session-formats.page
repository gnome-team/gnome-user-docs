<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="da">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vælg en region som skal bruges til dato og klokkeslæt, tal, valuta og måling.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Skift formater for dato og måling</title>

  <p>Du kan styre de formater, der bruges til datoer, klokkeslæt, tal, valuta og måling, så de svarer til de lokale skikke i dit område.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Your Account</gui> section, click <gui>Formats</gui>.</p>
    </item>
    <item>
      <p>Under <gui>Almindelige formater</gui> skal du vælge det område og det sprog, der passer bedst til de formater, du gerne vil bruge.</p>
    </item>
    <item>
      <p>Klik på <gui style="button">Færdig</gui> for at gemme.</p>
    </item>
    <item>
      <p>Du skal genstarte din session, førend ændringerne træder i kraft. Enten klik på <gui style="button">Genstart …</gui>, eller log ind igen senere.</p>
    </item>
  </steps>

  <p>Når du har valgt en region, viser området til højre for listen forskellige eksempler på, hvordan datoer og andre værdier vises. Selvom det ikke er vist i eksemplerne, styrer din region også ugens begyndelsesdag i kalendere.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    section for the <gui>Login Screen</gui> in the
    <gui>Region &amp; Language</gui> panel.</p>
  </note>

</page>
