<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="da">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Noget computerhardware forårsager problemer med hviletilstand.</desc>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Hvorfor tænder min computer ikke, efter jeg har sat den i hviletilstand?</title>

<p>Hvis du sætter din computer i <link xref="power-suspend">hviletilstand</link>, så prøv at vække den. Du vil muligvis opleve, at den ikke virker som forventet. Dette kan skyldes, at hviletilstand ikke understøttes korrekt af din hardware.</p>

<section id="resume">
  <title>Min computer sættes i hviletilstand og kan ikke vækkes</title>
  <p>Hvis du sætter din computer i hviletiltand og derefter trykker på en tast eller klikker med musen, skulle den vågne op og vise en skærm, der beder om din adgangskode. Hvis dette ikke sker, så prøv at trykke på tænd/sluk-knappen (hold den ikke inde, tryk bare én gang).</p>
  <p>Hjælper det ikke, skal du sørge for, at din computers skærm er tændt, og prøve at trykke på en tast på tastaturet igen.</p>
  <p>Som en sidste udvej kan du slukke for computeren ved at holde tænd/sluk-knappen inde i 5-10 sekunder, men du vil miste alt arbejde, der ikke er gemt. Du skulle derefter kunne tænde computeren igen.</p>
  <p>Sker det hver gang, du sætter din computer i hviletilstand, fungerer hviletilstand muligvis ikke med din hardware.</p>
  <note style="warning">
    <p>Hvis din computer mister strøm og ikke har en alternativ strømforsyning (såsom et fungerende batteri), slukker den.</p>
  </note>
</section>

<section id="hardware">
  <title>Min trådløse forbindelse (eller anden hardware) virker ikke, når jeg vækker min computer</title>
  <p>Hvis du sætter din computer i hviletilstand og derefter vækker den, kan du opleve, at din internetforbindelse, mus eller en anden enhed ikke fungerer korrekt. Det kan skyldes, at driveren til enheden ikke understøtter hviletilstand korrekt. Det er et <link xref="hardware-driver">problem med driveren</link> og ikke selve enheden.</p>
  <p>Hvis enheden har en afbryder, kan du prøve at slukke og tænde den igen. I de fleste tilfælde vil enheden begynde at fungere igen. Hvis den tilsluttes via et USB-kabel eller lignende, så tag enheden ud af stikket og sæt den derefter i igen og se, om den virker.</p>
  <p>Hvis du ikke kan slukke eller afbryde enheden, eller hvis dette ikke virker, skal du muligvis genstarte din computer, før enheden begynder at fungere igen.</p>
</section>

</page>
