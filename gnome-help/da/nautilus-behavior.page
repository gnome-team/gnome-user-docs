<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="da">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Enkeltklik for at åbne filer, kør eller vis kørbare tekstfiler og angiv opførsel for papirkurven.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>Indstillinger for opførsel i filhåndtering</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click the menu
button in the sidebar of the window, select <gui>Preferences</gui>,
then go to the <gui>General</gui> section.</p>

<section id="behavior">
<title>General</title>
<terms>
 <item>
  <title><gui>Handling til åbning af elementer</gui></title>
  <p>Som standard vælges filer ved at klikke én gang, og med dobbeltklik åbnes de. Du kan i stedet vælge at åbne filer og mapper med et enkelt klik. Når du bruger enkeltklik-tilstand, kan du holde <key>Ctrl</key>-tasten nede, mens du klikker for at vælge en eller flere filer.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Eksekverbare tekstfiler</title>
 <p>En eksekverbar tekstfil er en fil, der indeholder et program, som du kan køre (eksekvere). <link xref="nautilus-file-properties-permissions">Filtilladelserne</link> skal også tillade, at filen kan køre som et program. De mest almindelige er <sys>Shell</sys>-, <sys>Python</sys>- og <sys>Perl</sys>-scripts. Disse har filendelserne <file>.sh</file>, <file>.py</file> og <file>.pl</file>.</p>

 <p>Eksekverbare tekstfiler kaldes også <em>scripts</em>. Alle scripts i mappen <file>~/.local/share/nautilus/scripts</file> vises i genvejsmenuen for en fil under undermenuen <gui style="menuitem">Scripts</gui>. Når et script køres fra en lokal mappe, indsættes alle valgte filer i scriptet som parametre. Sådan køres et script på en fil:</p>

<steps>
  <item>
    <p>Gå til den ønskede mappe.</p>
  </item>
  <item>
    <p>Vælg den ønskede fil.</p>
  </item>
  <item>
    <p>Højreklik på filen for at åbne genvejsmenuen og vælg det ønskede script, der skal køres, fra menuen <gui style="menuitem">Scripts</gui>.</p>
  </item>
</steps>

 <note style="important">
  <p>Et script vil ikke blive videregivet nogen parametre, når det køres fra en ekstern mappe, såsom en mappe der viser web- eller <sys>ftp</sys>-indhold.</p>
 </note>

</section>

</page>
