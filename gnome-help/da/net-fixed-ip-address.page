<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-fixed-ip-address" xml:lang="da">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="seealso" xref="net-findip"/>

    <revision pkgversion="3.4.0" date="2012-03-13" status="final"/>
    <revision pkgversion="3.15" date="2014-12-04" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Med en statisk IP-adresse kan det være lettere at levere visse netværkstjenester fra din computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Opret en forbindelse med en fast IP-adresse</title>

  <p>De fleste netværk vil automatisk tildele en <link xref="net-what-is-ip-address">IP-adresse</link> og andre detaljer til din computer, når du opretter forbindelse til netværket. Disse detaljer kan ændres med jævne mellemrum, men du vil måske gerne have en fast IP-adresse til computeren, så du altid ved, hvad dens adresse er (f.eks. hvis det er en filserver).</p>

  <!-- TODO Update for Network/Wi-Fi split -->
  <steps>
    <title>Giv din computer en fast (statisk) IP-adresse:</title>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Netværk</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Netværk</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Find den netværksforbindelse, som du vil tildele en fast adresse. Klik på knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">indstillinger</span></media> ved siden af netværksforbindelsen. For en <gui>wi-fi</gui>-forbindelse vil knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">indstillinger</span></media> være placeret ved siden af det aktive netværk.</p>
    </item>
    <item>
      <p>Vælg fanebladet <gui>IPv4</gui> eller <gui>IPv6</gui> og skift <gui>Metode</gui> til <gui>Manuel</gui>.</p>
    </item>
    <item>
      <p>Indtast <gui xref="net-what-is-ip-address">IP-adressen</gui> og <gui>Gateway'en</gui> samt den relevante <gui>Netmaske</gui>.</p>
    </item>
    <item>
      <p>I sektionen <gui>DNS</gui> skal du slå <gui>Automatisk</gui>-kontakten fra. Indtast IP-adressen på den DNS-server, du vil bruge. Indtast yderligere DNS-serveradresser ved hjælp af <gui>+</gui>-knappen.</p>
    </item>
    <item>
      <p>I sektionen <gui>Ruter</gui> skal du slå kontakten <gui>Automatisk</gui> fra. Indtast <gui>Adresse</gui>, <gui>Netmaske</gui>, <gui>Gateway</gui> og <gui>Metrik</gui> for en rute, du vil bruge. Indtast yderligere ruter ved hjælp af <gui>+</gui>-knappen.</p>
    </item>
    <item>
      <p>Klik <gui>Anvend</gui>. Netværksforbindelsen burde nu have en fast IP-adresse.</p>
    </item>
  </steps>

</page>
