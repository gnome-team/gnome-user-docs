<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="da">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Styr hvilke informationer der vises i kolonnerne i listevisning.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Indstillinger for kolonner i filliste</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view..</p>
  <p>Click the view options button in the toolbar, pick <gui>Visible
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns.</p>


  <terms>
    <item>
      <title><gui>Navn</gui></title>
      <p>Mappers og filers navne.</p>
      <note style="tip">
        <p>Kolonnen <gui>Navn</gui> kan ikke skjules.</p>
      </note>
    </item>
    <item>
      <title><gui>Størrelse</gui></title>
      <p>En mappes størrelse er antallet af elementer i mappen. En fils størrelse angives i byte, kB eller MB.</p>
    </item>
    <item>
      <title><gui>Type</gui></title>
      <p>Vises som mappe eller filtype som f.eks. PDF-dokument, JPEG-billede, MP3-lyd mv.</p>
    </item>
    <item>
      <title><gui>Ejer</gui></title>
      <p>Navnet på den bruger der ejer filen eller mappen.</p>
    </item>
    <item>
      <title><gui>Gruppe</gui></title>
      <p>Gruppen som ejer filen. Hver bruger er sædvanligvis i deres egen gruppe, men det er muligt at have mange brugere i en gruppe. For eksempel kan en afdeling have deres egen gruppe i et arbejdsmiljø.</p>
    </item>
    <item>
      <title><gui>Rettigheder</gui></title>
      <p>Viser adgangsrettighederne til filen som for eksempel <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>Det første tegn er filtypen. <gui>-</gui> betyder almindelig fil og <gui>d</gui> betyder mappe. I sjældne tilfælde kan andre tegn også vises.</p>
        </item>
        <item>
          <p>De næste tre tegn <gui>rwx</gui> angiver rettighederne for den bruger, som ejer filen.</p>
        </item>
        <item>
          <p>De efterfølgende tre tegn <gui>rw-</gui> angiver rettighederne for alle medlemmer af den gruppe, der ejer filen.</p>
        </item>
        <item>
          <p>De sidste tre tegn i kolonnen, <gui>r--</gui>, angiver rettighederne for alle andre brugere på systemet.</p>
        </item>
      </list>
      <p>Rettighederne har følgende betydning:</p>
      <list>
        <item>
          <p><gui>r</gui>: læsbar. Du kan åbne filen eller mappen</p>
        </item>
        <item>
          <p><gui>w</gui>: skrivbar. Du kan gemme ændringer til filen eller mappen</p>
        </item>
        <item>
          <p><gui>x</gui>: eksekverbar. Du kan køre den, hvis det er et program eller script, eller du kan tilgå undermapper og filer, hvis det er en mappe</p>
        </item>
        <item>
          <p><gui>-</gui>: rettighed ikke givet</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Placering</gui></title>
      <p>Stien til filens placering.</p>
    </item>
    <item>
      <title><gui>Ændret</gui></title>
      <p>Angiver datoen for den seneste ændring af filen.</p>
    </item>
    <item>
      <title><gui>Ændret — Tid</gui></title>
      <p>Angiver datoen og tidspunktet for den seneste ændring af filen.</p>
    </item>
    <item>
      <title><gui>Tilgået</gui></title>
      <p>Gives the date or time of the last time the file was accessed.</p>
    </item>
    <item>
      <title><gui>Recency</gui></title>
      <p>Gives the date or time of the last time the file was accessed by the user.</p>
    </item>
    <item>
      <title><gui>Detailed Type</gui></title>
      <p>Viser elementets MIME-type.</p>
    </item>
    <item>
      <title><gui>Created</gui></title>
      <p>Gives the date and time when the file was created.</p>
    </item>
  </terms>

</page>
