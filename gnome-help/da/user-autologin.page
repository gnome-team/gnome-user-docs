<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="da">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Log automatisk ind, når du tænder din computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alan Mortensen</mal:name>
      <mal:email/>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Log automatisk ind</title>

  <p>Du kan ændre dine indstillinger, så du automatisk logges ind på din konto, når du starter din computer:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Vælg <gui>Brugere</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Tryk på <gui style="button">Lås op</gui> i øverste højre hjørne og skriv din adgangskode, når du bliver spurgt.</p>
    </item>
    <item>
      <p>Hvis det er en anden brugerkonto, du vil lade logge ind automatisk, skal du vælge kontoen under <gui>Andre brugere</gui>.</p>
    </item>
    <item>
      <p>Tænd for <gui>Automatisk login</gui>-kontakten.</p>
    </item>
  </steps>

  <p>Næste gang du tænder din computer, bliver du automatisk logget ind. Hvis du har denne mulighed aktiveret, behøver du ikke indtaste din adgangskode for at logge ind på din konto, hvilket betyder, at hvis en anden starter din computer op, vil de kunne få adgang til din konto og dine personlige data inklusive dine filer og browserhistorik.</p>

  <note>
    <p>Hvis din kontotype er <em>Standard</em>, kan du ikke ændre denne indstilling. Kontakt din systemadministrator, som kan ændre denne indstilling for dig.</p>
  </note>

</page>
