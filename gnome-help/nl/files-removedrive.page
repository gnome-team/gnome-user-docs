<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="nl">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Een usb-stick, cd, dvd of ander apparaat uitwerpen of ontkoppelen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Een externe schijf veilig verwijderen</title>

  <p>Als u externe opslagapparaten gebruikt zoals usb-sticks, dan moet u die op een veilige manier verwijderen voordat u ze ontkoppelt. Als u zomaar een apparaat ontkoppelt, dan loopt u het risico dat u het ontkoppelt terwijl een toepassing er nog gebruik van maakt, hetgeen kan resulteren in het verlies of beschadiging van sommige van uw bestanden. Als u een optische schijf 
zoals een cd of dvd gebruikt, dan kunt u dezelfde stappen doen om de schijf uit uw computer te werpen.</p>

  <steps>
    <title>Een verwijderbaar apparaat uitwerpen:</title>
    <item>
      <p>Open <app>Bestanden</app> vanuit het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht.</p>
    </item>
    <item>
      <p>Zoek het apparaat in de zijbalk. Het moet een klein uitwerp-pictogram naast de naam hebben staan. Klik op het uitwerp-pictogram om het apparaat veilig te verwijderen of uit te werpen.</p>
      <p>U kunt ook met rechts klikken op de naam van het apparaat in de zijbalk en <gui>Uitwerpen</gui> kiezen.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>Een apparaat dat in gebruik is veilig verwijderen</title>

  <p>Als een toepassing bestanden op het apparaat heeft geopend en gebruikt, dan kunt u het apparaat niet veilig verwijderen. U krijgt een venster te zien met de mededeling <gui>Het volume is bezet</gui>. Om het apparaat veilig te verwijderen:</p>

  <steps>
    <item><p>Klik op <gui>Annuleren</gui>.</p></item>
    <item><p>Sluit alle bestanden af die op het apparaat staan.</p></item>
    <item><p>Klik op het ontkoppelen-pictogram om het apparaat veilig te ontkoppelen of uit te werpen.</p></item>
    <item><p>U kunt ook met rechts klikken op de naam van het apparaat in de zijbalk en <gui>Uitwerpen</gui> kiezen.</p></item>
  </steps>

  <note style="warning"><p>U kunt ook <gui>Toch uitwerpen</gui> kiezen om het apparaat te verwijderen zonder de bestanden te sluiten. Dit can fouten veroorzaken in toepassingen waarin de bestanden nog hebben open zijn.</p></note>

  </section>

</page>
