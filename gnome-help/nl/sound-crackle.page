<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="nl">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uw audiokabels en stuurprogramma's voor de geluidskaart controleren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Ik hoor gekraak of gezoem wanneer er geluid wordt afgespeeld</title>

  <p>Als u gekraak of gezoem hoort wanneer er geluid wordt afgespeeld op uw computer, dan kan het zijn dat u een probleem heeft met de audiokabels of connectoren, of een probleem met het stuurprogramma voor de geluidskaart.</p>

<list>
 <item>
  <p>Zorg ervoor dat de luidsprekers op de juiste manier zijn aangesloten.</p>
  <p>Als de luidsprekers niet helemaal goed zijn aangesloten, of als ze in de verkeerde aansluiting zitten, dan hoort u mogelijk een zoemend geluid.</p>
 </item>

 <item>
  <p>Controleer of de kabel van de luidspreker/hoofdtelefoon niet beschadigd is.</p>
  <p>Audiokabels en connectoren kunnen geleidelijk aan slijten. Sluit de kabel of hoofdtelefoon aan op een ander geluidsapparaat (zoals een MP3-speler of een cd-speler) om te controleren of u nog steeds een krakend geluid hoort. Als dat zo is, dan kan het zijn dat u de kabel of hoofdtelefoon moet vervangen.</p>
 </item>

 <item>
  <p>Controleer of het stuurprogramma voor de geluidskaart wel helemaal goed functioneert.</p>
  <p>Sommige geluidskaarten werken niet goed op Linux-systemen omdat ze geen goede stuurprogramma's hebben. Dit probleem is moeilijker te herkennen. Zoek op het internet naar het merk en model van uw geluidskaart in combinatie met de term "Linux" om te zien of anderen hetzelfde probleem hebben.</p>
  <p>U kunt <cmd>lspci</cmd> in de <app>Terminal</app> uitvoeren om meer informatie te krijgen over uw geluidskaart.</p>
 </item>
</list>

</page>
