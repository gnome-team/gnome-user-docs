<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="nl">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het kan zijn dat de adapter is uitgeschakeld, of geen stuurprogramma heeft, of Bluetooth kan uitgeschakeld of geblokkeerd zijn.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Ik kan mijn Bluetooth-apparaat niet aansluiten</title>

  <p>Er zijn meerdere redenen waarom u een Bluetooth-apparaat (zoals een telefoon of koptelefoon) niet kunt aansluiten.</p>

  <terms>
    <item>
      <title>Verbinding geblokkeerd of niet te vertrouwen</title>
      <p>Sommige Bluetooth-apparaten blokkeren standaard alle verbindingen, of vereisen dat u de instellingen verandert om verbindingen toe te staan. Zorg ervoor dat uw apparaat zo is ingesteld dat verbindingen zijn toegestaan.</p>
    </item>
    <item>
      <title>Bluetooth-hardware niet herkend</title>
      <p>Het kan zijn dat uw Bluetooth-adapter of -dongel niet herkend werd door de computer. Dit kan komen doordat de <link xref="hardware-driver">stuurprogramma's</link> voor de adapter niet zijn geïnstalleerd. Sommige Bluetooth-adapters worden niet ondersteund door Linux waardoor u mogelijk niet aan de juiste stuurprogramma's ervoor kunt komen. In dit geval dient u waarschijnlijk een andere Bluetooth-adapter aan te schaffen.</p>
    </item>
    <item>
      <title>Adapter niet ingeschakeld</title>
        <p>Zorg ervoor dat uw Bluetooth-adapter is ingeschakeld. Open het Bluetooth-paneel en zorg ervoor dat het niet is <link xref="bluetooth-turn-on-off">uitgeschakeld</link>.</p>
    </item>
    <item>
      <title>Bluetooth is op het externe apparaat uitgeschakeld</title>
      <p>Controleer of Bluetooth is ingeschakeld op het apparaat waarmee u probeert te verbinden, en dat het <link xref="bluetooth-visibility">gevonden kan worden of zichtbaar is</link>. Bijvoorbeeld, als u probeert te verbinden met een telefoon, zorg er dan voor dat die niet in vliegtuigmodus staat.</p>
    </item>
    <item>
      <title>Geen Bluetooth-adapter op uw computer</title>
      <p>Veel computers hebben geen Bluetooth-adapters. U kunt een adapter kopen als u Bluetooth wilt gebruiken.</p>
    </item>
  </terms>

</page>
