<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-manual" xml:lang="nl">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>
    <revision pkgversion="3.33.3" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Als netwerkinstellingen niet automatisch toegekend worden, dan moet u die mogelijk zelf invoeren.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Netwerkinstellingen handmatig instellen</title>

  <p>Als uw netwerk niet automatisch netwerkinstellingen toewijst aan uw computer, dan moet u mogelijk zelf de instellingen handmatig invoeren. Dit onderwerp gaat ervan uit dat u de juiste instellingen die gebruikt moeten worden al kent. Zo niet, dan kunt u contact opnemen met uw netwerkbeheerder, of de instellingen van uw router of netwerkswitch bekijken.</p>

  <steps>
    <title>Om uw netwerkinstellingen handmatig in te voeren:</title>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Instellingen</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Instellingen</gui>.</p>
    </item>
    <item>
      <p>If you plug in to the network with a cable, click <gui>Network</gui>.
      Otherwise click <gui>Wi-Fi</gui>.</p>
      <p>Zorg ervoor dat uw draadloze kaart is ingeschakeld of dat een netwerkkabel is aangesloten.</p>
    </item>
    <item>      
      <p>Klik op <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">instellingen</span></media>.</p>
      <note>
        <p>For a <gui>Wi-Fi</gui> connection, the 
        <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
        button will be located next to the active network.</p>
      </note>
    </item>
    <item>
      <p>Select the <gui>IPv4</gui> or <gui>IPv6</gui> tab and change the
      <gui>Method</gui> to <gui>Manual</gui>.</p>
    </item>
    <item>
      <p>Typ het <gui xref="net-what-is-ip-address">IP-adres</gui> en <gui>Gateway</gui> in, evenals het juiste <gui>netmasker</gui>.</p>
    </item>
    <item>
      <p>In the <gui>DNS</gui> section, switch the <gui>Automatic</gui> switch
      to off. Enter the IP address of a DNS server you want to use. Enter
      additional DNS server addresses using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>In the <gui>Routes</gui> section, switch the <gui>Automatic</gui>
      switch to off. Enter the <gui>Address</gui>, <gui>Netmask</gui>,
      <gui>Gateway</gui> and <gui>Metric</gui> for a route you want to use.
      Enter additional routes using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. If you are not connected to the network, open the
      <gui xref="shell-introduction#systemmenu">system menu</gui> from the right side of
      the top bar and connect. Test the network
      settings by trying to visit a website or look at shared files on the
      network, for example.</p>
    </item>
  </steps>

</page>
