<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="nl">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set up a printer that is connected to your computer, or your local
    network.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Een lokale printer instellen</title>

  <p>Your system can recognize many types of printers automatically once they
  are connected. Most printers are connected with a USB cable that attaches to
  your computer, but some printers connect to your wired or wireless
  network.</p>

  <note style="tip">
    <p>If your printer is connected to the network, it will not be set up
    automatically – you should add it from the <gui>Printers</gui> panel in
    <gui>Settings</gui>.</p>
  </note>

  <steps>
    <item>
      <p>Zorg ervoor dat de printer aan staat.</p>
    </item>
    <item>
      <p>Sluit de kabel van de printer aan op uw systeem. Het kan zijn dat u activiteit ziet op het scherm terwijl het systeem zoekt naar stuurprogramma's, en mogelijk wordt u gevraagd om authenticatie om die te installeren.</p>
    </item>
    <item>
      <p>Zodra het systeem klaar is met het installeren van de nieuwe printer zal er een melding worden weergegeven. Kies <gui>Testpagina afdrukken</gui> om een testpagina te printen, of <gui>Opties</gui> om de printerinstellingen verder te wijzigen.</p>
    </item>
  </steps>

  <p>Als uw printer niet automatisch werd ingesteld, dan kunt u hem toevoegen bij de printerinstellingen:</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add Printer…</gui> button.</p>
    </item>
    <item>
      <p>In the pop-up window, select your new printer and press
      <gui style="button">Add</gui>.</p>
      <note style="tip">
        <p>Als uw printer niet automatisch ontdekt wordt, maar u kunt zijn netwerkadres, voer dit dan in in het tekstveld onderaan het dialoogvenster en druk op <gui style="button">Toevoegen</gui></p>
      </note>
    </item>
  </steps>

  <p>If your printer does not appear in the <gui>Add Printer</gui> window, you
  may need to install print drivers.</p>

  <p>Nadat u de printer heeft geïnstalleerd wilt u misschien <link xref="printing-setup-default-printer"> de standaardprinter wijzigen</link>.</p>

</page>
