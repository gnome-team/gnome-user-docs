<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="nl">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Loszittende kabels en hardwareproblemen zijn mogelijke oorzaken.</desc>
    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Ik kan mijn computer niet aanzetten</title>

<p>Er zijn een aantal redenen waarom u uw computer niet kunt aanzetten. Dit onderwerp geeft een kort overzicht van sommige van de mogelijke oorzaken.</p>
	
<section id="nopower">
  <title>Computer niet aangesloten, lege accu, of een loszittende kabel</title>
  <p>Zorg ervoor dat de stroomkabels van de computer goed zijn aangesloten en er stroom is. Zorg er ook voor dat de monitor is aangesloten en aan staat. Als u een laptop heeft, sluit de kabel aan (voor het geval dat accu leeg is). U kunt ook controleren of de accu goed op zijn plaats zit indien deze verwijderbaar is (controleer de onderkant van de laptop).</p>
</section>

<section id="hardwareproblem">
  <title>Probleem met de hardware van de computer</title>
  <p>Het kan zijn dat een onderdeel van uw computer kapot is of niet goed werkt. Als dit het geval is, dan dient u uw computer te laten repareren. Veelvoorkomende gebreken zijn een kapotte voeding, onderdelen die niet goed zijn gemonteerd (zoals het geheugen/RAM) en een defect moederbord.</p>
</section>

<section id="beeps">
  <title>De computer geeft een pieptoon en wordt dan uitgeschakeld</title>
  <p>Als de computer meerdere pieptonen geeft wanneer hij, direct nadat u hem heeft aangezet uitgeschakeld wordt (of helemaal niet wil starten), dan kan dit erop wijzen dat er een probleem is aangetroffen. Deze pieptonen worden soms <em>beep codes</em> genoemd en het pieptonenpatroon is bedoeld om u te vertellen om welk probleem met de computer het gaat. Verschillende fabrikanten gebruiken verschillende ‘beep codes’, dus u zult de handleiding van het moederbord moeten raadplegen of uw computer ter reparatie aanbieden.</p>
</section>

<section id="fans">
  <title>De ventilator van de computer draait, maar er is niets te zien op het scherm</title>
  <p>Het eerste wat u moet controleren is of uw monitor is aangesloten en aan staat.</p>
  <p>Dit probleem kan ook veroorzaakt worden door een hardware fout. Het kan zijn dat de ventilatoren gaan draaien wanneer u op de startknop drukt, terwijl andere essentiële onderdelen van de computer het niet doen. Breng in dit geval uw computer naar de reparateur.</p>
</section>

</page>
