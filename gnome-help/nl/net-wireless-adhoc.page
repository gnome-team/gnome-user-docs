<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="nl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32" date="2019-09-01" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Een ad hoc-netwerk gebruiken zodat andere apparaten verbinding kunnen maken met uw computer en zijn netwerkverbindingen.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Een draadloos-hotspot creëren</title>

  <p>U kunt uw computer gebruiken als een draadloos-hotspot. Hierdoor kunnen andere apparaten verbinding met u maken zonder apart netwerk, zodat u een internetverbinding kunt delen die u met een andere interface heeft gemaakt, zoals een bekabeld netwerk of een cellulair netwerk.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select the arrow of
    <gui>Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>All Networks</gui>.</p></item>
  <item><p>Press the menu button in the top-right corner of the window and
  select <gui>Turn On Wi-Fi Hotspot…</gui>.</p></item>
  <item><p>Als u al verbonden bent met een draadloos netwerk, dan wordt u gevraagd of u de verbinding met dat netwerk wilt verbreken. Een enkele draadloos-adapter kan met slechts één netwerk tegelijk verbinding maken of creëren. Klik op <gui>Aanzetten</gui> om te bevestigen.</p></item>
</steps>

<p>Een netwerknaam (SSID) en beveiligingssleutel worden automatisch gegenereerd. De netwerknaam zal gebaseerd zijn op de naam van uw computer. Andere apparaten hebben deze informatie nodig om verbinding te maken met de hotspot die u zojuist heeft aangemaakt.</p>

<note style="tip">
  <p>U kunt ook verbinding maken met het draadloze netwerk door de QR-code in te scannen op uw telefoon of tablet met de ingebouwde camera-app of een QR-codelezer.</p>
</note>

</page>
