<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-calibrationcharacterization" xml:lang="nl">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Kalibratie en karakterisering zijn totaal verschillende dingen.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Wat is het verschil tussen kalibreren en karakteriseren?</title>
  <p>Veel mensen kennen het verschil tussen kalibreren en karakteriseren niet. Kalibreren is het aanpassen van het kleurgedrag van een apparaat. Dit gebeurt meestal met het gebruik van twee mechanismen:</p>
  <list>
    <item><p>Het aanpassen van interne instellingen van het apparaat</p></item>
    <item><p>Het vervormen van de kleurkanalen</p></item>
  </list>
  <p>Het idee van kalibreren is om de kleurreactie van apparaten in een voorgedefinieerde staat te brengen. Dit wordt vaak gebruikt om het reproduceerbare gedrag van het apparaat van dag tot dag te behouden. Het resultaat van de kalibratie wordt normaal in het apparaat opgeslagen of in systeem-specifieke bestanden met daarin opgeslagen de apparaatinstellingen of kalibratiecurves per kanaal.</p>
  <p>Karakteriseren (of profileren) is het <em>opnemen</em> van de manier waarop een apparaat een kleur produceert of op een kleur reageert. De resultaten worden normaal opgeslagen in een ICC-profiel voor het apparaat. Zo'n profiel kan uit zichzelf geen kleuren aanpassen. Maar met zo'n profiel kan een systeem zoals een CMM (color management module) of een kleur-bewuste toepassing de kleur veranderen zodat u ziet wat u op het andere apparaat zou zien. Alleen wanneer u weet wat de karakterisering van de twee apparaten is, kunt u de kleuren van het ene apparaat op het andere apparaat weergeven.</p>
  <note>
    <p>Houd er rekening mee dat een karakterisering (profiel) alleen geldig is voor een apparaat indien het in dezelfde staat van kalibratie is als toen het gekarakteriseerd werd.</p>
  </note>
  <p>Als het om beeldschermprofielen gaat is er nog wat extra verwarring omdat de kalibratie-informatie vaak voor het gemak in het profiel wordt opgeslagen. Het is gebruikelijk deze informatie in de <em>vcgt</em>-tag op te slaan. Alhoewel het is opgeslagen in het profiel, zijn geen van de normale, op ICC-gebaseerde, hulpmiddelen of toepassingen zich ervan bewust. Tegelijkertijd zullen typische monitor-kalibratieprogramma's zich er niet bewust van zijn, en zullen het ICC-profiel dan ook niet gebruiken.</p>

</page>
