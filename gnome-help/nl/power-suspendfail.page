<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="nl">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Sommige computerhardware veroorzaakt problemen bij pauzestand.</desc>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Waarom wordt mijn computer niet meer actief nadat ik hem in de pauzestand heb gezet?</title>

<p>Wanneer u uw computer in de <link xref="power-suspend">pauzestand</link> zet, en daarna weer activeert, kan de computer mogelijk niet werken zoals verwacht. Dit kan mogelijk veroorzaakt worden doordat uw hardware de pauzestand niet goed ondersteunt.</p>

<section id="resume">
  <title>Mijn computer is in de pauzestand gezet en wordt niet meer actief</title>
  <p>Wanneer u uw computer in de pauzestand heeft gezet en daarna op een toets of met uw muis klikt, zou de computer weer actief moeten worden en een scherm tonen waarin om uw wachtwoord gevraagd wordt. Als dit niet het geval is, probeer dan op de aan/uit-knop te drukken (u hoeft er alleen maar op te drukken en niet ingedrukt te houden).</p>
  <p>Als dit niet helpt, controleer dan of de monitor aan staat en probeer het nogmaals door op een toets op het toetsenbord te drukken.</p>
  <p>Als laatste redmiddel kunt u de computer uitzetten door de aan/uit-knop 5-10 seconden ingedrukt te houden. Hierdoor zal wel al het niet opgeslagen werk verloren gaan. Maar daarna zou u de computer weer gewoon moeten kunnen aanzetten.</p>
  <p>Als dit telkens wanneer u de computer in de pauzestand zet gebeurt, dan kan het zijn dat de pauzestandfunctie niet werkt met uw hardware.</p>
  <note style="warning">
    <p>Als uw computer stroom verliest en geen alternatieve stroombron heeft (zoals een werkende accu), dan zal deze uitgeschakeld worden.</p>
  </note>
</section>

<section id="hardware">
  <title>Mijn draadloze verbinding (of andere hardware) werkt niet wanneer ik de computer weer activeer</title>
  <p>Als u uw computer in pauzestand zet en het weer hervat, dan kan het zijn dat uw internetverbinding, muis, of een ander apparaat niet meer correct werkt. Dit kan komen doordat het stuurprogramma van het apparaat de pauze- of slaapstand niet goed ondersteunt. Dit is een <link xref="hardware-driver">probleem met het stuurprogramma</link> en niet met het apparaat zelf.</p>
  <p>Als het apparaat een aan/uit-knop heeft, probeer het dan uit en weer aan te zetten. In de meeste gevallen zal het apparaat weer werken. Als het aangesloten is via een USB-kabel kunt u het apparaat afkoppelen en weer aansluiten en kijken of het dan werkt.</p>
  <p>Als u het apparaat niet uit kunt zetten of af kunt koppelen, of als dit niet werkt, dan kan het nodig zijn om de computer opnieuw op te starten om te zorgen dat het apparaat weer werkt.</p>
</section>

</page>
