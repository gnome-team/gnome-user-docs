<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Toepassingen starten vanuit het <gui>activiteiten</gui>-overzicht.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Toepassingen starten</title>

  <p if:test="!platform:gnome-classic">Breng de muiswijzer naar de <gui>Activiteiten</gui>-hoek linksboven van het scherm voor het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht. Hier vindt u al uw toepassingen. U kunt het overzicht ook openen door te drukken op de <key xref="keyboard-key-super">Super</key>-toets.</p>
  
  <p if:test="platform:gnome-classic">U kunt toepassingen starten vanuit het <gui xref="shell-introduction#activities">Toepassingen</gui>-menu links van het scherm, of u kunt het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht gebruiken door te drukken op de <key xref="keyboard-key-super">Super</key>-toets.</p>

  <p>Er zijn verschillende manieren om een toepassing te openen vaqnuit het <gui>Activiteiten</gui>-overzicht:</p>

  <list>
    <item>
      <p>Begin met het typen van de naam van een toepassing — het zoeken wordt meteen gestart. (Als dit niet het geval is, klik dan op de zoekbalk bovenaan het scherm en begin met typen.) Als u niet precies de naam van de toepassing weet, probeer dan een gerelateerde term te typen. Klik op het pictogram van de toepassing om deze te starten.</p>
    </item>
    <item>
      <p>Some applications have icons in the <em>dash</em>, the horizontal strip
      of icons at the bottom of the <gui>Activities</gui> overview.
      Click one of these to start the corresponding application.</p>
      <p>Als er toepassingen zijn die u veel gebruikt, kunt u deze zelf <link xref="shell-apps-favorites">toevoegen aan de Starter</link>.</p>
    </item>
    <item>
      <p>Click the grid button (which has nine dots) in the dash.
      You will see the first page of all installed applications. To see more
      applications, press the dots at the bottom, above the dash, to view other
      applications. Press on the application to start it.</p>
    </item>
    <item>
      <p>You can launch an application in a separate
      <link xref="shell-workspaces">workspace</link> by dragging its icon from
      the dash, and dropping it onto one of the workspaces. The application will
      open in the chosen workspace.</p>
      <p>You can launch an application in a <em>new</em> workspace by dragging its
      icon to an empty workspace, or to the small gap between two workspaces.</p>
    </item>
  </list>

  <note style="tip">
    <title>Een opdracht snel uitvoeren</title>
    <p>Een andere manier om een toepassing te starten is door op <keyseq><key>Alt</key><key>F2</key></keyseq> te drukken, de <em>opdrachtnaam</em> ervan in te voeren en op de <key>Enter</key>-toets te drukken.</p>
    <p>Om bijvoorbeeld <app>Rhythmbox</app> te starten: druk op <keyseq><key>Alt</key><key>F2</key></keyseq> en typ ‘<cmd>rhythmbox</cmd>’ (zonder de aanhalingstekens). De naam van de toepassing is de opdracht om het programma te starten.</p>
    <p>Use the arrow keys to quickly access previously run commands.</p>
  </note>

</page>
