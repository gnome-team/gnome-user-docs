<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision version="gnome:44" date="2023-12-30" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Leer hoe u uw gebruikersaccount verlaat, door af te melden, van gebruiker te wisselen, enz.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Afmelden, uitschakelen of van gebruiker wisselen</title>

  <p>Wanneer u klaar bent met het gebruiken van uw computer kunt u deze uitschakelen, in de pauzestand zetten (om energie te besparen), of hem aan laten staan en u alleen afmelden.</p>

<section id="logout">
  <info>
    <link type="seealso" xref="user-add"/>
  </info>

  <title>Afmelden of van gebruiker wisselen</title>

  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-exit-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Gebruikersmenu</p>
      </media>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-exit-classic-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Gebruikersmenu</p>
      </media>
    </if:when>
  </if:choose>

  <p>Om anderen uw computer te laten gebruiken kunt u zich afmelden of aangemeld blijven en alleen van gebruiker wisselen. Als u alleen maar van gebruiker wisselt, dan zullen al uw toepassingen blijven draaien en alles zal zich bevinden waar u het achterliet wanneer u zich weer aanmeldt.</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p>De onderdelen <gui>Afmelden</gui> en <gui>Gebruiker wisselen</gui> verschijnen alleen in het menu als u meer dan één gebruikersaccount op uw systeem heeft.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>Het onderdeel <gui>Gebruiker wisselen</gui> verschijnt alleen in het menu als u meer dan één gebruikersaccount op uw systeem heeft.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Het scherm vergrendelen</title>

  <p>If you’re leaving your computer for a short time, you should lock your
  screen to prevent other people from accessing your files or running
  applications. When you return, you will see the
  <link xref="shell-lockscreen">lock screen</link>. Enter your
  password to log back in. If you don’t lock your screen, it will lock
  automatically after a certain amount of time.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and click the
    <media type="image" its:translate="no" src="figures/system-lock-screen-symbolic.svg">
      Lock
    </media>
  button.</p>

  <p>When your screen is locked, other users can log in to their own accounts
  by clicking <gui>Log in as another user</gui> at the bottom right of the login
  screen. You can switch back to your desktop when they are finished.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Pauzestand</title>

  <p>To save power, suspend your computer when you are not using it. If you use
  a laptop, the system, by default, suspends your computer automatically when
  you close the lid.
  This saves your state to your computer’s memory and powers off most of the
  computer’s functions. A very small amount of power is still used during
  suspend.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select <gui>Suspend</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Uitschakelen of opnieuw opstarten</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>Als er andere gebruikers aangemeld zijn, dan kan het zijn dat u de computer niet mag uitschakelen of herstarten omdat daarmee hun sessies beëindigd zullen worden. Als u beheerder bent, dan kan het zijn dat u uw wachtwoord moet invoeren om uit te schakelen.</p>

  <note style="tip">
    <p>Het kan zijn dat u de computer uit wilt schakelen als u deze wilt verplaatsen en u geen accu heeft, als uw accu bijna leeg is of niet goed oplaadt. Een computer die uitgeschakeld is verbruikt ook <link xref="power-batterylife">minder energie</link> dan een computer in pauzestand.</p>
  </note>

</section>

</page>
