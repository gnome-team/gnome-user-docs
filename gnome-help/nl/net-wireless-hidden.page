<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="nl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Verbind met een draadloos netwerk dat niet in de netwerklijst staat.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Met een verborgen draadloos netwerk verbinden</title>

<p>Het is mogelijk een draadloos netwerk zo in te stellen dat het ‘verborgen’ is. Verborgen netwerken worden niet opgenomen in de lijst met draadloos netwerken in de <gui>Netwerk</gui>-instellingen. Om verbinding te maken met een verborgen draadloos netwerk:</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select the arrow of
    <gui>Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>All Networks</gui>.</p>
  </item>
  <item><p>Press the menu button in the top-right corner of the window and
  select <gui>Connect to Hidden Network…</gui>.</p></item>
 <item>
  <p>In het venster dat verschijnt, selecteer een verborgen netwerk waarmee u voorheen verbonden was met de <gui>Verbinding</gui>-menulijst, of <gui>Nieuw</gui> voor een nieuwe.</p>
 </item>
 <item>
  <p>Voor een nieuwe verbinding, typ de naam van het netwerk en kies het soort draadloze beveiliging in de <gui>Wifi-beveiliging</gui>-keuzelijst.</p>
 </item>
 <item>
  <p>Voer het wachtwoord of andere veiligheidsinformatie in.</p>
 </item>
 <item>
  <p>Klik op <gui>Verbinden</gui>.</p>
 </item>
</steps>

  <p>Mogelijk moet u de instellingen van het draadloos basisstation of router controleren om te zien wat de naam van het netwerk is. Als u de netwerknaam (SSID) niet heeft, kunt u de gebruik maken van <em>BSSID</em> (Basic Service Set Identifier, het MAC-adres van het basisstation), dat er ongeveer zo uit ziet: <gui>02:00:01:02:03:04</gui>. Dit is doorgaans te vinden aan de onderkant van het basisstation.</p>

  <p>U dient ook de beveiligingsinstellingen van het draadloos basisstation te controleren. Zoek naar termen zoals WEP en WPA.</p>

<note>
 <p>U zou kunnen denken dat het verbergen van uw draadloos netwerk de veiligheid zal verbeteren door te voorkomen dat mensen die er geen weet van hebben verbinding maken. In de praktijk is dit niet het geval; het netwerk is iets moeilijker te vinden maar het kan worden opgespoord.</p>
</note>

</page>
