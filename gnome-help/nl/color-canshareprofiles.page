<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="nl">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Het delen van kleurprofielen is niet handig, omdat de hardware zelf met de tijd verandert.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Kan ik mijn kleurprofiel delen?</title>

  <p>Kleurprofielen die u zelf heeft aangemaakt zijn specifiek voor de hardware en lichtcondities waarvoor u kalibreert. Een monitor die een paar honderd uur heeft aangestaan zal een totaal ander kleurprofiel hebben dan een soortgelijke monitor met het volgende serienummer als deze duizend uur aan staat.</p>
  <p>Dit betekent dat, als u uw kleurprofiel met anderen deelt, zij <em>dichter</em> tegen kalibratie aan zitten, maar het is op zijn minst misleidend om te zeggen dat hun scherm gekalibreerd is.</p>
  <p>Zo zal ook het delen van een profiel dat u aangemaakt heeft onder uw eigen specifieke lichtcondities geen zin hebben, tenzij iedereen dezelfde lichtcondities heeft (geen zonlicht via ramen, donkere muren, daglichtlampen enz.) in een kamer waar afbeeldingen worden bekeken en bewerkt.</p>

  <note style="warning">
    <p>U moet de herdistributievoorwaarden voor profielen die zijn gedownload van de website van de leverancier, of die namens u werden gemaakt, zorgvuldig controleren.</p>
  </note>

</page>
