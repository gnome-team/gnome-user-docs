<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="nl">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verwijder bestanden of mappen die niet langer nodig zijn.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Bestanden en mappen verwijderen</title>

  <p>If you do not want a file or folder any more, you can delete it. When you
  delete an item it is moved to the <gui>Trash</gui>, where it is stored
  until you empty the trash. You can <link xref="files-recover">restore
  items</link> in the <gui>Trash</gui> to their original location if you
  decide you need them, or if they were accidentally deleted.</p>

  <steps>
    <title>Om een bestand naar de prullenbak te verplaatsen:</title>
    <item><p>Selecteer het item dat u naar de prullenbak wilt verplaatsen door er één maal op te klikken.</p></item>
    <item><p>Druk op <key>Delete</key> op het toetsenbord. U kunt het item ook naar de <gui>Prullenbak</gui> in de zijbalk slepen.</p></item>
  </steps>

  <p>Het bestand zal naar de prullenbak worden verplaatst, en u krijgt de keuze voor het <gui>ongedaan maken</gui> van de verwijdering. De knop <gui>Ongedaan maken</gui> zal enkele seconden te zien zijn. Als u <gui>Ongedaan maken</gui> kiest, dan zal het bestand naar de oorspronkelijke locatie teruggezet worden.</p>

  <p>Om bestanden permanent te verwijderen en zo schijfruimte op uw computer vrij te maken, dient u de prullenbak te legen. Om de prullenbak te legen klikt u met rechts op de <gui>Prullenbak</gui> in de zijbalk en kiest u <gui>Prullenbak legen</gui>.</p>

  <section id="permanent">
    <title>Een bestand permanent verwijderen</title>
    <p>U kunt een bestand direct permanent verwijderen, zonder het eerst naar de prullenbak te sturen.</p>

  <steps>
    <title>Een bestand permanent verwijderen:</title>
    <item><p>Selecteer het item dat u wilt verwijderen.</p></item>
    <item><p>Houd de <gui>Shift</gui>-toets ingedrukt en druk vervolgens op de <key>Delete</key>-toets op u toetsenbord.</p></item>
    <item><p>Omdat u dit niet ongedaan kunt maken, wordt u gevraagd te bevestigen om een bestand of de map te verwijderen.</p></item>
  </steps>

  <note><p>Verwijderde bestanden op een <link xref="files#removable">verwijderbaar apparaat</link> zijn mogelijk niet zichtbaar in andere besturingssystemen zoals Windows of Mac OS. De bestanden zijn er nog wel, en zullen beschikbaar zijn wanneer u het apparaat weer aansluit op uw computer.</p></note>

  </section>

</page>
