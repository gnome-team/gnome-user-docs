<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="nl">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Een hardware/apparaatstuurprogramma stelt uw computer in staat aangesloten apparaten te gebruiken.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Wat is een stuurprogramma?</title>

<p>Apparaten zij de fysieke "delen" van uw computer. Ze kunnen <em>extern</em> zijn, zoals printers en monitoren of <em>intern</em> zoals grafische kaarten en geluidskaarten.</p>

<p>Om deze apparaten te kunnen gebruiken moet uw computer weten hoe ermee te communiceren. Dit wordt gedaan door een stukje software genaamd <em>apparaat-stuurprogramma</em>.</p>

<p>Wanneer u een apparaat aansluit op uw computer, dan moet u het juiste stuurprogramma geïnstalleerd hebben om het te laten werken. Als u bijvoorbeeld een printer aansluit, maar het juiste stuurprogramma is niet beschikbaar, dan kunt u de printer niet gebruiken. Normaal gesproken gebruikt elk model van een apparaat een stuurprogramma dat niet compatibel is met een ander model.</p>

<p>In Linux zijn de stuurprogramma's voor de meeste apparaten standaard geïnstalleerd, dus alles wat u aansluit zou moeten werken. Maar het kan ook zijn dat een stuurprogramma handmatig moet worden geïnstalleerd of helemaal niet te krijgen is.</p>

<p>Bovendien zijn sommige bestaande stuurprogramma's onvolledig of functioneren deels niet. Het kan bijvoorbeeld zijn dat uw printer niet dubbelzijdig afdrukt, maar voor de rest goed functioneert.</p>

</page>
