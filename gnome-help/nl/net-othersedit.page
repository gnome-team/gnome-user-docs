<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="nl">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>U moet het vinkje bij de optie <gui>Beschikbaar voor alle gebruikers</gui> in de netwerkverbindingsinstellingen weghalen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Andere gebruikers kunnen de netwerkverbindingen niet bewerken</title>

  <p>Als u wel een netwerkverbinding kunt bewerken, maar andere gebruikers op uw computer niet, dan heeft u mogelijk de verbinding zo ingesteld dat deze <gui>Beschikbaar voor alle gebruikers</gui> is. Dit betekent dat iedereen op de computer via die verbinding <em>verbinding</em> kan maken.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Netwerk</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Netwerk</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Selecteer <gui>Wifi</gui> in de lijst aan de linkerkant.</p>
    </item>
    <item>
      <p>Klik op de knop <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">instellingen</span></media> om de verbindingsgegevens te openen.</p>
    </item>
    <item>
      <p>Selecteer <gui>Identiteit</gui> vanuit het paneel aan de linkerkant.</p>
    </item>
    <item>
      <p>U moet in het paneel <gui>Identiteit</gui> een vinkje plaatsen bij de optie <gui>Beschikbaar voor andere gebruikers</gui> om andere gebruikers gebruik te laten maken van de netwerkverbinding.</p>
    </item>
    <item>
      <p>Klik op <gui style="button">Toepassen</gui> om de wijzigingen op te slaan.</p>
    </item>
  </steps>

</page>
