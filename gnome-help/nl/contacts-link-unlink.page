<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="nl">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Informatie over een contact uit meerdere bronnen bij elkaar brengen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Contactpersoon aan elkaar verbinden en weer losmaken</title>

<section id="link-contacts">
  <title>Contacten koppelen</title>

  <p>U kunt dubbele contactpersonen in uw adresboek en online accounts met elkaar combineren tot één item in <app>Contactpersonen</app>. Deze functie helpt u om uw adresboek te organiseren met alle gegevens over een contactpersoon op één lokatie.</p>

  <steps>
    <item>
      <p>Schakel <em>selectiemodus</em> in door deze boven de lijst met contactpersonen aan te vinken.</p>
    </item>
    <item>
      <p>Er verschijnt een aankruisvakje bij elk contact. Plaats een vinkje in de aankruisvakjes bij de contacten die u wilt samenvoegen.</p>
    </item>
    <item>
      <p>Klik <gui style="button">Koppelen</gui> om de geselecteerde contactpersonen aan elkaar te verbinden.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Contacten ontkoppelen</title>

  <p>Als u onbedoeld contactpersonen aan elkaar heeft verbonden die niet aan elkaar verbonden hadden moeten worden, dan kunt u ze van elkaar losmaken.</p>

  <steps>
    <item>
      <p>Select the contact you wish to unlink from your list of contacts.</p>
    </item>
    <item>
      <p>Druk op <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">view more</span></media> in de rechterbovenhoek van <app>Contacten</app>.</p>
    </item>
    <item>
      <p>Klik op <gui style="button">Ontkoppelen</gui> om het item van de contactpersoon los te maken.</p>
    </item>
  </steps>

</section>

</page>
