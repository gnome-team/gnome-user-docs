<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="nl">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Een bestandssysteem en diens partities vergroten of verkleinen.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>De grootte van een bestandssysteem aanpassen</title>

  <p>Een bestandssysteem kan uitgebreid worden door de vrije ruimte na de partitie te benutten. Vaak is dit zelfs mogelijk terwijl het bestandssysteem aangekoppeld is.</p>
  <p>Om ruimte te maken voor een andere partitie na het bestandssysteem kan het worden verkleind overeenkomstig de vrije ruimte erin.</p>
  <p>Niet alle bestandssystemen kunnen vergroot/verkleind worden.</p>
  <p>De partitiegrootte zal samen met de grootte van het bestandssysteem worden gewijzigd. Het is ook mogelijk om op dezelfde manier de grootte van een partitie zonder een bestandssysteem te wijzigen.</p>

<steps>
  <title>Een bestandssysteem/partitie vergroten of verkleinen</title>
  <item>
    <p>Open <app>Schijven</app> vanuit het <gui>Activiteiten</gui>-overzicht.</p>
  </item>
  <item>
    <p>Selecteer de schijf die u wilt controleren in de lijst met opslagapparaten aan de linkerkant. Als de schijf meer dan één volume bevat, selecteer dan het volume met daarop het bestandssysteem.</p>
  </item>
  <item>
    <p>Klik in de werkbalk onder de sectie <gui>Volumen</gui> op de menuknop. Klik daarna op <gui>Bestandssysteem vergroten/verkleinen…</gui>. of <gui>Vergroten/verkleinen…</gui> als er geen bestandssysteem is.</p>
  </item>
  <item>
    <p>Er wordt een dialoogvenster geopend waar de nieuwe grootte kan worden gekozen. Het bestandssysteem zal aangekoppeld worden om de minimale grootte te berekenen aan de hand van de huidige inhoud. Als krimpen niet mogelijk is, dan is de minimale grootte de huidige grootte. Houd bij het krimpen genoeg ruimte binnen het bestandssysteem over om er zeker van te zijn dat het snel en betrouwbaar kan werken.</p>
    <p>Afhankelijk van hoeveel gegevens er moeten worden verplaatst uit het gekrompen gedeelte, kan het aanpassen van het bestandssysteem langer duren.</p>
    <note style="warning">
      <p>Het aanpassen van de grootte van het bestandssysteem brengt automatisch de <link xref="disk-repair">reparatie</link> van dat bestandssysteem met zich mee. Het is daarom aan te raden voor het starten reservekopieën te maken van belangrijke gegevens. De actie mag niet gestopt worden, want anders zal het resultaat een beschadigd bestandssysteem zijn.</p>
    </note>
  </item>
  <item>
      <p>Bevestig om de actie te starten door te klikken op <gui style="button">Vergroten/verkleinen</gui>.</p>
   <p>De actie zal het bestandssysteem afkoppelen als het aanpassen van de grootte van een aangekoppeld bestandssysteem niet ondersteund wordt. Wees geduldig terwijl de grootte van het bestandssysteem wordt aangepast.</p>
  </item>
  <item>
    <p>Nadat de benodigde aanpassings- en reparatieacties voltooid zijn, kan het bestandssysteem weer gebruikt worden.</p>
  </item>
</steps>

</page>
