<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="mr">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>२०१२</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>USB फ्लॅश ड्राइव्ह, सीडी, डीवीडी, किंवा इतर साधन बाहेर काढा किंवा माउंट अशक्य करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>बाह्य ड्राइव सुरक्षितपणे काढा</title>

  <p>स्टोरेज साधने जसे कि USB फ्लॅश ड्राइव्हज यांचा वापर करताना, काढून टाकण्यापूर्वी त्यास सुरक्षितपणे काढून टाका. साधन फक्त काढून टाकल्यास, ॲप्लिकेशन त्याचा वापर करत असताना अनप्लग करण्याचा धोका निर्माण होतो. यामुळे काही फाइल्स गमवले जातील किंवा नष्टही होऊ शकतात. सीडी किंवा डीवीडी सारख्या ऑप्टिकल डिस्कचा वापर करतेवेळी, संगणकातून डिस्क बाहेर काढण्यासाठी तुम्ही समान टप्प्यांचा वापर करू शकता.</p>

  <steps>
    <title>निघण्यासारखे उपकरणे बाहेर काढण्यासाठी:</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>बाजूच्या पट्टीमध्ये साधनाचे ठिकाण शोधा. नावाच्या बाजूस छोटे इजेक्ट चिन्ह पाहिजे. साधन सुरक्षितपणे काढून टाकणे किंवा बाहेर काढण्याकरिता बाहेर काढा चिन्ह क्लिक करा.</p>
      <p>वैकल्पिकरित्या, बाजूच्या पट्टीमध्ये साधनावरील उजवी-क्लिक द्या आणि <gui>बाहेर काढा</gui> निवडा.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>वापरात असलेले उपकरण सुरक्षितपणे बाहेर काढा</title>

  <p>साधनावरील कोणत्याही फाइल्स खुल्या असल्यास आणि ॲप्लिकेशनतर्फे वापरात असल्यास, साधनाला सुरक्षितपणे काढून टाकणे शक्य होणार नाही. <gui>तीव्रता व्यस्त आहे</gui> असे सूचना असलेल्या पटलकरिता विचारले जाईल. साधनाला सुरक्षितपणे काढून टाकण्याकरिता:</p>

  <steps>
    <item><p><gui>रद्द</gui> क्लिक करा.</p></item>
    <item><p>उपकरणावरील सर्व फाइल्स बंद करा.</p></item>
    <item><p>उपकरणाला निष्कासित किंवा सुरक्षितपणे काढण्यासाठी निष्कासी चिन्हावर क्लिक करा.</p></item>
    <item><p>वैकल्पिकरित्या, बाजूच्या पट्टीमध्ये साधनावरील उजवी-क्लिक द्या आणि <gui>बाहेर काढा</gui> निवडा.</p></item>
  </steps>

  <note style="warning"><p>फाइल्स बंद न करता साधन काढून टाकण्याकरिता <gui>कसेही करून बाहेर काढा</gui> पसंत करणे शक्य आहे. फाइल्स खुले असल्यास, यामुळे ॲप्लिकेशन्समध्ये त्रुटी निर्माण होऊ शकते.</p></note>

  </section>

</page>
