<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="mr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="43" date="2022-09-10" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>कॉपी करा किंवा वस्तु नवीन फोल्डरला हलवा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>कॉपी करा किंवा फाइल्स आणि फोल्डर्स हलवा</title>

 <p>माऊससह ओढून आणि टाकून, कॉपी आणि पेस्ट आदेशांचा वापर करून, किंवा कळफलक शार्टक्ट्स्चा वापर करून एका फाइल किंवा फोल्डरचे प्रत बनवणे किंवा त्यास स्थानांतरित करणे शक्य आहे.</p>

 <p>For example, you might want to copy a presentation onto a memory stick so
 you can take it to work with you. Or, you could make a back-up copy of a
 document before you make changes to it (and then use the old copy if you don’t
 like your changes).</p>

 <p>ह्या सूचना दोन्ही फाइल्स आणि फोल्डर्सकरिता लागू होतात. तुम्ही फाइल्स आणि फोल्डर्सचे प्रत बनवणे आणि स्थानांतरन हुबेहुब समान पद्धतीने करू शकता.</p>

<steps ui:expanded="false">
<title>फाइल्स कॉपी आणि पेस्ट करा</title>
<item><p>इच्छित फाइल कॉपी करायला निवडण्यासाठी त्यावर एकदा क्लिक करा.</p></item>
<item><p>Right-click and select <gui>Copy</gui>, or press
 <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>दुसऱ्या फोल्डरकरिता संचारन करा, जेथे तुम्हाला फाइलचे प्रत बनवायचे आहे.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>कापा आणि फाइल्स हलवण्यासाठी पेस्ट करा</title>
<item><p>इच्छित फाइल हलवायला निवडण्यासाठी त्यावर एकदा क्लिक करा.</p></item>
<item><p>Right-click and select <gui>Cut</gui>, or press
 <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>दुसऱ्या फोल्डरकरिता संचारन करा, जेथे तुम्हाला फाइल स्थानांतरीत करायचे आहे.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>कॉपी किंवा हलवण्यासाठी फाइल्स ओढा</title>
<item><p>फाइल व्यवस्थापक उघडा आणि प्रत बनवण्याजोगी फाइल समाविष्टीत असणाऱ्या फोल्डरकडे जा.</p></item>
<item><p>Press the menu button in the sidebar of the window and select
 <gui style="menuitem">New Window</gui> (or
 press <keyseq><key>Ctrl</key><key>N</key></keyseq>) to open a second window. In
 the new window, navigate to the folder where you want to move or copy the file.
 </p></item>
<item>
 <p>एका पटलातून दुसऱ्या पटलात फाइल क्लिक करा आणि ओढा. लक्ष्य <em>समान</em> साधनावर असल्यास <em>स्थानांतरीत करा</em>, किंवा लक्ष्य <em>वेगळ्या</em> साधनावर असल्यास <em>प्रत बनवा</em>.</p>
 <p>For example, if you drag a file from a USB memory stick to your Home folder,
 it will be copied, because you’re dragging from one device to another.</p>
 <p>ओढताना <key>Ctrl</key> कि दाबून ठेवून फाइलचे प्रत जबरनरित्या बनवणे शक्य आहे, किंवा ओढताना <key>Shift</key> कि दाबून ठेवून त्यास जबरनरित्या स्थानांतरित करणे शक्य आहे.</p>
 </item>
</steps>

<note>
  <p>You cannot copy or move a file into a folder that is <em>read-only</em>.
  Some folders are read-only to prevent you from making changes to their
  contents. You can change things from being read-only by
  <link xref="nautilus-file-properties-permissions">changing file permissions
  </link>.</p>
</note>

</page>
