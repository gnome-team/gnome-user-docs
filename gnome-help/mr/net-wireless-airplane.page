<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="mr">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Open Network Settings and switch Airplane Mode to on.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>वायरलेस (विमान पद्धत) बंद करा</title>

<p>If you have your computer on an airplane (or some other area where wireless
 connections are not allowed), you should switch off your wireless. You may
 also want to switch off your wireless for other reasons (to save battery power,
 for example).</p>

  <note>
    <p>Using <em>Airplane Mode</em> will completely turn off all wireless
    connections, including Wi-Fi, 3G and Bluetooth connections.</p>
  </note>

  <p>To turn on airplane mode:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Airplane Mode</gui> switch to on. This will turn off
      your wireless connection until you disable airplane mode again.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can turn off your Wi-Fi connection from the
    <gui xref="shell-introduction#systemmenu">system menu</gui> by clicking on 
    the connection name and choosing <gui>Turn Off</gui>.</p>
  </note>

</page>
