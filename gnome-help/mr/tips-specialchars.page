<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="mr">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Type characters not found on your keyboard, including foreign alphabets, mathematical symbols, emoji, and dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>खास कॅरेक्टर टाका</title>

  <p>You can enter and view thousands of characters from most of the world’s
  writing systems, even those not found on your keyboard. This page lists
  some different ways you can enter special characters.</p>

  <links type="section">
    <title>कॅरेक्टर टाकायच्या पद्धती</title>
  </links>

  <section id="characters">
    <title>Characters</title>
    <p>The character map application allows you to find and insert unusual
    characters, including emoji, by browsing character categories or searching
    for keywords.</p>

    <p>You can launch <app>Characters</app> from the Activities overview.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Insert emoji</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Browse the categories at the bottom of the dialog or start typing a
    description in the search field.</p>
      </item>
      <item>
        <p>Select an emoji to insert.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>रचना की</title>
    <p>कम्पोज कि विशेष कि आहे जे तुम्हाला विशेष अक्षर प्राप्त करण्यासाठी एकापेक्षा जास्त किज प्राप्त करण्यास मदत करते. उदाहरणार्थ, ॲक्सेंटेड अक्षर <em>é</em> टाइप करण्याकरिता, तुम्ही<key>कम्पोज</key> त्यानंतर <key>'</key> आणि नंतर <key>e</key> टाइप करू शकता.</p>
    <p>Keyboards don’t have specific compose keys. Instead, you can define
    one of the existing keys on your keyboard as a compose key.</p>

    <steps>
      <title>रचना की बनवा</title>
      <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click on <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
      </item>
      <item>
        <p>In the <gui>Type Special Characters</gui> section, click <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Turn the switch on for the <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Close the dialog.</p>
      </item>
    </steps>

    <p>कम्पोज किचा वापर करून तुम्ही अनेक सामान्य अक्षर टाइप करू शकता, उदाहरणार्थ:</p>

    <list>
      <item><p><key>कम्पोज</key> नंतर <key>'</key> दाबा आणि त्यानंतर अक्षरावर ॲक्युट ॲक्सेंट अक्षर स्थित करा, जसे कि <em>é</em>.</p></item>
      <item><p><key>कम्पोज</key> नंतर <key>`</key> दाबा आणि त्यानंतर अक्षरावर ग्रेव ॲक्सेंट अक्षर स्थित करा, जसे कि <em>è</em>.</p></item>
      <item><p><key>कम्पोज</key> नंतर <key>"</key> दाबा आणि त्यानंतर अक्षरावर umlaut अक्षर स्थित करा, जसे कि <em>ë</em>.</p></item>
      <item><p><key>कम्पोज</key> नंतर <key>-</key> दाबा आणि त्यानंतर अक्षरावर मॅक्रॉन अक्षर स्थित करा, जसे कि <em>ē</em>.</p></item>
    </list>
    <p>For more compose key sequences, see <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">the
    compose key page on Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>कोड पॉइंटस्</title>

  <p>You can enter any Unicode character using only your keyboard with the
  numeric code point of the character. Every character is identified by a
  four-character code point. To find the code point for a character, look it up
  in the <app>Characters</app> application. The code point is the four characters
  after <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>कीबोर्ड माडंण्या</title>
    <p>कळफलकाला इतर भाषाकरिता, किजवरील छपाई अक्षरांखेरीज कळफलकासारखे वर्तन करा. शीर्ष पट्टीतील चिन्हाचा वापर करून विविध किबोर्ड मांडणींचा वापर शक्य आहे. अधिक शिकण्याकरिता, <link xref="keyboard-layouts"/> पहा.</p>
  </section>

<section id="im">
  <title>इनपुट पद्धती</title>

  <p>An Input Method expands the previous methods by allowing to enter
  characters not only with keyboard but also any input devices. For instance
  you could enter characters with a mouse using a gesture method, or enter
  Japanese characters using a Latin keyboard.</p>

  <p>इंपुट पद्धती निवडण्याकरिता, मजकूर विजेटवर उजवी-क्लिक द्या, आणि मेन्यु <gui>इंपुट पद्धती</gui> मध्ये, वापरण्याजोगी इंपुट पद्धती निवडा. पूर्वनिर्धारित इंपुट पद्धती पुरवली नाही, म्हणून इंपुट पद्धतींचे दस्तऐवजीकरण त्यांचा वापर कशा प्रकारे केला जातो, ते पहा.</p>

</section>

</page>
