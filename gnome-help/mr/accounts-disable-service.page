<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="mr">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>काही ऑनलाइन अकाउंट्संचा वापर एकापेक्षा जास्त सर्व्हिसेसकरिता प्रवेशसाठी (जसे कि दिनदर्शिका आणि ईमेल) शक्य आहे. ॲप्लिकेशन्सतर्फे वापरण्याजोगी सर्व्हिसेस नियंत्रीत करणे शक्य आहे.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>खातेतर्फे प्रवेशजोगी कोणती ऑनलाइन सर्व्हिसेस नियंत्रीत करायची</title>

  <p>Some types of online account providers allow you to access several services
  with the same user account. For example, Google accounts provide access to
  calendar, email, and contacts. You may want to use your account for some
  services, but not others. For example, you may want to use your Google account
  for email but not calendar if you have a different online account that you use
  for calendar.</p>

  <p>तुम्ही प्रत्येक ऑनलाइन खात्यातर्फे पुरवलेले काही सर्व्हिसेस बंद करू शकता:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Online Accounts</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Online Accounts</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the account which you want to change from the list on the
      right.</p>
    </item>
    <item>
      <p>A list of services that are available with this account will be
      shown under <gui>Use for</gui>. See <link xref="accounts-which-application"/>
      to see which applications access which services.</p>
    </item>
    <item>
      <p>Switch off any of the services that you do not want to use.</p>
    </item>
  </steps>

  <p>Once a service has been disabled for an account, applications on your
  computer will not be able to use the account to connect to that service any
  more.</p>

  <p>To turn on a service that you disabled, just go back to the <gui>Online
  Accounts</gui> panel and switch it on.</p>

</page>
