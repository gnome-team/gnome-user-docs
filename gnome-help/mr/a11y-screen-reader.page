<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="mr">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>वापरकर्ता इंटरफेस वदवण्यासाठी <app>Orca</app> स्क्रीन वाचक वापरा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>स्क्रीन मोठ्याने वाचा</title>

  <p>The <app>Orca</app> screen reader can speak the user interface. Depending
  on how you installed your system, you might not have Orca installed. If not,
  install Orca first.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Install Orca</link></p>
  
  <p>To start <app>Orca</app> using the keyboard:</p>
  
  <steps>
    <item>
    <p>Press <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Or to start <app>Orca</app> using a mouse and keyboard:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Seeing</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Screen Reader</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Quickly turn Screen Reader on and off</title>
    <p>You can turn Screen Reader on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> in the top bar and
    selecting <gui>Screen Reader</gui>.</p>
  </note>

  <p>Refer to the <link href="help:orca">Orca Help</link> for
  more information.</p>
</page>
