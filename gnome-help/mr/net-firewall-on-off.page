<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-firewall-on-off" xml:lang="mr">

  <info>
    <link type="guide" xref="net-security" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-03" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>कोणते प्रोग्राम्स नेटवर्ककरिता प्रवेश प्राप्त करू शकतात ते नियंत्रीत करणे शक्य आहे. यामुळे संगणक सुरक्षित राहते.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>फायरवॉल परवानगी लागू करा किंवा अडवा</title>

  <p>GNOME does not come with a firewall, so for support beyond this document
  check with your distribution’s support team or your organization’s IT department.
  Your computer should be equipped with a <em>firewall</em> that allows it to
  block programs from being accessed by other people on the internet or your
  network. This helps to keep your computer secure.</p>

  <p>अनेक ॲप्लिकेशन्स नेटवर्क जोडणीचा वापर करू शकतात. उदाहरणार्थ, फाइल्स शेअर करणे किंवा नेटवर्कशी जोडणी केल्यानंतर इतरांना दूरस्तरित्या तुमचे डेस्कटॉप पहाण्यास परवानगी द्या. संगणक कशा प्रकारे सेटअप केले आहे, त्यावर आधारित, तुम्हाला आवश्यकताप्रमाणे फायरवॉल सुस्थीत करावे लागेल.</p>

  <p>Each program that provides network services uses a specific <em>network
  port</em>. To enable other computers on the network to access a service, you
  may need to “open” its assigned port on the firewall:</p>


  <steps>
    <item>
      <p>Go to <gui>Activities</gui> in the top left corner of the screen and
      start your firewall application. You may need to install a firewall
      manager yourself if you can’t find one (for example, GUFW).</p>
    </item>
    <item>
      <p>वापरकर्त्यांना प्रवेश शक्य किंवा अशक्य करायचे, यावर आधारित नेटवर्क सर्व्हिसकरिता, पोर्ट उघडा किंवा बंद करा. <link xref="net-firewall-ports">सर्व्हिसवर आधारित</link> बदलण्याजोगी पोर्ट निवडा.</p>
    </item>
    <item>
      <p>बदल साठवा किंवा लागू करा, फायरवॉल साधनतर्फे दिलेल्या अगाऊ सूचनांच्या पाठोपाठ.</p>
    </item>
  </steps>

</page>
