<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-alert" xml:lang="mr">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>संदेशकरिता चालवण्याजोगी आवाज निवडा, सतर्कता आवाज सेट करा, किंवा सतर्कता आवाज बंद करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>सावधानता आवाज निवडा किंवा बंद करा</title>

  <p>संदेश आणि इव्हेंट्सच्या ठराविक प्रकारकरिता तुमचे संगणक सोपे सतर्कता आवाज चालवेल. सतर्कताकरिता, प्रणाली आवाजाची सतर्कता आवाज स्वतंत्रपणे सेट करा, किंवा सतर्कता आवाज पूर्णपणे बंद करण्यासाठी वेगळे आवाज क्लिप्स निवडणे शक्य आहे.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Sounds</gui> section, click on <gui>Alert Sound</gui>.</p>
    </item>
    <item>
      <p>Select an alert sound. Each
      sound will play when you click on it so you can hear how it sounds.</p>
    </item>
  </steps>

  <p>To change the volume of the alert sound, return to the <gui>Sounds</gui> section,
  click <gui>Volume Levels</gui> and adjust the <gui>System Sounds</gui> slider.
  Click the speaker button next to the slider to mute or unmute the alert sound.
  This will not affect the volume of your music, movies, or other sound files.</p>

</page>
