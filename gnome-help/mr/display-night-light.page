<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="mr">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Night Light changes the color of your displays according to the time
    of day.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>Adjust the color temperature of your screen</title>

  <p>A computer monitor emits blue light which contributes to sleeplessness and
  eye strain after dark. <gui>Night Light</gui> changes the color of your
  displays according to the time of day, making the color warmer in the
  evening. To enable <gui>Night Light</gui>:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Displays</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Night Light</gui> to open the settings.</p>
    </item>
    <item>
      <p>Ensure the <gui>Night Light</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Under <gui>Schedule</gui>, select <gui>Sunset to Sunrise</gui> to
      make the screen color follow the sunset and sunrise times for your location.
      Select <gui>Manual Schedule</gui> to set the <gui>Times</gui> to a custom schedule.</p>
    </item>
    <item>
      <p>Use the slider to adjust the <gui>Color Temperature</gui> to be more
      warm or less warm.</p>
    </item>
  </steps>
      <note>
        <p>The <link xref="shell-introduction">top bar</link> shows when
        <gui>Night Light</gui> is active. It can be temporarily disabled from
        the system menu.</p>
      </note>



</page>
