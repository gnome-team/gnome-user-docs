<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-singlesided" xml:lang="mr">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Print a booklet from a PDF using a single-sided printer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>एका-बाजूचे छपाईयंत्रवर बूकलेटची छपाई करा</title>

  <note>
    <p>ह्या सूचना PDF दस्तऐवजातून बूकलेटच्या छपाईकरिता आहे.</p>
    <p><app>LibreOffice</app> दस्तऐवजपासून बूकलेटची छपाई करायची असल्यास, पहिले <guiseq><gui>फाइल</gui><gui>PDF म्हणून एक्सपोर्ट करा…</gui></guiseq> निवडून तुम्ही PDF ला एक्सपोर्ट करू शकता. तुमचे दस्तऐवज पृष्ठे ४ संख्येच्या पटीत असणे आवश्यक आहे (४, ८, १२, १६,…). तुम्हाला ३ रिकामे पृष्ठे समाविष्ट करावे लागेल.</p>
  </note>

  <p>छापण्यासाठी:</p>

  <steps>
    <item>
      <p>छपाई संवाद उघडा. यास सहसा मेन्यु अंतर्गत <gui style="menuitem">छपाई</gui> यांच्या सहाय्याने किंवा <keyseq><key>Ctrl</key><key>P</key></keyseq> कळफलक शार्टकटचा वापर करून शक्य आहे.</p>
    </item>
    <item>
      <p>Click the <gui>Properties…</gui> button </p>
      <p>In the <gui>Orientation</gui> drop-down list, make sure that
      <gui>Landscape</gui> is selected.</p>
      <p>Click <gui>OK</gui> to go back to the print dialog.</p>
    </item>
    <item>
      <p>Under <gui>Range and Copies</gui>, choose <gui>Pages</gui>.</p>
      <p>पृष्ठांची संख्या या क्रमवारीत टाइप करा (n म्हणजे एकूण पृष्ठांची संख्या, आणि ४ संख्येच्या पटीत):</p>
      <p>n, १, २, n-१, n-२, ३, ४, n-३, n-४, ५, ६, n-५, n-६, ७, ८, n-७, n-८, ९, १०, n-९, n-१०, ११, १२, n-११…</p>
      <p>…जोपर्यंत तुम्ही सर्व पृष्ठे टाइप केले असाल.</p>
    <note>
      <p>उदाहरणे:</p>
      <p>४ पानी पुस्तिका: <input>४,१,२,३</input> टाइप करा</p>
      <p>८ पानी पुस्तिका: <input>८,१,२,७,६,३,४,५</input> टाइप करा</p>
      <p>१२ पानी पुस्तिका: <input>१२,१,२,११,१०,३,४,९,८,५,६,७</input> टाइप करा</p>
      <p>१६ पानी पुस्तिका: <input>१६,१,२,१५,१४,३,४,१३,१२,५,६,११,१०,७,८,९</input> टाइप करा</p>
      <p>२० पानी पुस्तिका: <input>२०,१,२,१९,१८,३,४,१७,१६,५,६,१५,१४,७,८,१३,१२,९,१०,११</input> टाइप करा</p>
     </note>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Layout</gui>, select <gui>Brochure</gui>.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>Front sides / right pages</gui>.</p>
    </item>
    <item>
      <p><gui>छापा</gui> क्लिक करा.</p>
    </item>
    <item>
      <p>सर्व पृष्ठांची छपाई झाल्यानंतर, पृष्ठांना पलटवा आणि त्यास छपाईयंत्रमध्ये पुन्हा स्थित करा.</p>
    </item>
    <item>
      <p>छपाई संवाद उघडा. यास सहसा मेन्यु अंतर्गत <gui style="menuitem">छपाई</gui> यांच्या सहाय्याने किंवा <keyseq><key>Ctrl</key><key>P</key></keyseq> कळफलक शार्टकटचा वापर करून शक्य आहे.</p>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>Back sides / left pages</gui>.</p>
    </item>
    <item>
      <p><gui>छापा</gui> क्लिक करा.</p>
    </item>
  </steps>

</page>
