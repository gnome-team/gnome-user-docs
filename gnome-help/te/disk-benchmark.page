<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="te">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>మైకేల్ హిల్</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>అది ఎంత వేగంగా వుందో పరిశీలించుటకు మీ హార్డుడిస్కుపై బెంచ్‌మార్క్స్ నడుపండి.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>మీ హార్డు డిస్కు యొక్క పనితనం పరిశీలించండి</title>

  <p>మీ హార్డు డిస్కు యొక్క వేగం పరిశీలించుటకు:</p>

  <steps>
    <item>
      <p>Open <app>Disks</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Choose the disk from the list in the left pane.</p>
    </item>
    <item>
      <p>Click the menu button and select <gui>Benchmark Disk…</gui> from the
      menu.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmark…</gui> and adjust the <gui>Transfer
      Rate</gui> and <gui>Access Time</gui> parameters as desired.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmarking…</gui> to test how fast data can be read
      from the disk. <link xref="user-admin-explain">Administrative
      privileges</link> may be required. Enter your password, or the password
      for the requested administrator account.</p>
      <note>
        <p>If <gui>Perform write-benchmark</gui> is checked, the benchmark
        will test how fast data can be read from and written to the disk. This
        will take longer to complete.</p>
      </note>
    </item>
  </steps>

  <p>పరీక్ష పూర్తైనప్పుడు, ఫలితాలు గ్రాఫ్ పైన కనిపించును. పచ్చని బిందువులు మరియు కలుపుతున్న వరుసలు తీసుకొన్న మాదిరిలను సూచించును; కుడి అక్షానికి చెందినవి, ఏక్సెస్ సమయాన్ని, కింది అక్షానికి ఎదురుగావున్నవి, బెంచ్‌మార్కునందు పట్టిన సమయ శాతంను చూపును. నీలం వరుస రీడ్ రేట్లను చూపును, ఎరుపు వరుస వ్రైట్ రేట్లను చూపును; ఇవి ఎడమ అక్షముపై ఏక్సెస్ దత్తాంశం రేట్లవలె చూపబడును, స్పిండిల్ బయట నుండి, డిస్కు ప్రయాణించిన శాతం, కింది ఏక్సెస్ పక్కన చూపబడును.</p>

  <p>రేఖాచిత్రం దిగువున, కనిష్ట, గరిష్ట మరియు సగటు రీడ్ మరియు వ్రైట్ రేట్లు, సగటు ఏక్సెస్ సమయపు మరియు ఆఖరి బెంచ్‌మార్క్ పరీక్ష నుండి గడిచిన సమయపు విలువలు ప్రదర్శించబడును.</p>

</page>
