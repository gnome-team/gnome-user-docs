<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="te">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Some fan-control software could be missing, or your laptop may be
    running hot.</desc>

    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>The laptop fan is always running</title>

<p>If cooling fan in your laptop is always running, it could be that the
hardware that controls the cooling system in the laptop is not very well
supported in Linux. Some laptops need extra software to control their cooling
fans efficiently, but this software may not be installed (or available for
Linux at all) and so the fans just run at full speed all of the time.</p>

<p>If this is the case, you may be able to change some settings or install
extra software that allows full control of the fan. For example, <link href="http://vaio-utils.org/fan/">vaiofand</link> can be installed to control
the fans of some Sony VAIO laptops. Installing this software is quite a
technical process which is highly dependent on the make and model of your
laptop, so you may wish to seek specific advice on how to do it for your
computer.</p>

<p>It is also possible that your laptop just produces a lot of heat. This does
not necessarily mean that it is overheating; it might just need the fan to run
at full speed all of the time to allow it to stay cool enough. If this is the
case, you have little option but to let the fan run at full speed all of the
time. You can sometimes buy additional cooling accessories for your laptop
which may help.</p>

</page>
