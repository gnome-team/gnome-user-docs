<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-whydim" xml:lang="ro">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="41" date="2021-09-08" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>The screen will dim when the computer is idle in order to save power.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016, 2019</mal:years>
    </mal:credit>
  </info>

  <title>Why does my screen go dim after a while?</title>

  <p>If it is possible to set the brightness of your screen, it will dim when
  the computer is idle in order to save power. When you start using the
  computer again, the screen will brighten.</p>

  <p>To stop the screen from dimming itself:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Power</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Dim Screen</gui> switch to off in the
      <gui>Power Saving Options</gui> section.</p>
    </item>
  </steps>

  <p>The screen will always dim, and dim more aggressively when choosing
  the “Power Saver” power mode. If you donʼt want the screen to dim at all,
  select the “Balanced” power mode instead.</p>

</page>
