<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="ro">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Look in <guiseq><gui>Settings</gui><gui>Color</gui></guiseq> to add a color profile for your screen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016, 2019</mal:years>
    </mal:credit>
  </info>

  <title>How do I assign profiles to devices?</title>

  <p>You may want to assign a color profile for your screen or printer so that
  the colors which it shows are more accurate.</p>

  <steps>
    <item>
      <p>Deschideți vederea de ansamblu <gui xref="shell-introduction#activities">Activități</gui> și tastați <gui>Configurări</gui>.</p>
    </item>
    <item>
      <p>Apăsați pe <gui>Configurări</gui>.</p>
    </item>
    <item>
      <p>Apăsați pe <gui>Culoare</gui> în bara laterală pentru a deschide panoul.</p>
    </item>
    <item>
      <p>Select the device for which you want to add a profile.</p>
    </item>
    <item>
      <p>Click <gui>Add profile</gui> to select an existing profile or import
      a new profile.</p>
    </item>
    <item>
      <p>Press <gui>Add</gui> to confirm your selection.</p>
    </item>
    <item>
      <p>To change the used profile, select the profile you would like to use
      and press <gui>Enable</gui> to confirm your selection.</p>
    </item>
  </steps>

  <p>Each device can have multiple profiles assigned to it, but only one
  profile can be the <em>default</em> profile. The default profile is used when
  there is no extra information to allow the profile to be chosen
  automatically. An example of this automatic selection would be if one profile
  was created for glossy paper and another plain paper.</p>

  <p>If calibration hardware is connected, the <gui>Calibrate…</gui> button
  will create a new profile.</p>

</page>
