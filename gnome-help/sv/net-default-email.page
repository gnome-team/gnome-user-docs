<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-email" xml:lang="sv">

  <info>
    <link type="guide" xref="net-email"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.38.2" date="2021-03-07" status="final"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändra e-postklienten att använda som standard genom att gå till <gui>Standardprogram</gui> i <gui>Inställningar</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ändra vilket e-postprogram som används för att skriva e-post</title>

  <p>När du klickar på en knapp eller en länk för att skick ett nytt e-postmeddelande (till exempel i ditt ordbehandlingsprogram), kommer standardepostprogrammet att öppnas med ett tomt meddelande så att du kan skriva. Om du däremot har mer än ett e-postprogram installerat så kan fel e-postprogram öppnas. Du kan fixa detta genom att ändra vilket program som är standardepostprogrammet:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Program</gui>.</p>
    </item>
    <item>
      <p>Välj <guiseq><gui>Inställningar</gui><gui>Program</gui></guiseq> från resultaten. Detta kommer att öppna panelen <gui>Program</gui>.</p>
    </item>
    <item>
      <p>Välj <gui>Standardprogram</gui>.</p>
    </item>
    <item>
      <p>Välj vilken e-postklient du önskar använda som standard genom att ändra alternativet <gui>E-post</gui>.</p>
    </item>
  </steps>

</page>
