<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="sv">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ställ in en skrivare som är ansluten till din dator, eller ditt lokala nätverk.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ställ in en lokal skrivare</title>

  <p>Ditt system kan automatiskt känna igen många typer av skrivare när de ansluts. De flesta skrivare kopplas in med en USB-kabel som ansluts till din dator, men en del skrivare ansluts till ditt trådbundna eller trådlösa nätverk.</p>

  <note style="tip">
    <p>Om din skrivare är ansluten till nätverket kommer den inte automatiskt att ställas in – du bör lägga till den från panelen <gui>Skrivare</gui> i <gui>Inställningar</gui>.</p>
  </note>

  <steps>
    <item>
      <p>Säkerställ att skrivaren är påslagen.</p>
    </item>
    <item>
      <p>Anslut skrivaren till ditt system via en lämplig kabel. Det kan förekomma aktivitet på skärmen medan systemet letar efter drivrutiner och du kan bli tillfrågad att autentisera dig för att installera dem.</p>
    </item>
    <item>
      <p>Ett meddelande kommer att visas när systemet är färdigt med att installera skrivaren. Välj <gui>Skriv ut testsida</gui> för att skriva ut en testsida eller <gui>Alternativ</gui> för att göra ytterligare ändringar i skrivarinställningen.</p>
    </item>
  </steps>

  <p>Om din skrivare inte ställdes in automatiskt kan du lägga till den bland skrivarinställningarna:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Skrivare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skrivare</gui>.</p>
    </item>
    <item>
      <p>Beroende på ditt system kan du behöva trycka på <gui style="button">Lås upp</gui> i övre högra hörnet och skriva in ditt lösenord när du blir tillfrågad.</p>
    </item>
    <item>
      <p>Tryck på knappen <gui style="button">Lägg till skrivare…</gui>.</p>
    </item>
    <item>
      <p>I fönstret som poppar upp, välj din nya skrivare och tryck på <gui style="button">Lägg till</gui>.</p>
      <note style="tip">
        <p>Om din skrivare inte upptäcks automatiskt, men du vet dess nätverksadress, mata in den i textfältet längst ner i dialogrutan och tryck på <gui style="button">Lägg till</gui></p>
      </note>
    </item>
  </steps>

  <p>Om din skrivare inte visas i fönstret <gui>Lägg till skrivare</gui> kan du behöva installera skrivardrivrutiner.</p>

  <p>Efter att du installerat skrivaren, kan det vara bra att <link xref="printing-setup-default-printer">ändra din standardskrivare</link>.</p>

</page>
