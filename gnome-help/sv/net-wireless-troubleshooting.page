<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="sv">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Medarbetare för Ubuntus dokumentations-wiki</name>
    </credit>
    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identifiera och fixa problem med trådlösa anslutningar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Felsökning av trådlösa nätverk</title>

  <p>Detta är en steg-för-steg felsökningsguide för att hjälpa dig att identifiera och fixa trådlösa problem. Om du inte kan ansluta till ett trådlöst nätverk av någon anledning, prova att följa instruktionerna här.</p>

  <p>Vi kommer att gå genom följande steg för att få din dator ansluten till internet:</p>

  <list style="numbered compact">
    <item>
      <p>Göra en första kontroll</p>
    </item>
    <item>
      <p>Hämta information om din hårdvara</p>
    </item>
    <item>
      <p>Kontrollera din hårdvara</p>
    </item>
    <item>
      <p>Försöka att skapa en anslutning till din trådlösa router</p>
    </item>
    <item>
      <p>Göra en kontroll av ditt modem och router</p>
    </item>
  </list>

  <p>För att komma igång, klicka på länken <em>Nästa</em> längst upp till höger på sidan. Denna länk, och andra som den på efterföljande sidor, kommer att ta dig genom varje steg i guiden.</p>

  <note>
    <title>Via kommandoraden</title>
    <p>Några av instruktionerna i denna guide ber dig skriva kommandon på <em>kommandoraden</em>. Du kan hitta <app>Terminal</app>-programmet i översiktsvyn <gui>Aktiviteter</gui>.</p>
    <p>Om du inte är bekant med en kommandorad, oroa inte dig — denna guide kommer att hjälpa dig genom varje steg. Allt du behöver komma ihåg är att kommandon är skiftlägeskänsliga (så du måste skriva in dem <em>exakt</em> som de visas här), och att trycka på <key>Retur</key> efter att varje kommando skrivits in för att köra det.</p>
  </note>

</page>
