<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="sv">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lägg till tangentbordslayouter och växla mellan dem.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Använd alternativa tangentbordslayouter</title>

  <p>Tangentbord levereras i hundratals olika layouter för olika språk. Ofta finns det till och med flera tangentbordslayouter för ett och samma språk, exempelvis Dvorak-layouten för engelska. Du kan få ditt tangentbord att bete sig som ett tangentbord med en annan layout, oavsett bokstäverna och symbolerna som står skrivna på tangenterna. Detta är användbart om du ofta växlar mellan flera språk.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Tangentbord</gui>.</p>
    </item>
    <item>
      <p>Välj <guiseq><gui>Inställningar</gui><gui>Tangentbord</gui></guiseq> från resultaten. Detta kommer att öppna panelen <gui>Tangentbord</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>+ Lägg till inmatningskälla…</gui>-knappen i avsnittet <gui>Inmatningskällor</gui>, välj språket som är associerat med layouten, välj sedan en layout och tryck på <gui>Lägg till</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Om det finns flera användarkonton på ditt system finns det ett separat avsnitt för <gui>Inloggningsskärm</gui> i panelen <gui>Region &amp; språk</gui> i panelen <gui>System</gui>.</p>

    <p>Några sällan använda varianter av tangentbordslayout finns inte tillgängliga som standard då du klickar på <gui>+ Lägg till inmatningskälla…</gui>-knappen. För att även göra dessa inmatningskällor tillgängliga kan du öppna ett terminalfönster och köra detta kommando:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Du kan förhandsgranska en bild av vilken layout som helst genom att klicka på knappen <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">inställningar</span></media> intill språket i listan <gui>Inmatningskällor</gui> och välja <gui style="menuitem">Visa tangentbordslayout</gui>.</p>
  </note>

  <p>Vissa språk erbjuder extra konfigurationsalternativ. Du kan identifiera dessa språk eftersom de har en <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">inställningar</span></media></gui>-ikon intill dem i dialogrutan <gui>Lägg till en inmatningskälla</gui>. Om du vill nå dessa extra parametrar, klicka på knappen <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">inställningar</span></media> intill språket i listan <gui>Inmatningskällor</gui> och välj <gui style="menuitem">Inställningar</gui> som kommer ge dig tillgång till de extra inställningarna.</p>

  <p>När du använder flera layouter kan du välja att låta alla fönster ha samma layout eller ställa in olika layouter för varje fönster. Att använda olika layouter för varje fönster är användbart om du till exempel skriver en artikel på ett annat språk i ett ordbehandlarfönster. Ditt tangentbordsval kommer att kommas ihåg för varje fönster när du växlar mellan fönster. Tryck på <gui style="button">Alternativ</gui>-knappen för att välja hur du vill hantera flera layouter.</p>

  <p>Systemraden kommer att visa ett kort namn på den aktuella layouten, som till exempel <gui>en</gui> för engelskans standardlayout. Klicka på layout-indikatorn och välj layouten du vill använda från menyn. Om det valda språket har några extra inställningar kommer dessa att visa under listan av tillgängliga layouter. Detta ger dig en snabb överblick av dina inställningar. Du kan också öppna en bild med den aktuella tangentbordslayouten för referens.</p>

  <p>Det snabbaste sättet att byta till en annan layout är genom att använda <gui>Tangentbordsgenvägar</gui> för <gui>Inmatningskällor</gui>. Dessa kortkommandon öppnar väljaren av <gui>Inmatningskällor</gui> där du kan flytta framåt eller bakåt. Som standard kan du växla till nästa inmatningskälla med <keyseq><key xref="keyboard-key-super">Super</key><key>Blanksteg</key></keyseq> och till föregående med <keyseq><key>Skift</key><key>Super</key><key>Blanksteg</key></keyseq>. Du kan ändra dessa kortkommandon i inställningarna för <gui>Tangentbord</gui> under <guiseq><gui>Tangentbordsgenvägar</gui><gui>Visa och anpassa kortkommandon</gui><gui>Skriva</gui></guiseq>.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
