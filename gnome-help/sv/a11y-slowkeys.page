<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="sv">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktivera en fördröjning mellan tangenttryckning till det att bokstaven syns på skärmen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Aktivera tröga tangenter</title>

  <p>Aktivera <em>tröga tangenter</em> om du vill att det ska bli en fördröjning från det att en tangent trycks ner tills det tecknet visas på skärmen. Detta innebär att du måste hålla ner varje tangent som du vill skriva ett litet tag innan tecknet visas. Använd tröga tangenter om du av misstag trycker ner flera tangenter åt gången när du skriver, eller om du tycker att det är svårt att direkt trycka på rätt tangent på tangentbordet.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Hjälpmedel</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj avsnittet <gui>Skriva</gui> för att öppna det.</p>
    </item>
    <item>
      <p>I avsnittet <gui>Skrivassistent</gui>, slå på <gui>Tröga tangenter</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Aktivera och inaktivera tröga tangenter snabbt</title>
    <p>Växla värdet för brytaren <gui>Aktivera med tangentbord</gui> för att kunna aktivera eller inaktivera tröga tangenter från tangentbordet. När denna inställning är vald kan du trycka och hålla ner <key>Skift</key> under åtta sekunder för att aktivera eller inaktivera tröga tangenter.</p>
    <p>Du kan också aktivera eller inaktivera tröga tangenter genom att klicka på <link xref="a11y-icon">hjälpmedelsikonen</link> i systemraden och välja <gui>Tröga tangenter</gui>. Hjälpmedelsikonen är synlig när en eller flera inställningar har aktiverats från panelen <gui>Hjälpmedel</gui>.</p>
  </note>

  <p>Använd skjutreglaget <gui>Acceptansfördröjning</gui> för att styra hur länge du måste hålla ner en tangent innan den registreras. Se <link xref="a11y-bouncekeys"/> för mer information.</p>

  <p>Du kan låta datorn spela upp ett ljud när du trycker ner en tangent, när en tangenttryckning accepteras eller när ett tangentnedslag avvisas för att du inte höll ner tangenten länge nog.</p>

</page>
