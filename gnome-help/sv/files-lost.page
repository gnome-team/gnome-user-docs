<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="sv">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision version="gnome:45" date="2024-03-04" status="candidate"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Följ dessa tips om du inte kan hitta en fil du har skapat eller hämtat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Hitta en borttappad fil</title>

<p>Om du skapat eller hämtat en fil men inte kan hitta den, följ dessa tips.</p>

<list>
  <item><p>Om du inte kommer ihåg var du sparade filen, men har en idé om vad du kallade den kan du <link xref="files-search">söka efter filen efter namn</link>.</p></item>

  <item><p>Om du nyss hämtade filen kan din webbläsare automatiskt ha sparat den till en gemensam mapp. Kontrollera mapparna <file>Skrivbord</file> och <file>Hämtningar</file> i din hemmapp.</p></item>

  <item><p>Du kan av misstag ha tagit bort filen. När du tar bort en fil flyttas den till papperskorgen där den stannar kvar tills du manuellt tömmer papperskorgen. Se <link xref="files-recover"/> för att lära dig hur du återställer en borttagen fil.</p></item>

  <item><p>Du kan ha bytt namn på filen på ett sätt som gjort filen dold. Filer som börjar med en <file>.</file> eller slutar med ett <file>~</file> är dolda i filhanteraren. Tryck på menyknappen i fönstrets sidopanel och aktivera <gui>Visa dolda filer</gui> för att visa dem. Se <link xref="files-hidden"/> för att lära dig mer.</p></item>
</list>

</page>
