<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="sv">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Styr hur snabbt du behöver trycka på musknappen en andra gång för att dubbelklicka.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Justera hastigheten för dubbelklick</title>

  <p>Dubbelklick händer bara när du trycker på musknappen två gånger tillräckligt snabbt. Om det andra klicket kommer för sent efter det första får du bara två separata klick, inte ett dubbelklick. Om du har svårt för att snabbt trycka på musknappen bör du öka tidsgränsen.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Hjälpmedel</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj avsnittet <gui>Peka och klicka</gui> för att öppna det.</p>
    </item>
    <item>
      <p>Justera skjutreglaget <gui>Dubbelklicksfördröjning</gui> till ett värde som du tycker är bekvämt.</p>
    </item>
  </steps>

  <p>Om din mus dubbelklickar när du vill att den enkelklickar även om du har ökat tidsgränsen för dubbelklick kan det vara något fel på musen. Försök att ansluta en annan mus till din dator och se om den fungerar som den ska. Du kan också ansluta din mus till en annan dator och se om den uppvisar samma problem.</p>

  <note>
    <p>Den här inställningen kommer påverka både din mus och din styrplatta, såväl som övriga pekdon.</p>
  </note>

</page>
