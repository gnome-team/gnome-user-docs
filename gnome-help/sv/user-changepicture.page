<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="sv">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lägg till ditt foto till inloggnings- och användarskärmarna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ändra ditt foto på inloggningsskärmen</title>

  <p>När du loggar in eller växlar användare kommer du att se en lista över användare med deras inloggningsbild. Du kan ändra ditt foto till en bild från en bildbank eller en egen bild. Du kan till och med ta en ny inloggningsbild med din webbkamera.</p>

  <p>Du behöver <link xref="user-admin-explain">administratörsbehörighet</link> för att redigera användarkonton andra än ditt eget.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>System</gui>.</p>
    </item>
    <item>
      <p>Välj <guiseq><gui>Inställningar</gui><gui>System</gui></guiseq> från resultaten. Detta kommer att öppna panelen <gui>System</gui>.</p>
    </item>
    <item>
      <p>Välj <gui>Användare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Om du vill redigera en annan användare än dig själv, tryck på <gui style="button">Lås upp</gui> i övre högra hörnet och skriv in ditt lösenord när du blir tillfrågad. Välj användaren från <gui>Andra användare</gui>.</p>
    </item>
    <item>
      <p>Klicka på pennikonen intill ditt namn. Ett rullgardinsgalleri visas som har några inloggningsbilder från en bildbank. Om du tycker om någon av dem klickar du på den för att använda den för dig själv.</p>
      <list>
        <item>
          <p>Om du hellre vill använda en bild du redan har på din dator, klicka på <gui>Välj en fil…</gui>.</p>
        </item>
        <item>
          <p>Om du har en webbkamera kan du nu ta en ny inloggningsbild genom att klicka på <gui>Ta en bild…</gui>. Ta din bild och flytta och ändra storlek på rutan för att beskära de delar som du inte vill ha. Om du inte tycker om bilden du tog, klicka på <gui style="button">Ta ett foto till</gui> för att försöka igen, eller <gui>Avbryt</gui> för att ge upp.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
