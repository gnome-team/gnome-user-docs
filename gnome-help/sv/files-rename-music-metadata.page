<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename-music-metadata" xml:lang="sv">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision version="gnome:47" date="2024-08-31" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Byt namn på musikfiler med hjälp av metadata de innehåller.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Byt namn på musikfiler med hjälp av metadata</title>

  <p>Du kan lägga till inbäddade metadata från musikfiler i deras filnamn.</p>

  <steps>
    <title>För att lägga till metadata till ett filnamn:</title>
    <item><p>Välj två eller flera musikfiler i <app>Filer</app>.</p></item>
    <item><p>Tryck <key>F2</key> eller högerklicka på markeringen och välj <gui>Byt namn</gui>. Fönstret <gui>Byt namn</gui> öppnas med <gui>Byt namn med hjälp av en mall</gui> valt.</p>
      <note>
        <p>Namnfältet innehåller ”[Ursprungligt filnamn]” som en platshållare. Den kan tas bort eller läggas tillbaka från menyn <gui>+ Lägg till</gui>.</p>
      </note>
    </item>
    <item><p>Klicka på <gui>+ Lägg till</gui> för att visa menyn med tillgängliga metadata, som <gui>Album</gui> och <gui>Spår</gui>. Välj varje objekt för att placera det i filnamnsmallen i ordningen du vill ha.</p>
    <p>Förhandsvisningslistan kommer visa de aktuella filnamnen till vänster och de resulterande filnamnen till höger.</p></item>
    <item><p>Klicka på <gui>Byt namn</gui>-knappen för att verkställa mönstret du skapat, så kommer filerna byta namn.</p></item>
  </steps>

  <note style="tip">
    <p>Om du bytt namn på fel fil eller gav filen fel namn kan du ångra namnbytet. För att ångra åtgärden tryck omedelbart på menyknappen i sidopanelen och välj <gui>Ångra namnbyte</gui>, eller tryck <keyseq><key>Ctrl</key><key>Z</key></keyseq> för att återställa det gamla namnet.</p>
  </note>

</page>
