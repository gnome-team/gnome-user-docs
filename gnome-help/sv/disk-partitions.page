<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="disk-partitions" xml:lang="sv">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <desc>Förstå vad volymer och partitioner är och använda diskverktyget för att hantera dem.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

 <title>Hantera volymer och partitioner</title>

  <p>Ordet <em>volym</em> används för att beskriva en lagringsenhet, exempelvis en hårddisk. Det kan också referera till en <em>del</em> av lagringen på den enheten, eftersom du kan dela upp lagringen i bitar. Datorn gör denna lagring tillgänglig via ditt filsystem via en process som kallas <em>montering</em>. Monterade volymer kan vara hårddiskar, USB-enheter, dvd-rw, SD-kort och övriga media. Om en volym för närvarande är monterad kan du läsa (och möjligtvis skriva till) filer på den.</p>

  <p>Ofta kallas en monterad volym för <em>partition</em> även om det inte nödvändigtvis är samma sak. En ”partition” refererar till en <em>fysisk</em> lagringsyta på en enda diskenhet. När en partition har monterats kan den refereras till som en volym eftersom du kan nå filerna på den. Du kan tänka på volymer som märkta, tillgängliga ”affärsfönster” till de funktionella bakre rummen som utgörs av partitioner och enheter.</p>

<section id="manage">
 <title>Visa och hantera volymer och partitioner via diskverktyget</title>

  <p>Du kan kontrollera och modifiera din dators lagringsvolymer med diskverktyget.</p>

<steps>
  <item>
    <p>Öppna översiktsvyn <gui>Aktiviteter</gui> och starta <app>Diskar</app>.</p>
  </item>
  <item>
    <p>I listan över lagringsenheter till vänster hittar du hårddiskar, cd/dvd-enheter och andra fysiska enheter. Klicka på enheten som du vill inspektera.</p>
  </item>
  <item>
    <p>Den högra panelen visar en visuell fördelning över volymerna och partitionerna som finns på den markerade enheten. Den innehåller också en uppsjö verktyg för att hantera dessa volymer.</p>
    <p>Var försiktig: det är möjligt att helt radera all data på din disk med dessa verktyg.</p>
  </item>
</steps>

  <p>Din dator har troligtvis en <em>primärpartition</em> och en enda <em>växlingspartition</em>. Växlingspartitionen används av operativsystemet för minneshantering och monteras sällan. Den primära partitionen innehåller ditt operativsystem, dina program, inställningar och personliga filer. Dessa filer kan också vara utspridda över flera partitioner av säkerhetsskäl eller av bekvämlighet.</p>

  <p>En primärpartition måste innehålla information som din dator använder för att starta (kallas ibland <em>boota</em>). Av denna anledning kallas den vanligtvis startpartition eller startvolym. För att avgöra om en volym är startbar så välj partitionen och klicka på menyknappen i verktygsfältet under partitionslistan. Klicka sedan på <gui>Redigera partition…</gui> och titta på dess <gui>Flaggor</gui>. Externa media såsom USB-enheter och cd kan också innehålla startbara volymer.</p>

</section>

</page>
