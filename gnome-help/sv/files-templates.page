<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="sv">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skapa snabbt dokument från anpassade filmallar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Mallar för vanliga dokumenttyper</title>

  <p>Om du ofta skapar dokument baserade på samma innehåll kan du dra nytta av att använda filmallar. En filmall kan vara ett dokument av vilken typ som helst med formatering eller innehåll som du önskar återanvända. Du kan till exempel skapa en dokumentmall med ditt brevhuvud.</p>

  <steps>
    <title>Skapa en ny mall</title>
    <item>
      <p>Skapa ett dokument som du vill använda som mall. Du kan till exempel skapa ditt brevhuvud i ett ordbehandlingsprogram.</p>
    </item>
    <item>
      <p>Spara filen med mallinnehållet i mappen <file>Mallar</file> i din <file>Hem</file>-mapp. Om mappen <file>Mallar</file> inte existerar, måste du skapa den först.</p>
    </item>
  </steps>

  <steps>
    <title>Använd en mall för att skapa ett dokument</title>
    <item>
      <p>Öppna mappen där du vill placera det nya dokumentet.</p>
    </item>
    <item>
      <p>Högerklicka var som helst i det tomma utrymmet i mappen, välj sedan <gui style="menuitem">Skapa nytt dokument</gui>. Namnen på tillgängliga mallar kommer att listas i undermenyn.</p>
    </item>
    <item>
      <p>Välj din mall från listan.</p>
    </item>
    <item>
      <p>Dubbelklicka på filen för att öppna den och börja redigera. Du kanske vill <link xref="files-rename">byta namn på filen</link> när du är klar.</p>
    </item>
  </steps>

</page>
