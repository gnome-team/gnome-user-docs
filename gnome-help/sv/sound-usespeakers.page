<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="sv">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Anslut högtalare eller hörlurar och välj en standardenhet för ljudutmatning.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Använd andra högtalare eller hörlurar</title>

  <p>Du kan använda externa högtalare eller hörlurar med din dator. Högtalare kopplas vanligtvis via en cirkulär teleplugg (kallas ibland <em>TRS</em>-plugg) eller via USB.</p>

  <p>Om dina högtalare eller hörlurar har en teleplugg, koppla in den i en lämplig kontakt på din dator. De flesta datorer har två kontakter: en för mikrofoner och en för högtalare. Denna kontakt är vanligtvis ljusröd eller har en bild på en mikrofon intill kontakten. Högtalare eller hörlurar som kopplats in i en telepluggskontakt kommer vanligtvis att användas som standard. Om inte, se instruktionerna nedan för att välja standardenheten.</p>

  <p>Vissa datorer har stöd för flerkanals ljud med surround-ljud. Detta använder vanligtvis flera telepluggskontakter. vilka ofta är färgkodade. Om du är osäker på vilket kontaktdon som ska anslutas till vilken kontakt kan du prova ljudutgången i ljudinställningarna.</p>

  <p>Om du har USB-högtalare eller hörlurar eller analoga hörlurar kopplade i ett USB-ljudkort kan du koppla in dem i vilken USB-kontakt som helst. USB-högtalare fungerar som separata ljudenheter och du kan behöver ange vilka högtalare som ska användas som standard.</p>

  <steps>
    <title>Välj en standardenhet för ljudutmatning</title>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Ljud</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ljud</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>I avsnittet <gui>Utgång</gui>, välj enheten som du vill använda.</p>
    </item>
  </steps>

  <p>Använd knappen <gui style="button">Testa</gui> för att kontrollera att alla högtalare fungerar och är anslutna till rätt kontakt.</p>

</page>
