<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="sv">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändra namnet eller platsen för en skrivare i skrivarinställningarna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>
  
  <title>Ändra namnet eller platsen för en skrivare</title>

  <p>Du kan ändra namnet eller platsen för en skrivare i skrivarinställningarna.</p>

  <note>
    <p>Du behöver <link xref="user-admin-explain">administratörsbehörighet</link> på systemet för att ändra namnet eller platsen för en skrivare.</p>
  </note>

  <section id="printer-name-change">
    <title>Ändra skrivarnamn</title>

  <p>Om du vill ändra namnet på en skrivare, följ dessa steg:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Skrivare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skrivare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Beroende på ditt system kan du behöva trycka på <gui style="button">Lås upp</gui> i övre högra hörnet och skriva in ditt lösenord när du blir tillfrågad.</p>
    </item>
    <item>
      <p>Klicka på knappen <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">inställningar</span></media> intill skrivaren.</p>
    </item>
    <item>
      <p>Välj <gui style="menuitem">Skrivardetaljer</gui>.</p>
    </item>
    <item>
      <p>Ange ett nytt namn på skrivaren i fältet <gui>Namn</gui>.</p>
    </item>
    <item>
      <p>Stäng dialogrutan.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Ändra skrivarplats</title>

  <p>För att ändra platsen för din skrivare:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Skrivare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skrivare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Beroende på ditt system kan du behöva trycka på <gui style="button">Lås upp</gui> i övre högra hörnet och skriva in ditt lösenord när du blir tillfrågad.</p>
    </item>
    <item>
      <p>Klicka på knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">inställningar</span></media> intill skrivaren.</p>
    </item>
    <item>
      <p>Välj <gui style="menuitem">Skrivardetaljer</gui>.</p>
    </item>
    <item>
      <p>Ange en ny plats för skrivaren i fältet <gui>Plats</gui>.</p>
    </item>
    <item>
      <p>Stäng dialogrutan.</p>
    </item>
  </steps>

  </section>

</page>
