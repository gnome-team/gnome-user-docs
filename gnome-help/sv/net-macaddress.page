<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="sv">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Den unika identifieraren som tilldelats din nätverkshårdvara.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Vad är en MAC-adress?</title>

  <p>En <em>MAC-adress</em> är den unika identifierare som av tillverkaren tilldelats till en nätverkshårdvara (exempelvis ett trådlöst kort eller ett ethernet-kort). MAC står för <em>Media Access Control</em> och varje identifierare är avsedd att vara unik för en specifik enhet.</p>

  <p>En MAC-adress består av sex grupper av två tecken, separerade med ett kolon. <code>00:1B:44:11:3A:B7</code> är ett exempel på en MAC-adress.</p>

  <p>För att identifiera MAC-adressen hos din egen nätverkshårdvara:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Nätverk</gui> för trådbundna anslutningar eller <gui>Trådlöst</gui> för trådlösa anslutningar.</p>
    </item>
    <item>
      <p>Klicka på <gui>Nätverk</gui> eller <gui>Trådlöst</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Klicka på knappen <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">inställningar</span></media> intill den aktiva anslutningen.</p>
    </item>
    <item>
      <p>MAC-adressen för enheten kommer att visas som <gui>Hårdvaruadress</gui> i panelen <gui>Detaljer</gui>.</p>
    </item>
  </steps>

  <p>I praktiken kan du behöva modifiera eller ”spoof:a” en MAC-adress. Vissa internetleverantörer kan till exempel kräva att en specifik MAC-adress används för att nå deras tjänst. Om nätverkskortet slutar fungera och du behöver ersätta med ett nytt kort, kommer tjänsten inte att fungera längre. I ett sådant fall kan du behöva modifiera MAC-adressen.</p>

</page>
