<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="sv">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hantera och organisera filer med filhanteraren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Bläddra bland filer och mappar</title>

<p>Använd filhanteraren <app>Filer</app> för att bläddra bland och organisera filerna på din dator. Du kan också använda den för att hantera filer på lagringsenheter (som externa hårddiskar), på <link xref="nautilus-connect">filservrar</link> och på nätverksdiskar.</p>

<p>För att starta filhanteraren, öppna <app>Filer</app> i översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui>. Du kan också söka efter filer och mappar via översiktsvyn på samma sätt som du kan <link xref="shell-apps-open">söka efter program</link>.</p>

<section id="files-view-folder-contents">
  <title>Utforska innehåller i mappar</title>

<p>I filhanteraren, dubbelklicka på vilken mapp som helst för att se dess innehåll och dubbelklicka eller <link xref="mouse-middleclick">mittenklicka</link> på vilken fil som helst för att öppna den med standardprogrammet för den filen. Mittenklicka på en mapp för att öppna den i en ny flik. Du kan också högerklicka på en mapp för att öppna den i en ny flik eller ett nytt fönster.</p>

<p>När du letar genom filerna i en mapp kan du snabbt <link xref="files-preview">förhandsgranska varje fil</link> genom att trycka på mellanslagstangenten för att säkerställa att du har fått tag i rätt fil innan du öppnar, kopierar eller tar bort den.</p>

<p><em>Sökvägsraden</em>, ovanför listan av filer och mappar, visar dig vilken mapp du tittar i, inklusive den aktuella mappens föräldramappar. Klicka på en föräldramapp i sökvägsraden för att gå till den mappen. Högerklicka på vilken mapp som helst i sökvägsraden för att öppna den i en ny flik eller ett nytt fönster, eller se dess egenskaper.</p>

<p>Om du vill snabbt <link xref="files-search">söka efter en fil</link>, i eller under mappen du tittar i, börja skriv dess namn. En <em>sökrad</em> kommer att visas i toppen av fönstret och den första filen som matchar din sökning kommer att markeras. Tryck på <key>Esc</key> för att avbryta sökningen.</p>

<p>Du kan snabbt nå vanliga platser från <em>sidopanelen</em>. Om du inte ser sidopanelen, tryck på <gui>Sidopanel</gui> i fönstrets övre vänstra hörn. Du kan <link xref="nautilus-bookmarks-edit">lägga till bokmärken för mappar som du använder ofta</link> så att de kommer att visas i sidopanelen.</p>

</section>

</page>
