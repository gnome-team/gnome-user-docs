<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="sv">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Andra saker kan hämta filer, du kan ha en dålig anslutning eller så kan det vara en hektisk tid på dagen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Internet verkar vara långsamt</title>

  <p>Om du använder internet och det verkar långsamt då finns det ett antal saker som kan orsaka att det går långsamt.</p>

  <p>Prova att stänga din webbläsare och sedan öppna den igen, samt att prova att koppla ner från internet och sedan ansluta igen. (Att göra detta återställer en massa saker som kan orsaka att internet går långsamt.)</p>

  <list>
    <item>
      <p><em style="strong">En hektisk tid på dagen</em></p>
      <p>Internetleverantörer tillhandahåller ställer ofta in internetanslutningar så att de delas mellan flera hushåll. Även om ni ansluter separat via era egna telefonlinjer eller kabelanslutningar, så är anslutningen till resten av internet vid telestationen antagligen delad. Om detta är fallet och många av dina grannar använder internet samtidigt som du kan du uppleva att det går långsamt. Du löper störst risk att uppleva detta vid tidpunkter när dina grannar antagligen använder internet (exempelvis på kvällar).</p>
    </item>
    <item>
      <p><em style="strong">Hämta många saker samtidigt</em></p>
      <p>Om du eller någon annan som använder din internetanslutning hämtar ner många filer samtidigt eller tittar på videor, så kanske internetanslutningen inte är snabb nog för att hinna med efterfrågan. Om detta är fallet kommer det att upplevas långsammare.</p>
    </item>
    <item>
      <p><em style="strong">Otillförlitlig anslutning</em></p>
      <p>Vissa internetanslutningar är helt enkelt otillförlitliga, speciellt tillfälliga sådana eller de på platser där efterfrågan är stor. Om du är på ett hektiskt café eller på ett konferenscenter så kanske internetanslutningen är allt för upptagen eller helt enkelt otillförlitlig.</p>
    </item>
    <item>
      <p><em style="strong">Låg signalstyrka för trådlösa anslutningar</em></p>
      <p>Om du är trådlöst ansluten till internet (Wi-Fi), kontrollera nätverksikonen i systemraden för att se om du har bra trådlös signalmottagning. Om inte kan internet upplevas långsamt för att du inte har en särskilt stark signal.</p>
    </item>
    <item>
      <p><em style="strong">Att använda en långsammare, mobil internetanslutning</em></p>
      <p>Om du har en mobil internetanslutning och märker att den är långsam kan du ha förflyttat dig in i ett område där signalmottagningen är dålig. När detta händer kommer internetanslutningen automatiskt att växla från en snabb ”mobilt bredbands”-anslutning som 3G till en mer tillförlitlig, men långsammare, anslutning som GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Webbläsaren har problem</em></p>
      <p>Ibland inträffar problem som gör webbläsare långsamma. Detta hända på grund av ett antal olika skäl — du kan ha besökt en webbplats som webbläsaren kämpade med att ladda, eller så kan du till exempel ha haft webbläsaren öppen länge. Prova att stänga alla webbläsarens fönster och sedan öppna webbläsaren igen för att se om detta gör någon skillnad.</p>
    </item>
  </list>

</page>
