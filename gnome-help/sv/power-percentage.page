<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-percentage" xml:lang="sv">

  <info>

    <link type="guide" xref="power"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-status"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Visa batteriprocent i systemraden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>
  
  <title>Visa batteristatus som en procentsats</title>

  <p><link xref="status-icons#batteryicons">Statusikonen</link> i systemraden visar laddningsnivån för det interna huvudbatteriet, och huruvida det för närvarande laddas eller inte. Den kan också visa laddningen som en <link xref="power-percentage">procentsats</link>.</p>

  <steps>

    <title>Visa batteriprocent i systemraden</title>

    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Ström</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ström</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Slå på <gui>Visa batteriprocent</gui>.</p>
    </item>

  </steps>

</page>
