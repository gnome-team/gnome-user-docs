<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="tip" id="backup-why" xml:lang="sv">

  <info>
    <link type="guide" xref="files#backup"/>
    <title type="link" role="trail">Säkerhetskopiering</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Varför, vad, var och hur man säkerhetskopierar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Säkerhetskopiera dina viktiga filer</title>

  <p>Att <em>säkerhetskopiera</em> dina filer innebär helt enkelt att skapa en kopia av dem för säker förvaring. Detta görs ifall originalfilerna blir oanvändbara på grund av förlust eller datakorruption. Dessa kopior kan användas till att återställa originaldata om den förloras. Kopior bör sparas på en annan enhet än originalfilerna. Du kan till exempel använda en USB-enhet, en extern hårddisk, en cd/dvd eller en tjänst på nätet.</p>

  <p>Det bästa sättet att säkerhetskopiera dina filer är att göra det regelbundet, och att förvara kopiorna på annan plats och (helst) krypterade.</p>

<links type="topic" style="2column"/>

</page>
