<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="sv">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Om utskrifter är streckade, tonande eller saknar färger, kontrollera dina bläcknivåer eller rensa skrivarhuvudet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Varför finns det streck, linjer eller fel färger på mina utskrifter?</title>

  <p>Om dina utskrifter är streckade, bleka, har linjer på sig som inte borde vara där eller på annat sätt är av dålig kvalitet, kan detta bero på ett problem med skrivaren eller låg bläck- eller tonernivå.</p>

  <terms>
     <item>
       <title>Tonande text eller bilder</title>
       <p>Du kan hålla på att få slut på bläck eller toner. Kontrollera din bläck- eller tonernivå och köp en ny patron om det behövs.</p>
     </item>
     <item>
       <title>Streck och linjer</title>
       <p>Om du har en bläckstråleskrivare kan skrivarhuvudet blir smutsigt eller delvis blockerat. Försök att rensa skrivarhuvudet. Se skrivarens manual för instruktioner.</p>
     </item>
     <item>
       <title>Felaktiga färger</title>
       <p>Skrivaren kan ha fått slut på en bläck- eller tonerfärg. Kontrollera dina bläck- eller tonernivåer och köp en ny patron om det behövs.</p>
     </item>
     <item>
       <title>Taggiga linjer eller linjer som inte är raka</title>
       <p>Om linjer på din utskrift som borde vara raka ser taggiga ut kan du behöva att rikta in skrivarhuvudet. Se skrivarens instruktionsbok för detaljer om hur du gör detta.</p>
     </item>
  </terms>

</page>
