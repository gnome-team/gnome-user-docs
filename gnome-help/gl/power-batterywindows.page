<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="gl">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Os axustes dos fabricantes e diferentes estimacións de duración da batería poden ser a causa deste problema.</desc>
    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

<title>Por que teño menor tempo de batería que o que tiña en Windows/Mac OS?</title>

<p>Algúns computadores parecen ter unha duración menor da batería funcionando baixo GNU/Linux que a que teñen cando funcionan con Windows ou Mac OS. Unha razón é que os fabricantes instalan algún software especial para Windows/Mac OS que optimiza varias configuracións de hardware/software para un modelo de computador en especial. Istos axustes son xeralmente moi específicos e poden non estar documentados, por iso incluilos en GNU/Linux é moi difícil.</p>

<p>Desafortunadamente, non hai unha forma doada de aplicar estes axustes por si mesmo sen saber exactamente que hai. Pode atopar que usando algún <link xref="power-batterylife">método de aforro de enerxía</link> lle pode axudar. Se o seu computador ten un <link xref="power-batteryslow">procesador con velocidade variábel</link> podería ser útil cambiar as súas configuracións.</p>

<p>Outra razón posíbel é que o método de estimar o tempo de batería é diferente en Windows/Mac OS que en Linux. A duración real da batería pode ser a mesma, pero os métodos distintos ofrecen estimacións distintas.</p>
	
</page>
