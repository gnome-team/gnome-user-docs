<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="gl">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Engadir (ou eliminar) iconas de programas de uso frecuente no taboleiro.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Ancorar as súas aplicacións favoritas ao dash</title>

  <p>Para engadir unha aplicación ao <link xref="shell-introduction#activities">dash</link> e poder acceder a el facilmente:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Abra a <link xref="shell-introduction#activities">Vista de actividades</link> premendo <gui>Actividades</gui> na esquina superior esquerda da pantalla</p>
      <p if:test="platform:gnome-classic">Prema o menú <link xref="shell-terminology"><gui>Aplicacións</gui></link> na parte superior esquerda da pantalla e seleccione a <gui>Vista de actividades</gui> desde o menú.</p></item>
    <item>
      <p>Prema no botón de grade no panel e busque a aplicación que quere engadir.</p>
    </item>
    <item>
      <p>Prema co botón dereito sobre a icona da aplicación e seleccione <gui>Engadir aos favoritos</gui>.</p>
      <p>De forma alternativa, pode arrastrar a icona sobre o panel lateral.</p>
    </item>
  </steps>

  <p>Para eliminar unha icona de aplicación do taboleiro, prema co botón dereito sobre a icona da aplicación e seleccione <gui>Quitar de favoritos</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>As aplicacións favoritos tamén aparecen na sección <gui>Favoritos</gui> do <link xref="shell-terminology">menú de <gui>Aplicacións</gui></link>.</p>
  </note>

</page>
