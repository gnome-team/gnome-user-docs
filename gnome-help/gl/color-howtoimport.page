<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="gl">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Os perfís de cor impórtanse con só abrilos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Como importo os perfiles de cor?</title>

  <p>Pode importar un perfíil de cor premendo dúas veces sobre o ficheiro <file>.ICC</file> ou <file>.ICM</file> no explorador de ficheiros.</p>

  <p>Alternativamente pode xestionar os perfís de cor no panel de <gui>Cor</gui>.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Preferencias</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Preferencias</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Cor</gui> na barra lateral para abir o panel.</p>
    </item>
    <item>
      <p>Seleccoine o seu dispositivo.</p>
    </item>
    <item>
      <p>Prema <gui>Engadir perfil</gui> para seleccionar un perfil existente ou importar un ficheiro novo.</p>
    </item>
    <item>
      <p>Prema <gui>Engadir</gui> para confirmar a súa selección.</p>
    </item>
  </steps>

  <p>O fabricante da súa pantalla pode fornecer un perfíl que pode usar. Estos perfiles normalmente fanse para a pantalla media, polo que non é perfecto para a súa. Para un mellor calibrado, debería <link xref="color-calibrate-screen">crear o seu propio perfíl</link> usando un colorímetro e un espetrofotómetro.</p>

</page>
