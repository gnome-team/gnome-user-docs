<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="gl">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use un micrófono analóxico ou USB e seleccione un dispositivo de entrada predeterminado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Usan un micrófono diferente</title>

  <p>Pode empregar un micrófono externo para conferencias de son, gravacións de voz ou o uso de outras aplicacións multimedia. Incluso se o seu equipo ten un micrófono ou unha cámara web con micrófono, un micrófono separado podería fornecer unha mellor calidade de son.</p>

  <p>Se o seu micrófono ten un enchufe circular, só ten que conéctalo á toma de audio axeitada do seu ordenador. A maioría dos ordenadores teñen dúas tomas: unha para micrófonos e outra para altofalantes. Este enchufe adoita ser de cor vermella clara ou vai acompañado dunha imaxe dun micrófono. Os micrófonos conectados á toma axeitada adoitan utilizarse por defecto. Se non, consulte as instrucións a continuación para seleccionar un dispositivo de entrada predeterminado.</p>

  <p>Se ten un micrófono USB, conécteo en calquera porto USB do seu computador. Os micrófonos USB actúan como dispositivos de son separados, e pode ter que especificar que micrófono usar de maneira predeterminada.</p>

  <steps>
    <title>Seleccione un dispositivo de entrada de son diferente</title>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Son</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Son</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Na sección <gui>Entrada</gui>, seleccione o dispositivo que quere utilizar. O indicador de nivel de entrada debe responder cando fale.</p>
    </item>
  </steps>

  <p>Pode axustar o volume e trocar o micrófono desde este panel.</p>

</page>
