<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-os" xml:lang="gl">

  <info>
    <link type="guide" xref="about" group="os"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Descubra información sobre o sistema operativo instalado no seu sistema.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Busca información sobre o seu sistema operativo</title>

  <p>Saber que sistema operativo está instalado pode axudarche a comprender se o novo software ou hardware será compatible co teu sistema.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>System</gui>.</p>
    </item>
    <item>
     <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
     results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Preme <gui>Sobre</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione <gui>Información do sistema</gui>.</p>
    </item>
    <item>
      <p>Mire a información que aparece en <gui>Nome do SO</gui>, <gui>Tipo de SO</gui>, <gui>Versión de GNOME</gui>, etc.</p>
    </item>
  </steps>

  <note style="tip">
    <p>A información pódese copiar de cada elemento seleccionándoo e despois copiando no portapapeis. Isto facilita compartir información sobre o teu sistema con outras persoas.</p>
  </note>

</page>
