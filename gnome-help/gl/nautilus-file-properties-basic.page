<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="gl">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:46" date="2024-03-05" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ver información básica do ficheiro, asignar permisos e seleccionar aplicacións predeterminadas.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Propiedades do ficheiro</title>

  <p>Prema co botón dereito sobre un ficheiro e seleccione <gui>Propiedades</gui>.</p>

  <p>A xanela de propiedades do ficheiro móstralle información como o tipo de ficheiro, o tamaño do ficheiro e cando foi modificado por última vez. Se usa esta información a miúdo, pode facer que se mostre na <link xref="nautilus-list">visualización en lista con columnas</link> ou nas <link xref="nautilus-display#icon-captions">lendas das iconas</link>.</p>

  <p>For certain types of files, such as images and videos, there will be an
  extra <gui>Properties</gui> entry that provides information like the
  dimensions, duration, and codec.</p>

  <p>For the <gui>Permissions</gui> entry see
  <link xref="nautilus-file-properties-permissions"/>.</p>

<section id="basic">
 <title>Propiedades básicas</title>
 <terms>
  <item>
    <title><gui>Nome</gui></title>
    <p>O nome dun ficheiro ou cartafol.</p>
    <note style="tip">
      <p>Para renomear un ficheiro vexa <link xref="files-rename"/>.</p>
    </note>
  </item>
  <item>
    <title><gui>Tipo</gui></title>
    <p>Isto axúdalle a identificar o tipo de ficheiro, como un documento PDF, texto OpenDocument, ou imaxe JPEG. O tipo de ficheiro determina que aplicacións poden abrir o ficheiro, entre outras cousas. Por exemplo, non pode abrir unha imaxe con un reprodutor de música. Vexa <link xref="files-open"/> para obter máis información.</p>
    <note style="tip">
      <p>To change the default application to open a file type, see <link xref="files-open#default"/>.</p>
    </note>
  </item>

  <item>
    <title>Contido</title>
    <p>Este campo móstrase se está buscando polas propiedades dun cartafol. Axudaralle a ver o número de elementos dun cartafol. Se o cartafol ten outros cartafoles, cada un deles contarase como un só elemento, incluso se conteñen máis elementos dentro. Tamén se contará cada ficheiro unha sola vez. Se un cartafol está baleiro, os contidos mostrarán <gui>ningún</gui>.</p>
  </item>

  <item>
    <title>Tamaño</title>
    <p>Este campo móstrase se está ollando un ficheiro (e non un cartafol). O tamaño dun ficheiro permítelle ver cando espazo de disco está empregando. Isto tamén é un indicador de canto tempo lle levará descargar un ficheiro ou envialo por correo electrónico (leva máis tempo enviar/recibir ficheiros máis grandes).</p>
    <p>Os tamañs pódense dar en bytes, KB, MB ou GB; nos tres últimos casos, o tamaño en bytes aparece entre parénteses. Técnicamente, 1KB son 1024 bytes, 1MB son 1024 KB e así sucesivamente.</p>
  </item>

  <item>
    <title>Espazo libre</title>
    <p>Isto só se mostra para os cartafoles. Indica a cantidade total de espazo dispoñíbel no disco no que se atopa o cartafol. É útil para comprobar se o disco duro está cheo.</p>
  </item>

  <item>
    <title>Cartafol pai</title>
    <p>A localización de cada ficheiro do seu computador é fornecida pola súa <em>ruta absoluta</em>. Isto é un «enderezo» único do ficheiro no seu computador, composto por unha lista dos cartafoles polos que debe navegar para atopar un ficheiro. Por exemplo se Serafina creou un ficheiro co nome <file>Resumo.pdf</file> no seu cartafol persoal, a súa localización será <file>/home/serafina/Resumo.pdf</file>.</p>
  </item>

  <item>
    <title>Accedido</title>
    <p>Data e hora da última apertura do ficheiro.</p>
  </item>

  <item>
    <title>Modificado</title>
    <p>Data e hora da última vez que se cambiou e gardou o ficheiro.</p>
  </item>

  <item>
    <title>Creado</title>
    <p>A data e hora da creación do ficheiro.</p>
  </item>
 </terms>
</section>

</page>
