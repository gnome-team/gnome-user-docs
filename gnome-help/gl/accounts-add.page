<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-add" xml:lang="gl">

  <info>
    <link type="guide" xref="accounts" group="add"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="draft"/>
    <revision pkgversion="3.9.92" date="2012-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permitir ás aplicacións acceder ás súas contas enliña para fotos, contactos, calendarios e máis.</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Engadir unha conta</title>

  <p>Engadir unha conta axudaralle a ligar as súas contas en liña co seu escritorio. Polo tanto, o seu cliente de correo-e, calendario e outras aplicacións relacionados estarán configurados.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Contas en liña</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Contas en liñla</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione unha conta da lista.</p>
    </item>
    <item>
      <p>Seleccione o tipo de conta que quere engadir.</p>
    </item>
    <item>
      <p>A dialog will open where you can either enter your online account
      credentials or a button which will open your web browser to log into the
      account. For example, if you are setting up a Google account, click
      <gui>Sign in...</gui> and then enter your email address and password.</p>
    </item>
    <item>
      <p>Se escribiu as súas credenciais correctamente preguntaráselle para permitirlle a GNOME acceder ás súas contas en liña. Autorice o acceso para continuar.</p>
    </item>
    <item>
      <p>Todos os servizos ofrecidos por un fornecedor de contas estarán activos por omisión, <link xref="accounts-disable-service">Desactíveos</link> de forma individual.</p>
    </item>
  </steps>

  <p>Despois de engadir as contas, as aplicacións poden usalas para os servizos que permitiu. Vexa <link xref="accounts-disable-service"/> para obter máis información sobre como controlar o acceso aos servizos.</p>

  <note style="tip">
    <p>Algúns servizos en liña fornecen un token de autorización que GNOME almacena no lugar do seu contrasinal. Se eliminou unha conta terá que revocar o certificado no servizo en liña. Vexa <link xref="accounts-remove"/> para obter máis información.</p>
  </note>

</page>
