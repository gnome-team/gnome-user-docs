<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="gl">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Contribuíntes da wiki de documentación de Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Para poder seguir os seguintes pasos de solución de problemas, pode que precise detalles como o número de modelo do seu adaptador sen fíos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Resolución de problemas en redes sen fíos</title>
  <subtitle>Recoller información sobre o seu hardware de rede</subtitle>

  <p>Neste paso vaise recopilar información sobre o seu dispositivo de rede sen fíos. A forma de solucionar moitos problemas da rede sen fíos depende da marca e o número de modelo do seu adaptador sen fíos, polo que terá que tomar nota destes detalles. Tamén pode ser útil contar con algúns dos elementos que viñan co seu equipo, tales como os discos de instalación do controlador do dispositivo. Busque os seguintes elementos, se aínda os ten:</p>

  <list>
    <item>
      <p>A caixa e as instrucións dos seus dispositivos sen fíos (particularmente o manual de usuario do seu enrutador)</p>
    </item>
    <item>
      <p>Un disco de controladores - incluso se só contén controladores para Windows</p>
    </item>
    <item>
      <p>Fabricantes e os números de modelo do seu computador, adaptador sen fíos e enrutador. Xeralmente, esta información pódese atopar na parte inferior/traseira do dispositivo.</p>
    </item>
    <item>
      <p>Calquera número de versión que apareza impreso nos seus dispositivos de rede sen fíos ou nas súas caixas. Poden ser especialmente útiles, así que mire ben.</p>
    </item>
    <item>
      <p>Calquera cousa que teña no disco do controlador e que identifique o dispositivo en sí, a versión do «firmware» ou os compoñentes (chipset) que usa.</p>
    </item>
  </list>

  <p>Se é posíbel, tente ter acceso a unha conexión a internet distinta para que poida descargar software e controladores se fora necesario. (Conectar o seu computador directamente nun enrutador con un cable de rede Ethernet é unha forma de facer isto, pero conécteo só cando o precise.)</p>

  <p>Canto teña tantos detalles destes como lle fora posíbel, prema <gui>Seguinte</gui>.</p>

</page>
