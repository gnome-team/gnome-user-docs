<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="gl">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controlar que información se visualiza en columnas na vista de lista.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Preferencias das columnas na lista de ficheiros</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view..</p>
  <p>Click the view options button in the toolbar, pick <gui>Visible
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns.</p>


  <terms>
    <item>
      <title><gui>Nome</gui></title>
      <p>O nome dos cartafoles e ficheiros.</p>
      <note style="tip">
        <p>A columna <gui>Nome</gui> non se pode ocultar.</p>
      </note>
    </item>
    <item>
      <title><gui>Tamaño</gui></title>
      <p>O tamaño do cartafol fornécese como o número de elementos que conten o cartafol. O tamaño dun ficheiro fornécese en bytes, KB ou MB.</p>
    </item>
    <item>
      <title><gui>Tipo</gui></title>
      <p>Mostrado como cartafol, o tipo de ficheiro como documento PDF, imaxe JPEG, son MP3, etcétera.</p>
    </item>
    <item>
      <title><gui>Propietario</gui></title>
      <p>O nome do usuario propietario do cartafol ou ficheiro.</p>
    </item>
    <item>
      <title><gui>Grupo</gui></title>
      <p>O grupo ao que pertence o ficheiro. Cada usuario está normalmente no seu propio grupo, pero é posíbel ter varios usuarios nun grupo. Por exemplo, cada departamento pode ter o seu propio grupo nun ambiente de traballo.</p>
    </item>
    <item>
      <title><gui>Permiso</gui></title>
      <p>Mostra os permisos de acceso do ficheiro. P.ex. <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>O primeiro carácter é o tipo de ficheiro. <gui>-</gui> significa ficheiro normal e <gui>d</gui> significa directorio (cartafol). En casos raros poden mostrarse outros caracteres.</p>
        </item>
        <item>
          <p>Os seguintes tres caracteres <gui>rwx</gui> especifican os permisos para o usuario privativo do ficheiro.</p>
        </item>
        <item>
          <p>Os seguintes tres <gui>rw-</gui> especifican os permisos para todos os membros do grupo ao que pertence o ficheiro.</p>
        </item>
        <item>
          <p>Os últimos tres caracteres da columna <gui>r--</gui> especifican os permisos para todos os usuarios no sistema.</p>
        </item>
      </list>
      <p>Cada permiso ten os seguintes significados:</p>
      <list>
        <item>
          <p><gui>r</gui>: rexíbel, o que significa que pode abrir o ficheiro ou cartafol</p>
        </item>
        <item>
          <p><gui>w</gui>: escribíbel, o que significa que pode gardar cambios nel</p>
        </item>
        <item>
          <p><gui>x</gui>: executábel, o que significa que pode executalo se é un programa ou ficheiro de script, ou que pode acceder aos subcartafoles e ficheiros se é un cartafol</p>
        </item>
        <item>
          <p><gui>-</gui>: permiso non estabelecido</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Localización</gui></title>
      <p>A ruta á localización do ficheiro.</p>
    </item>
    <item>
      <title><gui>Modificado</gui></title>
      <p>Dálle a data da última vez que o ficheiro foi modificado.</p>
    </item>
    <item>
      <title><gui>Modificado — Hora</gui></title>
      <p>Dálle a data e hora da última vez que o ficheiro foi modificado.</p>
    </item>
    <item>
      <title><gui>Accedido</gui></title>
      <p>Gives the date or time of the last time the file was accessed.</p>
    </item>
    <item>
      <title><gui>Recency</gui></title>
      <p>Gives the date or time of the last time the file was accessed by the user.</p>
    </item>
    <item>
      <title><gui>Tipo de detalle</gui></title>
      <p>Mostra o tipo MIME do elemento.</p>
    </item>
    <item>
      <title><gui>Creado</gui></title>
      <p>Gives the date and time when the file was created.</p>
    </item>
  </terms>

</page>
