<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="gl">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambia a tableta entre modo tableta e modo ratón.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Estabelece o modo de seguimento da tableta Wacom</title>

<p>O <gui>Modo tableta</gui> determina como se asigna o estilete á pantalla.</p>

<steps>
  <item>
    <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Tableta Wacom</gui>.</p>
  </item>
  <item>
    <p>Prema na <gui>Táboa de Wacom</gui> para abrir o panel.</p>
    <note style="tip"><p>Se non se detecta ningunha tableta, pediráselle que <gui>Conecte ou acenda a súa tableta Wacom</gui>. Faga clic en <gui>Bluetooth</gui> na barra lateral para conectar unha tableta sen fíos.</p></note>
  </item>
  <item>
    <p>Escolla entre o modo tableta (ou absoluto) e o modo área táctil (ou relativo). Para o modo tableta, active o <gui>Modo tableta</gui>.</p>
  </item>
</steps>

  <note style="info"><p>No modo <em>absoluto</em> cada punto da tableta corresponde cun punto da pantalla. A esquina superior esquerda da pantalla, por exemplo, sempre corresponde co mesmo punto da tableta.</p>
  <p>No modo <em>relativo</em> se deixa o punteiro fóra da tableta, e o pon nun lugar diferente, o cursor da pantalla non se move. Así é como funciona o rato.</p>
  </note>

</page>
