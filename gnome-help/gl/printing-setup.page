<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="gl">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configurar unha impresora que está conectada ao seu computador, ou rede local.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Configurar unha impresora local</title>

  <p>O seu sistema pode recoñecer moitos tipos de impresoras automaticamente unha vez conectadas. A maioría das impresoras están conectadas cun cable USB que se conecta ao teu ordenador, pero algunhas impresoras conéctanse á túa rede con cable ou sen fíos.</p>

  <note style="tip">
    <p>Se a súa impresora está conectada á rede, non se configurará de maneira automática – pode engadila desde o panel de <gui>Impresoras</gui> nas <gui>Preferencias</gui>.</p>
  </note>

  <steps>
    <item>
      <p>Asegúrese que a impresora está acendida.</p>
    </item>
    <item>
      <p>Conecte a impresora ao seu sistema mediante un cabo axeitado. Pode ver a actividade na pantalla, cando o sistema busca os controladores, e pode que se lle pida que se autentique para instalalos.</p>
    </item>
    <item>
      <p>Aparecerá un mensaxe cando o sistema remate de instalar a impresora. Seleccione <gui>Imprimir unha páxina de proba</gui> para imprimir unha páxina de proba, ou <gui>Opcións</gui> para facer cambios adicionais na configuración da impresora.</p>
    </item>
  </steps>

  <p>Se a súa impresora non se configurou de maneira automática, pode engadila desde as preferencias da impresora:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Impresoras</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Impresoras</gui>.</p>
    </item>
    <item>
      <p>Dependendo do seu sistema, podería ter que premer <gui style="button">Desbloquear</gui> na esquina superior dereita e escribir o seu contrasinal cando se lle pregunte.</p>
    </item>
    <item>
      <p>Prema o botón <gui style="button">Engadir impresora…</gui>.</p>
    </item>
    <item>
      <p>Na xanela emerxente, seleccione a súa impresora nova e prema <gui style="button">Engadir</gui>.</p>
      <note style="tip">
        <p>Se a súa impresora non se descobre automaticamente, pero coñece o seu enderezo de rede, Escríbaa no campo de texto na parte inferior do diálogo e prema <gui style="button">Engadir</gui></p>
      </note>
    </item>
  </steps>

  <p>Se a súa impresora non aparece na xanela <gui>Engadir impresora</gui>, pode precisar instalar os controladores da impresora.</p>

  <p>Despois de instalar a impresora pode querer <link xref="printing-setup-default-printer">cambiar a súa impresora predeterminada</link>.</p>

</page>
