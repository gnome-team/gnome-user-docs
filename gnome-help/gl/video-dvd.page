<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="gl">

  <info>
    <link type="guide" xref="media#music"/>
  
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pode que non teña instalados os códecs axeitados, ou que o reprodutor sexa dunha rexión incorrecta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Por que non se reproducen os DVD?</title>

  <p>Se inserta un DVD no seu computador e non se reproduce, pode ser que non teña os <em>codecs</em> de DVD correctos instalados, ou o DVD sexa dunha <em>rexión</em> diferente.</p>

<section id="codecs">
  <title>Instalar os códecs correctos para a reprodución de DVD</title>

  <p>Para reproducir DVD debe ter instalados os <em>códecs</em> axeitados. Un códec é unha peza de software que permite a outros aplicativos ler formatos de vídeo e son. Se tenta reproducir un DVD e non ten instalados os códecs axeitados, ofreceráselle instalalos por vostede. Se isto non acontece, debe instalar os códecs de forma manual - solicite axuda sobre como facer isto usando os foros de asistencia da súa distribución de Linux ou mediante outra forma.</p>

  <p>Os DVD tamén están <em>protexidos contra copias</em> mediante un sistema chamado CSS. Isto impide que copie DVD, pero tamén lle impide reproducilos a menos que teña software adicional para xestionar a protección contra copia. Este software está dispoñíbel en varias distribucións de Linux, pero non se pode usar legalmente en todos os países. Póñase en contacto coa súa distribución Linux para obter máis información.</p>

  </section>

<section id="region">
  <title>Comprobando a rexión dun DVD</title>

  <p>Os DVD teñen un <em>código de rexión</em>, que lle di en que rexión do mundo está permitido reproducir o DVD. Se a rexión do seu reprodutor de DVD do seu computador non coincide coa rexión do DVD que tenta reproducir non poderá reproducir o DVD. Por exemplo, se ten un reprodutor de DVD da rexión 1 só poderá reproducir DVDs de América do Norte.</p>

  <p>É posíbel en ocasións configurar a rexión usada polo seu reprodutor de DVD, pero só pode facerse unhas cantas veces antes de que se bloquee nunha rexión de forma permanente. Para cambiar a rexión de DVD do seu reprodutor de DVD do seu computador use <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p>Pode atopar máis <link href="https://en.wikipedia.org/wiki/DVD_region_code">información sobre códigos de rexión de DVD en Wikipedia</link>.</p>

</section>

</page>
