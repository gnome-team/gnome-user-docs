<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="gl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Amplíe a súa pantalla para que sexa máis sinxelo ver as cousas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Ampliar o área da pantalla</title>

  <p>Magnificar a pantalla non é simplemente aumentar o <link xref="a11y-font-size">tamaño do texto</link>. Esta característica é como ter unha lupa que pode mover aumentando partes da pantalla.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Accesibilidade</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Accesibilidade</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione a categoría <gui>Ampliación</gui> para abrila.</p>
    </item>
    <item>
      <p>Active <gui>Ampliación de escritorio</gui>.</p>
    </item>
  </steps>

  <p>Pode moverse na área da pantalla. Movendo o rato polos bordos da pantalla, pode mover a área magnificada en diferentes direccións, permitíndolle ver a área da súa elección.</p>

  <note style="tip">
    <p>Pode activar a ampliación de forma rápida premendo na <link xref="a11y-icon">icona de accesibilidade</link> desde a barra superior e seleccionando <gui>Ampliación</gui>.</p>
  </note>

  <p>Pode cambiar o factor de magnificación, o seguimento do rato e a posición da vista magnificada na pantalla. Pode axustalos na sección <gui>Magnificador</gui>.</p>

  <p>Para activar o punto de mira para axudarlle a atopar o rato ou punteiro da área táctil. Pode activala, axustar a súa largura, cor e grosor desde a sección <gui>Punto de mira</gui>.</p>

  <p>Pode trocar o vídeo de cores invertidos, e axustar as opcións de brillo, contraste e escala de grises do magnificador na sección <gui>Filtros de cor</gui>. A combinación destas opcións é útil para as persoas con visión reducida, calquera grao de fotofobia ou simplemente para usar o seu computador baixo condicións adversas de iluminación.</p>

</page>
