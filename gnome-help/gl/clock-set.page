<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="gl">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prema nas <gui>Preferencias de data e hora</gui> para cambiar a data e hora.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

<title>Cambiar a hora e data</title>

  <p>Se a data e hora mostradas na barra superior son incorrectas, ou teñen o formato incorrecto, pode cambialas:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Prema en <gui>Data e hora</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Se activou <gui>Data e hora automática</gui>, a súa data e hora debería actualizarse automaticamente se conta con conexión a internet. Para actualizar a súa data e hora manualmente, desactíveo.</p>
    </item> 
    <item>
      <p>Prema <gui>Data e hora</gui>, para axustar a data e hora.</p>
    </item>
    <item>
      <p>Pode cambiar o formato da hora mostrada seleccionando os formatos <gui>24 horas</gui> ou <gui>AM/PM</gui> para o <gui>Formato da hora</gui>.</p>
    </item>
  </steps>

  <p>Tamén pode querer <link xref="clock-timezone">estabelecer o fuso horario manualmente</link>.</p>

</page>
