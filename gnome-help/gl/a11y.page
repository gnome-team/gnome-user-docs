<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="guide" style="a11y task" id="a11y" xml:lang="gl">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Use tecnoloxías de asistencia para axudarlle coas necesidades especiais de visión, escoita e mobilidade.</desc>
    <uix:thumb role="experimental-gnome-tiles" src="figures/tile-a11y.svg"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Accesibilidade</title>

  <p>O escritorio GNOME inclúe tecnoloxías de asistencia para axudarlle aos usuarios con diversas deficiencias e necesidades especiais e para interactuar con dispositivos de asistencia comúns. Pode engadir un menú de accesibilidade á barra superior, fornecendo un acceso rápido a moitas das características de accesibilidade.</p>

  <section id="vision">
    <title>Deficiencias visuais</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Cegueira</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Visión deficiente</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Deficiencias visuais de cor</title>
    </links>
    <links type="topic" style="linklist">
      <title>Outros puntos</title>
    </links>
  </section>

  <section id="sound">
    <title>Deficiencias auditivas</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Deficiencias motoras</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Movemento do rato</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Facendo clic e arrastrando</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Uso do teclado</title>
    </links>
    <links type="topic" style="linklist">
      <title>Outros puntos</title>
    </links>
  </section>
</page>
