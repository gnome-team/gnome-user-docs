<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-profile" xml:lang="gl">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rendemento e uso de batería balanceado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Escoller un perfil de enerxía</title>

  <p>Pode xestionar o uso de enerxía escollendo un <gui>Modo de enerxía</gui>. Hai tres perfís posíbeis:</p>
  
  <list>
    <item>
      <p><gui>Rendemento</gui>: alto rendemento e uso de enerxía. Este modo só estará visible se o seu ordenador o admite. Será seleccionábel se o seu ordenador funciona enchufado. Se o seu dispositivo admite a detección de colo, non poderá seleccionar esta opción se o dispositivo está funcionando no colo de alguén.</p>
    </item>
    <item>
      <p><gui>Balanceado</gui>: Rendemento e uso de enerxía estándar. Esta é a configuración por omisión.</p>
    </item>
    <item>
      <p><gui>Aforro de enerxía</gui>: redución de rendemento e consumo de enerxía. Esta configuración maximiza a duración da batería. Pode activar algunhas opcións de aforro de enerxía, como atenuar a pantalla de forma agresiva, e evitar que se apaguen.</p>
    </item>
  </list>

  <steps>
    <title>Para seleccionar un perfil de enerxía:</title>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Enerxía</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Enerxía</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Na sección <gui>Modo de enerxía</gui>, escolla un dos perfiles.</p>
    </item>
    
  </steps>

</page>
