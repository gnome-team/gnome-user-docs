<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="gl">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comprobe a cantidade de tinta ou tóner nos cartuchos de impresión.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Como podo comprobar os niveis d tinta ou tóner da miña impresora?</title>

  <p>Como comprobar canta tinta ou tóner queda na súa impresora depende do modelo e fabricante da súa impresora e o dos controladores e aplicacións instaladas no seu computador.</p>

  <p>Algunhas impresoras teñen unha pantalla empotrada para mostrar os niveis de tinta e outra información.</p>

  <p>Algunhas impresoras informan dos niveis de tóner ou tinta ao ordenador, que se poden atopar no panel <gui>Impresoras</gui> en <app>Preferencias</app>. O nivel de tinta mostrarase cos detalles da impresora se está dispoñíbel.</p>

  <p>Os controladores e ferramentas de estado para a maioría das impresoras de HP fornéceos o Proxecto Imaxe e Impresión en Linux de HP («HP Linux Imaging and Printing», HPLIP). Outros fabricantes poden fornecer controladores privativos con características similares.</p>

  <p>De forma alternativa, pode instalar unha aplicación para comprobar os niveles de tinta. <app>Inkblot</app> móstralle o estado da tinta para algunhas impresoras de HP, Epson e Canon. Comprobe se a súa impresora está na <link href="http://libinklevel.sourceforge.net./#supported">lista de modelos compatíbeis</link>. Outra aplicación de niveis de tinta para as impresoras Epson e algunhas máis é <app>mktink</app>.</p>

  <p>Algunhas impresoras aínda non son moi compatíbeis con Linux, e outras non están deseñadas para informar dos seus niveis de tinta.</p>

</page>
