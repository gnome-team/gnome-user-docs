<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="gl">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use contrasinais máis longas e complicadas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Escoller unha frase de paso segura</title>

  <note style="important">
    <p>Faga que os seus contrasinais sexan sinxelos dabondo como para poder lembralos, pero o suficientemente difíciles de adiviñar por outros (incluíndo programas de computador).</p>
  </note>

  <p>Se selecciona un bo contrasinal poderá manter o seu computador a salvo. Se a súa frase de paso é doada de adiviñar alguén pode descubrila e obter acceso á súa información persoal.</p>

  <p>As persoas poden incluso usar computadores para adiviñar o seu contrasinal de forma sistemática, polo que incluso un contrasinal difícil para os humanos pode ser extremadamente sinxelo de adiviñar para un programa de computador. Aquí ten algúns consellos para escoller unha boa frase de paso:</p>
  
  <list>
    <item>
      <p>Use unha mistura de letras en maiúsculas e minúsculas, números, símbolos e espazos no contrasinal. Iso faino moi difícil de adiviñar. Hai moitos símbolos onde escoller polo que á hora de tentar adiviñar un contrasinal vai ser moito máis difícil.</p>
      <note>
        <p>Un bo método de escolla dunha frase de paso é coller a primeira letra de cada palabra dunha frase que poida lembrar. A frase pode ser o nome dun filme, un libro, unha canción ou un álbum. Por exemplo, "Luar: Espectáculo das Cantareiras de Ardebullo" poderá ser L:EdCdA" ou ledcda ou l: edcda.</p>
      </note>
    </item>
    <item>
      <p>Asegúrese de que a súa frase de paso é longa dabondo. Cantos máis caracteres conteña, máis tempo lle levará a unha persoa ou computador a adiviñala.</p>
    </item>
    <item>
      <p>Non use unha palabra que apareza nun dicionario estándar de calquera lingua. Os crackers de contrasinais probarán isto primeiro. O contrasinal máis común é "password" (contrasinal en inglés) - as persoas poden adiviñar este contrasinal moi rápido!</p>
    </item>
    <item>
      <p>No use información persoal como a data, número de DNI, matrícula ou calquera dos nome dos membros da súa familia.</p>
    </item>
    <item>
      <p>Non use nomes.</p>
    </item>
    <item>
      <p>Seleccione un contrasinal que poida escribir rápido, para reducir os intentos de que alguén poida adiviñala se está mirando mentres vostede a escribe.</p>
      <note style="tip">
        <p>Nunca escriba as súas frases de paso nun papel. Pode ser moi sinxelo atopalas!</p>
      </note>
    </item>
    <item>
      <p>Use distintas frases de paso para distintos asuntos.</p>
    </item>
    <item>
      <p>Use distintas frases de paso para distintas contas.</p>
      <p>Se usa a mesma frase de paso para todas as súas contas, calquera que adiviñe a súa frase de paso terá acceso a tódalas súas contas inmediatamente.</p>
      <p>Porén pode ser difícil lembrar moitos contrasinais á vez. Aínda que é máis seguro usar diferentes contrasinais para todo, é máis sinxelo usar o mesma para cousas que non son importantes (como sitios web), e outros diferentes para as cousas con máis importancia (como a súa conta de banca en liña ou o seu correo electrónico).</p>
   </item>
   <item>
     <p>Cambie os seus contrasinais de forma regular.</p>
   </item>
  </list>

</page>
