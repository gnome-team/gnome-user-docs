<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="gl">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Comprobe que o son non está silenciado, que os cables están conectados correctamente e que se detecta a tarxeta de son.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

<title>Non podo escoitar ningún son do computador</title>

  <p>Se non pode escoitar ningún son no seu computador, por exemplo cando reproduce música, tente estes pasos de resolución de erros para ver se pode arranxar o problema.</p>

<section id="mute">
  <title>Asegúrese que o son non está desactivado</title>

  <p>Abra o <gui xref="shell-introduction#systemmenu">menú do sistema</gui> e asegúrate de que o son non estea silenciado nin desactivado.</p>

  <p>Algúns portátiles teñen interruptores ou teclas de silencio nos seus teclados. Proba a premer a tecla para ver se silencia o son.</p>

  <p>Tamén pode comprobar que non silenciou a aplicación que está usando para reproducir o son (por exemplo, o seu reprodutor de música ou reprodutor de filmes). A aplicación pode ter un botón de silencio ou de volume na súa xanela principal, polo que debe comprobalo. Tamén pode premer na icona no panel superior e seleccionar <gui>Preferencias de son</gui>. Cando apareza a xanela de <gui>Son</gui>, vaia á lapela <gui>Aplicacións</gui> e comprobe que a súa aplicación non está silenciado.</p>

  <p>Ademais, pode comprobar o control deslízante de volume no panel <gui>Son</gui>:</p>
  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Son</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Son</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>En <gui>Niveis de volume</gui>, verifique que a súa aplicación non estea silenciada. O botón ao final do control deslizante de volume activa e desactiva <gui>Silencio</gui>.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>Comprobe que os altoparlantes están acendidos e están conectados correctamente</title>
  <p>Se o seu ordenador ten altofalantes externos, asegúrese de que estean acendidos e de que o volume está subido. Asegúrate de que o cable do altofalante estea conectado de forma segura á toma de son de "saída" do seu ordenador. Este enchufe adoita ser de cor verde claro.</p>

  <p>Algunhas tarxetas de son son capaces de cambiar o zócalo que usan para a saída (dos altofalantes) e o zócalo de entrada (desde o micrófono, por exemplo). O zócalo de saída pode ser diferente cando se executa Linux no lugar de Windows ou Mac OS. Tente conectar o cable do altofalante aos diferentes zócalos de son no equipo.</p>

 <p>Unha última cousa que pode comprobar é que o cable de son estea conectado correctamente na parte traseira dos altofalantes. Algúns altofalantes teñen máis dunha entrada.</p>
</section>

<section id="device">
  <title>Comprobe que o dispositivo de son correcto está seleccionado</title>

  <p>Algúns equipos teñen varios «dispositivos de son» instalados. Algúns destes son capaces de facer saír o son e outros non, así que vostede debe comprobar que ten seleccionado o correcto. Isto podería implicar algún ensaio e erro para elixir o correcto.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Son</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Son</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>En <gui>Saída</gui>, seleccione un <gui>Dispositivo de saída</gui> e prema no botón <gui>Probar</gui> para ver se funciona.</p>

      <p>Pode precisar probar cada un dos dispositivos dispoñíbeis.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>Comprobe que a tarxeta de son foi detectada correctamente</title>

  <p>Se non se detecta a súa tarxeta de son, pode que os controladores da tarxeta non estean instalados. Pode que teña que instalalos manualmente. A forma de facelo dependerá da tarxeta que teña.</p>

  <p>Execute a orde <cmd>lspci</cmd> no Terminal para obter máis información sobre a súa tarxeta de son:</p>
  <steps>
    <item>
      <p>Vaia á vista previa de <gui>Actividades</gui> e abra un terminal.</p>
    </item>
    <item>
      <p>Execute <cmd>lspci</cmd> con <link xref="user-admin-explain">permisos de administrador</link>; pode escribir <cmd>sudo lspi</cmd> e escribir o contrasinal, ou escriba <em>su</em>, escriba o contrasinal de<em>root</em> (administración) e logo escriba <cmd>lspci</cmd>.</p>
    </item>
    <item>
      <p>Comprobe se aparece un <em>controlador de audio</em> ou un <em>dispositivo de audio</em>: nese caso debería ver a marca e o número de modelo da tarxeta de son. Ademais, <cmd>lspci -v</cmd> mostra unha lista con información máis detallada.</p>
    </item>
  </steps>

  <p>Pode que encontre e saiba instalar os controladores para a súa tarxeta. O mellor é pedir instrucións nos foros de soporte (ou similares) para a súa distribución de Linux.</p>

  <p>Se non pode conseguir os controladores para a tarxeta de son, é posíbel que prefira comprar unha tarxeta de son nova. Pode obter tarxetas de son que se poden instalar no equipo e tarxetas de son externas USB.</p>

</section>

</page>
