<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="gl">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Engadir novos usuarios para que outras persoas poidan iniciar sesión no seu computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Engadir unha nova conta de usuario</title>

  <p>Pode engadir varias contas de usuario ao seu equipo. Dar unha conta a cada persoa no seu fogar ou empresa. Cada usuario ten o seu propio cartafol de inicio, documentos e configuración.</p>

  <p>Precisa de <link xref="user-admin-change">privilexios de administración</link> para engadir contas de usuario.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Seleccione <gui>Usuarios</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Prema <gui style="button">Desbloquear</gui> na esquina superior dereita e escriba o seu contrasinal cando se lle pregunte.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add User...</gui> button under
      <gui>Other Users</gui> to add a new user account.</p>
    </item>
    <item>
      <p>Se quere engadir un novo usuario con <link xref="user-admin-explain">acceso administrativo</link> ao computador, seleccione <gui>Administrador</gui> no tipo de conta.</p>
      <p>Os administradores poden facer cousas como engadir e eliminar usuarios, instar software e controladores, e cambiar a data e a hora.</p>
    </item>
    <item>
      <p>Escriba o nome completo do novo usuario. O nome de usuario encherase automaticamente en función do nome completo. O valor predeterminado probabelmente sexa correcto, pero pode cambialo se o desexa.</p>
    </item>
    <item>
      <p>Se quere estabelecer un contrasinal para o novo usuario, ou deixarlle escollelo a el mesmo no primeiro inicio de sesión. Se quere estabelecer o contrasinal agora, pode premer a icona <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">xerar un contrasinal</span></media></gui> para xerar un contrasinal aleatorio automaticamente.</p>
      <p>Para conectar o usuario a un dominio de rede, faga clic en <gui>Inicio de sesión empresarial</gui>.</p>
    </item>
    <item>
      <p>Faga clic en <gui>Engadir</gui>. Cando se engade o usuario, pódense axustar os axustes de <gui>Controis parentais</gui> e <gui>Idioma</gui>.</p>
    </item>
  </steps>

  <p>Se quere cambiar o contrasinal despoisd e crear unha conta, seleccione a conta , <gui style="button">Desbloquee</gui> o panel e prema o estado do contrasinal actual.</p>

  <note>
    <p>No panel <gui>Usuarios</gui> pode premer na imaxe ao carón do nome de usuario para estabelecer unha imaxe para a conta. Esta imaxe mostrarase na xanela de inicio de sesión. O sistema fornece algunhas imaxes que pode usar, pode seleccionar a súa propia ou sacar unha foto empregando a súa cámara web.</p>
  </note>

</page>
