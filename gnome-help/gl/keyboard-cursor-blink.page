<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-cursor-blink" xml:lang="gl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Facer que o punto de inserción parpadee e controlar a velocidade do parpadeo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Facer que parpadee o cursor do teclado</title>

  <p>Se ten dificultades para ver o cursor do teclado nun campo de texto pode facer que pestanexe sendo máis doado localizalo.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Accesibilidade</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Accesibilidade</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione a sección <gui>Escritura</gui> para abrila.</p>
    </item>
    <item>
      <p>Na sección <gui>Asistencia de escrita</gui>, active <gui>Pestanexo do cursor</gui>.</p>
    </item>
    <item>
      <p>Use o deslizador <gui>Velocidade de pestanexo</gui> para axustar a rapidez do pestanexo do cursor.</p>
    </item>
  </steps>

</page>
