<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="gl">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Activar Teclas do rato para premer e mover o punteiro do rato co teclado numérico.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Faga clic e mova o punteiro do rato usando o teclado numérico</title>

  <p>Se ten dificultades ao usar un rato ou outro dispositivo apuntador, pode controlar o punteiro do rato usando o teclado numérico do seu teclado. Esta característica chámase <em>teclas de rato</em>.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Accesibilidade</gui>.</p>
      <p>Podes acceder á vista xeral de <gui>Actividades</gui> premendo sobre ela, movendo o punteiro do rato contra a esquina superior esquerda da pantalla, empregando <keyseq><key>Ctrl</key> <key>Alt</key><key>Tab</key></keyseq> seguido de <key>Intro</key>, ou empregando a tecla <key xref="teclado-clave-super">Super</key>.</p>
    </item>
    <item>
      <p>Prema en <gui>Accesibilidade</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione a sección <gui>Apuntar e premer</gui> para abrila.</p>
    </item>
    <item>
      <p>Active <gui>Teclas do rato</gui>.</p>
    </item>
    <item>
      <p>Comprobe que a tecla <key>Bloq Num</key> está desactivada. Agora poderá mover o punteiro do rato usando o teclado numérico.</p>
    </item>
  </steps>

  <p>O teclado numérico é un conxunto de botóns numéricos do seu teclado, normalmente dispostos nunha matriz cadrada. Se o seu teclado non ten teclado numérico (por exemplo, o teclado dun portátil), pode que teña que manter premida a tecla de Función (<key>Fn</key>) e usar outras teclas do seu teclado como un teclado numérico. Se usa esta característica a miúdo nun portátil, pode comprar teclados numéricos USB externos.</p>

  <p>Cada número do teclado numérico correspóndese cun enderezo. Por exemplo, ao premer a tecla <key>8</key> moverase ao punteiro cara arriba e ao premer <key>2</key> moverase cara abaixo. Ao premer a tecla <key>5</key> farase unha pulsación co rato e ao premer dúas veces rapidamente farase unha dupla pulsación.</p>

  <p>A maioría dos teclado teñen unha tecla especial que permite facer unha pulsación dereita; algunhas veces chamada a tecla <key xref="keyboard-key-menu">Menú</key>. Porén teña en conta que esta tecla responde onde está o foco do teclado, non onde está o punteiro do rato. Consulte a <link xref="a11y-right-click"/> para obter máis información sobre como facer unha pulsación dereita mantendo premida a tecla <key>5</key> ou co botón esquerdo do rato.</p>

  <p>Se quere usar o teclado numérico para teclear números cando está activada a opción de teclas do rato, active <key>Bloq Num</key>. O rato non se pode controlar co teclado numérico mentres estea activado <key>Bloq Num</key>.</p>

  <note>
    <p>As teclas numéricas normais, situadas nunha fila na parte superior do teclado, non controlan o punteiro do rato. Só poden facelo as teclas do teclado numérico.</p>
  </note>

</page>
