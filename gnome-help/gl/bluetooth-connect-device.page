<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="bluetooth-connect-device" xml:lang="gl">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="seealso" xref="sharing-bluetooth"/>
    <link type="seealso" xref="bluetooth-remove-connection"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Emparellar dispositivos Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Conectar o seu computador a outro dispositivo Bluetooth</title>

  <p>Antes de poder usar un dispositivo Bluetooth como un rato ou uns auriculares, primeiro terá que conectalos ao equipo. A isto denomínaselle <em>emparellar</em> dispositivos Bluetooth.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Bluetooth</gui> no lateral dereito.</p>
    </item>
    <item>
      <p>Asegúrese que Bluetooth está activado: o trocador da parte superior debería estar activo. Co panel aberto e o trocador activado, o seu computador comezará a buscar dispositivos.</p>
    </item>
    <item>
      <p>Faga que o outro dispositivo Bluetooth <link xref="bluetooth-visibility">se poida descubrir ou que sexa visíbel</link> e colóqueo a menos de 5-10 metros (menos de 16-33 pes) de seu computador.</p>
    </item>
    <item>
      <p>Prema no dispositivo desde a lista de <gui>Dispositivos</gui>. O panel para o dispositivo abrirase.</p>
    </item>
    <item>
      <p>Se é necesario, confirme o PIN no outro dispositivo. Agora, o dispositivo debería mostrarlle o PIN que ve na pantalla do seu equipo Confirme o PIN no dispositivo (pode ter que premer <gui>Emparellar</gui> ou <gui>Confirmar</gui>), e logo <gui>Confirmar</gui> no seu computador.</p>
      <p>Na maioría dos dispositivos, deberá completar a entrada en menos de 20 segundos, ou a conexión non se completará. Se parasa isto, volva á lista de dispositivos e comece de novo.</p>
    </item>
    <item>
      <p>A entrada para o dispositivo na lista de <gui>Dispositivos</gui> mostrará o estado <gui>Conectado</gui>.</p>
    </item>
    <item>
      <p>Para editar o dispotivo, prema sobre el desde a lista de <gui>Dispositivos</gui>. Verá un panel específico para o dispositivo. Podería mostrar opcións adicionais aplicábeis ao tipo de dispositivo ao que está conectado.</p>
    </item>
    <item>
      <p>Peche o panel en canto cambie as configuracións.</p>
    </item>
  </steps>

  <media its:translate="no" type="image" src="figures/bluetooth-symbolic.svg" style="floatend">
    <p its:translate="yes">A icona de Bluetooth na barra superior</p>
  </media>
  <p>Cando un ou varios dispositivos Bluetooth están conectados, a icona de Bluetooth aparecerá no área de estado do sistema.</p>

</page>
