<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="gl">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Calibrar a súa cámara é importante para capturar cores precisos.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

  <title>Como calibrar a cámara?</title>

  <p>Os dispositivos de cámara son calibrados tomando unha fotografía dun obxectivo nas condicións de iluminación desexadas. Ao converter o ficheiro RAW nun ficheiro TIFF, pódese usar para calibrar o dispositivo da cámara no panel <gui>Cor</gui>.</p>
  <p>Debe recortar o ficheiro TIFF para que só se vexa o obxectivo. Asegúrese de que os bordos brancos e negros aínda se ven. O calibrado non funcionará se a imaxe está do revés ou moi distorsionada.</p>

  <note style="tip">
    <p>O perfil resultante só é válido baixo as condicións de lus nas que se obtivo a imaxe orixinal. Isto significa que pode precisar perfilar varias veces para condicións de lus de <em>estudio</em>, <em>lus solar brillante</em> e <em>nubrado</em>.</p>
  </note>

</page>
