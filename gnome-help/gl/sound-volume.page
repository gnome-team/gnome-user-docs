<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="gl">

  <info>
    <link type="guide" xref="media#sound" group="#first"/>

    <revision version="gnome:46" status="final" date="2024-04-06"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Axustar o volume do son para o equipo e controlar o volume de cada aplicación.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2024</mal:years>
    </mal:credit>
  </info>

<title>Cambiar o volume do sistema</title>

  <p>Para cambiar o volume de son, prema o <gui xref="shell-introduction#systemmenu">menú do sistema</gui>desde a parte dereita da barra superior e mova o deslizador de volume á dereita ou á esquerda. Pode desactivar completamente o son levando o deslizador ao extremo esquerdo.</p>
 
  <note style="tip">
    <p>Se pasa o rato sobre a icona de volume na barra superior ou o control deslízante no menú do sistema, o volume pódese controlar desprazando a roda do rato ou o panel táctil.</p>
  </note>

  <p>Algúns teclados teñen teclas que lle permiten controlar o volume. Normalmente representan altofalantes estilizados emitindo «ondas». Están frecuentemente preto das teclas «F» na parte superior. Nos portátiles, normalmente están nas teclas «F». Manteña a tecla <key>Fn</key> no seu teclado para usalas.</p>

  <p>Se ten altofalantes externos, tamén pode cambiar o volume usando o control de volume dos altofalantes. Algúns auriculares tamén teñen un control de volume.</p>

<section id="apps">
 <title>Cambiar o volume de son para aplicacións individuais</title>

  <p>Podes cambiar o volume dunha aplicación e deixar sen cambios o volume doutras. Isto é útil se estás escoitando música e navegando pola web, por exemplo. Quizais queiras desactivar o son no navegador web para que os sons dos sitios web non interrompan a música.</p>

  <p>Algunhas aplicacións teñen controis de volume nas súas xanelas principais. Se a súa aplicación teno, emprégueo para cambiar o volume. Se non:</p>

    <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Son</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Sounds</gui> section, click <gui>Volume Levels</gui>.</p>
    </item>
    <item>
      <p>Use the volume slider for each application to set its volume. Click the
      speaker button next to the slider to mute or unmute the application.</p>

  <note style="tip">
    <p>Só se listan as aplicacións que reproducen sons. Se unha aplicación reproduce sons pero non aparece na lista, é posible que non admita a función que lle permite controlar o seu volume deste xeito. Neste caso, non pode cambiar o seu volume.</p>
  </note>
    </item>

  </steps>

</section>

</page>
