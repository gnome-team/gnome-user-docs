<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="lv">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>
    <revision version="gnome:45" date="2024-03-03" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pievienot, dzēst un pārdēvēt grāmatzīmes datņu pārvaldniekā.</desc>

  </info>

  <title>Rediģēt mapju grāmatzīmes</title>

  <p>Jūsu grāmatzīmju saraksts ir redzams datņu pārvaldnieka sānjoslā.</p>

  <steps>
    <title>Lai pievienotu grāmatzīmi:</title>
    <item>
      <p>Atveriet mapi vai vietu, ko vēlaties pievienot grāmatzīmēm.</p>
    </item>
    <item>
      <p>Click the current folder in the path bar and then select
      <gui style="menuitem">Add to Bookmarks</gui>.</p>
      <note>
        <p>You can also drag a folder to the sidebar, and drop it
        over <gui>New bookmark</gui>, which appears dynamically.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Lai dzēstu grāmatzīmi:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Remove from Bookmarks</gui> from the menu.</p>
    </item>
  </steps>

  <steps>
    <title>Lai pārdēvētu grāmatzīmi:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Rename</gui>.</p>
    </item>
    <item>
      <p>Laukā <gui>Nosaukums</gui> ierakstiet grāmatzīmes jauno nosaukumu.</p>
      <note>
        <p>Grāmatzīmes pārdēvēšana nemaina mapes nosaukumu. Ja jums ir grāmatzīmes uz divām mapēm ar vienādiem nosaukumiem, kas atrodas dažādās vietās, šīs grāmatzīmes būs grūti atšķirt. Tādos gadījumos ir lietderīgi dot grāmatzīmei nosaukumu, kas nesakrīt ar atbilstošās mapes nosaukumu.</p>
      </note>
    </item>
  </steps>

</page>
