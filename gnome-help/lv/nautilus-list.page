<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="lv">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kontrolējiet, kāda informācija ir redzama saraksta skata kolonnā.</desc>
  </info>

  <title>Datņu saraksta kolonnu iestatījumi</title>

  <p>There are several columns of information that you can display in the
  <gui>Files</gui> list view..</p>
  <p>Click the view options button in the toolbar, pick <gui>Visible
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns.</p>


  <terms>
    <item>
      <title><gui>Nosaukums</gui></title>
      <p>Mapju un datņu nosaukumi.</p>
      <note style="tip">
        <p><gui>Nosaukums</gui> kolonna nevar tikt paslēpta.</p>
      </note>
    </item>
    <item>
      <title><gui>Izmērs</gui></title>
      <p>Mapes izmērs ir dots kā vienumu skaits mapē. Datņu izmērs ir dots baitos, KB vai MB.</p>
    </item>
    <item>
      <title><gui>Tips</gui></title>
      <p>Attēlots kā mape vai datnes veids, piemēram, PDF dokuments, JPEG attēls vai MP3 audio datne u.t.t.</p>
    </item>
    <item>
      <title><gui>Īpašnieks</gui></title>
      <p>Lietotāja vārds, kuram pieder mape vai datne.</p>
    </item>
    <item>
      <title><gui>Grupa</gui></title>
      <p>Grupa, kurai pieder datne. Parasti katram lietotājam ir arī sava grupa, bet ir iespējams, ka vairākiem lietotājiem ir viena grupa. Piemēram, uzņēmumā nodaļai var būt sava grupa darba vidē.</p>
    </item>
    <item>
      <title><gui>Atļaujas</gui></title>
      <p>Attēlo datnes piekļuves atļaujas. Piemēram, <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>Pirmā rakstzīme ir datnes veids. <gui>-</gui> nozīmē parastu datni un <gui>d</gui> nozīmē direktoriju (mapi). Retos gadījumos var parādīties citas rakstzīmes.</p>
        </item>
        <item>
          <p>Nākamās trīs rakstzīmes <gui>rwx</gui> norāda atļaujas lietotājam, kam pieder datne.</p>
        </item>
        <item>
          <p>Nākamās trīs rakstzīmes<gui>rw-</gui> norāda atļaujas visiem grupas locekļiem, kuriem pieder datne.</p>
        </item>
        <item>
          <p>Pēdējās trīs rakstzīmes kolonnā <gui>r--</gui> norāda atļaujas visiem sistēmas lietotājiem.</p>
        </item>
      </list>
      <p>Katrai atļaujai ir sekojošas nozīmes:</p>
      <list>
        <item>
          <p><gui>r</gui>: lasāms, nozīmē, ka jūs varat atvērt datni vai mapi</p>
        </item>
        <item>
          <p><gui>w</gui>: rakstāms, nozīmē, ka jūs varat saglabāt izmaiņas tajā</p>
        </item>
        <item>
          <p><gui>x</gui>: izpildāms, nozīmē, ka jūs varat palaist to, ja tas ir programma vai skripta datne, vai jūs varat piekļūt apakšmapēm vai datnēm, ja tas ir mape</p>
        </item>
        <item>
          <p><gui>-</gui>: atļauja nav iestatīta</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Atrašanās vieta</gui></title>
      <p>Ceļš līdz datnes atrašanās vietai.</p>
    </item>
    <item>
      <title><gui>Mainīts</gui></title>
      <p>Rāda datumu, kad datne pēdējo reizi ir rediģēta.</p>
    </item>
    <item>
      <title><gui>Mainīts — laiks</gui></title>
      <p>Rāda datumu un laiku, kad datne pēdējo reizi ir rediģēta.</p>
    </item>
    <item>
      <title><gui>Izmantots</gui></title>
      <p>Gives the date or time of the last time the file was accessed.</p>
    </item>
    <item>
      <title><gui>Recency</gui></title>
      <p>Gives the date or time of the last time the file was accessed by the user.</p>
    </item>
    <item>
      <title><gui>Detailed Type</gui></title>
      <p>Attēlo vienuma MIME tipu.</p>
    </item>
    <item>
      <title><gui>Created</gui></title>
      <p>Gives the date and time when the file was created.</p>
    </item>
  </terms>

</page>
