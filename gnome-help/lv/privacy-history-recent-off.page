<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="lv">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pārtraukt vai ierobežot nesen izmantoto datņu sekošanai.</desc>
  </info>

  <title>Izslēgt vai ierobežot datņu vēstures izsekošanu</title>
  
  <p>Nesen lietoto datņu izsekošana var atvieglot to dokumentu, pie kuriem nesen strādājāt, meklēšanu datņu pārvaldniekā un datņu dialoglodziņos lietotnēs. Iespējams, vēlaties paturēt datņu lietošanas vēsturi privātu, vai tikai sekot ļoti nesenai vēsturei.</p>

  <steps>
    <title>Izslēgt datņu vēstures izsekošanu</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy &amp; Security</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Privacy &amp; Security</gui></guiseq> from the
      results. This will open the <gui>Privacy &amp; Security</gui> panel.</p>
    </item>
    <item>
      <p>Click on <gui>File History &amp; Trash</gui> to open the panel.</p>
    </item>
    <item>
     <p>Switch the <gui>File History</gui> switch to off.</p>
     <p>To re-enable this feature, switch the <gui>File History</gui> switch
     to on.</p>
    </item>
    <item>
      <p>Use the <gui>Clear History…</gui> button to purge the history
      immediately.</p>
    </item>
  </steps>
  
  <note><p>Šis iestatījums neietekmēs to, kā tīmekļa pārlūks saglabā informāciju par apmeklētajām vietnēm.</p></note>

  <steps>
    <title>Ierobežot laiku, kādā tiek izsekotas datnes:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>File History &amp; Trash</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>File History &amp; Trash</gui> to open the panel.</p>
    </item>
    <item>
     <p>Ensure the <gui>File History</gui> switch is set to on.</p>
    </item>
    <item>
     <p>Under <gui>File History Duration</gui>, select how long to retain your file history.
     Choose from options <gui>1 day</gui>, <gui>7 days</gui>, <gui>30 days</gui>, or
     <gui>Forever</gui>.</p>
    </item>
    <item>
      <p>Use the <gui>Clear History…</gui> button to purge the history
      immediately.</p>
    </item>
  </steps>

</page>
