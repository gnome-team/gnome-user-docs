<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="lv">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Iestatīt papildu monitoru.</desc>
  </info>

<title>Pievienot citu monitoru datoram</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Iestatīt papildu monitoru</title>
  <p>Lai iestatītu papildu monitoru savam datoram, savienojiet monitoru ar savu klēpjdatoru. Ja jūsu sistēma to uzreiz neatpazīst, jūs varētu vēlēties mainīt iestatījumus:</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Displeji</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the display arrangement diagram, drag your displays to the relative
      positions you want.</p>
      <note style="tip">
        <p>The numbers on the diagram are shown at the top-left of each
        display when the <gui>Displays</gui> panel is active.</p>
      </note>
    </item>
    <item>
      <p>Spiediet <gui>Primārais displejs</gui>, lai izvēlētos savu primāro displeju.</p>

      <note>
        <p>Primārais displejs ir tas, kuram ir <link xref="shell-introduction">augšējā josla</link>, un kur ir redzams<gui>Aktivitāšu</gui> pārskats.</p>
      </note>
    </item>
    <item>
      <p>Click on each monitor in the list and select the orientation,
      resolution or scale, and refresh rate.</p>
    </item>
    <item>
      <p>Spiediet <gui>Pielietot</gui>. Jaunie iestatījumi tiks pielietoti uz 20 sekundēm. Ja neko neredzēsiet, iepriekšējie iestatījumi tiks automātiski atjaunoti. Taču, ja jūs apmierina jaunie iestatījumi, nospiediet <gui>Paturēt izmaiņas</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Displeja režīmi</title>
    <p>Ar diviem ekrāniem ir pieejami šie displeju režīmi:</p>
    <list>
      <item><p><gui>Savienot displejus:</gui> ekrānu malas ir savienotas, lai lietas varētu pārvietot no viena ekrāna otrā.</p></item>
      <item><p><gui>Dublēt:</gui> tas pats saturs tiek parādīts abos displejos ar vienādu izšķirtspēju un orientāciju.</p></item>
    </list>
    <p>When only one display is needed, for example, an external monitor
    connected to a docked laptop with the lid closed, the other monitor can be
    switched off. In the list, click on the monitor you want to disable, and
    turn the switch to off.</p>
      
</section>

<section id="multiple">

  <title>Pievienot vairāk kā vienu monitoru</title>
    <p>Ja ir vairāk kā divi ekrāni, vienīgais pieejamais režīms ir <gui>Savienot displejus</gui>.</p>
    <list>
      <item>
        <p>Press the button for the display that you would like to
        configure.</p>
      </item>
      <item>
        <p>Velciet ekrānus uz vēlamo relatīvo pozīciju.</p>
      </item>
    </list>

</section>
</page>
