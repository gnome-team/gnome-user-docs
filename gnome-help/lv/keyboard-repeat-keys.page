<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="lv">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Liec tastatūrai neatkārtot burtus, kad piespied un turi taustiņu, vai maniet atkārtošanās aizkavi un ātrumu.</desc>
  </info>

  <title>Pārvaldīt atkārtotos tautiņu spiedienus</title>

  <p>Pēc noklusējuma, kad tu turi taustiņu piespiestu, simbols tiks atkārtots tik ilgi, kamēr tu šo taustiņu atlaidīsi. Ja tev ir grūtības noņemt pirkstu no taustiņa pietiekami ātri, tu vari atslēgt šo iespēju vai pamainīt laiku, cik ilgam jāpaiet, kamēr taustiņi sāk atkārtoties, vai cik ātri taustiņu piespiešana atkārtojas.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Piekļūstamība</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Piekļūstamība</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties cilni <gui>Rakstīšana</gui>.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the <gui>Repeat Keys</gui> switch to off.</p>
      <p>Alternatīvi pielāgojiet <gui>Aizture</gui> slīdni, lai kontrolētu cik ilgi jūs vēlaties, lai būtu jātur nospiests taustiņš pirms to sākt atkārtot, un pielāgojiet <gui>Ātrums</gui> slīdni, lai kontrolētu cik ātri taustiņa spiedieni tiek atkārtoti.</p>
    </item>
  </steps>

</page>
