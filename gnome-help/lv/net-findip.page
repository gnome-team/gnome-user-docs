<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="lv">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jūsu IP adreses zināšana var palīdzēt jums novērst tīkla problēmas.</desc>
  </info>

  <title>Atrodiet savu IP adresi.</title>

  <p>Jūsu IP adreses zināšana var palīdzēt jums novērst interneta savienojuma problēmas.</p>
  
  <section id="wired">
    <title>Find your wired connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> in the sidebar to open the panel.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">
        If more than one type of wired connected is available, you might see
        names like <gui>PCI Ethernet</gui> or <gui>USB Ethernet</gui> instead
        of <gui>Wired</gui>.</p>
      </note>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Find your wireless connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Atrodiet savu ārējo (interneta) IP adresi.</title>
  <steps>
    <item>
      <p>Visit
      <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Lapa jums parādīs jūsu ārējo IP adresi.</p>
    </item>
  </steps>
  <p>Depending on how your computer connects to the internet, the internal and
  external addresses may be the same.</p>  
  </section>

</page>
