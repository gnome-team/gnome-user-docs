<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="lv">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izvēlēties reģionu, kuram pieskaņot datumu un laiku, skaitļus, valūtu un mērvienības.</desc>
  </info>

  <title>Mainīt datuma un mērvienību formātus</title>

  <p>Jūs varat noteikt, kādus formātus lietot datumiem, laikiem, skaitļiem, valūtai un mērvienībām, pielāgojot tos vietējām tradīcijām.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Your Account</gui> section, click <gui>Formats</gui>.</p>
    </item>
    <item>
      <p>Under <gui>Common Formats</gui>, select the region and language that most
      closely matches the formats you would like to use.</p>
    </item>
    <item>
      <p>Spiediet <gui style="button">Pabeigt</gui>, lai saglabātu.</p>
    </item>
    <item>
      <p>Your session needs to be restarted for changes to take effect. Either click
      <gui style="button">Restart…</gui>, or manually log back in later.</p>
    </item>
  </steps>

  <p>Kad izvēlēts reģions, pa labi no saraksta redzami datuma un citu formātu piemēri. Lai gan šajos piemēros tas nav redzams, reģions nosaka arī dienu, ar kuru sākas nedēļa kalendāros.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    section for the <gui>Login Screen</gui> in the
    <gui>Region &amp; Language</gui> panel.</p>
  </note>

</page>
