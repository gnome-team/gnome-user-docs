<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="lv">

  <info>
    <link type="guide" xref="media#music"/>

    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pārbaudiet, vai tiem ir instalēti pareizie video kodeki.</desc>
  </info>

  <title>Citi cilvēki nevar atskaņot video, kurus es izveidoju</title>

  <p>Ja esat izveidojis video jūsu Linux datorā un aizsūtījis to kādam, kas izmanto Windows vai Mac OS, jūs varētu konstatēt, ka viņiem ir problēmas ar jūsu video atskaņošanu.</p>

  <p>Lai varētu atskaņot jūsu video, tad personai, kurai jūs to nosūtījāt, vajag būt ieinstalētiem arī pareizajiem <em>kodekiem</em>. Kodeki ir mazs programmatūras gabals, kas zina, kā uzņemt video un parādīt to uz ekrāna. Ir daudz dažādu video formātu un katram ir nepieciešami savi kodeki, lai to atskaņotu. Jūs varat pārbaudīt jūsu video formātu, veicot sekojošas darbības:</p>

  <list>
    <item>
      <p>Atveriet <app>Datnes</app> no <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskata.</p>
    </item>
    <item>
      <p>Labā poga uz video datnes un izvēlieties <gui>Īpašības</gui>.</p>
    </item>
    <item>
      <p>Ejiet uz <gui>Audio/Video</gui> vai <gui>Video</gui> cilni un apskatiet, kādi <gui>kodeki</gui> ir sarakstā zem <gui>Video</gui> un <gui>Audio</gui> (ja video ir arī audio).</p>
    </item>
  </list>

  <p>Pajautājiet personai, kurai ir problēmas ar atskaņošanu, vai viņam ir ieinstalēti pareizie kodeki. Noderīgi varētu būt meklēt internetā kodeku nosaukumu un video atskaņošanas programmas nosaukumu. Piemēram, ja jūsu video izmanto <em>Theora</em> formātu un jūsu draugs izmanto Windows Media Player, lai atskaņotu to, meklējiet “theora windows media player”. Jūs parasti varēsit lejupielādēt pareizos kodekus bez maksas, ja tie nav ieinstalēti.</p>

  <p>Ja jūs nevarat atrast pareizos kodekus, mēģiniet <link href="http://www.videolan.org/vlc/">VLC media player</link>. Tas darbojas Windows un Mac OS, kā arī Linux, un atbalsta daudz dažādus video formātus. Ja tas neizdodas, mēģiniet pārveidot savu video citā formātā. Lielākā daļa video redaktori spēj to izdarīt un ir pieejamas īpašas video pārveidošanas programmas. Pārbaudiet programmatūras instalēšanas lietotni, lai aplūkotu, kas ir pieejams.</p>

  <note>
    <p>Ir dažas citas problēmas, kas kādu varētu atturēt jūsu video atskaņošanu.Video varētu būt bojāts, kamēr jūs to sūtījāt (dažreiz lielas datnes netiek pārkopēti perfekti), tām varētu būt problēmas ar to video atskaņošanas programmu, vai arī video varētu nebūt izveidots pareizi (tur varētu būt kādas kļūdas, kad jūs saglabājāt video).</p>
  </note>

</page>
