<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="lv">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Pārbaudiet, vai datņu sistēma ir bojāta, un atgrieziet to atpakaļ lietojamā stāvoklī.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Salabot bojātu datņu sistēmu</title>

  <p>Datņu sistēma var tik sabojāta dēļ negaidīta strāvas zuduma, sistēmas avārijas vai nedrošas dziņa izņemšanas. Pēc tāda incidenta ir vēlams <em>salabot</em> vai vismaz <em>pārbaudīt</em> datņu sistēmu, lai novērstu datu zudumu nākotnē.</p>
  <p>Dažkārt ir nepieciešama labošana, lai montētu vai modificētu datņu sistēmu. Pat ja <em>pārbaudīšana</em> neziņo ne par kādiem bojājumiem, datņu sistēma vēl aizvien varētu iekšēji tikt marķēta kā “netīra” un tā būtu jālabo.</p>

<steps>
  <title>Pārbaudīt vai datņu sistēma ir bojāta</title>
  <item>
    <p>Atveriet lietotni <app>Diski</app> no <gui>Aktivitāšu</gui> pārskata.</p>
  </item>
  <item>
    <p>Izvēlieties disku, kurš satur vajadzīgo datņu sistēmu, no krātuvju ierīču saraksta pa kreisi. Ja uz diska ir vairāk kā viens sējums, izvēlieties to sējumu, kurš satur vajadzīgo datņu sistēmu.</p>
  </item>
  <item>
    <p>Rīkjoslā zem <gui>Sējumu</gui> sadaļas spiediet izvēlnes pogu. Tad spiediet <gui>Pārbaudīt datņu sistēmu…</gui>.</p>
  </item>
  <item>
    <p>Atkarībā no tā, cik daudz dati ir saglabāti datņu sistēmā, pārbaude var aizņemt vairāk laika. Apstipriniet darbības sākumu uznirstošajā dialoglodziņā.</p>
   <p>Darbība nemodificēs datņu sistēmu, bet to atmontēs, ja tas ir nepieciešams. Datņu sistēmas pārbaude var aizņemt daudz laika.</p>
  </item>
  <item>
    <p>Pēc pabeigšanas jums paziņos, vai datņu sistēma ir bojāta. Ņemiet vērā, ka dažos gadījumos datņu sistēma varētu būt nebojāta, bet to tāpat vajadzētu salabot, lai atiestatītu iekšējo marķieri “netīra”.</p>
  </item>
</steps>

<note style="warning">
 <title>Iespējamie datu zudumi labojot</title>
  <p>Ja datņu sistēmas struktūra ir bojāta, tas var ietekmēt tajā saglabātās datnes. Dažos gadījumos šīs datnes vairs nevar atjaunot derīgā formā un tās tiks izdzēstas vai pārvietotas uz īpašu direktoriju. Parasti atgūtās datņu daļas ir atrodamas mapē <em>lost+found</em> augšējā līmeņa direktorijā datņu sistēmā.</p>
  <p>Ja dati ir pārāk vērtīgi, lai tos pazaudētu šajā procesā, vēlams tos dublēt, pirms labošana saglabājot sējuma attēlu.</p>
  <p>Šo attēlu tad var apstrādāt ar izmeklēšanas analīzes rīkiem, piemēram, <app>sleuthkit</app>, lai atrastu vēl vairāk pazudušu datņu un to daļas, kas netika atrastas labošanas laikā, kā arī iepriekš dzēstas datnes.</p>
</note>

<steps>
  <title>Salabot datņu sistēmu</title>
  <item>
    <p>Atveriet lietotni <app>Diski</app> no <gui>Aktivitāšu</gui> pārskata.</p>
  </item>
  <item>
    <p>Izvēlieties disku, kurš satur vajadzīgo datņu sistēmu, no krātuvju ierīču saraksta pa kreisi. Ja uz diska ir vairāk kā viens sējums, izvēlieties to sējumu, kurš satur vajadzīgo datņu sistēmu.</p>
  </item>
  <item>
    <p>Rīkjoslā zem <gui>Sējumu</gui> sadaļas spiediet izvēlnes pogu. Tad spiediet <gui>Salabot datņu sistēmu…</gui>.</p>
  </item>
  <item>
    <p>Atkarībā no tā, cik daudz dati ir saglabāti datņu sistēmā, labošana var aizņemt vairāk laika. Apstipriniet darbības sākumu uznirstošajā dialoglodziņā.</p>
   <p>Darbība atmontēs datņu sistēmu, ja nepieciešams. Labošanas darbība mēģinās novest datņu sistēmu konsekventā stāvoklī un pārvietos uz īpašu mapi bojātās datnes. Datņu sistēmas labošana var aizņemt daudz laika.</p>
  </item>
  <item>
    <p>Pēc pabeigšanas jums paziņos, vai datņu sistēmu varēja veiksmīgi salabot. Veiksmes gadījumā to atkal varēs lietot normāli.</p>
    <p>Ja datņu sistēmu nevar salabot, izveidojiet tās rezerves kopiju, saglabājot sējuma attēlu, no kura vēlāk varēsiet vēlāk iegūt svarīgās datnes. To var izdarīt, montējot attēlu tikai lasīšanas režīmā un izmantojot izmeklēšanas rīkus, kā piemēram <app>sleuthkit</app>.</p>
    <p>Lai atkal izmantotu visu sējumu, tam ir jābūt <link xref="disk-format">formatētam</link> ar jaunu datņu sistēmu. Visi dati tiks izmesti.</p>
  </item>
</steps>

</page>
