<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="lv">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>
    <revision version="gnome:45" date="2024-04-04" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Padariet datni neredzamu, lai jūs to nevarētu redzēt datņu pārvaldniekā.</desc>
  </info>

<title>Slēpt datni</title>

  <p><app>Datnes</app> datņu pārvaldnieks dod iespēju pēc izvēles paslēpt vai parādīt atsevišķas datnes. Kad datne ir paslēpta, to nerāda datņu pārvaldnieks, tomēr tā vēl joprojām atrodas mapē.</p>

  <p>Lai paslēptu datni, <link xref="files-rename">pārdēvējiet to</link> ar <file>.</file> jaunā nosaukuma sākumā. Piemēram, lai noslēptu datni <file>piemērs.txt</file>, tas jāpārdēvē par <file>.piemērs.txt</file>.</p>

<note>
  <p>Jūs varat paslēpt mapes tāpat kā datnes. Pārsauciet mapi, ieliekot <file>.</file> tās nosaukuma sākumā.</p>
</note>

<section id="show-hidden">
 <title>Parādīt visas slēptās datnes</title>

  <p>If you want to see all hidden files in a folder, go to that folder and
  either press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You will see all hidden files,
  along with regular files that are not hidden.</p>

  <p>To hide these files again,
  either press the menu button in the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq> again.</p>

</section>

<section id="unhide">
 <title>Padarīt slēptu datni par parastu</title>

  <p>To unhide a file, go to the folder containing the hidden file. Press the menu button in the sidebar of the window and select <gui style="menuitem">Show Hidden Files</gui>, or press
  <keyseq><key>Ctrl</key><key>H</key></keyseq>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either press the menu button in
  the sidebar of the window and switch off <gui style="menuitem">Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>Pēc noklusējuma slēptās datnes paliks redzamas līdz datņu pārvaldnieka aizvēršanai. Lai iestatītu slēpto datņu rādīšanu vienmēr, skatiet <link xref="nautilus-views"/>.</p></note>

  <note><p>Vairumam slēpto datņu ir <file>.</file> nosaukuma sākumā, bet citām var būt <file>~</file> nosaukuma beigās. Tās ir rezerves kopiju datnes. Skatiet <link xref="files-tilde"/>, lai uzzinātu vairāk.</p></note>

</section>

</page>
