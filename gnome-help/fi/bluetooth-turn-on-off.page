<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-turn-on-off" xml:lang="fi">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kytke tietokoneesi Bluetooth-lähetin päälle tai pois päältä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Bluetoothin kytkeminen päälle tai pois</title>

  <p>You can turn Bluetooth on to connect to other Bluetooth devices, or turn
  it off to conserve power. To turn Bluetooth on:</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Set the switch at the top to on.</p>
    </item>
  </steps>

  <p>Many laptop computers have a hardware switch or key combination to turn
  Bluetooth on and off. Look for a switch on your computer or a key on your
  keyboard. The keyboard key is often accessed with the help of the
  <key>Fn</key> key.</p>

  <p>Sammuta Bluetooth:</p>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the
      right side of the top bar.</p>
    </item>
    <item>
      <p>Valitse <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Bluetooth</gui>.</p>
    </item>
  </steps>

  <note><p>Tietokoneesi on <link xref="bluetooth-visibility">näkyvissä</link> <gui>Bluetooth</gui>-paneelin ollessa avoinna.</p></note>

</page>
