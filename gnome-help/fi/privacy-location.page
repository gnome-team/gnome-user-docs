<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-location" xml:lang="fi">
  <info>
    <link type="guide" xref="privacy" group="#last"/>

    <revision pkgversion="3.14" date="2014-10-13" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ota käyttöön tai poista käytöstä sijainnin paikantaminen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Hallitse paikannuspalveluita</title>

  <p>Geolocation, or location services, uses cell tower positioning, GPS, and
  nearby Wi-Fi access points to determine your current location for use in
  setting your timezone and by applications such as <app>Maps</app>. When
  enabled, it is possible for your location to be shared over the network with
  a great deal of precision.</p>

  <steps>
    <title>Poista käytöstä työpöytäsi paikannusominaisuudet</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy &amp; Security</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Privacy &amp; Security</gui></guiseq> from the
      results. This will open the <gui>Privacy &amp; Security</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Location</gui> to open the panel.</p>
    </item>
    <item>
     <p>Switch the <gui>Automatic Device Location</gui> switch to off.</p>
     <p>To re-enable this feature, set the <gui>Automatic Device Location</gui> switch
     to on.</p>
    </item>
  </steps>

</page>
