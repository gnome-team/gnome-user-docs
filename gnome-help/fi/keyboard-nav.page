<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="fi">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Käytä sovelluksia ja työpöytää ilman hiirtä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Näppäimistöllä liikkuminen</title>

  <p>Tämä sivu linjaa, kuinka käyttöliittymän eri osissa liikutaan näppäimistöä käyttäen. Ohje on tarkoitettu niille, jotka eivät pysty käyttämään hiirtä tai muuta osoitinlaitetta, tai jotka haluavat käyttää näppäimistöä mahdollisimman paljon. Kaikille hyödyllisiä pikanäppäimiä on lueteltu sivulla <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>Jos et syystä tai toisesta kykene käyttämään osoitinlaitetta (kuten hiirtä), voit ohjata hiiren osoitinta näppäimistön oikeassa laidassa olevaa numeronäppäimistöä käyttäen. Lisätietoja aiheesta on sivulla <link xref="mouse-mousekeys"/>.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Liiku käyttöliittymän eri osissa</title>
  <tr>
    <td><p><key>Tab</key> ja</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>Siirrä näppäimistön kohdistusta eri säätimien välillä. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> liikuttaa säädinryhmien välillä, kuten sivupaneelista sisältönäkymään. <keyseq><key>Ctrl</key><key>Tab</key></keyseq> -näppäinyhdistelmällä voi poistua säätimestä, joka käyttää itse <key>Tab</key>-näppäintä, kuten tekstikentästä.</p>
      <p>Pidä <key>Shift</key> pohjassa siirtääksesi kohdistuksen vastakkaiseen suuntaan.</p>
    </td>
  </tr>
  <tr>
    <td><p>Nuolinäppäimet</p></td>
    <td>
      <p>Move selection between items in a single control, or among a set of
      related controls. Use the arrow keys to focus buttons in a toolbar, select
      items in a list or grid view, or select a radio button from a group.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>nuolinäppäimet</keyseq></p></td>
    <td><p>In a list or grid view, move the keyboard focus to another item
    without changing which item is selected.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>nuolinäppäimet</keyseq></p></td>
    <td><p>In a list or grid view, select all items from the currently selected
    item to the newly focused item.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Välilyönti</key></p></td>
    <td><p>Aktivoi kohdistettu kohde, kuten painike tai näkymässä valittu kohde.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Välilyönti</key></keyseq></p></td>
    <td><p>In a list or grid view, select or deselect the focused item without
    deselecting other items.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Pidä <key>Alt</key> pohjassa tuodaksesi <em>valintanäppäimet</em> esiin: alleviivauksella varustetut kirjaimet esimerkiksi valikoissa ja painikkeissa.  Paina <key>Alt</key> ja alleviivauksella varustettua näppäintä aktivoidaksesi kyseisen valinnan, aivan kuin kyseistä valintaa olisi napsautettu hiirellä. Jos esimerkiksi samassa valikossa on useita samoja valintanäppäimiä, niiden välillä on mahdollista liikkua painamalla kyseistä näppäintä useampaan kertaan. Lopuksi valinta on mahdollista aktivoida painalla <key>Enter</key>.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Sulje valikko, vaihdin tai valintaikkuna.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Avaa ikkunan valikkopalkin ensimmäinen valikko. Käytä nuolinäppäimiä valikkojen välillä liikkumiseen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> tai</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Avaa nykyisen valinnan pikavalikko, sama kuin kohteen napsauttaminen hiiren oikealla painikkeella.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>Avaa nykyisen kansion pikavalikko tiedostonhallinnassa, sama kuin tiedostonhallinnan taustan napsauttaminen hiiren oikealla painikkeella.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>ja</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>Vaihda välilehteä vasemmalle tai oikealle.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Liiku työpöydällä</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Cycle through windows in the same application. Hold down the
    <key>Alt</key> key and press <key>F6</key> until the window you want is
    highlighted, then release <key>Alt</key>. This is similar to the
    <keyseq><key>Alt</key><key>`</key></keyseq> feature.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Vaihda kaikkien työtilassa avoinna olevien ikkunoiden välillä.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Avaa ilmoitusluettelo.</link> Paina <key>Esc</key> sulkeaksesi.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Hallitse ikkunoita</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Sulje nykyinen ikkuna.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> tai <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restore a maximized window to its original size. Use
    <keyseq><key>Alt</key><key>F10</key></keyseq> to maximize.
    <keyseq><key>Alt</key><key>F10</key></keyseq> both maximizes and
    restores.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Siirrä nykyistä ikkunaa. Paina <keyseq><key>Alt</key><key>F7</key></keyseq> ja käytä nuolinäppäimiä ikkunan siirtämiseen. Paina <key>Enter</key> lopettaaksesi siirron tai <key>Esc</key> palauttaaksesi ikkunan alkuperäiselle paikalleen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Muuta nykyisen ikkunan kokoa. Paina <keyseq><key>Alt</key><key>F8</key></keyseq> ja käytä nuolinäppäimiä ikkunan koon muuttamiseen. Paina <key>Enter</key> lopettaaksesi koonmuunnoksen tai <key>Esc</key> palauttaaksesi ikkunan alkuperäisen koon.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> tai <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Suurenna</link> ikkuna. Paina <keyseq><key>Alt</key><key>F10</key></keyseq> tai <keyseq><key>Super</key><key>↓</key></keyseq> palauttaaksesi ikkunan alkuperäiseen kokoon.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Pienennä ikkuna.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the left side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>→</key></keyseq> to switch
    sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximize a window vertically along the right side of the screen.
    Press again to restore the window to its previous size. Press
    <keyseq><key>Super</key><key>←</key></keyseq> to
    switch sides.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Välilyönti</key></keyseq></p></td>
    <td><p>Avaa ikkunanhallintavalikko, sama kuin otsikkopalkin napsautus hiiren oikealla painikkeella.</p></td>
  </tr>
</table>

</page>
