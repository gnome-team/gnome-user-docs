<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-noconnection" xml:lang="fi">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" version="0.2" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Varmista, että kirjoittamasi salasana oli oikein, ja muita yrittämisen arvoisia asioita.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Olen kirjoittanut salasanan oikein, mutta yhteydenmuodostus ei onnistu</title>

<p>Jos olet varma, että kirjoitit <link xref="net-wireless-wepwpa">langattoman verkon salasanan</link> oikein, mutta et siltikään pysty yhdistämään langattomaan verkkoon, kokeile jotain seuraavista:</p>

<list>
 <item>
  <p>Varmista, että kirjoittamasi salasana on oikein</p>
  <p>Passwords are case-sensitive (it matters whether they have capital or lower-case letters), so check that you didn’t get the case of one of the letters wrong.</p>
 </item>

 <item>
  <p>Yritä käyttää hex- tai ASCII-salausavainta</p>
  <p>The password you enter can also be represented in a different way — as a string of characters in hexadecimal (numbers 0-9 and letters a-f) called a pass key. Each password has an equivalent pass key. If you have access to the pass key as well as the password/passphrase, try typing the pass key instead. Make sure you select the correct <gui>wireless security</gui> option when asked for your password (for example, select <gui>WEP 40/128-bit Key</gui> if you’re typing the 40-character pass key for a WEP-encrypted connection).</p>
 </item>

 <item>
  <p>Kytke langaton verkkoyhteys pois päältä ja sitten takaisin päälle</p>
  <p>Sometimes wireless cards get stuck or experience a minor problem that means they won’t connect. Try turning the card off and then on again to reset it — see <link xref="net-wireless-troubleshooting"/> for more information.</p>
 </item>

 <item>
  <p>Varmista, että käytät oikeatyyppistä verkon salaustekniikkaa</p>
  <p>When prompted for your wireless security password, you can choose which type of wireless security to use. Make sure you choose the one that is used by the router or wireless base station. This should be selected by default, but sometimes it will not be for some reason. If you don’t know which one it is, use trial and error to go through the different options.</p>
 </item>

 <item>
  <p>Varmista, että langaton verkkokorttisi on kunnolla tuettu</p>
  <p>Some wireless cards aren’t supported very well. They show up as a wireless connection, but they can’t connect to a network because their drivers lack the ability to do this. See if you can get an alternative wireless driver, or if you need to perform some extra set-up (like installing a different <em>firmware</em>). See <link xref="net-wireless-troubleshooting"/> for more information.</p>
 </item>

</list>

</page>
