<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="fi">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sovellukset voivat käyttää <app>verkkotileissä</app> määriteltyjä palveluja.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Verkkopalvelut ja sovellukset</title>

  <p>Kun olet lisännyt verkkotilin, mikä tahansa sovellus voi käyttää sen tilin kaikkia palveluja, <link xref="accounts-disable-service">joita et ole erikseen poistanut käytöstä</link>. Eri palveluntarjoajat tarjoavat erilaisia palveluja. Tämä sivu listaa erilaisia palveluja ja joitain sovelluksia, joiden tiedetään hyödyntävän kyseisiä palveluja.</p>

  <terms>
    <item>
      <title>Kalenteri</title>
      <p>Kalenteripalvelu mahdollistaa verkkokalenterissa olevien tapahtumien näkemisen, lisäämisen ja muokkauksen. Monet sovellukset, kuten <app>Kalenteri</app>, <app>Evolution</app> ja <app>California</app>, käyttävät kalenteripalvelua.</p>
    </item>

    <item>
      <title>Keskustelu</title>
      <p>Keskustelupalvelu mahdollistaa keskustelun tuttavien kanssa suosituissa pikaviestinalustoissa. Sitä käyttää <app>Empathy</app>-sovellus.</p>
    </item>

    <item>
      <title>Yhteystiedot</title>
      <p>Yhteystietopalvelu mahdollistaa yhteystietojen näkemisen useista eri palveluista. Sitä käyttäviä sovelluksia ovat <app>Yhteystiedot</app> ja <app>Evolution</app>.</p>
    </item>

<!-- See https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/commit/32f35cc07fcbee839978e3630972b67ed55f0da6
    <item>
      <title>Documents</title>
      <p>The Documents service allows you to view your online documents
      such as those in Google docs. You can view your documents using the
      <app>Documents</app> application.</p>
    </item>
-->
    <item>
      <title>Tiedostot</title>
      <p>The Files service adds a remote file location, as if you had added
      one using the <link xref="nautilus-connect">Connect to Server</link>
      functionality in the file manager. You can access remote files using
      the file manager, as well as through file open and save dialogs in any
      application.</p>
    </item>

    <item>
      <title>Sähköposti</title>
      <p>Sähköpostipalvelut mahdollistaa sähköpostin lähettämisen ja vastaanottamisen eri palveluntarjoajilta, kuten Googlelta. Tätä palvelua käyttää <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->
<!-- TODO: Not sure what this does in which app. Seems to support e.g. VKontakte.
    <item>
      <title>Music</title>
    </item>
-->
    <item>
      <title>Kuvat</title>
      <p>The Photos service allows you to view your online photos such as
      those you post on Facebook. You can view your photos using the
      <app>Photos</app> application.</p>
    </item>

    <item>
      <title>Tulostimet</title>
      <p>The Printers service allows you to send a PDF copy to a provider from
      within the print dialog of any application. The provider might provide
      print services, or it might just serve as storage for the PDF, which
      you can download and print later.</p>
    </item>

  </terms>

</page>
