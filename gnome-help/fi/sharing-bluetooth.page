<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="fi">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014-2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Salli tiedostoja lähetettävän tietokoneellesi Bluetoothin välityksellä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Hallitse jakamista Bluetoothin välityksellä</title>

  <p>You can enable <gui>Bluetooth</gui> sharing to receive files over
  Bluetooth in the <file>Downloads</file> folder</p>

  <steps>
    <title>Salli tiedostojen jakaminen <file>Lataukset</file>-kansioosi</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Varmista, että <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> on kytketty päälle</link>.</p>
    </item>
    <item>
      <p>Bluetooth-enabled devices can send files to your
      <file>Downloads</file> folder only when the <gui>Bluetooth</gui> panel is
      open.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Voit <link xref="about-hostname">vaihtaa</link> tietokoneesi nimeä, joka näkyy muille.</p>
  </note>

</page>
