<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="fi">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mene <guiseq><gui>Asetukset</gui><gui>Värit</gui></guiseq> lisätäksesi väriprofiilin näytöllesi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Kuinka kohdistan profiileja laitteisiin?</title>

  <p>You may want to assign a color profile for your screen or printer so that
  the colors which it shows are more accurate.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Värit</gui> sivupalkista avataksesi paneelin.</p>
    </item>
    <item>
      <p>Valitse laite, jolle haluat lisätä profiilin.</p>
    </item>
    <item>
      <p>Napsauta <gui>Lisää profiili</gui> valitaksesi olemassa olevan profiilin tai tuodaksesi uuden profiilin.</p>
    </item>
    <item>
      <p>Napsauta <gui>Lisää</gui> vahvistaaksesi valintasi.</p>
    </item>
    <item>
      <p>To change the used profile, select the profile you would like to use
      and press <gui>Enable</gui> to confirm your selection.</p>
    </item>
  </steps>

  <p>Jokaiseen laitteeseen voidaan yhdistää monia profiileja mutta vain yksi profiili voi olla <em>oletusprofiili</em>. Oletusprofiilia käytetään, jos toisten profiilien käyttämiseen ei ole mitään syytä. Esimerkki automaattisesta valinnasta voisi olla, että yksi profiili on luotu kiiltävälle paperille ja toinen tavalliselle paperille.</p>

  <p>If calibration hardware is connected, the <gui>Calibrate…</gui> button
  will create a new profile.</p>

</page>
