<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="fi">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="43" date="2022-09-10" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kopioi tai siirrä kohteita uuteen kansioon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Kopioi tai poista tiedostoja ja kansioita</title>

 <p>Tiedosto tai kansio on kopioitavissa tai siirrettävissä uuteen sijaintiin hiirellä vetäen, tiedostonhallinnan valikon kautta tai käyttäen pikanäppäimiä.</p>

 <p>For example, you might want to copy a presentation onto a memory stick so
 you can take it to work with you. Or, you could make a back-up copy of a
 document before you make changes to it (and then use the old copy if you don’t
 like your changes).</p>

 <p>Nämä ohjeet pätevät sekä tiedostoihin että kansioihin. Tiedostoja ja kansioita on mahdollista siirtää täsmälleen samalla tavalla.</p>

<steps ui:expanded="false">
<title>Kopioi ja liitä tiedostoja</title>
<item><p>Valitse kopioitava kohde napsauttamalla sitä kerran.</p></item>
<item><p>Right-click and select <gui>Copy</gui>, or press
 <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>Siirry toiseen kansioon, johon haluat liittää kopioitavan kohteen.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Leikkaa ja liitä tiedostoja niiden siirtämiseksi</title>
<item><p>Valitse siirrettävä kohde napsauttamalla sitä kerran.</p></item>
<item><p>Right-click and select <gui>Cut</gui>, or press
 <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>Siirry toiseen kansioon, johon haluat siirtää kohteen.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Vedä tiedostot kopioidaksesi tai siirtääksesi</title>
<item><p>Avaa tiedostonhallinta siihen kansioon, jossa kopioitava kohde on.</p></item>
<item><p>Press the menu button in the sidebar of the window and select
 <gui style="menuitem">New Window</gui> (or
 press <keyseq><key>Ctrl</key><key>N</key></keyseq>) to open a second window. In
 the new window, navigate to the folder where you want to move or copy the file.
 </p></item>
<item>
 <p>Paina ja raahaa kohde ikkunasta toiseen. Oletuksena raahaaminen <em>siirtää sen</em>, jos kohde on <em>samalla</em> laitteella (esim. jos molemmat kansiot ovat koneesi samalla kiintolevyllä), tai <em>kopioi</em> jos kohde on <em>eri</em> laitteella.</p>
 <p>For example, if you drag a file from a USB memory stick to your Home folder,
 it will be copied, because you’re dragging from one device to another.</p>
 <p>Tiedoston kopioinnin voi pakottaa pitämällä pohjassa <key>Ctrl</key>-näppäintä vedettäessä, tai tiedoston siirtyminen voidaan pakottaa pitämällä pohjassa <key>Shift</key>-näppäintä vedettäessä.</p>
 </item>
</steps>

<note>
  <p>You cannot copy or move a file into a folder that is <em>read-only</em>.
  Some folders are read-only to prevent you from making changes to their
  contents. You can change things from being read-only by
  <link xref="nautilus-file-properties-permissions">changing file permissions
  </link>.</p>
</note>

</page>
