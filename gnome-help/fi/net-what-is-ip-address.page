<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="fi">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>IP-osoite on tavallaan tietokoneesi "puhelinnumero".</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Mikä IP-osoite on?</title>

  <p>IP-osoite on lyhenne sanoista <em>Internet Protocol</em>. Jokaisella verkkoon (esimerkiksi internetiin) kytketyllä laitteella on IP-osoite.</p>

  <p>IP-osoite vastaa hieman puhelinnumeroa. Puhelinnumerosi on yksilöllinen numerosarja, jonka avulla ihmisten puhelut voidaan ohjata puhelimeesi. IP-osoite on vastaavasti tietokoneesi verkossa yksilöivä numerosarja, jonka avulla tietokoneen on mahdollista lähettää ja vastaanottaa tietoa muiden tietokoneiden kanssa.</p>

  <p>Nykyään useimmat IP-osoitteet koostuvat neljästä pisteillä erotelluista numerosarjoista (esimerkiksi <code>192.168.1.42</code>).</p>

  <note style="tip">
    <p>An IP address can either be <em>dynamic</em> or <em>static</em>. Dynamic
    IP addresses are temporarily assigned each time your computer connects to a
    network. Static IP addresses are fixed, and do not change. Dynamic IP
    addresses are more common that static addresses — static addresses are
    typically only used when there is a special need for them, such as in the
    administration of a server.</p>
  </note>

</page>
