<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="fi">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kirjoita erikoismerkkejä, joita ei löydy näppäimistöstäsi, kuten ulkomaalaisia aakkosia, matemaattisia symboleja, emojeja ja muita erikoismerkkejä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Erikoismerkkien käyttäminen</title>

  <p>Voit kirjoittaa tai katsella tuhansilla eri merkeillä, joita esiintyy maailman eri kirjoitusjärjestelmissä. Myös niillä, joita ei näppäimistöstäsi löydy. Tämä sivu kuvaa eri tapoja erikoismerkkien käyttöön.</p>

  <links type="section">
    <title>Merkkien kirjoitustapoja</title>
  </links>

  <section id="characters">
    <title>Merkit</title>
    <p>The character map application allows you to find and insert unusual
    characters, including emoji, by browsing character categories or searching
    for keywords.</p>

    <p>Voit käynnistää <app>Merkit</app>-sovelluksen Toiminnot-yleisnäkymästä.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Syötä emoji</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Browse the categories at the bottom of the dialog or start typing a
    description in the search field.</p>
      </item>
      <item>
        <p>Valitse lisättävä emoji.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Compose-näppäin</title>
    <p>Compose-näppäin on erikoisnäppäin, jonka avulla voi painaa useampaa näppäintä ja näin tuottaa erikoismerkin. Esimerkiksi merkin <em>é</em> saa luotua painamalla <key>compose</key>-näppäintä, sitten <key>'</key> ja lopuksi <key>e</key>.</p>
    <p>Näppäimistöissä ei ole erillistä compose-näppäintä. Sen sijaan voit määrittää jonkin näppäimistösi näppäimistä compose-näppäimeksi.</p>

    <steps>
      <title>Määritä compose-näppäin</title>
      <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Asetukset</gui>.</p>
      </item>
      <item>
        <p>Napsauta <gui>Asetukset</gui>.</p>
      </item>
      <item>
        <p>Napsauta <gui>Näppäimistö</gui> sivupalkista avataksesi paneelin.</p>
      </item>
      <item>
        <p>In the <gui>Type Special Characters</gui> section, click <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Turn the switch on for the <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Sulje ikkuna.</p>
      </item>
    </steps>

    <p>Voit kirjoittaa monia yleisiä merkkejä compose-näppäintä käyttäen. Alla esimerkkejä:</p>

    <list>
      <item><p>Paina <key>compose</key>, sitten <key>'</key> ja lopuksi kirjainta, jonka päälle haluat asettaa akuutin. Jos painat e-kirjainta, lopputuloksena on <em>é</em>.</p></item>
      <item><p>Paina <key>compose</key>, sitten <key>`</key> ja lopuksi kirjainta, jonka päälle haluat asettaa graviksen. Jos painat e-kirjainta, lopputuloksena on <em>è</em>.</p></item>
      <item><p>Paina <key>compose</key>, sitten <key>"</key> ja lopuksi kirjainta, jonka päälle haluat asettaa treeman. Jos painat e-kirjainta, lopputuloksena on <em>ë</em>.</p></item>
      <item><p>Paina <key>compose</key>, sitten <key>-</key> ja lopuksi kirjainta, jonka päälle haluat asettaa pituusmerkin. Jos painat e-kirjainta, lopputuloksena on <em>ē</em>.</p></item>
    </list>
    <p>For more compose key sequences, see <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">the
    compose key page on Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Koodiarvot</title>

  <p>Voit kirjoittaa minkä tahansa Unicode-merkin käyttäen näppäimistöäsi ja ns. koodiarvoja. Jokaisella merkillä on oma nelimerkkinen koodiarvo. Etsi haluamasi merkin koodiarvo avaamalla <app>Merkit</app> ja valitsemalla haluamasi merkki. Koodiarvo on merkinnän <gui>U+</gui> jälkeen näkyvät neljä merkkiä.</p>

  <p>Kirjoita merkki sen koodiarvoa käyttäen painamalla <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq> ja sen jälkeen nelimerkkinen koodiarvo. Tämän jälkeen paina <key>Space</key> tai <key>Enter</key>. Jos käytät usein merkkejä, joita et voi käyttää muilla syötemenetelmillä, saatat haluta painaa mieleesi kyseisten merkkien koodiarvot.</p>

</section>

  <section id="layout">
    <title>Näppäimistöasettelut</title>
    <p>Voit asettaa näppäimistösi toimimaan ikään kuin eri kielen näppäimistönä huolimatta näppäimistöön painettujen merkkien kuvista. Voit vaihtaa eri näppäimistöasettelujen välillä yläpalkin kuvakkeen avulla. Lisätietoja tähän liittyen on kohdassa <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Syöttötavat</title>

  <p>Syöttötavat laajentavat aiempia esimerkkejä mahdollistamalla merkkien kirjoittamisen millä tahansa syötelaitteella näppäimistön lisäksi. Näin merkkejä voi kirjoittaa esimerkiksi hiirieleillä, tai japanilaisten merkkien kirjoittaminen suomalaisella näppäimistöllä on mahdollista.</p>

  <p>Valitse syöttötapa napsauttamalla hiiren oikealla painikkeella mitä tahansa tekstikenttää ja napsauta avautuvan valikon <gui>Syöttötavat</gui>-alivalikosta haluamaasi syöttötapaa. Yhtäkään syöttötapaa ei tarjota oletuksena, joten lue lisätietoja haluamastasi syöttötavasta sen omasta ohjeesta.</p>

</section>

</page>
