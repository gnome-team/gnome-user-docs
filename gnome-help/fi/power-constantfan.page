<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="fi">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tuulettimen hallintaohjelmisto saattaa puuttua tai kannettavan lämpötila voi olla korkea.</desc>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Kannettavan tuuletin pyörii jatkuvasti</title>

<p>If cooling fan in your laptop is always running, it could be that the
hardware that controls the cooling system in the laptop is not very well
supported in Linux. Some laptops need extra software to control their cooling
fans efficiently, but this software may not be installed (or available for
Linux at all) and so the fans just run at full speed all of the time.</p>

<p>Jos tilanne on tämä, voit ehkä muuttaa jotakin asetuksia tai asentaa tuulettimen hallintaan tarkoitettuja ohjelmistoja. Esimerkiksi joidenkin Sony VAIO -kannettavien tuuletinta voi hallita <link href="http://vaio-utils.org/fan/">vaiofand</link>-sovelluksella. Koska tuulettimen ohjausohjelmiston asentaminen on hyvin tekninen ja suurelta osin laitteistosta riippuva prosessi, haluat ehkä etsiä tarkempia ohjeita kyseisen ohjelmiston asentamiseen juuri sinun koneellesi.</p>

<p>On myös mahdollista, että kannettava tietokone yksinkertaisesti tuottaa paljon lämpöä. Vaikka tuuletin pyörii täyttä vauhtia, se ei välttämättä tarkoita, että kone ylikuumenee. Kone saattaa tarvita kaiken mahdollisen tuuletuksen, jotta se pysyy tarpeeksi kylmänä. Tässä tapauksessa ei ole kovin monia muita vaihtoehtoja kuin antaa tuulettimen pyöriä täyttä vauhtia kaiken aikaa. Yksi mahdollisuus on ostaa kannettavan tietokoneen jäähdyttämiseen tarkoitettuja lisävarusteita.</p>

</page>
