<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-find" xml:lang="fi">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-hidden"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.12" date="2014-03-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Langaton verkkolaite saattaa olla pois päältä tai epäkunnossa, tai saatat yrittää yhdistämistä piilotettuun verkkoon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>En näe langatonta verkkoani listalla</title>

  <p>There are a number of reasons why you might not be able to see your
  wireless network on the list of available networks from the system menu.</p>

<list>
 <item>
  <p>Jos listalla ei ole yhtään verkkoa, langaton verkkokortti voi olla pois päältä tai <link xref="net-wireless-troubleshooting">tai ei välttämättä toimi oikein</link>. Varmista että WLAN-vastaanotin on päällä. Tämä tehdään yleensä laitteessa olevasta painikkeesta.</p>
 </item>
<!-- Does anyone have lots of wireless networks to test this? Pretty sure it's
     no longer correct.
  <item>
    <p>If there are lots of wireless networks nearby, the network you are
    looking for might not be on the first page of the list. If this is the
    case, look at the bottom of the list for an arrow pointing towards the
    right and hover your mouse over it to display the rest of the wireless
    networks.</p>
  </item>-->
 <item>
  <p>Saatat olla verkon kantomatkan ulottumattomissa. Siirry lähemmäksi tukiasemaa ja tarkista hetken kuluttua, ilmestyykö verkon nimi luetteloon.</p>
 </item>
 <item>
  <p>Langattomien verkkojen saatavuuden päivittäminen vie jonkin aikaa. Jos tietokone on juuri käynnistetty tai siirretty toiseen paikkaan, odota hetki ja tarkista tilanne uudelleen.</p>
 </item>
 <item>
  <p>Verkko saattaa olla piilotettu. Piilotettuun verkkoon <link xref="net-wireless-hidden">yhdistetään eri tavalla</link>.</p>
 </item>
</list>

</page>
