<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="fi">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Käytä <app>Orca</app>-näytönlukijaa käyttöliittymän ääneen lukemiseen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Lue näytön sisältöä ääneen</title>

  <p>The <app>Orca</app> screen reader can speak the user interface. Depending
  on how you installed your system, you might not have Orca installed. If not,
  install Orca first.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Asenna Orca</link></p>
  
  <p>Käynnistä <app>Orca</app> näppäimistöllä seuraavasti:</p>
  
  <steps>
    <item>
    <p>Paina <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Tai käynnistä <app>Orca</app> hiirtä ja näppäimistöä käyttäen:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Seeing</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Screen Reader</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ota näytönlukija nopeasti käyttöön tai pois käytöstä</title>
    <p>Voit ottaa näytönlukijan käyttöön tai poistaa sen käytöstä napsauttamalla yläpalkista <link xref="a11y-icon">esteettömyyskuvaketta</link> ja valitsemalla <gui>Näytönlukija</gui>.</p>
  </note>

  <p>Lisätietoja on saatavilla <link href="help:orca">Orcan ohjeesta</link>.</p>
</page>
