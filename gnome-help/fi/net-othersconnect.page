<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="fi">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Voit tallentaa verkkoyhteyden asetukset (kuten salasanan), jotta kaikki tietokoneen käyttäjät voivat käyttää määrittelemääsi verkkoyhteyttä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Muut käyttäjät eivät voi yhdistää internetiin</title>

  <p>When you set up a network connection, all other users on your computer
  will normally be able to use it. If the connection information is not shared,
  you should check the connection settings.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Verkko</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Verkko</gui>.</p>
    </item>
    <item>
      <p>Select <gui>Wi-Fi</gui> from the list on the left.</p>
    </item>
    <item>
      <p>Click the 
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media> button to open the connection
      details.</p>
    </item>
    <item>
      <p>Select <gui>Identity</gui> from the pane on the left.</p>
    </item>
    <item>
      <p>At the bottom of the <gui>Identity</gui> panel, check the <gui>Make
      available to other users</gui> option to allow other users to use the
      network connection.</p>
    </item>
    <item>
      <p>Press <gui style="button">Apply</gui> to save the changes.</p>
    </item>
  </steps>

  <p>Muut tietokoneen käyttäjät voivat nyt käyttää tätä verkkoa ilman minkäännäköistä asetusten määrittämistä.</p>

  <note>
    <p>Kuka tahansa käyttäjä voi muuttaa tätä asetusta.</p>
  </note>

</page>
