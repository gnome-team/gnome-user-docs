<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="fa">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>رساندن آسان پرونده‌ها به آشنایان رایانامه‌ایتان از مدیر پرونده.</desc>
  </info>

<title>هم‌رسانی پرونده‌ها با رایانامه</title>

<p>You can easily share files with your contacts by email directly from the
file manager.</p>

  <note style="important">
    <p>Before you begin, make sure <app>Evolution</app> or <app>Geary</app> is
    installed on your computer, and your email account is configured.</p>
  </note>

<steps>
  <title>برای هم‌رسانی یک پرونده با رایانامه:</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
  <item><p>Locate the file you want to transfer.</p></item>
    <item>
      <p>Right-click the file and select <gui>Send to…</gui>. An email compose
      window will appear with the file attached.</p>
    </item>
  <item><p>Click <gui>To</gui> to choose a contact, or enter an email address
  where you want to send the file. Fill in the <gui>Subject</gui> and the body
  of the message as required and click <gui>Send</gui>.</p></item>
</steps>

<note style="tip">
  <p>You can send multiple files at once. Select multiple files by holding
  down <key>Ctrl</key> while clicking the files, then right-click any selected
  file.</p>
</note>

</page>
