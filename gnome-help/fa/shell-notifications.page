<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="fa">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="40" date="2021-09-10" status="review"/>

    <credit type="author">
      <name>مارینا ژوراخینسکایا</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
      <years>۲۰۱۳۷ ۲۰۱۵</years>
    </credit>
    <credit type="editor">
      <name>پیتر کوار</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>پیام‌هایی از بالای صفحه به پایین کشیده می‌شوند، زمان رخ دادن رویداهای خاص را به شما می‌گویند.</desc> </info>

<title>آگاهی‌ها و سیاههٔ آگاهی‌ها</title>

<section id="what">
  <title>آگاهی چیست؟</title>

  <p>اگر برنامه یا مولّفه‌ای از سامانه بخواهد توجّهتان را جلب کند، یک آگاهی در بالای صفحه یا روی صفحهٔ قفلتان نشان داده خواهد شد.</p>

  <p>برای نمونه اگر یک پیام گپ یا رایانامهٔ جدید بگیرید، آگاهی‌ای خواهید گرفت. آگاهی‌های گپ رفتار خاصی داشته و از سوی آشنایانی که آن پیام گپ را برایتان فرستاده‌اند نشان داده می‌شوند.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>دیگر آگاهی‌ها، دکمه‌هایی قابل گزینش دارند. برای بستن این آگاهی‌ها بدون گزینش یکی گزینه‌هایشان، روی دکمهٔ بستن بزنید.</p>

  <p>زدن دکمهٔ بستن روی برخی آگاهی‌ها از بین می‌بردشان. برخی دیگر همچون ریتم‌باکس یا برنامهٔ گپتان در سیاههٔ آگاهی‌ها پنهان خواهند ماند.</p>

</section>

<section id="notificationlist">

  <title>سیاههٔ آگاهی</title>

  <p>سیاههٔ آگاهی‌ها به شما راهی برای بازگشت به آگاهی‌هایتان هنگامی که برایتان مناسب است می‌دهد. هنگامی که روی ساعت کلیک کرده یا <keyseq><key xref="keyboard-key-super">سوپر</key><key>V</key></keyseq> را می‌زنید ظاهر می‌شود. سیاههٔ آگاهی‌ها شامل تمامی آگاهی‌هاییست که اقدامی برایشان نکرده یا در آن ثابت هستید.</p>

  <p>می‌توانید آگاهی را با زدن رویش در سیاهه ببینید. می‌توانید با زدن دوبارهٔ <keyseq><key>سوپر</key><key>V</key></keyseq> یا <key>گریز</key> سیاههٔ آگاهی‌ها را ببندید.</p>

  <p>برای خالی کردن سیاههٔ آگاهی‌ها دکمهٔ <gui>پاک‌سازی</gui> را بزنید.</p>

</section>

<section id="hidenotifications">

  <title>نهفتن آگاهی‌ها</title>

  <p>اگر دارید روی چیزی کار می‌کنید و نمی‌خواهید چیزی مزاحمتان شود، می‌توانید آگاهی‌ها را خاموش کنید.</p>

  <p>می‌توانید تمامی آگاهی‌ها ر ابا گشودن سیاههٔ آگاهی‌ها و تغییر وضعیت <gui>مزاحم نشوید</gui> در پایین پنهان کنید. یا این که:</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>تنظمیات</gui> کنید.</p>
    </item>
    <item>
      <p>روی <gui>تنظیمات</gui> بزنید.</p>
    </item>
    <item>
      <p>روی <gui>آگاهی‌ها</gui> در نوار کناری بزنید تا تابلو باز شود.</p>
    </item>
    <item>
      <p><gui>مزاحم نشوید</gui> را روشن کنید.</p>
    </item>
  </steps>

  <p>هنگام روشن بودن <gui>مزاحم نشوید</gui> تنها آگاهی‌های بسیار مهم چون خالی بودن بخرانی باتری در بالای صفحه ظاهر خواهند شد. همهٔ آگاهی‌ها همچنان در سیاههٔ آگاهی‌ها موجودند (نمایش با زدن روی ساغت یا فشردن <keyseq><key>سوپر</key><key>V</key></keyseq>) و پس از خاموش کردن <gui>مزاحم نشوید</gui> دوباره ظاهر خواهند شد.</p>

  <p>هم‌چنین می‌توانید از تابلوی <gui>آگاهی‌ها</gui>، آگاهی‌ها را برای برنامه‌ها به صورت تکی از کار انداخته یا دوباره به کار بیندازید.</p>

</section>

<section id="lock-screen-notifications">

  <title>نهفتن آگاهی‌های صفحهٔ قفل</title>

  <p>هنگام قفل بودن صفحه‌تان، آگاهی‌ها روی صفحهٔ قفل ظاهر خواهند شد. می‌توانید به دلایل محرمانگی، صفحهٔ قفل را برای نهفتن این آگاهی‌ها پیکربندی کنید.</p>

  <steps>
    <title>برای خاموش کردن آگاهی‌ها هنگام قفل بودن صفحه‌تان:</title>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>تنظمیات</gui> کنید.</p>
    </item>
    <item>
      <p>روی <gui>تنظیمات</gui> بزنید.</p>
    </item>
    <item>
      <p>روی <gui>آگاهی‌ها</gui> در نوار کناری بزنید تا تابلو باز شود.</p>
    </item>
    <item><p>وضعیت <gui>آگاهی‌های صفحهٔ قفل</gui> را به خاموش تغییر دهید.</p></item>
  </steps>

</section>

</page>
