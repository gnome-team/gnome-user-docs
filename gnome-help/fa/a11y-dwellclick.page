<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="fa">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>فیل بول</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>ویژگی <gui>کلیک شناور</gui> (کلیک ساکن) به شما امکان می دهد با ثابت نگه داشتن ماوس کلیک کنید.</desc>
  </info>

  <title>شبیه‌سازی کلیک با شناوری</title>

  <p>You can click or drag simply by hovering your mouse pointer over a control
  or object on the screen. This is useful if you find it difficult to move the
  mouse and click at the same time. This feature is called <gui>Hover
  Click</gui> or Dwell Click.</p>

  <p>When <gui>Hover Click</gui> is enabled, you can move your mouse pointer
  over a control, let go of the mouse, and then wait for a while before the
  button will be clicked for you.</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>دسترسی‌پذیری</gui> کنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو <gui>دسترسی‌پذیری</gui> را بزنید.</p>
    </item>
    <item>
      <p>بخش <gui>نشانه و کلیک</gui> را برای گشودن برگزینید.</p>
    </item>
    <item>
      <p>در بخش <gui>همیار کلیک</gui>، کلید <gui>کلیک شناور</gui> را روشن کنید.</p>
    </item>
  </steps>

  <p>The <gui>Hover Click</gui> window will open, and will stay above all of
  your other windows. You can use this to choose what sort of click should
  happen when you hover. For example, if you select <gui>Secondary Click</gui>,
  you will right-click when you hover. After you double-click, right-click, or
  drag, you will be automatically returned to clicking.</p>

  <p>وقتی نشانگر ماوس خود را روی دکمه قرار دهید و آن را حرکت ندهید، به تدریج تغییر رنگ می یابد. وقتی رنگ کاملاً تغییر کرد، روی دکمه کلیک می شود.</p>

  <p>تنظیم <gui>تأخیر</gui> را اصلاح کنید تا قبل از کلیک کردن، مدت زمان نگه داشتن نشانگر ماوس را تغییر دهید.</p>

  <p>You do not need to hold the mouse perfectly still when hovering to click.
  The pointer is allowed to move a little bit and will still click after a
  while. If it moves too much, however, the click will not happen.</p>

  <p>تنظیم <gui>آستانه حرکت</gui> را تنظیم کنید تا میزان اشاره گر را تغییر دهد و همچنان معلق باشد.</p>

</page>
