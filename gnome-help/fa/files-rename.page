<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="fa">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-rename-multiple"/>
    <link type="seealso" xref="files-rename-music-metadata"/>

    <revision version="gnome:47" date="2024-08-31" status="candidate"/>

    <credit type="author">
      <name>پروژهٔ مستندسازی گنوم</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>جیم کمپبل</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>دیوید کینگ</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>تغییر نام پرونده یا شاخه.</desc>
  </info>

  <title>تغییر نام یک پرونده یا شاخه</title>

  <p>As with other file managers, you can use <app>Files</app> to change the
  name of a file or folder.</p>

  <steps>
    <title>برای تغییر نام یک پرونده یا شاخه:</title>
    <item><p>Right-click on the item and select <gui>Rename</gui>, or select
    the file and press <key>F2</key>.</p></item>
    <item><p>Type the new name and press <key>Enter</key> or click
    <gui>Rename</gui>.</p></item>
  </steps>

  <p>When you rename a file, only the first part of the name of the file is
  selected, not the file extension (the part after the last <file>.</file>).
  The extension normally denotes what type of file it is (for example,
  <file>file.pdf</file> is a PDF document), and you usually do not want to
  change that. If you need to change the extension as well, select the entire
  file name and change it.</p>

  <note style="tip">
    <p>اگر نام پرونده‌ای اشتباهی یا نام پرونده را به اشتباه تغییر دادید، می‌توانید برش گردانید. برای بازگرداندن کنش، بلافاصله دکمهٔ فهرست را در نوار کناری زده و <gui>برگردان تغییر نام</gui> را گزیده یا برای برگرداندن نام پیشین <keyseq><key>مهار</key><key>Z</key></keyseq> را بزنید.</p>
  </note>

  <section id="valid-chars">
    <title>نویسه‌ّای معتبر برای نام‌های پرونده</title>

    <p>You can use any character except the <file>/</file> (slash) character in
    file names. Some devices, however, use a <em>file system</em> that has more
    restrictions on file names. Therefore, it is a best practice to avoid the
    following characters in your file names: <file>|</file>, <file>\</file>,
    <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>,
    <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>If you name a file with a <file>.</file> as the first character, the
    file will be <link xref="files-hidden">hidden</link> when you attempt to
    view it in the file manager.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>مشکلات مرسوم</title>

    <terms>
      <item>
        <title>نام پرونده از پیش در حال استفاده است</title>
        <p>You cannot have two files or folders with the same name in the same
        folder. If you try to rename a file to a name that already exists in
        the folder you are working in, the file manager will not allow it.</p>
        <p>File and folder names are case sensitive, so the file name
        <file>File.txt</file> is not the same as <file>FILE.txt</file>.
        Using different file names like this is allowed, though it is not
        recommended.</p>
      </item>
      <item>
        <title>نام پرونده بیش از حد طولانی است</title>
        <p>On some file systems, file names can have no more than 255
        characters in their names.  This 255 character limit includes both the
        file name and the path to the file (for example,
        <file>/home/wanda/Documents/work/business-proposals/…</file>), so you
        should avoid long file and folder names where possible.</p>
      </item>
      <item>
        <title>گزینهٔ تغییر نام خاکستری شده</title>
        <p>If <gui>Rename</gui> is grayed out, you do not have permission to
        rename the file. You should use caution with renaming such files, as
        renaming some protected files may cause your system to become unstable.
        See <link xref="nautilus-file-properties-permissions"/> for more
        information.</p>
      </item>
    </terms>

  </section>

</page>
