<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="fa">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>پیتر کوار</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>تغییر زمان سیاه شدن صفحه برای ذخیرهٔ نیرو.</desc>
  </info>

  <title>تنظیم زمان سیاه شدن صفحه</title>

  <p>To save power, you can adjust the time before the screen blanks when left
  idle. You can also disable the blanking completely.</p>

  <steps>
    <title>برای تنظیم زمان سیاه شدن صفحه:</title>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>نیرو</gui> کنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو <gui>نیرو</gui> را بزنید.</p>
    </item>
    <item>
      <p>Use the <gui>Screen Blank</gui> drop-down list under <gui>Power Saving
      Options</gui> to set the time until the screen blanks, or disable the blanking
      completely.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>When your computer is left idle, the screen will automatically lock
    itself for security reasons. To change this behavior, see
    <link xref="session-screenlocks"/>.</p>
  </note>

</page>
