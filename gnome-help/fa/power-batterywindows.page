<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="fa">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>سیخونک‌های سازنده و گذشت عمر باتری می‌تواند دلیل این مشکل باشد.</desc>
    <credit type="author">
      <name>پروژهٔ مستندسازی گنوم</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>فیل بول</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>چرا باتری کم‌تری نسبت به ویندوز یا مک دارم؟</title>

<p>Some computers appear to have a shorter battery life when running on Linux
than they do when running Windows or Mac OS. One reason for this is that
computer vendors install special software for Windows/Mac OS that optimizes
various hardware/software settings for a given model of computer. These tweaks
are often highly specific, and may not be documented, so including them in
Linux is difficult.</p>

<p>Unfortunately, there is not an easy way of applying these tweaks yourself
without knowing exactly what they are. You may find that using some <link xref="power-batterylife">power-saving methods</link> helps, though. If your
computer has a <link xref="power-batteryslow">variable-speed processor</link>,
you might find that changing its settings is also useful.</p>

<p>Another possible reason for the discrepancy is that the method of estimating
battery life is different on Windows/Mac OS than on Linux. The actual battery
life could be exactly the same, but the different methods give different
estimates.</p>
	
</page>
