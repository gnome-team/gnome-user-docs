<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="fa">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>پروژهٔ مستندسازی گنوم</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>افزودن عکستان به صفحه‌های کاربری و ورود.</desc>
  </info>

  <title>تغییر عکس صفحهٔ ورودتان</title>

  <p>When you log in or switch users, you will see a list of users with
  their login photos. You can change your photo to a stock image or an
  image of your own. You can even take a new login photo with your
  webcam.</p>

  <p>برای ویرایش حساب‌های کاربری‌ای جز خودتان نیاز به <link xref="user-admin-explain">امتیازهای مدیریتی</link> دارید.</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>سامانه</gui> کنید.</p>
    </item>
    <item>
      <p>از نتیجه‌ها <guiseq><gui>تنظمیات</gui><gui>سامانه</gui></guiseq> را برگزینید. این کار تابلوی <gui>سامانه</gui> را خواهد گشود.</p>
    </item>
    <item>
      <p>برای گشودن تابلو <gui>کاربران</gui> را برگزینید.</p>
    </item>
    <item>
      <p>If you want to edit a user other than yourself, press
      <gui style="button">Unlock</gui> in the top right corner and type in your
      password when prompted. Choose the user from <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>Click the pencil icon next to your name. A drop-down gallery will be
      shown with some stock login photos. If you like one of them, click it to
      use it for yourself.</p>
      <list>
        <item>
          <p>If you would rather use a picture you already have on your
          computer, click <gui>Select a file…</gui>.</p>
        </item>
        <item>
          <p>If you have a webcam, you can take a new login photo right now by
          clicking <gui>Take a picture…</gui>. Take your
          picture, then move and resize the square outline to crop out the
          parts you do not want. If you do not like the picture you took, click
          <gui style="button">Take Another Picture</gui> to try again, or
          <gui>Cancel</gui> to give up.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
