<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="fa">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>پروژهٔ مستندسازی گنوم</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>شوبها تیاگی</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>برنامه‌ها را از نمای کلی <gui>فعّالیت‌ها</gui> اجرا کنید.</desc>
  </info>

  <title>اجرای برنامه‌ها</title>

  <p if:test="!platform:gnome-classic">برای نمایش نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui>، نشانگر موشیتان را به گوشهٔ <gui>فعّالیت‌ها</gui> در بالای سمت راست صفحه ببرید. این‌جا، جایی است که می‌توانید تمامی برنامه‌هایتان را بیابید. همچنین می‌توانید نمای کلی را با فشردن کلید <key xref="keyboard-key-super">سوپر</key> بگشایید.</p>
  
  <p if:test="platform:gnome-classic">You can start applications from the
  <gui xref="shell-introduction#activities">Applications</gui> menu at the top
  left of the screen, or you can use the <gui>Activities</gui> overview by
  pressing the <key xref="keyboard-key-super">Super</key> key.</p>

  <p>هنگامی که در نمای کلی <gui>فعّالیت‌ها</gui> هستید، چندین راه برای گشودن یک برنامه وجود دارد:</p>

  <list>
    <item>
      <p>شروع به نوشتن نام یک برنامه کنید — جست‌وجو بی‌درنگ آغاز می‌شود. (اگر چنین نشد، روی نوار جست‌وجو در بالای صفحه کلیک کرده و شروع به نوشتن کنید.) اگر نام دقیق یک برنامه را نمی‌دانید، عبارت مربوطی را بنویسید. برای شروع برنامه، روی نقشکش کلیک کنید.</p>
    </item>
    <item>
      <p>برخی برنامه‌ها، نقشک‌هایی در <em>دش</em>، نوار افقی نقشک‌ها در پایین نمای کلی <gui>فعّالیت‌ها</gui> دارند. برای گشودنشان، روی نقشک مربوطه کلیک کنید.</p>
      <p>اگر برنامه‌ای دارید که زیاد استفاده‌اش می‌کنید، خودتان می‌توانید <link xref="shell-apps-favorites">به دش بیفزاییدش</link>.</p>
    </item>
    <item>
      <p>روی دکمهٔ شبکه (که نه نقطه دارد) در دش کلیک کنید. نخستین صفحهٔ برنامه‌های نصب شده را خواهید دید. برای دیدن برنامه‌های بیش‌تر، نقطه‌ها را در پایین، بالای دش بزنید. برای شروع یک برنامه، رویش بزنید.</p>
    </item>
    <item>
      <p>می‌توانید با کشیدن نقشک برنامه از دش و انداختنش روش یکی از <link xref="shell-workspaces">فضاهای کاری</link>، آن برنامه را در فضای کاری گزیده اجرا کنید.</p>
      <p>می‌توانید با کشیدن نقشک برنامه به یک فضای کاری خالی یا فاصلهٔ کوچک میان دو فضای کاری، برنامه را در یک فضای کاری <em>جدید</em> اجرا کنید.</p>
    </item>
  </list>

  <note style="tip">
    <title>اجرای سریع یک فرمان</title>
    <p>راهی دیگر برای اجرای یک برنامه، فشردن <keyseq><key>دگرساز</key><key>F2</key></keyseq>، ورود <em>نام فرمانش</em> و سپس زدن کلید <key>ورود</key> است.</p>
    <p>مثلاً برای اجرای <app>ریتم‌باکس</app>، <keyseq><key>دگرساز</key><key>F2</key></keyseq> را زده و <cmd>rhythmbox</cmd> را بنویسید. نام کاره، دستور اجرای برنامه است.</p>
    <p>برای دسترسی سریع به دستورهای اجرا شدهٔ پیشین، از کلیدهای جهت استفاده کنید.</p>
  </note>

</page>
