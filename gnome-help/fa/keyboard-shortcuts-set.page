<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="fa">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>جولیتا اینکا</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>جوانجو مارین</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>شوبها تیاگی</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>آندره کلپر</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>تعریف یا تغییر میان‌برهای صفحه‌کلید در تنظیمات <gui>صفحه‌کلید</gui>.</desc>
  </info>

  <title>تنظیم میان‌برهای صفحه‌کلید</title>

<p>To change the key or keys to be pressed for a keyboard shortcut:</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>تنظمیات</gui> کنید.</p>
    </item>
    <item>
      <p>روی <gui>تنظیمات</gui> بزنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو <gui>صفحه‌کلید</gui> را در نوار کناری بزنید.</p>
    </item>
    <item>
      <p>در بخش <gui>میان‌برهای صفحه‌کلید</gui>، <gui>تدیدن و سفارشی سازی میان‌برها</gui> را برگزینید.</p>
    </item>
    <item>
      <p>Select the desired category, or enter a search term.</p>
    </item>
    <item>
      <p>برای اقدام مورد نظر روی ردیف کلیک کنید. پنجره <gui>تنظیم میانبر</gui> پنجره نشان داده خواهد شد.</p>
    </item>
    <item>
      <p>Hold down the desired key combination, or press <key>Backspace</key> to
      reset, or press <key>Esc</key> to cancel.</p>
    </item>
  </steps>


<section id="defined">
<title>میان‌برهای ازپیش تعریف‌شده</title>
  <p>There are a number of pre-configured shortcuts that can be changed,
  grouped into these categories:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>دسترسی‌پذیری</title>
  <tr>
	<td><p>کاهش اندازهٔ متن</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>روشن یا خاموش کردن سایه‌روشن بالا</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>افزایش اندازهٔ متن</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>روشن یا خاموش کردن صفحه‌کلید مجازی</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>خاموش یا روشن کردن صفحه‌نمایش‌خوان</p></td>
	<td><p><keyseq><key>دگرساز</key><key>سوپر</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>خاموش یا روشن کردن بزرگنمایی</p></td>
	<td><p><keyseq><key>دگرساز</key><key>سوپر</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>بزرگ‌نمایی به داخل</p></td>
  <td><p><keyseq><key>دگرساز</key><key>سوپر</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>بزرگ‌نمایی به خارج</p></td>
  <td><p><keyseq><key>دگرساز</key><key>سوپر</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>اجراگرها</title>
  <tr>
	<td><p>شاخهٔ خانگی</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> یا <key>کشف</key></p></td>
  </tr>
  <tr>
	<td><p>راه‌اندازی ماشین حساب</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> یا <key>ماشین‌حساب</key></p></td>
  </tr>
  <tr>
	<td><p>راه‌اندازی کارخواه رایانامه</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> یا <key>نامه</key></p></td>
  </tr>
  <tr>
	<td><p>راه‌اندازی مرورگر راهنما</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>راه‌اندازی مرورگر وب</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> یا <key>وب</key></p></td>
  </tr>
  <tr>
	<td><p>جست‌وجو</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> یا <key>جست‌وجو</key></p></td>
  </tr>
  <tr>
	<td><p>تنظیمات</p></td>
	<td><p><key>ابزارها</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>ناوبری</title>
  <tr>
	<td><p>نهفتن تمام پنجره‌های عادی</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>انتقال به فضای کاری چپ</p></td>
	<td><p><keyseq><key>سوپر</key><key>صفحه بالا</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال به فضای کاری راست</p></td>
	<td><p><keyseq><key>سوپر</key><key>صفحه پایین</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به یک نمایشگر پایین</p></td>
	<td><p><keyseq><key>تبدیل</key><key xref="keyboard-key-super">سوپر</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به یک نمایشگر چپ</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به یک نمایشگر راست</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به یک نمایشگر بالا</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به یک فضای کاری در چپ</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key> <key>صفحه بالا</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به یک فضای کاری در راست</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>صفحه پایین</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به واپسین فضای کاری</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>پایان</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به فضای کاری ۱</p></td>
	<td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>خانه</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به فضای کاری ۲</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به فضای کاری ۳</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>انتقال پنجره به فضای کاری ۴</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>تعویض برنامه‌ها</p></td>
	<td><p><keyseq><key>سوپر</key><key>جهش</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض واپایش‌های سامانه</p></td>
	<td><p><keyseq><key>مهار</key><key>دگرساز</key><key>جهش</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض مستقیم واپایش‌های سامانه</p></td>
	<td><p><keyseq><key>مهار</key><key>دگرساز</key><key>گریز</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض به واپسین فضای کاری</p></td>
	<td><p><keyseq><key>سوپر</key><key>پایان</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض به فضای کاری ۱</p></td>
	<td><p><keyseq><key>سوپر</key><key>خانه</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض به فضای کاری ۲</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>تعویض به فضای کاری ۳</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>تعویض به فضای کاری ۴</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
        <td><p>تعویض پنجره‌ها</p></td>
        <td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>تعویض مستقیم پنجره‌ها</p></td>
	<td><p><keyseq><key>دگرساز</key><key>گریز</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض مستقیم پنجره‌های یک کاره</p></td>
	<td><p><keyseq><key>دگرساز</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تعویض پنجره‌های یک برنامه</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>نماگرفت‌ها</title>
  <tr>
	<td><p>ذخیرهٔ نماگرفتی از یک پنجره</p></td>
	<td><p><keyseq><key>دگرساز</key><key>نماگرفت</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>ذخیرهٔ نماگرفتی از یک ناحیه</p></td>
	<td><p><keyseq><key>تبدیل</key><key>نماگرفت</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>اجرای ابزار نماگرفت</p></td>
	<td><p><key>نماگرفت</key></p></td>
  </tr>
  <tr>
        <td><p>ضبط فیلم از نمایشگر</p></td>
        <td><p><keyseq><key>تبدیل</key><key>مهار</key><key>دگرساز</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>صدا و رسانه</title>
  <tr>
	<td><p>بیرون دادن</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (بیرون دادن)</p></td>
  </tr>
  <tr>
	<td><p>اجرای پخش‌کنندهٔ رسانه</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (رسانهٔ صوتی)</p></td>
  </tr>
  <tr>
	<td><p>سکوت/ناسکوت میکروفون</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>قطعهٔ بعدی</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (صدای بعدی)</p></td>
  </tr>
  <tr>
	<td><p>مکث پخش</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (مکث صدا)</p></td>
  </tr>
  <tr>
	<td><p>پخش (یا پخش/مکث)‏</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (پخش صدا)</p></td>
  </tr>
  <tr>
	<td><p>قطعهٔ پیشین</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (صدای پیشین)</p></td>
  </tr>
  <tr>
	<td><p>توقّف پخش</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (توقّف پخش)</p></td>
  </tr>
  <tr>
	<td><p>کاهش حجم صدا</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (کاهش حجم صدا)</p></td>
  </tr>
  <tr>
	<td><p>بی‌صدا کردن</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (بی‌صدا)</p></td>
  </tr>
  <tr>
	<td><p>افزایش حجم صدا</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (افزایش حجم صدا)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>سامانه</title>
  <tr>
        <td><p>تمرکز روی آگاهی فعال</p></td>
        <td><p><keyseq><key>سوپر</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>قفل کردن صفحه</p></td>
	<td><p><keyseq><key>سوپر</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>نمایش گفت‌وگوی خاموش کردن</p></td>
	<td><p><keyseq><key>مهار</key><key>دگرساز</key><key>حذف</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>بازگردانی میان‌برهای صفحه‌کلید</p></td>
        <td><p><keyseq><key>سوپر</key><key>گریز</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>نمایش همهٔ برنامه‌ها</p></td>
        <td><p><keyseq><key>سوپر</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>نمایش نمای کلی فعّالیت‌ها</p></td>
	<td><p><key>سوپر</key></p></td>
  </tr>
  <tr>
	<td><p>نمایش سیاههٔ آگاهی‌ها</p></td>
	<td><p><keyseq><key>سوپر</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>نمایش فهرست سامانه</p></td>
	<td><p><keyseq><key>سوپر</key><key>s</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>نمایش اعلان اجرای دستور</p></td>
	<td><p><keyseq><key>دگرساز</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>نوشتن</title>
  <tr>
  <td><p>تعویض به منبع ورودی بعدی</p></td>
  <td><p><keyseq><key>سوپر</key><key>فاصله</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>تعویض به منبع ورودی پیشین</p></td>
  <td><p><keyseq><key>تبدیل</key><key>سوپر</key><key>فاصله</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>پنجره‌‌ها</title>
  <tr>
	<td><p>فعّال‌سازی فهرست پنجره</p></td>
	<td><p><keyseq><key>Alt</key><key>Space</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>بستن پنجره</p></td>
	<td><p><keyseq><key>دگرساز</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>نهفتن پنجره</p></td>
        <td><p><keyseq><key>سوپر</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>بردن پنجره به زیر دیگر پنجره‌ها</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>بیشینه کردن پنجره</p></td>
	<td><p><keyseq><key>سوپر</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>بیشینه کردن افقی پنجره</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>بیشینه کردن عمودی پنجره</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>جابه‌جایی پنجره</p></td>
	<td><p><keyseq><key>دگرساز</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>آوردن پنجره به بالای دیگر پنجره‌ها</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>بالا آوردن در صورت پوشیده بودن، وگرنه پایین بردن</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>تغییر اندازهٔ پنجره</p></td>
	<td><p><keyseq><key>دگرساز</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>بازگرداندن پنجره</p></td>
        <td><p><keyseq><key>سوپر</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تغییر وضعیت حالت تمام‌صفحه</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
	<td><p>تغییر وضعیت بیشینگی</p></td>
	<td><p><keyseq><key>دگرساز</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>تغییر وضعیت پنرجه روی همهٔ فضاهای کاری یا یکی</p></td>
	<td><p>از کار افتاده</p></td>
  </tr>
  <tr>
        <td><p>تقسیم نما به چپ</p></td>
        <td><p><keyseq><key>سوپر</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>تقسیم نما به راست</p></td>
        <td><p><keyseq><key>سوپر</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>میان‌برهای سفارشی</title>

  <p>To create your own application keyboard shortcut in the
  <gui>Keyboard</gui> settings:</p>

  <steps>
    <item>
      <p><gui>میان‌برهای سفارشی</gui> را برگزینید.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Type a <gui>Name</gui> to identify the shortcut, and a
      <gui>Command</gui> to run an application.
      For example, if you wanted the shortcut to open <app>Rhythmbox</app>, you
      could name it <input>Music</input> and use the <input>rhythmbox</input>
      command.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut…</gui> button. In the
      <gui>Add Custom Shortcut</gui> window, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>روی <gui>افزودن</gui> بزنید.</p>
    </item>
  </steps>

  <p>The command name that you type should be a valid system command. You can
  check that the command works by opening a Terminal and typing it in there.
  The command that opens an application cannot have the same name as the
  application itself.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the row of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
