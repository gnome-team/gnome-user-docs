<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="fa">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author copyright">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
      <years>۲۰۱۳</years>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>برپایی ورود خودکار برای زمان روشن کردن رایانه‌تان.</desc>
  </info>

  <title>ورود خودکار</title>

  <p>می توانید تنظیماتتان را عوض کنید تا هنگام روشن کردن رایانه‌تان به طور خودکار وارد حساب کاربریتان شوید:</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>سامانه</gui> کنید.</p>
    </item>
    <item>
      <p>از نتیجه‌ها <guiseq><gui>تنظمیات</gui><gui>سامانه</gui></guiseq> را برگزینید. این کار تابلوی <gui>سامانه</gui> را خواهد گشود.</p>
    </item>
    <item>
      <p>برای گشودن تابلو <gui>کاربران</gui> را برگزینید.</p>
    </item>
    <item>
      <p>در گوشهٔ بالای چپ <gui style="button">قفل‌گشایی</gui> را زده و هنگام اعلان گذرواژه‌تان را بنویسید.</p>
    </item>
    <item>
      <p>اگر می‌خواهید به طور خودکار وارد حسابی دیگر شوید حساب مورد نظر را زیر <gui>دیگر کاربران</gui> برگزینید.</p>
    </item>
    <item>
      <p>کلید <gui>ورود خودکار</gui> را روشن کنید.</p>
    </item>
  </steps>

  <p>دفعهٔ بعدی که رایانه‌تان را روشن می‌کنید، به طور خودکار وارد خواهید شد. اگر این گزینه را به کار بیندازید، نیازی به ورود گذرواژه برای ورود به حسابتان نخواهد بود که یعنی اگر کسی دیگر رایانه‌تان را روشن کند، می‌تواند به حساب و داده‌های شخصیتان از جمله تاریخچهٔ مرورگر و پرونده‌هایتان دسترسی داشته باشد.</p>

  <note>
    <p>اگر گونهٔ حسابتان <em>استاندارد</em> باشد نمی‌توانید این تنظیم را تغییر دهید. با مدیر سامانه‌تان که می‌تواند این تنظیم را برایتان تغییر دهد تماس بگیرید.</p>
  </note>

</page>
