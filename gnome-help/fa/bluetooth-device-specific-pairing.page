<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="fa">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>بستین نوکرا</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>چگونه دستگاهی خاص را با رایانه‌تان جفت کنید.</desc>
  </info>

  <title>دستورالعمل‌های جفت‌کردن برای افزاره‌های خاص</title>

  <p>Even if you manage to source the manual for a device, it might not contain
  enough information to make pairing possible. Here are details for a few common
  devices.</p>

  <terms>
    <item>
      <title>دسته‌های پلی‌استیشن ۵</title>
      <p>Holding the <media its:translate="no" type="image" mime="image/svg" src="figures/ps-create.svg">Create</media>
      button, press the <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media>
      button until the light bar is blinking to make it visible and pair it like any other
      Bluetooth device.</p>
    </item>
    <item>
      <title>دسته‌های پلی‌استیشن ۴</title>
      <p>Using the <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media>
      and “Share” button combination to pair the joypad can also be used to
      make the joypad visible and pair it like any other Bluetooth device.</p>
      <p>Those devices can also use “cable-pairing”. Plug the joypads in via USB with the
      <gui>Bluetooth Settings</gui> opened, and Bluetooth turned on. You will get asked whether
      to set those joypads up without needing to press the
      <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media>
      button. Unplug them and press the
      <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media>
      to use them over Bluetooth.</p>
    </item>
    <item>
      <title>دسته‌های پلی‌استیشن ۳</title>
      <p>Those devices use “cable-pairing”. Plug the joypads in via USB with the
      <gui>Bluetooth Settings</gui> opened, and Bluetooth turned on. After pressing the
      <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media>
      button, you will get asked whether to set those joypads up. Unplug them and
      <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media>
      button to use them over Bluetooth.</p>
    </item>
    <item>
      <title>کنترل از راه دور BD پلی‌استیشن ۳</title>
      <p>Hold the “Start” and “Enter” buttons at the same time for around 5 seconds.
      You can then select the remote in the devices list as usual.</p>
    </item>
    <item>
      <title>کنترل‌های وی و وی‌یوی نینتندو</title>
      <p>Use the red “Sync” button inside the battery compartment to start the pairing
      process. Other button combinations will not keep pairing information, so you would
      need to do it all over again in short order. Also note that some software like console
      emulators will want direct access to the remotes, and, in those cases, you should
      not set them up in the Bluetooth panel. Refer to the application’s manual for
      instructions.</p>
    </item>
    <item>
      <title>ION iCade</title>
      <p>Hold down the bottom 4 buttons and the top white button to start the pairing
      process. When the pairing instructions appear, make sure to use only the cardinal
      directions to input the code, followed by any of the 2 white buttons to the far
      right of the arcade stick to confirm.</p>
    </item>
  </terms>

</page>
