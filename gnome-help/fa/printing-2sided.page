<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-2sided" xml:lang="fa">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>فیل بول</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>جیم کمپبل</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>چاپ روی هر دو سمت کاغذ یا چندین صفحه در ورق.</desc>
  </info>

  <title>چینش‌هیا چاپ دوطرفه یا چندصفحه‌ای</title>

  <p>To print on both sides of each sheet of paper:</p>

  <steps>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Go to the <gui>Page Setup</gui> tab of the Print window and choose an
      option from the <gui>Two-sided</gui> drop-down list. If the option is
      disabled, two-sided printing is not available for your printer.</p>
      <p>Printers handle two-sided printing in different ways. It is a good
      idea to experiment with your printer to see how it works.</p>
    </item>
    <item>
      <p>You can print more than one page of the document per <em>side</em> of
      paper too. Use the <gui>Pages per side</gui> option to do this.</p>
    </item>
  </steps>

  <note>
    <p>The availability of these options may depend on the type of printer you
    have, as well as the application you are using. This option may not always
    be available.</p>
  </note>

</page>
