<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="id">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Periksa apakah sistem berkas rusak dan membawanya kembali ke keadaan yang dapat digunakan.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Memperbaiki sistem berkas yang rusak</title>

  <p>Sistem berkas dapat rusak karena hilangnya daya yang tak terduga, sistem crash dan, pencabutan drive yang tidak aman. Setelah insiden seperti itu dianjurkan untuk <em>memperbaiki</em> atau setidaknya <em>memeriksa</em> sistem berkas untuk mencegah hilangnya data di masa depan.</p>
  <p>Terkadang perbaikan diperlukan untuk me-mount atau memodifikasi sistem berkas. Bahkan jika <em>pemeriksaan</em> tidak melaporkan kerusakan sistem berkas mungkin masih ditandai sebagai 'kotor' secara internal dan memerlukan perbaikan.</p>

<steps>
  <title>Memeriksa apakah suatu sistem berkas rusak</title>
  <item>
    <p>Buka <app>Pengaturan</app> dari ringkasan <gui>Aktivitas</gui>.</p>
  </item>
  <item>
    <p>Pilih disk yang berisi sistem berkas yang dimaksud dari daftar perangkat penyimpanan di sebelah kiri. Jika ada lebih dari satu volume pada disk, pilih volume yang berisi sistem berkas.</p>
  </item>
  <item>
    <p>Pada bilah alat di bawah bagian <gui>Volume</gui>, klik tombol menu. Lalu klik <gui>Periksa Sistem Berkas…</gui>.</p>
  </item>
  <item>
    <p>Tergantung pada berapa banyak data yang disimpan dalam sistem berkas, pemeriksaan mungkin memakan waktu lebih lama. Konfirmasikan untuk memulai tindakan dalam dialog yang muncul.</p>
   <p>Tindakan tidak akan memodifikasi sistem berkas tetapi akan melepas kait jika diperlukan. Bersabarlah sementara sistem berkas diperiksa.</p>
  </item>
  <item>
    <p>Setelah selesai Anda akan diberitahu apakah sistem berkas rusak. Perhatikan bahwa dalam beberapa kasus, bahkan bila sistem berkas tidak rusak, itu masih mungkin perlu diperbaiki untuk mengatur ulang penanda 'kotor' internal.</p>
  </item>
</steps>

<note style="warning">
 <title>Kemungkinan hilangnya data saat memperbaiki</title>
  <p>Jika struktur sistem berkas rusak itu dapat mempengaruhi berkas yang tersimpan di dalamnya. Dalam beberapa kasus berkas ini tidak dapat dibawa ke dalam bentuk yang valid lagi dan akan dihapus atau dipindahkan ke direktori khusus. Biasanya folder <em>lost+found</em> di direktori tingkat puncak sistem berkas dimana bagian-bagian berkas yang dipulihkan ini dapat ditemukan.</p>
  <p>Jika data terlalu berharga untuk hilang selama proses ini, Anda disarankan untuk mencadangkan dengan menyimpan image volume sebelum diperbaiki.</p>
  <p>Image ini dapat kemudian diproses dengan alat analisis forensik seperti <app>sleuthkit</app> untuk lebih jauh memulihkan berkas yang hilang dan bagian data yang tidak dipulihkan selama perbaikan, dan juga berkas yang sebelumnya dihapus.</p>
</note>

<steps>
  <title>Memperbaiki sistem berkas</title>
  <item>
    <p>Buka <app>Pengaturan</app> dari ringkasan <gui>Aktivitas</gui>.</p>
  </item>
  <item>
    <p>Pilih disk yang berisi sistem berkas yang dimaksud dari daftar perangkat penyimpanan di sebelah kiri. Jika ada lebih dari satu volume pada disk, pilih volume yang berisi sistem berkas.</p>
  </item>
  <item>
    <p>Pada bilah alat di bawah bagian <gui>Volume</gui>, klik tombol menu. Lalu klik <gui>Perbaiki Sistem Berkas…</gui>.</p>
  </item>
  <item>
    <p>Bergantung pada berapa banyak data yang disimpan dalam sistem berkas perbaikan mungkin memakan waktu lebih lama. Konfirmasikan untuk memulai tindakan dalam dialog yang muncul.</p>
   <p>Aksi akan melepas kait sistem berkas jika diperlukan. Tindakan perbaikan mencoba untuk membawa sistem berkas ke keadaan yang konsisten dan memindahkan berkas yang rusak dalam folder khusus. Bersabarlah sementara sistem berkas diperbaiki.</p>
  </item>
  <item>
    <p>Setelah selesai Anda akan diberitahu apakah sistem berkas bisa berhasil diperbaiki. Jika berhasil itu dapat digunakan lagi dengan cara yang normal.</p>
    <p>Jika sistem berkas tidak dapat diperbaiki, Cadangkan dengan menyimpan image volume untuk dapat mengambil berkas penting nanti. Hal ini dapat dilakukan dengan memasang image hanya-baca atau menggunakan alat analisis forensik seperti <app>sleuthkit</app>.</p>
    <p>Untuk memanfaatkan volume lagi itu harus <link xref="disk-format">diformat</link> dengan sistem berkas baru. Semua data akan dibuang.</p>
  </item>
</steps>

</page>
