<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="id">

  <info>
    <link type="guide" xref="media#music"/>
  
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Anda mungkin tidak menginstal kodek yang tepat, atau DVD mungkin punya wilayah yang salah.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Mengapa DVD tak bisa diputar?</title>

  <p>Bila Anda menyisipkan suatu DVD ke dalam komputer Anda dan itu tak bisa diputar, Anda mungkin tak memiliki <em>kodek</em> DVD yang benar, atau DVD mungkin dari <em>region</em> yang lain.</p>

<section id="codecs">
  <title>Memasang kodek yang benar untuk memutar DVD</title>

  <p>Untuk memutar DVD, Anda perlu punya <em>kodek</em> yang benar. Suatu kodek adalah sepenggal perangkat lunak yang memungkinkan aplikasi membaca format video atau audio. Bila perangkat lunak pemutar film Anda tak memiliki kodek yang benar, mungkin ia menawarkan pada Anda untuk memasangnya. Bila tidak, Anda mesti memasang kodek secara manual - mintalah bantuan tentang bagaimana melakukan ini, misalnya pada forum dukungan distribusi Linux Anda.</p>

  <p>DVD juga <em>dilindungi salin</em> menggunakan sistem yang disebut CSS. Ini mencegah Anda menyalin DVD, tetapi juga mencegah Anda memainkannya kecuali Anda memiliki perangkat lunak tambahan untuk menangani perlindungan salinan. Perangkat lunak ini tersedia dari sejumlah distribusi Linux, tetapi tidak dapat digunakan secara legal di semua negara. Harap hubungi distribusi Linux Anda untuk informasi lebih lanjut.</p>

  </section>

<section id="region">
  <title>Memeriksa wilayah DVD</title>

  <p>DVD memiliki <em>kode wilayah</em>, yang memberitahu Anda di wilayah mana dunia mereka diizinkan untuk dimainkan. Jika wilayah DVD player komputer Anda tidak cocok dengan wilayah DVD Anda mencoba untuk bermain, Anda tidak akan dapat memutar DVD. Misalnya, jika Anda memiliki pemutar DVD wilayah 1, Anda hanya akan diizinkan untuk memutar DVD dari Amerika Utara.</p>

  <p>Sering mungkin untuk mengubah wilayah yang digunakan oleh pemutar DVD Anda, tetapi hanya dapat dilakukan beberapa kali sebelum terkunci ke dalam satu wilayah secara permanen. Untuk mengubah wilayah DVD pemutar DVD komputer Anda, gunakan <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p>Anda dapat menemukan <link href="https://en.wikipedia.org/wiki/DVD_region_code">informasi lebih lanjut tentang kode wilayah DVD di Wikipedia</link>.</p>

</section>

</page>
