<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="id">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Berkas yang dihapus biasanya dikirim ke Tong Sampah, tapi dapat dipulihkan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Memulihkan berkas dari Tong Sampah</title>

  <p>Bila Anda menghapus berkas tanpa pengelola berkas, berkas biasanya ditempatkan ke dalam <gui>Tong Sampah</gui>, dan mestinya dapat dipulihkan.</p>

  <steps>
    <title>Untuk memulihkan berkas dari Tong Sampah:</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulailah mengetikkan <app>Berkas</app>.</p>
    </item>
    <item>
      <p>Klik pada <app>Berkas</app> untuk membuka manajer berkas.</p>
    </item>
    <item>
      <p>Klik <gui>Tong Sampah</gui> di bilah sisi. Jika Anda tidak melihat bilah sisi, tekan tombol <gui>Tampilkan Bilah Sisi</gui> menu di pojok kiri atas jendela.</p>
    </item>
    <item>
      <p>Bila berkas Anda yang terhapus ada, klik itu dan pilih <gui>Pulihkan Dari Tong Sampah</gui>. Itu akan dipulihkan ke folder tempat dia dihapus.</p>
    </item>
  </steps>

  <p>Jika Anda menghapus berkas dengan menekan <keyseq><key>Shift</key><key>Delete</key></keyseq>, atau dengan menggunakan baris perintah, berkas telah dihapus secara permanen. Berkasyang telah dihapus secara permanen tidak dapat dipulihkan dari <gui>Tong Sampah</gui>.</p>

  <p>Ada sejumlah alat pemulihan yang tersedia yang kadang-kadang dapat memulihkan berkas yang telah dihapus secara permanen. Namun alat-alat ini umumnya tidak sangat mudah digunakan. Jika Anda tanpa sengaja menghapus secara permanen berkas, mungkin lebih baik untuk meminta saran pada forum dukungan untuk melihat apakah Anda dapat memulihkannya.</p>

</page>
