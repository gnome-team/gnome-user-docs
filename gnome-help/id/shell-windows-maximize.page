<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="id">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Klik ganda atau seret bilah judul untuk memaksimumkan atau mengembalikan suatu jendela.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Maksimalkan dan tak memaksimalkan jendela</title>

  <p>Anda dapat memaksimalkan jendela untuk mengambil semua ruang pada desktop Anda dan tak-memaksimalkan jendela untuk mengembalikannya ke ukuran normal. Anda juga dapat memaksimalkan jendela secara vertikal di sepanjang sisi kiri dan kanan layar, sehingga Anda dapat dengan mudah melihat dua jendela sekaligus. Lihat <link xref="shell-windows-tiled"/> untuk rinciannya.</p>

  <p>Untuk memaksimalkan jendela, genggam bilah judul dan seret ke bagian atas layar, atau cukup klik ganda kali bilah judul. Untuk memaksimalkan jendela menggunakan papan tik, tahan tombol <key xref="keyboard-key-super">Super</key> dan tekan <key>↑</key>, atau tekan <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Anda juga dapat memaksimalkan jendela dengan mengklik tombol maksimalkan di bilah judul.</p>

  <p>Untuk mengembalikan jendela ke ukuran yang tidak dimaksimalkan, seret keluar dari tepi layar. Jika jendela dimaksimalkan sepenuhnya, Anda dapat mengklik ganda bilah judul untuk mengembalikannya. Anda juga dapat menggunakan pintasan papan tik yang sama yang Anda gunakan untuk memaksimalkan jendela.</p>

  <note style="tip">
    <p>Tahan tombol <key>Super</key> dan seret ke mana pun di jendela untuk memindahkannya.</p>
  </note>

</page>
