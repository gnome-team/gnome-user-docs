<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="id">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:45" date="2024-03-04" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Atur berkas berdasarkan nama, ukuran, jenis, atau kapan mereka diubah.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Urutkan berkas dan folder</title>

<p>Anda dapat mengurutkan berkas dalam cara yang berbeda dalam sebuah folder, misalnya dengan menyortir mereka dalam urutan tanggal atau ukuran berkas. Lihat <link xref="#ways"/> di bawah untuk daftar cara umum mengurutkan berkas. Lihat <link xref="nautilus-views"/> untuk informasi tentang cara mengubah urutan baku.</p>

<p>Cara Anda dapat mengurutkan berkas tergantung pada <em>tampilan folder</em> yang Anda gunakan. Anda dapat mengubah tampilan saat ini menggunakan tombol daftar/kisi di bilah alat.</p>

<section id="icon-view">
  <title>Tilikan kisi</title>

  <p>Untuk mengurutkan berkas dalam urutan yang berbeda, klik tombol opsi tampilan di bilah alat dan pilih <gui>A-Z</gui>, <gui>Z-A</gui>, <gui>Terakhir Berubah</gui>, <gui>Pertama Berubah</gui>, <gui>Ukuran</gui>, atau <gui>Tipe</gui>.</p>

  <p>Sebagai contoh, jika Anda memilih <gui>A-Z</gui>, berkas akan diurutkan berdasarkan nama mereka, dalam urutan abjad. Lihat <link xref="#ways"/> untuk pilihan lainnya.</p>

</section>

<section id="list-view">
  <title>Tilikan daftar</title>

  <p>Untuk mengurutkan berkas dalam urutan yang berbeda, klik salah satu tajuk kolom di manajer berkas. Misalnya, klik <gui>Tipe</gui> untuk mengurutkan berdasarkan tipe berkas. Klik tajuk kolom lagi untuk mengurutkan dalam urutan terbalik.</p>

  <p>Untuk opsi pengurutan lainnya, klik tombol opsi tampilan di bilah alat.</p>

  <p>Dalam tampilan daftar, Anda dapat menampilkan kolom dengan lebih banyak atribut dan mengurutkan pada kolom tersebut. Klik tombol opsi tampilan di bilah alat, pilih <gui>Kolom yang Terlihat…</gui> dan pilih kolom yang ingin Anda lihat. Anda kemudian akan dapat mengurutkan berdasarkan kolom tersebut. Lihat <link xref="nautilus-list"/> untuk deskripsi kolom yang tersedia.</p>

</section>

<section id="ways">
  <title>Cara mengurutkan berkas</title>

  <terms>
    <item>
      <title>A-Z</title>
      <p>Mengurutkan berkas secara alfabet berdasarkan nama berkas.</p>
    </item>
    <item>
      <title>Z-A</title>
      <p>Mengurut secara alfabet berdasarkan nama berkas dalam urutan terbalik.</p>
    </item>
    <item>
      <title>Terakhir Diubah</title>
      <p>Mengurut berdasarkan tanggal dan waktu berkas tersebut terakhir berubah. Mengurutkan dari paling lama sampai terbaru.</p>
    </item>
    <item>
      <title>Pertama Diubah</title>
      <p>Mengurut berdasarkan tanggal dan waktu berkas tersebut terakhir berubah. Mengurutkan dari paling baru sampai lama.</p>
    </item>
    <item>
      <title>Ukuran</title>
      <p>Mengurut berdasarkan ukuran berkas (berapa banyak ruang disk yang dia pakai). Urutan baku dari terkecil sampai terbesar.</p>
    </item>
    <item>
      <title>Tipe</title>
      <p>Mengurut secara alfabetis menurut tipe berkas. Berkas-berkas dengan tipe sama dikelompokkan menjadi satu, lalu diurut berdasarkan nama.</p>
    </item>
  </terms>

</section>

</page>
