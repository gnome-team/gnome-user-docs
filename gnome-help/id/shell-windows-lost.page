<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="id">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Periksa ringkasan <gui>Aktivitas</gui> atau ruang kerja lain.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Menemukan jendela yang hilang</title>

  <p>Suatu jendela pada ruang kerja lain, atau tersembunyi di balik jendela lain, mudah ditemukan memakai ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui>:</p>

  <list>
    <item>
      <p>Buka ringkasan <gui>Aktivitas</gui>. Jika jendela yang hilang ada di <link xref="shell-windows#working-with-workspaces">ruang kerja</link> saat ini, itu akan ditampilkan di sini dalam gambar mini. Cukup klik gambar mini untuk menampilkan kembali jendela, atau</p>
    </item>
    <item>
      <p>Klik ruang kerja yang berbeda di <link xref="shell-workspaces">pemilih ruang kerja</link> untuk mencoba menemukan jendela Anda, atau</p>
    </item>
    <item>
      <p>Klik kanan aplikasi di dasbor dan jendela yang terbuka akan dicantumkan. Klik jendela dalam daftar untuk beralih ke sana.</p>
    </item>
  </list>

  <p>Memakai penukar jendela:</p>

  <list>
    <item>
      <p>Tekan <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key> </keyseq> untuk menampilkan <link xref="shell-windows-switching">pengalih jendela</link>. Terus tahan tombol <key>Super</key> dan tekan <key>Tab</key> untuk menelusuri jendela yang terbuka, atau <keyseq><key>Shift</key><key>Tab</key> </keyseq> untuk bermutar mundur.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Jika aplikasi memiliki beberapa jendela yang terbuka, tahan <key>Super</key> dan tekan <key>`</key> (atau tombol di atas <key>tab</key>) untuk melangkah melalui mereka.</p>
    </item>
  </list>

</page>
