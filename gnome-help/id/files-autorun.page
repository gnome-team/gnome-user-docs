<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="id">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Secara otomatis menjalankan aplikasi bagi CD dan DVD, kamera, pemutar audio, dan perangkat serta media lain.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Buat aplikasi untuk perangkat atau disc</title>

  <p>Anda dapat memiliki aplikasi secara otomatis dimulai ketika Anda pasang di perangkat atau memasukkan disk atau media card. Sebagai contoh, Anda mungkin ingin organizer foto Anda untuk memulai ketika Anda pasang di kamera digital. Anda juga dapat mematikan ini, sehingga tidak ada yang terjadi ketika Anda pasang sesuatu di.</p>

  <p>Untuk memutuskan aplikasi mana yang mesti mulai berjalan ketika Anda menancapkan berbagai perangkat:</p>

<steps>
  <item>
    <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Media Lepasan</gui>.</p>
  </item>
  <item>
    <p>Pilih <guiseq><gui>Pengaturan</gui><gui>Aplikasi</gui></guiseq> dari hasil. Ini akan membuka panel <gui>Aplikasi</gui>.</p>
  </item>
  <item>
    <p>Pilih <gui>Aplikasi Baku</gui> untuk membuka panel.</p>
  </item>
  <item>
    <p>Di bagian <gui>Media Lepasan</gui>, nyalakan <gui>Secara Otomatis Luncurkan Aplikasi</gui>.</p>
  </item>
  <item>
    <p>Temukan tipe media atau perangkat yang Anda inginkan, lalu pilih suatu aplikasi atau aksi bagi tipe media itu. Lihat di bawah bagi suatu penjelasan dari tipe-tipe media dan perangkat yang berbeda.</p>
    <p>Sebagai pengganti dari mulai menjalankan suatu aplikasi, Anda juga dapat menatanya sehingga perangkat akan ditampilkan dalam pengelola berkas, dengan opsi <gui>Buka folder</gui>. Ketika itu terjadi, Anda akan ditanyai apa yang akan dilakukan, atau tak ada apapun yang akan terjadi secara otomatis.</p>
  </item>
  <item>
    <p>Bila Anda tak melihat tipe media atau perangkat yang ingin Anda ubah dalam daftar (seperti misalnya cakram Blu-ray atau pembaca E-book), klik <gui>Media Lain</gui> untuk melihat suatu daftar perangkat yang lebih terperinci. Pilih tipe perangkat atau media dari tarik-turun <gui>Tipe</gui> dan aplikasi atau aksi dari tarik-turun <gui>Aksi</gui>.</p>
  </item>
</steps>

  <note style="tip">
    <p>Bila Anda tak ingin aplikasi mana pun dibuka secara otomatis, apa pun yang Anda tancapkan, matikan <gui>Secara Otomatis Luncurkan Aplikasi</gui>.</p>
  </note>

<section id="files-types-of-devices">
  <title>Tipe perangkat dan media</title>
<terms>
  <item>
    <title>Cakram audio</title>
    <p>Pilih aplikasi musik atau pengekstrak audio CD favorit Anda untuk menangani CD audio. Jika Anda menggunakan DVD audio (DVD-A), pilih cara untuk membuka mereka di bawah <gui>Media Lainnya</gui>. Jika Anda membuka cakram audio dengan manajer berkas, trek akan muncul sebagai berkas WAV yang dapat Anda mainkan di sebarang aplikasi pemutar audio.</p>
  </item>
  <item>
    <title>Cakram video</title>
    <p>Pilih aplikasi video favorit Anda untuk menangani DVD video. Memakai tombol <gui>Media Lainnya</gui> untuk mengatur aplikasi untuk Blu-ray, HD DVD, video CD (VCD), dan video CD super (SVCD). Jika DVD atau cakram video lainnya tidak bekerja dengan benar ketika Anda memasukkan mereka, lihat <link xref="video-dvd"/>.</p>
  </item>
  <item>
    <title>Cakram kosong</title>
    <p>Gunakan tombol <gui>Media Lainnya</gui> untuk memilih aplikasi penulisan disk bagi CD kosong, DVD kosong, Blu-ray kosong, dan DVD HD kosong.</p>
  </item>
  <item>
    <title>Kamera dan foto</title>
    <p>Memakai <gui>Foto</gui> tarik-turun untuk memilih aplikasi foto-manajemen untuk menjalankan ketika Anda pasang di kamera digital Anda, atau ketika Anda memasukkan kartu media dari kamera, seperti CF, SD, MMC, atau kartu MS. Anda juga dapat hanya menelusuri foto menggunakan berkas manager.</p>
    <p>Di bawah <gui>Media Lainnya</gui>, Anda dapat memilih aplikasi untuk membuka CD gambar Kodak, seperti yang Anda mungkin telah buat di suatu toko. Ini adalah CD data biasa dengan gambar JPEG dalam folder bernama <file>Pictures</file>.</p>
  </item>
  <item>
    <title>Pemutar musik</title>
    <p>Pilih suatu aplikasi untuk mengelola pustaka musik pada pemutar musik portabel Anda, atau kelola sendiri berkas-berkas memakai pengelola berkas.</p>
    </item>
    <item>
      <title>Pembaca e-book</title>
      <p>Gunakan tombol <gui>Media Lain</gui> untuk memilih aplikasi untuk mengelola buku pada pembaca e-book Anda, atau kelola sendiri berkas-berkas memakai manajer berkas.</p>
    </item>
    <item>
      <title>Perangkat Lunak</title>
      <p>Beberapa disk dan media yang dapat dilepas berisi perangkat lunak yang seharusnya dijalankan secara otomatis ketika media dimasukkan. Memakai pilihan <gui>Perangkat Lunak</gui> untuk mengontrol apa yang harus dilakukan ketika Media dengan perangkat lunak yang jalan otomatis dimasukkan. Anda selalu akan diminta untuk konfirmasi sebelum perangkat lunak dijalankan.</p>
      <note style="warning">
        <p>Jangan pernah jalankan perangkat lunak dari media yang tidak Anda percayai.</p>
      </note>
   </item>
</terms>

</section>

</page>
