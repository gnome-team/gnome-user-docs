<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="id">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Komputer biasanya menjadi hangat, tapi bila mereka menjadi terlalu panas mereka dapat menjadi kelebihan panas, yang dapat merusak.</desc>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Komputer saya terasa sangat panas</title>

<p>Kebanyakan komputer menjadi hangat setelah beberapa saat, dan beberapa bisa menjadi cukup panas. Ini normal: sudah menjadi kebiasaan bahwa komputer mendinginkan dirinya sendiri. Namun bila komputer menjadi terlalu hangat itu mungkin tanda bahwa terjadi kelebihan panas, yang berpotensi menyebabkan kerusakan.</p>

<p>Kebanyakan laptop menjadi cukup hangat setelah Anda telah menggunakan mereka beberapa waktu. Hal ini umumnya tidak perlu khawatir - komputer menghasilkan banyak panas dan laptop itu sangat kompak, sehingga mereka perlu untuk membuang panas mereka dengan cepat dan casing luar menghangat sebagai hasilnya. Namun beberapa laptop menjadi terlalu panas, dan mungkin tidak nyaman untuk digunakan. Hal ini biasanya hasil dari sistem pendinginan yang dirancang buruk. Anda terkadang bisa mendapatkan tambahan aksesoris pendingin yang sesuai dengan bagian bawah laptop dan memberikan pendinginan yang lebih efisien.</p>

<p>Jika Anda memiliki komputer desktop yang terasa panas untuk disentuh, mungkin tidak cukup pendinginan. Jika ini mengkhawatirkan Anda, Anda dapat membeli kipas pendingin tambahan atau memeriksa apakah kipas pendingin dan ventilasi bebas dari debu dan penyumbatan lainnya. Anda mungkin ingin mempertimbangkan menempatkan komputer di daerah berventilasi yang lebih baik juga - jika disimpan di ruang terkurung (misalnya, dalam lemari), sistem pendinginan di komputer mungkin tidak dapat membuang panas dan mengedarkan udara dingin secara cukup cepat.</p>

<p>Beberapa orang khawatir tentang risiko kesehatan menggunakan laptop panas. Ada saran bahwa berkepanjangan menggunakan laptop panas di pangkuan Anda mungkin mengurangi kesuburan, dan ada laporan luka bakar kecil yang diderita juga (dalam kasus ekstrim). Jika Anda khawatir tentang potensi masalah ini, Anda mungkin ingin berkonsultasi dengan praktisi medis untuk saran. Tentu saja, Anda dapat memilih untuk tidak meletakkan laptop di pangkuan Anda.</p>

<p>Sebagian besar komputer modern akan mati sendiri jika mereka terlalu panas, untuk mencegah menjadi rusak. Jika komputer Anda terus mati sendiri, ini mungkin alasannya. Jika komputer Anda terlalu panas, Anda mungkin akan perlu untuk memerbaikinya.</p>

</page>
