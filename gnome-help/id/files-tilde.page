<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="files-tilde" xml:lang="id">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="files-hidden"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:45" date="2024-03-04" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ini adalah berkas cadangan. Mereka secara baku tersembunyi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Apa itu berkas dengan <file>~</file> di akhir namanya?</title>

  <p>Berkas dengan <file>~</file> di akhir nama mereka (misalnya, <file>example.txt~</file>) secara otomatis dibuat salinan cadangan dari dokumen yang disunting di penyunting teks <app>gedit</app> atau aplikasi lainnya. Ini aman untuk dihapus, tetapi tidak ada salahnya untuk membiarkan mereka pada komputer Anda.</p>

  <p>Berkas ini tersembunyi secara baku. Jika Anda melihat mereka, itu karena Anda memilih <gui>Tampilkan Berkas Tersembunyi</gui> (dalam menu utama dari bilah sisi <app>Berkas</app>) atau menekan <keyseq><key>Ctrl</key><key>H</key></keyseq>. Anda dapat menyembunyikannya lagi dengan mengulangi salah satu langkah ini.</p>

  <p>Berkas-berkas ini diperlakukan dengan cara yang sama seperti berkas tersembunyi normal. Lihat <link xref="files-hidden"/> untuk saran tentang berurusan dengan berkas tersembunyi.</p>

</page>
