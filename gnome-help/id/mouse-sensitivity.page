<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="id">

  <info>
    <link type="guide" xref="mouse" group="#first"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-05" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit>
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengubah seberapa cepat penunjuk bergerak saat Anda menggunakan tetikus atau touchpad.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Menyetel kecepatan tetikus dan touchpad</title>

  <p>Jika penunjuk bergerak terlalu cepat atau lambat saat menggerakkan tetikus atau menggunakan touchpad, Anda dapat menyesuaikan kecepatan penunjuk untuk perangkat ini.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulailah mengetikkan <gui>Tetikus &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Tetikus &amp; Touchpad</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Sesuaikan penggeser <gui>Kecepatan Penunjuk</gui> hingga gerakan penunjuk nyaman bagi Anda. Terkadang pengaturan yang paling nyaman untuk satu jenis perangkat bukanlah yang terbaik untuk yang lain.</p>
    </item>
  </steps>

  <note>
    <p>Bagian <gui>Touchpad</gui> hanya muncul jika sistem Anda memiliki touchpad, sedangkan bagian <gui>Tetikus</gui> hanya terlihat ketika tetikus terhubung.</p>
  </note>

</page>
