<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="id">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="41" date="2021-09-08" status="final"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tip untuk mengurangi konsumsi daya komputer Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Memakai daya lebih sedikit dan meningkatkan umur baterai</title>

  <p>Komputer dapat menggunakan banyak daya. Dengan menggunakan beberapa strategi penghematan energi sederhana, Anda dapat mengurangi tagihan listrik Anda dan membantu lingkungan.</p>

<section id="general">
  <title>Tip umum</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Suspensikan komputer Anda.</link> ketika Anda tidak sedang memakainya. Ini mengurangi daya yang dipakainya secara signifikan, dan dia dapat dibangunkan kembali dengan sangat cepat.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Matikan</link> komputer ketika Anda tidak menggunakannya untuk perioda yang lebih panjang. Beberapa orang khawatir bahwa mematikan komputer secara reguler dapat menyebabkannya aus lebih cepat, tapi sebenarnya tidak.</p>
  </item>
  <item>
    <p>Memakai panel <gui>Daya</gui> dalam <app>Pengaturan</app> untuk mengubah pengaturan daya Anda. Ada sejumlah pilihan yang akan membantu untuk menghemat daya: Anda dapat <link xref="display-blank">secara otomatis mengosongkan layar</link> setelah waktu tertentu, memfungsikan mode <gui><link xref="power-profile">Penghemat Daya</link> Otomatis</gui> ketika baterai lemah, dan membuat komputer <link xref="power-autosuspend">secara otomatis disuspensi</link> jika Anda tidak menggunakannya untuk jangka waktu tertentu.</p>
  </item>
  <item>
    <p>Kurangi <link xref="display-brightness">kecerahan layar</link>.</p>
  </item>
  <item>
    <p>Matikan perangkat eksternal apa pun (seperti pencetak dan pemindai) bila Anda tidak menggunakannya.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Laptop, netbook, dan perangkat lain yang memakai baterai</title>

 <list>
   <item>
     <p>Kurangi <link xref="display-brightness">kecerahan layar</link>. Menyalakan layar mengkonsumsi sebagian daya laptop yang signifikan.</p>
     <p>Kebanyakan laptop memiliki tombol pada papan tik (atau pintasan papan tik) yang dapat Anda gunakan untuk mengurangi kecerahan.</p>
   </item>
   <item>
     <p>Jika Anda tidak memerlukan koneksi internet untuk sementara, <link xref="power-wireless">matikan nirkabel atau Bluetooth</link>. Perangkat ini bekerja dengan menyiarkan gelombang radio, yang cukup mengambil daya.</p>
     <p>Beberapa komputer memiliki saklar fisik yang dapat digunakan untuk mematikannya, sedangkan yang lain memiliki pintasan papan tik yang dapat Anda gunakan sebagai gantinya. Anda dapat mengaktifkannya lagi saat membutuhkannya.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Tips lanjutan lainnya</title>

 <list>
   <item>
     <p>Mengurangi jumlah tugas yang berjalan di latar belakang. Komputer menggunakan lebih banyak daya ketika mereka memiliki lebih banyak pekerjaan yang harus dilakukan.</p>
     <p>Sebagian besar aplikasi Anda yang berjalan melakukan sangat sedikit ketika Anda tidak aktif menggunakan mereka. Namun, aplikasi yang sering mengambil data dari internet atau memutar musik atau film dapat berdampak pada konsumsi daya Anda.</p>
   </item>
 </list>

</section>

</page>
