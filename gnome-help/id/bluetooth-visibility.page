<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="id">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>
    <link type="seealso" xref="bluetooth-device-specific-pairing"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Apakah perangkat lain dapat menemukan komputer Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Apa itu kenampakan Bluetooth?</title>

  <p>Kenampakan Bluetooth hanya mengacu pada apakah perangkat lain dapat menemukan komputer Anda ketika sedang mencari perangkat Bluetooth. Ketika Bluetooth dinyalakan dan panel <gui>Bluetooth</gui> terbuka, komputer Anda akan mengiklankan dirinya ke semua perangkat lain dalam jangkauan, yang memungkinkan mereka untuk berusaha melakukan koneksi ke Anda.</p>

  <note style="tip">
    <p>Anda dapat <link xref="sharing-displayname">mengubah</link> nama komputer Anda yang ditampilkan ke perangkat lain.</p>
  </note>

  <p>Setelah Anda <link xref="bluetooth-connect-device">tersambung ke suatu perangkat</link>, komputer Anda maupun perangkat tak perlu nampak untuk saling berkomunikasi.</p>

  <p>Perangkat tanpa layar biasanya memiliki modus penyandingan yang dapat dimasukkan dengan menekan sebuah tombol, atau kombinasi tombol untuk sementara, apakah ketika mereka sudah dihidupkan, atau saat mereka sedang dihidupkan.</p>

  <p>Cara terbaik untuk mengetahui cara memasukkan mode tersebut adalah dengan merujuk ke panduan perangkat. Untuk beberapa perangkat, prosedur mungkin <link xref="bluetooth-device-specific-pairing">sedikit berbeda dari biasanya</link>.</p>

</page>
