<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-timezone" xml:lang="id">

  <info>
    <link type="guide" xref="clock"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.16" date="2015-01-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Jim Campbell</name>
       <email>jcampbell@gnome.org</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mutakhirkan zona waktu Anda ke lokasi saat ini sehingga waktu Anda benar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Mengubah zona waktu Anda</title>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Sistem</gui>.</p>
    </item>
    <item>
      <p>Pilih <guiseq><gui>Pengaturan</gui><gui>Sistem</gui></guiseq> dari hasil. Ini akan membuka panel <gui>Sistem</gui>.</p>
    </item>
    <item>
      <p>Pilih <gui>Tanggal &amp; Waktu</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Bila saklar <gui>Zona Waktu Otomatis</gui> ditata ke menyala, zona waktu Anda mestinya dimutakhirkan secara otomatis bila Anda memiliki koneksi internet dan <link xref="privacy-location">fitur layanan lokasi</link> difungsikan. Untuk memutakhirkan zona waktu Anda secara manual, atur ini ke mati.</p>
    </item>
    <item>
      <p>Klik <gui>Zona Waktu</gui>, lalu cari kota Anda saat ini.</p>
    </item>
  </steps>

  <p>Waktu akan dimutakhirkan secara otomatis ketika Anda memilih lokasi lain. Anda mungkin juga ingin <link xref="clock-set">mengatur jam secara manual</link>.</p>

</page>
