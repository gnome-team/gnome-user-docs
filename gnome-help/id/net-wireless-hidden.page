<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="id">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Menyambung ke jaringan nirkabel yang tidak ditampilkan dalam daftar jaringan.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Menyambung ke suatu jaringan nirkabel tersembunyi</title>

<p>Dimungkinkan untuk membuat jaringan nirkabel sehingga "tersembunyi." Jaringan tersembunyi tidak akan muncul dalam daftar jaringan nirkabel yang ditampilkan dalam pengaturan <gui>Jaringan</gui>. Untuk menyambung ke jaringan nirkabel tersembunyi:</p>

<steps>
  <item>
    <p>Buka <gui xref="shell-introduction#systemmenu">menu sistem</gui> pada bagian kanan dari bilah puncak.</p>
  </item>
  <item>
    <p>Pilih panah dari <gui>Wi-Fi</gui>. Bagian Wi-Fi pada menu akan diperluas.</p>
  </item>
  <item>
    <p>Klik <gui>Semua Jaringan</gui>.</p>
  </item>
  <item><p>Tekan tombol menu di pojok kanan atas jendela dan pilih <gui>Sambungkan ke Jaringan Tersembunyi…</gui>.</p></item>
 <item>
  <p>Di jendela yang muncul, pilih jaringan tersembunyi yang telah tersambung sebelumnya menggunakan daftar tarik-turun <gui>Koneksi</gui>, atau <gui>Baru</gui> untuk yang baru.</p>
 </item>
 <item>
  <p>Untuk sambungan baru, ketik nama jaringan dan pilih jenis keamanan nirkabel dari daftar tarik-turun <gui>keamanan Wi-Fi</gui>.</p>
 </item>
 <item>
  <p>Masukkan kata sandi atau rincian keamanan lainnya.</p>
 </item>
 <item>
  <p>Klik <gui>Sambung</gui>.</p>
 </item>
</steps>

  <p>Anda mungkin harus memeriksa pengaturan access point nirkabel atau router untuk melihat apa nama jaringan. Jika Anda tidak memiliki nama jaringan (SSID), Anda dapat menggunakan <em>BSSID</em> (Basic Service Set Identifier, alamat MAC access point), yang terlihat seperti <gui>02:00:01:02:03:04</gui> dan biasanya dapat ditemukan di bagian bawah access point.</p>

  <p>Anda juga harus memeriksa pengaturan keamanan untuk access point nirkabel. Cari istilah seperti WEP dan WPA.</p>

<note>
 <p>Anda mungkin berpikir bahwa menyembunyikan jaringan nirkabel Anda akan meningkatkan keamanan dengan mencegah orang yang tidak tahu tentang hal itu dari menyambung. Dalam prakteknya, hal ini tidak terjadi; jaringan sedikit lebih sulit untuk menemukan tetapi masih terdeteksi.</p>
</note>

</page>
