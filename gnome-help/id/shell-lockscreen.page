<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="id">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Layar kunci dekoratif dan fungsional menyampaikan informasi yang berguna.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Layar kunci</title>

  <p>Layar kunci berarti bahwa Anda dapat melihat apa yang terjadi saat komputer Anda terkunci, dan memungkinkan Anda untuk mendapatkan ringkasan dari apa yang telah terjadi saat Anda telah pergi. Layar kunci memberikan informasi yang berguna:</p>

  <list>
<!--<item><p>the name of the logged-in user</p></item> -->
    <item><p>tanggal dan waktu, serta notifikasi tertentu</p></item>
    <item><p>status baterai dan jaringan</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Untuk membuka kunci komputer Anda, angkat tirai layar kunci dengan menyeretnya ke atas dengan kursor, atau dengan menekan <key>Esc</key> atau <key>Enter</key>. Ini akan mengungkapkan layar log masuk, tempat Anda dapat memasukkan kata sandi untuk membuka kunci. Sebagai alternatif, mulailah mengetik kata sandi Anda dan tirai akan dinaikkan secara otomatis saat Anda mengetik. Anda juga dapat beralih pengguna jika komputer Anda dikonfigurasi untuk lebih dari satu.</p>

  <p>Untuk menyembunyikan notifikasi dari layar kunci, lihat <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
