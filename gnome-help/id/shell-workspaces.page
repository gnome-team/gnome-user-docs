<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="id">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ruang kerja adalah cara mengelompokkan jendela pada desktop Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Apa itu ruang kerja, dan bagaimana itu akan membantu saya?</title>

  <p if:test="!platform:gnome-classic">Ruang kerja mengacu pada pengelompokan jendela di desktop Anda. Anda dapat membuat beberapa ruang kerja, yang bertindak seperti desktop virtual. Ruang kerja dimaksudkan untuk mengurangi serakan dan membuat desktop lebih mudah dinavigasi.</p>

  <p if:test="platform:gnome-classic">Ruang kerja mengacu pada pengelompokan jendela di desktop Anda. Anda dapat menggunakan beberapa ruang kerja, yang bertindak seperti dekstop virtual. Ruang kerja dimaksudkan untuk mengurangi serakan dan membuat desktop lebih mudah dinavigasi.</p>

  <p>Ruang kerja dapat digunakan untuk mengatur pekerjaan Anda. Misalnya, Anda dapat memiliki semua jendela komunikasi, seperti program surel dan obrolan, di satu ruang kerja, dan pekerjaan yang Anda lakukan di ruang kerja yang berbeda. Manajer musik Anda bisa berada di ruang kerja ketiga.</p>

<p>Memakai area kerja:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">Di ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui>, Anda dapat menavigasi secara horizontal di antara ruang kerja.</p>
    <p if:test="platform:gnome-classic">Klik tombol di kiri bawah layar dalam daftar jendela, atau tekan tombol <key xref="keyboard-key-super">Super</key> untuk membuka ringkasan <gui>aktivitas</gui> .</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Jika lebih dari satu ruang kerja sudah digunakan, <em>pemilih ruang kerja</em> ditampilkan antara bidang pencarian dan daftar jendela. Ini akan menampilkan ruang kerja yang saat ini digunakan ditambah ruang kerja kosong.</p>
    <p if:test="platform:gnome-classic">Di sudut kanan bawah, Anda melihat empat kotak. Ini adalah pemilih ruang kerja.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Untuk menambahkan ruang kerja, seret dan jatuhkan suatu jendela dari ruang kerja yang ada ke ruang kerja kosong di pemilih ruang kerja. Ruang kerja ini sekarang berisi jendela yang telah Anda jatuhkan, dan ruang kerja kosong baru akan muncul di sebelahnya.</p>
    <p if:test="platform:gnome-classic">Seret dan jatuhkan jendela dari ruang kerja Anda saat ini ke ruang kerja kosong di pemilih ruang kerja. Ruang kerja ini sekarang memuat jendela yang telah Anda jatuhkan.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Untuk menghapus ruang kerja, cukup tutup semua jendelanya atau pindahkan ke ruang kerja lainnya.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Selalu ada paling tidak satu ruang kerja.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>Pemilih ruang kerja</p>
    </media>

</page>
