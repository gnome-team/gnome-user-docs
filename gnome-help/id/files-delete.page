<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="id">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Menghapus berkas-berkas atau folder-folder yang tidak Anda perlukan lagi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2024.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Menghapus berkas-berkas dan folder-folder</title>

  <p>Apabila Anda tidak menginginkan sebuah berkas atau folder lagi, Anda dapat menghapusnya. Bila Anda menghapus sebuah butir, itu akan dipindahkan ke folder <gui>Tong Sampah</gui>, di mana butir tersebut akan tetap tersimpan sampai Anda mengosongkan tong sampah. Anda dapat <link xref="files-recover">mengembalikan butir</link> di <gui>Tong Sampah</gui> ke lokasi asalnya jika Anda putuskan bahwa Anda membutuhkan mereka, atau bila mereka terhapus secara tidak sengaja.</p>

  <steps>
    <title>Untuk mengirim berkas ke tong sampah:</title>
    <item><p>Pilih berkas yang ingin Anda tempatkan ke tong sampah dengan mengkliknya sekali.</p></item>
    <item><p>Tekan <key>Hapus</key> pada papan tik Anda. Cara lainnya, seret dan lepaskan item ke <gui>Tong Sampah</gui> di bilah samping.</p></item>
  </steps>

  <p>Bkas akan dipindahkan ke tong sampah, dan Anda akan ditampilkan pilihan untuk <gui>Urungkan</gui> penghapusan. Tombol <gui>Urungkan</gui> akan tampil untuk beberapa detik. Bila Anda memilih <gui>Urungkan</gui>, berkas akan dikembalikan ke lokasi semula.</p>

  <p>Untuk menghapus berkas-berkas secara permanen, dan menambah ruang kosong pada komputer Anda, Anda perlu untuk mengosongkan tong sampah. Untuk mengosongkan tong sampah, klik kanan <gui>Tong Sampah</gui> pada bilah samping dan pilih <gui>Kosongkan Tong Sampah</gui>.</p>

  <section id="permanent">
    <title>Menghapus berkas secara permanen</title>
    <p>Anda dapat seketika menghapus sebuah berkas secara permanen, tanpa mesti mengirimnya terlebih dahulu ke tong sampah.</p>

  <steps>
    <title>Untuk menghapus berkas secara permanen:</title>
    <item><p>Pilih butir yang ingin Anda hapus.</p></item>
    <item><p>Tekan dan tahan tombol <key>Shift</key>, lalu tekan tombol <key>Delete</key> pada papan tik Anda.</p></item>
    <item><p>Karena Anda tidak bisa membatalkan ini, Anda akan diminta untuk mengkonfirmasi bahwa Anda ingin menghapus berkas atau folder.</p></item>
  </steps>

  <note><p>Berkas yang terhapus pada <link xref="files#removable">perangkat lepasan </link> mungkin tidak terlihat pada sistem operasi lainnya, seperti Windows atau Mac OS. Berkas-berkas tersebut masih berada di sana, dan akan tersedia saat Anda menancapkan kembali perangkat tersebut ke komputer Anda.</p></note>

  </section>

</page>
