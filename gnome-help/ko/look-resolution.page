<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="ko">

  <info>
    <link type="guide" xref="prefs-display" group="#first"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>
    <revision version="gnome:46" status="draft" date="2024-04-22"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Emanuel Cisár</name>
      <email>536429@mail.muni.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>화면 해상도 및 방향(회전)을 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>화면 해상도 또는 방향 바꾸기</title>

  <p><em>화면 해상도</em>를 바꾸어 화면 표시 요소를 크게(또는 세밀하게)보이는 정도를 바꿀 수 있습니다. <em>회전</em> 값을 바꾸면 (화면을 회전할 경우) 보이는 방식을 바꿀 수 있습니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>디스플레이</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>디스플레이</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>동일 화면으로 설정하지 않은 여러 디스플레이가 있다면, 각 디스플레이에 다른 설정을 적용할 수 있습니다. 미리 보기 영역에서 디스플레이를 선택합니다.</p>
    </item>
    <item>
      <p>방향, 해상도, 스케일, 재생율을 선택합니다.</p>
    </item>
    <item>
      <p><gui>적용</gui>을 누릅니다. 되돌리기 전에 20초 동안 새 설정을 적용합니다. 이런 방식을 통해 만약 새 설정으로 아무것도 볼 수 없을 경우 이전 설정으로 자동으로 복원합니다. 새 설정에 만족한다면, <gui>바뀐 내용 유지</gui>를 누릅니다.</p>
    </item>
  </steps>

<section id="orientation">
  <title>방향</title>

  <p>일부 장치의 경우 화면을 여러 방향으로 물리적으로 돌릴 수 있습니다. 창의 <gui>방향</gui>을 누른 후 <gui>가로</gui>, <gui>세로 오른쪽</gui>, <gui>세로 왼쪽</gui>, <gui>가로 (뒤집힘)</gui> 중 하나를 선택합니다.</p>

  <note style="tip">
    <p>장치에서 화면을 자동으로 회전할 경우 <gui xref="shell-introduction#systemmenu">시스템 메뉴</gui>의 하단에서 <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">회전 잠금</span></media> 단추를 눌러 현재 회전 상태를 잠글 수 있습니다. 잠금을 해제하려면 <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">회전 잠금 해제</span></media> 단추를 누릅니다</p>
  </note>

</section>

<section id="resolution">
  <title>해상도</title>

  <p>해상도란 각 방향으로 표시할 수 있는 화면의 점 수를 말합니다. 각 해상도에는 가로, 세로의 비율인 <em>종횡비</em>를 지닙니다. 기존 디스플레이는 4:3 종횡비를 활용하나, 와이드 화면에는 16:9 종횡비를 활용합니다. 화면의 종횡비에 맞지 않는 해상도를 선택하면, 화면의 상하단 또는 좌우 측면에 검은 테두리를 추가하여 화면의 왜곡을 막아줍니다.</p>

  <p><gui>해상도</gui> 드롭다운 목록에서 적절한 해상돌르 선택할 수 있습니다. 화면에 맞지 않는 해상도를 선택하면 <link xref="look-display-fuzzy">찌그러져 보이거나 픽셀이 도드라지게 나타납니다</link>.</p>

</section>

<section id="native">
  <title>자체 해상도</title>

  <p>랩톱 화면과 LCD 모니터의 <em>자체 해상도</em>는 가장 동작이 잘 되는 설정값입니다. 영상 신호에 따라 나타나는 픽셀이 화면상에 미세하게 정돈됩니다. 화면을 다른 해상도로 나타내야 한다면, 픽셀을 화면에 표현할 때 이미지 화질을 떨어뜨릴 수 있는 보간 처리가 필요합니다.</p>

</section>

<section id="refresh">
  <title>재생율</title>

  <p>재생율이란 화면 이미지를 그리는, 또는 새로 고치는 초당 횟수를 의미합니다.</p>

  <section id="variable-refresh-rate">
    <title>Variable refresh rate (VRR)</title>
      <p>For smoother visuals and to optimize power consumption you can
    synchronize monitor's refresh rate with content output.</p>

  <note style="tip">
    <p>VRR is different from Adaptive V-Sync (late V-Sync),
    which dynamically toggles V-Sync based on frame rate. </p>
  </note>

  </section>
  <section id="preferred-refresh-rate">
    <title>Preferred Refresh Rate</title>
    <p>Specify your preferred refresh rate to maintain optimal visual quality.</p>
  </section>
</section>

<section id="scale">
  <title>비율</title>

  <p>비율을 설정하면 디스플레이 밀도에 일치하도록 화면 객체 크기가 늘어납니다. 이렇게 하면 글씨를 읽기 쉬워집니다. <gui>100%</gui> 또는 <gui>200%</gui>를 선택합니다.</p>

</section>

</page>
