<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="ko">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>Bastien Nocera</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>컴퓨터에 특정 장치를 연결하는 방법을 설명합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>특정 장치 페어링 방법</title>

  <p>장치를 별도로 직접 관리한다면, 페어링할 수 있는 충분한 정보가 없을 수도 있습니다. 몇가지 일반 장치에 대해 자세한 설명을 드리도록 하겠습니다.</p>

  <terms>
    <item>
      <title>플레이스테이션 5 조이패드</title>
      <p><media its:translate="no" type="image" mime="image/svg" src="figures/ps-create.svg">Create</media> 단추를 누르고, 밝은 막대가 깜빡일 때까지 <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> 단추를 누르면 다른 블루투스 장치에서 볼 수 있고 연결할 수 있습니다.</p>
    </item>
    <item>
      <title>플레이스테이션 4 조이패드</title>
      <p><media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> 단추와 “공유” 단추를 동시에 눌러 조이패드를 페어링 하면 다른 블루투스 장치처럼 페어링할 수 있게 목록에 나타납니다.</p>
      <p>이 장치는 “케이블 페어링” 방식도 진행할 수 있습니다. USB로 조이패드를 연결한 후 <gui>블루투스 설정</gui>을 열고 블루투스를 켭니다. <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> 단추를 누르지 않고도 이 조이패드를 설정할 지 여부가 뜹니다. 케이블을 뽑고 <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> 단추를 누르면 블루투스로 연결하여 사용합니다.</p>
    </item>
    <item>
      <title>플레이스테이션 3 조이패드</title>
      <p>이 장치는 “케이블 페어링” 방식을 진행합니다. USB로 조이패드를 연결한 후 <gui>블루투스 설정</gui>을 열고 블루투스를 켭니다. <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> 단추를 누르고 나면 이 조이패드를 설정할 지 여부가 뜹니다. 케이블을 뽑고 <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> 단추를 누르면 블루투스로 연결하여 사용합니다.</p>
    </item>
    <item>
      <title>플레이스테이션 3 BD 원격 조종기</title>
      <p>“Start” 단추와 “Enter” 단추를 동시에 5초간 누릅니다. 장치 목록에서 일반 장치처럼 원격으로 선택할 수 있습니다.</p>
    </item>
    <item>
      <title>닌텐도 위, 위 U 원격 조종기</title>
      <p>배터리 칸의 붉은 “Sync” 단추를 활용하여 페어링 단계를 시작합니다. 다른 단추를 함께 누르는 동작은 페어링 정보를 유지하지 않으므로, 간단한 순서를 따라 전 과정을 다시 진행해야 할 수도 있습니다. 또한 콘솔 에뮬레이터 같은 일부 프로그램에서는 직접 접근 방식이든, 원격 접근 방식이든, 이 경우 블루투스 창에서 설정하면 안됩니다. 절차는 프로그램 설명서를 참고합니다.</p>
    </item>
    <item>
      <title>ION iCade</title>
      <p>하단 단추 네개와 상단 백색 단추를 눌러 페어링 과정을 시작합니다. 페어링 과정이 나타나면, 코드를 입력하는 가장 중요한 절차인지 아케이드 스틱 좌측의 백색 단추 두개를 아무거나 눌러 확인하는 단계인지 확인해가며 진행합니다.</p>
    </item>
  </terms>

</page>
