<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="ko">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>더 길고 복잡한 암호를 사용합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>안전한 암호 지정</title>

  <note style="important">
    <p>기억하기 충분히 쉽게, 그러나 다른 사람(그리고 컴퓨터 프로그램에서도)에겐 추측이 어렵게 암호를 지정합니다.</p>
  </note>

  <p>적절한 암호로 지정하면 컴퓨터를 안전하게 지키는데 도움을 줍니다. 암호가 추측하기 쉬우면 누군가는 암호를 알아내서 개인 정보를 취득할 수 있습니다.</p>

  <p>여러분의 암호를 시스템에서 추정할 수 있는 컴퓨터를 사용할 수도 있어, 해당 암호가 사람이 추측하기 어렵다 할 지라도 컴퓨터 프로그램에서 깨기 쉬울 수도 있습니다. 적절한 암호를 지정하는 몇가지 비결이 있습니다:</p>
  
  <list>
    <item>
      <p>대소문자, 숫자, 기호, 공백을 암호에 적절히 배합합니다. 이렇게 하면 추측하기 더 어려워집니다. 지정 기호가 더 많아지면, 여러분의 암호를 추측할 수 있는 암호 가지수가 더 많아진다는 의미일 수 있습니다.</p>
      <note>
        <p>적절한 암호 지정 방법은 여러분이 기억하기 쉬운 어구의 각 단어의 첫글자를 취하는 방법입니다. 이 어구는 영화, 책, 노래, 앨범의 제목이 될 수 있습니다. 예를 들면 “Flatland: A Romance of Many Dimensions”는 F:ARoMD, faromd, f: aromd 중 하나일 수 있습니다.</p>
      </note>
    </item>
    <item>
      <p>암호를 최대한 길게 만듭니다. 더 많은 문자가 들어가면 사람 또는 컴퓨터가 유추할 수 있는 시간이 길어집니다.</p>
    </item>
    <item>
      <p>어떤 언어의 표준 자너에 나타나는 단어는 사용하지 않습니다. 암호 찾기 프로그램이 이 단어를 먼저 시험해봅니다. 가장 일반적인 암호는 “password”입니다. 많은 사람이 이런 암호를 매우 빨리 유추할 수 있습니다!</p>
    </item>
    <item>
      <p>날짜, 자격증 번호, 가족 이름 같은 개인 정보는 활용하지 않습니다.</p>
    </item>
    <item>
      <p>어떤 명사도 사용하지 않습니다.</p>
    </item>
    <item>
      <p>빨리 입력할 수 있는 암호로 지정하여 여러분이 암호를 입력하는 모습을 보더라도 알아챌 수 있는 기회를 줄입니다.</p>
      <note style="tip">
        <p>암호를 어딘가에 적어두지 않습니다. 다른 사람이 쉽게 찾아볼 수 있습니다!</p>
      </note>
    </item>
    <item>
      <p>다른 대상에 다른 암호를 사용합니다.</p>
    </item>
    <item>
      <p>다른 계정에 다른 암호를 사용합니다.</p>
      <p>모든 계정에 동일한 암호를 사용하면, 이를 유추한 누군가는 모든 계정에 바로 뚫고 들어갈 수 있습니다.</p>
      <p>많은 암호를 기억하기 어려울 수 있습니다. 모든 대상에 각기 다른 암호를 사용하는 것 만큼은 안전하지 않겠지만, (웹사이트같이) 크게 신경쓰지 않는 부분에 동일한 암호를 사용하고, 중요한 부분(온라인 금융 계정과 전자메일 계정)에 다른 암호를 지정하면 좀 더 기억하기 쉬울 수 있습니다.</p>
   </item>
   <item>
     <p>암호를 정기적으로 바꿉니다.</p>
   </item>
  </list>

</page>
