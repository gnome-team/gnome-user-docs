<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="ko">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>마우스 키패드 기능을 켜서 숫자 키패드로 마우스를 제어합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>키패드로 마우스 단추를 누르고 마우스 포인터 움직이기</title>

  <p>마우스 또는 기타 포인터 장치 활용에 어려움이 있다면, 키보드의 숫자 키패드로 마우스 포인터를 제어할 수 있습니다. 이 기능을 <em>마우스 키</em>라고 합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>접근성</gui> 입력을 시작합니다.</p>
      <p><gui>현재 활동</gui>을 누르거나 화면 좌측 상단 구석에 마우스 포인터를 가져다 놓거나, <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> 키를 누른 다음 <key>Enter</key>키를 누르거나, <key xref="keyboard-key-super">Super</key> 키를 눌러 <gui>현재 활동</gui> 개요에 접근할 수 있습니다.</p>
    </item>
    <item>
      <p><gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>Select the <gui>Pointing &amp; Clicking</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Mouse Keys</gui> switch to on.</p>
    </item>
    <item>
      <p><key>Num Lock</key>키를 껐는지 확인합니다. 이제 키패드로 마우스 포인터를 움직일 수 있습니다.</p>
    </item>
  </steps>

  <p>키패드는 키보드에서 숫자 단추가 모여있는 부분이며, 정사각형 격자 모양새로 모여있습니다. (랩톱 키보드 처럼) 키보드에 키패드가 없다면, 펑션키(<key>Fn</key>)를 누른 채로 있어야 키보드의 다른 키를 키패드처럼 사용할 수 있습니다. 이 기능을 랩톱에서 종종 사용한다면, 외장 USB, 블루투스 숫자 키패드를 구매할 수 있습니다.</p>

  <p>키패드의 각 숫자는 해당 방향과 관련이 있습니다. 이를테면, <key>8</key> 키를 누르면 포인터를 위로, <key>2</key> 킬르 누르면 아래로 움직입니다. <key>5</key> 키는 마우스의 단추를 누르는 동작을 수행하며, 빨리 두번 두르면 두번 누르기로 인식합니다.</p>

  <p>대부분의 키보드는 마우스 오른쪽 단추를 누르는 특수 키가 있는데, 가끔 이를 <key xref="keyboard-key-menu">메뉴</key> 키라고 합니다. 그러나 이 키는 키보드 포커스가 어디에 있느냐에 따라 반응하지, 마우스 포인터가 어디에 있느냐에 따라 반응하는게 아닙니다. 키 패드의 <key>5</key> 키를 누르고 있거나 왼쪽 마우스 단추를 누른채로 있는 방법으로 오른쪽 단추를 누르는 방법에 대한 자세한 내용은 <link xref="a11y-right-click"/>를 살펴봅니다.</p>

  <p>마우스 키 기능을 켠 동안 키패드로 숫자를 입력하려면 <key>Num Lock</key> 키 램프를 켭니다. <key>Num Lock</key> 키 램프를 켠 동안에는 마우스 포인터를 키패드로 제어할 수 없습니다.</p>

  <note>
    <p>키보드 상단의 일반 숫자 키로는 마우스 포인터를 제어하지 못합니다. 오직 키패드 숫자 키만 사용할 수 있습니다.</p>
  </note>

</page>
