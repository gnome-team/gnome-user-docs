<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="ko">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>다른 용지 크기 또는 방향으로 문서를 인쇄합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>인쇄 용지 크기 바꾸기</title>

  <p>문서 용지 크기를 바꾸고자 한다면(예를 들어, A4 용지 규격 PDF를 US 레터 규격으로 인쇄할 때\), 문서의 인쇄 규격을 바꿀 수 있습니다.</p>

  <steps>
    <item>
      <p><keyseq><key>Ctrl</key><key>P</key></keyseq> 키를 눌러 인쇄 대화상자를 엽니다.</p>
    </item>
    <item>
      <p><gui>페이지 설정</gui> 탭을 선택합니다.</p>
    </item>
    <item>
      <p><gui>용지</gui>열의 드롭다운 목록에서 <gui>용지 크기</gui>를 선택합니다.</p>
    </item>
    <item>
      <p><gui>인쇄</gui>를 눌러 문서를 인쇄합니다.</p>
    </item>
  </steps>

  <p>드롭다운 목록에서 다른 <gui>방향</gui>으로 지정할 수도 있습니다:</p>

  <list>
    <item><p><gui>세로</gui></p></item>
    <item><p><gui>가로</gui></p></item>
    <item><p><gui>세로 반전</gui></p></item>
    <item><p><gui>가로 반전</gui></p></item>
  </list>

</page>
