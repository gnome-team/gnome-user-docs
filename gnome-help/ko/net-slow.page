<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="ko">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>다른 요소를 다운로드하고 있거나, 연결 상태가 좋지 않거나, 혼잡한 시간대일 수 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>인터넷이 느린 것 같아요</title>

  <p>인터넷을 사용하고 있는데 느린 것 같다면, 느리게 하는 요인이 여러가지가 있습니다.</p>

  <p>웹 브라우저를 닫고 다시 열거나, 인터넷 연결을 끊고 다시 연결해봅니다. (인터넷을 느리게 할 만한 여러가지 요인을 초기화해봅니다.)</p>

  <list>
    <item>
      <p><em style="strong">혼잡한 시간대</em></p>
      <p>인터넷 서비스 제공 업체는 보통 여러집에 인터넷 연결을 공유하게끔 구성합니다. 전화선 또는 케이블 연결 방식으로 연결을 따로 했다 하더라도 전화 교환망의 인터넷 연결 부분은 실제로 공유 방식을 취합니다. 이 경우 여러 이웃이 여러분과 동시간대에 인터넷을 사용한다면 인터넷 속도가 느려짐을 확인할 수 있습니다. 이웃이 인터넷망을 활용하고 있을 즈음(예를 들어 저녁 시간대에)이면, 대부분 이런 경험을 할 지도 모르겠습니다.</p>
    </item>
    <item>
      <p><em style="strong">한번에 많은 파일 다운로드</em></p>
      <p>여러분 또는 누군가가 여러 파일을 한번에 다운로드하려는 목적으로 인터넷 연결을 사용한다면, 인터넷 연결상 요구에 대한 응답이 충분히 빠르지 않을 수도 있습니다. 이 경우, 느리다고 느낍니다.</p>
    </item>
    <item>
      <p><em style="strong">신뢰할 수 없는 연결</em></p>
      <p>특히 요청량이 일시적으로 많을 때 인터넷 연결의 안정성이 떨어질 수도 있습니다. 붐비는 커피 상점이나 회랑의 경우 인터넷 연결이 붐비거나 단순히 안정성이 떨어지기도 합니다.</p>
    </item>
    <item>
      <p><em style="strong">약한 무선 연결 신호</em></p>
      <p>무선(Wi-Fi) 인터넷 연결을 할 경우 상단 표시줄의 네트워크 아이콘을 확인하여 무선 신호가 양호한지 확인합니다. 양호하지 않으면 충분히 강한 신호 상태가 아니기 때문에 인터넷이 느릴 수 있습니다.</p>
    </item>
    <item>
      <p><em style="strong">느린 모바일 인터넷 연결 사용</em></p>
      <p>모바일 인터넷 연결을 한 상태에서 느리다면, 신호 수신이 불량한 지역으로 이동했기 때문일 수도 있습니다. 이 경우, 인터넷 연결을 3G와 같은 고속 “모바일 광대역” 연결에서 더 느리지만 견고한 GPRS 같은 연결로 자동으로 전환합니다.</p>
    </item>
    <item>
      <p><em style="strong">웹 브라우저에 문제가 있어요</em></p>
      <p>가끔 웹 브라우저가 느리게 동작하게 만드는 문제가 있습니다. 몇가지 이유 때문일 수 있습니다. 웹 사이트를 불러오는 양 자체가 많은 곳이거나, 웹 브라우저에서 사이트를 여는데 시간이 오래걸리는 경우입니다. 모든 브라우저 창을 닫고 다시 열어서 차이가 있는지 확인해봅니다.</p>
    </item>
  </list>

</page>
