<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="ko">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>컴퓨터 앞에서 자리를 비웠을 때 타인의 데스크톱 사용을 막습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>화면 자동 잠금</title>
  
  <p>컴퓨터로부터 자리를 비울 때 다른 사람이 여러분의 데스크톱과 파일을 못보게 막으려면 <link xref="shell-exit#lock-screen">화면을 잠금</link>처리해야 합니다. 때에 따라 화면 잠그는 일을 일었다면, 일정 시간이 지난 후 컴퓨터에서 화면을 자동으로 잠그게 하고 싶을 때가 있습니다. 이 방법을 통해 컴퓨터를 사용하지 않을 때 컴퓨터를 안전하게 지킬 수 있습니다.</p>

  <note><p>화면을 잠궈두었다면, 프로그램과 시스템 프로세스는 계속 실행하지만, 해당 요소를 다시 사용하려면 암호를 입력해야합니다.</p></note>
  
  <steps>
    <title>자동 화면 잠금 시간을 설정하려면:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy &amp; Security</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Privacy &amp; Security</gui></guiseq> from the
      results. This will open the <gui>Privacy &amp; Security</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Screen Lock</gui> to open the panel.</p>
    </item>
    <item>
      <p><gui>자동 화면 잠금</gui> 스위치를 켰는지 확인한 다음, <gui>자동 화면 잠금 지연 시간</gui> 드롭다운 목록에서 시간을 선택합니다.</p>
    </item>
  </steps>

  <note style="tip">
    <p>앱에서는 잠금 화면에 알림을 나타낼 수 있습니다. 예를 들면 화면의 잠금을 굳이 풀지 않아도 어떤 전자메일을 볼 때 상당히 쓸만합니다. 다른 사람이 알림을 보는게 신경이 쓰인다면, <gui>잠금 화면에 알림 표시</gui>를 끕니다. 더 많은 알림 설정은 <link xref="shell-notifications"/>을 참고합니다.</p>
  </note>

  <p>화면을 잠근 후 잠금을 풀고자 할 대, <key>Esc</key>키를 누르거나 마우스로 화면 하단으로부터 상단으로 닦아 올립니다. 그 다음 암호를 입력하고 <key>Enter</key>키를 누르거나 <gui>잠금 해제</gui>를 누릅니다. 대신, 바로 암호 입력을 싲가해서 입력하는대로 잠금 커튼이 자동으로 나타나게 할 수 있습니다.</p>

</page>
