<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0" id="remote-login" xml:lang="ko">

  <info>
    <revision version="gnome:46" date="2024-04-21" status="draft"/>
    <link type="guide" xref="sharing"/>

    <credit type="author copyright">
      <name>Marie Stará</name>
      <email>413827@mail.muni.cz</email>
      <years>2024</years>
    </credit>
        <credit type="collaborator">
      <name>Felipe Borges</name>
    </credit>
    <credit type="collaborator">
      <name>Jeremy Bicha</name>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Log in to your device remotely using RDP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Log in remotely to your device</title>

  <p>You can log in to your user account from another device by using a
  remote desktop connection application. Configure <gui>Remote Login</gui> to
  access your device and set the security preferences.</p>

  <p>If you want to let others to view or control your desktop, see
  <link xref="sharing-desktop">Desktop Sharing</link>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from
      the results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Remote Desktop</gui><gui>Remote Login</gui>
      </guiseq>.</p>
    </item>
    <item>
      <p>Click <gui>Unlock</gui> and authenticate to change the settings.</p>
    </item>
    <item>
      <p>To be able to login to your user account remotely, switch on <gui>Remote
      Login</gui>.</p>
    </item>
  </steps>

  <section id="connecting">
  <title>연결</title>

  <p>The <gui>How to Connect</gui> section displays the <gui>Hostname</gui>
  and <gui>Port</gui> that can be used on the connecting device. Click the
  button next to each entry if you want to place it in the clipboard. A
  connection can also be established using your <link xref="net-findip">IP address</link>.</p>

  <note style="important">
    <p>If <gui>Remote Login</gui> is switched on, it sets its port number to 3389. If you also
    use <link xref="sharing-desktop">Desktop Sharing</link>, it will use a different port, for example,
    3390.</p>
  </note>

  </section>

  <section id="authentication">
  <title>Authentication</title>

  <p>The <gui>Login Details</gui> section displays the user name and password you will need to
  enter in the client software used for connecting to your user account.</p>

  <note style="tip">
    <p>Click <gui>Verify Encryption</gui> to display the encryption
    fingerprint. Compare it with the value displayed by the client when
    connecting: they should be identical.</p>
  </note>
  </section>

  <section id="clients">
  <title>클라이언트</title>

  <p>To connect to your desktop from another device, the following clients are
  known to work.</p>
  <terms>
    <item>
      <title>리눅스에서:</title>
      <list>
        <item><p><app>Remmina</app>, a GTK client, is available as a package in
        most distributions, and also as a
        <link href="https://flathub.org/apps/details/org.remmina.Remmina">flatpak</link>.
        Use default settings, particularly <gui>Color depth</gui> “Automatic”
        in the connection profile settings.</p>
        </item>
        <item><p><app>Connections</app>, a GTK client, is available as a package
        in most distributions, and also as a <link href="https://flathub.org/apps/org.gnome.Connections">flatpak</link>.</p>
        </item>
        <item><p><app>xfreerdp</app>는 대부분 배포판에서 활용할 수 있는 명령행 클라이언트입니다. 명령행에서 클라이언트에 <cmd>/network:auto</cmd> 옵션을 전달해야합니다.</p>
        </item>
      </list>
    </item>
    <item>
      <title>마이크로소프트 윈도우에서:</title>
      <list>
        <item><p>윈도우 내장 클라이언트인 <app>mstsc</app>가 있습니다. 기본 설정을 권장합니다.</p>
        </item>
      </list>
    </item>
    <item>
      <title>리눅스, 윈도우, 맥OS 에서:</title>
      <list>
        <item><p><app>Thincast</app>는 상용 클라이언트입니다. 리눅스 버전으로 <link href="https://flathub.org/apps/details/com.thincast.client">플랫팩</link>으로도 제공합니다. 기본 설정을 권장합니다.</p>
        </item>
      </list>
    </item>
  </terms>
  </section>

  <section id="checking-connection">
  <title>Checking connection</title>

  <p>To check that the connection works, follow the steps below.</p>

  <steps>
    <item>
      <p>In your chosen client, enter the <gui>Hostname</gui> or IP address.</p>
    </item>
    <item>
      <p>Fill in the user name and password for Remote Login.</p>
    </item>
  </steps>
  </section>

</page>
