<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="ko">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><keyseq><key>Ctrl</key><key>S</key></keyseq> 키를 눌러 비슷한 이름을 가진 여러 파일을 선택합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>패턴으로 파일 선택하기</title>

  <p>파일 이름 패턴을 활용하여 파일을 선택할 수 있습니다. <keyseq><key>Ctrl</key><key>S</key></keyseq> 키를 눌러 <gui>일치하는 항목 선택</gui> 창을 띄웁니다. 파일 이름의 공통 부분에 대한 패턴을 입력하고 와일드카드 문자를 넣습니다. 사용할 수 있는 두가지 와일드 카드 문자는 다음과 같습니다:</p>

  <list style="compact">
    <item><p><file>*</file>는 임의의 문자 임의 갯수와 일치하며, 문자가 없어도 됩니다.</p></item>
    <item><p><file>?</file> 기호는 임의의 문자 하나와 일치합니다.</p></item>
  </list>

  <p>예제:</p>

  <list>
    <item><p>동일한 <file>invoice</file> 공동 이름을 가진 오픈도큐먼트 텍스트 파일, PDF 파일, 사진 파일이 있다면, 다음 패턴으로 세 종류 파일을 모두 선택합니다</p>
    <example><p><file>Invoice.*</file></p></example></item>

    <item><p><file>Vacation-001.jpg</file>, <file>Vacation-002.jpg</file>, <file>Vacation-003.jpg</file>와 같은 이름을 가진 사진이 있을 경우, 다음 패턴으로 선택합니다</p>
    <example><p><file>Vacation-???.jpg</file></p></example></item>

    <item><p>이전 사진이 있는데 편집한 일부 사진의 파일 이름 뒤에 <file>-edited</file> 이름을 붙였을 경우 편집한 사진을 다음 패턴으로 검색합니다</p>
    <example><p><file>Vacation-???-edited.jpg</file></p></example></item>
  </list>

</page>
