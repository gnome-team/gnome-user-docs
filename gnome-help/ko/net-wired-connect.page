<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="ko">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>유선 네트워크 연결을 설정하려면, 네트워크 케이블 연결이 해야 할 일의 전부입니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>유선(이더넷) 네트워크 연결</title>

  <!-- TODO: create icon manually because it is one overlaid on top of another
       in real life. -->
  <p>유선 네트워크 연결을 설정하려면, 네트워크 케이블 연결이 해야 할 일의 전부입니다. 유선 네트워크 아이콘 (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">설정</span></media>)은 연결한 상태일 경우에 3점으로 상단 표시줄에 나타납니다. 연결하고 나면 3점 아이콘이 사라집니다.</p>
  <media its:translate="no" type="image" src="figures/net-wired-ethernet-diagram.svg" mime="image/svg" width="51" height="38" style="floatend floatright">
    <p its:translate="yes">이더넷 케이블 그림</p>
  </media>

  <p>연결을 하지 못했다면, 우선 네트워크 케이블을 연결했는지 확인해야 합니다. 컴퓨터에서 케이블 끝은 컴퓨터의 사각 이더넷(네트워크) 포트에 꼽고 다른 쪽은 스위치, 라우터, 네트워크 벽 소켓 등에(네트워크 구성에 따라) 연결해야 합니다. 이더넷 포트는 항상 랩톱의 옆 가장자리에 있거나 데스크톱 후면 상단 어디 근처에 붙어있습니다. 때로는 이더넷 포트에 꼽아 동작할 경우 불이 들어오기도 합니다.</p>

  <note>
    <p>컴퓨터와 다른 컴퓨터를 네트워크 케이블로 직접 연결할 수는 없습니다(최소한, 추가 설정없이는). 두 컴퓨터를 바로 연결하려면 네트워크 허브, 라우터, 스위치로 연결해야합니다.</p>
  </note>

  <p>여전히 연결이 안됐다면 자동 설정(DHCP)를 지원하지 않는 네트워크일 수도 있습니다. 이 경우 <link xref="net-manual">직접 설정</link>해야 합니다.</p>

</page>
