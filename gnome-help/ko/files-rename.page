<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="ko">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-rename-multiple"/>
    <link type="seealso" xref="files-rename-music-metadata"/>

    <revision version="gnome:47" date="2024-08-31" status="candidate"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>파일 또는 폴더의 이름을 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>파일 또는 폴더 이름 바꾸기</title>

  <p>다른 파일 관리자에서와 같이, <app>파일</app> 에서도 파일 이름 또는 폴더 이름을 바꿀 수 있습니다.</p>

  <steps>
    <title>파일 또는 폴더 이름을 바꾸려면:</title>
    <item><p>항목에 포인터를 올려두고 오른쪽 단추를 누른 후 <gui>이름 바꾸기</gui>를 선택하거나, 파일을 선택한 후 <key>F2</key>키를 누릅니다.</p></item>
    <item><p>새 이름을 입력하고 <key>Enter</key>키를 누르거나 <gui>이름 바꾸기</gui>를 누릅니다.</p></item>
  </steps>

  <p>파일 이름을 바꿀 때, 파일 확장자 부분(마지막 <file>.</file> 다음 부분)이 아닌 파일 앞 부분만 선택한 상태입니다. 확장자는 보통 파일 형식이 무엇인지 (예: <file>file.pdf</file> 파일은 PDF 문서임) 나타내며 보통 여러분께서 바꾸고 싶어하지 않는 부분입니다. 확장자도 마찬가지로 바꾸려면 파일 이름 전부를 선택하고 바꿉니다.</p>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the sidebar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

  <section id="valid-chars">
    <title>올바른 파일 이름 문자</title>

    <p>파일 이름에는 <file>/</file>(슬래시) 문자를 제외하고 어떤 문자든 사용할 수 있습니다. 그러나 어떤 장치에서는 파일 이름에 더 많은 제한을 가하는 <em>파일 시스템</em>을 사용하기도 합니다. 따라서 파일 이름에 <file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file>, <file>/</file> 문자는 가장 피하는게 좋은 습관이 되겠습니다.</p>

    <note style="warning">
    <p>파일 이름에 <file>.</file>이 처음 문자로 온다면 파일 관리자에서 해당 파일을 보려고 하면 <link xref="files-hidden">숨김</link> 상태가 됩니다.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>일반 문제</title>

    <terms>
      <item>
        <title>파일 이름을 이미 사용하고 있습니다</title>
        <p>동일한 이름을 지닌 파일 또는 폴더를 같은 위치에 돌 수 없습니다. 만약 폴더에 있는 작업중인 파일과 동일한 이름으로 파일 이름을 바꾼다하면, 파일 관리자에서 허용하지 않습니다.</p>
        <p>파일 또는 폴더 이름은 대소문자를 구분합니다. 따라서 <file>File.txt</file>는 <file>FILE.txt</file>와는 다릅니다. 이런식의 다른 파일 이름은 추천하지는 않지만 허용합니다.</p>
      </item>
      <item>
        <title>파일 이름이 너무 깁니다</title>
        <p>어떤 파일 시스템에서는 파일 이름의 길이가 255 글자를 넘을 수 없습니다. 255 글자 제한은 파일 이름과 파일 경로 모두(이를테면, <file>/home/cheolsoo/Documents/work/business-proposals/…</file> )에 해당하기에 가능하다면 긴 파일 이름 및 긴 폴더 이름을 피해야 합니다.</p>
      </item>
      <item>
        <title>파일 이름 바꾸기 옵션을 사용할 수 없게 바뀌었습니다</title>
        <p><gui>이름 바꾸기</gui> 항목이 흐릿하다면 파일 이름을 바꿀 권한이 없다는 의미가 되겠습니다. 이 파일의 이름을 바꿀 때 위험성을 동반하며, 보호 파일의 이름을 바꿨을 때 시스템이 불안정해질 수 있습니다. 자세한 내용은 <link xref="nautilus-file-properties-permissions"/>을 봅니다.</p>
      </item>
    </terms>

  </section>

</page>
