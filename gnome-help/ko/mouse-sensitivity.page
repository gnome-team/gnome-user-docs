<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-sensitivity" xml:lang="ko">

  <info>
    <link type="guide" xref="mouse" group="#first"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-05" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit>
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>마우스 또는 터치패드를 사용할 때 포인터의 움직이는 속도를 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>마우스 및 터치패드 움직임 속도 조절하기</title>

  <p>마우스 또는 터치패드를 움직일 때 포인터가 너무 빨리 또는 느리게 움직인다면, 장치 포인터 속도를 조절할 수 있습니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>마우스 및 터치패드</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>마우스 및 터치패드</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>Adjust the <gui>Pointer Speed</gui> slider
      until the pointer motion is comfortable for you. Sometimes the most
      comfortable settings for one type of device are not the best for
      the other.</p>
    </item>
  </steps>

  <note>
    <p><gui>마우스</gui> 섹션은 마우스를 연결했을 때만 나타나며, <gui>터치패드</gui> 섹션은 시스템에 터치패드를 연결했을 때만 나타납니다.</p>
  </note>

</page>
