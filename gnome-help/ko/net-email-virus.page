<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-email-virus" xml:lang="ko">

  <info>
    <link type="guide" xref="net-email"/>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-antivirus"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>바이러스는 컴퓨터로의 감염 가능성이 별로 없지만 여러사람이 보낸 전자메일로 옮을 수 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>전자메일 바이러스 검사가 필요한가요?</title>

  <p>바이러스는 컴퓨터에서 자체 목적 달성 수단을 찾아내어 관리하며 문제를 일으키는 프로그램입니다. 보통 컴퓨터에서는 전자메일 메시지로 받아봅니다.</p>

  <p>리눅스를 실행하는 컴퓨터에 영향을 주는 바이러스는 상당히 드물기 때문에, <link xref="net-antivirus">전자메일 또는 기타 방식으로 바이러스에 걸릴 일은 거의 없습니다</link>. 바이러스를 담은 전자메일을 받았다면 컴퓨터에 영향을 거의 주지 않습니다. 그렇기에 전자메일 바이러스 검사를 굳이 할 필요는 없을지도 모릅니다.</p>

  <p>그러나 다른 사람에게 바이러스를 전달하는 일을 겪었다면 전자메일 바이러스 겁사를 하고 싶을 수도 있습니다. 예를 들면 친구 중 하나가 바이러스에 감염된 윈도우 컴퓨터를 사용중이고 바이러스에 감염된 전자메일을 보냈고, 여러분이 그 전자메일을 다른 윈도우 컴퓨터 사용자 친구에게 전달을 했다면, 두번째 친구 역시 바이러스에 감염됩니다. 바이러스 검사/치료 프로그램을 설치하여 이런 문제를 막을 수 있게 전자메일을 검사할 수는 있지만, 보통 이런 경우는 드물며 대부분 윈도우 또는 Mac OS 사용자라면 그들 나름대로의 방식으로 바이러스 검사/치료 프로그램을 활용합니다.</p>

</page>
