<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-format" xml:lang="ko">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>
    <revision pkgversion="3.37.2" date="2020-08-13" status="review"/>

    <desc>외장 하드 디스크 또는 USB 플래시 드라이브를 포맷하여 모든 파일 및 폴더를 제거합니다.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>이동식 디스크의 모든 데이터 삭제하기</title>

  <p>USB 메모리 스틱 또는 외장 하드 디스크와 같은 이동식 디스크가 있다면, 모든 파일 및 폴더를 완전히 제거하고 싶을 때가 있습니다. 디스크를 <em>포맷</em> 하면 완전 삭제가 가능합니다. 디스크의 모든 파일을 삭제하고 빈 상태로 만듭니다.</p>

<steps>
  <title>이동식 디스크 포맷하기</title>
  <item>
    <p><gui>현재 활동</gui> 개요에서 <app>디스크</app>를 엽니다.</p>
  </item>
  <item>
    <p>좌측의 저장 장치 목록에서 삭제하고자 하는 디스크를 선택합니다.</p>

    <note style="warning">
      <p>올바른 디스크를 선택했는지 확인합니다! 엉뚱한 디스크를 선택하면 전혀 다른 디스크의 모든 파일을 삭제합니다!</p>
    </note>
  </item>
  <item>
    <p><gui>볼륨</gui> 섹션에 있는 도구 모음에서 메뉴 단추를 누릅니다. 그리고 <gui>분할 영역 포맷…</gui>을 누릅니다.</p>
  </item>
  <item>
    <p>창이 뜨면 디스크의 파일 시스템 <gui>형식</gui>을 선택합니다.</p>
   <p>윈도우와 맥 OS 컴퓨터에서 뿐만 아니라 리눅스 컴퓨터에서도 디스크를 사용한다면 <gui>FAT</gui>를 선택합니다. 윈도우에서만 사용하겠다면 <gui>NTFS</gui>가 더 나은 선택지일 수 있습니다. 파일 시스템 형식의 대략적인 설명은 레이블에 나타납니다.</p>
  </item>
  <item>
    <p>디스크 이름을 지정하고 <gui>다음</gui> 을 눌러 계속 진행하여 확인 창을 나타냅니다. 세부 내용을 주의깊게 확인 후 <gui>포맷</gui>을 눌러 디스크를 지웁니다.</p>
  </item>
  <item>
    <p>포맷이 끝나면, 꺼내기 아이콘을 눌러 디스크를 안전하게 제거합니다. 이제 디스크가 비어있어야 하며 다시 사용할 준비 상태여야 합니다.</p>
  </item>
</steps>

<note style="warning">
 <title>디스크 포맷은 파일을 안전하게 삭제하는 방식이 아닙니다</title>
  <p>디스크 포맷은 데이터 전체를 제거하는 완전히 안전한 방법은 아닙니다. 포맷한 디스크에는 들어있던 파일이 나타나지는 않지만 특수 복구 프로그램에서 이 파일 정보를 가져올 수 있습니다. 파일을 안전하게 지워야 한다면, <app>shred</app>와 같은 명령행 유틸리티를 활용해야합니다.</p>
</note>

</page>
