<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="ko">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>우분투 문서 위키 기여자</name>
    </credit>
    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>아마도 문제 해결 단계를 계속 진행하려면 자세한 무선 네트워크 어댑터 번호를 알고 있어야 할 수도 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>무선 네트워크 문제 해결사</title>
  <subtitle>네트워크 하드웨어 정보 수집</subtitle>

  <p>이 단계에서는 무선 네트워크 장치의 정보를 수집합니다. 대부분의 무선 문제를 해결하는 방법은 무선 네트워크 어댑터의 제조, 모델 번호에 따라 다릅니다. 따라서 세부 정보를 어딘가에 적어두어야 합니다. 컴퓨터 구매시 장치 드라이버 설치 디스크와 같이 딸려오는 일부 항목 정보가 도움이 될 수도 있습니다. 여전히 보유하고 있다면 다음 항목을 살펴봅니다:</p>

  <list>
    <item>
      <p>무선 네트워크 장치의 포장 및 절차 설명서(특히 라우터의 사용자 안내서)</p>
    </item>
    <item>
      <p>무선 네트워크 어댑터용 드라이버 디스크(윈도우 드라이버만 있는 경우도 포함)</p>
    </item>
    <item>
      <p>컴퓨터, 무선 네트워크 어댑터, 라우터의 제조사, 모델 번호. 이 정보는 보통 장치 하단이나 뒷편에서 찾아볼 수 있습니다.</p>
    </item>
    <item>
      <p>무선 네트워크 장치 또는 포장에 버전 또는 리비전을 인쇄했을 수도 있습니다. 상당히 도움이 될 수 있으므로 주의깊게 살펴봅니다.</p>
    </item>
    <item>
      <p>드라이버 디스크에는 장치 자체, “펌웨어” 버전, 사용 구성요소(칩셋) 정보를 보여줍니다.</p>
    </item>
  </list>

  <p>가능하다면 동작하는 인터넷 연결로 대신 접근해서 필요한 프로그램과 드라이버를 다운로드해봅니다. (컴퓨터를 라우터에 이더넷 네트워크 케이블로 직접 연결하는게 좋겠지만, 필요할 경우에만 연결합니다).</p>

  <p>여러 항목에 접속할 수 있다면 <gui>다음</gui>을 누릅니다.</p>

</page>
