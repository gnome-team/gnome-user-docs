<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="ko">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>개별 파일 양식으로 새 문서를 간단하게 만듭니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>일반적으로 활용하는 문서 서식 양식</title>

  <p>동일한 내용을 기반으로 문서를 자주 만든다면, 파일 서식을 활용하여 만드는게 좋습니다. 파일 서식은 구성면에서 또는 내용면에서 다시 사용할 수 있는 문서의 한 형식일 수 있습니다. 예를 들면 편지 인삿말로 문서 서식을 만들 수 있습니다.</p>

  <steps>
    <title>새 양식 만들기</title>
    <item>
      <p>서식으로 활용할 문서를 만듭니다. 예를 들면 워드 프로세서 프로그램에서 편지 인삿말을 만들 수 있습니다.</p>
    </item>
    <item>
      <p>파일을 <file>홈</file> 폴더의 <file>Templates</file> 폴더에 서식으로 저장합니다. <file>Templates</file> 폴더가 없다면 먼저 만들어야 합니다.</p>
    </item>
  </steps>

  <steps>
    <title>문서를 만들 때 서식 활용하기</title>
    <item>
      <p>새 문서를 만들려는 폴더 위치를 엽니다.</p>
    </item>
    <item>
      <p>폴더의 빈공간 아무데서든 오른쪽 단추를 누르고 <gui style="menuitem">새 문서</gui>를 선택합니다. 가용 서식의 이름은 하위 목록에 나타납니다.</p>
    </item>
    <item>
      <p>목록에서 원하는 양식을 선택합니다.</p>
    </item>
    <item>
      <p>파일을 두 번 눌러 열고 편집을 시작합니다. 편집이 끝나면 <link xref="files-rename">파일 이름 바꾸기</link>를 진행하셔도 좋습니다.</p>
    </item>
  </steps>

</page>
