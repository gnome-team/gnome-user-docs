<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-location" xml:lang="ko">
  <info>
    <link type="guide" xref="privacy" group="#last"/>

    <revision pkgversion="3.14" date="2014-10-13" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>지리 위치 서비스를 켜거나 끕니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>위치 서비스 제어하기</title>

  <p>지오로케이션, 위치 서비스는 기지국 위치, GPS, Wi-Fi 액세스 포인트 위치 근처를 활용하여 현재 위치를 알아내며, 이 정보로 시간대를 설정하고 <app>지도</app>와 같은 프로그램에 활용합니다. 여러분의 위치 정보는 정확성 보장을 목적으로 네트워크에 공유할 수 있습니다.</p>

  <steps>
    <title>데스크톱 위치 기능 끄기</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy &amp; Security</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>Privacy &amp; Security</gui></guiseq> from the
      results. This will open the <gui>Privacy &amp; Security</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Location</gui> to open the panel.</p>
    </item>
    <item>
     <p>Switch the <gui>Automatic Device Location</gui> switch to off.</p>
     <p>To re-enable this feature, set the <gui>Automatic Device Location</gui> switch
     to on.</p>
    </item>
  </steps>

</page>
