<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="ko">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>로컬 주소록에서 연락처를 추가 또는 제거합니다.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>연락처 추가 또는 제거</title>

  <p>연락처를 추가하려면:</p>

  <steps>
    <item>
      <p><gui style="button">+</gui> 단추를 누릅니다.</p>
    </item>
    <item>
      <p><gui>새 연락처</gui> 대화상자에서 연락처 이름과 정보를 입력합니다. 각 필드 옆의 드롭 다운 상자를 눌러 입력할 세부 정보 형식을 선택합니다.</p>
    </item>
    <item>
      <p>더 많은 세부 정보를 추가하려면 <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">더 보기</span></media> 단추를 누릅니다.</p>
    </item>
    <item>
      <p><gui style="button">추가</gui>를 눌러 연락처를 저장합니다.</p>
    </item>
  </steps>

  <p>연락처를 제거하려면:</p>

  <steps>
    <item>
      <p>연락처 목록에서 연락처를 선택합니다.</p>
    </item>
    <item>
      <p>헤더 모음 우측 상단 구석에서 <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">더 보기</span></media> 단추를 누릅니다.</p>
    </item>
    <item>
      <p><gui style="menu item">삭제</gui> 옵션을 눌러 연락처를 제거합니다.</p>
    </item>
   </steps>
    <p>하나 이상의 연락처를 제거하려면, 삭제하고자 하는 연락처 옆의 상자에 표시한 후 <gui style="button">제거</gui>를 누릅니다.</p>
</page>
