<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="ko">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>봉투를 올바른 방향으로 올려두었는지 올바른 용지 규격을 선택했는지 확인합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>봉투 인쇄</title>

  <p>대부분의 프린터에서는 봉투에 직접 인쇄할 수 있습니다. 예를 들면 많은 사람에게 우편물을 보낼 때 특히 쓸만합니다.</p>

  <section id="envelope">
    <title>봉투 상단 인쇄</title>

  <p>봉투 위에 인쇄할 때 확인해야 할 두가지 요건이 있습니다.</p>
  <p>첫번째는 프린터에서 봉투 규격을 아는지 여부입니다. <keyseq><key>Ctrl</key><key>P</key></keyseq> 키를 눌러 인쇄 대화상자를 엽니다. 가능하다면 <gui>용지 설정</gui> 탭에 들어갔을 때 <gui>용지 형식</gui>에서 “Envelope(봉투)”를 선택합니다. 선택이 안된다면, <gui>용지 규격</gui>을 봉투 크기(예: C5)로 바꿀 수 있는지 확인해봅니다. 봉투 포장에 어떤 규격인지 나타납니다. 대부분의 봉투는 표준 규격으로 나옵니다.</p>

  <p>두번째로 프린터의 용지함에 올바른 방향으로 봉투를 올려놓았는지 확인해보아야 합니다. 프린터 설명서를 확인하거나, 단일 봉투를 넣고 인쇄하여 어떤 면으로 인쇄하는지 어떤 방향이 올바른 방향인지 인쇄해봅니다.</p>

  <note style="warning">
    <p>특히 일부 레이저 프린터 같은 경우는 봉투를 바로 인쇄할 수 있게 설계하지 않았습니다. 프린터 설명서를 확인하여 봉투 인쇄가 가능한지 확인합니다. 그렇지 않으면 봉투를 용지로 공급했을 때 기계적 고장을 유발할 수 있습니다.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
