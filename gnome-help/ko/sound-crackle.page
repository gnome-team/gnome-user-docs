<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="ko">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>음향 케이블과 사운드 카드 드라이버를 확인합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>소리를 재생할 때 지직거리는 소리를 들어요</title>

  <p>컴퓨터에서 소리를 재생할 때 지직거리는 소리를 들을 경우 음향 케이블 또는 커넥터의 문제거나 사운드카드 드라이버의 문제일 수 있습니다.</p>

<list>
 <item>
  <p>스피커를 제대로 연결했는지 확인합니다.</p>
  <p>스피커를 제대로 연결하지 않았거나 잘못된 소켓에 연결했다면, 지직거리는 소리를 들을 수 있습니다.</p>
 </item>

 <item>
  <p>스피커/헤드폰 케이블이 끊어지지 않았는지 확인합니다.</p>
  <p>음향 케이블과 커넥터는 사용 과정에서 조금씩 마모될 수 있습니다. 케이블을 꼽아보거나 헤드폰을 다른 오디오 장치(MP3 재생기, CD 재생기)에 연결하여 깨지는 음이 들리는지 확인합니다. 만약 깨지는 음이 난다면 케이블이나 헤드폰을 바꿔야 할 수 있습니다.</p>
 </item>

 <item>
  <p>사운드 드라이버 상태가 별로 좋지 않은지 확인합니다.</p>
  <p>리눅스 운영체제에서 일부 사운드 카드는 상태가 양호하지 않은 드라이버를 활용하기 때문에 제대로 동작하지 않습니다. 이 문제를 인지하기엔 좀 어렵습니다. 인터넷에서 사운드 카드 제조사 및 모델을 검색하면서 검색 단어에 “Linux”를 추가하여 동일 증상을 누군가가 겪고 있는지 확인합니다.</p>
  <p><cmd>lspci</cmd> 명령으로 사운드 카드 추가 정보를 확인할 수 있습니다.</p>
 </item>
</list>

</page>
