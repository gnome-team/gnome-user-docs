<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="ko">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    	<revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>화면 해상도를 제대로 설정하지 않은 것 같습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>왜 화면이 찌그러지거나 각진 모양이 보일까요?</title>

  <p>설정한 디스플레이 해상도가 화면에 맞지 않을 수 있습니다. 이 문제를 해결하려면:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>디스플레이</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>디스플레이</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>일부 <gui>해상도</gui> 옵션을 시도해보고 화면을 더욱 맵시있게 보여주는 옵션을 선택합니다.</p>
    </item>
  </steps>

<section id="multihead">
  <title>여러 디스플레이를 연결했을 때</title>

  <p>컴퓨터에 두 대의 디스플레이를 연결했다면(예를 들어, 일반 모니터와 프로젝터), 디스플레이는 다른 방식의 최적화 값을 갖거나 <link xref="look-resolution#native">자체</link> 해상도 값을 갖습니다.</p>

  <p><link xref="display-dual-monitors#modes">동일 화면</link> 모드를 활용하여 두 화면을 동일한 화면으로 나타낼 수 있습니다. 두 화면은 각 화면과 맞지 않을 수도 있는 동일한 해상도 설정을 사용하므로, 두 화면의 이미지 선명도를 떨어뜨릴 수도 있습니다.</p>

  <p><link xref="display-dual-monitors#modes">디스플레이 연결</link> 모드에서 각 화면의 해상도를 개별적으로 설정할 수 있어 자체 해상도를 설정할 수 있습니다.</p>

</section>

</page>
