<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="ko">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>프린터 카트리지에 남은 잉크 또는 토너의 양을 확인합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>프린터 잉크 또는 토너 수준을 어떻게 확인할 수 있을까요?</title>

  <p>잉크 또는 토너가 프린터에 얼마나 남아있는지 확인하는 방법은 프린터의 제조사 및 모델, 컴퓨터에 설치한 드라이버 및 프로그램에 따라 다릅니다.</p>

  <p>일부 프린터는 내장 화면으로 잉크 수준과 기타 정보를 표시합니다.</p>

  <p>일부 프린터는 토너 또는 잉크 잔량 수준을 컴퓨터에 보고하여 <app>설정</app>의 <gui>프린터</gui> 창에서 확인할 수 있습니다. 잉크 잔량은 정보를 볼 수 있을 경우 프린터 새부 정보에 나타납니다.</p>

  <p>대부분의 HP 드라이버에서 제공하는 드라이버와 상태 도구는 HP 리눅스 이미징 및 인쇄(HPLIP) 프로젝트에서 제공합니다. 다른 제조사에서도 비슷한 기능을 지닌 상업용 드라이버를 제공할지도 모릅니다.</p>

  <p>대신 잉크 잔량 수준을 확인하는 프로그램을 설치할 수 있습니다. <app>Inkblot</app> 프로그램은 대부분의 HP, 엡손, 캐논 프린터의 잉크 상태를 보여줍니다. 프린터가 <link href="http://libinklevel.sourceforge.net/#supported">지원 모델 목록</link>에 있는지 확인해봅니다. 엡손과 기타 프린터용 잉크 잔량 수준 표시 프로그램으로 <app>mtink </app>가 있습니다.</p>

  <p>일부 프린터는 리눅스를 제대로 지원하지 않으며, 다른 기종의 경우 잉크 수준 보고를 하지 않게 설계하기도 합니다.</p>

</page>
