<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-contrast" xml:lang="ko">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>화면의 창과 단추를 더(또는 덜) 도드라지게 하여 쉽게 볼 수 있게 합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>대비 조절</title>

  <p>쉽게 볼 수 있도록 창과 단추의 대비를 조절할 수 있습니다. 창 전체의 밝기를 조절하는 개념과는 다릅니다. 오직 <em>사용자 인터페이스</em>만 바뀝니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>접근성</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>Select the <gui>Seeing</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>High Contrast</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>고대비 간단하게 켜고 끄기</title>
    <p>상단 표시줄의 <link xref="a11y-icon">접근성 아이콘</link>을 누른 후 <gui>고대비</gui>를 선택하여 고대비 효과를 간단하게 켜고 끌 수 있습니다.</p>
  </note>

</page>
