<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="ko">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>접근성 메뉴는 사람 모양을 지난 상단 표시줄의 아이콘입니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>접근성 메뉴 찾기</title>

  <p>The <em>accessibility menu</em> is where you can turn on some of the
  accessibility settings. You can find this menu by clicking the icon which
  looks like a person on the top bar.</p>

  <figure>
    <desc>접근성 메뉴는 상단 표시줄에서 찾을 수 있습니다.</desc>
        <media its:translate="no" type="image" src="figures/classic-topbar-accessibility.svg"/>
  </figure>

  <p>접근성 메뉴가 나타나지 않으면 <gui>접근성</gui> 설정 창에서 켤 수 있습니다:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>접근성</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>접근성</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>Switch the <gui>Accessibility Menu</gui> switch to on.</p>
    </item>
  </steps>

  <p>To access this menu using the keyboard rather than the mouse, press
  <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> to move the
  keyboard focus to the top bar. The
  <gui xref="shell-introduction#activities">Activities</gui> button in the
  top-left corner will get highlighted — this tells you which item on the top
  bar is selected. Use the arrow keys on the keyboard to move the selected item
  to the accessibility menu icon and then press <key>Enter</key> to open it.
  You can use the up and down arrow keys to select items in the menu. Press
  <key>Enter</key> to toggle the selected item.</p>

</page>
