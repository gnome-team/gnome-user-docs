<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="cs">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přidělením oprávnění správce umožnit uživatelům provádět změny systému.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Změna osob s oprávněním správce</title>

  <p>Správcovská oprávnění jsou způsobem, jak rozhodovat, kdo může provádět změny v důležitých částech systému. Můžete měnit, kteří uživatelé mají oprávnění správce, a kteří ne. Je to dobrý způsob, jak udržet váš systém bezpečný a jak zabránit potenciálním neoprávněným změnám vedoucím k jeho poškození.</p>

  <p>Abyste mohli změnit typ účtu, potřebujete <link xref="user-admin-explain">oprávnění správce</link>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Odemknout</gui> v pravém horním rohu a po vyzvání zadejte heslo.</p>
    </item>
    <item>
      <p>Under <gui>Other Users</gui>, select the user whose privileges you want
      to change.</p>
    </item>
    <item>
      <p>Set the <gui>Administrator</gui> switch to on.</p>
    </item>
    <item>
      <p>Nová oprávnění uživatele se použijí, až se příště přihlásí.</p>
    </item>
  </steps>

  <note>
    <p>První uživatelský účet v systému je obvykle ten, který má oprávnění správce. Jedná se o uživatelský účet, který byl vytvořen při instalaci systému.</p>
    <p>Není rozumné mít v jednom systému příliš uživatelů s oprávněními <gui>Správce</gui>.</p>
  </note>

</page>
