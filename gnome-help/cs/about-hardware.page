<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hardware" xml:lang="cs">

  <info>
    <link type="guide" xref="about" group="hardware"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="draft" date="2024-03-02"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Discover information about the hardware installed on your
    system.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Discover information about your system hardware</title>

  <p>Knowing about what hardware is installed may help you understand if new
  software or hardware will be compatible with your system.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>System</gui>.</p>
    </item>
    <item>
     <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
     results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>About</gui> to open the panel.</p>
    </item>
    <item>
      <p>Look at the information that is listed under <gui>Hardware
      Model</gui>, <gui>Processor</gui>, <gui>Memory</gui> and so on.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Information can be copied from each item by selecting it and then
    copying to the clipboard. This makes it easy to share information about
    your system with others.</p>
  </note>

</page>
