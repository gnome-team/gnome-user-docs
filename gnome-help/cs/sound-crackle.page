<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="cs">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zkontrolujte audiokabely a ovladače zvukové karty.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Při přehrávání zvuku slyším praskání a bzučení</title>

  <p>Pokud při přehrávání zvuku na svém počítači slyšíte praskání nebo bzučení, máte nejspíše problém s audiokabelem nebo konektorem a nebo s ovladačem zvukové karty.</p>

<list>
 <item>
  <p>Ujistěte se, že jsou správně připojené reproduktory.</p>
  <p>Když konektor od reproduktorů není pořádně zasunutý nebo je zasunutý do nesprávné zdířky, můžete slyšet bzučivý zvuk.</p>
 </item>

 <item>
  <p>Zkontrolujte, jestli není poškozený kabel k reproduktorům/sluchátkům.</p>
  <p>Zvukové kabely a konektory se používáním postupně opotřebovávají. Zkuste zapojit kabel nebo sluchátka do jiného zvukového zařízení (jako je přehrávače MP3 nebo přehrávač CD), abyste si ověřili, jestli i tam uslyšíte praskavý zvuk. Pokud ano, měli byste kabel či sluchátka vyměnit.</p>
 </item>

 <item>
  <p>Zkontrolujte, jestli má ovladač zvukové karty patřičnou kvalitu.</p>
  <p>Některé zvukové karty nefungují v Linuxu úplně nejlépe, protože nemají kvalitní ovladače. Odhalit takovýto problém je náročnější. Zkuste vyhledat výrobce a model své zvukové karty na Internetu a zároveň hledejte výraz „Linux“, abyste zjistili, jestli obdobný problém nemají i jiní.</p>
  <p>Abyste získali o zvukové kartě více informací, můžete použít příkaz <cmd>lspci</cmd>.</p>
 </item>
</list>

</page>
