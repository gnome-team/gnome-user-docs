<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="cs">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Jak zkontrolovat, jestli není souborový systém poškozený a jak jej případně dostat do použitelného stavu.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Oprava poškozeného souborového systému</title>

  <p>Souborový systém může být poškozený z důvodu nenadálého výpadku napájení, zhroucení systému nebo vyjmutí média v průběhu zápisu. Po takovýchto nehodách je doporučeno souborový systém <em>opravit</em> nebo aspoň <em>zkontrolovat</em>, aby se předešlo ztrátě dat.</p>
  <p>Občas je oprava požadována kvůli připojení nebo změnám souborového systému. I když <em>kontrola</em> nehlásí žádné poškození, může být souborový systém interně označený jako „problémový“ a potřebuje opravu.</p>

<steps>
  <title>Kontrola, jestli není souborový systém poškozený</title>
  <item>
    <p>Z přehledu <gui>Činnosti</gui> otevřete <app>Disky</app>.</p>
  </item>
  <item>
    <p>V seznamu úložných zařízení nalevo vyberte disk, který obsahuje dotčený souborový systém. Pokud je na disku více svazků, vyberte svazek, který jej obsahuje.</p>
  </item>
  <item>
    <p>Na nástrojové liště pod částí <gui>Svazky</gui> klikněte na nabídkové tlačítko. Pak klikněte na <gui>Zkontrolovat souborový systém…</gui>.</p>
  </item>
  <item>
    <p>V závislosti na tom, kolik je v souborovém systému uloženo dat, může kontrola zabrat hodně času. Její spuštění potvrďte v dialogovém okně, které se objeví.</p>
   <p>Při této činnosti se v souborovém systému neprovádí žádné změny, ale v případě potřeby se může odpojit. Trpělivě vyčkejte na konec kontroly.</p>
  </item>
  <item>
    <p>Po dokončení kontroly budete informováni, jestli je souborový systém poškozený. Pamatujte, že v některých případech může být souborový systém nepoškozený a přesto požadovat opravu, aby se odstranil příznak „poškození“.</p>
  </item>
</steps>

<note style="warning">
 <title>Riziko ztráty dat při opravě</title>
  <p>Když je poškozená struktura souborového systému, může to zasáhnout soubory v něm uložené. V některých případech nemusí být možné dostat je zpět do správného stavu a mohou být smazány nebo přesunuty do speciální složky. Ta se normálně nazývá <em>lost+found</em> (ztracené a nalezené) a nachází se v nejvyšší úrovni souborového systému. Najdete v ní soubory, které se podařilo při opravě obnovit.</p>
  <p>Jestliže jsou data příliš cenná na to, než abyste riskovali jejich ztrátu, doporučujeme vám před opravou pořídit zálohu v podobě obrazu disku.</p>
  <p>Tento obraz se dá podrobit analýze nástroji, jako je <app>sleuthkit</app>, které mohou obnovit chybějící soubory a části dat, které se nepodařilo obnovit během normální opravy. Umí také obnovit dříve smazané soubory.</p>
</note>

<steps>
  <title>Oprava souborového systému</title>
  <item>
    <p>Z přehledu <gui>Činnosti</gui> otevřete <app>Disky</app>.</p>
  </item>
  <item>
    <p>V seznamu úložných zařízení nalevo vyberte disk, který obsahuje dotčený souborový systém. Pokud je na disku více svazků, vyberte svazek, který jej obsahuje.</p>
  </item>
  <item>
    <p>Na nástrojové liště pod částí <gui>Svazky</gui> klikněte na nabídkové tlačítko. Pak klikněte na <gui>Opravit souborový systém…</gui>.</p>
  </item>
  <item>
    <p>V závislosti na tom, kolik je v souborovém systému uloženo dat, může oprava zabrat hodně času. Její spuštění potvrďte v dialogovém okně, které se objeví.</p>
   <p>V průběhu se v případě potřeby může souborový systém odpojit. Cílem opravy je dostat souborový systém do konzistentního stavu a soubory, které jsou poškozené přesunout do speciální složky. Během opravy buďte prosím trpěliví.</p>
  </item>
  <item>
    <p>Po dokončení budete informování, zda se souborový systém podařilo opravit. V případě úspěchu jej můžete opět normálně používat.</p>
    <p>Pokud se souborový systém opravit nepodařilo, zazálohujte jej uložením obrazu svazku, abyste později mohli získat důležité soubory. To se dá po připojení obrazu jen pro čtení nebo použitím analytických nástrojů, jako je <app>sleuthkit</app>.</p>
    <p>Abyste neopravitelný svazek znovu zprovoznili, musíte jej <link xref="disk-format">zformátovat</link> novým souborovým systémem. Při tom dojde ke ztrátě všech dat.</p>
  </item>
</steps>

</page>
