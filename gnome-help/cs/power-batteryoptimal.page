<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="cs">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Rady jako „Nenechte baterii vybít úplně do dna“</desc>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Vymáčkněte z baterie svého notebooku co nejvíce</title>

<p>Jak baterie notebooku stárne, snižuje se její schopnost uchovávat energii a její kapacita postupně klesá. Existuje pár postupů, které můžete použít k prodloužení její životnosti, ale nečekejte od toho žádné zázraky.</p>

<list>
  <item>
    <p>Nenechávejte baterii úplně vybít. Vždy dobíjejte <em>dříve</em>, než je baterie úplně na nule, i když má většina baterií zabudovanou ochranu bránící v jejím úplném vybití. Dobíjení, když je baterie vybitá jen z části je účinnější. Na druhou stranu dobíjení jen velmi lehce vybité baterie pro ni také není dobré.</p>
  </item>
  <item>
    <p>Teplo má neblahý vliv na účinnost dobíjení baterie. Nenechávejte baterii zbytečně zahřívat.</p>
  </item>
  <item>
    <p>Baterie stárnou, i když leží nepoužité v původním balení. Nemá cenu kupovat náhradní baterii zároveň s původní baterii – ponechte nákup až na dobu, kdy bude skutečně zapotřebí.</p>
  </item>
</list>

<note>
  <p>Tyto rady se týkají lithium iontových baterií (Li-Ion), které jsou dnes nejběžnější. Jiné typy baterií mohou vyžadovat jiný způsob zacházení.</p>
</note>

</page>
