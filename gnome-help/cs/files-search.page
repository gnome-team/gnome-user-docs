<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="cs">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>
    <revision pkgversion="3.36" date="2020-04-18" status="draft"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">Hledání</title>
    <desc>Jak najít soubory podle jejich názvu a typu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Hledání souborů</title>

  <p>Přímo ve správci souborů můžete hledat soubory podle jejich názvu nebo typu.</p>

  <links type="topic" style="linklist">
    <title>Další vyhledávací aplikace</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Hledání</title>
    <item>
      <p>Z přehledu <gui xref="shell-introduction#activities">Činnosti</gui> otevřete aplikaci <app>Soubory</app>.</p>
    </item>
    <item>
      <p>V případě, že víte, ve které konkrétní složce se požadované soubory nachází, přejděte do ní.</p>
    </item>
    <item>
      <p>Napište slovo nebo slova, o kterých víte, že se v názvu souboru nachází, a ty se objeví ve vyhledávací liště. Například, když vaše soubory s fakturami začínají slovem „Faktura“, napište <input>faktura</input>. Slova se porovnávají bez ohledu na velikost písmen.</p>
      <note>
        <p>Místo abyste začali přímo psát, můžete si vyhledávací lištu zobrazit předem kliknutím na tlačítko <media its:translate="no" type="image" mime="image/svg" src="figures/nautilus-folder-search-symbolic.svg"> <key>Search</key> key symbol
        </media> na nástrojové liště nebo zmáčknutím <keyseq><key>Ctrl</key> <key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>Výsledky hledání můžete zúžit zadáním data, typu souboru a rozlišením, jestli se hledá v celém textu obsaženém v souborech nebo jen v názvech souborů.</p>
      <p>Když chcete použít filtry, zmáčkněte tlačítko rozbalovací nabídky vedle ikony <media its:translate="no" type="image" mime="image/svg" src="figures/nautilus-folder-search-symbolic.svg"> <key>Search</key> key symbol</media> správce souborů a vyberte si z nabízených filtrů:</p>
      <list>
        <item>
          <p><gui>Kdy</gui>: Jak daleko do minulosti hledat?</p>
        </item>
        <item>
          <p><gui>Co</gui>: Jaký typ položky?</p>
        </item>
        <item>
          <p><gui>Search</gui>: Should your search include a full-text search,
          or search only the file names?</p>
        </item>
      </list>
    </item>
    <item>
      <p>Když chcete filtr odstranit, zmáčkněte <gui>×</gui>, který se vedle něj nachází.</p>
    </item>
    <item>
      <p>Soubory ve výsledcích hledání můžete otevírat, kopírovat, mazat a i jinak s nimi pracovat stejným způsobem, jako byste byli v běžné složce ve správci souborů.</p>
    </item>
    <item>
      <p>Opětovným kliknutím na tlačítko <media its:translate="no" type="image" mime="image/svg" src="figures/nautilus-folder-search-symbolic.svg"> <key>Search</key> key symbol
      </media> na nástrojové liště hledání opustíte a vrátíte se do složky.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Přizpůsobení hledání souborů</title>

<p>Můžete chtít, aby některé složky byly v hledání v aplikaci <app>Soubory</app> zahrnuté a jiné naopak vyjmuté. Když si chcete přizpůsobit, které složky se mají prohledávat:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Hledání</gui>.</p>
    </item>
    <item>
      <p>Ve výsledcích vyberte <guiseq><gui>Nastavení</gui><gui>Hledání</gui></guiseq>. Tím se otevře panel <gui>Nastavení hledání</gui>.</p>
    </item>
    <item>
      <p>Select <gui>Search Locations</gui>.</p>
    </item>
  </steps>

<p>This will open a dialog which allows you toggle directory
searches on or off. You can toggle searches in each of the three sections:</p>

  <list>
    <item>
      <p><gui>Místa</gui>: seznam běžných složek v domovské složce</p>
    </item>
    <item>
      <p><gui>Záložky</gui>: seznam složek, které máte v záložkách aplikace <app>Soubory</app></p>
    </item>
    <item>
      <p><gui>Ostatní</gui>: seznam složek, které jste přidali tlačítkem <gui>+</gui>.</p>
    </item>
  </list>

</section>

</page>
