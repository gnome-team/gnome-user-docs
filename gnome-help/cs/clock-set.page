<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="cs">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>
    <revision version="gnome:46" status="review" date="2024-03-02"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ke změně data nebo času použijte <gui>Nastavení data a času</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Změna data a času</title>

  <p>Jestliže jsou datum a čas zobrazené v horní liště nesprávné, nebo mají nesprávný formát, můžete to změnit:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Date &amp; Time</gui> to open the panel.</p>
    </item>
    <item>
      <p>Pokud máte vypínač <gui>Datum a čas automaticky</gui> přepnutý do polohy zapnuto, měly by se vaše datum a čas v době, kdy jste připojeni k Internetu, automaticky aktualizovat. Abyste je mohli měnit ručně, musí být vypínač přepnutý do polohy vypnuto.</p>
    </item> 
    <item>
      <p>Klikněte na <gui>Datum a čas</gui> a následně čas a datum upravte.</p>
    </item>
    <item>
      <p>Výběrem z formátů <gui>24hodinový</gui> a <gui>AM/PM</gui> ve <gui>Formát času</gui> můžete také změnit, jak budou zobrazeny hodiny.</p>
    </item>
  </steps>

  <p>Můžete si také přát <link xref="clock-timezone">nastavič časové pásmo ručně</link>.</p>

</page>
