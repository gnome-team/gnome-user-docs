<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="cs">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Jak provádět kliknutí pravým tlačítkem pomocí držení levého tlačítka.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Simulace kliknutí pravým tlačítkem</title>

  <p>Kliknutí pravým tlačítkem myši může provést delším podržením levého tlačítka. To je užitečné, když je pro vás obtížné pohybovat prsty na ruce nezávisle nebo když má vaše ukazovací zařízení jen jedno tlačítko.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Přístupnost</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Přístupnost</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Select the <gui>Pointing &amp; Clicking</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Click Assist</gui> section, switch the <gui>Simulated
      Secondary Click</gui> switch to on.</p>
    </item>
  </steps>

  <p>Úpravou <gui>Zpoždění přijetí</gui> můžete měnit, jak dlouho je potřeba držet levé tlačítko zmáčknuté, než je to považováno za kliknutí pravým tlačítkem.</p>

  <p>Abyste klikli pravým tlačítkem se simulovaným druhým kliknutím, držte levé tlačítko myši zmáčknuté tam, kde byste pravým tlačítkem klikli normálně. Ukazatel myši se během držení levého tlačítka začne vyplňovat jinou barvou. Jakmile změní barvu celý, uvolněte tlačítko a provede se dvojité kliknutí pravým tlačítkem.</p>

  <p>Některé speciální ukazatele, jako je ukazatel při změně velikosti, barvu nemění. I tak můžete normálně používat simulaci druhého kliknutí, akorát nebudete mít vizuální zpětnou odezvu od ukazatele.</p>

  <p>Pokud používáte <link xref="mouse-mousekeys">myš z klávesnice</link>, můžete také kliknout pravým tlačítkem držením zmáčknuté klávesy <key>5</key> na numerické klávesnici.</p>

  <note>
    <p>V přehledu <gui>Činností</gui> funguje dlouhé zmáčknutí stejně jako kliknutí pravým tlačítkem vždy, i když je tato funkce v systému vypnutá. V přehledu funguje dlouhé kliknutí mírně odlišně: nemusíte tlačítko ani uvolnit, aby se provedlo kliknutí pravým tlačítkem.</p>
  </note>

</page>
