<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-setup" xml:lang="cs">

  <info>
    <link type="guide" xref="contacts" group="#first"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="draft"/>
    <revision pkgversion="3.12" date="2014-02-26" status="final"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.22" date="2017-03-19" status="draft"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    
    <credit type="author copyright">
      <name>Paul Cutler</name>
      <email>pcutler@gnome.org</email>
      <years>2017</years>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak ukládat své kontakty do místního adresáře nebo do účtu on-line.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Prvotní spuštění Kontaktů</title>

  <p>Při prvním spuštění <app>Kontatků</app> se otevře okno <gui>Výběr adresáře</gui>.</p>
   
  <p>Pokud máte nastaveny <link xref="accounts">účty on-line</link>, jsou vypsány spolu s <gui>Místním adresářem</gui>. Vyberte některou položku v seznamu a zmáčkněte tlačítko <gui style="button">Hotovo</gui>.</p>
  
  <p>Všechny nové kontakty, které vytvoříte, se uloží do adresáře, který jste zvolili. Bude zároveň moci prohlížet, upravovat a mazat kontakty v ostatních adresářích.</p>
  
  <p>Pokud účty on-line nemáte nastaveny, zmáčkněte tlačítko <gui style="button">Účty on-line</gui>, aby se zahájilo jejich nastavení. Jestliže si nepřejete účty on-line nastavit v tuto chvíli, zmáčkněte tlačítko <gui style="button">Místní adresář</gui>.</p>

</page>
