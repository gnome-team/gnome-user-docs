<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="cs">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>
    <revision version="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak změnit jas obrazovky, aby byla čitelnější na ostrém světle.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Nastavení jasu obrazovky</title>

  <p>V závislosti na možnostech vašeho hardwaru, můžete snížit jas obrazovky, abyste šetřili energii, nebo jej naopak zvýšit, aby byla obrazovka čitelnější na jasném světle.</p>

  <p>Pro změnu jasu obrazovky otevřete <gui xref="shell-introduction#systemmenu">systémovou nabídku</gui> na pravé straně horní lišty a posuňte táhlo jasu obrazovky na hodnotu, která vám vyhovuje. Nastavení by se mělo projevit okamžitě.</p>

  <note style="tip">
    <p>Řada notebooků má na klávesnici k úpravě jasu obrazovky speciální klávesy. Většinou je na nich symbol sluníčka. Tyto klávesy použijte za současného držení klávesy <key>Fn</key>.</p>
  </note>

  <note style="tip">
    <p>Pokud má počítač zabudovaný světelný senzor, bude se vám jas obrazovky automaticky přizpůsobovat okolním světelným podmínkám. Více informací viz <link xref="power-autobrightness"/>.</p>
  </note>

  <p>Když funguje nastavení jasu obrazovky, je také možné nechat automaticky ztlumit jas obrazovky kvůli úspoře energie. Další informace viz <link xref="power-whydim"/>.</p>

</page>
