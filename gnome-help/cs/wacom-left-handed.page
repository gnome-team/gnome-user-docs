<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-left-handed" xml:lang="cs">

  <info>
    <revision version="gnome:46" date="2024-03-11" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přepínat grafický tablet pro leváky a praváky.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Používání tabletu levou a pravou rukou</title>

  <p>Některé tablety mají hardwarová tlačítka jen u jednoho okraje. Tablet je možné otočit o 180 stupňů, aby tlačítka byly na požadované straně. Výchozí otočení je připravené pro praváky. Když chcete přepnout tablet pro leváky:</p>

<steps>
  <item>
    <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Grafický tablet</gui>.</p>
  </item>
  <item>
    <p>Kliknutím na položku <gui>Grafický tablet</gui> otevřete příslušný panel.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click
    <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
  </item>
  <item>
    <p>Set the <gui>Left Hand Orientation</gui> switch to on.</p>
  </item>
</steps>

</page>
