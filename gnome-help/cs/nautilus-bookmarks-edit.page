<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="cs">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>
    <revision version="gnome:45" date="2024-03-03" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak ve správci souborů přidat, odebrat a přejmenovat záložky.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Úprava záložky pro složku</title>

  <p>Vaše záložky jsou vypsány v postranním panelu správce souborů.</p>

  <steps>
    <title>Přidání záložky:</title>
    <item>
      <p>Otevřete složku (nebo umístění), které chcete mít v záložkách.</p>
    </item>
    <item>
      <p>Klikněte na aktuální složku v adresní liště a vyberte <gui style="menuitem">Přidat do záložek</gui>.</p>
      <note>
        <p>Můžete také složku přetáhnout do postranního panelu a upustit na popisek <gui>Nová záložka</gui>, který se dynamicky objeví.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Odebrání záložky:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Remove from Bookmarks</gui> from the menu.</p>
    </item>
  </steps>

  <steps>
    <title>Přejmenování záložky:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Rename</gui>.</p>
    </item>
    <item>
      <p>Do textového pole <gui>Název</gui> napište nový název záložky.</p>
      <note>
        <p>Přejmenováním záložky se nepřejmenuje složka. Pokud máte záložky pro dvě různé složky na dvou různých místech, ale obě složky mají stejný název, budou se obě záložky nazývat stejně a nedokážete je od sebe odlišit. V takovém případě je právě vhodné dát záložce jiný název než má složka, na kterou ukazuje.</p>
      </note>
    </item>
  </steps>

</page>
