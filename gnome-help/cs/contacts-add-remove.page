<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="cs">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přidat a odebrat kontakt v místním adresáři.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Přidání a odebrání kontaktu</title>

  <p>Když chcete přidat kontakt:</p>

  <steps>
    <item>
      <p>Zmáčkněte tlačítko <gui style="button">+</gui>.</p>
    </item>
    <item>
      <p>V dialogovém okně <gui>Nový kontakt</gui> zadejte jméno kontaktu a jeho údaje. V rozbalovacím seznam u každého z polí zvolte typ jednotlivého údaje.</p>
    </item>
    <item>
      <p>Když chcete přidat prodrobnější údaje, zmáčkněte volbu <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Zobrazit více</span></media>.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Přidat</gui> kontakt uložte.</p>
    </item>
  </steps>

  <p>Když chcete odebrat kontakt:</p>

  <steps>
    <item>
      <p>Vyberte kontakt v seznamu kontaktů.</p>
    </item>
    <item>
      <p>Zmáčkněte tlačítko <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Zobrazit více</span></media> v pravém horním rohu v záhlavní liště.</p>
    </item>
    <item>
      <p>Zmáčknutím volby <gui style="menu item">Smazat</gui> kontakt odstraníte.</p>
    </item>
   </steps>
    <p>Když chcete odstranit jeden, nebo více kontaktů, zaškrtněte políčka vedle nich a zmáčkněte tlačítko <gui style="button">Odstranit</gui></p>
</page>
