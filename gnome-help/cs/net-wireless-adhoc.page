<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="cs">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.32" date="2019-09-01" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Jak vytvořit dočasnou síť (Ad-hoc), která umožní ostatním zařízením připojit se k vašemu počítači a využít jeho síťová připojení.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Vytvoření bezdrátového přístupového bodu</title>

  <p>Můžete svůj počítač použít jako bezdrátový přístupový bod. Tím umožníte ostatním zařízením připojit se k vám, aniž by potřebovaly zvláštní síť, a vy s nimi můžete sdílet internetové připojení, které máte přes jiné rozhraní, jako je drátová síť nebo mobilní síť.</p>

<steps>
  <item>
    <p>Otevřete <gui xref="shell-introduction#systemmenu">systémovou nabídku</gui> vpravo na horní liště.</p>
  </item>
  <item>
    <p>Select the arrow of
    <gui>Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>All Networks</gui>.</p></item>
  <item><p>Zmáčkněte tlačítko nabídky v pravém horním rohu okna a vyberte <gui>Zapnout bezdrátový přístupový bod…</gui></p></item>
  <item><p>Pokud jste právě připojeni k bezdrátové síti, budete dotázáni, jestli se chcete od sítě odpojit. Jeden bezdrátový adaptér může být v jeden okamžik připojen jen k jedné síti nebo jednu síť vytvářet. Kliknutím na <gui>Zapnout</gui> odpojení potvrďte.</p></item>
</steps>

<p>Název sítě (SSID) a bezpečnostní klíč budou automaticky vygenerovány. Název sítě bude vycházet z názvu vašeho počítače. Ostatní zařízení budou tyto informace potřebovat, aby se mohly připojit k přístupovému bodu, který jste právě vytvořili.</p>

<note style="tip">
  <p>K bezdrátové síti se můžete připojit rovněž naskenováním kódu QR na svém telefonu nebo tabletu pomocí vestavěné kamery nebo skeneru kódů QR.</p>
</note>

</page>
