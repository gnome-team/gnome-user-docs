<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wrongnetwork" xml:lang="cs">
  <info>

    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-connect"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovář</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak upravit nastavení připojení a odebrat nechtěné volby připojení.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Můj počítač se připojuje k nesprávné síti</title>

  <p>Když zapnete počítač, pokusí se automaticky připojit k bezdrátovým sítím, ke kterým jste se již připojili někdy v minulosti. Pokud se tak připojuje k síti, ke které si připojovat nechcete, použije postup uvedený dále.</p>

  <steps>
    <title>Když chcete zapomenout některá bezdrátové připojení:</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Najděte síť, ke které se již dále <em>nechcete</em> připojovat.</p>
    </item>
    <item>
      <p>Zmáčkněte tlačítko <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">nastavení</span></media> vedle názvu sítě.</p></item>
    <item>
      <p>Zmáčkněte tlačítko <gui>Zapomenout připojení</gui>. Počítač se již napříště nebude pokoušet k této síti připojit.</p>
    </item>
  </steps>

  <p>Jestli se později budete chtít do sítě, kterou jste nechali zapomenout, znovu připojit, postupujte podle kroků v <link xref="net-wireless-connect"/>.</p>

</page>
