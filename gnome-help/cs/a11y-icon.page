<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="cs">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Nabídka přístupnosti je pod ikonou v podobě panáčka na horní liště.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Vyhledání nabídky přístupnosti</title>

  <p>The <em>accessibility menu</em> is where you can turn on some of the
  accessibility settings. You can find this menu by clicking the icon which
  looks like a person on the top bar.</p>

  <figure>
    <desc>Nabídku přístupnosti najdete na horní liště.</desc>
        <media its:translate="no" type="image" src="figures/classic-topbar-accessibility.svg"/>
  </figure>

  <p>Pokud nabídku přístupnosti nevidíte, můžete ji zapnout v panelu nastavení <gui>Přístupnost</gui>:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Přístupnost</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na položku <gui>Přístupnost</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Accessibility Menu</gui> switch to on.</p>
    </item>
  </steps>

  <p>To access this menu using the keyboard rather than the mouse, press
  <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> to move the
  keyboard focus to the top bar. The
  <gui xref="shell-introduction#activities">Activities</gui> button in the
  top-left corner will get highlighted — this tells you which item on the top
  bar is selected. Use the arrow keys on the keyboard to move the selected item
  to the accessibility menu icon and then press <key>Enter</key> to open it.
  You can use the up and down arrow keys to select items in the menu. Press
  <key>Enter</key> to toggle the selected item.</p>

</page>
