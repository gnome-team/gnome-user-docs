<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-turn-on-off" xml:lang="cs">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zapnout nebo vypnout zařízení Bluetooth na svém počítači.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Zapnutí nebo vypnutí Bluetooth</title>

  <p>Bluetooth můžete mít zapnuté, abyste se mohli připojit k jinému zařízení Bluetooth, nebo vypnuté, abyste šetřili energii. Zapnete jej následovně:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Bluetooth</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vypínač nahoře přepněte do polohy zapnuto.</p>
    </item>
  </steps>

  <p>Řada notebooků má hardwarový vypínač nebo kombinaci kláves, kterým lze Bluetooth zapnout a vypnout. Podívejte se po hardwarovém vypínači nebo příslušné klávese na klávesnici. Klávesa je často v kombinaci s pomocnou klávesou <key>Fn</key>.</p>

  <p>Když chcete Bluetooth vypnout:</p>
  <steps>
    <item>
      <p>Otevřete <gui xref="shell-introduction#systemmenu">systémovou nabídku</gui> vpravo na horní liště.</p>
    </item>
    <item>
      <p>Select
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/>
      Bluetooth</gui>.</p>
    </item>
  </steps>

  <note><p>Váš počítač je <link xref="bluetooth-visibility">viditelný</link> po celo dobu, co je otevřen panel <gui>Bluetooth</gui>.</p></note>

</page>
