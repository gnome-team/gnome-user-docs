<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="cs">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Sdílení profilů barev není nikdy dobrý nápad, protože hardware s postupem času mění své parametry.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Mohu sdílet své profily barev?</title>

  <p>Profil barev, který jste vytvořili, je specifický pro daný hardware a světelné podmínky, za kterých jste kalibraci prováděli. Displej, který byl v provozu stovky hodin, bude mít zcela rozdílný profil barev, než stejný model displeje s hned následujícím sériovým číslem, který ale svítil již několik tisíc hodin.</p>
  <p>To znamená, že když budete svůj profil s někým sdílet, může se jeho zařízení <em>přiblížit</em> kalibraci, ale je přinejmenším zavádějící tvrdit, že displej je zkalibrován.</p>
  <p>Obdobně, pokud všichni nemají v místnosti, kde si zobrazují a upravují fotky, doporučené řízené osvětlení (bez slunečního svitu z okna, černé stěny, žárovky s denním světlem atd.), nedává sdílení profilu, který jste si vytvořili za svých konkrétních světelných podmínek, smysl.</p>

  <note style="warning">
    <p>U profilů stažených z webu výrobce nebo vytvořených pro vaši potřebu byste měli pečlivě přezkoumat rozložení podmínek.</p>
  </note>

</page>
