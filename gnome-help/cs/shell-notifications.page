<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="cs">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="40" date="2021-09-10" status="review"/>

    <credit type="author">
      <name>Marina Zhurakhinskaya</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovář</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zprávy, které vyskakují z horní části obrazovky, vás informují, když nastanou určité události.</desc> 
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Upozornění a seznam upozornění</title>

<section id="what">
  <title>Co je to upozornění?</title>

  <p>Když chce některá aplikace nebo systémová komponenta získat vaši pozornost, zobrazí se upozornění u horní části obrazovky nebo na uzamknuté obrazovce.</p>

  <p>Například, když dostanete novou zprávu v diskuzi nebo nový e-mail, budo vás o tom upozornění informovat. Upozornění z diskuzí jsou pojata trochu jinak a jsou podána podle jednotlivých kontaktů, které vám zprávy poslaly.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>Jiná upozornění mohou mít tlačítka s volbami. Abyste takovéto upozornění zavřeli bez použití některé z nabízených voleb, stačí kliknout na zavírací tlačítko.</p>

  <p>Kliknutí na zavírací tlačítko u některých upozornění způsobí jejích zmizení. Jiné, jako jsou od přehrávače Rhythmbox nebo od diskuzních aplikací, zůstanou skryté v seznamu upozornění.</p>

</section>

<section id="notificationlist">

  <title>Seznam upozornění</title>

  <p>Seznam upozornění vám poskytuje způsob, jak se vrátit ke svým upozorněním, až se vám to bude hodit. Objeví se po kliknutí na hodiny nebo po zmáčknutí <keyseq><key xref="keyboard-key-super">Super</key> <key>V</key></keyseq>. Obsahuje všechna upozornění, na která jste zatím nereagovali nebo která jsou v něm umístěna trvale.</p>

  <p>Upozoronění si můžete zobrazit kliknutím na něj v seznamu. Zavřít seznam upozornění můžete zmáčknutím <keyseq><key>Super</key> <key>V</key></keyseq> nebo klávesy <key>Esc</key>.</p>

  <p>Použijte tlačítko <gui>Vymazat vše</gui> k vyčištění seznamu upozornění.</p>

</section>

<section id="hidenotifications">

  <title>Skrývání upozornění</title>

  <p>Když na něčem pracujete a nechcete být obtěžováni, můžete upozornění vypnout.</p>

  <p>Můžete skrýt všechna upozornění tak, že otevřete seznam upozornění a v dolní části přepnete <gui>Nerušit</gui> do polohy zapnuto. Jiný možný postup je:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Upozornění</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Switch <gui>Do Not Disturb</gui> to on.</p>
    </item>
  </steps>

  <p>When <gui>Do Not Disturb</gui> is switched on, only very important
  notifications, such as when your battery is critically low, will pop up at
  the top of the screen. All notifications will still be available
  in the notification list when you display it (by clicking on the clock, or by
  pressing <keyseq><key>Super</key><key>V</key></keyseq>), and they will start
  popping up again when you switch <gui>Do Not Disturb</gui> back to off.</p>

  <p>Můžete také zakázat nebo znovu povolit upozornění pro jednotlivé aplikace v panelu <gui>Upozornění</gui>.</p>

</section>

<section id="lock-screen-notifications">

  <title>Skrývání upozornění na uzamknuté obrazovce</title>

  <p>Když je obrazovka uzamknutá, objevují se na ní upozornění. Můžete ji nastavit tak, aby tato upozornění zůstala z důvodu soukromí skrytá.</p>

  <steps>
    <title>Když chcete vypnout upozornění po dobu uzamknutí obrazovky:</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Upozornění</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item><p>Přepněte vypínač <gui>Upozornění na uzamknuté obrazovce</gui> do polohy vypnuto.</p></item>
  </steps>

</section>

</page>
