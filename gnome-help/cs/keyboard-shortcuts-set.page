<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="cs">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak definovat nebo změnit klávesové zkratky v nastavení <gui>Klávesnice</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Nastavení klávesových zkratek</title>

<p>Abyste změnili klávesu či klávesy, které se mají u klávesové zkratky mačkat:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Klávesnice</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>View and Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Vyberte požadovanou kategorii, nebo zadejte hledaný výraz.</p>
    </item>
    <item>
      <p>Klikněte na řádek s požadovanou činností. Objeví se okno <gui>Nastavení klávesové zkratky</gui>.</p>
    </item>
    <item>
      <p>Zmáčkněte požadovanou kombinaci kláves nebo použijte <key>Backspace</key> pro vrácení výchozí kombinace nebo <key>Esc</key> ke zrušení.</p>
    </item>
  </steps>


<section id="defined">
<title>Předdefinované klávesové zkratky</title>
  <p>K dispozici je řada přednastavených klávesových zkratek, které si může změnit. Jsou seskupeny do těchto kategorií:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Přístupnost</title>
  <tr>
	<td><p>Zmenšit velikost textu</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Zapnout nebo vypnout vysoký kontrast</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Zvětšit velikost textu</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Zapnout nebo vypnout klávesnici na obrazovce</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Zapnout nebo vypnout čtečku obrazovky</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zapnout nebo vypnout lupu</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přiblížit</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Oddálit</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Spouštění</title>
  <tr>
	<td><p>Domovská složka</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> nebo <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> nebo <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>Spustit kalkulačku</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> nebo <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>Spustit poštovního klienta</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> nebo <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>Spustit prohlížeč nápovědy</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Spustit webový prohlížeč</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> nebo <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> nebo <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Hledání</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> nebo <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>Nastavení</p></td>
	<td><p><key>Nástroje</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Pohyb v uživatelském prostředí</title>
  <tr>
	<td><p>Skrýt všechna normální okna</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přepnout na pracovní plochu vlevo</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na pracovní plochu vpravo</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno o jeden monitor dolů</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno o jeden monitor doleva</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno o jeden monitor doprava</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno o jeden monitor nahoru</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno o jednu pracovní plochu doleva</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno o jednu pracovní plochu doprava</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno na poslední pracovní plochu</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno na pracovní plochu 1</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno na pracovní plochu 2</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno na pracovní plochu 3</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno na pracovní plochu 4</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přepnout do jiné aplikace</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na systémový ovládací prvek</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na minulý systémový ovládací prvek</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na poslední pracovní plochu</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na pracovní plochu 1</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na pracovní plochu 2</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přepnout na pracovní plochu 3</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přepnout na pracovní plochu 4</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
        <td><p>Přepnout do jiného okna</p></td>
        <td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přepnout do minulého okna</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout do minulého okna aplikace</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout na jiné okno aplikace</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Snímky obrazovky</title>
  <tr>
	<td><p>Save a screenshot of a window</p></td>
	<td><p><keyseq><key>Alt</key><key>Print Screen</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of the entire screen</p></td>
	<td><p><keyseq><key>Shift</key><key>Print Screen</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Launch the screenshot tool</p></td>
	<td><p><key>Print Screen</key></p></td>
  </tr>
  <tr>
        <td><p>Pořídit krátkou nahrávku obrazovky</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Zvuk a multimédia</title>
  <tr>
	<td><p>Vysunout</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media>  (audiotlačítko vysunout)</p></td>
  </tr>
  <tr>
	<td><p>Spustit hudební přehrávač</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (audiotlačítko multimédia)</p></td>
  </tr>
  <tr>
	<td><p>Vypnout/zapnout mikrofon</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>Následující skladba</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media>  (audiotlačítko následující)</p></td>
  </tr>
  <tr>
	<td><p>Pozastavit přehrávání</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media>  (audiotlačítko pozastavit)</p></td>
  </tr>
  <tr>
	<td><p>Přehrát (nebo přehrát/pozastavit)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (audiotlačítko přehrát)</p></td>
  </tr>
  <tr>
	<td><p>Předchozí skladba</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (audiotlačítko předchozí)</p></td>
  </tr>
  <tr>
	<td><p>Zastavit přehrávání</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media>  (audiotlačítko zastavit)</p></td>
  </tr>
  <tr>
	<td><p>Snížit hlasitost</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (audiotlačítko snížit hlasitost)</p></td>
  </tr>
  <tr>
	<td><p>Vypnout zvuk</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (audiotlačítko vypnout zvuk)</p></td>
  </tr>
  <tr>
	<td><p>Zvýšit hlasitost</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (audiotlačítko zvýšit hlasitost)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Systém</title>
  <tr>
        <td><p>Zaměřit aktivní upozornění</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zamknout obrazovku</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zobrazit dialogové okno Vypnout</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Obnovit klávesové zkratky</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Zobrazit všechny aplikace</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zobrazit přehled činností</p></td>
	<td><p><key>Super</key></p></td>
  </tr>
  <tr>
	<td><p>Zobrazí seznam upozornění</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the system menu</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zobrazit výzvu k zadání příkazu</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Psaní</title>
  <tr>
  <td><p>Přepnout na následující vstupní zdroj</p></td>
  <td><p><keyseq><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Přepnout na předchozí vstupní zdroj</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>mezerník</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Okna</title>
  <tr>
	<td><p>Aktivovat nabídku okna</p></td>
	<td><p><keyseq><key>Alt</key><key>mezerník</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zavřít okno</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Skrýt okno</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Odsunout okno do pozadí za ostatní okna</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Maximalizovat okno</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximalizovat okno vodorovně</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Maximalizovat okno svisle</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přesunout okno</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vynést okno do popředí nad ostatní okna</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Když je okno zakryté vynést jej do popředí, jinak odsunout do pozadí</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Změnit velikost okna</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Obnovit velikost okna</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout režim celé obrazovky</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
	<td><p>Přepnout stav maximalizace</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Přepnout okno na všechny/jednu pracovní plochu</p></td>
	<td><p>nepřiřazeno</p></td>
  </tr>
  <tr>
        <td><p>Roztáhnout okno přes levou půlku obrazovky</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Roztáhnout okno přes pravou půlku obrazovky</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Vlastní klávesové zkratky</title>

  <p>Když si chcete nadefinovat své vlastní klávesové zkratky v nastavení <gui>Klávesové zkratky</gui>:</p>

  <steps>
    <item>
      <p>Vyberte <gui>Vlastní zkratky</gui>.</p>
    </item>
    <item>
      <p>Jestliže zatím neexistuje žádná vlastní klávesová zkratka, klikněte na tlačítko <gui style="button">Přidat klávesovou zkratku</gui>, jinak klikněte na tlačítko <gui style="button">+</gui>. Objeví se okno <gui>Přidání vlastní klávesové zkratky</gui>.</p>
    </item>
    <item>
      <p>Napište <gui>Název</gui>, podle kterého se klávesová zkratka pozná, a <gui>Příkaz</gui>, který spustí aplikaci. Například, když chcete klávesovou zkratku, která spustí <app>Rhythmbox</app>, můžete ji pojmenovat <input>Hudba</input> a použít příkaz <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Klikněte na tlačítko <gui style="button">Přidat klávesovou zkratku…</gui>. V okně <gui>Přidání vlastní klávesové zkratky</gui> podržte zmáčknoutou požadovanou kombinaci kláves.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Přidat</gui>.</p>
    </item>
  </steps>

  <p>Název příkazu, který jste zadali, by měl být platný systémový příkaz. Jestli funguje, si můžete ověřit po otevření Terminálu a jeho zadání v něm. Příkaz, který otevírá aplikace, nemusí mít nutně stejný název, jako ona aplikace.</p>

  <p>Když chcete změnit příkaz, který je přiřazený vlastní klávesové zkratce, klikněte na řádek s klávesovou zkratkou. Objeví se okno <gui>Nastavení vlastní klávesové zkratky</gui>, ve kterém můžete příkaz upravit.</p>

</section>

</page>
