<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="cs">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Proxy je prostředníkem ve webovém provozu. Používá se pro anonymní přístup k webům, k řízení provozu nebo z bezpečnostních důvodů.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Nastavení proxy</title>

<section id="what">
  <title>Co je to proxy?</title>

  <p><em>Webová proxy</em> filtruje webové stránky, na které si díváte, přijímá požadavky od vašeho webového prohlížeče, aby stránky a jejich prvky stáhla a následně je s dodržením předepsaných zásad předala zpět prohlížeči. Obvykle se používá ve firemním prostředí a u veřejných přístupových bodů, aby bylo možné řídit, na které webové stránky se můžete dívat, aby vám zabránila v přístupu k Internetu bez přihlášení a nebo kvůli bezpečnostní kontrole webových stránek.</p>

</section>

<section id="change">
  <title>Změna metody proxy</title>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Síť</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Síť</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V seznamu nalevo vyberte <gui>Proxy sítě</gui>.</p>
    </item>
    <item>
      <p>Zvolte, kterou metodu chcete používat:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Žádná</gui></title>
          <p>Aplikace budu používat k získání obsahu z webu přímé připojení.</p>
        </item>
        <item>
          <title><gui>Ručně</gui></title>
          <p>Pro každý protokol určete adresu proxy a port protokolu. Protokoly jsou <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> a <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title><gui>Automaticky</gui></title>
          <p>Adresa URL ukazující na zdroj, který obsahuje příslušná nastavení pro váš systém.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Aplikace, které používají síťové připojení, budou používat vámi určená nastavení proxy.</p>

</section>

</page>
