<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-send-file" xml:lang="hu">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Fájlok megosztása Bluetooth-eszközökre, például a telefonjára.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Fájlok küldése Bluetooth-eszközre</title>

  <p>Az összekapcsolt Bluetooth-eszközökre, például telefonokra és más számítógépekre fájlokat küldhet. Egyes eszköztípusok nem engedélyezik fájlok, vagy bizonyos típusú fájlok küldését. Fájlokat a Bluetooth beállításai ablakból küldhet.</p>

  <note style="important">
    <p>A <gui>Fájlok küldése</gui> nem működik a nem támogatott eszközökön, például az iPhone-okon.</p>
  </note>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Bluetooth</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Bluetooth</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Győződjön meg róla, hogy a Bluetooth be van kapcsolva: a címsorban lévő kapcsolónak bekapcsolva kell lennie.</p>
    </item>
    <item>
      <p>Az <gui>Eszközök</gui> listában válassza ki az eszközt, amelyre a fájlokat küldeni szeretné. Ha a kívánt eszköz nem jelenik meg <gui>Kapcsolódva</gui> állapotúként a listában, akkor <link xref="bluetooth-connect-device">csatlakoznia</link> kell ahhoz.</p>
      <p>Megjelenik egy, a külső eszközre jellemző panel.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Fájlok küldése…</gui> gombra, ekkor megjelenik a fájlválasztó.</p>
    </item>
    <item>
      <p>Válassza ki az elküldeni kívánt fájlt, és nyomja meg a <gui>Kiválasztás</gui> gombot.</p>
      <p>Több fájl küldéséhez tartsa lenyomva a <key>Ctrl</key> billentyűt az egyes fájlok kiválasztása közben.</p>
    </item>
    <item>
      <p>A fogadó eszköz tulajdonosának általában meg kell nyomnia egy gombot a fájl elfogadásához. A <gui>Bluetooth fájlátvitel</gui> ablak megjeleníti a folyamatjelzőt. Amikor az átvitel kész, nyomja meg a <gui>Bezárás</gui> gombot.</p>
    </item>
  </steps>

</page>
