<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="hu">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Törölje azokat a felhasználókat, akik már nem használják a számítógépét.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Felhasználói fiók törlése</title>

  <p><link xref="user-add">Több felhasználói fiókot is létrehozhat a számítógépén</link>. Ha valaki többé már nem használja a számítógépét, akkor törölheti a felhasználói fiókját.</p>

  <p>A felhasználói fiókok törléséhez <link xref="user-admin-explain">rendszergazdai jogosultság</link> szükséges.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Rendszer</gui> szót.</p>
    </item>
    <item>
      <p>Válassza a <guiseq><gui>Beállítások</gui><gui>Rendszer</gui></guiseq> elemet a találatokból. Ez megnyitja a <gui>Rendszer</gui> panelt.</p>
    </item>
    <item>
      <p>Válassza a <gui>Felhasználók</gui> elemet a panel megnyitásához.</p>
    </item>
    <item>
      <p>Nyomja meg a jobb felső sarokban lévő <gui style="button">Feloldás</gui> gombot, és adja meg jelszavát.</p>
    </item>
    <item>
      <p>Kattintson a törölni kívánt felhasználói fiókra az <gui>Egyéb felhasználók</gui> alatt.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui style="button">Felhasználó eltávolítása…</gui> gombot a felhasználói fiók törléséhez.</p>
    </item>
    <item>
      <p>Minden felhasználó saját könyvtárral és beállításokkal rendelkezik. Eldöntheti, hogy megtartja vagy törli a felhasználók saját könyvtárát. Nyomja meg a <gui>Fájlok törlése</gui> gombot, ha biztos benne, hogy nem fogja használni azokat senki, és szabad lemezterületre van szüksége. A fájlok véglegesen törlődnek, nem állíthatóak vissza. A fájlok törlése előtt érdemes lehet biztonsági mentést készíteni azokról egy külső tárolóeszközre.</p>
    </item>
  </steps>

</page>
