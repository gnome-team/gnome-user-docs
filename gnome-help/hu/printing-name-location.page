<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="hu">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A nyomtató nevének vagy helyének módosítása a nyomtatás beállításaiban.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>
  
  <title>Nyomtató nevének vagy helyének megváltoztatása</title>

  <p>A nyomtató nevét vagy helyét módosíthatja a nyomtatás beállításaiban.</p>

  <note>
    <p>A nyomtató nevének vagy helyének módosításához <link xref="user-admin-explain">rendszergazdai jogosultságokra</link> van szükség.</p>
  </note>

  <section id="printer-name-change">
    <title>Nyomtató nevének módosítása</title>

  <p>A nyomtató nevének megváltoztatásához tegye a következőket:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Nyomtatók</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Nyomtatók</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>A rendszerétől függően előfordulhat, hogy meg kell nyomnia a jobb felső sarokban lévő <gui style="button">Feloldás</gui> gombot, és meg kell adnia a jelszavát, ha kérik.</p>
    </item>
    <item>
      <p>Kattintson a nyomtató mellett lévő <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">beállítások</span></media> gombra.</p>
    </item>
    <item>
      <p>Válassza a <gui style="menuitem">Nyomtató részletei</gui> menüpontot.</p>
    </item>
    <item>
      <p>Adjon új nevet a nyomtatónak a <gui>Név</gui> mezőben.</p>
    </item>
    <item>
      <p>A párbeszédablak bezárása.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>A nyomtató helyének módosítása</title>

  <p>Nyomtató helyének megváltoztatása:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Nyomtatók</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Nyomtatók</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>A rendszerétől függően előfordulhat, hogy meg kell nyomnia a jobb felső sarokban lévő <gui style="button">Feloldás</gui> gombot, és meg kell adnia a jelszavát, ha kérik.</p>
    </item>
    <item>
      <p>Kattintson a nyomtató mellett lévő <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">beállítások</span></media> gombra.</p>
    </item>
    <item>
      <p>Válassza a <gui style="menuitem">Nyomtató részletei</gui> menüpontot.</p>
    </item>
    <item>
      <p>Adjon új helyet a nyomtatónak a <gui>Hely</gui> mezőben.</p>
    </item>
    <item>
      <p>A párbeszédablak bezárása.</p>
    </item>
  </steps>

  </section>

</page>
