<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Gyenge lehet a jel, vagy a hálózat nem engedi a megfelelő csatlakozást.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

<title>Miért bontja folyamatosan a kapcsolatot a vezeték nélküli hálózatom?</title>

<p>Előfordulhat, hogy a vezeték nélküli kapcsolata akkor is bontásra kerül, ha ezt nem akarta. A számítógép normális esetben megpróbál azonnal újracsatlakozni a hálózathoz (a felső sávon a hálózat ikon három pontot jelenít meg az újracsatlakozás közben), de ez zavaró lehet, különösen ha éppen használta az internetet.</p>

<section id="signal">
 <title>Gyenge vezeték nélküli jel</title>

 <p>A vezeték nélküli hálózati kapcsolat bontásának gyakori oka, hogy a jelerősség nem elegendő. A vezeték nélküli hálózatok hatóköre korlátozott, így ha túl messze van a vezeték nélküli bázisállomástól, akkor a jel gyenge lehet a kapcsolat fenntartásához. Az Ön és a bázisállomás közti falak és más objektumok is gyengíthetik a jelet.</p>

 <p>A hálózati ikon a felső sávon megjeleníti a vezeték nélküli jel erősségét. Ha a jel gyengének tűnik, próbáljon közelebb menni a vezeték nélküli bázisállomáshoz.</p>

</section>

<section id="network">
 <title>A hálózati kapcsolat nem jön létre megfelelően</title>

 <p>Néha a vezeték nélküli hálózathoz csatlakozáskor először úgy tűnhet, hogy sikeresen csatlakozott, de nem sokkal később a kapcsolat megszakad. Ez akkor történhet meg, ha a számítógép csak részlegesen kapcsolódik a hálózathoz. A kapcsolat ilyenkor létrejön, de valamiért nem sikerül befejezni a kapcsolódást, és az megszakad.</p>

 <p>Ennek oka lehet, hogy hibás vezeték nélküli jelszót adott meg, vagy a számítógépének nem engedélyezett a hálózatra csatlakozás (például mert a hálózat felhasználónevet kér a bejelentkezéshez).</p>

</section>

<section id="hardware">
 <title>Megbízhatatlan vezeték nélküli hardver/illesztőprogramok</title>

 <p>Egyes vezeték nélküli hálózati hardverek kissé megbízhatatlanok. A vezeték nélküli hálózatok bonyolultak, így a vezeték nélküli csatolók és bázisállomások néha kisebb problémákba ütköznek, és eldobják a kapcsolatot. Ez zavaró, de sok eszközzel rendszeresen megtörténik. Ha a vezeték nélküli kapcsolata rendszeresen megszakad, akkor ez lehet az egyetlen ok. Ha ez folyamatosan bekövetkezik, akkor fontolja meg más hardver beszerzését.</p>

</section>

<section id="busy">
 <title>Forgalmas vezeték nélküli hálózatok</title>

 <p>A forgalmas helyeken (például egyetemeken és kávézókban) lévő vezeték nélküli hálózatokra gyakran sok számítógép próbál egyszerre csatlakozni. Néha ezek a hálózatok túlterhelődnek, és képtelenek kezelni a csatlakozni próbáló összes számítógépet, így néhány kapcsolata megszakad.</p>

</section>

</page>
