<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="hu">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.38.1" date="2020-11-04" status="review"/>
    <revision version="gnome:42" status="final" date="2022-03-02"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ivan Stanton</name>
      <email>northivanastan@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Egy stílus kiválasztása és egy háttér beállítása.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Az asztala megjelenésének módosítása</title>
  
  <p>A képernyőn megjelenő dolgok kinézetét a stílusbeállítás világosra vagy sötétre állításával változtathatja meg. Kiválaszthat egy képet vagy háttérképet az asztal háttereként.</p>
  
<section id="style">
  <title>Világos vagy sötét stílus</title>
  <p>A világos és sötét asztalstílus közti váltáshoz:</p>
  
  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Megjelenés</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Megjelenés</gui> elemre a panel megnyitásához. A jelenleg kiválasztott stílus kék kerettel jelenik meg fent.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Világos</gui> vagy a <gui>Sötét</gui> kiválasztásához.</p>
    </item>
    <item>
      <p>A beállítások azonnal alkalmazásra kerülnek.</p>
    </item>
  </steps>
  
</section>

<section id="background">
  <title>Háttér</title>
  <p>A háttérként használt kép megváltoztatásához:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Megjelenés</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Megjelenés</gui> elemre a panel megnyitásához. A jelenleg kiválasztott háttérkép jelenik meg fent a jelenlegi stílus előnézetében.</p>
    </item>
    <item>
      <p>A háttérként használt kép megváltoztatásának két módja van:</p>
      <list>
        <item>
          <p>Kattintson a rendszerrel szállított háttérképek egyikére.</p>
        <note style="info">
          <p>Néhány háttérkép a nap folyamán változik. Ezek a háttérképek a jobb alsó sarokban egy kis óra ikonnal rendelkeznek.</p>
        </note>
        </item>
        <item>
          <p>Kattintson a <gui>Kép hozzáadása…</gui> gombra a saját fényképei egyikének használatához. Alapértelmezetten a <file>Képek</file> mappa lesz megnyitva, mivel a legtöbb fényképkezelő alkalmazás itt tárolja a fényképeket.</p>
        </item>
      </list>
    </item>
    <item>
      <p>A beállítások azonnal alkalmazásra kerülnek.</p>
        <note style="tip">
          <p>A saját fényképek háttérképként való beállításának másik módja, hogy kattintson a jobb egérgombbal egy képre a <app>Fájlok</app> alkalmazásban, és válassza a <gui>Beállítás háttérképként</gui> lehetőséget, vagy nyissa meg a képfájt a <app>Képmegjelenítő</app> alkalmazásban, kattintson a címsoron lévő menü gombra, és válassza a <gui>Beállítás háttérképként</gui> menüpontot.</p>
        </note>
    </item>
    <item>
      <p>A teljes asztal megjelenítéséhez <link xref="shell-workspaces-switch">váltson egy üres munkaterületre</link>.</p>
    </item>
  </steps>
  
  </section>

</page>
