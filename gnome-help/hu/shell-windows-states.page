<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="hu">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rendezze át a munkaterület ablakait, hogy hatékonyabban dolgozhasson.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Ablakok mozgatása és átméretezése</title>

  <p>Az ablakok mozgatásával és átméretezésével hatékonyabban dolgozhat. A húzás várható viselkedésén kívül a rendszer gyorsbillentyűket és módosítókat biztosít az ablakok gyors elrendezéséhez.</p>

  <list>
    <item>
      <p>Az ablak mozgatásához húzza a címsorát, vagy tartsa lenyomva a <key xref="keyboard-key-super">Super</key> billentyűt, és rákattintás után húzza az ablakot. A mozgatás során tartsa lenyomva a <key>Shift</key> billentyűt az ablaknak a képernyő széleihez és más ablakokhoz igazításához.</p>
    </item>
    <item>
      <p>Az ablak átméretezéséhez húzza az ablak széleit vagy sarkait. Az átméretezés során tartsa lenyomva a <key>Shift</key> billentyűt az ablaknak a képernyő széleihez és más ablakokhoz igazításához.</p>
      <p if:test="platform:gnome-classic">Egy maximalizált ablakot a címsorában lévő maximalizálás gombra kattintva is átméretezhet.</p>
    </item>
    <item>
      <p>Az ablakokat kizárólag a billentyűzet használatával is mozgathatja vagy átméretezheti. Az <keyseq><key>Alt</key><key>F8</key></keyseq> lenyomásával mozgathatja az ablakot, vagy az <keyseq><key>Alt</key><key>F8</key></keyseq> lenyomása után átméretezheti azt. Használja a nyílbillentyűket az ablak mozgatására vagy átméretezésére, majd nyomja le az <key>Enter</key> billentyűt a befejezéshez, vagy az <key>Esc</key> megnyomásával visszatérhet az eredeti pozícióra és méretre.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Ablak maximalizálásához</link> húzza azt a képernyő tetejére. Húzza az ablakot a képernyő egyik oldalára az adott oldal menti maximalizálásához, ami lehetővé teszi az <link xref="shell-windows-tiled">ablakok egymás mellé helyezését</link>.</p>
    </item>
  </list>

</page>
