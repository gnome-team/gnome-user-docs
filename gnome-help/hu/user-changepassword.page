<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="hu">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tartsa biztonságban fiókját a jelszava gyakori megváltoztatásával a fiókbeállításokban.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Módosítsa jelszavát</title>

  <p>Módosítsa jelszavát rendszeresen, különösen ha úgy véli, mások is tudhatják azt.</p>

  <p>A saját fiókjától eltérő fiókok szerkesztéséhez <link xref="user-admin-explain">rendszergazdai jogosultság</link> szükséges.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Rendszer</gui> szót.</p>
    </item>
    <item>
      <p>Válassza a <guiseq><gui>Beállítások</gui><gui>Rendszer</gui></guiseq> elemet a találatokból. Ez megnyitja a <gui>Rendszer</gui> panelt.</p>
    </item>
    <item>
      <p>Válassza a <gui>Felhasználók</gui> elemet a panel megnyitásához.</p>
    </item>
    <item>
      <p>Kattintson a <gui>·····</gui> feliratra a <gui>Jelszó</gui> mellett. Ha egy másik felhasználó jelszavát módosítja, akkor először a <gui>Feloldás</gui> gombra kell kattintania a panel feloldásához, majd válassza ki a fiókot az <gui>Egyéb felhasználók</gui> alatt.</p>
    </item>
    <item>
      <p>Gépelje be a jelenlegi, majd az új jelszavát. Az <gui>Új jelszó ellenőrzése</gui> mezőbe gépelje be újra az új jelszavát.</p>
      <p>A <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">jelszó előállítása</span></media></gui> ikonra kattintva automatikusan előállíthat egy véletlen jelszót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Módosítás</gui> gombra.</p>
    </item>
  </steps>

  <p>Győződjön meg róla, hogy <link xref="user-goodpassword">jó jelszót választott-e</link>. Ez segít biztonságban tartani a fiókját.</p>

  <note>
    <p>Amikor megváltoztatja a bejelentkezési jelszavát, a bejelentkezési kulcstartó jelszava automatikusan frissítésre kerül, hogy megegyezzen az új bejelentkezési jelszóval.</p>
  </note>

  <p>Ha elfelejti jelszavát, akkor bármely rendszergazdai jogosultsággal rendelkező felhasználó megváltoztathatja.</p>

</page>
