<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="hu">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mentsen mindent, aminek az elvesztését nem tudná elviselni, ha valami elromlik.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Mit mentsek?</title>

  <p>A prioritást a <link xref="backup-thinkabout">legfontosabb fájlok</link> mentésének kell kapnia, valamint a nehezen előállíthatóknak. A legfontosabbaktól a legkevésbé fontos fájlok listája például:</p>

<terms>
 <item>
  <title>Személyes fájljai</title>
   <p>Ide tartozhatnak a dokumentumok, táblázatok, e-mail, naptárbejegyzések, pénzügyi adatok, családi fényképek vagy bármely saját készítésű és emiatt fontos fájl, amit pótolhatatlannak tekint.</p>
 </item>

 <item>
  <title>Személyes beállításai</title>
   <p>Ide a színek, hátterek, képernyő-felbontások és egérbeállítások változtatásai tartoznak. Ebbe a kategóriába esnek az alkalmazások beállításai, például a <app>LibreOffice</app>, a zenelejátszó és az e-mail kliens beállításai. Ezek pótolhatók, de ez eltarthat egy ideig.</p>
 </item>

 <item>
  <title>Rendszerbeállítások</title>
   <p>A legtöbben nem módosítják a telepítéskor létrehozott rendszerbeállításokat. Ha rendszerét személyre szabja, vagy számítógépét kiszolgálóként használja, akkor ezeket a beállításokat is mentse.</p>
 </item>

 <item>
  <title>Telepített szoftverek</title>
   <p>Az általában használt szoftverek általában újratelepítéssel gyorsan helyreállíthatók.</p>
 </item>
</terms>

  <p>Általában a pótolhatatlan fájlokról és a mentés nélkül csak nagy időbefektetés árán helyreállítható fájlokról kell mentést készíteni. Ha egyes fájlok egyszerűen helyreállíthatók, akkor azokra nem muszáj lemezhelyet pazarolni.</p>

</page>
