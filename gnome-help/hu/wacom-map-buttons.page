<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-map-buttons" xml:lang="hu">
      
  <info>
    <revision version="gnome:46" date="2024-03-10" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rendeljen funkciókat a rajztábla hardveres gombjaihoz.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>A rajztábla gombjainak hozzárendelése</title>

  <p>A rajztábla hardveres gombjai számos funkcióra beállíthatók.</p>

<steps>
  <item>
    <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Wacom rajztábla</gui> szavakat.</p>
  </item>
  <item>
    <p>Kattintson a <gui>Wacom rajztábla</gui> elemre a panel megnyitásához.</p>
    <note style="tip"><p>Ha a rajztábla nem került felismerésre, akkor a program megkéri, hogy <gui>Csatlakoztassa vagy kapcsolja be Wacom rajztábláját</gui>. Kattintson az oldalsávon a <gui>Bluetooth</gui> hivatkozásra a vezeték nélküli rajztábla csatlakoztatásához.</p></note>
  </item>
  <item>
    <p>Kattintson a <gui>Gombok leképezése</gui> gombra.</p>
  </item>
  <item>
    <p>A képernyőn megjelenő kijelző megjeleníti a rajztábla gombjainak elrendezését. Nyomja meg a rajztábla minden egyes gombját, és válassza ki a következő funkciók valamelyikét:</p>
    <list>
      <item><p><gui>Alkalmazás által meghatározott</gui></p></item>
      <item><p><gui>Képernyőn lévő súgó megjelenítése</gui></p></item>
      <item><p><gui>Monitor váltása</gui></p></item>
      <item><p><gui>Billentyűleütés küldése</gui></p></item>
   </list>
  </item>
  <item>
    <p>Kattintson a <gui>Kész</gui> gombra, ha minden gomb beállításra került, és nyomja meg az <key>Esc</key> billentyűt a kilépéshez.</p>
  </item>
</steps>


</page>
