<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="hu">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Az adapter ki van kapcsolva, esetleg nincs illesztőprogramja, vagy a Bluetooth ki van kapcsolva/blokkolva van.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Nem tudom csatlakoztatni a Bluetooth-eszközöm</title>

  <p>Számos oka lehet annak, hogy nem tud Bluetooth-eszközhöz, például telefonhoz vagy fejhallgatóhoz kapcsolódni:</p>

  <terms>
    <item>
      <title>A kapcsolat blokkolva van, vagy nem megbízható</title>
      <p>Egyes Bluetooth-eszközök alapesetben blokkolják a kapcsolatokat, vagy megkövetelik egy beállítás megváltoztatását a kapcsolatok létrehozásához. Győződjön meg róla, hogy az eszközön engedélyezett a kapcsolatok létrehozása.</p>
    </item>
    <item>
      <title>A Bluetooth hardver ismeretlen</title>
      <p>Lehet, hogy a számítógép nem ismerte fel a Bluetooth-adaptert vagy dongle-t. Ennek az lehet az oka, hogy az adapter <link xref="hardware-driver">illesztőprogramja</link> nincs telepítve. Néhány Bluetooth-adapter nem támogatott Linux alatt, így lehet, hogy nem tud illesztőprogramot beszerezni hozzájuk. Ebben az esetben próbáljon meg másik Bluetooth-adaptert beszerezni.</p>
    </item>
    <item>
      <title>Az adapter nincs bekapcsolva</title>
        <p>Győződjön meg róla, hogy a Bluetooth-adapter be van kapcsolva. Nyissa meg a Bluetooth panelt, és ellenőrizze, hogy nincs-e <link xref="bluetooth-turn-on-off">kikapcsolva</link>.</p>
    </item>
    <item>
      <title>Az eszköz Bluetooth-kapcsolata ki van kapcsolva</title>
      <p>Ellenőrizze, hogy a Bluetooth be van kapcsolva az eszközön, amelyhez csatlakozni kíván, és <link xref="bluetooth-visibility">felfedezhető vagy látható</link>. Ha például egy telefonhoz próbál csatlakozni, ellenőrizze, hogy az nincs-e repülési üzemmódban.</p>
    </item>
    <item>
      <title>Nincs Bluetooth csatoló a számítógépben</title>
      <p>Sok számítógépben nincs Bluetooth csatoló. Bluetooth-eszközök használatához külön csatolót kell vásárolnia.</p>
    </item>
  </terms>

</page>
