<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="hu">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Névjegy felvétele a helyi címjegyzékbe vagy eltávolítása abból.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

<title>Névjegy felvétele vagy eltávolítása</title>

  <p>Névjegy felvétele:</p>

  <steps>
    <item>
      <p>Kattintson a <gui style="button">+</gui> gombra.</p>
    </item>
    <item>
      <p>Az <gui>Új névjegy</gui> párbeszédablakban adja meg a névjegyhez tartozó nevet és a kívánt információkat. Kattintson az egyes mezők melletti legördülő menükre a részletek típusának kiválasztásához.</p>
    </item>
    <item>
      <p>További részletek hozzáadásához nyomja meg a <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Továbbiak megtekintése</span></media> lehetőséget.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui style="button">Hozzáadás</gui> gombot a névjegy mentéséhez.</p>
    </item>
  </steps>

  <p>Névjegy eltávolítása:</p>

  <steps>
    <item>
      <p>Válassza ki a névjegyet a névjegylistából.</p>
    </item>
    <item>
      <p>Nyomja meg a fejlécsávon lévő <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Továbbiak megtekintése</span></media> gombot a jobb felső sarokban.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui style="menu item">Törlés</gui> menüpontot a névjegy eltávolításához.</p>
    </item>
   </steps>
    <p>Egy vagy több névjegy eltávolításához jelölje be a törölni kívánt névjegyek melletti jelölőmezőket, és nyomja meg az <gui style="button">Eltávolítás</gui> gombot.</p>
</page>
