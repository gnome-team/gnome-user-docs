<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="hu">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>A számítógépe működni fog, de másik tápkábelre vagy utazóadapterre lehet szüksége.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

<title>Működni fog a számítógépem egy másik ország elektromos hálózatában?</title>

<p>A különböző országok különböző feszültségű (általában 110V vagy 220-240V) elektromos hálózatot és váltóáramú frekvenciákat (általában 50 Hz vagy 60 Hz) használnak. A számítógépe működni fog más országok elektromos hálózatán, ha megfelelő adaptert használ. Szükség lehet egy kapcsoló átbillentésére is.</p>

<p>Ha laptopot használ, akkor csak a megfelelő villásdugóra van szüksége a laptop csatlakozójához. Néhány laptopot több villásdugóval szállítanak, így akár már rendelkezhet is a megfelelővel. Ellenkező esetben egy szabványos utazóadapterhez való csatlakoztatás elegendő.</p>

<p>Ha asztali számítógépet használ, akkor beszerezhet egy más típusú villásdugóval rendelkező kábelt, vagy használhat utazóadaptert. Ebben az esetben azonban szükséges lehet a számítógép tápján átállítani a feszültségkapcsolót, ha van. Sok számítógépen nincs ilyen kapcsoló, és bármelyik feszültségen remekül működnek. Keresse meg a számítógép hátulján azt az aljzatot, amelybe a tápkábel megy. Ennek közelében kell lennie egy „110V” és „230V” feliratú kapcsolónak. Ha szükséges, kapcsolja át.</p>

<note style="warning">
  <p>Legyen óvatos a tápkábelek cseréjekor vagy utazóadapterek használatakor. Először mindig kapcsolja ki készülékeit.</p>
</note>

</page>
