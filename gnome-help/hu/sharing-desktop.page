<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 ui/1.0" id="sharing-desktop" xml:lang="hu">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>
    <revision version="gnome:46" status="final" date="2024-03-02"/>
    <revision version="gnome:46" status="draft" date="2024-04-21"/>
    <credit type="author">
      <name>Marie Stará</name>
      <email>413827@mail.muni.cz</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Engedélyezze másoknak, hogy RDP használatával megnézzék és használják az asztalát.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023, 2024</mal:years>
    </mal:credit>
  </info>

  <title>Az asztal megosztása</title>

  <p>Lehetővé teheti másoknak, hogy egy asztalmegjelenítő alkalmazás segítségével megjelenítsék és irányítsák asztalát egy másik eszközről. Az <gui>Asztal megosztása</gui> beállítóablakában lehetővé teheti másoknak az asztala elérését, és megadhatja a biztonsági beállításokat is.</p>

  <p>Ha távolról szeretne bejelentkezni a felhasználói fiókjába, akkor nézze meg a <link xref="remote-login">Távoli bejelentkezés</link> fejezetet.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Rendszer</gui> szót.</p>
    </item>
    <item>
      <p>Válassza a <guiseq><gui>Beállítások</gui><gui>Rendszer</gui></guiseq> elemet a találatokból. Ez megnyitja a <gui>Rendszer</gui> panelt.</p>
    </item>
    <item>
      <p>Válassza a <gui>Távoli asztal</gui> elemet az <gui>Asztal megosztása</gui> panel megnyitásához.</p>
    </item>
    <item>
      <p>Ahhoz, hogy mások is láthassák az asztalát, kapcsolja be az <gui>Asztal megosztása</gui> kapcsolót. Ez azt jelenti, hogy mások megpróbálhatnak kapcsolódni az eszközéhez, és láthatják képernyőjét.</p>
    </item>
    <item>
      <p>Ahhoz, hogy mások is interakcióba léphessenek az asztalával, kapcsolja be a <gui>Távoli vezérlés</gui> kapcsolót. Ez lehetővé teheti a másik személy számára a kurzor mozgatását, az alkalmazások futtatását és a fájlok böngészését a jelenleg használt biztonsági beállításoktól függően.</p>
    </item>
  </steps>

  <section id="connecting">
  <title>Kapcsolódás</title>

  <p>A <gui>Hogyan kapcsolódjon</gui> szakasz jeleníti meg a <gui>Gépnév</gui> és a <gui>Port</gui> értékét, amely a kapcsolódó számítógépen használható. Kattintson az egyes bejegyzések melletti gombra, ha el szeretné helyezni a vágólapon. Kapcsolat az <link xref="net-findip">IP-címe</link> használatával is létrehozható.</p>

  <note style="important">
    <p>Ha a <link xref="remote-login">Távoli bejelentkezést</link> is engedélyezi, akkor a port számát 3389-re állítja be. Ez azt jelenti, hogy az <gui>Asztal megosztása</gui> másik portot fog használni, például a 3390-et.</p>
    <p>Ha a port száma nem 3389, akkor a kapcsolódáskor adja meg.</p>
  </note>


  <p>Ha a másik eszköz sikeresen kapcsolódott az asztalához, akkor az <gui>A képernyő meg van osztva</gui> ikont, <media its:translate="no" type="image" src="figures/topbar-screen-shared.svg" style="floatend"/> fog látni a rendszer állapotterületén.</p>

  <note style="info"><p>Ha az <gui>Eszköz neve</gui> elemben beállított szöveg nem szerkeszthető, akkor <link xref="about-hostname">megváltoztathatja</link> az eszköz hálózaton megjelenített nevét.</p></note>

  </section>

  <section id="authentication">
  <title>Hitelesítés</title>

  <p>A <gui>Bejelentkezés részletei</gui> szakasz jeleníti meg azt a felhasználónevet és jelszót, amelyet az asztalához való kapcsolódáshoz használt ügyfélszoftverben meg kell használnia.</p>

  <note style="tip">
    <p>Kattintson a <gui>Titkosítás ellenőrzése</gui> gombra a titkosítás ujjlenyomatának megjelenítéséhez. Hasonlítsa össze az ügyfélprogram által a kapcsolódáskor megjelenített értékkel: a kettőnek azonosnak kell lennie.</p>
  </note>
  </section>

  <section id="clients">
  <title>Ügyfélprogramok</title>

  <p>Egy másik eszközről az asztalhoz való kapcsolódáshoz a következő ügyfélprogramok működnek biztosan.</p>
  <terms>
    <item>
      <title>Linux alól:</title>
      <list>
        <item><p><app>Remmina</app>, egy GTK ügyfélprogram, amely a legtöbb disztribúcióban elérhető csomagként, valamint <link href="https://flathub.org/apps/details/org.remmina.Remmina">flatpak</link> telepítéssel is. Használja az alapértelmezett beállításokat, különösen a kapcsolati profil beállításaiban lévő <gui>Színmélység</gui> legyen „Automatikus”.</p>
        </item>
        <item><p><app>Kapcsolatok</app>, egy GTK ügyfélprogram, amely a legtöbb disztribúcióban elérhető csomagként, valamint <link href="https://flathub.org/apps/org.gnome.Connections">flatpak</link> telepítéssel is.</p>
        </item>
        <item><p><app>xfreerdp</app>, amely a legtöbb disztribúcióban csomagként elérhető parancssori ügyfélprogram. A <cmd>/network:auto</cmd> beállítást kell átadni az ügyfélprogramnak a parancssorban.</p>
        </item>
      </list>
    </item>
    <item>
      <title>Microsoft Windows alól:</title>
      <list>
        <item><p><app>mstsc</app>, amely a beépített Windows ügyfélprogram. Az alapértelmezett beállítások javasoltak.</p>
        </item>
      </list>
    </item>
    <item>
      <title>Linux, Windows vagy macOS alól:</title>
      <list>
        <item><p><app>Thincast</app>, amely egy tulajdonosi ügyfélprogram. A linuxos verzió elérhető <link href="https://flathub.org/apps/details/com.thincast.client">flatpak</link> csomagként. Az alapértelmezett beállítások javasoltak.</p>
        </item>
      </list>
    </item>
  </terms>
  </section>

  <section id="checking-connection">
  <title>Kapcsolat ellenőrzése</title>
  <steps>
    <item>
      <p>A választott ügyfélprogramjában adja meg a <gui>Gépnevet</gui> vagy az IP-címet.</p>
     <note style="tip">
      <p>Ha a port száma nem 3389, akkor adja meg (cím:port).</p>
      <p>Sok hálózaton a számítógép nevéhez hozzá kell adni egy „.local” utótagot, hogy a kapcsolat működjön.</p>
    </note>
    </item>
    <item>
      <p>Adja meg a felhasználónevet és a jelszót az asztal megosztásához.</p>
    </item>
  </steps>
  </section>

  <section id="disconnect">
  <title>Az asztal megosztásának befejezése</title>

  <p>Az asztalát néző személy kapcsolatának bontásához kövesse az alábbi lépéseket.</p>

  <steps>
    <item>
      <p>Kattintson a felső sáv jobb oldalán lévő rendszer menüre.</p>
    </item>
    <item>
      <p>Kattintson az <gui>A képernyő meg van osztva</gui> elemre.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Kikapcsolás</gui> gombra.</p>
    </item>
  </steps>

  </section>

  <section id="advanced" ui:expanded="false">
  <title>Speciális témák</title>

  <terms>
    <item>
      <title>Parancssori beállítás</title>
      <p>A <cmd>grdctl</cmd> segédprogram lehetővé teszi a gép beállításainak konfigurálását egy konzolablakban. A használat részleteiért írja be a <cmd>grdctl --help</cmd> parancsot.</p>
    </item>
    <item>
      <title>H.264</title>
      <p>A H.264 videokódolás nagymértékben csökkenti a sávszélességet. A <app>GNOME távoli asztal</app> H.264 kódolást fog használni, ha a grafikus adatcsatorna használatban van (a protokoll követelménye), az ügyfélprogram támogatja és az NVENC (az NVIDIA kódolója) elérhető.</p>
    </item>
  </terms>
  </section>

</page>
