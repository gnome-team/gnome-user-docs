<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="lt">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Patikrinkite ar padarytos atsarginės kopijos yra tinkamos.</desc>
  </info>

  <title>Tikrinkite savo atsargines kopijas</title>

  <p>After you have backed up your files, you should make sure that the
 backup was successful. If it didn’t work properly, you could lose important
 data since some files could be missing from the backup.</p>

   <p>When you use <app>Files</app> to copy or move files, the computer checks
   to make sure that all of the data transferred correctly. However, if you are
   transferring data that is very important to you, you may want to perform
   additional checks to confirm that your data has been transferred
   properly.</p>

  <p>You can do an extra check by looking through the copied files
 and folders on the destination media. By checking to make sure that the files
 and folders you transferred are actually there in the backup, you can have
 extra confidence that the process was successful.</p>

  <note style="tip"><p>If you find that you do regular backups of large amounts
 of data, you may find it easier to use a dedicated backup program, such as
 <app>Déjà Dup</app>. Such a program is more powerful and more reliable than
 just copying and pasting files.</p></note>

</page>
