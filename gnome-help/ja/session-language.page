<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="ja">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>
    <revision version="gnome:46" status="candidate" date="2024-03-02"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ユーザーインターフェースやヘルプの言語を切り替えます。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>使用する言語を変更する</title>

  <p>デスクトップやアプリケーションを多くの言語で使用できます。ただし、お使いのコンピューターに適切な言語パックがインストールされている必要があります。</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Your Account</gui> section, click <gui>Language</gui>.</p>
    </item>
    <item>
      <p>Select your desired region and language. If your region and language
      are not listed, click
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui>
      at the bottom of the list to select from all available regions and
      languages.</p>
    </item>
    <item>
      <p>Click <gui style="button">Select</gui> to save.</p>
    </item>
    <item>
      <p>Your session needs to be restarted for changes to take effect. Either click
      <gui style="button">Restart…</gui>, or manually log back in later.</p>
    </item>
  </steps>

  <p>Some translations may be incomplete, and certain applications may not
  support your language at all. Any untranslated text will appear in the
  language in which the software was originally developed, usually American
  English.</p>

  <p>ホームフォルダーには、アプリケーションが使用する、音楽や画像、ドキュメントなどの特別なフォルダーがあります。これらのフォルダーは、お使いの言語に合わせて標準的な名称を使用しています。ログインし直したときに、これらのフォルダーの名前を、選択した言語の標準的な名称に変更するかどうか尋ねられます。新しい言語を以後継続して使用する場合は、フォルダー名を更新すると良いでしょう。</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    section for the <gui>Login Screen</gui> in the
    <gui>Region &amp; Language</gui> panel.</p>
  </note>

</page>
