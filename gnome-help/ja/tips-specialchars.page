<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="ja">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Type characters not found on your keyboard, including foreign alphabets, mathematical symbols, emoji, and dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>特殊な文字を入力する</title>

  <p>You can enter and view thousands of characters from most of the world’s
  writing systems, even those not found on your keyboard. This page lists
  some different ways you can enter special characters.</p>

  <links type="section">
    <title>文字入力の方法</title>
  </links>

  <section id="characters">
    <title>Characters</title>
    <p>The character map application allows you to find and insert unusual
    characters, including emoji, by browsing character categories or searching
    for keywords.</p>

    <p>You can launch <app>Characters</app> from the Activities overview.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Insert emoji</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Browse the categories at the bottom of the dialog or start typing a
    description in the search field.</p>
      </item>
      <item>
        <p>Select an emoji to insert.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Compose キー</title>
    <p>Compose キーは特殊なキーで、これを使って複数キーを連続して押すことで特殊な文字を入力できます。たとえば、アクセント付きの <em>é</em> は、<key>Compose</key> キーを押し、次に <key>'</key> キー、その次に <key>e</key> キーを押して入力できます。</p>
    <p>Keyboards don’t have specific compose keys. Instead, you can define
    one of the existing keys on your keyboard as a compose key.</p>

    <steps>
      <title>Compose キーの割り当て</title>
      <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click on <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
      </item>
      <item>
        <p>In the <gui>Type Special Characters</gui> section, click <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Turn the switch on for the <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Close the dialog.</p>
      </item>
    </steps>

    <p>Compose キーを使って、よく使用される文字を入力できます。たとえば次のとおりです。</p>

    <list>
      <item><p><key>Compose</key> キーを押し、次に <key>'</key> キー、そしてアキュートアクセントを付けたい文字を入力すると、<em>é</em> のようになります。</p></item>
      <item><p><key>Compose</key> キーを押し、次に <key>`</key> キー、そしてグレーブアクセントを付けたい文字を入力すると、<em>è</em> のようになります。</p></item>
      <item><p><key>Compose</key> キーを押し、次に <key>"</key> キー、そしてウムラウトを付けたい文字を入力すると、<em>ë</em> のようになります。</p></item>
      <item><p><key>Compose</key> キーを押し、次に <key>-</key> キー、そしてマクロンを付けたい文字を入力すると、<em>ē</em> のようになります。</p></item>
    </list>
    <p>For more compose key sequences, see <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">the
    compose key page on Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>コードポイント</title>

  <p>You can enter any Unicode character using only your keyboard with the
  numeric code point of the character. Every character is identified by a
  four-character code point. To find the code point for a character, look it up
  in the <app>Characters</app> application. The code point is the four characters
  after <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>キーボードレイアウト</title>
    <p>お使いのキーボードを、キーの印字に関係なく、異なる言語用のキーボードとして使用できます。キーボードレイアウトは、トップバーのアイコンから簡単に切り替えることができます。方法については、<link xref="keyboard-layouts"/> を参照してください。</p>
  </section>

<section id="im">
  <title>インプットメソッド</title>

  <p>インプットメソッドは、前記の方法を拡張し、キーボードだけでなく、他の入力デバイスでも文字入力できるようになります。たとえば、マウスのジェスチャー操作で文字入力が可能だったり、あるいはラテン文字用キーボードで日本語の文字の入力が可能だったりします。</p>

  <p>インプットメソッドを選択するには、テキストウィジェットの上で右クリックし、<gui>入力メソッド</gui> メニューから使用するインプットメソッドを選択します。既定のインプットメソッドは決められていません、インプットメソッドのドキュメントを参照し、使用方法を確認してください。</p>

</section>

</page>
