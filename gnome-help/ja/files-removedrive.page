<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="ja">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>USB フラッシュドライブ、CD、DVD、あるいはその他のデバイスの取り出し、アンマウント。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>外付けドライブを安全に取り出す</title>

  <p>USB フラッシュドライブなどの外付けストレージデバイスを使用するときは、デバイスをコンピューターから抜く前に、安全な取り出し処理を行ってください。デバイスをそのまま抜いてしまうと、アプリケーションがまだデバイスを使用中の状態で抜いてしまうリスクがあります。これは、ファイルの消失や破損につながる恐れがあります。CD や DVD などの光学ディスクを使用する場合も、コンピューターからディスクを取り出すまえに同様の手順を踏んでください。</p>

  <steps>
    <title>リムーバブルデバイスを取り出す方法は次のとおりです。</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>サイドバーから該当のデバイスを探します。取り出し用の小さなアイコンが名前の隣にあります。デバイスの安全な取り出しを行うには、取り出し用のアイコンをクリックします。</p>
      <p>別の方法としては、サイドバーのデバイス名を右クリックし、<gui>取り出し</gui>を選択することもできます。</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>使用中のデバイスを安全に取り出す</title>

  <p>If any of the files on the device are open and in use by an application,
  you will not be able to safely remove the device. You will be prompted with a
  window telling you <gui>Volume is busy</gui>. To safely remove the device:</p>

  <steps>
    <item><p>Click <gui>Cancel</gui>.</p></item>
    <item><p>Close all the files on the device.</p></item>
    <item><p>Click the eject icon to safely remove or eject the
    device.</p></item>
    <item><p>別の方法としては、サイドバーのデバイス名を右クリックし、<gui>取り出し</gui>を選択することもできます。</p></item>
  </steps>

  <note style="warning"><p>You can also choose <gui>Eject Anyway</gui> to
  remove the device without closing the files. This may cause errors in
  applications that have those files open.</p></note>

  </section>

</page>
