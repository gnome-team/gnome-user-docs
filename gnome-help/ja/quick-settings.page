<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.1" id="quick-settings" xml:lang="ja">
  <info>
    <!--
        Recommended statuses: stub incomplete draft outdated review candidate final
    -->
    <revision version="gnome:44" date="2023-12-29" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2023</years>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <!--
        This puts a link to this topic on the index page.
        Change the xref to link it from another guide.
    -->
    <link type="guide" xref="prefs"/>

    <!--
        Think about whether other pages should be in the seealso list.
        The target page will automatically get a seealso link back.
    <link type="seealso" xref="someotherid"/>
    -->

    <!--
        Think about whether external resources should be in the seealso
        list. These require a title.
    <link type="seealso" href="http://someurl">
      <title>Link title</title>
    </link>
    -->

    <desc>Quickly toggle different settings and choose devices.</desc>

    <keywords>wi-fi, bluetooth, background</keywords>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Quick Settings</title>

  <p>The Quick Settings buttons in the
  <link xref="shell-introduction#systemmenu">system menu</link> let you quickly
  switch available services on or off, and choose Bluetooth devices or Wi-Fi
  networks.</p>

  <section id="wifi">
    <title>Wi-Fi</title>
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright"/>
    <p>Press the <gui>Wi-Fi</gui> button to switch Wi-Fi off or back on.</p>
    <p>The button shows the Wi-Fi network that's currently connected.</p>
    <p>Press
    <media type="image" its:translate="no" src="figures/go-next-symbolic.svg">
    right</media> to show available networks.</p>
    <p>Select a network to initiate a connection, or select
    <gui>All Networks</gui> to open the
    <link xref="net-wireless">Wi-Fi</link> settings panel.</p>    
  </section>
  
  <section id="wired">
    <title>Wired</title>
    <p>Press the <gui>Wired</gui> button to switch wired networking off or back
    on. The button shows information about the current wired network connection.
    Press
    <media type="image" its:translate="no" src="figures/go-next-symbolic.svg">
    right</media> to show more settings. Select <gui>Wired Settings</gui>
    to open the <link xref="net-wired">Network</link> settings panel.</p>    
  </section>

  <section id="bluetooth">
    <title>Bluetooth</title>
    <p>Press the <gui>Bluetooth</gui> button to switch bluetooth off or back on.
    The button shows the name of the first device, or the number of devices
    connected. Press
    <media type="image" its:translate="no" src="figures/go-next-symbolic.svg">
    right</media> to show paired and connected Bluetooth devices. Select one to
    connect or disconnect. Select <gui>Bluetooth Settings</gui> to open the
    <link xref="bluetooth">Bluetooth</link> settings panel.</p>    
  </section>
  
  <section id="power">
    <title>Power Mode</title>
    <p>The <gui>Power Mode</gui> button shows the current
    <link xref="power-profile">power mode</link> setting. Press the button to
    switch to <gui>Power Save</gui> or back to the current setting. Press
    <media type="image" its:translate="no" src="figures/go-next-symbolic.svg">
    right</media> to select from all modes. Select <gui>Power Settings</gui> to
    open the <link xref="power">Power</link> settings panel.</p>    
  </section>

  <section id="toggles">
    <title>Toggles</title>
    <p>The other buttons show the current status of
    <link xref="display-night-light">Night Light</link>, <gui>Dark Style</gui>,
    <link xref="net-wireless-airplane">Airplane Mode</link>, or
    <gui>Keyboard</gui> backlight, where available. Press the button to switch
    on or off.</p>  
  </section>

  <section id="background">
    <title>Background Apps</title>
    <p>The number of background applications running on the system is shown at
    the bottom of the system menu. Click to show a list of these apps. Selecting
    an application from the list opens a window for that app and removes it from
    <gui>Background Apps</gui>. Select <gui>App Settings</gui> to open the
    <gui>Apps</gui> settings panel.</p>
  </section>

</page>
