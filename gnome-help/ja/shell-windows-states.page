<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="ja">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>効率的に作業できるようワークスペースにウィンドウを配置します。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>ウィンドウを移動する、サイズを変更する</title>

  <p>You can move and resize windows to help you work more efficiently. In
  addition to the dragging behavior you might expect, the system features
  shortcuts and modifiers to help you arrange windows quickly.</p>

  <list>
    <item>
      <p>Move a window by dragging the titlebar, or hold down
      <key xref="keyboard-key-super">Super</key> and drag anywhere in the
      window. Hold down <key>Shift</key> while moving to snap the window to the
      edges of the screen and other windows.</p>
    </item>
    <item>
      <p>ウィンドウのサイズを変更するには、ウィンドウの端か角をドラッグします。<key>Shift</key> を押しながらサイズ変更すると、画面や他のウィンドウの端まで瞬時にサイズ変更します。</p>
      <p if:test="platform:gnome-classic">You can also resize a maximized
      window by clicking the maximize button in the titlebar.</p>
    </item>
    <item>
      <p>キーボードだけを使ってウィンドウの移動やサイズ変更を行う方法もあります。ウィンドウを移動するには、<keyseq><key>Alt</key><key>F7</key></keyseq> を押し、サイズを変更するには、<keyseq><key>Alt</key><key>F8</key></keyseq> を押します。矢印キーで移動やサイズ変更を行い、次に <key>Enter</key> キーで確定するか、あるいは <key>Esc</key> キーで元の位置やサイズに戻します。</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">ウィンドウを最大化する</link>には、ウィンドウを画面トップまでドラッグします。ウィンドウを画面の左右どちらかの側へドラッグすると、片側半分に最大化し、<link xref="shell-windows-tiled">ウィンドウを両側に並べる</link>ことができます。</p>
    </item>
  </list>

</page>
