<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="sl">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>IP naslov je kot telefonska številka vašega računalnika.</desc>
  </info>

  <title>Kaj je naslov IP?</title>

  <p>“IP address” stands for <em>Internet Protocol address</em>, and each
  device that is connected to a network (like the internet) has one.</p>

  <p>IP naslov je podoben vaši telefonski številki. Vaša telefonska številka je edinstveno zaporedje števil, ki vaš telefon določi tako, da vas drugi ljudje lahko pokličejo. Podobno je IP naslov edinstveno zaporedje števil, ki določi vaš računalnik tako, da lahko pošlje in prejme podatke z drugih računalnikov.</p>

  <p>Trenutno je večina naslovov IP sestavljena iz štirih zaporedij števil, ki so ločena s piko. Primer naslova IP je <code>192.168.1.42</code>.</p>

  <note style="tip">
    <p>An IP address can either be <em>dynamic</em> or <em>static</em>. Dynamic
    IP addresses are temporarily assigned each time your computer connects to a
    network. Static IP addresses are fixed, and do not change. Dynamic IP
    addresses are more common that static addresses — static addresses are
    typically only used when there is a special need for them, such as in the
    administration of a server.</p>
  </note>

</page>
