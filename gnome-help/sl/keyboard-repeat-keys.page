<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="sl">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nastavite tipkovnico tako, da ne ponavlja črk medtem ko držite tipko ali spremenite zakasnitev in hitrost ponavljanja tipk.</desc>
  </info>

  <title>Manage repeated key presses</title>

  <p>By default, when you hold down a key on your keyboard, the letter
  or symbol will be repeated until you release the key. If you have
  difficulty picking your finger back up quickly enough, you can
  disable this feature, or change how long it takes before key presses
  start repeating, or how quickly key presses repeat.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the <gui>Repeat Keys</gui> switch to off.</p>
      <p>Namesto tega lahko tudi prilagodite drsnik <gui>Zakasnitev</gui> za spremembo kako dolgo morate držati tipko preden se začne ponavljati in prilagodite drsnik <gui>Hitrost</gui> za prilagoditev hitrosti ponavljanja pritiskov tipk.</p>
    </item>
  </steps>

</page>
