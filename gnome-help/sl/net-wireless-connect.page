<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="net-wireless-connect" xml:lang="sl">

  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Get on the internet — wirelessly.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Povezava z brezžičnim omrežjem</title>

<p>V primeru da imate računalnik z omogočenim brezžičnim internetom, se lahko povežete z brezžičnim omrežjem v dometu za dostop do interneta, ogled souporabljenih datotek na internetu in tako naprej.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-disabled-symbolic.svg"/>
    Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click the name of the network you want.</p>
    <p>If the name of the network is not shown, scroll down the list. If you
    still do not see the network, you may be out of range, or the network
    <link xref="net-wireless-hidden">might be hidden</link>.</p>
  </item>
  <item>
    <p>V primeru da je omrežje zaščiteno z geslom (<link xref="net-wireless-wepwpa">šifrirni ključ</link>), ob pozivu vnesite geslo in kliknite <gui>Poveži se</gui>.</p>
    <p>If you do not know the key, it may be written on the underside of the
    wireless router or base station, or in its instruction manual, or you may
    have to ask the person who administers the wireless network.</p>
  </item>
  <item>
    <p>Videz ikone omrežja se bo spremenil medtem ko se računalnik poskuša povezati z omrežjem.</p>
  </item>
  <item>
    <if:choose>
    <if:when test="platform:gnome-classic">
    <p>If the connection is successful, the icon will change to a dot with
    several curved bars above it
    (<media its:translate="no" type="image" mime="image/svg" src="figures/classic-topbar-network-wireless-signal-excellent.svg" width="28" height="28"/>). More bars
    indicate a stronger connection to the network. Fewer bars mean the
    connection is weaker and might not be very reliable.</p>
    </if:when>
    <p>If the connection is successful, the icon will change to a dot with
    several curved bars above it
    (<media its:translate="no" type="image" mime="image/svg" src="figures/topbar-network-wireless-signal-excellent.svg" width="28" height="28"/>). More bars
    indicate a stronger connection to the network. Fewer bars mean the
    connection is weaker and might not be very reliable.</p>
    </if:choose>
  </item>
</steps>

  <p>If the connection is not successful, you may be asked for your password
  again or it might just tell you that the connection has been disconnected.
  There are a number of things that could have caused this to happen. You could
  have entered the wrong password, the wireless signal could be too weak, or
  your computer’s wireless card might have a problem, for example. See
  <link xref="net-wireless-troubleshooting"/> for more help.</p>

  <p>Močnejša povezava z brezžičnim omrežjem ne pomeni nujno hitrejše internetne povezave ali višjih hitrosti prejemanja. Brezžična povezava vaš računalnik poveže z <em>napravo, ki zagotavlja brezžično povezavo</em> (kot je usmernik ali modem), vendar sta dve povezavi dejansko različni in bosta delovali pri različnih hitrostih.</p>

</page>
