<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="guide" style="a11y task" id="a11y" xml:lang="pl">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Używanie technologii pomagających osobom ze szczególnymi potrzebami wzrokowymi, słuchowymi i ruchowymi.</desc>
    <uix:thumb role="experimental-gnome-tiles" src="figures/tile-a11y.svg"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Ułatwienia dostępu</title>

  <p>System zawiera technologie wspomagające użytkowników z różnymi niepełnosprawnościami i szczególnymi potrzebami oraz do działania z często używanymi urządzeniami ułatwiającymi dostęp. Do górnego paska można dodać menu ułatwień dostępu dające szybki dostęp do tych funkcji.</p>

  <section id="vision">
    <title>Wady wzroku</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Ślepota</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Niedowidzenie</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Ślepota barw</title>
    </links>
    <links type="topic" style="linklist">
      <title>Pozostałe tematy</title>
    </links>
  </section>

  <section id="sound">
    <title>Wady słuchu</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Niepełnosprawność ruchowa</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Używanie myszy</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Klikanie i przeciąganie</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Używanie klawiatury</title>
    </links>
    <links type="topic" style="linklist">
      <title>Pozostałe tematy</title>
    </links>
  </section>
</page>
