<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-whydim" xml:lang="pl">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="41" date="2021-09-08" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ekran jest przygaszany, kiedy komputer nie jest używany, aby oszczędzać prąd.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Dlaczego ekran jest po chwili przygaszany?</title>

  <p>Jeśli można ustawić jasność ekranu, to będzie on przygaszany, kiedy komputer jest nieużywany, aby oszczędzać prąd. Kiedy komputer jest znowu używany, ekran jest rozjaśniany.</p>

  <p>Aby wyłączyć przygaszanie ekranu:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Zasilanie</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Zasilanie</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Przełącz <gui>Przygaszanie ekranu</gui> na <gui>◯</gui> (wyłączone) w sekcji <gui>Opcje oszczędzania energii</gui>.</p>
    </item>
  </steps>

  <p>Ekran będzie zawsze przygaszany, a w trybie zasilania „Oszczędzanie energii” będzie przygaszany bardziej agresywnie. Aby całkowicie wyłączyć przygaszanie ekranu, wybierz tryb „Zrównoważone zasilanie”.</p>

</page>
