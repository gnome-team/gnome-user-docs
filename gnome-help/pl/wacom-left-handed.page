<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-left-handed" xml:lang="pl">

  <info>
    <revision version="gnome:46" date="2024-03-11" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Przełączanie tabletu firmy Wacom między orientację leworęczną a praworęczną.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Używanie tabletu za pomocą lewej lub prawej ręki</title>

  <p>Część tabletów ma sprzętowe przyciski po jednej stronie. Tablet może zostać obrócony o 180 stopni, aby odpowiednio ułożyć te przyciski. Domyślnie używana jest orientacja dla osób praworęcznych. Aby zmienić ją na leworęczną:</p>

<steps>
  <item>
    <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Tablet firmy Wacom</gui>.</p>
  </item>
  <item>
    <p>Kliknij <gui>Tablet firmy Wacom</gui>, aby otworzyć panel.</p>
    <note style="tip"><p>Jeśli nie wykryto żadnego tabletu, to zostanie wyświetlony komunikat <gui>Proszę podłączyć lub włączyć tablet firmy Wacom</gui>. Kliknij <gui>Bluetooth</gui> na panelu bocznym, aby podłączyć tablet bezprzewodowy.</p></note>
  </item>
  <item>
    <p>Set the <gui>Left Hand Orientation</gui> switch to on.</p>
  </item>
</steps>

</page>
