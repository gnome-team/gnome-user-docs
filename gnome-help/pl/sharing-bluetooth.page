<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="pl">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014-2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Umożliwienie wysyłania plików na komputer przez Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Sterowanie udostępnianiem przez Bluetooth</title>

  <p>Można włączyć udostępnianie przez <gui>Bluetooth</gui>, aby odbierać pliki przez Bluetooth w katalogu <file>Pobrane</file>.</p>

  <steps>
    <title>Umożliwienie udostępniania plików w katalogu <file>Pobrane</file></title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Bluetooth</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Upewnij się, że <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> jest włączony</link>.</p>
    </item>
    <item>
      <p>Urządzenia z włączonym Bluetooth mogą wysyłać pliki do katalogu <file>Pobrane</file> komputera tylko, kiedy panel <gui>Bluetooth</gui> jest otwarty.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Można <link xref="about-hostname">zmienić</link> nazwę, pod jaką komputer jest znany innym urządzeniom.</p>
  </note>

</page>
