<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="pl">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adres IP to jak numer telefonu do komputera.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Czym jest adres IP?</title>

  <p>Adres IP (adres <em>Protokołu internetowego</em>) to adres każdego urządzenia połączonego z siecią (taką jak Internet).</p>

  <p>Adres IP jest podobny do numeru telefonu. Numer telefonu to unikalny zestaw cyfr identyfikujący telefon, aby inne osoby mogły na niego dzwonić. Podobnie adres IP to unikalny zestaw cyfr identyfikujący komputer, aby mógł on wysyłać i odbierać dane do i z innych komputerów.</p>

  <p>Obecnie większość adresów IP składa się z czterech zestawów cyfr oddzielonych kropkami. <code>192.168.1.42</code> to przykładowy adres IP.</p>

  <note style="tip">
    <p>Adres IP może być <em>dynamiczny</em> lub <em>statyczny</em>. Dynamiczne adresy IP są tymczasowo przydzielane za każdym razem, gdy komputer łączy się z siecią. Statyczne adresy IP są stałe i nie ulegają zmianie. Dynamiczne adresy IP są częściej używane niż adresy statyczne, które są zwykle używane tylko w szczególnych przypadkach, np. do administracji serwerem.</p>
  </note>

</page>
