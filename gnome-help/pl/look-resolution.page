<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="pl">

  <info>
    <link type="guide" xref="prefs-display" group="#first"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>
    <revision version="gnome:46" status="draft" date="2024-04-22"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Emanuel Cisár</name>
      <email>536429@mail.muni.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zmiana rozdzielczości ekranu i jego orientacji (obrotu).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Zmiana rozdzielczości lub orientacji ekranu</title>

  <p>Można zmienić, jak duże (lub jak szczegółowe) są rzeczy na ekranie zmieniając <em>rozdzielczość ekranu</em>. Można zmienić kierunek, w jakim położone są rzeczy (np. jeśli ekran jest obrotowy), zmieniając <em>obrót</em>.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ekrany</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ekrany</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Jeśli podłączonych jest kilka ekranów, ale nie jest używana opcja <gui>Ten sam obraz</gui>, to każdy ekran może mieć inne ustawienia. Wybierz ekran w obszarze podglądu.</p>
    </item>
    <item>
      <p>Wybierz orientację, rozdzielczość lub skalowanie oraz częstotliwość odświeżania.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Zastosuj</gui>. Nowe ustawienia zostaną będą używane przez 20 sekund, po czym zostaną przywrócone. W ten sposób, jeśli nowe ustawienia powodują brak obrazu, to poprzednie zostaną automatycznie przywrócone. Jeśli nowe ustawienia pasują, to kliknij przycisk <gui>Zachowaj zmiany</gui>.</p>
    </item>
  </steps>

<section id="orientation">
  <title>Orientacja</title>

  <p>Ekrany niektórych urządzeń mogą być fizycznie obracane w różnych kierunkach. Kliknij <gui>Orientacja</gui> w panelu i wybierz z opcji <gui>Pozioma</gui>, <gui>Pionowa (w prawo)</gui>, <gui>Pionowa (w lewo)</gui> lub <gui>Pozioma (odwrócona)</gui>.</p>

  <note style="tip">
    <p>Jeśli urządzenie automatycznie obraca ekran, to można go zablokować w obecnym położeniu za pomocą przycisku <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">zablokowanie obrotu</span></media> na dole <gui xref="shell-introduction#systemmenu">menu systemowego</gui>. Aby go odblokować, kliknij przycisk <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">odblokowanie obrotu</span></media>.</p>
  </note>

</section>

<section id="resolution">
  <title>Rozdzielczość</title>

  <p>Rozdzielczość to liczba pikseli (punktów na ekranie), które można wyświetlać w każdym kierunku. Każda rozdzielczość ma <em>format obrazu</em>, proporcje szerokości do wysokości. Ekrany panoramiczne mają format 16∶9, a tradycyjne 4∶3. Jeśli wybrano rozdzielczość niepasującą do formatu ekranu, to będzie on wyświetlał obraz tylko na części powierzchni (do górnej i dolnej krawędzi zostaną dodane czarne paski).</p>

  <p>Można wybrać preferowaną rozdzielczość z rozwijanej listy <gui>Rozdzielczość</gui>. Wybranie niewłaściwej dla ekranu może spowodować <link xref="look-display-fuzzy">rozmyty/rozpikselowany obraz</link>.</p>

</section>

<section id="native">
  <title>Natywna rozdzielczość</title>

  <p><em>Natywna rozdzielczość</em> ekranu laptopa lub monitora LCD to ta, która działa najlepiej: piksele sygnału wideo odpowiadają dokładnie pikselom na ekranie. Kiedy ekran jest ustawiony na inną rozdzielczość, to do przedstawienia pikseli potrzebna jest interpolacja, co zmniejsza jakość obrazu.</p>

</section>

<section id="refresh">
  <title>Częstotliwość odświeżania</title>

  <p>Częstotliwość odświeżania wskazuje, ile razy na sekundę na ekranie jest wyświetlany nowy obraz.</p>

  <section id="variable-refresh-rate">
    <title>Variable refresh rate (VRR)</title>
      <p>For smoother visuals and to optimize power consumption you can
    synchronize monitor's refresh rate with content output.</p>

  <note style="tip">
    <p>VRR is different from Adaptive V-Sync (late V-Sync),
    which dynamically toggles V-Sync based on frame rate. </p>
  </note>

  </section>
  <section id="preferred-refresh-rate">
    <title>Preferred Refresh Rate</title>
    <p>Specify your preferred refresh rate to maintain optimal visual quality.</p>
  </section>
</section>

<section id="scale">
  <title>Skalowanie</title>

  <p>Ustawienie skalowania zwiększa rozmiar obiektów wyświetlanych na ekranie tak, aby pasowały do gęstości ekranu, ułatwiając ich używanie. Wybierz <gui>100%</gui> lub <gui>200%</gui>.</p>

</section>

</page>
