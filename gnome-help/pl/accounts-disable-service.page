<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="pl">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Niektóre konta online mogą być używane do dostępu do wielu usług (np. kalendarza i poczty e-mail). Można kontrolować, które z tych usług mogą być używane przez programy.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Kontrolowanie, których usług konta online można używać</title>

  <p>Niektóre rodzaje kont online umożliwiają dostęp do kilku usług za pomocą tego samego konta użytkownika. Na przykład, konta Google dają dostęp do kalendarza, poczty e-mail i kontaktów. Można używać tylko części usług i wyłączyć inne: korzystać z poczty Google, ale używać kalendarza innego serwisu.</p>

  <p>Można wyłączyć część usług dostarczanych przez każde konto online:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Konta online</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Konta online</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Wybierz konto do zmiany z listy po prawej.</p>
    </item>
    <item>
      <p>Zostanie wyświetlona lista usług dostępnych dla tego konta pod tekstem <gui>Użycie dla</gui>. <link xref="accounts-which-application"/> zawiera informacje o tym, które programy używają jakich usług.</p>
    </item>
    <item>
      <p>Wyłącz dowolną usługę, która nie ma być używana.</p>
    </item>
  </steps>

  <p>Po wyłączeniu usługi dla konta programy na komputerze nie będą mogły już używać go do łączenia się z daną usługą.</p>

  <p>Aby włączyć usługę, wystarczy wrócić do panelu <gui>Konta online</gui> i ją przełączyć z powrotem.</p>

</page>
