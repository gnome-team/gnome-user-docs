<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="pl">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Używanie dłuższych, bardziej skomplikowanych haseł.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Wybór bezpiecznego hasła</title>

  <note style="important">
    <p>Hasła powinny być łatwe do zapamiętania, ale bardzo trudne do odgadnięcia przez innych (także programów komputerowych).</p>
  </note>

  <p>Wybór dobrego hasła pomoże zabezpieczać komputer. Jeśli hasło jest łatwe do odgadnięcia, to ktoś może to zrobić i uzyskać dostęp do prywatnych informacji użytkownika.</p>

  <p>Atakujący mogą nawet używać komputerów, aby spróbować metodycznie odgadnąć hasło, więc nawet hasło trudne dla człowieka może być niezwykle proste dla programu komputerowego do złamania. Poniżej znajduje się kilka wskazówek, jak wybrać dobre hasło.</p>
  
  <list>
    <item>
      <p>Używaj mieszanki wielkich i małych znaków, cyfr, symboli i spacji. Utrudnia to odgadnięcie hasła: więcej rodzajów znaków oznacza więcej możliwych haseł, których ktoś próbujący odgadnąć musi sprawdzić.</p>
      <note>
        <p>Dobrym sposobem na wybór hasła jest wybranie pierwszych liter każdego słowa w zdaniu, które można zapamiętać. To zdanie może być tytułem filmu, książki, utworu lub albumu. Na przykład z tytułu „Kongres futurologiczny: ze wspomnień Ijona Tichego” robi się Kf:zwIT lub kfzwit lub kf: zwit.</p>
      </note>
    </item>
    <item>
      <p>Hasło powinno być jak najdłuższe. Im więcej znaków zawiera, tym dłużej zajmuje jego odgadnięcie przez osobę lub komputer.</p>
    </item>
    <item>
      <p>Nie używaj słów występujących w standardowym słowniku dowolnego języka. Łamiący hasła najpierw próbują właśnie takie. Najczęstszym haseł jest „hasło” — co można złamać błyskawicznie!</p>
    </item>
    <item>
      <p>Nie używaj informacji osobistych, takich jak data, numer rejestracyjny ani imion członków rodziny.</p>
    </item>
    <item>
      <p>Nie używaj żadnych rzeczowników.</p>
    </item>
    <item>
      <p>Wybierz hasło, które można szybko wpisać, aby zmniejszyć ryzyko, że ktoś je odgadnie obserwując jego wpisywanie.</p>
      <note style="tip">
        <p>Nigdy nie zapisuj haseł. Można je łatwo znaleźć!</p>
      </note>
    </item>
    <item>
      <p>Używaj różnych haseł do różnych rzeczy.</p>
    </item>
    <item>
      <p>Używaj różnych haseł na różnych kontach.</p>
      <p>Jeśli używasz tego samego hasła na wszystkich kontach, to po odgadnięciu ktoś będzie miał od razu dostęp do wszystkich kont.</p>
      <p>Zapamiętane wielu haseł może być trudne. Chociaż nie jest to tak bezpieczne jak używanie różnych haseł do wszystkiego, to można używać tego samego hasła do rzeczy, które nie mają znaczenia (np. stron internetowych), a różnych dla ważnych (np. konta w banku internetowym i poczty e-mail).</p>
   </item>
   <item>
     <p>Regularnie zmieniaj hasło.</p>
   </item>
  </list>

</page>
