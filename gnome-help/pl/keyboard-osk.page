<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="pl">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Używanie klawiatury ekranowej do wpisywania tekstu przez klikanie przycisków za pomocą myszy lub ekranu dotykowego.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Klawiatura ekranowa</title>

  <p>Jeśli do komputera nie jest podłączona żadna klawiatura lub nie chcesz jej używać, to można włączyć <em>klawiaturę ekranową</em>, aby wpisywać tekst.</p>

  <note>
    <p>Klawiatura ekranowa jest włączana automatycznie, jeśli używany jest ekran dotykowy.</p>
  </note>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>Switch the <gui>Screen Keyboard</gui> switch to on.</p>
    </item>
  </steps>

  <p>Od teraz kiedy można coś pisać, klawiatura ekranowa będzie otwierana na dole ekranu.</p>

  <p>Kliknij przycisk <gui style="button">?123</gui>, aby wpisywać liczby i symbole. Więcej symboli jest dostępnych po kliknięciu przycisku <gui style="button">=/&lt;</gui>. Aby wrócić do klawiatury alfabetycznej, kliknij przycisk <gui style="button">ABC</gui>.</p>

  <p>Można kliknąć ikonę <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">w dół</span></media></gui>, aby tymczasowo ukryć klawiaturę. Zostanie ona ponownie wyświetlona po następnym kliknięciu czegoś, w czym można pisać. Na ekranie dotykowym można także wyciągnąć klawiaturę <link xref="touchscreen-gestures">przeciągając w górę od dolnej krawędzi ekranu</link>.</p>
  <p>Kliknij przycisk <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flaga</span></media></gui>, aby zmienić ustawienia <link xref="session-language">języka</link> lub <link xref="keyboard-layouts">źródeł wprowadzania</link>.</p>

</page>
