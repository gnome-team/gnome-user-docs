<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-tiled" xml:lang="pl">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Maksymalizowanie dwóch okien obok siebie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>


<title>Układanie okien</title>

  <p>Można zmaksymalizować okno tylko po lewej lub prawej stronie ekranu, umożliwiając umieszczenie dwóch okien obok siebie, aby szybko przełączać między nimi.</p>

  <p>Aby zmaksymalizować okno po jednej stronie ekranu, chwyć pasek tytułu i przeciągnij je do lewej lub prawej strony, aż połowa ekranu zostanie wyróżniona. Aby zrobić to za pomocą klawiatury, przytrzymaj klawisz <key xref="keyboard-key-super">Super</key> i naciśnij klawisz <key>W lewo</key> lub <key>W prawo</key>.</p>

  <p>Aby przywrócić okno do jego pierwotnego rozmiaru, przeciągnij je od strony ekranu, albo użyj tego samego skrótu klawiszowego, którego użyto do maksymalizacji.</p>

  <note style="tip">
    <p>Przytrzymaj klawisz <key>Super</key> i przeciągnij okno w dowolnym miejscu, aby je przenieść.</p>
  </note>

</page>
