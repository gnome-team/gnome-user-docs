<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="pl">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usuwanie użytkowników, którzy nie używają już komputera.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Usuwanie konta użytkownika</title>

  <p>Można <link xref="user-add">dodawać wiele kont użytkowników na komputerze</link>. Jeśli ktoś nie używa już komputera, to można usunąć jego konto.</p>

  <p>Do usuwania kont użytkowników wymagane są <link xref="user-admin-explain">uprawnienia administratora</link>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Wybierz <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Odblokuj</gui> w górnym prawym rogu i wpisz swoje hasło.</p>
    </item>
    <item>
      <p>Kliknij konto użytkownika do usunięcia pod <gui>Inni użytkownicy</gui>.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Usuń użytkownika…</gui>, aby usunąć to konto użytkownika.</p>
    </item>
    <item>
      <p>Każdy użytkownik ma własny katalog domowy dla swoich plików i ustawień. Można wybrać jego zachowanie lub usunięcie. Kliknij przycisk <gui>Usuń pliki</gui>, jeśli ma się pewność, że nie będą one więcej używane i potrzeba zwolnić miejsce na dysku. Zostaną one trwale usunięte. Nie można ich przywrócić. Przed usunięciem można zachować ich kopię na zewnętrznym urządzeniu.</p>
    </item>
  </steps>

</page>
