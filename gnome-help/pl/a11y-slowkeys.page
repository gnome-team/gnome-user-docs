<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Opóźnienie między naciśnięciem klawisza a pojawieniem się litery na ekranie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Włączenie powolnych klawiszy</title>

  <p>Włącz <em>powolne klawisze</em>, jeśli potrzebujesz opóźnienia między naciśnięciem klawisza a pojawieniem się litery na ekranie. Oznacza to, że trzeba przytrzymać każdy klawisz podczas pisania. Użyj powolnych klawiszy, jeśli przypadkowo naciskasz kilka klawiszy na raz podczas pisania, lub jeśli naciśnięcie właściwego klawisza za pierwszym razem sprawia trudności.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Select the <gui>Typing</gui> section to open it.</p>
    </item>
    <item>
      <p>In the <gui>Typing Assist</gui> section, switch the <gui>Slow Keys</gui> switch to on.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Szybkie przełączanie powolnych klawiszy</title>
    <p>Przełącz <gui>Włączanie za pomocą klawiatury</gui>, aby przełączać powolne klawisze za pomocą klawiatury. Po włączeniu tej opcji można nacisnąć i przytrzymać klawisz <key>Shift</key> przez osiem sekund, aby włączyć lub wyłączyć powolne klawisze.</p>
    <p>Można także włączać i wyłączać powolne klawisze klikając <link xref="a11y-icon">ikonę ułatwień dostępu</link> na górnym pasku i wybierając <gui>Powolne klawisze</gui>. Ikona ułatwień dostępu jest widoczna, jeśli włączono co najmniej jedno ustawienie w panelu <gui>Ułatwienia dostępu</gui>.</p>
  </note>

  <p>Użyj suwaka <gui>Opóźnienie akceptacji</gui>, aby zmienić czas, po jakim przytrzymany klawisz zostanie zarejestrowany. <link xref="a11y-bouncekeys"/> zawiera więcej informacji.</p>

  <p>Komputer może wydawać dźwięki po naciśnięciu klawisza, po akceptacji wciśnięcia lub po odrzuceniu wciśnięcia z powodu za krótkiego czasu przytrzymania.</p>

</page>
