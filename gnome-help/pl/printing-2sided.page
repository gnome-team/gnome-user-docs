<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-2sided" xml:lang="pl">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Drukowanie na obu stronach papieru lub wielu stron na kartkę.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Drukowanie dwustronne i układy wielostronicowe</title>

  <p>Aby wydrukować na obu stronach kartki papieru:</p>

  <steps>
    <item>
      <p>Otwórz okno wydruku naciskając klawisze <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Przejdź do karty <gui>Ustawienia strony</gui> okna wydruku i wybierz opcję z rozwijanej listy <gui>Dwustronnie</gui>. Jeśli opcja jest wyłączona, to drukowanie obustronne jest niedostępne dla tej drukarki.</p>
      <p>Drukarki obsługują drukowanie dwustronne na różne sposoby. Warto poeksperymentować z używaną drukarką, aby zobaczyć jak działa.</p>
    </item>
    <item>
      <p>Można także drukować więcej niż jedną stronę dokumentu na <em>stronę</em> kartki. Użyj opcji <gui>Stron na kartkę</gui>.</p>
    </item>
  </steps>

  <note>
    <p>Dostępność tych opcji może zależeć od typu używanej drukarki, a także używanego programu. Ta opcja może nie zawsze być dostępna.</p>
  </note>

</page>
