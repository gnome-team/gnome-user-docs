<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="pl">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision version="gnome:44" date="2023-12-30" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wyświetlanie stanu akumulatora i podłączonych urządzeń.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Sprawdzanie stanu akumulatora</title>

  <steps>

    <title>Wyświetlanie stanu akumulatora i podłączonych urządzeń</title>

    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Zasilanie</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Zasilanie</gui>, aby otworzyć panel. Zostanie wyświetlony stan naładowania <gui>Akumulatorów</gui> i znanych <gui>Urządzeń</gui>.</p>
    </item>

  </steps>

    <p>Jeśli wykryto wewnętrzny akumulator, to sekcja <gui>Akumulatory</gui> wyświetla stan jednego lub więcej akumulatorów laptopa. Pasek pokazuje procent naładowania, a także czas pozostały do pełnego naładowania, jeśli komputer jest podłączony do prądu, oraz pozostały czas pracy, jeśli komputer jest zasilany z akumulatora.</p>

    <p>Sekcja <gui>Urządzenia</gui> wyświetla stan podłączonych urządzeń.</p>
    
    <p>The <link xref="status-icons#batteryicons">status icon</link> in the top
    bar shows the charge level of the main internal battery, and whether it is
    currently charging or not. The battery indicator in the
    <gui xref="shell-introduction#systemmenu">system menu</gui> shows
    the charge as a percentage. The top bar icon can also be configured to
    display the <link xref="power-percentage">percentage</link>.</p>

</page>
