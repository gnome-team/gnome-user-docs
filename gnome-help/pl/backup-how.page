<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="pl">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Używanie Déjà Dup (lub innego programu) do wykonywania kopii zapasowych wartościowych plików i ustawień, aby chronić się przed ich utratą.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

<title>Jak wykonywać kopię zapasową</title>

  <p>Najłatwiejszym sposobem tworzenia kopii zapasowej plików i ustawień jest użycie programu, który zrobi to automatycznie. Dostępnych jest wiele programów tego typu, na przykład <app>Déjà Dup</app>.</p>

  <p>Pomoc wybranego programu pomoże ustawić preferencje kopii zapasowych, a także pokaże, jak przywrócić dane.</p>

  <p>Inną możliwością jest <link xref="files-copy">skopiowanie plików</link> w bezpieczne miejsce, takie jak zewnętrzny dysk twardy, serwis przechowywania plików lub dysk USB. <link xref="backup-thinkabout">Pliki użytkownika</link> i ustawienia zwykle znajdują się w katalogu domowym, więc można je z niego skopiować.</p>

  <p>Ilość danych, jaką można zachować w kopii zapasowej jest ograniczona rozmiarem urządzenia do przechowywania danych. Jeśli na urządzeniu jest wystarczająca ilość miejsca, to najlepiej jest zachować cały katalog domowy, z wyjątkiem:</p>

<list>
 <item><p>Pliki już zachowane na innym nośniku, takim jak dysk USB czy innym nośniku wymiennym.</p></item>
 <item><p>Pliki, które można łatwo odtworzyć. Na przykład programiści nie muszą zachowywać plików tworzonych podczas kompilowania programów. Lepiej jest upewnić się, że oryginalne pliki źródłowe są zachowane.</p></item>
 <item><p>Pliki w Koszu. Kosz znajduje się w katalogu <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
