<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="pl">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="gnome:46" date="2024-03-05" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Kontrola, kto może wyświetlać i modyfikować pliki i katalogi użytkownika.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>
  <title>Ustawianie uprawnień plików</title>

  <p>You can use file permissions to control who can view and edit files that
  you own. To view and set the permissions for a file, right click it and
  select <gui>Properties</gui>, then select <gui>Permissions</gui>.</p>

  <p><link xref="#files"/> i <link xref="#folders"/> poniżej zawierają informacje o typach uprawnień, które można ustawić.</p>

  <section id="files">
    <title>Pliki</title>

    <p>Można ustawiać uprawnienia dla właściciela pliku, właściciela grupy i wszystkich pozostałych użytkowników komputera. Użytkownik jest właścicielem swoich plików i może nadać sobie uprawnienia tylko do odczytu lub do odczytu i zapisu. Ustaw plik jako tylko do odczytu, aby przypadkiem go nie zmienić.</p>

    <p>Każdy użytkownik komputera należy do jakiejś grupy. W przypadku komputerów domowych najczęściej każdy użytkownik ma swoją grupę, a uprawnienia grup są rzadko używane. W środowiskach firmowych grupa są czasem używane dla wydziałów lub projektów. Oprócz właściciela każdy plik ma także jakąś grupę. Można ustawić grupę pliku i kontrolować uprawnienia wszystkich użytkowników w tej grupie. Można ustawiać grupę pliku tylko na grupę, do której się należy.</p>

    <p>Można także ustawiać uprawnienia dla użytkowników innych niż właściciel i tych w grupie pliku.</p>

    <p>If the file is a program, such as a script, you must switch on the
    <gui>Executable as Program</gui> switch to run it. Even with this option
    selected, the file manager will still open the file in an application. See
    <link xref="nautilus-behavior#executable"/> for more information.</p>
  </section>

  <section id="folders">
    <title>Katalogi</title>
    <p>Można ustawiać uprawnienia katalogów dla właściciela, grupy i pozostałych użytkowników. Informacje o uprawnieniach plików powyżej zawierają wyjaśnienie, kim jest właściciel, grupa i pozostali użytkownicy.</p>
    <p>Uprawnienia dla katalogu są inne, niż te dla pliku.</p>
    <terms>
      <item>
        <title><gui itst:context="permission">Brak</gui></title>
        <p>Użytkownik nie będzie nawet widział, jakie pliki są w katalogu.</p>
      </item>
      <item>
        <title><gui>Tylko wyświetlanie plików</gui></title>
        <p>Użytkownik będzie widział, jakie pliki są w katalogu, ale nie będzie mógł otwierać, tworzyć ani usuwać plików.</p>
      </item>
      <item>
        <title><gui>Dostęp do plików</gui></title>
        <p>Użytkownik będzie mógł otwierać pliki w katalogu (jeśli ma uprawnienia do otwierania konkretnego pliku), ale nie będzie mógł tworzyć nowych plików ani usuwać istniejących.</p>
      </item>
      <item>
        <title><gui>Tworzenie i usuwanie plików</gui></title>
        <p>Użytkownik będzie miał pełny dostęp do katalogu, w tym otwierania, tworzenia i usuwania plików.</p>
      </item>
    </terms>

    <p>You can also quickly set the file permissions for all the files
    in the folder by clicking <gui>Change Permissions for Enclosed Files…</gui>.
    Use the drop-down lists to adjust the permissions of contained files or
    folders, and click <gui>Change</gui>. Permissions are applied to files and
    folders in subfolders as well, to any depth.</p>
  </section>

</page>
