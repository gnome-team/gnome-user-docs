<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wrongnetwork" xml:lang="pl">
  <info>

    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-connect"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Modyfikowanie ustawień połączenia i usuwanie niepotrzebnych połączeń.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Mój komputer łączy się z niewłaściwą siecią</title>

  <p>Po włączeniu komputer automatycznie połączy się z sieciami bezprzewodowymi, z którymi połączono się w przeszłości. Jeśli łączy się z siecią, z którą nie powinien, wykonaj poniższe instrukcje.</p>

  <steps>
    <title>Aby usunąć sieć bezprzewodową z pamięci:</title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Znajdź sieć, z którą komputer <em>nie</em> ma się już łączyć.</p>
    </item>
    <item>
      <p>Kliknij przycisk <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">ustawienia</span></media> położony obok sieci.</p></item>
    <item>
      <p>Kliknij przycisk <gui>Zapomnij połączenie</gui>. Komputer nie będzie już łączyć się z tą siecią.</p>
    </item>
  </steps>

  <p><link xref="net-wireless-connect"/> zawiera instrukcje, jak ponownie połączyć z właśnie usuniętą siecią.</p>

</page>
