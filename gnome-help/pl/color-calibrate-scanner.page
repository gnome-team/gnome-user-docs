<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-scanner" xml:lang="pl">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kalibracja skanera jest ważna, aby zeskanowane dokumenty miały dokładne kolory.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Jak skalibrować skaner?</title>

  <p>Aby skaner dokładnie odwzorowywał kolory skanowanych dokumentów, należy go skalibrować.</p>

  <steps>
    <item>
      <p>Upewnij się, że skaner jest podłączony do komputera kablem lub przez sieć.</p>
    </item>
    <item>
      <p>Zeskanuj obiekt kalibracji i zapisz go jako nieskompresowany plik TIFF.</p>
    </item>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Kolor</gui> na panelu bocznym, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Wybierz skaner.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Skalibruj…</gui>, aby rozpocząć kalibrację.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Skanery są bardzo odporne na czas i temperaturę, więc zwykle nie muszą być ponownie kalibrowane.</p>
  </note>

</page>
