<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="pl">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zmiana czasu wygaszania ekranu, aby oszczędzać prąd.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Ustawianie czasu wygaszania ekranu</title>

  <p>Aby oszczędzać prąd, można dostosować czas, po jakim ekran jest wygaszany w przypadku nieaktywności. Można także całkowicie wyłączyć wygaszanie.</p>

  <steps>
    <title>Aby ustawić czas wygaszania ekranu:</title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Zasilanie</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Zasilanie</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Użyj rozwijanej listy <gui>Wygaszanie ekranu</gui> pod napisem <gui>Opcje oszczędzanie energii</gui>, aby ustawić czas do wygaszenia ekranu, albo całkowicie je wyłączyć.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Po nieaktywności komputera ekran zostanie automatycznie zablokowany z powodów bezpieczeństwa. <link xref="session-screenlocks"/> zawiera informacje, jak to zmienić.</p>
  </note>

</page>
