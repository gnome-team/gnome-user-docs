<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-tiled" xml:lang="it">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Massimizzare due finestre una a fianco dell'altra.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>


<title>Affiancare finestre</title>

  <p>Una finestra può essere massimizzata solo verso destra o sinistra dello schermo, consentendo di posizionare due finestre una a fianco dell'altra.</p>

  <p>To maximize a window along a side of the screen, grab the titlebar and
  drag it to the left or right side until half of the screen is highlighted.
  Using the keyboard, hold down <key xref="keyboard-key-super">Super</key> and
  press the <key>Left</key> or <key>Right</key> key.</p>

  <p>Per ripristinare una finestra alle sue dimensioni originali, trascinarla via dal lato dello schermo, oppure sfruttare la stessa scorciatoia usata per massimizzarla.</p>

  <note style="tip">
    <p>Hold down the <key>Super</key> key and drag anywhere in a window to move
    it.</p>
  </note>

</page>
