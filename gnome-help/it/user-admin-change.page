<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="it">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>You can allow users to make changes to the system by giving them
    administrative privileges.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Concedere i privilegi di amministrazione</title>

  <p>Administrative privileges are a way of deciding who can make changes to
  important parts of the system. You can change which users have administrative
  privileges and which ones do not. They are a good way of keeping your system
  secure and preventing potentially damaging unauthorized changes.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to change account types.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>System</gui>.</p>
    </item>
    <item>
      <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
      results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Under <gui>Other Users</gui>, select the user whose privileges you want
      to change.</p>
    </item>
    <item>
      <p>Set the <gui>Administrator</gui> switch to on.</p>
    </item>
    <item>
      <p>The user’s privileges will be changed when they next log in.</p>
    </item>
  </steps>

  <note>
    <p>The first user account on the system is usually the one that has
    administrator privileges. This is the user account that was created when
    you first installed the system.</p>
    <p>Non è consigliato avere troppi utenti con privilegi di amministrazione nel sistema.</p>
  </note>

</page>
