<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="it">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision version="gnome:44" date="2023-12-30" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Alexandre Franke</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Imparare come terminare la sessione, cambiare utente e altro.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Log out, power off or switch users</title>

  <p>When you have finished using your computer, you can turn it off, suspend
  it (to save power), or leave it powered on and log out.</p>

<section id="logout">
  <info>
    <link type="seealso" xref="user-add"/>
  </info>

  <title>Terminare la sessione o passare a un altro utente</title>

  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-exit-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menù utente</p>
      </media>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-exit-classic-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menù utente</p>
      </media>
    </if:when>
  </if:choose>

  <p>Per permettere l'uso del computer a un altro utente, è possibile sia terminare la propria sessione sia lasciare in esecuzione la sessione e semplicemente cambiare utente. Se si cambia utente, tutte le proprie applicazioni resteranno in esecuzione, disponibili come le si era lasciate quando si riaccederà.</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p>The <gui>Log Out</gui> and <gui>Switch User</gui> entries only appear in
    the menu if you have more than one user account on your system.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>The <gui>Switch User</gui> entry only appears in the menu if you have
    more than one user account on your system.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Bloccare lo schermo</title>

  <p>If you’re leaving your computer for a short time, you should lock your
  screen to prevent other people from accessing your files or running
  applications. When you return, you will see the
  <link xref="shell-lockscreen">lock screen</link>. Enter your
  password to log back in. If you don’t lock your screen, it will lock
  automatically after a certain amount of time.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and click the
    <media type="image" its:translate="no" src="figures/system-lock-screen-symbolic.svg">
      Lock
    </media>
  button.</p>

  <p>When your screen is locked, other users can log in to their own accounts
  by clicking <gui>Log in as another user</gui> at the bottom right of the login
  screen. You can switch back to your desktop when they are finished.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Sospendere</title>

  <p>To save power, suspend your computer when you are not using it. If you use
  a laptop, the system, by default, suspends your computer automatically when
  you close the lid.
  This saves your state to your computer’s memory and powers off most of the
  computer’s functions. A very small amount of power is still used during
  suspend.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select <gui>Suspend</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Spegnere o riavviare</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>If there are other users logged in, you may not be allowed to power off or
  restart the computer because this will end their sessions.  If you are an
  administrative user, you may be asked for your password to power off.</p>

  <note style="tip">
    <p>You may want to power off your computer if you wish to move it and do
    not have a battery, if your battery is low or does not hold charge well. A
    powered off computer also uses <link xref="power-batterylife">less
    energy</link> than one which is suspended.</p>
  </note>

</section>

</page>
