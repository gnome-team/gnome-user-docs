<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="user-admin-problems" xml:lang="it">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>
    <link type="seealso" xref="user-admin-change"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <!-- TODO: review that this is actually correct -->
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="authohar">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>You can do some things, like installing applications, only if you
    have administrative privileges.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Problemi causati dalla mancanza di privilegi di amministrazione</title>

  <p>You may experience a few problems if you do not have
  <link xref="user-admin-explain">administrative privileges</link>. Some tasks
  require administrative privileges in order to work, such as:</p>

  <list>
    <item>
      <p>connecting to networks or wireless networks,</p>
    </item>
    <item>
      <p>viewing the contents of a different disk partition (for example, a
      Windows partition), or</p>
    </item>
    <item>
      <p>installing new applications.</p>
    </item>
  </list>

  <p>You can <link xref="user-admin-change">change who has administrative
  privileges</link>.</p>

</page>
