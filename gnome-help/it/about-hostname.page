<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hostname" xml:lang="it">

  <info>
    <link type="guide" xref="about" group="hostname"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>
    <revision version="gnome:46" status="draft" date="2024-03-02"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the name used to identify your system on the network and to
    Bluetooth devices.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Change the device name</title>

  <p>Having a name that is uniquely recognisable makes it easier to identify
  your system on the network and when pairing Bluetooth devices.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>System</gui>.</p>
    </item>
    <item>
     <p>Select <guiseq><gui>Settings</gui><gui>System</gui></guiseq> from the
     results. This will open the <gui>System</gui> panel.</p>
    </item>
    <item>
      <p>Select <gui>About</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Device Name</gui> from the list.</p>
    </item>
    <item>
      <p>Enter a name for your system, and confirm.</p>
    </item>
  </steps>

  <note style="important">
    <p>Although the hostname is changed immediately after the system name has
    been changed, some running programs and services may have to be restarted
    for the change to take effect.</p>
  </note>

</page>
