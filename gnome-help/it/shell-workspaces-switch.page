<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="it">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use the workspace selector.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

 <title>Passare da uno spazio di lavoro all'altro</title>

 <steps>  
 <title>Usando il mouse:</title>
 <item>
  <p if:test="!platform:gnome-classic">Open the
  <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
  <p if:test="platform:gnome-classic">At the bottom right of the screen, click
  on one of the four workspaces to activate the workspace.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Click on the workspace partially displayed on the right-hand side of
  the current workspace to view the open windows on the next workspace.
  If the currently selected workspace is not the leftmost, click the workspace
  partially displayed on the left-hand side to view the previous workspace.</p>
  <p>If more than one workspace is being used, then you can also click on
  the <link xref="shell-workspaces">workspace selector</link> between the
  search field and the window list to directly access another workspace.</p>
 </item>
 <item if:test="!platform:gnome-classic">
  <p>Click on the workspace to activate the workspace.</p>
 </item>
 </steps>
 
 <list>
 <title>Usando la tastiera:</title>  
  <item>
    <p if:test="!platform:gnome-classic">Press
    <keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq>
    or <keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq> to move to
    the workspace shown left of the current workspace in the workspace selector.
    </p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>←</key></keyseq> to move to the workspace shown left
    of the current workspace on the <em>workspace selector</em>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Press <keyseq><key>Super</key><key>Page Down</key></keyseq> or
    <keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq> to move to the
    workspace shown right of the current workspace in the workspace selector.</p>
    <p if:test="platform:gnome-classic">Press <keyseq><key>Ctrl</key>
    <key>Alt</key><key>→</key></keyseq> to move to the workspace shown right of
    the current workspace on the <em>workspace selector</em>.</p>
  </item>
 </list>

</page>
