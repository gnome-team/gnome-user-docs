<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="it">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aggiungere (o rimuovere) alla dash le icone di programmi usati frequentemente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Mantenere le proprie applicazioni preferite nella dash</title>

  <p>To add an application to the <link xref="shell-introduction#activities">dash</link>
  for easy access:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Open the
      <gui xref="shell-introduction#activities">Activities</gui> overview by
      clicking <gui>Activities</gui> at the top left of the screen</p>
      <p if:test="platform:gnome-classic">Click the
      <gui xref="shell-introduction#activities">Applications</gui> menu at the
      top left of the screen and choose the <gui>Activities Overview</gui> item
      from the menu.</p></item>
    <item>
      <p>Click the grid button in the dash and find the application you want to
      add.</p>
    </item>
    <item>
      <p>Right-click the application icon and select <gui>Add to
      Favorites</gui>.</p>
      <p>Alternatively, you can click-and-drag the icon into the dash.</p>
    </item>
  </steps>

  <p>To remove an application icon from the dash, right-click the application
  icon and select <gui>Remove from Favorites</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Favorite applications
    also appear in the <gui>Favorites</gui> section of the
    <gui xref="shell-introduction#activities">Applications</gui> menu.</p>
  </note>

</page>
